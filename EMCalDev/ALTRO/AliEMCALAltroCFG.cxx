#include "AliEMCALAltroCFG.h"
#include <Riostream.h>

ClassImp(AliEMCALAltroCFG);

AliEMCALAltroCFG::AliEMCALAltroCFG() :
  TObject()
{
  for (Int_t i = 0; i < 20; i++) {
    fAltroCFG1[i] = 0;
    fAltroCFG2[i] = 0;
  }
}

void AliEMCALAltroCFG::Reset()
{
  for (Int_t i = 0; i < 20; i++) {
    fAltroCFG1[i] = 0;
    fAltroCFG2[i] = 0;
  }
}

void AliEMCALAltroCFG::Print(Option_t*) const
{
  for (Int_t i = 0; i < 20; i++) {
    printf("SM %d:  %u   %u\n", i, GetAltroCFG1(i), GetAltroCFG2(i));
  }
}

bool operator==(const AliEMCALAltroCFG& cfg1, const AliEMCALAltroCFG& cfg2)
{
  for (Int_t i = 0; i < 20; i++) {
    if (cfg1.fAltroCFG1[i] != cfg2.fAltroCFG1[i]) return false;
    if (cfg1.fAltroCFG2[i] != cfg2.fAltroCFG2[i]) return false;
  }

  return true;
}
