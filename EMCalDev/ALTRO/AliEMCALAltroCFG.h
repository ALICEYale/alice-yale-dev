#ifndef ALIEMCALALTROCFG
#define ALIEMCALALTROCFG

#include "TObject.h"

class AliEMCALAltroCFG : public TObject 
{
 public:
   AliEMCALAltroCFG();
   ~AliEMCALAltroCFG(){;}

   void   SetAltroCFG1(Int_t i, UInt_t a) { fAltroCFG1[i] = a    ; }
   void   SetAltroCFG2(Int_t i, UInt_t a) { fAltroCFG2[i] = a    ; }
   
   UInt_t GetAltroCFG1(Int_t i) const     { return fAltroCFG1[i] ; }
   UInt_t GetAltroCFG2(Int_t i) const     { return fAltroCFG2[i] ; }

   void Reset();
   
   void Print(Option_t* = "") const;

 private:
   
   UInt_t fAltroCFG1[20];    // 
   UInt_t fAltroCFG2[20];    //
   
   ClassDef(AliEMCALAltroCFG, 1)
 };

bool operator==(const AliEMCALAltroCFG& cfg1, const AliEMCALAltroCFG& cfg2);

#endif
