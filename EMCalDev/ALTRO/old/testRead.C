#include <AliOADBContainer.h>
#include <TFile.h>
#include <Riostream.h>

#include "AliEMCALAltroCFG.h"

void testRead()
{
  Int_t run = 1109;
  
  TFile* f = TFile::Open("EMCALAltroCFGtree.root", "read");
  
  AliOADBContainer* cont = dynamic_cast<AliOADBContainer*>(f->Get("EMCALAltroCFGcontainer"));

  if (!cont) {
    Printf("Could not find OADB container.");
    return;
  }
  
  AliEMCALAltroCFG* a = dynamic_cast<AliEMCALAltroCFG*>(cont->GetObject(run));

  if (!a) {
    Printf("Could not find ALTRO conf.");
    return;
  }  

  for (Int_t i = 0; i < 20; i++) {
    Printf("%d   %u   %u", i, a->GetAltroCFG1(i), a->GetAltroCFG2(i));
  }

  f->Close();
  delete f;
  f = 0;
}
