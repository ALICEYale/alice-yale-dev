// Analyze Invariant Mass for EMCAL clusters
//
// @Author: Raymond Ehlers <raymond.ehlers@yale.edu>

#include <cstdio>

#include <TString.h>
#include <TFile.h>
#include <TList.h>
#include <THnSparse.h>

void analyzeInvariantMass()
{
    TString filename = "AnalysisResults.root";
    TFile * fIn = TFile(filename.Data(), "READ");

    TList * analysisResults = static_cast<TList *>(->FindObject("AliAnalysisTaskEmcalInvariantMass_histos")); 

    if (analysisResults == 0)
    {
        Printf("Could not find output list");
        std::exit(0);
    }

    TString invariantMassLowName = "hInvariantMassLowVsPairEVsMult_%i";
    TString invariantMassName = "hInvariantMassVsPairEVsMult_%i";
    TString mixedEventsName = "hMixedEvent_0";
    
    THnSparse * mixedEvents = analysisResults->FindObject(mixedEventsName.Data());
    for (int cent = 0; i < 1; i++)
    {
        TH3F * invariantMassLow = analysisResults->FindObject(Form(invariantMassLowName.Data(), cent));
        TH3F * invariantMass = analysisResults->FindObject(Form(invariantMassName.Data(), cent));
    }
}
