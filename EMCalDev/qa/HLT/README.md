# ALICE OVERWATCH (Process Runs)

This project has been moved over to [ALICE OVERWATCH](https://github.com/raymondEhlers/OVERWATCH).

This folder contains previous versions and iterations of the project. It is kept around for historical reasons.

------

Some notes from this project are archived below:

Receiving messages from the HLT:

```
# Anyone on the emcal machine can access the information coming from the hlt machine
# http://superuser.com/a/97007
ssh -L 9999:localhost:9999 emcalMachine ssh -L 9999:localhost:60320 alihlt-dcsgw01
```

```
# Alternatively: put one ssh tunnel inside of another. Perhaps unnecessary
# http://superuser.com/a/97007
# Tunnel to emcalMachine
ssh -L 9998:alihlt-dcsgw01:22 -N emcalMachine
# Tunnel through the above tunnel to the hlt machine
ssh -L 9999:localhost:60320 -N -p 9998 localhost
```

```
# Recursively copy the files and folders in "test" to pdsf:
rsync -rvltph test/ rehlers@pdsf.nersc.gov:/project/projectdirs/alice/www/emcalMonitoring/2015/
```
