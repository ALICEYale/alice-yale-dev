#! usr/bin/python

# Periodically reads unread messages in email account, and downloads attachments
#
# Author: James Mulligan <james.mulligan@yale.edu>, Yale University
#

import easyimap # See https://pypi.python.org/pypi/easyimap/
import time
from os import makedirs
from os.path import exists

host = "imap.gmail.com"
user = "aliceEMCALtrigger@gmail.com"
password = "onlinemonitor"
mailbox = "INBOX"
imapper = easyimap.connect(host, user, password, mailbox)

while(True):
    mailList = []
    mailList = imapper.unseen(10)
    # Read any unread messages (up to 10), which are then automatically marked as read
    for mail in mailList:
        body = mail.body
        bodyLines = body.splitlines()
        runDir = bodyLines[0]
        outfileName = bodyLines[1]
        print "Processing mail from " + runDir

        outDir = "."
        runDir = outDir + "/" + runDir
        if not exists(runDir):
            makedirs(runDir)
        outfileName = outfileName[5:]
        outfileName = runDir + "/" + outfileName

        for attachment in mail.attachments:
            print "Incoming file name: " + attachment[0]
            att = attachment[1]
            outfile = open(outfileName, "wb")
            outfile.write(att)
            print "Wrote file " + outfileName 
            print "==================================="

    time.sleep(20)
