#!/usr/bin/env bash

# Drawing heavily on this source: http://backreference.org/2013/05/22/send-email-with-attachments-from-script-or-command-line/

# TODO: Handle command line arguments for these two values. They may change as we go along
fileName="../combined.root"
runNumber=10

# Subject is time since epoch
subject="Time $(date +%s)"
# filename is only the basename. From: https://en.wikipedia.org/wiki/Basename#Performance
body="Run${runNumber}
File "${fileName##*/}""

if [[ "$(uname -s)" == "Darwin" ]];
then
    echo "OS X"
    # Check if mutt exists to simplify sending attachments
    # From: http://stackoverflow.com/a/677212
    hash mutt 2>/dev/null || { echo >&2 "mutt is required but it's not installed. Aborting."; exit 1; }

    # If mutt exists, use it
    echo mutt -s "$subject" -a "$fileName" -- "aliceEMCALtrigger@gmail.com" \<\<\< "$body"
    mutt -s "$subject" -a "$fileName" -- "aliceEMCALtrigger@gmail.com" <<< "$body"
else
    echo "Linux"
    # Send via mailx. Does not work with the BSD version, so wont work on OS X!
    echo mail -s "$subject" -a "$fileName" "aliceEMCALtrigger@gmail.com" \<\<\< "$body"
    #mail -s "$subject" -a "$fileName" "aliceEMCALtrigger@gmail.com" <<< "$body"
fi


