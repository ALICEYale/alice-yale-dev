#!/usr/bin/env python

import os
from os import makedirs
from os.path import exists
import time
from calendar import timegm
from platform import system
from subprocess import Popen, PIPE, STDOUT
from distutils import spawn
from shutil import move

def enumerateFiles(dirPrefix):

    if dirPrefix == "":
        currentDir = os.getcwd()
    else:
        currentDir = os.path.abspath(dirPrefix)

    filesToSend = []
    for name in os.listdir(currentDir):
        if "EMC" in name and ".root" in name:
            filesToSend.append(name)

    return filesToSend

def sendNewFiles(filesToSend, dirPrefix, beVerbose):

    # Loop over all files to send
    for filename in filesToSend:

        # Extract time stamp and run number
        splitFilename = filename.replace(".root","").split("_")
        #print("splitFilename: ", splitFilename)
        timeString = "_".join(splitFilename[2:])
        #print("timeString: ", timeString)

        timeStamp = time.strptime(timeString, "%Y_%m_%d_%H_%M_%S")
        runString = splitFilename[1]
        runNumber = int(runString)

        #print "runNumber: ", runNumber
        #print "timeString: ", timeString

        # Email characteristics
        subject = "Time " + str(timegm(timeStamp))
        body = "Run" + str(runNumber) + "\nFile " + filename

        if system() == "Darwin":
            if beVerbose:
                print("Running on OS X")

            muttPath = spawn.find_executable("mutt")
            if not muttPath:
                print("mutt is required, but not found. Please install.")
                exit(1)

            #mutt -s "$subject" -a "$fileName" -- "aliceEMCALtrigger@gmail.com" <<< "$body"
            mailCall = ["mutt", "-s", subject, "-a", os.path.join(dirPrefix, filename), "--", "aliceEMCALtrigger@gmail.com"]

        else:
            if beVerbose:
                print("Running on Linux")

            #mail -s "$subject" -a "$fileName" "aliceEMCALtrigger@gmail.com" <<< "$body"
            mailCall = ["mail", "-s", subject, "-a", os.path.join(dirPrefix, filename), "aliceEMCALtrigger@gmail.com"]

        if beVerbose:
            print(mailCall)

        # Start 
        #p = Popen(mailCall, stdout=PIPE, stdin=PIPE, stderr=PIPE)
        # Pass 
        #p.communicate(body)[0]
        # Ensure that it is finished before moving on
        #p.wait()

        # Now move the file
        if not exists(os.path.join(dirPrefix, "sent")):
            makedirs(os.path.join(dirPrefix, "sent"))

        move(os.path.join(dirPrefix, filename), os.path.join(dirPrefix, "sent", filename))

def removeOldFiles():
    # Delete if older than an hour
    pass

def mailFiles():
    dirPrefix = "test"
    beVerbose = True

    filesToSend = enumerateFiles(dirPrefix)
    sendNewFiles(filesToSend, dirPrefix, beVerbose)
    removeOldFiles()

if __name__ == "__main__":
    mailFiles()
