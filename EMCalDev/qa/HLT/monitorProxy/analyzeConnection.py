#!/usr/bin/python

file = open("connectionLog.txt", "r")

lines = file.readlines()
connection = 0
noConnection = 0
for line in lines:
    if "0" in line:
        noConnection += 1
    elif "1" in line:
        connection += 1
print "connections: " + `connection`
print "failures: " + `noConnection`
print "live time: " + `(float(connection)/noConnection)`
