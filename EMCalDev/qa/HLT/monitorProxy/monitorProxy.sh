#!/usr/bin/env bash

# Script to monitor how often we are connected to the HLT proxy. Run as cron job each minute. 

. /home/james/alice/alice-env.sh -n 1 -q

proxyLogFile="/home/james/alice-yale-dev/EMCalDev/qa/HLT/monitorProxy/proxy.log"
connLogFile="/home/james/alice-yale-dev/EMCalDev/qa/HLT/monitorProxy/connectionLog.txt"

# Start connection to proxy and output to proxy.log
ZMQhistViewer in="REQ>tcp://lbnl5core.cern.ch:40321" Verbose timeout=30 -drawoptions="colzlogy" select="EMC" > $proxyLogFile 2>&1 &
FOO_PID=$!
sleep 30
kill $FOO_PID # Destroy the ZMQhistViewer so we do not accumulate them!

# Check if we have connected, and tabulate in connectionLog.txt
if grep "EMCTRQA" $proxyLogFile
then # if proxy runs
    echo "connection!"
    echo "1" >> $connLogFile
else # if no connection
    echo "no connection :("
    echo "0" >> $connLogFile
fi

# Clear our garbage
rm $proxyLogFile
rm EMC*
