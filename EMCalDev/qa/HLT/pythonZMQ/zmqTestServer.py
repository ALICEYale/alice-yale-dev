#!/usr/bin/env python

import zmq
import pickle
import sys
from time import sleep
from ROOT import gROOT, TFile

def zmqTestServer(context):
    socket = context.socket(zmq.PUB)
    socket.bind("tcp://*:5556")

    gROOT.Reset()

    fIn = TFile("combined.root", "READ")
    keysInFile = fIn.GetListOfKeys();

    # TEMP
    syncService = context.socket(zmq.REP)
    syncService.bind("tcp://*:5557")

    print "Waiting for subscribers"
    # Wait for sync message
    msg = syncService.recv()
    # Sync Reply
    syncService.send(b'')
    print "We have a subscriber! Continuing"
    # End temp

    count = 0

    while True:

        for key in keysInFile:
            classOfObject = gROOT.GetClass(key.GetClassName())
            # Ensure that we only take histograms
            if classOfObject.InheritsFrom("TH1"):
                hist = key.ReadObj()
                # Explained here: http://stackoverflow.com/a/24049232
                # Does not need to be pickle for the second part.
                socket.send_multipart([b'EMCAL', pickle.dumps(hist)])
        
        count += 1
        if (count % 2 == 0):
            print "End of run"
            sleep(10)
        else:
            print "Repeating"
            sleep(1)

def startZMQTestServer():
    context = zmq.Context()

    try:
        zmqTestServer(context)
    except KeyboardInterrupt:
        context.destroy()
        print("Caught keyboard interrupt. Exiting.")
        sys.exit(0)

if __name__ == "__main__":
    startZMQTestServer()
