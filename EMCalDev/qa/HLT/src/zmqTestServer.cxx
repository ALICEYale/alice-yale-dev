// ZMQ Test Server - ripped from ZMQROOTmerger to simplify the code
//
// Modifications: Raymond Ehlers <raymond.ehlers@cern.ch>, Yale University

#include <zmq.h>
#include <iostream>
#include <string>
#include <map>
#include <ctime>

#include <AliHLTDataTypes.h>
#include <AliHLTComponent.h>
#include <AliHLTMessage.h>
#include <AliZMQhelpers.h>

#include <TClass.h>
#include <TMap.h>
#include <TPRegexp.h>
#include <TObjString.h>
#include <TList.h>
#include <TMessage.h>
#include <TRint.h>
#include <TApplication.h>
#include <TROOT.h>
#include <TTimeStamp.h>
#include <TKey.h>
#include <TH1.h>
#include <TFile.h>

//this is meant to become a class, hence the structure with global vars etc.
//Also the code is rather flat - it is a bit of a playground to test ideas.
//TODO structure this at some point, e.g. introduce a SIMPLE unified way of handling
//zmq payloads, maybe a AliZMQmessage class which would by default be multipart and provide
//easy access to payloads based on topic or so (a la HLT GetFirstInputObject() etc...)

typedef std::map<std::string,std::string> stringMap;

//methods
TObject* UnpackMessage(zmq_msg_t* message);
Int_t Merge(TObject* object, TCollection* list);
TString GetFullArgString(int argc, char** argv);
int ProcessOptionString(TString arguments);
stringMap* TokenizeOptionString(const TString str);
int ProcessOption(TString option, TString value);
int InitZMQ();
void* InitZMQsocket(void* context, int socketMode, const char* configs);
int SendMergerData(void* socket);
void ResetOutputData();
void* work(void* param);
int Run();

int HandleDataIn(zmq_msg_t* topicMsg, zmq_msg_t* dataMsg, void* /*socket*/=NULL);
int HandleRequest(zmq_msg_t* /*topicMsg*/, zmq_msg_t* /*dataMsg*/, void* /*socket*/=NULL);
int SendRequest(void* /*socket*/);

//configuration vars
Bool_t  fVerbose = kFALSE;
int fNumberOfRepeats = 4;
TString fZMQsubscriptionIN = "";
TString fZMQconfigOUT  = "PUB@tcp://*:60201";
TString fZMQconfigMON  = "PUB@tcp://*:60212";

Bool_t  fResetOnSend = kFALSE;

//internal state
TMap fMergeObjectMap;        //map of the merged objects, all incoming stuff is merged into these
TMap fMergeListMap;          //map with the lists of objects to be merged in
int fMaxObjects = 1;        //trigger merge after this many messages

long fPushbackPeriod = -1;        //! in milliseconds
TTimeStamp fLastPushBackTime;

//ZMQ stuff
void* fZMQcontext = NULL;    //ze zmq context

void* fZMQmon = NULL;        //the request-reply socket, here we request the merged data
void* fZMQout = NULL;        //the monitoring socket, here we publish a copy of the data
void* fZMQinternal = NULL;   //the pair socket for thread communication

void* work(void* /*param*/)
{
    return NULL;
}

//_______________________________________________________________________________________
/*int Run()
{
    int rc = 0;
    //main loop
    while(1)
    {
        errno=0;

        int outType=alizmq_socket_type(fZMQout);

        //request first
        if (outType==ZMQ_REQ) SendRequest(fZMQout);

        //wait for the data
        //poll sockets - we want to take action on one of two conditions:
        //  1 - request comes in - then we merge whatever is not yet merged and send
        //  2 - data comes in - then we add it to the merging list
        zmq_pollitem_t sockets[] = { 
            //{ fZMQin, 0, ZMQ_POLLIN, 0 },
            { fZMQout, 0, ZMQ_POLLIN, 0 },
            //{ fZMQmon, 0, ZMQ_POLLIN, 0 },
            { fZMQinternal, 0, ZMQ_POLLIN, 0 },
        };
        rc = zmq_poll(sockets, 2, -1); //poll sockets
        if (rc==-1 && errno==ETERM)
        {
            //this can only happen it the context was terminated, one of the sockets are
            //not valid or operation was interrupted
            Printf("zmq_poll was interrupted! rc = %i, %s", rc, zmq_strerror(errno));
            break;
        }

        //data present socket 1 - out
        if (sockets[1].revents & ZMQ_POLLIN)
        {
            aliZMQmsg message;
            alizmq_msg_recv(&message, fZMQout, 0);
            for (aliZMQmsg::iterator i=message.begin(); i!=message.end(); ++i)
            {
                if (alizmq_socket_type(fZMQout)==ZMQ_REP) 
                { HandleRequest(i->first, i->second, fZMQout); }
                else
                { HandleDataIn(i->first, i->second, fZMQin); }
            }
            alizmq_msg_close(&message);
        }//socket 1

    }//main loop

    return 0;
}*/

int sendStartOrEndOfRun(int runNumber, bool startOfRun)
{
    //the topic
    // When sending the message, tell zmq that more is coming
    AliHLTDataTopic topic;
    if (startOfRun == true)
    {
        topic = kAliHLTDataTypeSOR;
    }
    else
    {
        topic = kAliHLTDataTypeEOR;
    }

    int rc = zmq_send(fZMQout, &topic, sizeof(topic), ZMQ_SNDMORE);

    //the data
    AliHLTRunDesc run;
    run.fRunNo = runNumber;

    // Initialize the message and send
    zmq_msg_t dataMsg;
    // Copying the message since it is small
    zmq_msg_init_size(&dataMsg, sizeof(run));
    memcpy(zmq_msg_data(&dataMsg), &run, zmq_msg_size(&dataMsg));
    // Not necessary to call close unless sending fails
    rc = zmq_msg_send(&dataMsg, fZMQout, 0);
    if (rc<0) 
    {
        printf("unable to send data: EOR for run %i\n", runNumber);
        zmq_msg_close(&dataMsg);
        return rc;
    }

    return rc;
}

int sendStartOfRun(int runNumber)
{
    //the topic
    AliHLTDataTopic topic = kAliHLTDataTypeSOR;
    int rc = zmq_send(fZMQout, &topic, sizeof(topic), 0);

    //the data
    AliHLTRunDesc run;
    run.fRunNo = runNumber;
    // Container for the data
    /*AliHLTBlockComponenetBlockData outputBlock;
    outputBlock.fPtr = &run;
    outputBlock.fSize = sizeof(run);
    zmq_send(fZMQout, outputBlock.fPtr, outputBlock.fSize, 0);*/

    zmq_send(fZMQout, &run, sizeof(run), 0);

    int flags = 0;
    //int rc = alizmq_msg_send(topic, run, fZMQout, flags, 0);
    return rc;
}

int sendEndOfRun(int runNumber)
{
    //the topic
    // When sending the message, tell zmq that more is coming
    AliHLTDataTopic topic = kAliHLTDataTypeEOR;
    int rc = zmq_send(fZMQout, &topic, sizeof(topic), ZMQ_SNDMORE);

    //the data
    AliHLTRunDesc run;
    run.fRunNo = runNumber;

    // Initialize the message and send
    zmq_msg_t dataMsg;
    // Copying the message since it is small
    zmq_msg_init_size(&dataMsg, sizeof(run));
    memcpy(zmq_msg_data(&dataMsg), &run, zmq_msg_size(&dataMsg));
    // Not necessary to call close unless sending fails
    rc = zmq_msg_send(&dataMsg, fZMQout, 0);
    if (rc<0) 
    {
        printf("unable to send data: EOR for run %i\n", runNumber);
        zmq_msg_close(&dataMsg);
        return rc;
    }

    return rc;
    // Container for the data
    /*AliHLTBlockComponenetBlockData outputBlock;
    outputBlock.fPtr = &run;
    outputBlock.fSize = sizeof(run);
    zmq_send(fZMQout, outputBlock.fPtr, outputBlock.fSize, 0);*/

    //zmq_msg_init_data()
    //zmq_send(fZMQout, &run, sizeof(run), 0);
    //alizmq_msg_send(data, fZMQout, 0);
    //int rc = alizmq_msg_send(topic, run, fZMQout, flags, 0);
    //return rc;
}

int Run()
{
    TFile * fIn = TFile::Open("combined.root", "READ");
    TKey *key;

    int count = 0;
    int runNumber = 10;
    int rc = 0;
    // Define struct needed for sleep
    struct timespec ts;
    ts.tv_nsec = 0;

    Printf("Waiting 2 seconds to allow for all necessary connections.");
    ts.tv_sec = 2;
    nanosleep(&ts, NULL);

    while (true)
    {
        if (count % fNumberOfRepeats == 0)
        {
            Printf("Start of run %i!", runNumber);
            sendStartOrEndOfRun(runNumber, true);
        }

        // From: https://root.cern.ch/root/html/tutorials/io/loopdir.C.html
        TIter next(fIn->GetListOfKeys());
        while ((key = (TKey*)next())) {
            TClass *cl = gROOT->GetClass(key->GetClassName());
            if (cl->InheritsFrom("TH1"))
            {
                TH1 *h = (TH1*)key->ReadObj();
                Printf("Sending: %s", h->GetName());

                //the topic
                AliHLTDataTopic topic = kAliHLTDataTypeHistogram;

                //the data
                int flags = 0;
                rc = alizmq_msg_send(topic, h, fZMQout, flags, 0);
                if (rc<0)
                {
                    Printf("could not send object");
                    break;
                }

                ts.tv_sec = 0;
                ts.tv_nsec = 10000000;
                nanosleep(&ts, NULL);
            }
            else
            {
                continue;
            }
        }
        
        ++count;
        if (count % fNumberOfRepeats == 0)
        {
            Printf("End of run %i!", runNumber);
            sendStartOrEndOfRun(runNumber, false);
            ts.tv_sec = 20;
            nanosleep(&ts, NULL);

            // Increment run number now that the run has ended
            ++runNumber;
        }
        else
        {
            Printf("Repeating");
            /*if (count % fNumberOfRepeats == 1)
            {
                ts.tv_sec = 1;
            }
            else
            {
                ts.tv_sec = 2;
            }*/
            ts.tv_sec = 6;
            nanosleep(&ts, NULL);
        }
    }

    return 0;
}

//_____________________________________________________________________
int HandleRequest(zmq_msg_t* topicMsg, zmq_msg_t* dataMsg, void* socket)
{
    std::string requestTopic;
    requestTopic.assign(static_cast<char*>(zmq_msg_data(topicMsg)), zmq_msg_size(topicMsg));

    std::string requestBody;
    requestBody.assign(static_cast<char*>(zmq_msg_data(dataMsg)), zmq_msg_size(dataMsg));

    if (fVerbose) Printf("in: request topic: %s body: %s", requestTopic.c_str(), requestBody.c_str());

    if (requestTopic.compare(0, 6, "CONFIG")==0)
    {
        //reconfigure (first send a reply to not cause problems on the other end)
        std::string reply = "Reconfiguring...";
        zmq_send(socket, "INFO", 4, ZMQ_SNDMORE);
        zmq_send(socket, reply.c_str(), reply.size(), 0);
        ProcessOptionString(requestBody.c_str());
    }
    else if (requestTopic.compare(0, 4, "INFO")==0)
    {
        //do nothing, maybe log, send back an empty info reply
        zmq_send(socket, "INFO", 4, ZMQ_SNDMORE);
        zmq_send(socket, 0, 0, 0);
    }
    else
    {
        SendMergerData(socket);
    }
    return 0;
}

//______________________________________________________________________________
int HandleDataIn(zmq_msg_t* topicMsg, zmq_msg_t* dataMsg, void* ignore)
{
    return 0;
}

//______________________________________________________________________________
int SendRequest(void*)
{
    return 0;
}

//______________________________________________________________________________
int SendMergerData(void* socket)
{
    //send back merged data, one object per frame

    int rc = 0;
    TIter mapIter(&fMergeObjectMap);
    TObject* object = NULL;
    int objectNumber=0;
    while ((object = mapIter.Next()))
    {
        //the topic
        AliHLTDataTopic topic = kAliHLTDataTypeTObject;
        //the data

        int flags = ( objectNumber < fMergeObjectMap.GetEntries()-1 ) ? ZMQ_SNDMORE : 0;    
        rc = alizmq_msg_send(topic, fMergeObjectMap.GetValue(object), socket, flags, 0);
        if (rc<0)
        {
            Printf("could not send object");
            break;
        }
        objectNumber++;

        if (fResetOnSend)
        {
            ResetOutputData();
            if (fVerbose) Printf("data reset on send");
        }
    }
    //always at least send an empty reply
    if (objectNumber==0) alizmq_msg_send("INFO","NODATA",socket,0);

    fLastPushBackTime.Set();

    return 0;
}

//______________________________________________________________________________
void ResetOutputData()
{
    fMergeObjectMap.DeleteAll();
}

//______________________________________________________________________________
int ProcessOption(TString option, TString value)
{
    //process option
    //to be implemented by the user

    //if (option.EqualTo("ZMQpollIn"))
    //{
    //  fZMQpollIn = (value.EqualTo("0"))?kFALSE:kTRUE;
    //}

    if (option.EqualTo("reset")) 
    {
        ResetOutputData();
    }
    else if (option.EqualTo("ResetOnSend"))
    {
        fResetOnSend = value.Contains("0")?kFALSE:kTRUE;
    }
    else if (option.EqualTo("MaxObjects"))
    {
        fMaxObjects = value.Atoi();
    }
    else if (option.EqualTo("numberOfRepeats"))
    {
        fNumberOfRepeats = value.Atoi();
    }
    else if (option.EqualTo("ZMQconfigOUT") || option.EqualTo("out"))
    {
        fZMQconfigOUT = value;
    }
    else if (option.EqualTo("Verbose"))
    {
        fVerbose=kTRUE;
    }
    else if (option.EqualTo("pushback-period"))
    {
        fPushbackPeriod=value.Atoi();
    }

    return 1; 
}

//_______________________________________________________________________________________
int InitZMQ()
{
    //init or reinit stuff
    int rc = 0;
    rc += alizmq_socket_init(fZMQout, fZMQcontext, fZMQconfigOUT.Data(), 0);
    return rc;
}

////////////////////////////////////////////////////////////////////////////////
//_______________________________________________________________________________________
TString GetFullArgString(int argc, char** argv)
{
    TString argString;
    TString argument="";
    if (argc>0) {
        for (int i=1; i<argc; i++) {
            argument=argv[i];
            if (argument.IsNull()) continue;
            if (!argString.IsNull()) argString+=" ";
            argString+=argument;
        }  
    }
    return argString;
}

//______________________________________________________________________________
int ProcessOptionString(TString arguments)
{
    //process passed options
    stringMap* options = TokenizeOptionString(arguments);
    for (stringMap::iterator i=options->begin(); i!=options->end(); ++i)
    {
        //Printf("  %s : %s", i->first.data(), i->second.data());
        ProcessOption(i->first,i->second);
    }
    delete options; //tidy up

    return 1; 
}

//______________________________________________________________________________
stringMap* TokenizeOptionString(const TString str)
{
    //options have the form:
    // -option value
    // -option=value
    // -option
    // --option value
    // --option=value
    // --option
    // option=value
    // option value
    // (value can also be a string like 'some string')
    //
    // options can be separated by ' ' or ',' arbitrarily combined, e.g:
    //"-option option1=value1 --option2 value2, -option4=\'some string\'"

    //optionRE by construction contains a pure option name as 3rd submatch (without --,-, =)
    //valueRE does NOT match options
    TPRegexp optionRE("(?:(-{1,2})|((?='?[^,=]+=?)))"
            "((?(2)(?:(?(?=')'(?:[^'\\\\]++|\\.)*+'|[^, =]+))(?==?))"
            "(?(1)[^, =]+(?=[= ,$])))");
    TPRegexp valueRE("(?(?!(-{1,2}|[^, =]+=))"
            "(?(?=')'(?:[^'\\\\]++|\\.)*+'"
            "|[^, =]+))");

    stringMap* options = new stringMap;

    TArrayI pos;
    const TString mods="";
    Int_t start = 0;
    while (1) {
        Int_t prevStart=start;
        TString optionStr="";
        TString valueStr="";

        //check if we have a new option in this field
        Int_t nOption=optionRE.Match(str,mods,start,10,&pos);
        if (nOption>0)
        {
            optionStr = str(pos[6],pos[7]-pos[6]);
            optionStr=optionStr.Strip(TString::kBoth,'\'');
            start=pos[1]; //update the current character to the end of match
        }

        //check if the next field is a value
        Int_t nValue=valueRE.Match(str,mods,start,10,&pos);
        if (nValue>0)
        {
            valueStr = str(pos[0],pos[1]-pos[0]);
            valueStr=valueStr.Strip(TString::kBoth,'\'');
            start=pos[1]; //update the current character to the end of match
        }

        //skip empty entries
        if (nOption>0 || nValue>0)
        {
            (*options)[optionStr.Data()] = valueStr.Data();
        }

        if (start>=str.Length()-1 || start==prevStart ) break;
    }

    //for (stringMap::iterator i=options->begin(); i!=options->end(); ++i)
    //{
    //  printf("%s : %s\n", i->first.data(), i->second.data());
    //}
    return options;
}

//_______________________________________________________________________________________
int main(int argc, char** argv)
{
    int mainReturnCode=0;

    //process args
    TString argString = GetFullArgString(argc,argv);
    ProcessOptionString(argString);

    //globally enable schema evolution for serializing ROOT objects
    TMessage::EnableSchemaEvolutionForAll(kTRUE);
    //the context
    fZMQcontext = zmq_ctx_new();

    //init stuff
    if (InitZMQ()<0) {
        Printf("failed init");
        return 1;
    }

    Run();

    //destroy ZMQ sockets
    zmq_close(fZMQout);
    zmq_ctx_destroy(fZMQcontext);

    return mainReturnCode;
}

