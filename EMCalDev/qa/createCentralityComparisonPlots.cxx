#include <vector>

#include <TString.h>
#include <TFile.h>
#include <TList.h>
#include <TCanvas.h>
#include <TH2D.h>
#include <TH1D.h>
#include <TProfile.h>
#include <THnSparse.h>
#include <TLegend.h>
#include <TStyle.h>
#include <TSystem.h>
#include <TPad.h>
#include <TROOT.h>

void plotForTrigger(TCanvas & canvas, TLegend * leg, std::vector <TProfile *> & emcMedianProjection, std::vector <TProfile *> & dmcMedianProjection, int positionInArray, int colorOne, int colorTwo, TString triggerType)
{
    canvas.cd();
    TString drawOptions = "same";
    if (triggerType == "MB")
    {
        drawOptions = "";
        emcMedianProjection.at(positionInArray)->SetMaximum(160);
        emcMedianProjection.at(positionInArray)->GetYaxis()->SetTitle("<Patch median> (GeV)");
    }
    emcMedianProjection.at(positionInArray)->SetLineColor(colorOne);
    //emcMedianProjection.at(positionInArray)->SetMarkerStyle(kOpenCircle);
    //emcMedianProjection.at(positionInArray)->SetMarkerSize(1);
    emcMedianProjection.at(positionInArray)->Draw(drawOptions.Data());
    dmcMedianProjection.at(positionInArray)->SetLineColor(colorTwo);
    //dmcMedianProjection.at(positionInArray)->SetMarkerStyle(kOpenSquare);
    //dmcMedianProjection.at(positionInArray)->SetMarkerSize(1);
    dmcMedianProjection.at(positionInArray)->Draw("same");
    leg->AddEntry(emcMedianProjection.at(positionInArray), Form("EMCal Median - %s", triggerType.Data()), "L");
    leg->AddEntry(dmcMedianProjection.at(positionInArray), Form("DCal Median - %s", triggerType.Data()), "L");
}

void createCentralityComparisonPlots()
{
    // Setup style
    gStyle->SetOptStat(0);

    // Setup output
    gSystem->Exec("mkdir -p output");

    //TString filename = "245705/AnalysisResults.root";
    TString filename = "train1023Data/Jets_EMC_PbPb_1023/AnalysisResults_allRuns.root";
    //TString filename = "train1023Data/Jets_EMC_PbPb_1023/AnalysisResults_allMinus245705.root";
    TString filename = "train1023Data/Jets_EMC_PbPb_1023/245705/AnalysisResults.root";
    TFile fIn = TFile(filename.Data(), "READ");

    // Loop over EMC and DMC
    //   AliEmcalTriggerQATask_EMCEJ1_histos;1   Doubly linked list
    //   AliAnalysisTaskSAQA_CaloClusters_TPC_EMCEJ1_histos;1    Doubly linked list
    TString qaTriggerName = "AliEmcalTriggerQATask_%s_histos";
    TString qaTaskName = "AliAnalysisTaskSAQA_CaloClusters_TPC_AfterTender_%s_histos";
    // Ex: INT7OfflineDMCMedianCentralityProj
    TString medianCentraltiyPrintName = "output/%s%s%sMedianCentrality%s";
    TString numClVsCentName = "output/%sCentVsNCl%s";
    // INT7, EMCEG1, EMCEJ1, DMCEG1, DMCEJ1
    TString detectorType[2] = { "EMC", "DMC" };
    TString triggerNames[5] = { "INT7", "EMCEG1", "EMCEJ1", "DMCEG1", "DMCEJ1" };
    TCanvas canvas;

    // Store 2D hists
    std::vector <TH2D *> dmcOfflineMedianVsCentrality;
    std::vector <TH2D *> dmcRecalcMedianVsCentrality;
    std::vector <TH2D *> emcOfflineMedianVsCentrality;
    std::vector <TH2D *> emcRecalcMedianVsCentrality;
    std::vector <TH2D *> numClusVsCentrality;
    // Store 1D hists
    std::vector <TProfile *> dmcOfflineMedianProjection;
    std::vector <TProfile *> dmcRecalcMedianProjection;
    std::vector <TProfile *> emcOfflineMedianProjection;
    std::vector <TProfile *> emcRecalcMedianProjection;
    std::vector <TProfile *> numClusVsCentralityProfile;

    // Loop over triggers types
    for (Int_t j = 0; j < 5; j++)
    {
        Printf("%s", Form(qaTriggerName, triggerNames[j].Data()));
        Printf("%s", Form(qaTaskName, triggerNames[j].Data()));
        TList * qaTrigger = static_cast<TList *>(fIn.GetObjectChecked(Form(qaTriggerName, triggerNames[j].Data()), "TList"));
        TList * qaTask = static_cast<TList *>(fIn.GetObjectChecked(Form(qaTaskName, triggerNames[j].Data()), "TList"));

        if (qaTrigger == 0)
        {
            Printf("qaTrigger is null. Continuing.");
            continue;
        }
        if (qaTask == 0)
        {
            Printf("qaTask is null. Continuing.");
            continue;
        }

        THnSparse * clusterHist = static_cast<THnSparse *>(qaTask->FindObject("fHistEventQA"));
        if (clusterHist == 0)
        {
            Printf("clusterHist is null. Continuing.");
            continue;
        }

        // TH2 of Centrality vs. number of clusters
        TString numClusVsCentralityName = Form(numClVsCentName, triggerNames[j].Data(), "");
        TString numClusVsCentralityTitle = Form("Number of Clusters vs. Centrality, Trigger %s", triggerNames[j].Data());
        numClusVsCentrality.push_back(clusterHist->Projection(1,0)); 
        numClusVsCentrality.at(j)->SetName(numClusVsCentralityName);
        numClusVsCentrality.at(j)->SetTitle(numClusVsCentralityTitle);
        numClusVsCentrality.at(j)->SetStats(0);
        numClusVsCentrality.at(j)->GetXaxis()->SetRangeUser(0,90);
        numClusVsCentrality.at(j)->Draw("colz");
        canvas.Print((numClusVsCentralityName + ".pdf").Data());

        // Profile of above TH2
        TString numClusVsCentralityProfName = Form(numClVsCentName, triggerNames[j].Data(), "Prof");
        TString numClusVsCentralityProfTitle = Form("Mean Number of Clusters vs. Centrality, Trigger %s", triggerNames[j].Data());
        numClusVsCentralityProfile.push_back(numClusVsCentrality.at(j)->ProfileX());
        numClusVsCentralityProfile.at(j)->SetTitle(numClusVsCentralityProfTitle);
        numClusVsCentralityProfile.at(j)->SetStats(0);
        numClusVsCentralityProfile.at(j)->GetXaxis()->SetRangeUser(0,90);
        numClusVsCentralityProfile.at(j)->Draw("colz");
        canvas.Print((numClusVsCentralityProfName + ".pdf").Data());

        THnSparse * eventHist = static_cast<THnSparse *>(qaTrigger->FindObject("fHistEventQA"));
        if (eventHist == 0)
        {
            Printf("eventHist is null. Continuing.");
            continue;
        }

        // 0 = cent
        // 1 = DCal median offline
        // 2 = EMCal median offline
        // 3 = DCal median recalc
        // 4 = EMCal median recalc
        // DMC
        TString dmcOfflineMedianVsCentralityName = Form(medianCentraltiyPrintName, triggerNames[j].Data(), "Offline", detectorType[1].Data(), "");
        dmcOfflineMedianVsCentrality.push_back(eventHist->Projection(1, 0));
	dmcOfflineMedianVsCentrality.at(j)->GetXaxis()->SetRangeUser(0,90);
        dmcOfflineMedianVsCentrality.at(j)->SetName(dmcOfflineMedianVsCentralityName.Data());
        dmcOfflineMedianVsCentrality.at(j)->SetTitle("");
        TString dmcRecalcMedianVsCentralityName = Form(medianCentraltiyPrintName, triggerNames[j].Data(), "Recalc", detectorType[1].Data(), "");
        dmcRecalcMedianVsCentrality.push_back(eventHist->Projection(3, 0));
	dmcRecalcMedianVsCentrality.at(j)->GetXaxis()->SetRangeUser(0,90);
        dmcRecalcMedianVsCentrality.at(j)->SetName(dmcRecalcMedianVsCentralityName.Data());
        dmcRecalcMedianVsCentrality.at(j)->SetTitle("");
        // EMC
        TString emcOfflineMedianVsCentralityName = Form(medianCentraltiyPrintName, triggerNames[j].Data(), "Offline", detectorType[0].Data(), "");
        emcOfflineMedianVsCentrality.push_back(eventHist->Projection(2, 0));
	emcOfflineMedianVsCentrality.at(j)->GetXaxis()->SetRangeUser(0,90);
	emcOfflineMedianVsCentrality.at(j)->SetName(emcOfflineMedianVsCentralityName.Data());
        emcOfflineMedianVsCentrality.at(j)->SetTitle("");
        TString emcRecalcMedianVsCentralityName = Form(medianCentraltiyPrintName, triggerNames[j].Data(), "Recalc", detectorType[0].Data(), "");
        emcRecalcMedianVsCentrality.push_back(eventHist->Projection(4, 0));
	emcRecalcMedianVsCentrality.at(j)->GetXaxis()->SetRangeUser(0,90);
        emcRecalcMedianVsCentrality.at(j)->SetName(emcRecalcMedianVsCentralityName.Data());
        emcRecalcMedianVsCentrality.at(j)->SetTitle("");


        TString dmcOfflineMedianProjectionName = Form(medianCentraltiyPrintName, triggerNames[j].Data(), "Offline", detectorType[1].Data(), "Proj");
        dmcOfflineMedianProjection.push_back(dmcOfflineMedianVsCentrality.at(j)->ProfileX(dmcOfflineMedianProjectionName.Data()));
	dmcOfflineMedianProjection.at(j)->GetXaxis()->SetRangeUser(0,90);
	dmcOfflineMedianProjection.at(j)->SetName(dmcOfflineMedianProjectionName.Data());
        dmcOfflineMedianProjection.at(j)->SetTitle("");
        TString dmcRecalcMedianProjectionName = Form(medianCentraltiyPrintName, triggerNames[j].Data(), "Recalc", detectorType[1].Data(), "Proj");
        dmcRecalcMedianProjection.push_back(dmcRecalcMedianVsCentrality.at(j)->ProfileX(dmcRecalcMedianProjectionName.Data()));
	dmcRecalcMedianProjection.at(j)->GetXaxis()->SetRangeUser(0,90);
	dmcRecalcMedianProjection.at(j)->SetName(dmcRecalcMedianProjectionName.Data());
        dmcRecalcMedianProjection.at(j)->SetTitle("");
        TString emcOfflineMedianProjectionName = Form(medianCentraltiyPrintName, triggerNames[j].Data(), "Offline", detectorType[0].Data(), "Proj");
        emcOfflineMedianProjection.push_back(emcOfflineMedianVsCentrality.at(j)->ProfileX(emcOfflineMedianProjectionName.Data()));
	emcOfflineMedianProjection.at(j)->GetXaxis()->SetRangeUser(0,90);
	emcOfflineMedianProjection.at(j)->SetName(emcOfflineMedianProjectionName.Data());
        emcOfflineMedianProjection.at(j)->SetTitle("");
        TString emcRecalcMedianProjectionName = Form(medianCentraltiyPrintName, triggerNames[j].Data(), "Recalc", detectorType[0].Data(), "Proj");
        emcRecalcMedianProjection.push_back(emcRecalcMedianVsCentrality.at(j)->ProfileX( emcRecalcMedianProjectionName.Data()));
	emcRecalcMedianProjection.at(j)->GetXaxis()->SetRangeUser(0,90);
	emcRecalcMedianProjection.at(j)->SetName(emcRecalcMedianProjectionName.Data());
        emcRecalcMedianProjection.at(j)->SetTitle("");

        // TODO: Only print dmc if above channel number!! -> Needs special merge!
        dmcOfflineMedianVsCentrality.at(j)->Draw("colz");
        canvas.Print((dmcOfflineMedianVsCentralityName + ".pdf").Data());
        dmcRecalcMedianVsCentrality.at(j)->Draw("colz");
        canvas.Print((dmcRecalcMedianVsCentralityName + ".pdf").Data());
        emcOfflineMedianVsCentrality.at(j)->Draw("colz");
        canvas.Print((emcOfflineMedianVsCentralityName + ".pdf").Data());
        emcRecalcMedianVsCentrality.at(j)->Draw("colz");
        canvas.Print((emcRecalcMedianVsCentralityName + ".pdf").Data());

        /*TLegend * leg = new TLegend(0.60, 0.45, 0.90, 0.80);
          leg->SetBorderSize(0);
          leg->SetFillStyle(0);
          dmcOfflineMedianProjection.at(j)->SetMarkerColor(kBlack);
          dmcOfflineMedianProjection.at(j)->SetLineColor(kBlack);
          dmcOfflineMedianProjection.at(j)->Draw();
          leg->AddEntry(dmcOfflineMedianProjection.at(j), "DMC Offline", "L");
          canvas.Print((dmcOfflineMedianProjectionName + ".pdf").Data());
          dmcRecalcMedianProjection.at(j)->SetMarkerColor(kBlue);
          dmcRecalcMedianProjection.at(j)->SetLineColor(kBlue);
          dmcRecalcMedianProjection.at(j)->Draw("same");
          leg->AddEntry(dmcRecalcMedianProjection.at(j), "DMC Recalc", "L");
          canvas.Print((dmcRecalcMedianProjectionName + ".pdf").Data());
          emcOfflineMedianProjection.at(j)->SetMarkerColor(kRed);
          emcOfflineMedianProjection.at(j)->SetLineColor(kRed);
          emcOfflineMedianProjection.at(j)->Draw("same");
          leg->AddEntry(emcOfflineMedianProjection.at(j), "EMC Offline", "L");
          canvas.Print((emcOfflineMedianProjectionName + ".pdf").Data());
          emcRecalcMedianProjection.at(j)->SetMarkerColor(kGreen+2);
          emcRecalcMedianProjection.at(j)->SetLineColor(kGreen+2);
          emcRecalcMedianProjection.at(j)->Draw("same");
          leg->AddEntry(emcRecalcMedianProjection.at(j), "EMC Recalc", "L");
          canvas.Print((emcRecalcMedianProjectionName + ".pdf").Data());
          leg->Draw("same");

        // Print
        TString printName = "output/";
        printName += triggerNames[j].Data();
        printName += "Projections.pdf";
        canvas.Print(printName.Data());

        delete leg;*/
    }

    // Classify by trigger
    //for (unsigned int j = 0; j < )
    {

    }

    // Plot vs calculation type
    Printf("Plot vs calculation type");
    for (unsigned int j = 0; j < 5; j++)
    {
        // Draw Proj
        TLegend * leg = new TLegend(0.60, 0.45, 0.90, 0.80);
        leg->SetBorderSize(0);
        leg->SetFillStyle(0);
        dmcOfflineMedianProjection.at(j)->SetMarkerColor(kBlack);
        dmcOfflineMedianProjection.at(j)->SetLineColor(kBlack);
        dmcOfflineMedianProjection.at(j)->Draw();
        leg->AddEntry(dmcOfflineMedianProjection.at(j), "DMC Offline", "L");
        //canvas.Print((dmcOfflineMedianProjectionName + ".pdf").Data());
        dmcRecalcMedianProjection.at(j)->SetMarkerColor(kBlue);
        dmcRecalcMedianProjection.at(j)->SetLineColor(kBlue);
        dmcRecalcMedianProjection.at(j)->Draw("same");
        leg->AddEntry(dmcRecalcMedianProjection.at(j), "DMC Recalc", "L");
        //canvas.Print((dmcRecalcMedianProjectionName + ".pdf").Data());
        emcOfflineMedianProjection.at(j)->SetMarkerColor(kRed);
        emcOfflineMedianProjection.at(j)->SetLineColor(kRed);
        emcOfflineMedianProjection.at(j)->Draw("same");
        leg->AddEntry(emcOfflineMedianProjection.at(j), "EMC Offline", "L");
        //canvas.Print((emcOfflineMedianProjectionName + ".pdf").Data());
        emcRecalcMedianProjection.at(j)->SetMarkerColor(kGreen+2);
        emcRecalcMedianProjection.at(j)->SetLineColor(kGreen+2);
        emcRecalcMedianProjection.at(j)->Draw("same");
        leg->AddEntry(emcRecalcMedianProjection.at(j), "EMC Recalc", "L");
        //canvas.Print((emcRecalcMedianProjectionName + ".pdf").Data());
        leg->Draw("same");

        // Print
        TString printName = "output/";
        printName += triggerNames[j].Data();
        printName += "Projections.pdf";
        canvas.Print(printName.Data());

        delete leg;
    }

    // Classify by triggers of offline
    Printf("Classify by triggers of offline");
    // { "INT7", "EMCEG1", "EMCEJ1", "DMCEG1", "DMCEJ1" };
    //
    // Jet trigger
    //
    TLegend * leg = new TLegend(0.60, 0.45, 0.90, 0.80);
    leg->SetBorderSize(0);
    leg->SetFillStyle(0);
    plotForTrigger(canvas, leg, emcOfflineMedianProjection, dmcOfflineMedianProjection, 0, kRed, kRed+2, "MB");
    plotForTrigger(canvas, leg, emcOfflineMedianProjection, dmcOfflineMedianProjection, 2, kBlue, kMagenta+2, "EMCal Jet Trigger");
    plotForTrigger(canvas, leg, emcOfflineMedianProjection, dmcOfflineMedianProjection, 4, kGreen+1, kGreen+3, "DCal Jet Trigger");
    leg->Draw("same");
    canvas.Print("output/offlineJetTriggers.pdf");

    delete leg;
    canvas.Clear();

    //
    // Gamma trigger
    //
    //void plotForTrigger(TCanvas & canvas, TLegend * leg, int positionInArray, int colorOne, int colorTwo, TString triggerType)
    leg = new TLegend(0.60, 0.45, 0.90, 0.80);
    leg->SetBorderSize(0);
    leg->SetFillStyle(0);
    plotForTrigger(canvas, leg, emcOfflineMedianProjection, dmcOfflineMedianProjection, 0, kRed, kRed+2, "MB");
    plotForTrigger(canvas, leg, emcOfflineMedianProjection, dmcOfflineMedianProjection, 1, kBlue, kMagenta+2, "EMCal Gamma Trigger");
    plotForTrigger(canvas, leg, emcOfflineMedianProjection, dmcOfflineMedianProjection, 3, kGreen+1, kGreen+3, "DCal Gamma Trigger");
    leg->Draw("same");
    canvas.Print("output/offlineGammaTriggers.pdf");
    delete leg;

    canvas.Clear();

    // Centrality vs median
    gStyle->SetPadBorderMode(0);
    gStyle->SetFrameBorderMode(0);
    Float_t small = 1e-5;
    Float_t big = 1;
    canvas.Divide(1,3, small, small);

    // INT7
    canvas.cd(1);
    gPad->SetBottomMargin(small);
    gPad->SetTopMargin(small);
    emcOfflineMedianVsCentrality.at(0)->GetYaxis()->SetTitle("");
    emcOfflineMedianVsCentrality.at(0)->Draw("colz");

    // EMCEG1
    canvas.cd(2);
    gPad->SetBottomMargin(small);
    gPad->SetTopMargin(small);
    gPad->SetTickx();
    emcOfflineMedianVsCentrality.at(1)->GetYaxis()->SetTitle("");
    emcOfflineMedianVsCentrality.at(1)->Draw("colz");

    // EMCEJ1
    canvas.cd(3);
    gPad->SetTopMargin(small);
    gPad->SetBottomMargin(small);
    gPad->SetTickx();
    emcOfflineMedianVsCentrality.at(2)->GetYaxis()->SetTitle("");
    emcOfflineMedianVsCentrality.at(2)->Draw("colz");

    canvas.Print("output/emcOfflineMedianVsCentrality.pdf");
    canvas.Clear();

    ///////////

    // Plot cluster profiles of all trigger classes on same plot
    TLegend * legCl = new TLegend(0.60, 0.45, 0.90, 0.80);
    legCl->SetBorderSize(0);
    legCl->SetFillStyle(0);
    numClusVsCentralityProfile.at(0)->SetMarkerColor(kBlack);
    numClusVsCentralityProfile.at(0)->SetLineColor(kBlack);
    //numClusVsCentralityProfile.at(0)->GetYaxis()->SetTitle("<Number Clusters>");
    numClusVsCentralityProfile.at(0)->GetYaxis()->SetTitle("");
    numClusVsCentralityProfile.at(0)->GetXaxis()->SetTitle("");
    //numClusVsCentralityProfile.at(0)->SetTitle("Mean Number of Clusters vs. Centrality");
    numClusVsCentralityProfile.at(0)->SetTitle("");
    numClusVsCentralityProfile.at(0)->Draw();
    legCl->AddEntry(numClusVsCentralityProfile.at(0), triggerNames[0].Data(), "L");
    numClusVsCentralityProfile.at(1)->SetMarkerColor(kBlue);
    numClusVsCentralityProfile.at(1)->SetLineColor(kBlue);
    numClusVsCentralityProfile.at(1)->Draw("same");
    legCl->AddEntry(numClusVsCentralityProfile.at(1), triggerNames[1].Data(), "L");	
    numClusVsCentralityProfile.at(2)->SetMarkerColor(kRed);
    numClusVsCentralityProfile.at(2)->SetLineColor(kRed);
    numClusVsCentralityProfile.at(2)->Draw("same");
    legCl->AddEntry(numClusVsCentralityProfile.at(2), triggerNames[2].Data(), "L");
    numClusVsCentralityProfile.at(3)->SetMarkerColor(kGreen+2);
    numClusVsCentralityProfile.at(3)->SetLineColor(kGreen+2);
    numClusVsCentralityProfile.at(3)->Draw("same");
    legCl->AddEntry(numClusVsCentralityProfile.at(3), triggerNames[3].Data(), "L");
    numClusVsCentralityProfile.at(4)->SetMarkerColor(kMagenta);
    numClusVsCentralityProfile.at(4)->SetLineColor(kMagenta);
    //    numClusVsCentralityProfile.at(4)->Draw("same");
    legCl->AddEntry(numClusVsCentralityProfile.at(4), triggerNames[4].Data(), "L");
    legCl->Draw("same");
    canvas.Print("output/ClusterProfiles.pdf");
    delete legCl;

    /////////////////////////

    TH2D* h1 = numClusVsCentrality.at(0); // INT7
    //h1->SetTitle("Number of Clusters vs. Centrality");
    h1->SetTitle("");
    h1->GetYaxis()->SetTitle("");
    TH2D* h2 = numClusVsCentrality.at(1); // EMCEG1
    h2->SetTitle("");
    h2->GetYaxis()->SetTitle("");
    TH2D* h3 = numClusVsCentrality.at(2); // EMCEJ1
    h3->SetTitle("");
    h3->GetYaxis()->SetTitle("");

    gROOT->Reset();
    TCanvas c1("c1","multipads",700,700);
    gStyle->SetPadBorderMode(0);
    gStyle->SetFrameBorderMode(0);
    small = 1e-5;
    big = 1;
    c1.Divide(1,3, small, small);

    c1.cd(1);
    gPad->SetBottomMargin(small);
    gPad->SetTopMargin(small);
    h1->Draw("colz");

    c1.cd(2);
    gPad->SetBottomMargin(small);
    gPad->SetTopMargin(small);
    h2->Draw("colz");

    c1.cd(3);
    gPad->SetTopMargin(small);
    gPad->SetBottomMargin(small);
    gPad->SetTickx();
    h3->Draw("colz");

    c1.Print("output/ClustersVsCentrality.pdf");

}
