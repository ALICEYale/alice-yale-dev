#! /usr/bin/env python

# Macro to plot Jet QA histograms, using AliEmcalTrackingQATask, AliAnalysisTaskEmcalJetQA, and AliAnalysisTaskEmcalJetSpectraQA
#
# Author: James Mulligan (james.mulligan@yale.edu)
#
# To run, you should configure:
#   (1) Set if pp, MC, Run2, and includePhos
#   (2) The input/output files
#   (3) The analysis task lists (if different than standard AOD names)

# General
import os
import sys
# ROOT
import ROOT
from ROOT import gStyle
from ROOT import gPad
from ROOT import TMath

# Config
ispp = False
isMC = False
isRun2 = True
includePhos = True

# Set location of AnalysisResults.root
inputDir = "./"
inputFile = os.path.join(inputDir + "AnalysisResults.root")

# Set directory for QA output
outputDir = "./outputQA/"
if not os.path.exists(outputDir):
  os.makedirs(outputDir)

# Open input file and get relevant lists
f = ROOT.TFile(inputFile)

# AliEmcalTrackingQATask
if isMC:
  TrackQAname = "AliEmcalTrackingQATask_mcparticles_tracks"
else:
  TrackQAname = "AliEmcalTrackingQATask__tracks"
TrackQAList = f.Get("%s_histos" % TrackQAname)
trackTHnSparse = TrackQAList.FindObject("fTracks")
# (Centrality, Pt, Eta, Phi, MC gen, Track type, sigma(pT)/pT)
if isMC:
  generatorTHnSparse = TrackQAList.FindObject("fParticlesPhysPrim")
  # (Pt, Eta, Phi, MC gen, Findable)
  matchedTHnSparse = TrackQAList.FindObject("fParticlesMatched")
  # (Pt-gen, Eta-gen, Phi-gen, Pt-det, Eta-det, Phi-det, (pT-gen - pT-det)/pT-det, Track type)

# AliAnalysisTaskEmcalJetQA
EmcalQAname = "AliAnalysisTaskEmcalJetQA_tracks_caloClusters_emcalCells"
EmcalQAList = f.Get("%s_histos" % EmcalQAname)
EmcalQASubList = EmcalQAList.FindObject("histos%s" % EmcalQAname)
EmcalClusterList = EmcalQASubList.FindObject("caloClusters")
EmcalCellList = EmcalQASubList.FindObject("emcalCells")
clusterTHnSparse = EmcalQASubList.FindObject("fHistEventQA")
# (Centrality, N tracks, pT leading track, N clusters, EMCal leading cluster E, DCal leading cluster E, N cells)

# AliAnalysisTaskEmcalJetSpectraQA
JetQAList = f.Get("AliAnalysisTaskEmcalJetSpectraQA_histos")
chargedJetList = JetQAList.FindObject("Jet_AKTChargedR020_tracks_pT0150_pt_scheme")
chargedJetTHnSparse = chargedJetList.FindObject("fHistJetObservables")
fullJetList = JetQAList.FindObject("Jet_AKTFullR020_tracks_pT0150_caloClusters_E0300_pt_scheme")
fullJetTHnSparse = fullJetList.FindObject("fHistJetObservables")
# (Centrality, eta, phi, pT, pTcorr, A, NEF, z_leading, N const, pT leading particle)

# Set number of centrality bins
nCentBins = 1
if not ispp:
  nCentBins = 4

#=======================================================================================================

# Plotting options
#gStyle.SetOptStat(0)
gStyle.SetOptTitle(0)

# Get number of events
histNEvent = TrackQAList.FindObject("fHistEventCount")
nEvents = histNEvent.GetBinContent(1)
print("N events: %d" % nEvents)

########################################################################################################
# Track histograms        ##############################################################################
########################################################################################################

#---------------------------------------------------------------------------------------------------
#                        phi distribution of hybrid tracks
#---------------------------------------------------------------------------------------------------

c1 = ROOT.TCanvas("c1","c1: Phi",600,450)
c1.cd()

# Project to (Phi, Track type)
if ispp:
  hPhiTracktype = trackTHnSparse.Projection(2,4)
else:
  hPhiTracktype = trackTHnSparse.Projection(3,5)

hPhiGlobal = hPhiTracktype.ProjectionY("PhiGlobal", 1, 1)
hPhiComplementary = hPhiTracktype.ProjectionY("PhiComplementary", 2, 2)

hPhiGlobal.SetLineColor(2)
hPhiGlobal.SetLineWidth(3)
hPhiGlobal.SetLineStyle(1)
hPhiComplementary.SetLineStyle(1)
hPhiComplementary.SetLineColor(4)
hPhiComplementary.SetLineWidth(3)

hPhiSum = hPhiGlobal.Clone()
hPhiSum.Add(hPhiComplementary)
hPhiSum.SetTitle("hPhiSum")
hPhiSum.SetName("hPhiSum")
hPhiSum.SetLineColor(1)
hPhiSum.SetMarkerColor(1)
hPhiSum.SetLineStyle(1)

hPhiGlobal.Scale(1./nEvents,"width") # Note: "width" normalizes also by the bin width
hPhiComplementary.Scale(1./nEvents,"width")
hPhiSum.Scale(1./nEvents,"width")

hPhiGlobal.SetTitle("#phi Distribution of Hybrid Tracks")
hPhiGlobal.GetYaxis().SetTitle("#frac{1}{N_{evts}} #frac{dN}{d#varphi}")
hPhiGlobal.GetYaxis().SetTitleSize(0.06)
hPhiGlobal.GetXaxis().SetTitleSize(0.06)
hPhiGlobal.GetYaxis().SetRangeUser(0,200)
gPad.SetLeftMargin(0.15)
gPad.SetRightMargin(0.05)
gPad.SetBottomMargin(0.13)
gPad.SetTopMargin(0.05)

hPhiGlobal.Draw()
hPhiComplementary.Draw("same")
hPhiSum.Draw("same")

leg1 = ROOT.TLegend(0.17,0.7,0.83,0.93,"Hybrid tracks")
leg1.SetFillColor(10)
leg1.SetBorderSize(0)
leg1.SetFillStyle(0)
leg1.SetTextSize(0.04)
leg1.AddEntry(hPhiGlobal, "w/ SPD & ITSrefit", "l")
leg1.AddEntry(hPhiComplementary, "w/o SPD & w/ ITSrefit", "l")
leg1.AddEntry(hPhiSum, "sum", "l")
leg1.Draw("same")

textNEvents = ROOT.TLatex()
textNEvents.SetNDC()
c1.cd()
textNEvents.DrawLatex(0.52,0.63,"#it{N}_{events} = %d" % nEvents)

outputFilename = os.path.join(outputDir + "hTrackPhi" + ".pdf")
c1.SaveAs(outputFilename)

# Also plot the TH2 phi vs. pT -- make sure that phi is uniform at all pT
# Project to (Pt, Phi)
if ispp:
  hPhiPtSum = trackTHnSparse.Projection(2,0)
else:
  hPhiPtSum = trackTHnSparse.Projection(3,1)
hPhiPtSum.Scale(1.,"width")
c2 = ROOT.TCanvas("c2","c2: phi vs pT",600,450)
c2.cd()
c2.SetLogz()
hPhiPtSum.Draw("colz")
outputFilename = os.path.join(outputDir + "hTrackPhiPt" + ".pdf")
c2.SaveAs(outputFilename)

#---------------------------------------------------------------------------------------------------
#                        pT distribution of hybrid tracks
#---------------------------------------------------------------------------------------------------

c4 = ROOT.TCanvas("c4","c4: pT",600,450)
c4.cd();

# Project to (Phi, Track type)
if ispp:
  hPtTracktype = trackTHnSparse.Projection(0,4)
else:
  hPtTracktype = trackTHnSparse.Projection(1,5)

hPtGlobal = hPtTracktype.ProjectionY("PtGlobal", 1, 1)
hPtComplementary = hPtTracktype.ProjectionY("PtComplementary", 2, 2)

hPtGlobal.SetLineColor(2)
hPtGlobal.SetLineWidth(3)
hPtGlobal.SetLineStyle(1)
hPtComplementary.SetLineStyle(1)
hPtComplementary.SetLineColor(4)
hPtComplementary.SetLineWidth(3)

hPtSum = hPtGlobal.Clone()
hPtSum.Add(hPtComplementary)
hPtSum.SetTitle("hPtSum")
hPtSum.SetName("hPtSum")
hPtSum.SetLineColor(1)
hPtSum.SetMarkerColor(1)
hPtSum.SetLineStyle(1)

hPtGlobal.Scale(1./nEvents, "width") # Note: already normalized by the bin width
hPtComplementary.Scale(1./nEvents, "width")
hPtSum.Scale(1./nEvents, "width")

hPtGlobal.SetTitle("p_T Distribution of Hybrid Tracks")
hPtGlobal.GetYaxis().SetTitle("#frac{1}{N_{evts}}#frac{dN}{dp_{T}} [GeV^{-1}]")
hPtGlobal.GetYaxis().SetTitleSize(0.06)
hPtGlobal.GetXaxis().SetTitleSize(0.06)
hPtGlobal.GetYaxis().SetRangeUser(1e-6,20)
gPad.SetLeftMargin(0.15)
gPad.SetRightMargin(0.05)
gPad.SetBottomMargin(0.14)
gPad.SetTopMargin(0.05)
gPad.SetLogy()

hPtGlobal.Draw()
hPtComplementary.Draw("same")
hPtSum.Draw("same")

leg2 = ROOT.TLegend(0.3,0.7,0.88,0.93,"Hybrid tracks")
leg2.SetFillColor(10)
leg2.SetBorderSize(0)
leg2.SetFillStyle(0)
leg2.SetTextSize(0.04)
leg2.AddEntry(hPtGlobal, "w/ SPD & ITSrefit", "l")
leg2.AddEntry(hPtComplementary, "w/o SPD & w/ ITSrefit", "l")
leg2.AddEntry(hPtSum, "sum", "l")
leg2.Draw("same")

textNEvents.SetNDC()
textNEvents.DrawLatex(0.55,0.6,"#it{N}_{events} = %d" % nEvents)

outputFilename = os.path.join(outputDir + "hTrackPt" + ".pdf")
c4.SaveAs(outputFilename)

#---------------------------------------------------------------------------------------------------
#                        pT resolution of hybrid tracks -- from track fitting
#---------------------------------------------------------------------------------------------------

c5 = ROOT.TCanvas("c5","c5: pT resolution",600,450)
c5.cd()

# Project to (Pt, Track type, pT resolution)
if ispp:
  hPtTracktypePtSigma1Pt = trackTHnSparse.Projection(0,4,5)
else:
  hPtTracktypePtSigma1Pt = trackTHnSparse.Projection(1,5,6)

# Project to global tracks and take profile, to get the pT resolution as a function of pT (Profile of pT vs pT*sigma(1/pT), i.e. pT vs sigma(pT)/pT)
hPtTracktypePtSigma1Pt.GetYaxis().SetRange(1,1)
hPtPtSigma1PtGlobal = hPtTracktypePtSigma1Pt.Project3D("zx")
profPtPtSigma1PtGlobal = hPtPtSigma1PtGlobal.ProfileX()
profPtPtSigma1PtGlobal.SetName("profPtPtSigma1PtGlobal")
profPtPtSigma1PtGlobal.SetTitle("profPtPtSigma1PtGlobal")
profPtPtSigma1PtGlobal.SetLineColor(2)
profPtPtSigma1PtGlobal.SetLineWidth(3)
profPtPtSigma1PtGlobal.SetMarkerStyle(21)
profPtPtSigma1PtGlobal.SetMarkerColor(2)
profPtPtSigma1PtGlobal.SetMaximum(0.3)
#profPtPtSigma1PtGlobal.GetYaxis().SetTitle("#it{p}_{T} #times #sigma(1/#it{p}_{T})")
profPtPtSigma1PtGlobal.GetYaxis().SetTitle(" #sigma(#it{p}_{T}) / #it{p}_{T}")
profPtPtSigma1PtGlobal.GetXaxis().SetTitleSize(0.06)
profPtPtSigma1PtGlobal.GetYaxis().SetTitleSize(0.06)
gPad.SetLeftMargin(0.15)
gPad.SetRightMargin(0.05)
gPad.SetBottomMargin(0.14)
gPad.SetTopMargin(0.05)
profPtPtSigma1PtGlobal.Draw()

# Project to complementary tracks and take profile
hPtTracktypePtSigma1Pt.GetYaxis().SetRange(2,2)
hPtPtSigma1PtComplementary = hPtTracktypePtSigma1Pt.Project3D("zx")
profPtPtSigma1PtComplementary = hPtPtSigma1PtComplementary.ProfileX()
profPtPtSigma1PtComplementary.SetName("profPtPtSigma1PtComplementary")
profPtPtSigma1PtComplementary.SetTitle("profPtPtSigma1PtComplementary")
profPtPtSigma1PtComplementary.SetLineColor(4)
profPtPtSigma1PtComplementary.SetLineWidth(3)
profPtPtSigma1PtComplementary.SetMarkerStyle(24)
profPtPtSigma1PtComplementary.SetMarkerColor(4)
profPtPtSigma1PtComplementary.Draw("same")

leg3 = ROOT.TLegend(0.21,0.6,0.88,0.88,"Hybrid tracks")
leg3.SetFillColor(10)
leg3.SetBorderSize(0)
leg3.SetFillStyle(0)
leg3.SetTextSize(0.04)
leg3.AddEntry(profPtPtSigma1PtGlobal, "w/ SPD & ITSrefit", "lp")
leg3.AddEntry(profPtPtSigma1PtComplementary, "w/o SPD & w/ ITSrefit", "lp")
leg3.Draw("same")

outputFilename = os.path.join(outputDir + "pTrackPtResolution" + ".pdf")
c5.SaveAs(outputFilename)


#---------------------------------------------------------------------------------------------------
#                        pT resolution of hybrid tracks -- from MC
#---------------------------------------------------------------------------------------------------

if isMC:
  # Plot distribution (pT-gen - pT-det)/pT-det
  c25 = ROOT.TCanvas("c25","c25: pT Res Dist MC",600,450)
  c25.cd()
  c25.SetLogy()
  hPtRes = matchedTHnSparse.Projection(6)
  hPtRes.Draw()
  outputFilename = os.path.join(outputDir + "pTrackPtResolutionMCDist" + ".pdf")
  c25.SaveAs(outputFilename)

  # Plot mean of the distribution as a function of pT, with error bars as the standard deviation of the distribution
  c24 = ROOT.TCanvas("c24","c24: pT Resolution MC",600,450)
  c24.cd()
  # Project to (Pt, Track type, pT resolution)
  hPtTracktypePtRes = matchedTHnSparse.Projection(3,7,6)
  # Project to global tracks and take profile, to get the pT resolution as a function of pT
  hPtTracktypePtRes.GetYaxis().SetRange(1,1)
  hPtPtResGlobal = hPtTracktypePtRes.Project3D("zx")
  profPtPtResGlobal = hPtPtResGlobal.ProfileX("prof",1,-1,"s") # set errors to standard deviation (rather than standard error on mean)
  profPtPtResGlobal.SetName("profPtPtResGlobal")
  profPtPtResGlobal.SetTitle("profPtPtResGlobal")
  profPtPtResGlobal.SetLineColor(2)
  profPtPtResGlobal.SetLineWidth(3)
  profPtPtResGlobal.SetMarkerStyle(21)
  profPtPtResGlobal.SetMarkerColor(2)
  profPtPtResGlobal.SetMaximum(0.3)
  profPtPtResGlobal.GetYaxis().SetTitle("(#it{p}_{T}^{gen} - #it{p}_{T}^{det}) / #it{p}_{T}^{det}")
  profPtPtResGlobal.GetXaxis().SetTitleSize(0.06)
  profPtPtResGlobal.GetYaxis().SetTitleSize(0.06)
  gPad.SetLeftMargin(0.15)
  gPad.SetRightMargin(0.05)
  gPad.SetBottomMargin(0.14)
  gPad.SetTopMargin(0.05)
  profPtPtResGlobal.Draw()
  # Project to complementary tracks and take profile
  hPtTracktypePtRes.GetYaxis().SetRange(2,2)
  hPtPtResComplementary = hPtTracktypePtRes.Project3D("zx")
  profPtPtResComplementary = hPtPtResComplementary.ProfileX()
  profPtPtResComplementary.SetName("profPtPtResComplementary")
  profPtPtResComplementary.SetTitle("profPtPtResComplementary")
  profPtPtResComplementary.SetLineColor(4)
  profPtPtResComplementary.SetLineWidth(3)
  profPtPtResComplementary.SetMarkerStyle(24)
  profPtPtResComplementary.SetMarkerColor(4)
  profPtPtResComplementary.Draw("same")
  leg3 = ROOT.TLegend(0.21,0.6,0.88,0.88,"Hybrid tracks")
  leg3.SetFillColor(10)
  leg3.SetBorderSize(0)
  leg3.SetFillStyle(0)
  leg3.SetTextSize(0.04)
  leg3.AddEntry(profPtPtResGlobal, "w/ SPD & ITSrefit", "lp")
  leg3.AddEntry(profPtPtResComplementary, "w/o SPD & w/ ITSrefit", "lp")
  leg3.Draw("same")
  outputFilename = os.path.join(outputDir + "pTrackPtResolutionMC" + ".pdf")
  c24.SaveAs(outputFilename)

#---------------------------------------------------------------------------------------------------
#                        Tracking efficiency
#---------------------------------------------------------------------------------------------------

if isMC:
  # Plot ratio of pT-gen-matched to pT-gen
  c26 = ROOT.TCanvas("c26","c26: TrackingEfficiency",600,450)
  c26.cd()
  
  hPtGenMatched = matchedTHnSparse.Projection(0)
  hPtGen = generatorTHnSparse.Projection(0)
  hPtGenMatched.Divide(hPtGen)
  
  hTrackingEfficiency = hPtGenMatched
  hTrackingEfficiency.GetYaxis().SetTitle("Tracking Efficiency")
  hTrackingEfficiency.SetMarkerStyle(21)
  hTrackingEfficiency.SetMarkerColor(2)
  hTrackingEfficiency.Draw("P")

  outputFilename = os.path.join(outputDir + "hTrackingEfficiency" + ".pdf")
  c26.SaveAs(outputFilename)

#---------------------------------------------------------------------------------------------------
#                        eta distribution of hybrid tracks
#---------------------------------------------------------------------------------------------------

c6 = ROOT.TCanvas("c6","c6: Eta",600,450)
c6.cd()

# Project to (Eta, Track type)
if ispp:
  hEtaTracktype = trackTHnSparse.Projection(1,4)
else:
  hEtaTracktype = trackTHnSparse.Projection(2,5)

hEtaGlobal = hEtaTracktype.ProjectionY("EtaGlobal", 1, 1)
hEtaComplementary = hEtaTracktype.ProjectionY("EtaComplementary", 2, 2)

hEtaGlobal.SetLineColor(2)
hEtaGlobal.SetLineWidth(3)
hEtaGlobal.SetLineStyle(1)
hEtaComplementary.SetLineStyle(1)
hEtaComplementary.SetLineColor(4)
hEtaComplementary.SetLineWidth(3)

hEtaSum = hEtaGlobal.Clone()
hEtaSum.Add(hEtaComplementary)
hEtaSum.SetTitle("hEtaSum")
hEtaSum.SetName("hEtaSum")
hEtaSum.SetLineColor(1)
hEtaSum.SetMarkerColor(1)
hEtaSum.SetLineStyle(1)

hEtaGlobal.Scale(1./nEvents,"width") # Note: "width" normalizes also by the bin width
hEtaComplementary.Scale(1./nEvents,"width")
hEtaSum.Scale(1./nEvents,"width")

hEtaGlobal.SetTitle("#eta Distribution of Hybrid Tracks")
hEtaGlobal.GetYaxis().SetTitle("#frac{1}{N_{evts}} #frac{dN}{d#eta}")
hEtaGlobal.GetYaxis().SetTitleSize(0.06)
hEtaGlobal.GetXaxis().SetTitleSize(0.06)
hEtaGlobal.GetYaxis().SetRangeUser(0,600)
gPad.SetLeftMargin(0.15)
gPad.SetRightMargin(0.05)
gPad.SetBottomMargin(0.13)
gPad.SetTopMargin(0.05)

hEtaGlobal.Draw()
hEtaComplementary.Draw("same")
hEtaSum.Draw("same")

leg1 = ROOT.TLegend(0.17,0.7,0.83,0.93,"Hybrid tracks")
leg1.SetFillColor(10)
leg1.SetBorderSize(0)
leg1.SetFillStyle(0)
leg1.SetTextSize(0.04)
leg1.AddEntry(hEtaGlobal, "w/ SPD & ITSrefit", "l")
leg1.AddEntry(hEtaComplementary, "w/o SPD & w/ ITSrefit", "l")
leg1.AddEntry(hEtaSum, "sum", "l")
leg1.Draw("same")

textNEvents = ROOT.TLatex()
textNEvents.SetNDC()
textNEvents.DrawLatex(0.65,0.87,"#it{N}_{events} = %d" % nEvents)

outputFilename = os.path.join(outputDir + "hTrackEta" + ".pdf")
c6.SaveAs(outputFilename)

# Also plot the TH2 eta vs. pT -- make sure that eta is uniform at all pT
# Project to (Pt, Eta)
if ispp:
  hEtaPtSum = trackTHnSparse.Projection(1,0)
else:
  hEtaPtSum = trackTHnSparse.Projection(2,1)
hEtaPtSum.Scale(1.,"width")
c3 = ROOT.TCanvas("c3","c3: eta vs pT",600,450)
c3.cd()
c3.SetLogz()
hEtaPtSum.Draw("colz")
outputFilename = os.path.join(outputDir + "hTrackEtaPt" + ".pdf")
c3.SaveAs(outputFilename)

#---------------------------------------------------------------------------------------------------
#                        eta-phi distribution of hybrid tracks
#---------------------------------------------------------------------------------------------------

# Project to (Eta, Phi)
if ispp:
  hEtaPhiSum = trackTHnSparse.Projection(1,2)
else:
  hEtaPhiSum = trackTHnSparse.Projection(2,3)
c7 = ROOT.TCanvas("c7","c7: Eta",600,450)
c7.cd()
c7.SetLogz()
hEtaPhiSum.Draw("colz")
outputFilename = os.path.join(outputDir + "hTrackEtaPhi" + ".pdf")
c7.SaveAs(outputFilename)

########################################################################################################
# Cluster histograms      ##############################################################################
########################################################################################################

# Plot EMCAL CLUSTERS --------------------------------------------------------------

# Get the EMCal phi/eta/E histograms, summed over centrality
hClusEMCalPhiEtaEnergy = EmcalClusterList.FindObject("fHistClusEMCalPhiEtaEnergy_0")
if nCentBins > 1:
  for centBin in range(1,nCentBins):
    name = "fHistClusEMCalPhiEtaEnergy_%d" % centBin
    h = EmcalClusterList.FindObject(name)
    hClusEMCalPhiEtaEnergy.Add(h)

# Plot eta-phi distribution
c27 = ROOT.TCanvas("c27","c27: EtaPhiEMCal",600,450)
c27.cd()
hClusEMCalPhiEta = hClusEMCalPhiEtaEnergy.Project3D("yx")
hClusEMCalPhiEta.Draw("colz")
outputFilename = os.path.join(outputDir + "hClusEMCalPhiEta" + ".pdf")
c27.SaveAs(outputFilename)

# Plot phi distribution
c28 = ROOT.TCanvas("c28","c28: PhiEMCal",600,450)
c28.cd()
hClusEMCalPhi = hClusEMCalPhiEtaEnergy.ProjectionY()
hClusEMCalPhi.Draw()
outputFilename = os.path.join(outputDir + "hClusEMCalPhi" + ".pdf")
c28.SaveAs(outputFilename)

# Plot eta distribution
c29 = ROOT.TCanvas("c29","c29: EtaEMCal",600,450)
c29.cd()
hClusEMCalEta = hClusEMCalPhiEtaEnergy.ProjectionX()
hClusEMCalEta.Draw()
outputFilename = os.path.join(outputDir + "hClusEMCalEta" + ".pdf")
c29.SaveAs(outputFilename)

# Plot energy distribution
c30 = ROOT.TCanvas("c30","c30: EEMCal",600,450)
c30.cd()
c30.SetLogy()
hClusEMCalEnergy = hClusEMCalPhiEtaEnergy.ProjectionZ()
hClusEMCalEnergy.Draw()
outputFilename = os.path.join(outputDir + "hClusEMCalEnergy" + ".pdf")
c30.SaveAs(outputFilename)

# Plot EMCal leading cluster energy
c31 = ROOT.TCanvas("c31","c31: EMC Lead Clus E",600,450)
c31.cd()
hEmcalLeadClusE = clusterTHnSparse.Projection(4)
hEmcalLeadClusE.Draw()
outputFilename = os.path.join(outputDir + "hClusEMCalLeadClusE" + ".pdf")
c31.SaveAs(outputFilename)

# Plot DCAL CLUSTERS (if isRun2) ----------------------------------------------------

# Get the DCal phi/eta/E histograms, summed over centrality
if isRun2:
  hClusDCalPhiEtaEnergy = EmcalClusterList.FindObject("fHistClusDCalPhiEtaEnergy_0")
  if nCentBins > 1:
    for centBin in range(1,nCentBins):
      name = "fHistClusDCalPhiEtaEnergy_%d" % centBin
      h = EmcalClusterList.FindObject(name)
      hClusDCalPhiEtaEnergy.Add(h)

  # Plot eta-phi distribution
  c32 = ROOT.TCanvas("c32","c32: EtaPhiDCal",600,450)
  c32.cd()
  hClusDCalPhiEta = hClusDCalPhiEtaEnergy.Project3D("yx")
  hClusDCalPhiEta.Draw("colz")
  outputFilename = os.path.join(outputDir + "hClusDCalPhiEta" + ".pdf")
  c32.SaveAs(outputFilename)

  # Plot phi distribution
  c33 = ROOT.TCanvas("c33","c33: PhiDCal",600,450)
  c33.cd()
  hClusDCalPhi = hClusDCalPhiEtaEnergy.ProjectionY()
  hClusDCalPhi.Draw()
  outputFilename = os.path.join(outputDir + "hClusDCalPhi" + ".pdf")
  c33.SaveAs(outputFilename)

  # Plot eta distribution
  c34 = ROOT.TCanvas("c34","c34: EtaDCal",600,450)
  c34.cd()
  hClusDCalEta = hClusDCalPhiEtaEnergy.ProjectionX()
  hClusDCalEta.Draw()
  outputFilename = os.path.join(outputDir + "hClusDCalEta" + ".pdf")
  c34.SaveAs(outputFilename)

  # Plot energy distribution
  c35 = ROOT.TCanvas("c35","c35: EDCal",600,450)
  c35.cd()
  c35.SetLogy()
  hClusDCalEnergy = hClusDCalPhiEtaEnergy.ProjectionZ()
  hClusDCalEnergy.Draw()
  outputFilename = os.path.join(outputDir + "hClusDCalEnergy" + ".pdf")
  c35.SaveAs(outputFilename)

  # Plot DCal leading cluster energy
  c36 = ROOT.TCanvas("c36","c36: DMC Lead Clus E",600,450)
  c36.cd()
  hDcalLeadClusE = clusterTHnSparse.Projection(5)
  hDcalLeadClusE.Draw()
  outputFilename = os.path.join(outputDir + "hClusDCalLeadClusE" + ".pdf")
  c36.SaveAs(outputFilename)

# Plot PHOS CLUSTERS (is includePhos) -----------------------------------------------

# Get the PHOS phi/eta/E histograms, summed over centrality
if includePhos:
  hClusPHOSPhiEtaEnergy = EmcalClusterList.FindObject("fHistClusPHOSPhiEtaEnergy_0")
  if nCentBins > 1:
    for centBin in range(1,nCentBins):
      name = "fHistClusPHOSPhiEtaEnergy_%d" % centBin
      h = EmcalClusterList.FindObject(name)
      hClusPHOSPhiEtaEnergy.Add(h)

  # Plot eta-phi distribution
  c37 = ROOT.TCanvas("c37","c37: EtaPhiPHOS",600,450)
  c37.cd()
  hClusPHOSPhiEta = hClusPHOSPhiEtaEnergy.Project3D("yx")
  hClusPHOSPhiEta.Draw("colz")
  outputFilename = os.path.join(outputDir + "hClusPHOSPhiEta" + ".pdf")
  c37.SaveAs(outputFilename)

  # Plot phi distribution
  c38 = ROOT.TCanvas("c38","c38: PhiPHOS",600,450)
  c38.cd()
  hClusPHOSPhi = hClusPHOSPhiEtaEnergy.ProjectionY()
  hClusPHOSPhi.Draw()
  outputFilename = os.path.join(outputDir + "hClusPHOSPhi" + ".pdf")
  c38.SaveAs(outputFilename)

  # Plot eta distribution
  c39 = ROOT.TCanvas("c39","c39: EtaPHOS",600,450)
  c39.cd()
  hClusPHOSEta = hClusPHOSPhiEtaEnergy.ProjectionX()
  hClusPHOSEta.Draw()
  outputFilename = os.path.join(outputDir + "hClusPHOSEta" + ".pdf")
  c39.SaveAs(outputFilename)

  # Plot energy distribution
  c40 = ROOT.TCanvas("c40","c40: EPHOS",600,450)
  c40.cd()
  c40.SetLogy()
  hClusPHOSEnergy = hClusPHOSPhiEtaEnergy.ProjectionZ()
  hClusPHOSEnergy.Draw()
  outputFilename = os.path.join(outputDir + "hClusPHOSEnergy" + ".pdf")
  c40.SaveAs(outputFilename)

########################################################################################################
# Cell histograms         ##############################################################################
########################################################################################################

# Plot fHistCellsAbsIdEnergy_i as TH2 (summed over centrality)
hCellsAbsIdEnergy = EmcalCellList.FindObject("fHistCellsAbsIdEnergy_0")
if nCentBins > 1:
  for centBin in range(1,nCentBins):
    name = "fHistCellsAbsIdEnergy_%d" % centBin
    h = EmcalCellList.FindObject(name)
    hCellsAbsIdEnergy.Add(h)
c15 = ROOT.TCanvas("c15","c15: CellsAbsIdEnergy",600,450)
c15.cd()
hCellsAbsIdEnergy.Draw("colz")
outputFilename = os.path.join(outputDir + "hCellAbsIdEnergy" + ".pdf")
c15.SaveAs(outputFilename)

# Plot E spectrum as TH1
hCellsEnergy = hCellsAbsIdEnergy.ProjectionY()
c16 = ROOT.TCanvas("c16","c16: CellEnergy",600,450)
c16.cd()
c16.SetLogy()
hCellsEnergy.Draw()
outputFilename = os.path.join(outputDir + "hCellEnergy" + ".pdf")
c16.SaveAs(outputFilename)

########################################################################################################
# Jet histograms          ##############################################################################
########################################################################################################

if not ispp:
  # Plot rho vs. centrality
  hChargedJetRhoVsCent = chargedJetList.FindObject("fHistRhoVsCent")
  c18 = ROOT.TCanvas("c18","c18: hChargedJetRhoVsCent",600,450)
  c18.cd()
  hChargedJetRhoVsCent.Draw("colz")
  outputFilename = os.path.join(outputDir + "hChargedJetRhoVsCent" + ".pdf")
  c18.SaveAs(outputFilename)

# Plot charged jet eta-phi
hChargedJetEtaPhi = chargedJetTHnSparse.Projection(2,1)
hChargedJetEtaPhi.SetName("ChargedJetEtaPhi")
c19 = ROOT.TCanvas("c19","c19: ChargedJetEtaPhi",600,450)
c19.cd()
c19.SetLogz()
hChargedJetEtaPhi.Draw("colz")
outputFilename = os.path.join(outputDir + "hChargedJetEtaPhi" + ".pdf")
c19.SaveAs(outputFilename)

# Plot full jet eta-phi
hFullJetEtaPhi = fullJetTHnSparse.Projection(2,1)
c20 = ROOT.TCanvas("c20","c20: FullJetEtaPhi",600,450)
c20.cd()
c20.SetLogz()
hFullJetEtaPhi.Draw("colz")
outputFilename = os.path.join(outputDir + "hFullJetEtaPhi" + ".pdf")
c20.SaveAs(outputFilename)

# Plot charged jet pT
hChargedJetPt = chargedJetTHnSparse.Projection(3)
hChargedJetPt.SetName("hChargedJetPt")
c21 = ROOT.TCanvas("c21","c21: hChargedJetPt",600,450)
c21.cd()
c21.SetLogy()
hChargedJetPt.Draw()
outputFilename = os.path.join(outputDir + "hChargedJetPt" + ".pdf")
c21.SaveAs(outputFilename)

# Plot full jet pT
hFullJetPt = fullJetTHnSparse.Projection(3)
c22 = ROOT.TCanvas("c22","c22: hFullJetPt",600,450)
c22.cd()
c22.SetLogy()
hFullJetPt.Draw()
outputFilename = os.path.join(outputDir + "hFullJetPt" + ".pdf")
c22.SaveAs(outputFilename)

# Plot full jet NEF
hFullJetNEF = fullJetTHnSparse.Projection(6)
c23 = ROOT.TCanvas("c23","c23: hFullJetNEF",600,450)
c23.cd()
hFullJetNEF.Draw()
outputFilename = os.path.join(outputDir + "hFullJetNEF" + ".pdf")
c23.SaveAs(outputFilename)

########################################################################################################
# Other histograms        ##############################################################################
########################################################################################################

# Event rejection reasons
histEventReject = TrackQAList.FindObject("fHistEventRejection")
histEventReject.GetXaxis().SetTitle("Rejection reason")
histEventReject.GetYaxis().SetTitle("N events rejected")
c17 = ROOT.TCanvas("c17","c17: NEventReject",600,450)
c17.cd()
c17.SetLogy()
histEventReject.Draw()
outputFilename = os.path.join(outputDir + "histEventReject" + ".pdf")
c17.SaveAs(outputFilename)

#---------------------------------------------------------------------------------------------------
#if __name__ == '__main__':
    # Define arguments
