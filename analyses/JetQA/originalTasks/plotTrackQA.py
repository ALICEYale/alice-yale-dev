#! /usr/bin/env python

# Macro to plot track QA histograms, from AddTaskPWG4HighPtTrackQA

# General
import os
import sys
# ROOT
import ROOT
from ROOT import gStyle
from ROOT import gPad
from ROOT import TMath

inputDir = "./MC/343/"
inputFile = os.path.join(inputDir + "AnalysisResults.root")

f = ROOT.TFile(inputFile)

# 2011 global tracks
trackType = 0
cuts = 5
globalTrackFile = f.Get("PWG4_HighPtTrackQACent10TrackType%dCuts%dkMB" % (trackType, cuts))
globalTrackList = globalTrackFile.Get("qa_histsQAtrackCent10Type%dcuts%dkMB" % (trackType, cuts))

# 2011 complementary tracks (no SPD)
trackType = 7
cuts = 5
complementaryTrackFile = f.Get("PWG4_HighPtTrackQACent10TrackType%dCuts%dkMB" % (trackType, cuts))
complementaryTrackList = complementaryTrackFile.Get("qa_histsQAtrackCent10Type%dcuts%dkMB" % (trackType, cuts))

# Plotting options
gStyle.SetOptStat(0)
gStyle.SetOptTitle(0)

# Get number of events
histNEvent = globalTrackList.FindObject("fNEventSel")
nEvents = histNEvent.GetEntries()

##########################
# Track histograms
##########################

#---------------------------------------------------------------------------------------------------
#                        phi distribution of hybrid tracks
#---------------------------------------------------------------------------------------------------

c1 = ROOT.TCanvas("c1","c1: Phi",600,450)
c1.cd()

# Get the 2d histograms Pt vs. Phi
histPtPhiGlobal = globalTrackList.FindObject("fPtPhi")
histPtPhiComplementary = complementaryTrackList.FindObject("fPtPhi")
      
histPtPhiGlobal.SetXTitle("p_{T,track} [GeV]")
histPtPhiGlobal.SetYTitle("#varphi")
histPtPhiComplementary.SetXTitle("p_{T,track} [GeV]")
histPtPhiComplementary.SetYTitle("#varphi")

# Project to 1D Phi histograms
ptmin = 2.0
ptmax = 100.0
binMin = histPtPhiGlobal.GetXaxis().FindBin(ptmin+0.00001)
binMax = histPtPhiGlobal.GetXaxis().FindBin(ptmax+0.00001)
# include the overflow bin, since we have jets > 100 GeV in the event normalization
# note that the amplitude of the histogram depends on the bin size, since they are normalized by bin width
histPhiGlobal = histPtPhiGlobal.ProjectionY("histPhiGlobal",binMin, binMax)
histPhiComplementary = histPtPhiComplementary.ProjectionY("histPhiComplementary",binMin, binMax)

histPhiGlobal.SetLineColor(2)
histPhiGlobal.SetLineWidth(3)
histPhiGlobal.SetLineStyle(1)
histPhiComplementary.SetLineStyle(1)
histPhiComplementary.SetLineColor(4)
histPhiComplementary.SetLineWidth(3)

histPhiSum = histPhiGlobal.Clone()
histPhiSum.Add(histPhiComplementary)
histPhiSum.SetTitle("histPhiSum")
histPhiSum.SetName("histPhiSum")
histPhiSum.SetLineColor(1)
histPhiSum.SetMarkerColor(1)
histPhiSum.SetLineStyle(1)

histPhiGlobal.Scale(1./nEvents,"width") # Note: "width" normalizes also by the bin width
histPhiComplementary.Scale(1./nEvents,"width")
histPhiSum.Scale(1./nEvents,"width")

histPhiGlobal.SetTitle("#phi Distribution of Hybrid Tracks")
histPhiGlobal.GetYaxis().SetTitle("#frac{1}{N_{evts}} #frac{dN}{d#varphi}")
histPhiGlobal.GetYaxis().SetTitleSize(0.06)
histPhiGlobal.GetXaxis().SetTitleSize(0.06)
histPhiGlobal.GetYaxis().SetRangeUser(0,2)
gPad.SetLeftMargin(0.15)
gPad.SetRightMargin(0.05)
gPad.SetBottomMargin(0.13)
gPad.SetTopMargin(0.05)

histPhiGlobal.Draw()
histPhiComplementary.Draw("same")
histPhiSum.Draw("same")

leg1 = ROOT.TLegend(0.17,0.7,0.83,0.93,"Hybrid tracks")
leg1.SetFillColor(10)
leg1.SetBorderSize(0)
leg1.SetFillStyle(0)
leg1.SetTextSize(0.04)
leg1.AddEntry(histPhiGlobal, "w/ SPD & ITSrefit", "l")
leg1.AddEntry(histPhiComplementary, "w/o SPD & w/ ITSrefit", "l")
leg1.AddEntry(histPhiSum, "sum", "l")
leg1.Draw("same")

textNEvents = ROOT.TLatex()
textNEvents.SetNDC()
c1.cd()
textNEvents.DrawLatex(0.52,0.63,"#it{N}_{events} = %d" % nEvents)

outputFilename = os.path.join(inputDir + histPhiGlobal.GetName() + ".pdf")
c1.SaveAs(outputFilename)

# Also plot the TH2 phi vs. pT -- make sure that phi is uniform at all pT
c2 = ROOT.TCanvas("c2","c2: phi vs pT",600,450)
c2.cd()
c2.SetLogz()
histPtPhiGlobal.Draw("colz")
outputFilename = os.path.join(inputDir + "histPtPhiGlobal" + ".pdf")
c2.SaveAs(outputFilename)

c3 = ROOT.TCanvas("c3","c3: phi vs pT",600,450)
c3.cd()
c3.SetLogz()
histPtPhiComplementary.Draw("colz")
outputFilename = os.path.join(inputDir + "histPtPhiComplementary" + ".pdf")
c3.SaveAs(outputFilename)

#---------------------------------------------------------------------------------------------------
#                        pT distribution of hybrid tracks
#---------------------------------------------------------------------------------------------------

c4 = ROOT.TCanvas("c4","c4: pT",600,450)
c4.cd();

# Project to 1D pT histograms
binMin = 1
binMax = histPtPhiGlobal.GetYaxis().FindBin(2*TMath.Pi()-0.00001)
histPtGlobal = histPtPhiGlobal.ProjectionX("histPtGlobal", binMin, binMax)
histPtComplementary = histPtPhiComplementary.ProjectionX("histPtComplementary", binMin, binMax)

histPtGlobal.SetLineColor(2)
histPtGlobal.SetLineWidth(3)
histPtGlobal.SetLineStyle(1)
histPtComplementary.SetLineStyle(1)
histPtComplementary.SetLineColor(4)
histPtComplementary.SetLineWidth(3)

histPtSum = histPtGlobal.Clone()
histPtSum.Add(histPtComplementary)
histPtSum.SetTitle("histPtSum")
histPtSum.SetName("histPtSum")
histPtSum.SetLineColor(1)
histPtSum.SetMarkerColor(1)
histPtSum.SetLineStyle(1)

histPtGlobal.Scale(1./nEvents, "width") # Note: already normalized by the bin width
histPtComplementary.Scale(1./nEvents, "width")
histPtSum.Scale(1./nEvents, "width")

histPtGlobal.SetTitle("p_T Distribution of Hybrid Tracks")
histPtGlobal.GetYaxis().SetTitle("#frac{1}{N_{evts}}#frac{dN}{dp_{T}} [GeV^{-1}]")
histPtGlobal.GetYaxis().SetTitleSize(0.06)
histPtGlobal.GetXaxis().SetTitleSize(0.06)
histPtGlobal.GetYaxis().SetRangeUser(1e-6,20)
gPad.SetLeftMargin(0.15)
gPad.SetRightMargin(0.05)
gPad.SetBottomMargin(0.14)
gPad.SetTopMargin(0.05)
gPad.SetLogy()

histPtGlobal.Draw()
histPtComplementary.Draw("same")
histPtSum.Draw("same")

leg2 = ROOT.TLegend(0.3,0.7,0.88,0.93,"Hybrid tracks")
leg2.SetFillColor(10)
leg2.SetBorderSize(0)
leg2.SetFillStyle(0)
leg2.SetTextSize(0.04)
leg2.AddEntry(histPtGlobal, "w/ SPD & ITSrefit", "l")
leg2.AddEntry(histPtComplementary, "w/o SPD & w/ ITSrefit", "l")
leg2.AddEntry(histPtSum, "sum", "l")
leg2.Draw("same")

textNEvents.SetNDC()
textNEvents.DrawLatex(0.55,0.6,"#it{N}_{events} = %d" % nEvents)

outputFilename = os.path.join(inputDir + histPtGlobal.GetName() + ".pdf")
c4.SaveAs(outputFilename)

#---------------------------------------------------------------------------------------------------
#                        pT resolution of hybrid tracks
#---------------------------------------------------------------------------------------------------

c5 = ROOT.TCanvas("c5","c5: pT resolution",600,450)
c5.cd()

# Profile of pT vs pT*sigma(1/pT), i.e. pT vs sigma(pT)/pT
# why is it done like this? sigma(1/pT) is a function in the track AliExternalTrackParam...perhaps it is because factors of (1/pT^2) show up in field theory...

# Also plot TH2 of track resolution vs. pT -- look at outliers (tracks that are reconstructed at very wrong pT)
profPtPtSigma1PtGlobal = globalTrackList.FindObject("fProfPtPtSigma1Pt")
profPtPtSigma1PtComplementary = complementaryTrackList.FindObject("fProfPtPtSigma1Pt")
    
profPtPtSigma1PtGlobal.SetName("profPtPtSigma1PtGlobal")
profPtPtSigma1PtGlobal.SetTitle("profPtPtSigma1PtGlobal")
profPtPtSigma1PtComplementary.SetName("profPtPtSigma1PtComplementary")
profPtPtSigma1PtComplementary.SetTitle("profPtPtSigma1PtComplementary")

profPtPtSigma1PtGlobal.SetLineColor(2)
profPtPtSigma1PtGlobal.SetLineWidth(3)
profPtPtSigma1PtGlobal.SetMarkerStyle(21)
profPtPtSigma1PtGlobal.SetMarkerColor(2)
profPtPtSigma1PtComplementary.SetLineColor(4)
profPtPtSigma1PtComplementary.SetLineWidth(3)
profPtPtSigma1PtComplementary.SetMarkerStyle(24)
profPtPtSigma1PtComplementary.SetMarkerColor(4)

profPtPtSigma1PtGlobal.SetMaximum(0.3)
profPtPtSigma1PtGlobal.GetYaxis().SetTitle("#it{p}_{T} #times #sigma(1/#it{p}_{T})")
#profPtPtSigma1PtGlobal.GetYaxis().SetTitle(" #sigma(1/#it{p}_{T}) / #it{p}_{T}")
profPtPtSigma1PtGlobal.GetXaxis().SetTitleSize(0.06)
profPtPtSigma1PtGlobal.GetYaxis().SetTitleSize(0.06)
gPad.SetLeftMargin(0.15)
gPad.SetRightMargin(0.05)
gPad.SetBottomMargin(0.14)
gPad.SetTopMargin(0.05)

profPtPtSigma1PtGlobal.Draw()
profPtPtSigma1PtComplementary.Draw("same")

leg3 = ROOT.TLegend(0.21,0.6,0.88,0.88,"Hybrid tracks")
leg3.SetFillColor(10)
leg3.SetBorderSize(0)
leg3.SetFillStyle(0)
leg3.SetTextSize(0.04)
leg3.AddEntry(profPtPtSigma1PtGlobal, "w/ SPD & ITSrefit", "lp")
leg3.AddEntry(profPtPtSigma1PtComplementary, "w/o SPD & w/ ITSrefit", "lp")
leg3.Draw("same")

outputFilename = os.path.join(inputDir + profPtPtSigma1PtGlobal.GetName() + ".pdf")
c5.SaveAs(outputFilename)

#---------------------------------------------------------------------------------------------------
#                        eta distribution of hybrid tracks
#---------------------------------------------------------------------------------------------------

c6 = ROOT.TCanvas("c6","c6: Eta",600,450)
c6.cd()

# Get the 2d histograms Pt vs. Eta
histPtEtaGlobal = globalTrackList.FindObject("fPtEta")
histPtEtaComplementary = complementaryTrackList.FindObject("fPtEta")

histPtEtaGlobal.SetXTitle("p_{T,track} [GeV]")
histPtEtaGlobal.SetYTitle("#eta")
histPtEtaComplementary.SetXTitle("p_{T,track} [GeV]")
histPtEtaComplementary.SetYTitle("#eta")

# Project to 1D Eta histograms
binMin = histPtEtaGlobal.GetXaxis().FindBin(ptmin+0.00001)
binMax = histPtEtaGlobal.GetXaxis().FindBin(ptmax+0.00001)
# include the overflow bin, since we have jets > 100 GeV in the event normalization
# note that the amplitude of the histogram depends on the bin size, since they are normalized by bin width
histEtaGlobal = histPtEtaGlobal.ProjectionY("histEtaGlobal",binMin, binMax)
histEtaComplementary = histPtEtaComplementary.ProjectionY("histEtaComplementary",binMin, binMax)

histEtaGlobal.SetLineColor(2)
histEtaGlobal.SetLineWidth(3)
histEtaGlobal.SetLineStyle(1)
histEtaComplementary.SetLineStyle(1)
histEtaComplementary.SetLineColor(4)
histEtaComplementary.SetLineWidth(3)

histEtaSum = histEtaGlobal.Clone()
histEtaSum.Add(histEtaComplementary)
histEtaSum.SetTitle("histEtaSum")
histEtaSum.SetName("histEtaSum")
histEtaSum.SetLineColor(1)
histEtaSum.SetMarkerColor(1)
histEtaSum.SetLineStyle(1)

histEtaGlobal.Scale(1./nEvents,"width") # Note: "width" normalizes also by the bin width
histEtaComplementary.Scale(1./nEvents,"width")
histEtaSum.Scale(1./nEvents,"width")

histEtaGlobal.SetTitle("#eta Distribution of Hybrid Tracks")
histEtaGlobal.GetYaxis().SetTitle("#frac{1}{N_{evts}} #frac{dN}{d#eta}")
histEtaGlobal.GetYaxis().SetTitleSize(0.06)
histEtaGlobal.GetXaxis().SetTitleSize(0.06)
histEtaGlobal.GetYaxis().SetRangeUser(0,5)
gPad.SetLeftMargin(0.15)
gPad.SetRightMargin(0.05)
gPad.SetBottomMargin(0.13)
gPad.SetTopMargin(0.05)

histEtaGlobal.Draw()
histEtaComplementary.Draw("same")
histEtaSum.Draw("same")

leg1 = ROOT.TLegend(0.17,0.7,0.83,0.93,"Hybrid tracks")
leg1.SetFillColor(10)
leg1.SetBorderSize(0)
leg1.SetFillStyle(0)
leg1.SetTextSize(0.04)
leg1.AddEntry(histEtaGlobal, "w/ SPD & ITSrefit", "l")
leg1.AddEntry(histEtaComplementary, "w/o SPD & w/ ITSrefit", "l")
leg1.AddEntry(histEtaSum, "sum", "l")
leg1.Draw("same")

textNEvents = ROOT.TLatex()
textNEvents.SetNDC()
textNEvents.DrawLatex(0.65,0.87,"#it{N}_{events} = %d" % nEvents)

outputFilename = os.path.join(inputDir + histEtaGlobal.GetName() + ".pdf")
c6.SaveAs(outputFilename)

# Also plot the TH2 eta vs. pT -- make sure that eta is uniform at all pT
c7 = ROOT.TCanvas("c7","c7: eta vs pT",600,450)
c7.cd()
c7.SetLogz()
histPtEtaGlobal.Draw("colz")
outputFilename = os.path.join(inputDir + "histPtEtaGlobal" + ".pdf")
c7.SaveAs(outputFilename)

c8 = ROOT.TCanvas("c8","c8: eta vs pT",600,450)
c8.cd()
c8.SetLogz()
histPtEtaComplementary.Draw("colz")
outputFilename = os.path.join(inputDir + "histPtEtaComplementary" + ".pdf")
c8.SaveAs(outputFilename)

#---------------------------------------------------------------------------------------------------
#                        DCA histograms
#---------------------------------------------------------------------------------------------------

# DCAxy vs. pT -- distance of closest approach of track in xy-plane
histPtDCA2DGlobal = globalTrackList.FindObject("fPtDCA2D")
c9 = ROOT.TCanvas("c9","c9: pT vs DCAxy",600,450)
c9.cd()
c9.SetLogz()
histPtDCA2DGlobal.Draw("colz")
outputFilename = os.path.join(inputDir + "histPtDCA2DGlobal" + ".pdf")
c9.SaveAs(outputFilename)

histPtDCA2DComplementary = complementaryTrackList.FindObject("fPtDCA2D")
c10 = ROOT.TCanvas("c10","c10: pT vs DCAxy",600,450)
c10.cd()
c10.SetLogz()
histPtDCA2DComplementary.Draw("colz")
outputFilename = os.path.join(inputDir + "histPtDCA2DComplementary" + ".pdf")
c10.SaveAs(outputFilename)

# DCAz vs. pT -- distance of closest approach of track on z-axis
histPtDCAZGlobal = globalTrackList.FindObject("fPtDCAZ")
c11 = ROOT.TCanvas("c11","c11: pT vs DCAz",600,450)
c11.cd()
c11.SetLogz()
histPtDCAZGlobal.Draw("colz")
outputFilename = os.path.join(inputDir + "histPtDCAzGlobal" + ".pdf")
c11.SaveAs(outputFilename)

histPtDCAZComplementary = complementaryTrackList.FindObject("fPtDCAZ")
c12 = ROOT.TCanvas("c12","c12: pT vs DCAz",600,450)
c12.cd()
c12.SetLogz()
histPtDCAZComplementary.Draw("colz")
outputFilename = os.path.join(inputDir + "histPtDCAzComplementary" + ".pdf")
c12.SaveAs(outputFilename)

#---------------------------------------------------------------------------------------------------
#                        TPC histograms
#---------------------------------------------------------------------------------------------------

# Number of TPC clusters vs. pT
histPtNClustersGlobal = globalTrackList.FindObject("fPtNClustersTPC")
c16 = ROOT.TCanvas("c16","c16: pT vs NClus",600,450)
c16.cd()
c16.SetLogz()
histPtNClustersGlobal.Draw("colz")
outputFilename = os.path.join(inputDir + "histPtNClustersGlobal" + ".pdf")
c16.SaveAs(outputFilename)

histPtNClustersComplementary = complementaryTrackList.FindObject("fPtNClustersTPC")
c13 = ROOT.TCanvas("c13","c13: pT vs NClus",600,450)
c13.cd()
c13.SetLogz()
histPtNClustersComplementary.Draw("colz")
outputFilename = os.path.join(inputDir + "histPtNClustersComplementary" + ".pdf")
c13.SaveAs(outputFilename)

# Number of crossed rows vs. pT
histPtNCrossedRowsGlobal = globalTrackList.FindObject("fPtNCrossedRows")
c14 = ROOT.TCanvas("c14","c14: pT vs NCrossedRows",600,450)
c14.cd()
c14.SetLogz()
histPtNCrossedRowsGlobal.Draw("colz")
outputFilename = os.path.join(inputDir + "histPtNCrossedRowsGlobal" + ".pdf")
c14.SaveAs(outputFilename)

histPtNCrossedRowsComplementary = complementaryTrackList.FindObject("fPtNCrossedRows")
c15 = ROOT.TCanvas("c15","c15: pT vs NCrossedRows",600,450)
c15.cd()
c15.SetLogz()
histPtNCrossedRowsComplementary.Draw("colz")
outputFilename = os.path.join(inputDir + "histPtNCrossedRowsComplementary" + ".pdf")
c15.SaveAs(outputFilename)

#---------------------------------------------------------------------------------------------------
#                        Other histograms
#---------------------------------------------------------------------------------------------------

# Event rejection reasons
histEventReject = globalTrackList.FindObject("fNEventReject")
histEventReject.GetXaxis().SetTitle("Rejection reason")
histEventReject.GetYaxis().SetTitle("N events rejected")
c17 = ROOT.TCanvas("c17","c17: NEventReject",600,450)
c17.cd()
c17.SetLogy()
histEventReject.Draw()
outputFilename = os.path.join(inputDir + "histEventReject" + ".pdf")
c17.SaveAs(outputFilename)


#---------------------------------------------------------------------------------------------------
#if __name__ == '__main__':
    # Define arguments
