#! /usr/bin/env python

# Macro to plot dijet imbalance analysis histograms, using AliAnalysisTaskEmcalDijetImbalance.
#
# It automatically detects what to plot, based on the content of the analysis output file:
# whether to do jet/dijet/etc plots, as well as MC vs. data, PbPb vs. pp, Phos vs. no Phos.
#
# To run:
#     python plotPWGJEQA.py -f "/my/dir/AnalysisResults.root" -o "/my/dir/outputQA/" -i ".png"
#
#     (or, run without options: defaults are "AnalysisResults.root" and "./outputQA/" and ".png")
#
# If not using standard AOD collections, you need to set the list names in the config below.
# You may need to set some of the displayed ranges on the plots.
#
# Note: It is possible you will have to change the scaling on a couple plots, to give them reasonable ranges.
#
# Note: AliAnalysisTaskEmcalDijetImbalance uses variable binning for [centrality, track pT, track pT-res, and cluster E].
#       Relevant histograms are plotted below using "width" scaling option to divide by bin width, when applicable.
#
# Note: Changing the binning in the analysis task may break some functionality here.
#
# Author: James Mulligan (james.mulligan@yale.edu)

# General
import os
import sys
import argparse
import itertools
import math
# ROOT
import ROOT

# Prevent ROOT from stealing focus when plotting
ROOT.gROOT.SetBatch(True)

def plotDijetImbalance(inputFile, outputDir, fileFormat):

  # Open input file and get relevant lists
  f = ROOT.TFile(inputFile)
  
  # Set directory for QA output
  if not outputDir.endswith("/"):
      outputDir = outputDir + "/"
  if not os.path.exists(outputDir):
    os.makedirs(outputDir)
  
  # Detect whether this is a Pt-hard production (only returns true if the histos have been scaled, with scalePtHardHistos.py)
  isPtHard = False
  for key in f.GetListOfKeys():
    if "Scaled" in key.GetName():
      isPtHard = True
  print("Is Pt-hard: %s" % isPtHard)

  # Get the output list of AliAnalysisTaskEmcalDijetImbalance
  qaTaskBaseName = "AliAnalysisTaskEmcalDijetImbalance"
  qaTaskName = determineQATaskName(qaTaskBaseName, f, isPtHard)
  print("Found qaTaskName \"{0}\"".format(qaTaskName))
  qaList = f.Get(qaTaskName)

  # If not a Pt-hard production (since it is done already), we need to set Sumw2 since we will scale and divide histograms
  if not isPtHard:
    print "Setting Sumw2 on histograms."
    for obj in qaList:
      SetSumw2(obj)

  # Get the output lists
  eventSelList = qaList.FindObject("EventCutOutput")
  fullJetList = qaList.FindObject("Jet_AKTFullR020_tracks_pT0150_caloClusters_E0300_pt_scheme")
  jetHistList = fullJetList.FindObject("JetHistograms")
  backgroundHistList = fullJetList.FindObject("BackgroundHistograms")
  dijetCandTHnSparse = fullJetList.FindObject("DijetCandObservables")
  dijetImbalanceTHnSparse = fullJetList.FindObject("DijetImbalanceObservables")
  momentumBalanceTHnSparse = fullJetList.FindObject("MomentumBalance")

  # Get number of events
  histNEvent = eventSelList.FindObject("fCutStats")
  nEvents = histNEvent.GetBinContent(10)
  print("N events: %d" % nEvents)

  # Set config: ispp, isMC, isRun2, includePhos
  if fullJetList.FindObject("fHistRhoVsCent"):
    ispp = False
  else:
    ispp = True
  print("Is pp: %s" % ispp)

  isMC = False # For now, no test for MC
  print("Is MC: %s" % isMC)

  includePhos = True # For now, no test for Phos
  print("Include Phos: %s" % includePhos)

  # Plotting options
  ROOT.gStyle.SetOptStat(0)
  ROOT.gStyle.SetOptTitle(0)

  # Plot QA
  print("Plotting Histograms...")
  if eventSelList:
    plotEventHistograms(qaList, eventSelList, outputDir, fileFormat)
  if jetHistList:
    plotJetHistograms(ispp, jetHistList, outputDir, nEvents, fileFormat)
  if backgroundHistList:
    plotBackgroundHistograms(ispp, backgroundHistList, outputDir, nEvents, fileFormat)
  if dijetCandTHnSparse:
    plotDijetCandHistograms(ispp, dijetCandTHnSparse, outputDir, nEvents, fileFormat)
  if dijetImbalanceTHnSparse:
    plotDijetImbalanceHistograms(ispp, dijetImbalanceTHnSparse, outputDir, nEvents, fileFormat)
  if momentumBalanceTHnSparse:
    plotMomentumBalanceHistograms(ispp, momentumBalanceTHnSparse, outputDir, nEvents, fileFormat)

def determineQATaskName(qaTaskBaseName, f, isPtHard):
    """ Determine the task name based on a wide variety of possible names.

    Since the task name varies depending on what input objects are included,
    we need to guess the name.

    Args:
        qaTaskBaseName (str): Base name of the QA task without any of the input object names
        f (TFile): Root file containing the QA task

    """
    # Get all task names stored in the input file
    possibleTaskNames = [key.GetName() for key in f.GetListOfKeys()]

    # Possible input object names
    tracksName = "tracks"
    mcTracksName = "mcparticles"
    cellsName = "emcalCells"
    clustersName = "caloClusters"

    # Compile into a list for easy processing
    possibleNames = [tracksName, mcTracksName, cellsName, clustersName]
    suffix = "histos"
    if isPtHard:
      suffix = "histosScaled"

    for length in range(0, len(possibleNames)+1):
        for elements in itertools.permutations(possibleNames, length):
            joined = "_".join(elements)
            testTaskName = qaTaskBaseName
            if joined:
                testTaskName += "_" + joined
            # Also Try ESD
            testTaskNameESD = testTaskName.replace("emcalCells", "EMCALCells").replace("caloClusters", "CaloClusters").replace("tracks", "Tracks").replace("mcparticles", "MCParticles")

            for taskName in [testTaskName, testTaskNameESD]:
              taskName = "{0}_{1}".format(taskName, suffix)
              if taskName in possibleTaskNames:
                  return taskName

    print("Could not determine QA task name! Please check your spelling!")
    exit(1)

########################################################################################################
# Plot event histograms   ##############################################################################
########################################################################################################
def plotEventHistograms(qaList, eventSelList, outputDir, fileFormat):
  
  # Create subdirectory for EventQA
  outputDirEventHistograms = outputDir + "Event/"
  if not os.path.exists(outputDirEventHistograms):
    os.makedirs(outputDirEventHistograms)

  # Event rejection reasons
  hEventSel = eventSelList.FindObject("fCutStats")
  outputFilename = os.path.join(outputDirEventHistograms, "hEventSel" + fileFormat)
  plotHist(hEventSel, outputFilename, "hist")

  # Centrality of selected events
  ROOT.gStyle.SetOptTitle(1)
  hCentrality = eventSelList.FindObject("Centrality_selected")
  outputFilename = os.path.join(outputDirEventHistograms, "hCentrality" + fileFormat)
  plotHist(hCentrality, outputFilename, "hist")
  ROOT.gStyle.SetOptTitle(0)

########################################################################################################
# Plot full jet histograms  (plot all single jets)  ####################################################
########################################################################################################
def plotJetHistograms(ispp, jetHistList, outputDir, nEvents, fileFormat):
  
  # Create subdirectory for Jets
  outputDirJets = outputDir + "Jets/"
  if not os.path.exists(outputDirJets):
    os.makedirs(outputDirJets)
  
  ROOT.gStyle.SetOptTitle(1)

  # Plot jet rejection reasons
  hJetRejection = jetHistList.FindObject("hJetRejectionReason")
  hJetRejection.SetTitle("Jet Rejection Reason")
  outputFilename = os.path.join(outputDirJets, "hJetRejection" + fileFormat)
  plotHist(hJetRejection, outputFilename, "colz")

  # Rho vs Centrality
  hRhoVsCent = jetHistList.FindObject("hRhoVsCent")
  hRhoVsCent.SetTitle("Rho vs. Centrality, Full Jets")
  outputFilename = os.path.join(outputDirJets, "hRhoVsCent" + fileFormat)
  plotHist(hRhoVsCent, outputFilename, "colz", False, True)

  # Plot full jet pT, background subtracted
  hCentVsPtCorr = jetHistList.FindObject("hCentVsPt")
  hJetPtCorr = hCentVsPtCorr.ProjectionX()
  hJetPtCorr.SetTitle("Full jets, background subtracted")
  hJetPtCorr.GetYaxis().SetTitle("#frac{dN}{dp_{T}} [GeV^{-1}]")
  hJetPtCorr.GetXaxis().SetRangeUser(0,150)

  fPt = ROOT.TF1("fPt", "[0]*pow(x,[1])", 50, 120)
  fPt.SetParameter(0, 1.)
  fPt.SetParameter(1, -5.)
  
  hJetPtCorr.Fit(fPt, "R")
  par0 = fPt.GetParameter(0)
  par1 = fPt.GetParameter(1)
  fitResult = "%f #times p_{T}^{%f}" % (par0, par1)

  outputFilename = os.path.join(outputDirJets, "hJetPtCorr" + fileFormat)
  plotHist(hJetPtCorr, outputFilename, "", True, False, fitResult)

  # Plot full jet pT leading particle vs. jet pT
  hJetPtLeadjetPt = jetHistList.FindObject("hPtLeadingVsPt")
  hJetPtLeadjetPt.SetTitle("Leading pT vs. Jet pT, Full Jets")
  outputFilename = os.path.join(outputDirJets, "hJetPtLeadjetPt" + fileFormat)
  plotHist(hJetPtLeadjetPt, outputFilename, "colz", "", True)

  # Plot the jet pT spectrum for different leading pT thresholds
  c = ROOT.TCanvas("c","c: hist",600,450)
  c.cd()
  c.SetLogy()
  hJetPtLeadjetPt.GetXaxis().SetRangeUser(0, 150)
  hJetPt = hJetPtLeadjetPt.ProjectionX()
  hJetPt.SetName("hJetPt0")
  hJetPt.SetTitle("Jet pT, Full jets")
  nEntries = hJetPt.GetEntries()
  hJetPt.Scale(1./nEntries)
  hJetPt.Draw("hist E")

  leg = ROOT.TLegend(0.3,0.7,0.83,0.88, "Leading particle threshold")
  leg.SetFillColor(10)
  leg.SetBorderSize(0)
  leg.SetFillStyle(0)
  leg.SetTextSize(0.04)
  leg.AddEntry(hJetPt, "0 GeV", "l")

  for leadPt in range(3, 7):
    hJetPtLeadjetPt.GetYaxis().SetRangeUser(leadPt+0.5, 150)
    hJetPtLeadjetPtThresh = hJetPtLeadjetPt.ProjectionX()
    hJetPtLeadjetPtThresh.SetName("hJetPtLeadjetPtThresh" + str(leadPt))
    hJetPtLeadjetPtThresh.SetMarkerStyle(leadPt-1)
    hJetPtLeadjetPtThresh.SetMarkerColor(leadPt-2)
    hJetPtLeadjetPtThresh.SetLineColor(leadPt-2)

    nEntries = hJetPtLeadjetPtThresh.GetEntries()
    hJetPtLeadjetPtThresh.Scale(1./nEntries)

    hJetPtLeadjetPtThresh.Draw("same")
    leg.AddEntry(hJetPtLeadjetPtThresh, str(leadPt) + " GeV", "p")

  leg.Draw("same")
  outputFilename = os.path.join(outputDirJets, "hJetPtLeadjetPtThresh" + fileFormat)
  c.SaveAs(outputFilename)
  c.Close()

  # Plot also the ratio of the leading threshold to the min bias
  ROOT.gStyle.SetOptTitle(0)
  outputFilename = os.path.join(outputDirJets, "hJetPtLeadjetPtThreshRatio" + fileFormat)
  xRangeMax = 150
  yAxisTitle = "#frac{1}{N_{evts}}#frac{dN}{dp_{T}} [GeV^{-1}]"
  legendTitle = "Full jets, background subtracted"
  legendRunLabel = "7 GeV"
  legendRefLabel = "0 GeV"
  ratioYAxisTitle = "Ratio to 0 GeV"
  h2LegendLabel = ""
  plotSpectra(hJetPtLeadjetPtThresh, hJetPt, "", 1., ispp, xRangeMax, yAxisTitle, ratioYAxisTitle, outputFilename, "", legendTitle, legendRunLabel, legendRefLabel, h2LegendLabel)
  ROOT.gStyle.SetOptTitle(1)

  # Plot full jet Area vs. jet pT
  hJetPtAjet = jetHistList.FindObject("hAreaVsPt")
  hJetPtAjet.SetTitle("Jet Area vs. Jet pT, Full Jets")
  outputFilename = os.path.join(outputDirJets, "hJetPtArea" + fileFormat)
  plotHist(hJetPtAjet, outputFilename, "colz", "", True)

  # Plot NEF vs. jet pT
  hJetPtNEF = jetHistList.FindObject("hNEFVsPt")
  hJetPtNEF.SetTitle("NEF vs. Jet pT, Full Jets")
  outputFilename = os.path.join(outputDirJets, "hJetPtNEF" + fileFormat)
  plotHist(hJetPtNEF, outputFilename, "colz", "", True)

  # Plot full jet z-leading vs. jet pT
  hJetPtZleading = jetHistList.FindObject("hZLeadingVsPt")
  hJetPtZleading.SetTitle("Z-leading vs. Jet pT, Full Jets")
  outputFilename = os.path.join(outputDirJets, "hJetPtZleading" + fileFormat)
  plotHist(hJetPtZleading, outputFilename, "colz", "", True)

  # Plot full jet Nconst vs. jet pT
  hJetPtNconst = jetHistList.FindObject("hNConstVsPt")
  hJetPtNconst.SetTitle("N constituents vs. Jet pT, Full Jets")
  outputFilename = os.path.join(outputDirJets, "hJetPtNconst" + fileFormat)
  plotHist(hJetPtNconst, outputFilename, "colz", "", True)

  ROOT.gStyle.SetOptTitle(0)

  # Plot full jet pT, background-subtracted, by centrality
  hCentVsPtCorr.GetYaxis().SetRangeUser(0, 10)
  hJetPtCorr010 = hCentVsPtCorr.ProjectionX()
  hJetPtCorr010.SetName("hJetPtCorr010")
  
  hCentVsPtCorr.GetYaxis().SetRangeUser(10, 30)
  hJetPtCorr1030 = hCentVsPtCorr.ProjectionX()
  hJetPtCorr1030.SetName("hJetPtCorr1030")
  
  hCentVsPtCorr.GetYaxis().SetRangeUser(30, 50)
  hJetPtCorr3050 = hCentVsPtCorr.ProjectionX()
  hJetPtCorr3050.SetName("hJetPtCorr3050")
  
  hCentVsPtCorr.GetYaxis().SetRangeUser(50, 90)
  hJetPtCorr5090 = hCentVsPtCorr.ProjectionX()
  hJetPtCorr5090.SetName("hJetPtCorr5090")
  
  outputFilename = os.path.join(outputDirJets, "hJetPtCorrCentral" + fileFormat)
  xRangeMax = 150
  yAxisTitle = "#propto#frac{1}{N_{evts}N_{coll}}#frac{dN}{dp_{T}} [GeV^{-1}]"
  legendTitle = "Full jets, background subtracted"
  legendRunLabel = "0-10%"
  legendRefLabel = "50-90%"
  DCalLegendLabel = "10-30%"
  PHOSLegendLabel = "30-50%"
  ratioYAxisTitle = "R_{CP}"
  
  # Scale by Ncoll, to compare different centralities
  # Values taken from https://twiki.cern.ch/twiki/bin/view/ALICE/CentralityCodeSnippets
  Ncoll010 = 1636.
  Ncoll1030 = 801.
  Ncoll3050 = 264.
  Ncoll5090 = 38.1
  Ncoll090 = 435.3
  
  hJetPtCorr010.Scale(4.) # Scale by number of events in 0-10% relative to 50-90%
  hJetPtCorr1030.Scale(Ncoll010/Ncoll1030 * 2.)
  hJetPtCorr3050.Scale(Ncoll010/Ncoll3050 * 2.)
  hJetPtCorr5090.Scale(Ncoll010/Ncoll5090)
  plotSpectra(hJetPtCorr010, hJetPtCorr5090, 0, nEvents, ispp, xRangeMax, yAxisTitle, ratioYAxisTitle, outputFilename, "", legendTitle, legendRunLabel, legendRefLabel, DCalLegendLabel)
  hCentVsPtCorr.GetYaxis().SetRangeUser(0,100)

  # Plot full jet pT spectra separately for EMCal, DCal, PHOS jets
  hPtEtaPhi = jetHistList.FindObject("hPtVsEtaVsPhi")

  #EMCal jets -- divide from DCal/PHOS by phi cut
  hPtEtaPhi.GetYaxis().SetRangeUser(0, 4.)

  hJetEMCalEtaPhi = hPtEtaPhi.Project3D("yx")
  outputFilename = os.path.join(outputDirJets, "hJetEtaPhiEMCal" + fileFormat)
  #plotHist(hJetEMCalEtaPhi, outputFilename, "colz")

  hJetEMCalPtCorr = hPtEtaPhi.Project3D("z")

  # DCal jets -- divide from EMCal by phi cut, and divide from PHOS by |eta| > 0.22 (no fiducial cut on inner eta)
  hPtEtaPhi.GetYaxis().SetRangeUser(4.,6.28)

  hDCalPtEtaPhiNeg = hPtEtaPhi.Clone()
  hDCalPtEtaPhiNeg.SetName("hDCalPtEtaPhiNeg")
  hDCalPtEtaPhiNeg.GetXaxis().SetRangeUser(-0.5, -0.22)
  hDCalPtNeg = hDCalPtEtaPhiNeg.Project3D("z o")
  hDCalEtaPhiNeg = hDCalPtEtaPhiNeg.Project3D("yx o")

  hDCalPtEtaPhiPos = hPtEtaPhi.Clone()
  hDCalPtEtaPhiPos.SetName("hDCalPtEtaPhiPos")
  hDCalPtEtaPhiPos.GetXaxis().SetRangeUser(0.22, 0.5)
  hDCalPtPos = hDCalPtEtaPhiPos.Project3D("z o")
  hDCalEtaPhiPos = hDCalPtEtaPhiPos.Project3D("yx o")

  # Add the TH3s
  hDCalEtaPhi = hDCalEtaPhiNeg.Clone()
  hDCalEtaPhi.SetName("hDCalEtaPhi")
  hDCalEtaPhi.Add(hDCalEtaPhiPos)

  hDCalPt = hDCalPtNeg.Clone()
  hDCalPt.SetName("hDCalPt")
  hDCalPt.Add(hDCalPtPos)

  # Plot TH2 for eta-phi
  outputFilename = os.path.join(outputDirJets, "hJetEtaPhiDCal" + fileFormat)
  #plotHist(hDCalEtaPhi, outputFilename, "colz")

  # Gap jets -- divide from EMCal by phi cut, and divide from PHOS by |eta| > 0.13 and DCal by |eta| < 0.22
  hGapPtEtaPhiNeg = hPtEtaPhi.Clone()
  hGapPtEtaPhiNeg.SetName("hGapPtEtaPhiNeg")
  hGapPtEtaPhiNeg.GetXaxis().SetRangeUser(-0.22, -0.13)
  hGapPtNeg = hGapPtEtaPhiNeg.Project3D("z o")
  hGapEtaPhiNeg = hGapPtEtaPhiNeg.Project3D("yx o")

  hGapPtEtaPhiPos = hPtEtaPhi.Clone()
  hGapPtEtaPhiPos.SetName("hGapPtEtaPhiPos")
  hGapPtEtaPhiPos.GetXaxis().SetRangeUser(0.13, 0.22)
  hGapPtPos = hGapPtEtaPhiPos.Project3D("z o")
  hGapEtaPhiPos = hGapPtEtaPhiPos.Project3D("yx o")
  
  # Add the TH3s
  hGapEtaPhi = hGapEtaPhiNeg.Clone()
  hGapEtaPhi.SetName("hGapEtaPhi")
  hGapEtaPhi.Add(hGapEtaPhiPos)

  hGapPt = hGapPtNeg.Clone()
  hGapPt.SetName("hGapPt")
  hGapPt.Add(hGapPtPos)

  # Plot TH2 for eta-phi
  outputFilename = os.path.join(outputDirJets, "hJetEtaPhiGap" + fileFormat)
  #plotHist(hGapEtaPhi, outputFilename, "colz")

  # PHOS jets -- divide from EMCal by phi cut, and divide from DCal by eta < 0.13 (no fiducial cut on inner eta)
  #              fiducial cut on DCal (kDCALfid) ensures that remaining region is only PHOS
  hPtEtaPhi.GetXaxis().SetRangeUser(-0.13, 0.13)
  hPHOSPt = hPtEtaPhi.Project3D("z o")
  hPHOSEtaPhi = hPtEtaPhi.Project3D("yx o")

  outputFilename = os.path.join(outputDirJets, "hJetEtaPhiPHOS" + fileFormat)
  #plotHist(hPHOSEtaPhi, outputFilename, "colz")

  # And plot the background subtracted EMCal/DCal/PHOS jet pT spectra and their ratio to the reference
  outputFilename = os.path.join(outputDirJets, "hJetPtCorrCalo" + fileFormat)
  xRangeMax = 250
  yAxisTitle = "#frac{1}{N_{evts}}#frac{dN}{dp_{T}} [GeV^{-1}]"
  legendTitle = "Full jets, background subtracted"
  legendRunLabel = "EMCal jets"
  legendRefLabel = "PHOS jets"
  ratioYAxisTitle = "Ratio to PHOS"
  h2LegendLabel = "DCal jets"
  plotSpectra(hJetEMCalPtCorr, hPHOSPt, hDCalPt, nEvents, ispp, xRangeMax, yAxisTitle, ratioYAxisTitle, outputFilename, "", legendTitle, legendRunLabel, legendRefLabel, h2LegendLabel)

########################################################################################################
# Plot background histograms                        ####################################################
########################################################################################################
def plotBackgroundHistograms(ispp, backgroundHistList, outputDir, nEvents, fileFormat):

  # Create subdirectory for Jets
  outputDirBackground = outputDir + "Background/"
  if not os.path.exists(outputDirBackground):
    os.makedirs(outputDirBackground)

  ROOT.gStyle.SetOptTitle(1)

  # Background scale factor, EMCal
  hScaleFactorEMCal = backgroundHistList.FindObject("hScaleFactorEMCal")
  hScaleFactorEMCal.SetTitle("Full jet background scale factor, EMCal")
  outputFilename = os.path.join(outputDirBackground, "hScaleFactorEMCal" + fileFormat)
  plotHist(hScaleFactorEMCal, outputFilename, "colz")

  hScaleFactorEMCalProf = hScaleFactorEMCal.ProfileX()
  hScaleFactorEMCalProf.GetYaxis().SetTitle("Scale factor")

  fScaleFactorEMCal = ROOT.TF1("fScaleFactorEMCal", "[0] + [1]*x + [2]*x*x", 0, 50)
  fScaleFactorEMCal.SetParameter(0, 1.)
  fScaleFactorEMCal.SetParameter(1, 0.1)
  fScaleFactorEMCal.SetParameter(2, 0.1)

  hScaleFactorEMCalProf.Fit(fScaleFactorEMCal, "R Q") # remove Q to print fit info
  par0 = fScaleFactorEMCal.GetParameter(0)
  par1 = fScaleFactorEMCal.GetParameter(1)
  par2 = fScaleFactorEMCal.GetParameter(2)

  outputFilename = os.path.join(outputDirBackground, "hScaleFactorEMCalProf" + fileFormat)
  fitResult = "s = %f + %f#timesC + %f#timesC^{2}" % (par0, par1, par2)
  plotHist(hScaleFactorEMCalProf, outputFilename, "colz", False, False, fitResult)

  # Background scale factor, DCal region
  cSF = ROOT.TCanvas("cSF","cSF",6000,4500)
  cSF.Divide(6,5)
  cSFprof = ROOT.TCanvas("cSFprof","cSFprof",6000,4500)
  cSFprof.Divide(6,5)

  hScaleFactorVsEta = ROOT.TH1F("hScaleFactorVsEta", "Scale Factor #eta-dependence", 28, -0.7, 0.7)

  hScaleFactorDCal = backgroundHistList.FindObject("hScaleFactorDCalRegion")
  etaStep = 0.05
  for bin in range(1, 29):
    etaMin = -0.7 + (bin-1)*etaStep;
    etaMax = etaMin + etaStep;
    
    hScaleFactorDCal.GetZaxis().SetRange(bin, bin)
    hScaleFactorDCalBin = hScaleFactorDCal.Project3D("yx")
    hScaleFactorDCalBin.SetName("hScaleFactorDCalBin" + str(bin))
    hScaleFactorDCalBin.SetTitle("Full jet background scale factor, DCal Region " + str(etaMin) + "< #eta < " + str(etaMax))
    cSF.cd(0)
    cSF.cd(bin)
    hScaleFactorDCalBin.Draw("colz")
    
    hScaleFactorDCalBinProf = hScaleFactorDCalBin.ProfileX()
    hScaleFactorDCalBinProf.GetYaxis().SetTitle("Scale factor")
    hScaleFactorDCalBinProf.SetName("hScaleFactorDCalBinProf" + str(bin))
    
    cSFprof.cd(0)
    cSFprof.cd(bin)
    
    fScaleFactorDCal = ROOT.TF1("fScaleFactorDCal", "[0] + [1]*x + [2]*x*x", 0, 50)
    fScaleFactorDCal.SetParameter(0, 1.)
    fScaleFactorDCal.SetParameter(1, 0.1)
    fScaleFactorDCal.SetParameter(2, 0.1)
  
    hScaleFactorDCalBinProf.Fit(fScaleFactorDCal, "R Q") # remove Q to print fit info
    par0 = fScaleFactorDCal.GetParameter(0)
    par1 = fScaleFactorDCal.GetParameter(1)
    par2 = fScaleFactorDCal.GetParameter(2)
    par0Err = fScaleFactorDCal.GetParError(0)
    hScaleFactorVsEta.Fill(etaMin+0.025, par0)
    hScaleFactorVsEta.SetBinError(bin, par0Err)

    fitResult = "s = %f + %f#timesC + %f#timesC^{2}" % (par0, par1, par2)

    hScaleFactorDCalBinProf.Draw()
    
    textFit = ROOT.TLatex()
    textFit.SetTextSize(0.04)
    textFit.SetNDC()
    textFit.DrawLatex(0.15,0.8,fitResult)

  outputFilename = os.path.join(outputDirBackground, "hScaleFactorDCal" + fileFormat)
  cSF.cd(0)
  cSF.SaveAs(outputFilename)
  cSF.Close()

  outputFilename = os.path.join(outputDirBackground, "hScaleFactorDCalProf" + fileFormat)
  cSF.cd(0)
  cSFprof.SaveAs(outputFilename)
  cSFprof.Close()

  outputFilename = os.path.join(outputDirBackground, "hScaleFactorVsEta" + fileFormat)
  hScaleFactorVsEta.GetXaxis().SetTitle("#eta_{particle}")
  hScaleFactorVsEta.GetYaxis().SetTitle("s(C=0)")
  plotHist(hScaleFactorVsEta, outputFilename)

  # Delta-pT, EMCal
  ROOT.gStyle.SetOptStat(1)

  hDeltaPtEMCal = backgroundHistList.FindObject("hDeltaPtEMCal")
  hDeltaPtEMCal.SetTitle("#deltap_{T}, EMCal (RC method)")
  outputFilename = os.path.join(outputDirBackground, "hDeltaPtEMCal" + fileFormat)
  plotHist(hDeltaPtEMCal, outputFilename, "hist E", True)

  # Delta-pT, DCal
  cdPt = ROOT.TCanvas("cSF","cSF",6000,4500)
  cdPt.Divide(5,4)
  cdPt.cd(0)
  hDeltaPtDCal = backgroundHistList.FindObject("hDeltaPtDCalRegion")
  for bin in range(1, 21):
    etaMin = -0.5 + (bin-1)*etaStep;
    etaMax = etaMin + etaStep;
    
    hDeltaPtDCal.GetYaxis().SetRange(bin, bin)
    hDeltaPtDCalBin = hDeltaPtDCal.ProjectionX()
    hDeltaPtDCalBin.SetName("hDeltaPtDCalBin" + str(bin))
    hDeltaPtDCalBin.SetTitle("#deltap_{T}, DCal Region " + str(etaMin) + "< #eta < " + str(etaMax) + " (RC method)")
    
    cdPt.cd(bin)
    cdPt.SetLogy()
    hDeltaPtDCalBin.Draw("hist E")

  outputFilename = os.path.join(outputDirBackground, "hDeltaPtDCal" + fileFormat)
  cdPt.SaveAs(outputFilename)
  cdPt.Close()

  ROOT.gStyle.SetOptStat(0)

########################################################################################################
# Plot dijet jet histograms (one entry per jet that comprises a dijet candidate -- defined as a    #####
###########################  a dijet with a trig jet that passes threshold, regardless of assJet)  #####
########################################################################################################
def plotDijetCandHistograms(ispp, dijetCandTHnSparse, outputDir, nEvents, fileFormat):
  
  # Create subdirectory for Dijet jets
  outputDirDijetCand = outputDir + "DijetJetCand/"
  if not os.path.exists(outputDirDijetCand):
    os.makedirs(outputDirDijetCand)
  leadHadDir = outputDirDijetCand + "LeadHadRequired/"
  if not os.path.exists(leadHadDir):
    os.makedirs(leadHadDir)

  # (Centrality, LeadingHadronRequired, trigJetPt, assJetPt, trigJetPhi, assJetPhi, trigJetEta, assJetEta)
  ROOT.gStyle.SetOptTitle(1)
  ROOT.gStyle.SetOptStat(1)

  # Study kinematic selection
  trigPtVals = [30, 40]
  assPtVals = [20, 30, 40]
  nLeadingHadron = dijetCandTHnSparse.GetAxis(1).GetNbins()
  leadingHadronLabel = ""
  for leadingHadronBin in range(1, nLeadingHadron+1):
    
    dijetCandTHnSparse.GetAxis(1).SetRange(leadingHadronBin, leadingHadronBin)
    if leadingHadronBin is 2:
      outputDirDijetCand = leadHadDir
      leadingHadronLabel = "LeadHadRequired"
    
    # Plot pT trig vs pT ass, for the minimal thresholds used in the analysis
    dijetCandTHnSparse.GetAxis(2).SetRangeUser(0, 150)
    dijetCandTHnSparse.GetAxis(3).SetRangeUser(0, 150)
    PtTrigVsPtAss = dijetCandTHnSparse.Projection(2,3)
    PtTrigVsPtAss.SetName("PtTrigVsPtAss" + leadingHadronLabel)
    outputFilename = os.path.join(outputDirDijetCand, "PtTrigVsPtAss" + fileFormat)
    plotHist(PtTrigVsPtAss, outputFilename, "colz")

    # Loop through trig/ass jet pT combinations
    for trigPt in trigPtVals:
      
      dijetCandTHnSparse.GetAxis(2).SetRangeUser(trigPt, 150)
      
      for assPt in assPtVals:

        # Only allow cases where trig pT > ass pT
        if assPt > trigPt:
          continue
          
        dijetCandTHnSparse.GetAxis(3).SetRangeUser(assPt, 150)

        # Plot pT trig vs pT ass, for each specified trig/ass jet pT
        PtTrigVsPtAssSel = dijetCandTHnSparse.Projection(2,3)
        PtTrigVsPtAssSel.SetName("PtTrigVsPtAssSel_" + str(trigPt) + "_" + str(assPt) + "_" + leadingHadronLabel)
        PtTrigVsPtAssSel.SetTitle("trigJetPt > " + str(trigPt) + ", assJetPt > " + str(assPt) + " " + leadingHadronLabel)
        outputFilename = os.path.join(outputDirDijetCand, "PtTrigVsPtAss_trig" + str(trigPt) + "_ass" + str(assPt) + fileFormat)
        plotHist(PtTrigVsPtAssSel, outputFilename, "colz")


########################################################################################################
# Plot dijet imbalance histograms (one entry per accepted dijet)  ###############################################
########################################################################################################
def plotDijetImbalanceHistograms(ispp, dijetImbalanceTHnSparse, outputDir, nEvents, fileFormat):

  # Create subdirectory for Dijets
  outputDirDijetImbalance = outputDir + "DijetImbalance/"
  if not os.path.exists(outputDirDijetImbalance):
    os.makedirs(outputDirDijetImbalance)

  ROOT.gStyle.SetOptTitle(1)
  ROOT.gStyle.SetOptStat(1)

  # (Centrality, deltaPhi, deltaEta, AJ, xJ, kTy, Ntracks trig, Ntracks ass)

  # plot deltaPhi
  hDeltaPhi = dijetImbalanceTHnSparse.Projection(1)
  hDeltaPhi.SetTitle("trigJetPt > 20 GeV, assJetPt > 20 GeV")
  outputFilename = os.path.join(outputDirDijetImbalance, "hDeltaPhi" + fileFormat)
  plotHist(hDeltaPhi, outputFilename, "hist E")

  # plot deltaEta
  hDeltaEta = dijetImbalanceTHnSparse.Projection(2)
  hDeltaEta.SetTitle("trigJetPt > 20 GeV, assJetPt > 20 GeV")
  outputFilename = os.path.join(outputDirDijetImbalance, "hDeltaEta" + fileFormat)
  plotHist(hDeltaEta, outputFilename, "hist E")

  # plot AJ
  hAJ = dijetImbalanceTHnSparse.Projection(3)
  hAJ.SetTitle("trigJetPt > 20 GeV, assJetPt > 20 GeV")
  outputFilename = os.path.join(outputDirDijetImbalance, "hAJ" + fileFormat)
  plotHist(hAJ, outputFilename, "hist E")

  # plot xJ
  hxJ = dijetImbalanceTHnSparse.Projection(4)
  hxJ.SetTitle("trigJetPt > 20 GeV, assJetPt > 20 GeV")
  outputFilename = os.path.join(outputDirDijetImbalance, "hxJ" + fileFormat)
  plotHist(hxJ, outputFilename, "hist E")

  # plot kTy
  hkTy = dijetImbalanceTHnSparse.Projection(5)
  hkTy.SetTitle("trigJetPt > 20 GeV, assJetPt > 20 GeV")
  outputFilename = os.path.join(outputDirDijetImbalance, "hkTy" + fileFormat)
  plotHist(hkTy, outputFilename, "hist E")

  # plot Ntracks trig - Ntracks ass
  hTrigTracks = dijetImbalanceTHnSparse.Projection(6)
  hAssTracks = dijetImbalanceTHnSparse.Projection(7)
  hNTracksDiff = hTrigTracks.Clone()
  hNTracksDiff.Add(hAssTracks, -1)
  hNTracksDiff.GetXaxis().SetTitle("#DeltaN_{tracks}")
  hNTracksDiff.SetTitle("trigJetPt > 20 GeV, assJetPt > 20 GeV")
  outputFilename = os.path.join(outputDirDijetImbalance, "hNTracksDiff" + fileFormat)
  plotHist(hNTracksDiff, outputFilename, "hist E")


########################################################################################################
# Plot momentum balance histograms (one entry per track, only of events with accepted dijet)  ##########
########################################################################################################
def plotMomentumBalanceHistograms(ispp, momentumBalanceTHnSparse, outputDir, nEvents, fileFormat):

  # Create subdirectory for Jets
  outputDirMomentumBalance = outputDir + "MomentumBalance/"
  if not os.path.exists(outputDirMomentumBalance):
    os.makedirs(outputDirMomentumBalance)

  # (LeadingHadronRequired, pT trig min bin, pT ass min bin, AJ, deltaPhi, pT-particle, pT-parallel)

########################################################################################################
# Plot basic histogram    ##############################################################################
########################################################################################################
def plotHist(h, outputFilename, drawOptions = "", setLogy = False, setLogz = False, fitResult = ""):
 
  h.SetLineColor(1)
  h.SetLineWidth(1)
  h.SetLineStyle(1)
 
  c = ROOT.TCanvas("c","c: hist",600,450)
  c.cd()
  if setLogy:
    c.SetLogy()
  if setLogz:
    c.SetLogz()
  h.Draw(drawOptions)
  if fitResult:
    textFit = ROOT.TLatex()
    textFit.SetTextSize(0.04)
    textFit.SetNDC()
    textFit.DrawLatex(0.15,0.8,fitResult)
  c.SaveAs(outputFilename)
  c.Close()

########################################################################################################
# Plot spectra and ratio    ############################################################################
########################################################################################################
def plotSpectra(h, h2, h3, nEvents, ispp, xRangeMax, yAxisTitle, ratioYAxisTitle, outputFilename, scalingOptions = "", legendTitle = "",hLegendLabel = "", h2LegendLabel = "", h3LegendLabel = ""):

  c = ROOT.TCanvas("c","c: pT",800,850)
  c.cd()
  pad1 = ROOT.TPad("pad1", "pad1", 0, 0.3, 1, 1.0)
  pad1.SetBottomMargin(0)
  pad1.SetLeftMargin(0.15)
  pad1.SetRightMargin(0.05)
  pad1.SetTopMargin(0.05)
  pad1.SetLogy()
  pad1.Draw()
  pad1.cd()

  h.SetLineColor(1)
  h.SetLineWidth(2)
  h.SetLineStyle(1)
  h.Scale(1./nEvents, scalingOptions)
  h.GetYaxis().SetTitle(yAxisTitle)
  h.GetYaxis().SetTitleSize(0.06)
  h.GetXaxis().SetRangeUser(0,xRangeMax)
  h.GetYaxis().SetRangeUser(2e-9,2e3)
  h.GetYaxis().SetLabelFont(43)
  h.GetYaxis().SetLabelSize(20)

  h2.SetLineColor(4)
  h2.SetLineWidth(2)
  h2.SetLineStyle(1)
  h2.Scale(1./nEvents, scalingOptions)
  
  h.Draw("hist")
  h2.Draw("hist same")
  
  if h3:
    h3.SetLineColor(2)
    h3.SetLineWidth(2)
    h3.SetLineStyle(1)
    h3.Scale(1./nEvents, scalingOptions)
    h3.Draw("hist same")

  c.cd()
  pad2 = ROOT.TPad("pad2", "pad2", 0, 0.05, 1, 0.3)
  pad2.SetTopMargin(0)
  pad2.SetBottomMargin(0.35)
  pad2.SetLeftMargin(0.15)
  pad2.SetRightMargin(0.05)
  pad2.Draw()
  pad2.cd()

  # plot ratio h/h2
  hRatio = h.Clone()
  hRatio.Divide(h2)
  
  hRatio.SetMarkerStyle(21)
  hRatio.SetMarkerColor(1)

  hRatio.GetYaxis().SetTitle(ratioYAxisTitle)
  hRatio.GetYaxis().SetTitleSize(20)
  hRatio.GetYaxis().SetTitleFont(43)
  hRatio.GetYaxis().SetTitleOffset(2.2)
  hRatio.GetYaxis().SetLabelFont(43)
  hRatio.GetYaxis().SetLabelSize(20)
  hRatio.GetYaxis().SetNdivisions(505)
  hRatio.GetYaxis().SetRangeUser(0,2.2)
  
  hRatio.GetXaxis().SetRangeUser(0,xRangeMax)
  hRatio.GetXaxis().SetTitleSize(30)
  hRatio.GetXaxis().SetTitleFont(43)
  hRatio.GetXaxis().SetTitleOffset(4.)
  hRatio.GetXaxis().SetLabelFont(43)
  hRatio.GetXaxis().SetLabelSize(20)
  
  hRatio.Draw("P E")

  # plot ratio h3/h2
  if h3:
    hRatio3 = h3.Clone()
    hRatio3.Divide(h2)
    hRatio3.SetMarkerStyle(21)
    hRatio3.SetMarkerColor(2)
    hRatio3.Draw("P E same")

  pad1.cd()

  if nEvents > 1:
    textNEvents = ROOT.TLatex()
    textNEvents.SetNDC()
    textNEvents.DrawLatex(0.55,0.6,"#it{N}_{events} = %d" % nEvents)

  leg2 = ROOT.TLegend(0.3,0.7,0.88,0.93,legendTitle)
  leg2.SetFillColor(10)
  leg2.SetBorderSize(0)
  leg2.SetFillStyle(0)
  leg2.SetTextSize(0.04)
  leg2.AddEntry(h, hLegendLabel, "l")
  if h3:
    leg2.AddEntry(h3, h3LegendLabel, "l")
  if h2:
    leg2.AddEntry(h2, h2LegendLabel, "l")
  leg2.Draw("same")
  
  c.SaveAs(outputFilename)
  c.Close()

#########################################################################################
# Function to iterate recursively through an object to set Sumw2 on all TH1/TH2/THnSparse
#########################################################################################
def SetSumw2(obj):
  if obj.InheritsFrom(ROOT.TProfile.Class()):
    pass
    #print("Sumw2 not called for TProfile %s" % obj.GetName())
  elif obj.InheritsFrom(ROOT.TH2.Class()):
    obj.Sumw2()
    #print("Sumw2 called on TH2 %s" % obj.GetName())
  elif obj.InheritsFrom(ROOT.TH1.Class()):
    obj.Sumw2()
    #print("Sumw2 called on TH1 %s" % obj.GetName())
  elif obj.InheritsFrom(ROOT.THnSparse.Class()):
    obj.Sumw2()
    #print("Sumw2 called on THnSparse %s" % obj.GetName())
  else:
    #print("Not a histogram!")
    #print obj.GetName()
    for subobj in obj:
      SetSumw2(subobj)

#---------------------------------------------------------------------------------------------------
if __name__ == '__main__':
  # Define arguments
  parser = argparse.ArgumentParser(description="Compare histograms to test the new EMCal corrections framework")
  parser.add_argument("-f", "--inputFile", action="store",
                      type=str, metavar="inputFile",
                      default="AnalysisResults.root",
                      help="Path of AnalysisResults.root file")
  parser.add_argument("-o", "--outputDir", action="store",
                      type=str, metavar="outputDir",
                      default="./outputQA/",
                      help="Output directory for QA plots to be written to")
  parser.add_argument("-i", "--imageFormat", action="store",
                      type=str, metavar="imageFormat",
                      default=".png",
                      help="Image format to save plots in, e.g. \".pdf\" or \".png\"")

  # Parse the arguments
  args = parser.parse_args()
  
  print("Configuring...")
  print("inputFile: \"{0}\"".format(args.inputFile))
  print("ouputDir: \"{0}\"".format(args.outputDir))
  print("imageFormat: \"{0}\"".format(args.imageFormat))

  # If invalid inputFile is given, exit
  if not os.path.exists(args.inputFile):
    print("File \"{0}\" does not exist! Exiting!".format(args.inputFile))
    sys.exit(0)
  
  plotDijetImbalance(inputFile = args.inputFile, outputDir = args.outputDir, fileFormat = args.imageFormat)
