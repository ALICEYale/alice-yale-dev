// runEmcalDijetImbalance.C
// Run macro for dijet imbalance analysis, using new emcal correction framework, AOD

class AliAODInputHandler;
class AliVEvent;
class AliAnalysisManager;
class AliAnalysisGrid;

void LoadMacros();

//______________________________________________________________________________
AliAnalysisManager* runEmcalDijetImbalance(
        const char   *cLocalFiles    = "files.txt",                             // set the local list file
        UInt_t        iNumFiles      = 100,                                     // number of files analyzed locally
        UInt_t        iNumEvents     = 1000,                                    // number of events to be analyzed
        const char   *cRunPeriod     = "LHC15o",                                // set the run period
        UInt_t        kPhysSel       = AliVEvent::kINT7,                        // physics selection - Set to 0 for MC productions!
        const char   *cTaskName      = "EMCalCorrections",                      // sets name of analysis manager
        const Bool_t  bDoChargedJets = kTRUE,
        const Bool_t  bDoFullJets    = kTRUE,
        const Bool_t  bDoGeometricalMatching = kFALSE,
        Int_t         iStartAnalysis = 1
        )
{
    TString sRunPeriod(cRunPeriod);
    sRunPeriod.ToLower();

    AliAnalysisTaskEmcal::BeamType iBeamType = AliAnalysisTaskEmcal::kpp;

    Bool_t bIsRun2 = kFALSE;
    if (sRunPeriod.Length() == 6 && sRunPeriod.BeginsWith("lhc15")) bIsRun2 = kTRUE;
    Bool_t bIsRun2 = kTRUE;
  
    if (sRunPeriod == "lhc10h" || sRunPeriod == "lhc11h" || sRunPeriod == "lhc15o") {
        iBeamType = AliAnalysisTaskEmcal::kAA;
    }
    else if (sRunPeriod == "lhc12g" || sRunPeriod == "lhc13b" || sRunPeriod == "lhc13c" ||
            sRunPeriod == "lhc13d" || sRunPeriod == "lhc13e" || sRunPeriod == "lhc13f") {
        iBeamType = AliAnalysisTaskEmcal::kpA;
    }

    Bool_t bSeparateEMCalDCal = bIsRun2;

    AliTrackContainer::SetDefTrackCutsPeriod(sRunPeriod);
    Printf("Default track cut period set to: %s", AliTrackContainer::GetDefTrackCutsPeriod().Data());

    TString sLocalFiles(cLocalFiles);
    if (sLocalFiles == "") {
        Printf("You need to provide the list of local files!");
        return 0;
    }
    Printf("Setting local analysis for %d files from list %s, max events = %d", iNumFiles, sLocalFiles.Data(), iNumEvents);

    LoadMacros();

    AliAnalysisManager* pMgr = new AliAnalysisManager(cTaskName);
  
    AliAODInputHandler* pAODHandler = AddAODHandler();
  
    // AliMultSelection
    // Works for both pp and PbPb for the periods that it is calibrated
    if (bIsRun2 == kTRUE && iBeamType != AliAnalysisTaskEmcal::kpp ) {
      AliMultSelectionTask * pMultSelectionTask = AddTaskMultSelection(kFALSE);
      pMultSelectionTask->SelectCollisionCandidates(AliVEvent::kAny);
    }

    // CDBconnect task
    AliTaskCDBconnect *taskCDB = AddTaskCDBconnect();
    taskCDB->SetFallBackToRaw(kTRUE);

    // EMCal corrections
    // Uncomment for debugging!
    // Mainly used for debugging the main task
    // Must be set before Initialization so that we can see what is occuring
    //AliLog::SetClassDebugLevel("AliEmcalCorrectionTask", AliLog::kDebug+2);
    //pMgr->AddClassDebug("AliEmcalCorrectionTask", AliLog::kDebug+2);
    // Mainly used for debugging loading values from yaml
    //AliLog::SetClassDebugLevel("AliEmcalCorrectionComponent", AliLog::kDebug+0);
    //pMgr->AddClassDebug("AliEmcalCorrectionComponent", AliLog::kDebug+0);
    AliEmcalCorrectionTask * correctionTask = AddTaskEmcalCorrectionTask();
    correctionTask->SelectCollisionCandidates(kPhysSel);
    if (bIsRun2) {
      Printf("Setting new MultSelection for task %s", correctionTask->GetName());
      correctionTask->SetUseNewCentralityEstimation(kTRUE);
      if (iBeamType != AliAnalysisTaskEmcal::kpp) {
        correctionTask->SetNCentBins(5);
      }
    }
    // Local configuration
    correctionTask->SetUserConfigurationFilename("userPWGJEQA_LHC15o.yaml");
    // Grid configuration
    //correctionTask->SetUserConfigurationFilename("alien:///alice/cern.ch/user/r/rehlersi/AliEmcalCorrectionConfigurationTestComparison.yaml");
    correctionTask->Initialize();

    // PHOS tender
    AliPHOSTenderTask* phosTenderTask = AddAODPHOSTender();
    phosTenderTask->SelectCollisionCandidates(kPhysSel);

    // Jet finding
    AliEmcalJet::JetAcceptanceType acceptanceType = AliEmcalJet::kEMCALfid | AliEmcalJet::kDCALfid;
    TString jetTag = "Jet";
    const AliJetContainer::EJetAlgo_t jetAlgorithm = AliJetContainer::antikt_algorithm;
    const Double_t jetRadius = 0.2;
    const AliJetContainer::EJetType_t jetType = AliJetContainer::kFullJet;
    const Double_t minTrackPt = 0.15;
    const Double_t minClusterPt = 0.30;
    Double_t kGhostArea = 0.01;
    if (iBeamType != AliAnalysisTaskEmcal::kpp) kGhostArea = 0.005;
    const AliJetContainer::ERecoScheme_t recoScheme = AliJetContainer::pt_scheme;
    const char * label = "Jet";
    const Double_t minJetPt = 0;
    const Bool_t lockTask = kTRUE;
    const Bool_t fillGhosts = kFALSE;
    
    // Background
    TString sRhoChName;
    TString sRhoFuName;
    if (iBeamType != AliAnalysisTaskEmcal::kpp) {
      sRhoChName = "Rho";
      sRhoFuName = "Rho_Scaled";
      
      AliEmcalJetTask *pKtChJetTask = AddTaskEmcalJet("usedefault", "", AliJetContainer::kt_algorithm, jetRadius, AliJetContainer::kChargedJet, 0.15, 0, kGhostArea, AliJetContainer::pt_scheme, "Jet", 0., kFALSE, kFALSE);
      pKtChJetTask->SelectCollisionCandidates(kPhysSel);
      pKtChJetTask->SetUseNewCentralityEstimation(kTRUE);
      if (iBeamType != AliAnalysisTaskEmcal::kpp) {
        pKtChJetTask->SetNCentBins(5);
      }
      
      AliAnalysisTaskRho* pRhoTask = AddTaskRhoNew("usedefault", "usedefault", sRhoChName, jetRadius);
      pRhoTask->SetExcludeLeadJets(2);
      pRhoTask->SelectCollisionCandidates(kPhysSel);
      pRhoTask->SetUseNewCentralityEstimation(kTRUE);
      if (iBeamType != AliAnalysisTaskEmcal::kpp) {
        pRhoTask->SetNCentBins(5);
      }
      
      if (bDoFullJets) {
        TString sFuncPath = "alien:///alice/cern.ch/user/s/saiola/LHC11h_ScaleFactorFunctions.root";
        TString sFuncName = "LHC11h_HadCorr20_ClustersV2";
        pRhoTask->LoadRhoFunction(sFuncPath, sFuncName);
      }
    }
    
    // Charged jet analysis
    if (bDoChargedJets) {
      AliEmcalJetTask *pChJetTask = AddTaskEmcalJet("usedefault", "", AliJetContainer::antikt_algorithm, jetRadius, AliJetContainer::kChargedJet, 0.15, 0, kGhostArea, AliJetContainer::pt_scheme, "Jet", 1., kFALSE, kFALSE);
      pChJetTask->SelectCollisionCandidates(kPhysSel);
      pChJetTask->SetUseNewCentralityEstimation(kTRUE);
      if (iBeamType != AliAnalysisTaskEmcal::kpp) {
        pChJetTask->SetNCentBins(5);
      }
    }
    
    // Full jet analysis
    if (bDoFullJets) {
      AliEmcalJetTask *pFuJetTask = AddTaskEmcalJet("usedefault", "usedefault", AliJetContainer::antikt_algorithm, jetRadius, AliJetContainer::kFullJet, 0.15, 0.30, kGhostArea, AliJetContainer::pt_scheme, "Jet", 1., kFALSE, kFALSE);
      pFuJetTask->SelectCollisionCandidates(kPhysSel);
      pFuJetTask->GetClusterContainer(0)->SetDefaultClusterEnergy(AliVCluster::kHadCorr);
      pFuJetTask->SetUseNewCentralityEstimation(kTRUE);
      if (iBeamType != AliAnalysisTaskEmcal::kpp) {
        pFuJetTask->SetNCentBins(5);
      }
      pFuJetTask->GetClusterContainer(0)->SetIncludePHOS(kTRUE);
      pFuJetTask->GetClusterContainer(0)->SetPhosMinNcells(3);
      pFuJetTask->GetClusterContainer(0)->SetPhosMinM02(0.2);
      
      if (bDoGeometricalMatching) {
        Printf("Adding geom matching jet finder");
        AliEmcalJetTask *pFuJetTaskHardCore = AddTaskEmcalJet("usedefault", "usedefault", AliJetContainer::antikt_algorithm, jetRadius, AliJetContainer::kFullJet, 1., 1., kGhostArea, AliJetContainer::pt_scheme, "JetHardCore", 1., kFALSE, kFALSE);
        pFuJetTaskHardCore->SelectCollisionCandidates(kPhysSel);
        pFuJetTaskHardCore->GetClusterContainer(0)->SetDefaultClusterEnergy(AliVCluster::kHadCorr);
        pFuJetTaskHardCore->SetUseNewCentralityEstimation(kTRUE);
        if (iBeamType != AliAnalysisTaskEmcal::kpp) {
          pFuJetTaskHardCore->SetNCentBins(5);
        }
        pFuJetTaskHardCore->GetClusterContainer(0)->SetIncludePHOS(kTRUE);
        pFuJetTaskHardCore->GetClusterContainer(0)->SetPhosMinNcells(3);
        pFuJetTaskHardCore->GetClusterContainer(0)->SetPhosMinM02(0.2);
      }
    }
 
  
  ////////////////////////////////
  // Analysis Task ///////////////
    AliAnalysisTaskEmcalDijetImbalance *dijetImbalanceTask = 0;
    const Double_t deltaPhiMin     = 2*TMath::Pi()/3;         // Minimum delta phi between di-jets
    const Bool_t doGeomMatching    = bDoGeometricalMatching;  // Set whether to enable constituent study with geometrical matching
    const Double_t minTrPtHardCore = 1.0;                     // Minimum track pT in high-threshold track container (for hard-core jets)
    const Double_t minClPtHardCore = 1.0;                     // Minimum cluster E in high-threshold cluster container (for hard-core jets)
    const Double_t jetR            = 0.2;                     // jet R (for hard-core jets)
    const Bool_t includePHOS       = kTRUE;                   // Set whether to include PHOS clusters (if enabled, must also include phos clusters in jet finder) -- this also enables default phos exotic cluster cuts
    const Double_t minTrPt         = 0.15;                    // Minimum track pT in standard track container
    const Double_t minClPt         = 0.30;                    // Minimum cluster E in standard cluster container
  
    dijetImbalanceTask = AddTaskEmcalDijetImbalance("usedefault", "usedefault", deltaPhiMin, doGeomMatching, minTrPtHardCore, minClPtHardCore, jetR, includePHOS, minTrPt, minClPt);

    dijetImbalanceTask->SelectCollisionCandidates(kPhysSel);
    dijetImbalanceTask->SetUseNewCentralityEstimation(kTRUE);
    if (iBeamType != AliAnalysisTaskEmcal::kpp) {
      dijetImbalanceTask->SetNCentBins(5);
    }
  
    // Set plotting options
    dijetImbalanceTask->SetPlotJetHistograms(kTRUE);
    dijetImbalanceTask->SetPlotDijetCandHistograms(kTRUE);
    dijetImbalanceTask->SetPlotDijetImbalanceHistograms(kTRUE);
    dijetImbalanceTask->SetComputeBackground(kTRUE);
    //dijetImbalanceTask->SetDoMomentumBalance(kTRUE);
  
    // Set di-jet pT thresholds
    dijetImbalanceTask->SetMinTrigJetPt(2);
    dijetImbalanceTask->SetMinAssJetPt(1);
  
    // Set di-jet leading hadron threshold for leading jet
    dijetImbalanceTask->SetDijetLeadingHadronPt(5);
  
  /////////////////////////////////

    if (bDoFullJets) {
      AliTrackContainer* trackCont = dijetImbalanceTask->GetTrackContainer("tracks");
      AliClusterContainer* clusCont = dijetImbalanceTask->GetClusterContainer("caloClusters");
      AliJetContainer* jetCont = dijetImbalanceTask->AddJetContainer(AliJetContainer::kFullJet, AliJetContainer::antikt_algorithm, AliJetContainer::pt_scheme, jetRadius, acceptanceType, "Jet");
      if (iBeamType != AliAnalysisTaskEmcal::kpp) {
        jetCont->SetRhoName(sRhoFuName);
        jetCont->SetPercAreaCut(0.6);
      }
      
      if (bDoGeometricalMatching) {
        Printf("Adding geom matching container");
        AliTrackContainer* trackContThresh = dijetImbalanceTask->GetTrackContainer("tracksThresh");
        AliClusterContainer* clusContThresh = dijetImbalanceTask->GetClusterContainer("caloClustersThresh");
        AliJetContainer* jetContHardCore = dijetImbalanceTask->AddJetContainer(AliJetContainer::kFullJet, AliJetContainer::antikt_algorithm, AliJetContainer::pt_scheme, jetRadius, acceptanceType, trackContThresh, clusContThresh, "JetHardCore");
        if (iBeamType != AliAnalysisTaskEmcal::kpp) {
          jetContHardCore->SetRhoName(sRhoFuName);
          jetContHardCore->SetPercAreaCut(0.6);
        }
        Printf("Added geom matching container: %s", jetContHardCore->GetName());
      }
    }
  
    if (bDoChargedJets) {
      AliTrackContainer* trackCont = dijetImbalanceTask->GetTrackContainer("tracks");
      AliClusterContainer* clusCont = dijetImbalanceTask->GetClusterContainer("caloClusters");
      AliJetContainer* jetCont = dijetImbalanceTask->AddJetContainer(AliJetContainer::kChargedJet, AliJetContainer::antikt_algorithm, AliJetContainer::pt_scheme, jetRadius, AliEmcalJet::kTPCfid, "Jet");
      if (iBeamType != AliAnalysisTaskEmcal::kpp) {
        jetCont->SetRhoName(sRhoChName);
        jetCont->SetPercAreaCut(0.6);
      }
    }
  
    ///////////////////////////////////////////////////////////////////////////////////////////////////

    TObjArray *pTopTasks = pMgr->GetTasks();
    for (Int_t i = 0; i < pTopTasks->GetEntries(); ++i) {
        AliAnalysisTaskSE *pTask = dynamic_cast<AliAnalysisTaskSE*>(pTopTasks->At(i));
        if (!pTask) continue;
        if (pTask->InheritsFrom("AliAnalysisTaskEmcal") || pTask->InheritsFrom("AliAnalysisTaskEmcalJetLight")) {
            AliAnalysisTaskEmcal *pTaskEmcal = static_cast<AliAnalysisTaskEmcal*>(pTask);
            Printf("Setting beam type %d for task %s", iBeamType, pTaskEmcal->GetName());
            pTaskEmcal->SetForceBeamType(iBeamType);
          if (bIsRun2) {
            Printf("Setting new MultSelection for task %s", pTaskEmcal->GetName());
            pTaskEmcal->SetUseNewCentralityEstimation(kTRUE);
            if (iBeamType != AliAnalysisTaskEmcal::kpp) {
              pTaskEmcal->SetNCentBins(5);
            }
          }
        }
        if (pTask->InheritsFrom("AliEmcalCorrectionTask")) {
            AliEmcalCorrectionTask * pTaskEmcalCorrection = static_cast<AliEmcalCorrectionTask*>(pTask);
            Printf("Setting beam type %d for task %s", iBeamType, pTaskEmcalCorrection->GetName());
            pTaskEmcalCorrection->SetForceBeamType(iBeamType);
        }
    }

    if (!pMgr->InitAnalysis()) return 0;
    pMgr->PrintStatus();

    pMgr->SetUseProgressBar(kTRUE, 250);

    TFile *pOutFile = new TFile("train.root","RECREATE");
    pOutFile->cd();
    pMgr->Write();
    pOutFile->Close();
    delete pOutFile;

    if (iStartAnalysis == 1) {
      // start local analysis
      TChain* pChain = 0;
      gROOT->LoadMacro("$ALICE_PHYSICS/PWG/EMCAL/macros/CreateAODChain.C");
      pChain = CreateAODChain(sLocalFiles.Data(), iNumFiles, 0, kFALSE);
      Printf("Starting Analysis...");
      pMgr->StartAnalysis("local", pChain, iNumEvents);
    }
  
    return pMgr;
}

void LoadMacros()
{
    gROOT->LoadMacro("$ALICE_ROOT/ANALYSIS/macros/train/AddAODHandler.C");
    gROOT->LoadMacro("$ALICE_PHYSICS/OADB/COMMON/MULTIPLICITY/macros/AddTaskMultSelection.C");
    gROOT->LoadMacro("$ALICE_PHYSICS/PWGPP/PilotTrain/AddTaskCDBconnect.C");
    gROOT->LoadMacro("$ALICE_PHYSICS/PWG/EMCAL/macros/AddTaskEmcalCorrectionTask.C");
    gROOT->LoadMacro("$ALICE_PHYSICS/PWGJE/EMCALJetTasks/macros/AddTaskEmcalJet.C");
    gROOT->LoadMacro("$ALICE_PHYSICS/PWGJE/EMCALJetTasks/macros/AddTaskRhoNew.C");
    gROOT->LoadMacro("$ALICE_PHYSICS/PWGGA/PHOSTasks/PHOS_PbPb/AddAODPHOSTender.C");
    gROOT->LoadMacro("$ALICE_PHYSICS/PWGJE/EMCALJetTasks/macros/AddTaskEmcalDijetImbalance.C");
}
