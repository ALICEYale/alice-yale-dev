// ---------------------
//  Running the macro
// ---------------------
// use root -b to speed up (no canvas drawn)
// first compile the canvas plotting class
// root [1] .L ./PlotGHcorrelation2.cxx++
// then compile the canvas plotting run macro
// root [2] .L GHcorr_PlotMacro.C++
// root [3] GHcorr_PlotMacro(0)
//
#include <TString.h>
#include <TFile.h>
#include <TCanvas.h>
#include <TLatex.h>
#include <TH2D.h>
#include <TH1D.h>
#include <TList.h>
#include <TSystem.h>

#include <TROOT.h>
#include <Riostream.h>


#include "PlotGHcorrelation2.h"  //include when compile

//#include <TBox.h>
//
//To Do list:
//
//-at very large eta wings there is low statistic and such large error bars.
//its not adding an advantage including these bins. They blow up the total
//error.
//
//-check systematic on fit range for flow function
//
//-add scale as a parameter to function to pass on
//
//-maybe move this macro to a class
//
//-add number of v_ns as flexible parameter. Right now its hard coded
//
//-check with porjection what the ranges are included/excluded
//
	//This macro takes the histograms from the
	//gamma and pi0 hadron correlation and analyzes them

void GHcorr_PlotMacro(Bool_t User)
{

//	gROOT->LoadMacro("./PlotGHcorrelation2.cxx++");

	cout<<"..................................."<<endl;
    cout<<"Plot_GH_Correlation loaded for "<<flush;
    if(User==0)cout<<"Eliane"<<endl;
    if(User==1)cout<<"Tyler"<<endl;


    TFile *InputFileSE;
	TFile *InputFileME;
	TString InFilePath;
	TString InFileVersionSE;
	TString InFileVersionME;
	TString InFileNameSE;
	TString InFileNameME;
	TString wagonName;

//	if(User==0)InFilePath="/Users/Eliane/Software/Files/GH_rootFiles/";
	if(User==0)InFilePath="/Users/Eliane/Software/Files/GH_rootFilesPbPb/";
	if(User==1)InFilePath="/Users/thl7/GHMacroGit/WorkingYaleDev/";

	if(User==0)
	{
		//Eliane's part
		//- - - - - - - - - - - - - - - -
		//- - - - pPb  - - - - - - - - -
		//- - - - - - - - - - - - - - - -
		//InFileVersionSE ="GH_Train1174_listSum.root";
		//InFileVersionSE ="AnalysisResults_NewMixedEvents.root";
		//InFileVersionSE ="AnalysisResults_14Jun_13d_2Triggers.root";
		//InFileVersionSE ="GH_TestTrain1190.root";
		//InFileVersionSE ="AnalysisResults_14Jun_13d_2Triggers_trackD30_kMB.root";
		//InFileVersionME =InFileVersionSE;
		//InFileVersionME="GH_Train1156.root"; //LHC13b_AOD154   MB data
		//InFileVersionME ="AnalysisResults_14Jun_13d_2Triggers.root";
		//InFileVersionME ="AnalysisResults_14Jun_13d_2Triggers_trackD100.root";
		//InFileVersionME ="AnalysisResults_14Jun_13d_2Triggers_trackD30.root";
		//InFileVersionME ="AnalysisResults_15o_GH.root";
		//InFileVersionME ="GH_TestTrain1190.root";
		//InFileVersionME ="AnalysisResults_14Jun_13d_2Triggers_trackD30_kMB.root";
		//..after Oct13
		//InFileVersionSE ="gh_Train1325.root";  //..13e - with trigger filter&finer eta binning
		//InFileVersionME ="gh_Train1324.root";  //..13c - with trigger filter&finer eta binning
		//InFileVersionSE ="Train1368_LHC13e.root"; //..13e - better eta binning, assoc. only till 0.8, included more QA
		//InFileVersionME ="Train1367_LHC13c.root"; //..13c - better eta binning, assoc. only till 0.8, included more QA

		//InFileVersionSE ="GH_Train1413_LHC13e.root"; //..13e - try different subwagons
		//InFileVersionME ="GH_Train1412_LHC13c.root"; //..13c - try different subwagons

		//wagonName="AliAnalysisTask_GH_ME_tracks_caloClusters_histos";

		//- - - - - - - - - - - - - - - -
		//- - - - PbPb  - - - - - - - - -
		//- - - - - - - - - - - - - - - -
//		InFileVersionME ="Train1644_PbPb.root";
//		InFileVersionME ="Train1648_PbPb.root";
//		InFileVersionME ="TT1652.root";
//		InFileVersionME ="TT1653.root";
//		InFileVersionME ="Train1652.root";   //all trigger Eg>2GeV for Zt and Xi
//		InFileVersionME ="Train1653.root";   //14.1.17 all trigger track cut on 2GeV or 4GeV, Eg>2GeV for Zt and Xi
//		InFileVersionME ="TT1657.root";
//		InFileVersionME ="TT1648.root";
//		InFileVersionME ="Train1657.root";  //18.Jan.17 fixed trigger & added cut on 5GeV and 10 GeV
		InFileVersionME ="Train1695.root";  //1.Feb.17 added different zvertex bins

		//..set the wagon or subwagon name
//		wagonName="AliAnalysisTask_GH_ME_tracks_caloClusters_EtaTr0_EtaCl0_histos";
//		wagonName="AliAnalysisTask_GH_ME_tracks_caloClusters__T09_C07_histos";
//		wagonName="AliAnalysisTaskMB_GH_ME_tracks_caloClusters_EtaTr0_EtaCl0_T09_C07_histos";
//		wagonName="AliAnalysisTask_GH_ME_tracks_caloClusters__T09_C07_histos";
		//wagonName="AliAnalysisTask_GH_ME_tracks_caloClusters_EtaTr0_EtaCl0_T085_C06_histos";
//		//wagonName="AliAnalysisTask_GH_ME_tracks_caloClusters_EtaTr0_EtaCl0_T085_C065_histos";
		//wagonName="AliAnalysisTask_GH_ME_tracks_caloClusters_EtaTr0_EtaCl0_T08_C06_histos";

//		wagonName="AliAnalysisTaskGA_GH_ME_tracks_caloClusters_T08_C07_histos";
		wagonName="AliAnalysisTaskGA_GH_ME_tracks_caloClusters_wMatched_histos";
//		wagonName="AliAnalysisTaskGA2GeV_GH_ME_tracks_caloClusters_T015GeV_histos";
//		wagonName="AliAnalysisTaskGA2GeV_GH_ME_tracks_caloClusters_T2GeV_histos";
		//- - - - - - - - - - - - - - - -
		//- - - -local PbPb tests - - - -
		//- - - - - - - - - - - - - - - -
		//InFileVersionME ="PbPb5Test_woUtils_kAny_AllClusters.root";
//		InFileVersionME ="PbPb5Test_woUtils_kAny_AllClustersNoEtaCut.root";
		//		InFileVersionME ="Track2GeVCut_Pt1.root";                     //kINT7+EMCGA and 2 GeV cut on tracks
//		InFileVersionSE ="Track2GeVCut_Merged.root";                          //kINT7+EMCGA and 2 GeV cut on tracks
//		InFileVersionME ="AnyTrack2GeVCut_Merged.root";                       //kAnyINT+EMCGA and 2 GeV cut on tracks
		//..if you want to use untriggered files for the input
//		InFileVersionME = "Feb1_VertexBins.root";                             //kAnyINT+EMCGA more cluster cuts and zVertex binning

//		wagonName="AliAnalysisTask_GH_ME_tracks_caloClusters_histos";

		//..for PbPb events (there are no 2 distinct run periods for trigger and MB like in pPb)
		InFileVersionSE = InFileVersionME;
	}
	if(User==1)
	{
		//Tyler's part
		InFileVersionSE ="";
		InFileVersionME =InFileVersionSE; //..if you want to use untriggered files for the input
		wagonName="";
	}

	InFileNameSE = InFilePath+InFileVersionSE;
	InFileNameME = InFilePath+InFileVersionME;
	cout<<"InfileName same event:  "<<InFileNameSE<<endl;
	cout<<"InfileName mixed event: "<<InFileNameME<<endl;
	InputFileSE = TFile::Open(InFileNameSE);
	InputFileME = TFile::Open(InFileNameME);
	cout<<"Wagon Name: "<<wagonName<<endl;

	//..Create the task and run it
    Int_t observable=0;   //.. do you want to plot 2DCorr for Ga,Zt, or Xi bins?
	Bool_t mergeME=0;     //..merge ME plots for all bins to have more statistic
	Bool_t plotAdvProj=0; //..plot advanced histograms such as projections
	PlotGHcorrelation2* task = new PlotGHcorrelation2(mergeME,plotAdvProj);//..set option
	task->SetInputFileSE(InputFileSE);
	task->SetInputFileME(InputFileME);
	task->SetWagonName(wagonName);
	task->Run(observable);

}
