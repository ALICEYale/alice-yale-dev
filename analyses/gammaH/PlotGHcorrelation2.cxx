#if defined(__CINT__)
#define _SYS_TYPES_H_
#endif
// --- ROOT system ---
#include <TFile.h>
#include <TH1F.h>
#include <TH2F.h>
#include <TF1.h>
#include <TSpectrum.h>
#include <TROOT.h>
#include <TLine.h>
#include <TCanvas.h>
#include <TStyle.h>
#include <TLegend.h>
#include <TString.h>
#include <TSystem.h>
#include <TList.h>
#include <TLatex.h>
#include <TColor.h>
#include <TGaxis.h>
#include <TStyle.h>
#include <TEnv.h>
#include <TMath.h>

#include <Riostream.h>
#include <stdio.h>
#include <fstream>
#include <iostream>

#include <PlotGHcorrelation2.h>


using std::vector;
using std::cout;
using std::endl;
using std::ofstream;
using std::flush;
using std::ios;
/// \cond CLASSIMP
ClassImp(PlotGHcorrelation2);
/// \endcond

///
/// Default constructor
///
//________________________________________________________________________
PlotGHcorrelation2::PlotGHcorrelation2():
		TObject(),
		fInputFileSE(),fInputFileME(),fWagonName(),fObservable(-1),
		fdEdP_G(),fdEdP_G_Clone(),fdEdP_G_ME(),fdEdP_G_BgSub(),
		fdEdP_ZT(),fdEdP_ZT_ME(),fdEdP_ZT_BgSub(),
		fdEdP_XI(),fdEdP_XI_ME(),fdEdP_XI_BgSub()
{
	fMergeMEplots=0;
	fPlotAdvancedSB=0;

	InitArrays();
}
///
/// constructor
///
//________________________________________________________________________
PlotGHcorrelation2::PlotGHcorrelation2(Bool_t mergeME, Bool_t plotAdvanced1):
		TObject(),
		fInputFileSE(),fInputFileME(),fWagonName(),fObservable(-1),//fMergeMEplots(),
		fdEdP_G(),fdEdP_G_Clone(),fdEdP_G_ME(),fdEdP_G_BgSub(),
		fdEdP_ZT(),fdEdP_ZT_ME(),fdEdP_ZT_BgSub(),
		fdEdP_XI(),fdEdP_XI_ME(),fdEdP_XI_BgSub()
{
	//...set parameter values
	fMergeMEplots  =mergeME;
	fPlotAdvancedSB=plotAdvanced1;

	InitArrays();
}
///
/// Initializes values of arrays
///
//________________________________________________________________________
void PlotGHcorrelation2::InitArrays()
{
	fZtStep =1.0/(7-1.0);
	fXiStep =2.5/(8-1.0);

	//..assign valued to initialized arrays
//	Double_t fArray_G_BinsValue[kGammaNBINS+1] ={5,7,9,11,14,17,22,30,60,90};
	Double_t fArray_G_BinsValue[kGammaNBINS+1] ={5,7,9,11,14};
	Double_t fArray_ZT_BinsValue[kZtNBINS+1]   ={0,fZtStep,2*fZtStep,3*fZtStep,4*fZtStep,5*fZtStep,6*fZtStep,20};
	Double_t fArray_XI_BinsValue[kXiNBINS+1]   ={-100,0,fXiStep,2*fXiStep,3*fXiStep,4*fXiStep,5*fXiStep,6*fXiStep,10};
	Double_t fDeltaPhiBinsValue[kNDeltaPhiBins]={180,125,180,24,156,24,132,24};
	//..color sceme for plots
	Color_t fColorScemeValue[6]={kSpring+10,kCyan-10,kCyan-6,kOrange+7,kOrange-3,kOrange};
	//Double_t fColorScemeValue[6]={kGreen-9,kBlue-9,kRed-9,kOrange+7,kOrange-3,kOrange};

	memcpy (fArray_G_Bins, fArray_G_BinsValue, sizeof (fArray_G_Bins));
	memcpy (fArray_ZT_Bins, fArray_ZT_BinsValue, sizeof (fArray_ZT_Bins));
	memcpy (fArray_XI_Bins, fArray_XI_BinsValue, sizeof (fArray_XI_Bins));
	memcpy (fDeltaPhiBins, fDeltaPhiBinsValue, sizeof (fDeltaPhiBins));
	memcpy (fColorSceme, fColorScemeValue, sizeof (fColorSceme));
}

///
/// Main run function
///
//________________________________________________________________________
void PlotGHcorrelation2::Run(Int_t observable)
{
	if(fInputFileSE==0 || fInputFileSE==0)cout<<"Error, input file needs to be set"<<endl;
	fObservable=observable;
	cout<<"Analysis is run for "<<flush;
	if(fObservable==0)cout<<"bins in GA"<<endl;
	if(fObservable==1)cout<<"bins in zT"<<endl;
	if(fObservable==2)cout<<"bins in xi"<<endl;

	gStyle->SetOptTitle(0);
	gStyle->SetOptStat(0);
	//gStyle->SetPalette(53);  //standard is 1
	gStyle->SetCanvasColor(10);
	TGaxis::SetMaxDigits(4);
	gStyle->SetPadTopMargin(0.05);//0.05
	gStyle->SetPadBottomMargin(0.18);//0.15
	gStyle->SetPadRightMargin(0.10);
	gStyle->SetPadLeftMargin(0.15);
	gStyle->SetFrameFillColor(10);
	gStyle->SetLabelSize(0.05,"X");
	gStyle->SetLabelSize(0.05,"Y");
	gStyle->SetTitleSize(5.0,"X");
	gStyle->SetTitleSize(5.0,"Y");
	gEnv->SetValue("Canvas.ShowEventStatus",1);  //shows the status bar in the canvas

//	SetPlotStyle();

	//..Load the histograms for the analyis
	LoadHistograms();
	cout<<"-Loaded histograms"<<endl;
	DrawSEplots();

	if(fMergeMEplots==1)
	{
		MergeMEplots();
	}
	//..Normalize g-h pairs to No of triggers
	NormalizeSEsignal();
	//..Normalize the ME so that it is 1 at its plateau
	NormalizeMEsignal();
	cout<<"-Normalized ME and SE"<<endl;
	Plot2DHistograms();
	cout<<"-Plotted raw SE"<<endl;
	//!!!to be done!!! maybe instead of overwriting the original plot one
	//!!!to be done!!! could create a clone and correct this one for acc and efficiency
	PlotCorrected2DHistograms();
	cout<<"-Plotted corrected SE correlations"<<endl;


	/*
	//..Plot the 2D raw spectra of SE and ME
	Plot2DHistograms(fRaw_Plots_2D_GammaV1,0,fdEdP_G,fdEdP_G_ME);
	Plot2DHistograms(fRaw_Plots_2D_GammaV2,1,fdEdP_G,fdEdP_G_ME);
	Plot2DHistograms(fRaw_Plots_2D_GammaV3,2,fdEdP_G,fdEdP_G_ME);
	Plot2DHistograms(fRaw_Plots_2D_ZtV1,0,fdEdP_ZT,fdEdP_ZT_ME);
	Plot2DHistograms(fRaw_Plots_2D_ZtV2,1,fdEdP_ZT,fdEdP_ZT_ME);
	Plot2DHistograms(fRaw_Plots_2D_ZtV3,2,fdEdP_ZT,fdEdP_ZT_ME);
	Plot2DHistograms(fRaw_Plots_2D_XiV1,0,fdEdP_XI,fdEdP_XI_ME);
	Plot2DHistograms(fRaw_Plots_2D_XiV2,1,fdEdP_XI,fdEdP_XI_ME);
	Plot2DHistograms(fRaw_Plots_2D_XiV3,2,fdEdP_XI,fdEdP_XI_ME);
	cout<<"-Plotted ME and SE"<<endl;

	//maybe instead of overwriting the original plot one
	//could create a clone and correct this one for acc and efficiency
	PlotCorrected2DHistograms(fCorrected_Plots_2D_Gamma,fdEdP_G,fdEdP_G_ME);
	PlotCorrected2DHistograms(fCorrected_Plots_2D_Zt,fdEdP_ZT,fdEdP_ZT_ME);
	PlotCorrected2DHistograms(fCorrected_Plots_2D_Xi,fdEdP_XI,fdEdP_XI_ME);
	cout<<"-Plotted corrected SE correlations"<<endl;
	for(Int_t i=0;i<kGammaNBINS;i++)
	{
		fdEdP_G_Clone[i] = (TH2D*)fdEdP_G[i]->Clone(Form("%s_Clone",fdEdP_G[i]->GetName()));
	}

	//-.-.-.-.-.-.Checkout the projections to the Delta Phi axis.-.-.-.-.-.-.-.-.-.
	//-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-
	if(fPlotAdvancedSB==1)
	{
		DetermineWidths(fCanCorr1DG,fdEdP_G);
		DetermineWidths(fCanCorr1DZt,fdEdP_ZT);
		DetermineWidths(fCanCorr1DXi,fdEdP_XI);
		cout<<"-Determined widths of NS peak"<<endl;

		DrawWidths(fCanvWidth,fEtaWidth_gamma,fPhiWidth_gamma);
		DrawWidths(fCanvWidth2,fEtaWidth_Zt,fPhiWidth_Zt);
		DrawWidths(fCanvWidth3,fEtaWidth_Xi,fPhiWidth_Xi);
		cout<<"-Plotted width of NS peak vs bin"<<endl;

		FitEtaSides(fdEdP_G,3.5,fCanProjG);
		FitEtaSides(fdEdP_G_Clone,2.5,fCanProjG2);
		FitEtaSides(fdEdP_ZT,3.5,fCanProjZt);
		FitEtaSides(fdEdP_XI,3.5,fCanProjXi);
		cout<<"-Fitted eta side bands"<<endl;
	}
	*/
}

///
/// load the histograms from the provided files
///
//________________________________________________________________________
void PlotGHcorrelation2::LoadHistograms()
{
	Int_t maxBins=0;
	if(fObservable==0)maxBins=kGammaNBINS;
	if(fObservable==1)maxBins=kZtNBINS;
	if(fObservable==2)maxBins=kXiNBINS;

	for(Int_t j=0; j<maxBins;j++)  //-->kGammaNBINS+1??
	{
		fdEdP_G[j]       = new TH2D*[kNvertBins+1];
		fdEdP_G_Clone[j] = new TH2D*[kNvertBins+1];
		fdEdP_G_ME[j]    = new TH2D*[kNvertBins+1];
		fdEdP_G_BgSub[j] = new TH1D*[kNvertBins+1];
	}
	TString histName="";
	cout<<"Start loading histograms: "<<flush;
	for(Int_t i=0;i<kGammaNBINS;i++)
	{
		cout<<"-"<<i<<"-"<<flush;

		for(Int_t j=0; j<kNvertBins; j++)
		{
		    cout<<". "<<flush;
			histName        = Form("fHistDEtaDPhiG%d_Id1_V%d",i,j);
			fdEdP_G[i][j]   = Get2DHistoFromFile(1,"Different_Gamma_2DHistograms",histName);
			if(!fdEdP_G[i][j])cout<<"Problem with loading :"<<histName<<endl;
			fdEdP_G[i][j]   ->Sumw2();

			histName        = Form("fHistDEtaDPhiG%d_Id0_V%d",i,j);
			fdEdP_G_ME[i][j]= Get2DHistoFromFile(0,"Different_Gamma_2DHistograms",histName);
			if(!fdEdP_G_ME[i][j])cout<<"Problem with loading :"<<histName<<endl;
			fdEdP_G_ME[i][j]->Sumw2();
		}
		fsumCorrSE[i] = (TH2D*)fdEdP_G[i][0]->Clone(Form("%s_Clone",fdEdP_G[i][0]->GetName()));
		fsumCorrSE[i]->Reset();

		fsumCorrSEalt[i] = (TH2D*)fdEdP_G[i][0]->Clone(Form("%s_CloneAlt",fdEdP_G[i][0]->GetName()));
		fsumCorrSEalt[i]->Reset();

		fsumCorrMEalt[i] = (TH2D*)fdEdP_G_ME[i][0]->Clone(Form("%s_CloneAlt",fdEdP_G_ME[i][0]->GetName()));
		fsumCorrMEalt[i]->Reset();
	}
	for(Int_t i=0;i<maxBins;i++)
	{
		for(Int_t j=0;j<kNvertBins;j++)
		{
			fsumCorrSEalt[i]->Add(fdEdP_G[i][j]);
			fsumCorrMEalt[i]->Add(fdEdP_G_ME[i][j]);
		}
	}
	cout<<endl;
    //ID1
	fGammaPt  = Get1DHistoFromFile(0,"pT_distributions_of_the_gamma","fHistNoClusPt_Id1");
	fGammahXi = Get1DHistoFromFile(0,"","fHistBinCheckXi_1");
	fGammahZt = Get1DHistoFromFile(0,"","fHistBinCheckZt_1");
	fGammaPt2 = Get1DHistoFromFile(0,"","fHistBinCheckPt_1");
	if(!fGammaPt) cout<<"Problem with loading : fGammaPt "<<endl;
	if(!fGammahXi)cout<<"Problem with loading : fGammahXi"<<endl;
	if(!fGammahZt)cout<<"Problem with loading : fGammahZt"<<endl;
	if(!fGammaPt2)cout<<"Problem with loading : fGammaPt2"<<endl;

	//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
	//..Histograms created new from the analysis information
	for(Int_t i=0;i<4;i++)
	{
		fYield_VS_Eg[i] = new TH1D(Form("Yield_VS_EGamma_Angle%0d",i),Form("Yield_VS_EGamma_Angle%0d",i), kGammaNBINS, fArray_G_Bins);
		//fYield_VS_Zt[i] = new TH1D(Form("Yield_VS_Zt_Angle%0d",i),Form("Yield_VS_Zt_Angle%0d",i), kZtNBINS, fArray_ZT_Bins);
		//fYield_VS_Xi[i] = new TH1D(Form("Yield_VS_Xi_Angle%0d",i),Form("Yield_VS_Xi_Angle%0d",i), kXiNBINS, fArray_XI_Bins);
	}

	fEtaWidth_gamma = new TH1D("EtaWidth_gamma","EtaWidth_gamma",kGammaNBINS, fArray_G_Bins);
	fPhiWidth_gamma = new TH1D("PhiWidth_gamma","PhiWidth_gamma",kGammaNBINS, fArray_G_Bins);
	/*fEtaWidth_Zt    = new TH1D("EtaWidth_Zt","EtaWidth_Zt",kZtNBINS, fArray_ZT_Bins);
	fPhiWidth_Zt    = new TH1D("PhiWidth_Zt","PhiWidth_Zt",kZtNBINS, fArray_ZT_Bins);
	fEtaWidth_Xi    = new TH1D("EtaWidth_Xi","EtaWidth_Xi",kXiNBINS, fArray_XI_Bins);
	fPhiWidth_Xi    = new TH1D("PhiWidth_Xi","PhiWidth_Xi",kXiNBINS, fArray_XI_Bins);
*/
	//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
	//..Create boxes for the histograms

	for(Int_t i=0;i<kNDeltaPhiBins/2;i++)
	{
		fBoxes[i*2]   =new TBox(fDeltaPhiBins[i*2]-fDeltaPhiBins[i*2+1],0, fDeltaPhiBins[i*2],100);

		if(fDeltaPhiBins[i*2]==180)
		{
			fBoxes[i*2+1] =new TBox(fDeltaPhiBins[i*2],0,fDeltaPhiBins[i*2]+fDeltaPhiBins[i*2+1],100);
		}
		else
		{
			Double_t Delta=180-fDeltaPhiBins[i*2];
			fBoxes[i*2+1] =new TBox(180+Delta,0,180+Delta+fDeltaPhiBins[i*2+1],100);
		}
	}

	//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
	//..Create Canvases
	for(Int_t j=0; j<maxBins; j++)
	{
		fMEPlots1DGamma[j]       = new TCanvas(Form("ME_1D_Plots_gBin%0d",j),Form("A) 1D ME E_gamma norm. Bin%0d",j),1400,1400);
		fMEPlots2DGamma[j]       = new TCanvas(Form("ME_2D_Plots_gBin%0d",j),Form("B) 2D ME E_gamma norm. Bin%0d",j),1400,1400);
		fRaw_Plots_2D_GammaV1[j] = new TCanvas(Form("Raw_2D_Plots1Bin%0d",j),Form("C) 2D Raw E_gamma V1Bin%0d",j),1400,900);
		fCorrected_Plots_2D_Gamma[j] = new TCanvas(Form("Corrected_2D_Plots%0d",j),Form("2D Correct. multiple Z-vtx for Bin %0d",j),1400,1400);
		//fMEPlots2DXi[j]    = new TCanvas(Form("ME_Plots_2D_XiBin%0d",j),Form("ME Xi norm. Bin%0d",j),1400,1400);
		//fMEPlots2DZt[j]    = new TCanvas(Form("ME_2D_Plots_ZtBin%0d",j),Form("ME Zt norm. Bin%0d",j),1400,1400);
	}
	fPlots_2D_GammaSum     = new TCanvas("Corr_2D_Plots","2D Corr E_gamma Sum",1400,900);
	fPlots_2D_GammaSum_alt = new TCanvas("Corr_2D_PlotsAlt","2D Corr E_gamma Sum (not per zVert)",1400,900);

	fCanBinCheck    = new TCanvas("Can_BinCheck","Check for the binning",800,800);
	/*fRaw_Plots_2D_ZtV1 = new TCanvas("Raw_2D_PlotsZt1","2D Raw Z_T V1",1400,900);
	fRaw_Plots_2D_ZtV2 = new TCanvas("Raw_2D_PlotsZt2","2D Raw Z_T V2",1400,900);
	fRaw_Plots_2D_ZtV3 = new TCanvas("Raw_2D_PlotsZt3","2D Raw Z_T V3",1400,900);
	fRaw_Plots_2D_XiV1 = new TCanvas("Raw_2D_PlotsXi1","2D Raw Xi V1",1400,900);
	fRaw_Plots_2D_XiV2 = new TCanvas("Raw_2D_PlotsXi2","2D Raw Xi V2",1400,900);
	fRaw_Plots_2D_XiV3 = new TCanvas("Raw_2D_PlotsXi3","2D Raw Xi V3",1400,900);
*/
/*	fCorrected_Plots_2D_Zt    = new TCanvas("Corrected_2D_Plots_Zt","2D Correct. - 7 Z_T histograms",1400,1400);
	fCorrected_Plots_2D_Xi    = new TCanvas("Corrected_2D_Plots_Xi","2D Correct. - 8 Xi histograms",1400,1400);
*/
	//..alternative to show them all together
	fSEplotsG  = new TCanvas("fSEplotsG","SE 2D Correct. - 9 E_gamma histograms",1400,1400);
//	fSEplotsZt = new TCanvas("fSEplotsZt","SE 2D Correct. - 7 Z_T histograms",1400,1400);
//	fSEplotsXi = new TCanvas("fSEplotsXi","SE 2D Correct. - 8 Xi histograms",1400,1400);
	fCanDeleteMe = new TCanvas("fCanDeleteMe","special histogram for poster",800,400);

	if(fMergeMEplots==1)
	{
		fMEplotsG  = new TCanvas("fMEplotsG","...",1400,1400);
		fMEplotsZt = new TCanvas("fMEplotsZt","...",1400,1400);
		fMEplotsXi = new TCanvas("fMEplotsXi","...",1400,1400);
	}

	if(fPlotAdvancedSB==1)
	{
		fCanCorr1DG  = new TCanvas("Corr1D_GPlots","Corrected_G 1D Plots 1) Full",1400,1400);
		//fCanCorr1DZt = new TCanvas("Corr1D_ZTPlots","Corrected_ZT 1D Plots 1)Full",1400,1400);
		//fCanCorr1DXi = new TCanvas("Corr1D_XIPlots","Corrected_XI 1D Plots 1) Full",1400,1400);

		fCanvWidth  = new TCanvas("Canv_Width","Width of NS peak vs E_g",1400,700);
		//fCanvWidth2 = new TCanvas("Canv_Width2","Width of NS peak vs Zt",1400,700);
		//fCanvWidth3 = new TCanvas("Canv_Width3","Width of NS peak vs Xi",1400,700);

		fCanProjG  = new TCanvas("Corr1D_GPlots_SB","Corrected_G 1D Plots 2) SB",1400,1400);
		fCanProjG2 = new TCanvas("Corr1D_GPlots_SB2","Corrected_G 1D Plots 2) SB",1400,1400);
		//fCanProjZt = new TCanvas("Corr1D_ZTPlots_SB","Corrected_ZT 1D Plots 2) SB",1400,1400);
		//fCanProjXi = new TCanvas("Corr1D_XIPlots_SB","Corrected_XI 1D Plots 2) SB",1400,1400);
	}
}
///
/// load the 2D histograms from input file
///
//________________________________________________________________________
TH2D* PlotGHcorrelation2::Get2DHistoFromFile(Bool_t smaMix,TString subListName,TString name)
{
	TFile* RootFile;
	if(smaMix==0)RootFile=fInputFileME; //..mixed event
	if(smaMix==1)RootFile=fInputFileSE;

	TList* IntermediatList;
	TList* FinalList;

	if(subListName=="")
	{
		FinalList    =(TList*)RootFile->Get(fWagonName);
	}
	else
	{
		IntermediatList=(TList*)RootFile       ->Get(fWagonName);
		FinalList      =(TList*)IntermediatList->FindObject(subListName);
	}
	TH2D* Histo   =(TH2D*)FinalList->FindObject(name);

	return Histo;
}
///
/// load the 1D histograms from input file
///
//________________________________________________________________________
TH1D* PlotGHcorrelation2::Get1DHistoFromFile(Bool_t smaMix,TString subListName,TString name)
{
	TFile* RootFile;
	if(smaMix==0)RootFile=fInputFileME; //..mixed event
	if(smaMix==1)RootFile=fInputFileSE;

	TList* IntermediatList;
	TList* FinalList;

	if(subListName=="")
	{
		FinalList    =(TList*)RootFile->Get(fWagonName);
	}
	else
	{
		IntermediatList=(TList*)RootFile       ->Get(fWagonName);
		FinalList      =(TList*)IntermediatList->FindObject(subListName);
	}
	TH1D* Histo   =(TH1D*)FinalList->FindObject(name);

	return Histo;
}
///
/// Funtion to set TH1 histograms to a similar style
///
//________________________________________________________________________
void PlotGHcorrelation2::SetTH1Histo(TH1 *Histo,TString Xtitel,TString Ytitel,Bool_t big)
{
	Histo->SetStats(0);
	Histo->SetTitle("");
	if(big==0)	Histo->GetYaxis()->SetTitleOffset(1.4);
	if(big==0)	Histo->GetXaxis()->SetTitleOffset(1.4);
	if(big==1)	Histo->GetYaxis()->SetTitleOffset(0.8);
	if(big==1)	Histo->GetXaxis()->SetTitleOffset(1.0);
	//if(big==1)	Histo->GetYaxis()->SetLabelOffset(0.015);
	//if(big==1)	Histo->GetXaxis()->SetLabelOffset(0.015);
	if(big==0)	Histo->GetXaxis()->SetLabelSize(0.05);
	if(big==0)  Histo->GetYaxis()->SetLabelSize(0.05);
	if(big==1)	Histo->GetXaxis()->SetLabelSize(0.07);
	if(big==1)  Histo->GetYaxis()->SetLabelSize(0.07);
	if(big==0)	Histo->GetXaxis()->SetTitleSize(0.045);
	if(big==0)	Histo->GetYaxis()->SetTitleSize(0.045);
	if(big==1)  Histo->GetXaxis()->SetTitleSize(0.08);
	if(big==1)	Histo->GetYaxis()->SetTitleSize(0.08);
	//Histo->GetXaxis()->CenterTitle();
	//Histo->GetYaxis()->CenterTitle();
	Histo->GetXaxis()->SetNdivisions(505);
	Histo->GetYaxis()->SetNdivisions(505);
	//..make nice font
	Histo->GetXaxis()->SetLabelFont(42);
	Histo->GetYaxis()->SetLabelFont(42);
	Histo->GetXaxis()->SetTitleFont(42);
	Histo->GetYaxis()->SetTitleFont(42);
	if(Xtitel!="")Histo->GetXaxis()->SetTitle(Xtitel);
	if(Ytitel!="")Histo->GetYaxis()->SetTitle(Ytitel);

	Histo->SetLineColor(1);
	Histo->SetMarkerColor(1);
	Histo->SetMarkerStyle(20);
	Histo->SetMarkerSize(0.5);
}
///
/// Funtion to set TH2 histograms to a similar style
///
//________________________________________________________________________
void PlotGHcorrelation2::SetTH2Histo(TH2 *Histo,TString Xtitel,TString Ytitel,Int_t rows)
{
	Histo->SetStats(0);
	Histo->SetTitle("");

	//..ideally for 3x2 canvas
	if(rows==2)
	{
		Histo->GetXaxis()->SetTitleOffset(1.7);
		Histo->GetYaxis()->SetTitleOffset(1.7);
		Histo->GetZaxis()->SetTitleOffset(1.1);
		Histo->GetXaxis()->SetLabelSize(0.05);
		Histo->GetYaxis()->SetLabelSize(0.05);
		//Histo->GetZaxis()->SetLabelSize(0.05);
		Histo->GetXaxis()->SetTitleSize(0.045);
		Histo->GetYaxis()->SetTitleSize(0.045);
		Histo->GetZaxis()->SetTitleSize(0.045);
	}
	//..ideally for 3x3 canvas
	if(rows==3)
	{
		Histo->GetXaxis()->SetTitleOffset(1.7);
		Histo->GetYaxis()->SetTitleOffset(1.7);
		Histo->GetZaxis()->SetTitleOffset(1.1);
		Histo->GetXaxis()->SetLabelOffset(0.01);
		Histo->GetYaxis()->SetLabelOffset(0.01);
		//Histo->GetZaxis()->SetLabelOffset(0.01);
		Histo->GetXaxis()->SetLabelSize(0.06);
		Histo->GetYaxis()->SetLabelSize(0.06);
		//Histo->GetZaxis()->SetLabelSize(0.06);
		Histo->GetXaxis()->SetTitleSize(0.055);
		Histo->GetYaxis()->SetTitleSize(0.055);
		Histo->GetZaxis()->SetTitleSize(0.055);
	}


	Histo->GetXaxis()->CenterTitle();
	Histo->GetYaxis()->CenterTitle();
	Histo->GetXaxis()->SetNdivisions(505);
	Histo->GetYaxis()->SetNdivisions(505);
	//make nice font
    Histo->GetXaxis()->SetLabelFont(42);
    Histo->GetYaxis()->SetLabelFont(42);
    Histo->GetXaxis()->SetTitleFont(42);
    Histo->GetYaxis()->SetTitleFont(42);
	if(Xtitel!="")Histo->GetXaxis()->SetTitle(Xtitel);
	if(Ytitel!="")Histo->GetYaxis()->SetTitle(Ytitel);
	Histo->SetLineColorAlpha(kBlue+2,0.095);
	Histo->SetLineWidth(0.0000);
//	Histo->GetYaxis()->SetRangeUser(-1.3,1.3);
}
///
/// Zoom the y axis into a reasonable range. minimum-border% and maximum+border%
///
//________________________________________________________________________
void PlotGHcorrelation2::ZoomYRange(TH1 *Histo,Double_t border)
{
	//Double_t min=Histo->GetBinContent(Histo->GetMinimumBin());
	Double_t min=Histo->GetMinimum(0);
	Double_t max=Histo->GetBinContent(Histo->GetMaximumBin());
	//cout<<"zoom into range: "<<min<<"-"<<max<<", "<<min*(1-border)<<"-"<<max*(1+border)<<endl;
	Histo->GetYaxis()->SetRangeUser(min*(1-border),max*(1+2*border));
}
///
/// Set a nice color sceme for plotting 2D histograms
///
//________________________________________________________________________
void PlotGHcorrelation2::SetPlotStyle()
{
	const Int_t NRGBs = 5;
	const Int_t NCont = 99;//max possible?

	//Double_t stops[NRGBs] = { 0.00, 0.34, 0.61, 0.84, 1.00 }; //log??
	//Double_t stops[NRGBs] = { 0.00, 0.18, 0.34, 0.61, 1.00 }; //log ELI test??
	//Double_t stops[NRGBs] = { 0.00, 0.25, 0.5, 0.75, 1.00 };  //linear
	Double_t stops[NRGBs] = { 0.00, 0.16, 0.39, 0.66, 1.00 };  //squeezing high and
	Double_t red[NRGBs]   = { 0.00, 0.00, 0.87, 1.00, 0.51 };
	Double_t green[NRGBs] = { 0.00, 0.81, 1.00, 0.20, 0.00 };
	Double_t blue[NRGBs]  = { 0.51, 1.00, 0.12, 0.00, 0.00 };
	TColor::CreateGradientColorTable(NRGBs, stops, red, green, blue, NCont);
	gStyle->SetNumberContours(NCont);
}
///
/// Plot a latex text into a histogram to add additonal infomation
///
//________________________________________________________________________
TLatex* PlotGHcorrelation2::PlotTopLegend(const char* label,Float_t x,Float_t y,Float_t size,Int_t color,Float_t angle)
{
	// coordinates in NDC!
	// plots the string label in position x and y in NDC coordinates
	// size is the text size
	// color is the text color

	if(x<0||y<0)
	{   // defaults
		x=gPad->GetLeftMargin()*1.15;
		y=(1-gPad->GetTopMargin())*1.04;
	}
	TLatex* text=new TLatex(x,y,label);
	text->SetTextSize(size);
	text->SetNDC();
	text->SetTextColor(color);
	text->SetTextAngle(angle);
	text->Draw();
	return text;
}
///
/// 2 Gauss functions plus a polynomial
///
//________________________________________________________________________
Double_t PlotGHcorrelation2::PolyTwoGaussFitFunc(Double_t* x_val, Double_t* par)
{
    Double_t x, y, par0, par1, par2, par3, par4, par5, par6, par7, par8, par9, par10,CommomMean;
    par0  = par[0]; //amplitude gauss 1
    par1  = par[1]; //mean gauss 1
    par2  = par[2]; //width gauss 1
    //the second gaus is smaller in amplitude and larger in width
    par3  = par[3]*par[0]; //amplitude gauss 2 (parameter3 ranges from 0-1)
    par4  = par[4]; //mean gauss 2
    par5  = par[5]*par[2]; //width gauss 2 (parameter5 is larger than 1)
    par6  = par[6]; //a
    par7  = par[7]; //b x^1
    par8  = par[8]; //c x^2
    par9  = par[9]; //d x^3
    par10 = par[10];//e x^4
    x = x_val[0];

    //Do that so that the mean of the two gaussians are the same
    CommomMean=(par1+par4)*0.5;

 //   cout<<"current p0: "<<par0<<", current p3: "<<par3<<endl;

    y = par0*(TMath::Gaus(x,CommomMean,par2,0))+par3*(TMath::Gaus(x,CommomMean,par5,0))+par6;//+(par6+par7*x+par8*x*x+par9*x*x*x+par10*x*x*x*x);
    return y;
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
TF1* PlotGHcorrelation2::allFitFuncVn(TString name,Int_t VnTerms,Double_t allPhiMin,Double_t allPhiMax)
{
	TF1* f1;
  if(VnTerms == 10)  f1 = new TF1 (name,JoelsVnFunctionValue, allPhiMin,allPhiMax, 16);
  if(VnTerms == 7)   f1 = new TF1 (name,JoelsVnFunctionValue, allPhiMin,allPhiMax, 11);
  if(VnTerms == 5)   f1 = new TF1 (name,JoelsVnFunctionValue, allPhiMin,allPhiMax, 8);
  if(VnTerms == 4)   f1 = new TF1 (name,JoelsVnFunctionValue, allPhiMin,allPhiMax, 7);
  if(VnTerms == 3)   f1 = new TF1 (name,JoelsVnFunctionValue, allPhiMin,allPhiMax, 5);

  return f1;
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
Double_t PlotGHcorrelation2::JoelsVnFunctionValue(const double * x, const double * p)
{
  Int_t VnTerms=3; //!!!!!!!!!!!!! do not hard code
  double B  = p[0];
  double v1       = p[1];
  double v2jet    = p[2];
  double v2assoc  = p[3];
  double v3 = p[4];
  double v4jet    = p[5];
  double v4assoc  = p[6];
  double v5 = p[7];
  double v6jet    = p[8];
  double v6assoc  = p[9];
  double v7 = p[10];
  double v8jet    = p[11];
  double v8assoc  = p[12];
  double v9 = p[13];
  double v10jet   = p[14];
  double v10assoc = p[15];

  //- - - - - - - - - - - - - - - - -
  //..set not needed terms to 0
  if(VnTerms<=7)
  {
	  v8jet    = 0.;
	  v8assoc  = 0.;
	  v9       = 0.;
	  v10jet   = 0.;
	  v10assoc = 0.;
  }
  if(VnTerms<=5)
  {
	  v6jet   = 0.;
	  v6assoc = 0.;
	  v7      = 0.;
  }
  if(VnTerms<=4)
  {
	  v5 = 0.;
  }
  if(VnTerms<=3)
  {
	  v4jet   = 0.;
	  v4assoc = 0.;
  }
  //- - - - - - - - - - - - - - - - -

  double result;
  double phi = x[0]*TMath::Pi()/180.0;  //transform from deg to rad


  // changed the following line, because normalizing by triggers not # of events
  //..why is there no v1?   -> 2.0*v1*TMath::Cos(1*phi);
  Float_t Part1  = 60.*TMath::Pi();
  Float_t Part2  = 2.0*v2jet*v2assoc*TMath::Cos(2.0*phi);
  Float_t Part4  = 2.0*v4jet*v4assoc*TMath::Cos(4.0*phi);
  Float_t Part6  = 2.0*v6jet*v6assoc*TMath::Cos(6.0*phi);
  Float_t Part8  = 2.0*v8jet*v8assoc*TMath::Cos(8.0*phi);
  Float_t Part10 = 2.0*v10jet*v10assoc*TMath::Cos(10.*phi);

  Float_t Part3 = 2.0*v3*TMath::Cos(3*phi);
  Float_t Part5 = 2.0*v5*TMath::Cos(5*phi);
  Float_t Part7 = 2.0*v7*TMath::Cos(7*phi);
  Float_t Part9 = 2.0*v9*TMath::Cos(9*phi);

  // final func
  //Joel original result = (B/(60.*TMath::Pi()))*Part1*(1 + Part2 + Part3 + Part4 + Part5 + Part6 + Part7 + Part8 + Part9 + Part10);
  //..Eliane adding v1 into the function
  Part1  = 2.0*v1*TMath::Cos(phi);
  result = B*(1 + Part1 + Part2 + Part3 + Part4 + Part5 + Part6 + Part7 + Part8 + Part9 + Part10);

  return result;
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
void PlotGHcorrelation2::SetJoelsParLimits(TF1 *func, TH1 *histoToFit,Double_t par_V10[])
{
  par_V10[0] = (histoToFit->GetMinimum()+histoToFit->GetMaximum());
  func->SetParameters(&par_V10[0]);
  func->SetParNames("B","v1", "v2jet", "v2assoc", "v3", "v4jet", "v4assoc", "v5", "v6jet", "v6assoc", "v7");
  func->SetParName(11, "v8jet");
  func->SetParName(12, "v8assoc");
  func->SetParName(13, "v9");
  func->SetParName(14, "v10jet");
  func->SetParName(15, "v10assoc");

  // - completely arbirtary - set whats right for you
  func->SetParLimits(0,1e-6,histoToFit->GetMaximum());
  func->SetParLimits(1,1e-6,10);
  func->SetParLimits(2,1e-6,10);
  func->SetParLimits(3,1e-6,10);
  func->SetParLimits(4,1e-6,10);
  func->SetParLimits(5,1e-6,10);
  func->SetParLimits(6,1e-6,10);
  func->SetParLimits(7,1e-4,10);
  func->SetParLimits(8,1e-4,10);
  func->SetParLimits(9,1e-6, 10);
  func->SetParLimits(10,1e-4,10);
  func->SetParLimits(11,1e-4,10);
  func->SetParLimits(12,1e-6,10);
  func->SetParLimits(13,1e-4,10);
  func->SetParLimits(14,1e-4,10);
  func->SetParLimits(15,1e-4,10);
}
//-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-..-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-
Double_t PlotGHcorrelation2::FlowFunction(Double_t* x_val, Double_t* par)
{
    Double_t x, y, par0, par1, par2, par3;
    par0  = par[0]; //Background level
    par1  = par[1]; //
    par2  = par[2]; //0-1
    par3  = par[3]; //0-1

    x = x_val[0]*TMath::Pi()/180.0;  //transform from deg to rad

    y = par0*(1+2*par1*TMath::Cos(x)+2*par1*par2*TMath::Cos(2*x)+2*par2*par3*TMath::Cos(3*x));
    return y;
}
///
/// A vertical line that can be plotted
///
//________________________________________________________________________
void PlotGHcorrelation2::PlotVerLine3(Double_t x_val,TH1* Histo, Double_t y_fac, Int_t Line_Col)
{
	Double_t min=Histo->GetMinimum(0);
	Double_t max=Histo->GetBinContent(Histo->GetMaximumBin());
	Double_t maxlocal=Histo->GetBinContent(Histo->FindBin(x_val));
    if(y_fac*max<maxlocal*1.1)max=maxlocal*1.1*(1/y_fac);

    TLine* Zero_line = new TLine();
    Zero_line -> SetX1(x_val);
    Zero_line -> SetX2(x_val);
    Zero_line -> SetY1(min);
    Zero_line -> SetY2(y_fac*max);
    //cout << "x_val = " << x_val << ", Bin = " << Histo->FindBin(x_val) << ", Y2 = " << Histo->GetBinContent(Histo->FindBin(x_val)) << endl;
    Zero_line -> SetLineWidth(2);
    Zero_line -> SetLineStyle(2);
    Zero_line -> SetLineColor(Line_Col);
    Zero_line -> Draw();
    //delete Zero_line;
}
///
/// Draw Summary of SE plots
///
//________________________________________________________________________
void PlotGHcorrelation2::DrawSEplots()
{
	fSEplotsG->Divide(3,3);
	fCanDeleteMe->Divide(2);
	for(Int_t i=0;i<kGammaNBINS;i++)
	{
		fSEplotsG->cd(i+1);
		SetTH2Histo(fdEdP_G[i][0],"","",3);
		fdEdP_G[i][0]->DrawCopy("lego2");
		DrawAliceInfoBox(fdEdP_G[i][0]);

		if(i==4)//1= 5-7GeV
		{
			fCanDeleteMe->cd(1);
			SetTH2Histo(fdEdP_G[i][0],"#Delta #varphi^{cluster-h}","#Delta #eta^{cluster-h}",3);
			fdEdP_G[i][0]->GetZaxis()->SetTitle("d#it{N}^{ cluster-h}/d#Delta#eta#Delta#varphi");
			fdEdP_G[i][0]->DrawCopy("lego2 F");
			//DrawAliceInfoBox(fdEdP_G[i]);

			TLegend *leg = new TLegend(0.60,0.60,0.9,0.85);
			leg->SetHeader("ALICE performance (27.1.2017)");
			leg->AddEntry(fdEdP_G[i][0],"Pb-Pb #sqrt{#it{s}_{NN}} = 5.02 TeV","pe");
			leg->AddEntry(fdEdP_G[i][0],"#it{p}_{T}^{h}      > 0.15 GeV/#it{c}","");
			leg->AddEntry(fdEdP_G[i][0],"11 <#it{E}_{T}^{cluster}< 14 GeV/#it{c}","");
			leg->SetTextSize(0.05);
			leg->SetBorderSize(0);
			leg->SetFillColorAlpha(10, 0);

			leg->Draw("same");

		}
		if(i==(kGammaNBINS-1))
		{
			fCanDeleteMe->cd(2);
			SetTH2Histo(fdEdP_G[i][0],"#Delta #varphi^{cluster-h}","#Delta #eta^{cluster-h}",3);
			fdEdP_G[i][0]->GetZaxis()->SetTitle("d#it{N}^{ cluster-h}/d#Delta#eta#Delta#varphi");
			fdEdP_G[i][0]->DrawCopy("lego2 F");
			//DrawAliceInfoBox(fdEdP_G[i]);

			TLegend *leg = new TLegend(0.60,0.60,0.9,0.85);
			leg->SetHeader("ALICE performance (27.1.2017)");
			leg->AddEntry(fdEdP_G[i][0],"Pb-Pb #sqrt{#it{s}_{NN}} = 5.02 TeV","pe");
			leg->AddEntry(fdEdP_G[i][0],"#it{p}_{T}^{h}      > 0.15 GeV/#it{c}","");
			leg->AddEntry(fdEdP_G[i][0],"30 <#it{E}_{T}^{cluster}< 60 GeV/#it{c}","");
			leg->SetTextSize(0.05);
			leg->SetBorderSize(0);
			leg->SetFillColorAlpha(10, 0);

			leg->Draw("same");

		}
	}
/*	fSEplotsZt->Divide(3,3);
	for(Int_t i=0;i<kZtNBINS;i++)
	{
		fSEplotsZt->cd(i+1);
		SetTH2Histo(fdEdP_ZT[i][0],"","",3);
		fdEdP_ZT[i][0]->DrawCopy("lego2");
	}
	fSEplotsXi->Divide(3,3);
	for(Int_t i=0;i<kXiNBINS;i++)
	{
		fSEplotsXi->cd(i+1);
		SetTH2Histo(fdEdP_XI[i][0],"","",3);
		fdEdP_XI[i][0]->DrawCopy("lego2");
	}*/
}
/// In the case of poor statistic - for the moment - merge different ME histograms
/// for different bins
/// !!!!!! This needs more work
///
//________________________________________________________________________
void PlotGHcorrelation2::MergeMEplots()
{
/*  Int_t maxCanvasPad=0;
	TString histoName=rawHistoSE[0]->GetName();
	if(histoName.Contains("DEtaDPhiG")) {maxCanvasPad = kGammaNBINS;}
	if(histoName.Contains("DEtaDPhiZT")){maxCanvasPad = kZtNBINS;	}
	if(histoName.Contains("DEtaDPhiXI")){maxCanvasPad = kXiNBINS;	}
*/

	//add all the histograms to hiso No 0
	fMEplotsG->Divide(3,3);
	for(Int_t i=1;i<kGammaNBINS;i++)
	{
		fMEplotsG->cd(i+1);
		SetTH2Histo(fdEdP_G_ME[i][0],"","");
		fdEdP_G_ME[i][0]->DrawCopy("colz");

		fdEdP_G_ME[0][0]->Add(fdEdP_G_ME[i][0]);
		fdEdP_G_ME[i][0]->Reset();
	}
	fMEplotsZt->Divide(3,3);
	for(Int_t i=1;i<kZtNBINS;i++)
	{
		fMEplotsZt->cd(i+1);
		SetTH2Histo(fdEdP_ZT_ME[i][0],"","");
		fdEdP_ZT_ME[i][0]->DrawCopy("colz");

		fdEdP_ZT_ME[0][0]->Add(fdEdP_ZT_ME[i][0]);
		fdEdP_ZT_ME[i][0]->Reset();
	}
	fMEplotsXi->Divide(3,3);
	for(Int_t i=1;i<kXiNBINS;i++)
	{
		fMEplotsXi->cd(i+1);
		SetTH2Histo(fdEdP_XI_ME[i][0],"","");
		fdEdP_XI_ME[i][0]->DrawCopy("colz");

		fdEdP_XI_ME[0][0]->Add(fdEdP_XI_ME[i][0]);
		fdEdP_XI_ME[i][0]->Reset();
	}

	//..
	for(Int_t i=1;i<kGammaNBINS;i++)
	{
		fdEdP_G_ME[i][0]->Add(fdEdP_G_ME[0][0]);
	}
	for(Int_t i=1;i<kZtNBINS;i++)
	{
		fdEdP_ZT_ME[i][0]->Add(fdEdP_ZT_ME[0][0]);
	}
	for(Int_t i=1;i<kXiNBINS;i++)
	{
		fdEdP_XI_ME[i][0]->Add(fdEdP_XI_ME[0][0]);
	}

}
///
/// Determine the normalization for the same event histograms
/// see how many triggers were in the collected statistic
/// to determina a normalization factor
///
//________________________________________________________________________
void PlotGHcorrelation2::NormalizeSEsignal()
{
	//..so far they are only plotted
	fCanBinCheck->Divide(2,2);
	fCanBinCheck->cd(1)->SetLogy();
	SetTH1Histo(fGammaPt,"#it{E}^{ cluster}_{T} (GeV)","arb. counts");
	fGammaPt->DrawCopy("E");
	DrawAliceInfoBox(fGammaPt);
	fCanBinCheck->cd(2)->SetLogy();
	SetTH1Histo(fGammaPt2,"#it{E}^{ cluster}_{T} (GeV)","arb. counts");
	fGammaPt2->DrawCopy("E");
	DrawAliceInfoBox(fGammaPt2);
	fCanBinCheck->cd(3)->SetLogy();
	SetTH1Histo(fGammahZt,"#it{p}_{T}^{ h}/#it{E}^{ cluster}_{T}","arb. counts");
	fGammahZt->DrawCopy("E");
	DrawAliceInfoBox(fGammahZt);
	fCanBinCheck->cd(4)->SetLogy();
	SetTH1Histo(fGammahXi,"log(#it{E}^{ cluster}_{T}/#it{p}_{T}^{ h})","arb. counts");
	fGammahXi->DrawCopy("E");
	DrawAliceInfoBox(fGammahXi);


	//..missing here is integrating and counting
	//..and then scaling the according histograms
	//..by the number of triggers
}
//________________________________________________________________________
void PlotGHcorrelation2::DrawAliceInfoBox(TObject* Histo)
{
//	\newcommand{\PbPb}{\ensuremath{\mbox{Pb--Pb}} }
//	\newcommand{\sqrtSnn}{\ensuremath{\sqrt{s_{\mathrm{NN}}}}}

	TLegend *leg = new TLegend(0.60,0.60,0.9,0.85);
	leg->SetHeader("ALICE performance (27.1.2017)");
	leg->AddEntry(Histo,"Pb-Pb #sqrt{#it{s}_{NN}} = 5.02 TeV","pe");
	leg->AddEntry(Histo,"#it{E}_{T}^{ cluster}> 10 GeV/#it{c}","");
	leg->AddEntry(Histo,"#it{p}_{T}^{ h}      > 0.15 GeV/#it{c}","");
	leg->SetTextSize(0.05);
	leg->SetBorderSize(0);
	leg->SetFillColorAlpha(10, 0);

	leg->Draw("same");
}
///
///
//________________________________________________________________________
void PlotGHcorrelation2::NormalizeMEsignal()
{
	Int_t maxBins=0;
	if(fObservable==0)maxBins=kGammaNBINS;
	if(fObservable==1)maxBins=kZtNBINS;
	if(fObservable==2)maxBins=kXiNBINS;

	for(Int_t i=0;i<maxBins;i++)
	{
		fMEPlots2DGamma[i]->Divide(5,4);
		fMEPlots1DGamma[i]->Divide(5,4);

		for(Int_t j=0;j<kNvertBins;j++)
		{
			//..project the 2D distribution in a narrow eta window (2nd and 3rd argument)
			//..and scaled the plateau to one
			ScaleMEbackground(fdEdP_G_ME[i][j],-0.15,+0.15,fMEPlots1DGamma[i],j);
			//ScaleMEbackground2(dEdP_G_ME[i],-30,+30,ME2_Plots_2D_Gamma,i);

			//..Now plot the scaled 2D distribution
			fMEPlots2DGamma[i]->cd(j+1);
			SetTH2Histo(fdEdP_G_ME[i][j],"","");
			fdEdP_G_ME[i][j]->DrawCopy("lego2");
		}
	}
}
///
///
//________________________________________________________________________
void PlotGHcorrelation2::ScaleMEbackground(TH2D* Histo,Double_t lowRange,Double_t highRange,TCanvas* Can,Int_t CanvasPad)
{
	//	TF1* LinFit = new TF1("LinFit","","[0]",-50,50,1);
	TF1* LinFit = new TF1("pol0","pol0",-50,50);

	Can->cd(CanvasPad+1);
	//..project to the x-axis. But only around y=0.
	TString ProjectionName;
	ProjectionName= Histo->GetName();
	ProjectionName+="_projX_range";
	TH1D *PprojX=Histo->ProjectionX((const char*)ProjectionName,Histo->GetYaxis()->FindBin(lowRange),Histo->GetYaxis()->FindBin(highRange));
	SetTH1Histo(PprojX,"#Delta #phi","dN^{#gamma-h}/dN^{#gamma}",1);
	PprojX->DrawCopy("E");
	PprojX->Fit("pol0","Q","",-50,50);//Q = quiet mode, no printout

	//cout<<"param: "<<LinFit->GetParameter(0)<<endl;
	//..determine/etabin yield (count bins over which it was integrated)
	Int_t nBins= Histo->GetYaxis()->FindBin(highRange)-Histo->GetYaxis()->FindBin(lowRange);
	Double_t Scalef = LinFit->GetParameter(0)/(1.0*nBins);
	TString TopLegendText=Form("fit value: %0.2f/%i",LinFit->GetParameter(0),nBins);
	PlotTopLegend((const char*)TopLegendText,0.2,0.25,0.07,kGreen-2);
	Histo->Scale(1/Scalef);
}
///
/// Plot the 2D correlation functions
//
//________________________________________________________________________
void PlotGHcorrelation2::Plot2DHistograms()
{
	Int_t maxBins=0;
	if(fObservable==0)maxBins=kGammaNBINS;
	if(fObservable==1)maxBins=kZtNBINS;
	if(fObservable==2)maxBins=kXiNBINS;

	for(Int_t i=0;i<maxBins;i++)
	{
		fRaw_Plots_2D_GammaV1[i]->Divide(5,4);

		for(Int_t j=0;j<kNvertBins;j++)
		{
			fRaw_Plots_2D_GammaV1[i]->cd(j+1);
			SetTH2Histo(fdEdP_G[i][j],"","");
			fdEdP_G[i][j]->DrawCopy("lego2");
		}
	}
/*
	Int_t startValue=0; //..to access the right histograms for the 3 canvases
	if(version==0)startValue=0;
	if(version==1)startValue=3;
	if(version==2)startValue=6;

	Int_t maxCanvasPad=3; //..to access the right histograms for the 3 canvases
    //..check that the last canvas (version=3) is not exceeding the limit of the histogram array
	if(version==2)
    {
		TString histoName=rawHistoSE[0]->GetName();
		if(histoName.Contains("DEtaDPhiG")) {maxCanvasPad = kGammaNBINS-2*3;}
		if(histoName.Contains("DEtaDPhiZT")){maxCanvasPad = kZtNBINS-2*3;	}
		if(histoName.Contains("DEtaDPhiXI")){maxCanvasPad = kXiNBINS-2*3;	}
   }
    //- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
	//..GH Histograms - for different gamma energies
	rawPlots->Divide(3,2);
	for(Int_t i=0;i<maxCanvasPad;i++)
	{
		rawPlots->cd(i+1);
		SetTH2Histo(rawHistoSE[i+startValue],"","");
		rawHistoSE[i+startValue]->DrawCopy("lego2");

		rawPlots->cd(i+4);
		SetTH2Histo(rawHistoME[i+startValue],"","");
		rawHistoME[i+startValue]->DrawCopy("lego2");
	}
*/
}
///
/// Plot the corrected 2D correlation functions (SE/ME)
//
//________________________________________________________________________
void PlotGHcorrelation2::PlotCorrected2DHistograms()
{
	Int_t maxBins=0;
	if(fObservable==0)maxBins=kGammaNBINS;
	if(fObservable==1)maxBins=kZtNBINS;
	if(fObservable==2)maxBins=kXiNBINS;

	for(Int_t i=0;i<maxBins;i++)
	{
		fCorrected_Plots_2D_Gamma[i]->Divide(5,4);

		for(Int_t j=0;j<kNvertBins;j++)
		{
			fCorrected_Plots_2D_Gamma[i]->cd(j+1);
			fdEdP_G[i][j]->Divide(fdEdP_G_ME[i][j]);
			SetTH2Histo(fdEdP_G[i][j],"","");
			fdEdP_G[i][j]->DrawCopy("lego2");
		}
	}
	//..now add all the corrected 2D histograms
	//..for different Z vertices
	fPlots_2D_GammaSum->Divide(3,3);
	for(Int_t i=0;i<maxBins;i++)
	{
		for(Int_t j=0;j<kNvertBins;j++)
		{
			fsumCorrSE[i]->Add(fdEdP_G[i][j]);
		}
		fPlots_2D_GammaSum->cd(i+1);
		SetTH2Histo(fsumCorrSE[i],"","");
		fsumCorrSE[i]->DrawCopy("lego2");
	}
	//..As an alternative (to see the difference)
	//..add all Z-vtx bins for SE and ME and then
	//..divide them. (old simple method)
	fPlots_2D_GammaSum_alt->Divide(3,3);
	for(Int_t i=0;i<maxBins;i++)
	{
		fPlots_2D_GammaSum_alt->cd(i+1);
		fsumCorrSEalt[i]->Divide(fsumCorrMEalt[i]);
		SetTH2Histo(fsumCorrSEalt[i],"","");
		fsumCorrSEalt[i]->DrawCopy("lego2");
	}
}
///
/// Project the corrected histogram on the eta and phi axis to determine the NS width
//
//________________________________________________________________________
void PlotGHcorrelation2::DetermineWidths(TCanvas* Can,TH2D* corrHistoSE[])
{
    Int_t maxCanvasPad=0;
	TString histoName=corrHistoSE[0]->GetName();
	if(histoName.Contains("DEtaDPhiG")) {maxCanvasPad = kGammaNBINS;}
	if(histoName.Contains("DEtaDPhiZT")){maxCanvasPad = kZtNBINS;	}
	if(histoName.Contains("DEtaDPhiXI")){maxCanvasPad = kXiNBINS;	}

	//cout<<"inside of DetermineWidths()"<<endl;
/*	TF1* GaussFunc  = new TF1("GaussFunc",&PlotGHcorrelation2::PolyTwoGaussFitFunc,-100,300,11);
	TF1* GaussFunc1 = new TF1("GaussFunc1",&PlotGHcorrelation2::PolyTwoGaussFitFunc,-100,300,11);
	TF1* GaussFunc2 = new TF1("GaussFunc2",&PlotGHcorrelation2::PolyTwoGaussFitFunc,-100,300,11);
*/	TF1* GaussFunc  = new TF1("GaussFunc",&PlotGHcorrelation2::PolyTwoGaussFitFunc,-100,300,11);
	TF1* GaussFunc1 = new TF1("GaussFunc1",&PlotGHcorrelation2::PolyTwoGaussFitFunc,-100,300,11);
	TF1* GaussFunc2 = new TF1("GaussFunc2",&PlotGHcorrelation2::PolyTwoGaussFitFunc,-100,300,11);
	TString ProjectionName;

	Can->Divide(4,4,0.001,0.001);
	for(Int_t i=0;i<maxCanvasPad;i++)
	{
		Can->cd(i*2+1);
		//project to the y-axis. But only the near side.
		ProjectionName= corrHistoSE[i]->GetName();
		ProjectionName+="_projY";
		TH1D *projY=corrHistoSE[i]->ProjectionY((const char*)ProjectionName,0,corrHistoSE[i]->GetYaxis()->FindBin(180)); //! correct for 2pi case
		SetTH1Histo(projY,"","dN^{#gamma-h}/dN^{#gamma}",1);
		ZoomYRange(projY);
		projY->DrawCopy("E");
		//..fit, draw and save widths
		FitGaussAndDraw(i+1,projY,GaussFunc,GaussFunc1,GaussFunc2,0);

		Can->cd(i*2+2);
		TH1D *projX=corrHistoSE[i]->ProjectionX();
		SetTH1Histo(projX,"","dN^{#gamma-h}/dN^{#gamma}",1);
		ZoomYRange(projX);
		projX->DrawCopy("E");
		FitGaussAndDraw(i+1,projX,GaussFunc,GaussFunc1,GaussFunc2,1);
	}
}

///
/// Fit a Gauss function to the near side peak
/// to determine its width
//
//________________________________________________________________________
void PlotGHcorrelation2::FitGaussAndDraw(Int_t bin,TH1D* corrProjHistoSE,TF1* Func1, TF1* Func2,TF1* Func3,Bool_t EtaPhi)
{
	TString histoName=corrProjHistoSE->GetName();
	Int_t maxCanvasPad;
	if(histoName.Contains("DEtaDPhiG")) {maxCanvasPad = kGammaNBINS;}
	if(histoName.Contains("DEtaDPhiZT")){maxCanvasPad = kZtNBINS;	}
	if(histoName.Contains("DEtaDPhiXI")){maxCanvasPad = kXiNBINS;	}

	for(Int_t g = 0; g < 11; g++)
	{
		Func1->ReleaseParameter(g);
		Func1->SetParameter(g,0);
		Func1->SetParError(g,0.0);
	}
	Func1->SetParameter(1,0); //..start value for mean1
	Func1->SetParameter(4,0); //..start value for mean2


	if(EtaPhi==0)//..Eta case
	{
		//- - - - - - - - - - - - - - - -
		//..Flat background
		//..for eta -> estimate the background level on eta=-0.7&+0.7
		Double_t backgroundLevel=corrProjHistoSE->GetBinContent(corrProjHistoSE->FindBin(-0.7));
	    backgroundLevel+=corrProjHistoSE->GetBinContent(corrProjHistoSE->FindBin(-0.7));
	    backgroundLevel*=0.5;
	    //cout<<"Background level="<<backgroundLevel<<endl;
	    Func1->SetParameter(6,backgroundLevel);
	    Func1->SetParLimits(6,backgroundLevel*0.9,backgroundLevel*1.1);  //..allow a variation of +-10%

	    //- - - - - - - - - - - - - - - -
		//..big, narrow gaussian
	    //..etimate amplitude by 0 heigth and background level
		Double_t amplEst=corrProjHistoSE->GetBinContent(corrProjHistoSE->FindBin(0));
		amplEst-=backgroundLevel;
		Func1->SetParameter(0,amplEst);    //..amplitude
		Func1->SetParameter(2,0.05);       //..width
		Func1->SetParLimits(0,amplEst*0.9,amplEst*1.1);
		Func1->SetParLimits(1,-0.1,0.1);  //..mean limits
		Func1->SetParLimits(2,0.05,0.5);  //..width limits

	    //- - - - - - - - - - - - - - - -
		//..small, wide gaussian
		Func1->SetParameter(3,0.05);       //..amplitude
		Func1->SetParameter(5,1.1);        //..width
		Func1->SetParLimits(3,0.05,0.5);   //..amplitude limits 5-50% of the main peak (ampl2 = param0*param3)
		Func1->SetParLimits(4,-0.1,0.1);   //..mean limits
		Func1->SetParLimits(5,1.05,3.0);   //..width limits 105%-300% of the big one (width2 = param2*param5)

	    //- - - - - - - - - - - - - - - -
		//..plot range for eta projection
	    Func1->SetRange(-1.5,1.5);
	    Func2->SetRange(-1.5,1.5);
	    Func3->SetRange(-1.5,1.5);
	}

	if(EtaPhi==1)//..phi case
	{
	    //- - - - - - - - - - - - - - - -
		//..Flat background
		//..for phi -> estimate the background level on phi=-85&+85
		Double_t backgroundLevel=corrProjHistoSE->GetBinContent(corrProjHistoSE->FindBin(-85));
	    backgroundLevel+=corrProjHistoSE->GetBinContent(corrProjHistoSE->FindBin(+85));
	    backgroundLevel*=0.5;
	    //cout<<"Background level="<<backgroundLevel<<endl;
	    Func1->SetParameter(6,backgroundLevel);
	    Func1->SetParLimits(6,backgroundLevel*0.9,backgroundLevel*1.1);  //..allow a variation of +-10%

	    //- - - - - - - - - - - - - - - -
		//..big, narrow gaussian
	    //..etimate amplitude by 0 heigth and background level
		Double_t amplEst=corrProjHistoSE->GetBinContent(corrProjHistoSE->FindBin(0));
		amplEst-=backgroundLevel;
		Func1->SetParameter(0,amplEst);   //..amplitude
		Func1->SetParameter(2,15);        //..width
		Func1->SetParLimits(0,amplEst*0.9,amplEst*1.1);
		Func1->SetParLimits(1,-0.1,0.1);  //..mean limits
		Func1->SetParLimits(2,5,30);      //..width limits

	    //- - - - - - - - - - - - - - - -
		//..small, wide gaussian
		Func1->SetParameter(3,0.05);       //..amplitude
		Func1->SetParameter(5,1.1);        //..width
		Func1->SetParLimits(3,0.05,0.5);   //..amplitude limits 5-50% of the main peak (ampl2 = param0*param3)
		Func1->SetParLimits(4,-0.1,0.1);   //..mean limits
		Func1->SetParLimits(5,1.05,3.0);   //..width limits 105%-300% of the big one (width2 = param2*param5)

	    //- - - - - - - - - - - - - - - -
		//..plot range for eta projection
	    Func1->SetRange(-90,90);
	    Func2->SetRange(-90,90);
	    Func3->SetRange(-90,90);
	}

    //.. CAREFUL YOU CAN ALSO SET THE SECOND GAUSS TO 0.
	Func1->FixParameter(3,0);

	Func1->SetLineColor(15);
	Func1->SetLineStyle(2);

	TString Name= Func1->GetName();
	if(EtaPhi==0)corrProjHistoSE->Fit(Name,"Q","",-1,1);//Q = quiet mode, no printout
	if(EtaPhi==1)corrProjHistoSE->Fit(Name,"Q","",-70,70);//Q = quiet mode, no printout

	//..width that is used to define an eta range not contaminated by the near side peak
	//..you can define the width in multiple ways.
	//..THINK ABOUT THAT!
	//Width   =Func1->GetParameter(5)*Func1->GetParameter(2);//bigger width in delta phi
	Double_t Width   =Func1->GetParameter(2); //bigger width in delta phi
	Double_t ErrWidth=Func1->GetParError(2);
	if(EtaPhi==0)//..eta case
	{
		if(maxCanvasPad == kGammaNBINS)
		{
			fEtaWidth_gamma->SetBinContent(bin,Width);
			fEtaWidth_gamma->SetBinError(bin,ErrWidth);
		}
		if(maxCanvasPad == kZtNBINS)
		{
			fEtaWidth_Zt->SetBinContent(bin,Width);
			fEtaWidth_Zt->SetBinError(bin,ErrWidth);
		}
		if(maxCanvasPad == kXiNBINS)
		{
			fEtaWidth_Xi->SetBinContent(bin,Width);
			fEtaWidth_Xi->SetBinError(bin,ErrWidth);
		}
	}
	if(EtaPhi==1)//..phi case
	{
		if(maxCanvasPad == kGammaNBINS)
		{
			fPhiWidth_gamma->SetBinContent(bin,Width);
			fPhiWidth_gamma->SetBinError(bin,ErrWidth);
		}
		if(maxCanvasPad == kZtNBINS)
		{
			fPhiWidth_Zt->SetBinContent(bin,Width);
			fPhiWidth_Zt->SetBinError(bin,ErrWidth);
		}
		if(maxCanvasPad == kXiNBINS)
		{
			fPhiWidth_Xi->SetBinContent(bin,Width);
			fPhiWidth_Xi->SetBinError(bin,ErrWidth);
		}
	}
	//  cout<<"Width gauss2: "<<Func1->GetParameter(5)<<" times of width 1"<<endl;

	for(Int_t g = 0; g < 11; g++)
	{
		Func2->SetParameter(g,Func1->GetParameter(g));
		Func3->SetParameter(g,Func1->GetParameter(g));
	}
	//..small, wide gaussian
	//..due to the fact that param 0 and param 3 are proportional
	//..we do a little hack here. Setting param0 to 0 is neseccary
	//..to see only the small wiede gaussian. If we set param0 to 0
	//..however, param3 will become 0 by defualt. We can however
	//..set param0 to a negligibly small value x and multiply param3
	//..by the inverse of x. (normally param3 is in the range 0-1, but we omit this for this specific case)
	Double_t Shrinkage=0.00001;
	//Func2
	Func2->SetParameter(0,Shrinkage);
	Func2->SetParameter(3,1.0*Func1->GetParameter(3)*Func1->GetParameter(0)/Shrinkage);
	Func2->SetLineColor(kPink-9);
	if(Func1->GetParameter(3)!=0)Func2 ->DrawCopy("same"); //..only when the small-broad gaussian is not set to 0

    //..big, narrow gaussian (green)
	Func3->SetParameter(3,0);
	Func3->SetLineColor(kGreen-2);
	Func3 ->DrawCopy("same");

	//..flat background
	Func1->SetParameter(0,0);
	Func1->SetParameter(3,0);
	Func1 ->DrawCopy("same");

	//..plot 3 sigma range (enough to split off near side peak)
	PlotVerLine3(Width*3,corrProjHistoSE,0.8,17);
	PlotVerLine3(-(Width*3),corrProjHistoSE,0.8,17);

	//..Draw legend
	TString TopLegendText;
	Double_t x_Pos;
	if(EtaPhi==0)x_Pos=0.2;//Eta case
	if(EtaPhi==1)x_Pos=0.6;//Phi case
	TopLegendText=Form("#mu_{1}: %0.3f",Func1->GetParameter(1));
	PlotTopLegend((const char*)TopLegendText,x_Pos,0.85,0.07,kGreen-2);
	TopLegendText=Form("#sigma_{1}: %0.2f",Func1->GetParameter(2));
	PlotTopLegend((const char*)TopLegendText,x_Pos,0.78,0.07,kGreen-2);

	if(Func1->GetParameter(3)!=0)
	{
		TopLegendText=Form("#mu_{2}: %0.3f",Func1->GetParameter(4));
		PlotTopLegend((const char*)TopLegendText,x_Pos,0.71,0.07,kPink-9);
		TopLegendText=Form("#sigma_{2}: %0.2f",Func1->GetParameter(5)*Func1->GetParameter(2));
		PlotTopLegend((const char*)TopLegendText,x_Pos,0.64,0.07,kPink-9);
	}
}
///
/// Project the corrected histogram on the eta and phi axis to determine the NS width
//
//________________________________________________________________________
void PlotGHcorrelation2::DrawWidths(TCanvas* Can,TH1* etaWidth,TH1* phiWidth)
{
	Int_t version;
	TString histoName=etaWidth->GetName();
	if(histoName.Contains("gamma")) {version = 0;}
	if(histoName.Contains("Zt"))    {version = 1;}
	if(histoName.Contains("Xi"))    {version = 2;}

	cout<<"histoname: "<<histoName<<", version: "<<version<<endl;
	//..Draw a canvas with the eta and phi widths
	Can->Divide(2);
	Can->cd(1);
	if(version==0)SetTH1Histo(etaWidth,"E_{#gamma}","#sigma of #Delta#eta");
	if(version==1)SetTH1Histo(etaWidth,"z_{T}","#sigma of #Delta#eta");
	if(version==2)SetTH1Histo(etaWidth,"#xi","#sigma of #Delta#eta");
	//if(version==0)etaWidth->GetXaxis()->SetRangeUser(0,1);
	if(version==1)etaWidth->GetXaxis()->SetRangeUser(0,1);
	if(version==2)etaWidth->GetXaxis()->SetRangeUser(0,2);
	etaWidth->SetMarkerColor(17);
	etaWidth->SetMarkerStyle(20);
	etaWidth->SetMarkerSize(1.1);
	etaWidth->DrawCopy("E");
	etaWidth->SetLineColor(fColorSceme[0]);
	etaWidth->SetMarkerColor(fColorSceme[0]);
	etaWidth->SetMarkerSize(0.8);
	etaWidth->DrawCopy("same E");

	Can->cd(2);
	if(version==0)SetTH1Histo(phiWidth,"E_{#gamma}","#sigma of #Delta#phi");
	if(version==1)SetTH1Histo(phiWidth,"z_{T}","#sigma of #Delta#phi");
	if(version==2)SetTH1Histo(phiWidth,"#xi","#sigma of #Delta#phi");
	//if(version==0)phiWidth->GetXaxis()->SetRangeUser(0,1);
	if(version==1)phiWidth->GetXaxis()->SetRangeUser(0,1);
	if(version==2)phiWidth->GetXaxis()->SetRangeUser(0,2);
	phiWidth->SetMarkerColor(17);
	phiWidth->SetMarkerStyle(20);
	phiWidth->SetMarkerSize(1.1);
	phiWidth->DrawCopy("E");
	phiWidth->SetLineColor(fColorSceme[0]);
	phiWidth->SetMarkerColor(fColorSceme[0]);
	phiWidth->SetMarkerSize(0.8);
	phiWidth->DrawCopy("same E");
}
///
///
///
//________________________________________________________________________
void PlotGHcorrelation2::FitEtaSides(TH2D* corrHistoSE[],Double_t sigmas,TCanvas* can)
{
	can->Divide(4,4);

	TString histoName=corrHistoSE[0]->GetName();
	Int_t maxCanvasPad;
	Double_t width=0;
	if(histoName.Contains("DEtaDPhiG")) {maxCanvasPad = kGammaNBINS;}
	if(histoName.Contains("DEtaDPhiZT")){maxCanvasPad = kZtNBINS;	}
	if(histoName.Contains("DEtaDPhiXI")){maxCanvasPad = kXiNBINS;	}

	for(Int_t i=0;i<maxCanvasPad;i++)
	{
		if(maxCanvasPad == kGammaNBINS)
		{
			width=fEtaWidth_gamma->GetBinContent(i+1);
		}
		if(maxCanvasPad == kZtNBINS)
		{
			width=fEtaWidth_Zt->GetBinContent(i+1);
		}
		if(maxCanvasPad == kXiNBINS)
		{
			width=fEtaWidth_Xi->GetBinContent(i+1);
		}
		FitEtaSide(corrHistoSE[i],width,sigmas,can,i);
	}
}
///
///
///
//________________________________________________________________________
void PlotGHcorrelation2::FitEtaSide(TH2D* corrHistoSE,Double_t width,Double_t Sigmas,TCanvas* Can,Int_t CanvasPad)
{
	TString TopLegendText;
    TString ProjectionName;
    TF1* BackgroundFunction = new TF1("FlowFunction",FlowFunction,-100,300,4);
    Int_t vN=3;
    TF1 *allFit             = allFitFuncVn("JoelsFlowFunction",vN,-100,300);

	//..select the eta range in which you want to project your signal
	Int_t SignalCenter    = corrHistoSE->GetYaxis()->FindBin(0.0);
	Int_t SignalEdgeLow   = corrHistoSE->GetYaxis()->FindBin(0-width*Sigmas);
	Int_t SignalEdgeHigh  = corrHistoSE->GetYaxis()->FindBin(0+width*Sigmas);
	Int_t SignalEdgeLowT  = corrHistoSE->GetYaxis()->FindBin(0-width*(Sigmas+1));//..increase sigma ragne by 1 to check signal left over
	Int_t SignalEdgeHighT = corrHistoSE->GetYaxis()->FindBin(0+width*(Sigmas+1));//..increase sigma ragne by 1 to check signal left over
	Int_t LowestBin       = corrHistoSE->GetYaxis()->FindBin(-1.4); //! write a function alla find last good bin
	Int_t HighestBin      = corrHistoSE->GetYaxis()->FindBin(1.4);

	//..check that the mean+-sigma is not larger or smaller than the histogram range
	if(SignalEdgeLow<LowestBin || SignalEdgeHigh>HighestBin)
	{
		cout<<"Error: Problem detected!"<<endl;
		cout<<"In Histo: "<<corrHistoSE->GetName()<<endl;
		cout<<"Signal range is reaching outside the histogram boundaries - please correct"<<endl;
		cout<<"bins lowes"<<LowestBin<<", edge "<<SignalEdgeLow<<", center"<<SignalCenter <<", uppedge"<< SignalEdgeHigh<<", highestbin "<< HighestBin<<endl;
		SignalEdgeLow=LowestBin;
		SignalEdgeHigh=HighestBin;
	}
	//..determine a scale factor to fit NS to away side
	//..this is necessary since the flow function is only fit
	//..to the side band but needs to be scaled to the whole eta range
	Double_t scaleFactorSBtoNS;// = ScaleSBtoNS(corrHistoSE,LowestBin,SignalEdgeLow,SignalEdgeHigh,HighestBin);

	Int_t nBinsSB = (SignalEdgeLow-LowestBin)+(HighestBin-SignalEdgeHigh);
	Int_t nBinsNS = SignalEdgeHigh-SignalEdgeLow;
	scaleFactorSBtoNS=1.0*nBinsSB/nBinsNS;
    cout<<"^-^"<<"bins SB: "<<nBinsSB<<", bins NS: "<<nBinsNS<<", scaling: "<<scaleFactorSBtoNS<<endl;

    ProjectionName=corrHistoSE->GetName();
    ProjectionName+="PprojXSig";
	TH1D *PprojXSig  =corrHistoSE->ProjectionX(ProjectionName,SignalEdgeLow,SignalEdgeHigh);
    ProjectionName=corrHistoSE->GetName();
    ProjectionName+="PprojXSide1";
	TH1D *PprojXSide1=corrHistoSE->ProjectionX(ProjectionName,LowestBin,SignalEdgeLow);
    ProjectionName=corrHistoSE->GetName();
    ProjectionName+="PprojXSide2";
    TH1D *PprojXSide2=corrHistoSE->ProjectionX(ProjectionName,SignalEdgeHigh,HighestBin);
    //.. Test part
    ProjectionName+="PprojXSide1_Test";
	TH1D *PprojXSide1_T=corrHistoSE->ProjectionX(ProjectionName,LowestBin,SignalEdgeLowT);
    ProjectionName=corrHistoSE->GetName();
    ProjectionName+="PprojXSide2_Test";
    TH1D *PprojXSide2_T=corrHistoSE->ProjectionX(ProjectionName,SignalEdgeHighT,HighestBin);


	//..perform now a projection of the 2Dhistogram in a given range x-sigmas outside the jet region
	Can->cd(CanvasPad*2+1);
	//..project to plot the delta phi distribution in a certain Delta Eta intervall
	SetTH1Histo(PprojXSig,"","",1);
	ZoomYRange(PprojXSig);
	PprojXSig->DrawCopy("E");
	TopLegendText=Form("Projection in range:");
	PlotTopLegend((const char*)TopLegendText,0.53,0.85,0.07);
	TopLegendText=Form("%0.2f < #eta < %0.2f",0-width*Sigmas,0+width*Sigmas);
	PlotTopLegend((const char*)TopLegendText,0.53,0.77,0.07);

	//..large eta
	Can->cd(CanvasPad*2+2);
	PprojXSide1  ->Add(PprojXSide2);
	PprojXSide1_T->Add(PprojXSide2_T);
	SetTH1Histo(PprojXSide1,"","",1);
	ZoomYRange(PprojXSide1);
	PprojXSide1->DrawCopy("E");
	//..scale the test histo to the awway side yield
//	Double_t Integral1=PprojXSide1  ->Integral(PprojXSide1->FindBin(160),PprojXSide1->FindBin(200));
//	Double_t Integral2=PprojXSide1_T->Integral(PprojXSide1_T->FindBin(160),PprojXSide1_T->FindBin(200));
//	PprojXSide1_T->SetLineColor(6);
//	PprojXSide1_T->SetMarkerColor(6);
//	Double_t ratio=(Double_t)Integral1/(Double_t)Integral2;
//	PprojXSide1_T->Scale(ratio);
//	Double_t Integral3=PprojXSide1_T->Integral(PprojXSide1_T->FindBin(160),PprojXSide1_T->FindBin(200));
//	PprojXSide1_T->DrawCopy("same E");


    //fit with the flow function
	BackgroundFunction->SetParNames("B", "comb v1", "comb v2", "comb v3");

	for(Int_t g = 0; g < 4; g++)
	{
		BackgroundFunction->ReleaseParameter(g);
		BackgroundFunction->SetParameter(g,0);
		BackgroundFunction->SetParError(g,0.0);
	}
	//flat line
//	BackgroundFunction->FixParameter(1,0.0);
//	BackgroundFunction->FixParameter(2,0.0);
//	BackgroundFunction->FixParameter(3,0.0);

	BackgroundFunction->SetParLimits(1,0.0,100); //..do not allow negative values
	BackgroundFunction->SetParLimits(2,0.0,1);   //..do not allow negative values, v2<v1 (v2 strength fraction of v1 strength)
	BackgroundFunction->SetParLimits(3,0.0,1);   //..do not allow negative values, v3<v2 (v3 strength fraction of v2 strength)

	// arbirtary - set for you, I'm just copying and generalizing stuff here
	//double par_V10[15] = {450, 0.8e-1, 5e-2, 1e-4, 1e-2, 1e-2,1e-4, 1e-3, 1e-3, 1e-5, 1e-3, 1e-3, 1e-5, 1e-3, 1e-3};
	double par_V10[15] = {450, 0, 0, 0,0, 0,0, 0, 0, 0, 0, 0, 0, 0, 0};
	//.. Fit Range
	Double_t limits = 90; //..pi
	//limits=1.25*180.0/TMath::Pi();  //..joels paper range 1 (71.6deg too small)
	limits=1.57*180.0/TMath::Pi();  //..joels paper range 2 (90deg)

	SetJoelsParLimits(allFit,PprojXSide1,par_V10);

	TString funcName    = BackgroundFunction->GetName();
	TString JoelfuncName= allFit->GetName();
	TFitResultPtr r  = PprojXSide1->Fit(funcName,"","",-limits,limits);//Q = quiet mode, no printout
	TFitResultPtr r2 = PprojXSide1->Fit(JoelfuncName,"S0","",-limits,limits);

	allFit->SetLineColor(kRed-9);
	allFit->SetRange(-100,300);
	allFit->DrawCopy("same");
	BackgroundFunction->SetLineStyle(9);//
	BackgroundFunction->SetLineColor(kCyan-5);//
	BackgroundFunction->SetRange(-100,300);
	BackgroundFunction->DrawCopy("same");

	TopLegendText=Form("Projection in range:");
	PlotTopLegend((const char*)TopLegendText,0.2,0.85,0.07);
	TopLegendText=Form("%0.2f < #eta < %0.2f",-1.5,0-width*Sigmas);
	PlotTopLegend((const char*)TopLegendText,0.2,0.77,0.07);
	TopLegendText=Form(" %0.2f < #eta < %0.2f",0+width*Sigmas,1.5);
	PlotTopLegend((const char*)TopLegendText,0.2,0.69,0.07);

	//..Return the background subtracted correlation spectrum
	ProjectionName = PprojXSide1->GetName();
	ProjectionName+="_backgroundSubtracted";
	BackgroundFunction->SetLineColor(kWhite);
	TH1D* BackgroundSubtraction = (TH1D*)PprojXSide1->Clone(ProjectionName);
	BackgroundSubtraction->Add(BackgroundFunction,-1);
//	BackgroundSubtraction->GetFunction(funcName)->Delete();//..to not draw the fit function at any point later

  //  return BackgroundSubtraction;
}

/*
///
/// Plot the 2D correlation functions
//
//________________________________________________________________________
void PlotGHcorrelation2::PlotProjectedCorr(Int_t version,TH2D* corrHistoSE[],TH2D* corrHistoSEClone[])
{
	Double_t SigmaEtaSigmaPhi[4];
	TCanvas *corrPlotsWidth[version] = new TCanvas("Corr1D_GPlots","Corrected_G 1D Plots 1) Full",...,1400,1400);
	TCanvas *CanvProj = new TCanvas("Corr1D_GPlots_SB","Corrected_G 1D Plots 2) SB",1400,1400);
	TCanvas *CanvProj2 = new TCanvas("Corr1D_GPlots_SB2","Corrected_G 1D Plots 2) SB",1400,1400);

	Int_t maxCanvasPad=0;
	TString histoName=rawHistoSE[0]->GetName();
	//kan ich auch mit version machen
	if(histoName.Contains("DEtaDPhiG")) {maxCanvasPad = kGammaNBINS;}
	if(histoName.Contains("DEtaDPhiZT")){maxCanvasPad = kZtNBINS;	}
	if(histoName.Contains("DEtaDPhiXI")){maxCanvasPad = kXiNBINS;	}

	corrPlotsWidth[version] ->Divide(4,4,0.001,0.001);
	corrPlotsNSfit[version] ->Divide(4,4);
	corrPlotsNSfit2[version]->Divide(4,4);

	for(Int_t i=0;i<maxCanvasPad;i++)
	{
		//..perform a 2D gaussian fit to determine the near side width in eta and phi
		//..this is useful to determine the range of eta not influenced by the jet signal
		DetermineWidths(corrHistoSE[i],SigmaEtaSigmaPhi,Canv,i);
		//..plot large delta eta for checking out the background (determine flow coefficients for g_dir + g_decay mixture)
		//..get the width from the previous function

		FitEtaSides(corrHistoSEClone[i],SigmaEtaSigmaPhi,2.0,corrPlotsNSfit2[version],i);

		FitEtaSides(corrHistoSE[i],SigmaEtaSigmaPhi,3.5,corrPlotsNSfit[version],i);

		if(version==0)
		{
			fEtaWidth_gamma->SetBinContent(i+1,SigmaEtaSigmaPhi[0]);
			fEtaWidth_gamma->SetBinError(i+1,SigmaEtaSigmaPhi[1]);
			fPhiWidth_gamma->SetBinContent(i+1,SigmaEtaSigmaPhi[2]);
			fPhiWidth_gamma->SetBinError(i+1,SigmaEtaSigmaPhi[3]);
		}
		if(version==1)
		{
			fEtaWidth_Zt->SetBinContent(i+1,SigmaEtaSigmaPhi[0]);
			fEtaWidth_Zt->SetBinError(i+1,SigmaEtaSigmaPhi[1]);
			fPhiWidth_Zt->SetBinContent(i+1,SigmaEtaSigmaPhi[2]);
			fPhiWidth_Zt->SetBinError(i+1,SigmaEtaSigmaPhi[3]);
		}
		if(version==2)
		{
			fEtaWidth_Xi->SetBinContent(i+1,SigmaEtaSigmaPhi[0]);
			fEtaWidth_Xi->SetBinError(i+1,SigmaEtaSigmaPhi[1]);
			fPhiWidth_Xi->SetBinContent(i+1,SigmaEtaSigmaPhi[2]);
			fPhiWidth_Xi->SetBinError(i+1,SigmaEtaSigmaPhi[3]);
		}
	}
    //..Draw a canvas with the eta and phi widths
	TCanvas *Canv_Width = new TCanvas("Canv_Width","Width of NS peak vs E_g",1400,700);
	Canv_Width->Divide(2);
	Canv_Width->cd(1);
	SetTH1Histo(EtaWidth_gamma,"E_{#gamma}","#sigma of #Delta#eta");
	EtaWidth_gamma->SetMarkerColor(17);
	EtaWidth_gamma->SetMarkerStyle(20);
	EtaWidth_gamma->SetMarkerSize(1.1);
	EtaWidth_gamma->DrawCopy("E");
	EtaWidth_gamma->SetLineColor(colorSceme[0]);
	EtaWidth_gamma->SetMarkerColor(colorSceme[0]);
	EtaWidth_gamma->SetMarkerSize(0.8);
	EtaWidth_gamma->DrawCopy("same E");

	Canv_Width->cd(2);
	SetTH1Histo(PhiWidth_gamma,"E_{#gamma}","#sigma of #Delta#phi");
	PhiWidth_gamma->SetMarkerColor(17);
	PhiWidth_gamma->SetMarkerStyle(20);
	PhiWidth_gamma->SetMarkerSize(1.1);
	PhiWidth_gamma->DrawCopy("E");
	PhiWidth_gamma->SetLineColor(colorSceme[0]);
	PhiWidth_gamma->SetMarkerColor(colorSceme[0]);
	PhiWidth_gamma->SetMarkerSize(0.8);
	PhiWidth_gamma->DrawCopy("same E");


	//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 	cout<<"Draw Projections and fit near side with for ZT"<<endl;
	TCanvas *Canv2 = new TCanvas("Corr1D_ZTPlots","Corrected_ZT 1D Plots 1)Full",1400,1400);
	Canv2->Divide(4,4,0.001,0.001);
	TCanvas *Canv2Proj = new TCanvas("Corr1D_ZTPlots_SB","Corrected_ZT 1D Plots 2) SB",1400,1400);
	Canv2Proj->Divide(4,4);
	for(Int_t i=0;i<7;i++)
	{
		DetermineWidths(dEdP_ZT[i],SigmaEtaSigmaPhi,Canv2,i);
		dEdP_ZT_BgSub[i] = FitEtaSides(dEdP_ZT[i],SigmaEtaSigmaPhi,3.5,Canv2Proj,i);

		EtaWidth_Zt->SetBinContent(i+1,SigmaEtaSigmaPhi[0]);
		EtaWidth_Zt->SetBinError(i+1,SigmaEtaSigmaPhi[1]);
		PhiWidth_Zt->SetBinContent(i+1,SigmaEtaSigmaPhi[2]);
		PhiWidth_Zt->SetBinError(i+1,SigmaEtaSigmaPhi[3]);
	}

	//..Draw a canvas with the eta and phi widths
	TCanvas *Canv_Width2 = new TCanvas("Canv_Width2","Width of NS peak vs Zt",1400,700);
	Canv_Width2->Divide(2);
	Canv_Width2->cd(1);
	SetTH1Histo(EtaWidth_Zt,"z_{T}","#sigma of #Delta#eta");
	EtaWidth_Zt->GetXaxis()->SetRangeUser(0,1);
	EtaWidth_Zt->SetMarkerColor(17);
	EtaWidth_Zt->SetMarkerStyle(20);
	EtaWidth_Zt->SetMarkerSize(1.1);
	EtaWidth_Zt->DrawCopy("E");
	EtaWidth_Zt->SetLineColor(colorSceme[0]);
	EtaWidth_Zt->SetMarkerColor(colorSceme[0]);
	EtaWidth_Zt->SetMarkerSize(0.8);
	EtaWidth_Zt->DrawCopy("same E");

	Canv_Width2->cd(2);
	SetTH1Histo(PhiWidth_Zt,"z_{T}","#sigma of #Delta#phi");
	PhiWidth_Zt->GetXaxis()->SetRangeUser(0,1);
	PhiWidth_Zt->SetMarkerColor(17);
	PhiWidth_Zt->SetMarkerStyle(20);
	PhiWidth_Zt->SetMarkerSize(1.1);
	PhiWidth_Zt->DrawCopy("E");
	PhiWidth_Zt->SetLineColor(colorSceme[0]);
	PhiWidth_Zt->SetMarkerColor(colorSceme[0]);
	PhiWidth_Zt->SetMarkerSize(0.8);
	PhiWidth_Zt->DrawCopy("same E");

	//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
	cout<<"Draw Projections and fit near side with for Xi"<<endl;
	TCanvas *Canv3 = new TCanvas("Corr1D_XIPlots","Corrected_XI 1D Plots 1) Full",1400,1400);
	Canv3->Divide(4,4,0.001,0.001);
	TCanvas *Canv3Proj = new TCanvas("Corr1D_XIPlots_SB","Corrected_XI 1D Plots 2) SB",1400,1400);
	Canv3Proj->Divide(4,4);
	for(Int_t i=0;i<8;i++)
	{
		DetermineWidths(dEdP_XI[i],SigmaEtaSigmaPhi,Canv3,i);
		dEdP_XI_BgSub[i] = FitEtaSides(dEdP_XI[i],SigmaEtaSigmaPhi,3.5,Canv3Proj,i);

		EtaWidth_Xi->SetBinContent(i+1,SigmaEtaSigmaPhi[0]);
		EtaWidth_Xi->SetBinError(i+1,SigmaEtaSigmaPhi[1]);
		PhiWidth_Xi->SetBinContent(i+1,SigmaEtaSigmaPhi[2]);
		PhiWidth_Xi->SetBinError(i+1,SigmaEtaSigmaPhi[3]);
	}

	//..Draw a canvas with the eta and phi widths
	TCanvas *Canv_Width3 = new TCanvas("Canv_Width3","Width of NS peak vs Xi",1400,700);
	Canv_Width3->Divide(2);
	Canv_Width3->cd(1);
	SetTH1Histo(EtaWidth_Xi,"#xi","#sigma of #Delta#eta");
	EtaWidth_Xi->GetXaxis()->SetRangeUser(0,2);
	EtaWidth_Xi->SetMarkerColor(17);
	EtaWidth_Xi->SetMarkerStyle(20);
	EtaWidth_Xi->SetMarkerSize(1.1);
	EtaWidth_Xi->DrawCopy("E");
	EtaWidth_Xi->SetLineColor(colorSceme[0]);
	EtaWidth_Xi->SetMarkerColor(colorSceme[0]);
	EtaWidth_Xi->SetMarkerSize(0.8);
	EtaWidth_Xi->DrawCopy("same E");

	Canv_Width3->cd(2);
	SetTH1Histo(PhiWidth_Xi,"#xi","#sigma of #Delta#phi");
	PhiWidth_Xi->GetXaxis()->SetRangeUser(0,2);
	PhiWidth_Xi->SetMarkerColor(17);
	PhiWidth_Xi->SetMarkerStyle(20);
	PhiWidth_Xi->SetMarkerSize(1.1);
	PhiWidth_Xi->DrawCopy("E");
	PhiWidth_Xi->SetLineColor(colorSceme[0]);
	PhiWidth_Xi->SetMarkerColor(colorSceme[0]);
	PhiWidth_Xi->SetMarkerSize(0.8);
	PhiWidth_Xi->DrawCopy("same E");

}
*/
/*  |
   |
   |
  \  /
   \/
 to be updated

 */
//
//To Do list:
//
//-at very large eta wings there is low statistic and such large error bars.
//its not adding an advantage including these bins. They blow up the total
//error.
//
//-check systematic on fit range for flow function
//
//-add scale as a parameter to function to pass on
//
//-maybe move this macro to a class
//
//-add number of v_ns as flexible parameter. Right now its hard coded
//
//-check with porjection what the ranges are included/excluded
//
