//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//- - - - - - - Here are functions that get the historgams from the files - - - - - - - - - - - - - -
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include <TString.h>
#include <TFile.h>
#include <TH2D.h>
#include <TH1D.h>
#include <TROOT.h>
#include <TLatex.h>
#include <TLegend.h>
#include <TLine.h>
#include <TAxis.h>
#include <TGaxis.h>
#include <TCanvas.h>
#include <TColor.h>
#include <TStyle.h>
#include <TEnv.h>

#include <Riostream.h>

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
Int_t GetAccEvtFromFile(TFile* RootFile, TString wagonName)
{

	TList* lub    =(TList*)RootFile->Get(wagonName);
	if(!lub)cout<<"No list with name: "<<wagonName<<" found in root file: "<<RootFile->GetName()<<endl;
	TH1D* Histo   =(TH1D*)lub->FindObject("fHistEventCount");

	Int_t AccEvt=Histo->GetBinContent(1);
	return AccEvt;
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
Int_t GetRejEvtFromFile(TFile* RootFile, TString wagonName)
{

	TList* lub    =(TList*)RootFile->Get(wagonName);
	TH1D* Histo   =(TH1D*)lub->FindObject("fHistEventCount");


	Int_t AccEvt=Histo->GetBinContent(2);
	return AccEvt;
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
TH1D* Get1DHistoFromFile(TFile* RootFile,TString SubListName,TString Name, TString wagonName)
{
	TList* IntermediatList;
	TList* FinalList;

	if(SubListName=="")
	{
		FinalList    =(TList*)RootFile->Get(wagonName);
		//cout<<"Name of fin. ListA: "<<FinalList->GetName()<<endl;
	}
	else
	{
		IntermediatList=(TList*)RootFile       ->Get(wagonName);
		//cout<<"Name of interm. ListB: "<<IntermediatList->GetName()<<endl;
		FinalList      =(TList*)IntermediatList->FindObject(SubListName);
		//cout<<"Name of fin. ListB: "<<FinalList->GetName()<<endl;
	}
	TH1D* Histo   =(TH1D*)FinalList->FindObject(Name);

	return Histo;
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
TH2D* Get2DHistoFromFile(TFile* RootFile,TString SubListName,TString Name, TString wagonName)
{

    TList* IntermediatList;
	TList* FinalList;

	if(SubListName=="")
	{
		FinalList    =(TList*)RootFile->Get(wagonName);
	}
	else
	{
		IntermediatList=(TList*)RootFile       ->Get(wagonName);
		FinalList      =(TList*)IntermediatList->FindObject(SubListName);
	}
	TH2D* Histo   =(TH2D*)FinalList->FindObject(Name);

	return Histo;
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//- - - - - - - Here are functions that make the plotting of figures nicer- - - - - - - - - - - - - -
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
void set_plot_style()
{
    const Int_t NRGBs = 5;
    const Int_t NCont = 99;//max possible?

    //Double_t stops[NRGBs] = { 0.00, 0.34, 0.61, 0.84, 1.00 };
    Double_t stops[NRGBs] = { 0.00, 0.25, 0.5, 0.75, 1.00 };
    Double_t red[NRGBs]   = { 0.00, 0.00, 0.87, 1.00, 0.51 };
    Double_t green[NRGBs] = { 0.00, 0.81, 1.00, 0.20, 0.00 };
    Double_t blue[NRGBs]  = { 0.51, 1.00, 0.12, 0.00, 0.00 };
    TColor::CreateGradientColorTable(NRGBs, stops, red, green, blue, NCont);
    gStyle->SetNumberContours(NCont);
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
void SetTH1Histo(TH1 *Histo,TString Xtitel,TString Ytitel)
{
	Histo->SetStats(0);
	Histo->SetTitle("");
	Histo->GetYaxis()->SetTitleOffset(1.4);
	Histo->GetXaxis()->SetTitleOffset(1.4);
	Histo->GetXaxis()->SetLabelSize(0.05);
	Histo->GetYaxis()->SetLabelSize(0.05);
	Histo->GetXaxis()->SetTitleSize(0.045);
	Histo->GetYaxis()->SetTitleSize(0.045);
	Histo->GetXaxis()->CenterTitle();
	Histo->GetYaxis()->CenterTitle();
	Histo->GetXaxis()->SetNdivisions(505);
	Histo->GetYaxis()->SetNdivisions(505);
	//make nice font
    Histo->GetXaxis()->SetLabelFont(42);
    Histo->GetYaxis()->SetLabelFont(42);
    Histo->GetXaxis()->SetTitleFont(42);
    Histo->GetYaxis()->SetTitleFont(42);
	if(Xtitel!="")Histo->GetXaxis()->SetTitle(Xtitel);
	if(Ytitel!="")Histo->GetYaxis()->SetTitle(Ytitel);

	Histo->SetLineColor(1);
	Histo->SetMarkerColor(1);
	Histo->SetMarkerStyle(20);
	Histo->SetMarkerSize(0.5);

}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
void SetTH2Histo(TH2 *Histo,TString Xtitel,TString Ytitel)
{
	Histo->SetStats(0);
	Histo->SetTitle("");
	Histo->GetYaxis()->SetTitleOffset(1.7);
	Histo->GetXaxis()->SetTitleOffset(1.7);
	Histo->GetXaxis()->SetLabelSize(0.05);
	Histo->GetYaxis()->SetLabelSize(0.05);
	Histo->GetXaxis()->SetTitleSize(0.045);
	Histo->GetYaxis()->SetTitleSize(0.045);
	Histo->GetXaxis()->CenterTitle();
	Histo->GetYaxis()->CenterTitle();
	Histo->GetXaxis()->SetNdivisions(505);
	Histo->GetYaxis()->SetNdivisions(505);
	//make nice font
    Histo->GetXaxis()->SetLabelFont(42);
    Histo->GetYaxis()->SetLabelFont(42);
    Histo->GetXaxis()->SetTitleFont(42);
    Histo->GetYaxis()->SetTitleFont(42);
	if(Xtitel!="")Histo->GetXaxis()->SetTitle(Xtitel);
	if(Ytitel!="")Histo->GetYaxis()->SetTitle(Ytitel);
	Histo->SetLineColorAlpha(kBlue+2,0.095);
//	Histo->GetYaxis()->SetRangeUser(-1.3,1.3);

}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
TLatex* plotTopLegend(char* label,Float_t x=-1,Float_t y=-1,Float_t size=0.06,Int_t color=1,Float_t angle=0.0)
{
	// coordinates in NDC!
	// plots the string label in position x and y in NDC coordinates
	// size is the text size
	// color is the text color

	if(x<0||y<0)
	{   // defaults
		x=gPad->GetLeftMargin()*1.15;
		y=(1-gPad->GetTopMargin())*1.04;
	}
	TLatex* text=new TLatex(x,y,label);
	text->SetTextSize(size);
	text->SetNDC();
	text->SetTextColor(color);
	text->SetTextAngle(angle);
	text->Draw();
	return text;
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
void Draw_Legend(TH1* File1Histo,TString File1HistoName,TH1* File2Histo,TString File2HistoName)
{
	TLegend *leg1=new TLegend(0.35,0.65,0.80,0.88);
	leg1->SetBorderSize(0);
	leg1->SetFillColor(0);
	leg1->AddEntry(File1Histo,File1HistoName,"L");
	leg1->AddEntry(File2Histo,File2HistoName,"L");
	leg1->Draw();
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
void Draw_Legend_V2(TH1* File1Histo,TString HistoName1,TH1* File2Histo,TString HistoName2,TH1* File3Histo,TString HistoName3,TH1* File4Histo,TString HistoName4)
{
	TLegend *leg1=new TLegend(0.35,0.78,0.85,0.88);
	leg1->SetBorderSize(0);
	leg1->SetTextSize(0.025);
	leg1->SetFillColor(0);
	leg1->AddEntry(File1Histo,HistoName1,"LP");
	leg1->AddEntry(File2Histo,HistoName2,"LP");
	leg1->AddEntry(File3Histo,HistoName3,"LP");
	leg1->AddEntry(File4Histo,HistoName4,"LP");
	leg1->Draw();
}

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
void PlotHorLine(Double_t x1_val, Double_t x2_val, Double_t y_val, Int_t Line_Col)
{
    TLine* Zero_line = new TLine();
    Zero_line -> SetX1(x1_val);
    Zero_line -> SetX2(x2_val);
    Zero_line -> SetY1(y_val);
    Zero_line -> SetY2(y_val);
    Zero_line -> SetLineWidth(2);
    Zero_line -> SetLineStyle(2);
    Zero_line -> SetLineColor(Line_Col);
    Zero_line -> Draw();
    //delete Zero_line;
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
void PlotVerLine(Double_t x_val, Double_t y_val_low, TH1* Histo, Double_t y_fac, Int_t Line_Col)
{
    TLine* Zero_line = new TLine();
    Zero_line -> SetX1(x_val);
    Zero_line -> SetX2(x_val);
    Zero_line -> SetY1(y_val_low);
    Zero_line -> SetY2(y_fac*Histo->GetBinContent(Histo->FindBin(x_val)));
    //cout << "x_val = " << x_val << ", Bin = " << Histo->FindBin(x_val) << ", Y2 = " << Histo->GetBinContent(Histo->FindBin(x_val)) << endl;
    Zero_line -> SetLineWidth(2);
    Zero_line -> SetLineStyle(1);
    Zero_line -> SetLineColor(Line_Col);
    Zero_line -> Draw();
    //delete Zero_line;
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
void PlotVerLine2(Double_t x_val, Double_t y_val_low, Double_t y_val_high, Int_t Line_Col)
{
    TLine* Zero_line = new TLine();
    Zero_line -> SetX1(x_val);
    Zero_line -> SetX2(x_val);
    Zero_line -> SetY1(y_val_low);
    Zero_line -> SetY2(y_val_high);
    //cout << "x_val = " << x_val << ", Bin = " << Histo->FindBin(x_val) << ", Y2 = " << Histo->GetBinContent(Histo->FindBin(x_val)) << endl;
    Zero_line -> SetLineWidth(2);
    Zero_line -> SetLineStyle(1);
    Zero_line -> SetLineColor(Line_Col);
    Zero_line -> Draw();
    //delete Zero_line;
}
