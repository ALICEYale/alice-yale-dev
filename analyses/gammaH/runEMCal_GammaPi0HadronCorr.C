// runEMCalJetAnalysisNew.C

class AliESDInputHandler;
class AliAODInputHandler;
class AliVEvent;
class AliAnalysisManager;
class AliPhysicsSelectionTask;
class AliCentralitySelectionTask;
class AliEmcalSetupTask;
class AliAnalysisGrid;

void LoadMacros();
void StartGridAnalysis(AliAnalysisManager* pMgr, const char* uniqueName, const char* cGridMode);
AliAnalysisGrid* CreateAlienHandler(const char* uniqueName, const char* gridDir, const char* gridMode, const char* runNumbers,
		const char* pattern, TString additionalCode, TString additionalHeaders, Int_t maxFilesPerWorker, Int_t workerTTL, Bool_t isMC);

//______________________________________________________________________________
AliAnalysisManager* runEMCal_GammaPi0HadronCorr(
		const char   *cDataType      = "AOD",                                   // set the analysis type, AOD or ESD
//		const char   *cRunPeriod     = "LHC13d",                                // set the run period
		const char   *cRunPeriod     = "LHC15o",                                // set the run period
		const char   *cLocalFiles    = "", 									   // set the local list file
		const UInt_t  iNumFiles      = 10,                                      // number of files analyzed locally
		//const UInt_t  iNumEvents     = 20000000,                                // number of events to be analyzed
		const UInt_t  iNumEvents     = 2800,                                 // full stat PbPb number of events to be analyzed
//		const UInt_t  iNumEvents     = 37,                                 // number of events to be analyzed
//		const UInt_t  kPhysSel       = (AliVEvent::kEMCEGA | AliVEvent::kAnyINT), // physics selection
		const UInt_t  kPhysSel       = AliVEvent::kEMCEGA + AliVEvent::kAnyINT, // physics selection
		//const UInt_t  kPhysSel       = AliVEvent::kEMCEGA,                      // physics selection  -- AliVEvent::kEMCEGA
		const char   *cTaskName      = "EMCal_GH_Ana",                            // sets name of analysis manager
	    const char   *obsolete       = "",                                        // Previous handled the ocdb settings, but obsolete due to CDBconnect task
		// 0 = only prepare the analysis manager but do not start the analysis
		// 1 = prepare the analysis manager and start the analysis
		// 2 = launch a grid analysis
		Int_t         iStartAnalysis = 1,
		const char   *cGridMode      = "test"
)
{
	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
	//
	//    Some pre-settings and constants
	//
	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
	TString sRunPeriod(cRunPeriod);
	sRunPeriod.ToLower();

	//..set the beamtype and the run2 flag
	Bool_t bIsRun2 = kFALSE;
	if (sRunPeriod.Length() == 6 && sRunPeriod.BeginsWith("lhc15")) bIsRun2 = kTRUE;

	AliAnalysisTaskEmcal::BeamType iBeamType = AliAnalysisTaskEmcal::kpp;
	if (sRunPeriod == "lhc10h" || sRunPeriod == "lhc11h" || sRunPeriod == "lhc15o")
	{
		iBeamType = AliAnalysisTaskEmcal::kAA;
	}
	else if (sRunPeriod == "lhc12g" || sRunPeriod == "lhc13b" || sRunPeriod == "lhc13c" ||
			 sRunPeriod == "lhc13d" || sRunPeriod == "lhc13e" || sRunPeriod == "lhc13f")
	{
		iBeamType = AliAnalysisTaskEmcal::kpA;
	}
	AliTrackContainer::SetDefTrackCutsPeriod(sRunPeriod);
	Printf("Default track cut period set to: %s", AliTrackContainer::GetDefTrackCutsPeriod().Data());

	//..specify settings for data type AOD || ESD
	enum eDataType { kAod, kEsd };
	eDataType iDataType;
	if (!strcmp(cDataType, "ESD"))
	{
		iDataType = kEsd;
	}
	else if (!strcmp(cDataType, "AOD"))
	{
		iDataType = kAod;
	}
	else
	{
		Printf("Incorrect data type option, check third argument of run macro.");
		Printf("datatype = AOD or ESD");
		return 0;
	}
	Printf("%s analysis chosen.", cDataType);

	//..load the local files
	if(cRunPeriod=="LHC13b")cLocalFiles = "/Users/Eliane/Software/Files/files_AOD13b_pPb.txt";
	if(cRunPeriod=="LHC13d")cLocalFiles = "/Users/Eliane/Software/Files/files_AOD13d_pPb.txt";
//	if(cRunPeriod=="LHC15o")cLocalFiles = "/Users/Eliane/Software/Files/files_MuonCalo15o_PbPb.txt"; //LHC15o
	if(cRunPeriod=="LHC15o")cLocalFiles = "/Users/Eliane/Software/Files/filesLHC15oRun246272.txt";   //LHC15o
	TString sLocalFiles(cLocalFiles);
	if (iStartAnalysis == 1)
	{
		if (sLocalFiles == "")
		{
			Printf("You need to provide the list of local files!");
			return 0;
		}
		Printf("Setting local analysis for %d files from list %s, max events = %d", iNumFiles, sLocalFiles.Data(), iNumEvents);
	}

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
	//
	//    Create the manager and set the handlers
	//    Set here which tasks should be executed with help of the bools
	//
	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	//..load necessary libraries
	gSystem->Load("/usr/local/lib/libCGAL");
    //..load all the necessary macros
	LoadMacros();

	//..create an analysis manager
	AliAnalysisManager* pMgr = new AliAnalysisManager(cTaskName);

	//..check type of input and create a handler for it
	if (iDataType == kAod)
	{
		AliAODInputHandler* pAODHandler = AddAODHandler();
	}
	else
	{
		AliESDInputHandler* pESDHandler = AddESDHandler();
	}

	TString RootFileName = "/Users/Eliane/Software/alice/ali-master/AliPhysics/PWGGA/EMCALTasks/macros/AnalysisResults_May24_perTrigger_MER_SavedPool_Try2.root";
	TFile* RootFile = TFile::Open(RootFileName);
	AliEventPoolManager* ExternalMgr = GetPoolMngFromFile(RootFile,"AliEventPoolManager",1);
	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
	//
	//    Add the tasks that sould be executed by the analyis manager
	//    The order matters!
	//    http://alidoc.cern.ch/AliPhysics/master/_r_e_a_d_m_eemcfw.html
	//
	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	//* * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	//..Physics selection task    [is this the right description? ->> (sorts out crappy events and makes a centrality selection, trigger selection)]
	if (iDataType == kEsd)
	{
		AliPhysicsSelectionTask *pPhysSelTask = AddTaskPhysicsSelection();
	}

	//* * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	//..Centrality task
	if (iDataType == kEsd && iBeamType != AliAnalysisTaskEmcal::kpp && bIsRun2 == kFALSE)
	{
		AliCentralitySelectionTask *pCentralityTask = AddTaskCentrality(kTRUE);
		pCentralityTask->SelectCollisionCandidates(AliVEvent::kAny);
	}

    //* * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    //..AliMultSelection
    //..Works for both pp and PbPb for the periods that it is calibrated
    if (bIsRun2 == kTRUE)//if(cRunPeriod=="LHC15o")  //
    	{
    		AliMultSelectionTask *pMultSelectionTask = AddTaskMultSelection(kFALSE);
    		pMultSelectionTask->SelectCollisionCandidates(AliVEvent::kAny);
    	}
	//* * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	//..CDBconnect task
    AliTaskCDBconnect *taskCDB = AddTaskCDBconnect();
    taskCDB->SetFallBackToRaw(kTRUE);

	//* * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	//..Correction task (BadChannel, time+energy calib., cluzterizer, had corr.)
	AliEmcalCorrectionTask* correctionTask = AddTaskEmcalCorrectionTask();
	correctionTask->SelectCollisionCandidates(kPhysSel);
	correctionTask->SetRunPeriod("");
	correctionTask->SetUserConfigurationFilename("userGH_Analysis_LHC13.yaml");
	if (bIsRun2)
	{
		correctionTask->SetUseNewCentralityEstimation(kTRUE);
	}
	correctionTask->Initialize();

	//* * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	//EE next task: gamma hadron task
//	AliAnalysisTaskGammaHadron *AnalysisTaskGammaHadron = AddTaskGammaHadron(0,1,8,7,AliVEvent::kEMCEGA,AliVEvent::kAnyINT,0.15,0.3,0,"usedefault","usedefault","AliAnalysisTask");
	AliAnalysisTaskGammaHadron *AnalysisTaskGammaHadron = AddTaskGammaHadron(0,1,8,7,AliVEvent::kAnyINT,AliVEvent::kAnyINT,0.15,0.3,0,"usedefault","usedefault","AliAnalysisTask");
	AnalysisTaskGammaHadron->SelectCollisionCandidates(kPhysSel);
	//AnalysisTaskGammaHadron->SetExternalEventPoolManager(ExternalMgr);


	//* * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
/*	AliAnalysisTaskCaloTrackCorrelation *Task = AddTaskCalorimeterQA("AnyINTnoBC",kFALSE,"",2015);
	Task->SelectCollisionCandidates(AliVEvent::kAnyINT);
	Task->GetAnalysisMaker()->GetCaloUtils()->SwitchOffBadChannelsRemoval();
*/
	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
	//
	//    Run the whole thing!
	//
	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
	TObjArray *pTopTasks = pMgr->GetTasks();
	for (Int_t i = 0; i < pTopTasks->GetEntries(); ++i)
	{
		AliAnalysisTaskSE *pTask = dynamic_cast<AliAnalysisTaskSE*>(pTopTasks->At(i));
		if (!pTask) continue;
		if (pTask->InheritsFrom("AliAnalysisTaskEmcal"))
		{
			AliAnalysisTaskEmcal *pTaskEmcal = static_cast<AliAnalysisTaskEmcal*>(pTask);
			Printf("Setting beam type %d for task %s", iBeamType, pTaskEmcal->GetName());
			pTaskEmcal->SetForceBeamType(iBeamType);
            if (bIsRun2) pTaskEmcal->SetUseNewCentralityEstimation(kTRUE);
		}
        if (pTask->InheritsFrom("AliEmcalCorrectionTask"))
        {
            AliEmcalCorrectionTask * pTaskEmcalCorrection = static_cast<AliEmcalCorrectionTask*>(pTask);
            Printf("Setting beam type %d for task %s", iBeamType, pTaskEmcalCorrection->GetName());
            pTaskEmcalCorrection->SetForceBeamType(iBeamType);
        }
	}

	if (!pMgr->InitAnalysis()) return 0;
	pMgr->PrintStatus();

	pMgr->SetUseProgressBar(1, 25);

	//.. what is this??
	TFile *pOutFile = new TFile("train.root","RECREATE");
	pOutFile->cd();
	pMgr->Write();
	pOutFile->Close();
	delete pOutFile;

	//..start local analysis
	if (iStartAnalysis == 1)
	{
		TChain* pChain = 0;
		if (iDataType == kAod)
		{
			gROOT->LoadMacro("$ALICE_PHYSICS/PWG/EMCAL/macros/CreateAODChain.C");
			pChain = CreateAODChain(sLocalFiles.Data(), iNumFiles, 0, kFALSE);
		}
		else
		{
			gROOT->LoadMacro("$ALICE_PHYSICS/PWG/EMCAL/macros/CreateESDChain.C");
			pChain = CreateESDChain(sLocalFiles.Data(), iNumFiles, 0, kFALSE);
		}

		// start analysis
		Printf("Starting Analysis...");

		//TEST IF YOUR CLASS RUNS ON THE TRAIN (ONCE BEFORE CREATING A WAGON)
		Bool_t TestTheCodeForTheTrain=0;
		if(TestTheCodeForTheTrain==1)
		{
			//Version to test the code:
			// Store the data to a file
			// In a second step open the file and do mgr->StartAnalysis
			TString OutputfileName="TestForTheTrain.root";
			cout<<"Outputfile: "<<OutputfileName<<endl;
			TFile *Outputfile = new TFile(OutputfileName,"RECREATE");
			Outputfile->cd();
			pMgr->Write();
		}
		else
		{
			//pMgr->StartAnalysis("local", pChain, iNumEvents);
			pMgr->StartAnalysis("local", pChain,iNumEvents); //over all events
		}
	}
	else if (iStartAnalysis == 2)
	{  // start grid analysis
		StartGridAnalysis(pMgr, cTaskName, cGridMode);
	}

	return pMgr;
}
//______________________________________________________________________________
AliEventPoolManager* GetPoolMngFromFile(TFile* RootFile, TString Name,Int_t Option)
{
	TList *dfFull;
	TString TListName;
	if(Option==0)TListName="AliAnalysisTask_GH_SE_tracks_caloClusters_histos";
	if(Option==1)TListName="AliAnalysisTask_GH_ME_tracks_caloClusters_histos";

	TList* FinalList    =(TList*)RootFile->Get(TListName);

	//if(!dfFull) cout << "Cannot find List with name: "<<Name<<endl;
	AliEventPoolManager *Mngr=(AliEventPoolManager*)FinalList->FindObject(Name);
	//Hist->SetName(Name);

	return Mngr;
}
//______________________________________________________________________________________________________
void LoadMacros()
{
	// Aliroot macros
	gROOT->LoadMacro("$ALICE_ROOT/ANALYSIS/macros/train/AddAODHandler.C");
	gROOT->LoadMacro("$ALICE_ROOT/ANALYSIS/macros/train/AddESDHandler.C");
	gROOT->LoadMacro("$ALICE_PHYSICS/OADB/macros/AddTaskCentrality.C");
	gROOT->LoadMacro("$ALICE_PHYSICS/OADB/macros/AddTaskPhysicsSelection.C");
	gROOT->LoadMacro("$ALICE_PHYSICS/PWGPP/PilotTrain/AddTaskCDBconnect.C");
	gROOT->LoadMacro("$ALICE_PHYSICS/OADB/COMMON/MULTIPLICITY/macros/AddTaskMultSelection.C");
	gROOT->LoadMacro("$ALICE_PHYSICS/PWG/EMCAL/macros/AddTaskEmcalCorrectionTask.C");
	gROOT->LoadMacro("$ALICE_PHYSICS/PWG/EMCAL/macros/AddTaskEmcalCopyCollection.C"); //delete
	gROOT->LoadMacro("$ALICE_PHYSICS/PWG/EMCAL/macros/AddTaskEMCALTender.C");  //
	gROOT->LoadMacro("$ALICE_PHYSICS/PWG/EMCAL/macros/AddTaskClusterizerFast.C");  //
	gROOT->LoadMacro("$ALICE_PHYSICS/PWG/EMCAL/macros/AddTaskEmcalClusterMaker.C");  //
	gROOT->LoadMacro("$ALICE_PHYSICS/PWG/EMCAL/macros/AddTaskEmcalClusTrackMatcher.C");  //
	gROOT->LoadMacro("$ALICE_PHYSICS/PWG/EMCAL/macros/AddTaskHadCorr.C");  //
	gROOT->LoadMacro("$ALICE_PHYSICS/PWGJE/EMCALJetTasks/macros/AddTaskEmcalJet.C");
	gROOT->LoadMacro("$ALICE_PHYSICS/PWGJE/EMCALJetTasks/macros/AddTaskEmcalJetSample.C");
	gROOT->LoadMacro("$ALICE_PHYSICS/PWGJE/EMCALJetTasks/macros/AddTaskRhoNew.C");
	gROOT->LoadMacro("$ALICE_PHYSICS/PWGGA/EMCALTasks/macros/AddTaskGammaHadron.C");
	gROOT->LoadMacro("$ALICE_PHYSICS/PWGGA/CaloTrackCorrelations/macros/QA/AddTaskCalorimeterQA.C");
}

//______________________________________________________________________________________________________
void StartGridAnalysis(AliAnalysisManager* pMgr, const char* uniqueName, const char* cGridMode)
{
	Int_t maxFilesPerWorker = 4;
	Int_t workerTTL = 7200;
	const char* runNumbers = "180720";
	const char* pattern = "pass2/AOD/*/AliAOD.root";
	const char* gridDir = "/alice/data/2012/LHC12c";
	const char* additionalCXXs = "";
	const char* additionalHs = "";

	AliAnalysisGrid *plugin = CreateAlienHandler(uniqueName, gridDir, cGridMode, runNumbers, pattern, additionalCXXs, additionalHs, maxFilesPerWorker, workerTTL, kFALSE);
	pMgr->SetGridHandler(plugin);

	// start analysis
	Printf("Starting GRID Analysis...");
	pMgr->SetDebugLevel(0);
	pMgr->StartAnalysis("grid");
}

//______________________________________________________________________________________________________
AliAnalysisGrid* CreateAlienHandler(const char* uniqueName, const char* gridDir, const char* gridMode, const char* runNumbers,
		const char* pattern, TString additionalCode, TString additionalHeaders, Int_t maxFilesPerWorker, Int_t workerTTL, Bool_t isMC)
{
	TDatime currentTime;
	TString tmpName(uniqueName);

	// Only add current date and time when not in terminate mode! In this case the exact name has to be supplied by the user
	if (strcmp(gridMode, "terminate")) {
		tmpName += "_";
		tmpName += currentTime.GetDate();
		tmpName += "_";
		tmpName += currentTime.GetTime();
	}

	TString macroName("");
	TString execName("");
	TString jdlName("");
	macroName = Form("%s.C", tmpName.Data());
	execName = Form("%s.sh", tmpName.Data());
	jdlName = Form("%s.jdl", tmpName.Data());

	AliAnalysisAlien *plugin = new AliAnalysisAlien();
	plugin->SetOverwriteMode();
	plugin->SetRunMode(gridMode);

	// Here you can set the (Ali)PHYSICS version you want to use
	plugin->SetAliPhysicsVersion("vAN-20160203-1");

	plugin->SetGridDataDir(gridDir); // e.g. "/alice/sim/LHC10a6"
	plugin->SetDataPattern(pattern); //dir structure in run directory

	if (!isMC) plugin->SetRunPrefix("000");

	plugin->AddRunList(runNumbers);

	plugin->SetGridWorkingDir(Form("work/%s",tmpName.Data()));
	plugin->SetGridOutputDir("output"); // In this case will be $HOME/work/output

	plugin->SetAnalysisSource(additionalCode.Data());

	plugin->SetDefaultOutputs(kTRUE);
	plugin->SetAnalysisMacro(macroName.Data());
	plugin->SetSplitMaxInputFileNumber(maxFilesPerWorker);
	plugin->SetExecutable(execName.Data());
	plugin->SetTTL(workerTTL);
	plugin->SetInputFormat("xml-single");
	plugin->SetJDLName(jdlName.Data());
	plugin->SetPrice(1);
	plugin->SetSplitMode("se");

	// merging via jdl
	plugin->SetMergeViaJDL(kTRUE);
	plugin->SetOneStageMerging(kFALSE);
	plugin->SetMaxMergeStages(2);

	return plugin;
}
