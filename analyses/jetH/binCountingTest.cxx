// To ensure that the bin correct is applied correctly

#include <vector>
#include <numeric>

#include <TH1F.h>
#include <TH2F.h>
#include <THnSparse.h>
#include <TString.h>

void fillHist(TH1F * hist, TH2F * correctionHist, Double_t fillValue, Double_t weight = 1.0);
void fillHist(THnSparse * hist, TH2F * correctionHist, Double_t *fillValue, Double_t weight = 1.0);
void accessSetOfYBinValues(TH2F * hist, Int_t xBin, std::vector <Double_t> & yBinsContent, Double_t scaleFactor = -1);
void normalizeHist(TH2F * hist);

void binCountingTest()
{
  TH2F * test = new TH2F("binTest", "binTest", 10, 0, 10, 10, 0, 20);

  //test->FillRandom("gaus", 10000);
  test->Fill(3.5,0.5);
  test->Fill(3.5,1.5);

  // Global bin tests
  Int_t binX = test->GetXaxis()->FindBin(3.5);
  Int_t binY = test->GetYaxis()->FindBin(0.5);
  Printf("(4,1): %d", test->GetBin(4,1));
  Printf("(4,1) -> (%i,%i)", binX, binY);

  Double_t content = test->GetBinContent(test->GetBin(4,1));
  Printf("Content(4,1): %f", content);

  // Starts at 1 because first bin number starts at 1
  // <= since it goes through nbins
  Double_t xValue = 3.5;

  Int_t xBin = test->GetXaxis()->FindBin(xValue);

  Printf("Before normalization");
  std::vector <Double_t> yBinsContent;
  accessSetOfYBinValues(test, xBin, yBinsContent);
  Printf("Sum: %f", std::accumulate(yBinsContent.begin(), yBinsContent.end(), 0.0));
  yBinsContent.clear();

  normalizeHist(test);

  Printf("After normalization");
  accessSetOfYBinValues(test, xBin, yBinsContent);
  Printf("Sum: %f", std::accumulate(yBinsContent.begin(), yBinsContent.end(), 0.0));
  yBinsContent.clear();

  TH1F * test2 = new TH1F("test2", "test2", 5, 0, 20);
  // Only need to work on 1D hist
  Double_t fillValue = 3.7;
  fillHist(test2, test, fillValue);
  Printf("Integral: %f", test2->Integral());
}

void fillHist(TH1F * hist, TH2F * correctionHist, Double_t fillValue, Double_t weight)
{
  // Determine where to get the values in the correction hist
  Int_t xBin = correctionHist->GetXaxis()->FindBin(fillValue);

  std::vector <Double_t> yBinsContent;
  accessSetOfYBinValues(correctionHist, xBin, yBinsContent);

  // Loop over all possible bins to contribute.
  // If content is 0 then calling Fill won't make a difference
  for (Int_t index = 1; index <= correctionHist->GetYaxis()->GetNbins(); index++)
  {
    // Determine the value to fill based on the center of the bins.
    // This in principle allows the binning between the correction and hist to be different
    Double_t fillLocation = correctionHist->GetYaxis()->GetBinCenter(index); 
    Printf("fillLocation: %f, weight: %f", fillLocation, yBinsContent.at(index-1));
    // minus 1 since loop starts at 1
    hist->Fill(fillLocation, weight*yBinsContent.at(index-1));
  }

  //TEMP
  hist->Draw();
  //END TEMP
}

void fillHist(THnSparse * hist, TH2F * correctionHist, Double_t *fillValue, Double_t weight)
{
  // Jet pt is always located in the second position
  Double_t jetPt = fillValue[1];

  // Determine where to get the values in the correction hist
  Int_t xBin = correctionHist->GetXaxis()->FindBin(jetPt);

  std::vector <Double_t> yBinsContent;
  accessSetOfYBinValues(correctionHist, xBin, yBinsContent);

  // Loop over all possible bins to contribute.
  // If content is 0 then calling Fill won't make a difference
  for (Int_t index = 1; index <= correctionHist->GetYaxis()->GetNbins(); index++)
  {
    // Determine the value to fill based on the center of the bins.
    // This in principle allows the binning between the correction and hist to be different
    fillValue[1]  = correctionHist->GetYaxis()->GetBinCenter(index); 
    Printf("fillValue[1]: %f, weight: %f", fillValue[1], yBinsContent.at(index-1));
    // minus 1 since loop starts at 1
    hist->Fill(fillValue, weight*yBinsContent.at(index-1));
  }

}

void accessSetOfYBinValues(TH2F * hist, Int_t xBin, std::vector <Double_t> & yBinsContent, Double_t scaleFactor)
{
  for (Int_t index = 1; index <= hist->GetYaxis()->GetNbins(); index++)
  {
    //yBinsContent[index-1] = hist->GetBinContent(hist->GetBin(xBin,index));
    yBinsContent.push_back(hist->GetBinContent(hist->GetBin(xBin,index)));

    if (scaleFactor >= 0)
    {
      // -1 since index starts at 1
      hist->SetBinContent(hist->GetBin(xBin,index), yBinsContent.at(index-1)/scaleFactor);
    }
  }
}

void normalizeHist(TH2F * hist)
{
  std::vector <Double_t> yBinsContent;
  Int_t sum = 0;
  for (Int_t index = 1; index <= hist->GetXaxis()->GetNbins(); index++)
  {
    // Access bins
    accessSetOfYBinValues(hist, index, yBinsContent);
    sum = std::accumulate(yBinsContent.begin(), yBinsContent.end(), 0.0);

    // normalization by sum
    accessSetOfYBinValues(hist, index, yBinsContent, sum);

    // Reset for next loop
    yBinsContent.clear();
    sum = 0;
  }

}
