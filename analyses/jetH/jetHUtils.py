#!/usr/bin/env python

# Utilities for the jet-hadron anaylsis
#
# Author: Raymond Ehlers <raymond.ehlers@cern.ch>, Yale University
# Date: 13 Jan 2017

# Bins
# eta is absolute value!
etaBins = [0, 0.4, 0.8, 1.5]
# NOTE: 8-20 will likely be only 8-10 because of how the THnSparse is defined
#trackPtBins = [0, 0.5, 1, 2, 3, 5, 8, 20]
trackPtBins = [0.15, 0.5, 1, 2, 3, 5, 8, 10]
# These are a more useful range
jetPtBins = [15, 20, 60, 200]
# These are from the task
#jetPtBins = [15, 20, 25, 30, 60, 200]
# From: https://arxiv.org/pdf/1405.4632v2.pdf
v2Cent00_05Values = [0.0125, 0.03, 0.04, 0.07, 0.07, 0.07, 0.06]
v2Cent05_10Values = [ 0.015, 0.04, 0.075, 0.1, 0.101, 0.101, 0.101]

def generateJetPtRangeString(iJetPtBin, latexCommands = False):
    """ Generate string to describe jet pt ranges. """
    if latexCommands:
        ptDescription = "{\\pTJet}"
        leftSign = "$\\leq"
        #rightSign = "\\textless$"
        rightSign = "<$"
    else:
        ptDescription = "p_{T}^{jet}"
        leftSign = "<="
        rightSign = "<"
    jetPtRange = "{0:.1f}{1}{2}".format(jetPtBins[iJetPtBin], leftSign, ptDescription)
    # -2 because we have 3 jet pt bins, which will be index 0, 1, 2, but there are 4 values in jetPtBins
    if not iJetPtBin == len(jetPtBins)-2:
        jetPtRange += "{0}{1:.1f}".format(rightSign, jetPtBins[iJetPtBin+1])
    elif latexCommands:
        jetPtRange += "$"

    # Add GeV/c label
    if latexCommands:
        jetPtRange += " {\\gevc}"
    else:
        jetPtRange += " GeV/c"

    return jetPtRange

def generateTrackPtRangeString(iTrackPtBin, latexCommands = False):
    """ Generate string to describe track pt ranges. """
    if latexCommands:
        ptDescription = "{\\pTAssoc}"
        leftSign = "$\\leq"
        #rightSign = "\\textless$"
        rightSign = "<$"
    else:
        ptDescription = "p_{T}^{assoc}"
        leftSign = "<="
        rightSign = "<"
    # TODO: Fix when this is the track pt bin edge 0.15. Right now it gets truncated!
    trackPtRange = "{0:.1f}{1}{2}{3}{4:.1f}".format(trackPtBins[iTrackPtBin], leftSign, ptDescription, rightSign, trackPtBins[iTrackPtBin+1])

    # Add GeV/c label
    if latexCommands:
        trackPtRange += " {\\gevc}"
    else:
        trackPtRange += " GeV/c"

    return trackPtRange
