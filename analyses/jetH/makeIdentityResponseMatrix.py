#!/usr/bin/env python

# Simple script to make an identity matrix to test reweighting with a response matrix

from builtins import range

import os
import numpy
import ROOT

def makeIdentityMatrix():
    matrix = ROOT.TH2F("embeddingCorrection", "embeddingCorrection", 250, 0, 250, 250, 0, 250)

    # 249.5 because linspace is inclusive!
    for i in numpy.linspace(0.5, 249.5, 250):
        # Full weight for a normal test
        matrix.Fill(i, i)
        # Half weight for testing purposes
        #matrix.Fill(i, i, 0.5)

    canvas = ROOT.TCanvas("canvas", "canvas")
    matrix.Draw("colz")
    canvas.SaveAs(os.path.join("awaySide", "identityMatrix.pdf"))

    # Write the matrix out
    fOut = ROOT.TFile(os.path.join("awaySide", "identityMatrix.root"), "UPDATE")
    matrix.Write()
    fOut.Close()

if __name__ == "__main__":
    makeIdentityMatrix()
