#!/usr/bin/env python

import os
import collections

import ROOT

def getListOfHistograms(filename, listTaskName = "AliAnalysisTaskJetH_tracks_caloClusters_clusbias5R2GA"):
    """ Get histograms from the file and make them available in a dict """
    fIn = ROOT.TFile(filename)
    if not fIn or fIn.IsZombie():
        print("Could not find file '{0}".format(filename))
        return
    taskOutputList = fIn.Get(listTaskName)
    if not taskOutputList:
        print("Could not find list '{0}'".format(taskOutputList))
        return
    #list = listTask.FindObject(listName)
    #if not list:
    #    print("Could not find list '{0}'".format(listName))
    #    return
    
    histList = dict()
    next = ROOT.TIter(taskOutputList)
    obj = next()
    while obj:
        histList[obj.GetName()] = obj
        obj = next()
    
    return histList

def plotEmbeddingExamplesData():
    # Filename
    filename = os.path.expanduser("~/code/alice/jetH/train936/results/AnalysisResults.root")
    #filename = os.path.expanduser("~/code/alice/yaleDev/analyses/jetH/responseMatrix/pp/embeddingCorrection.root")
    # Get hists
    hists = getListOfHistograms(filename, "AliJetResponseMaker_Jet_AKTFullR020_PicoTracks_pT3000_CaloClustersCorr_ET3000_pt_scheme_Jet_AKTFullR020_MCSelectedParticles_pT3000_pt_scheme_Bias0_BiasType1_EMCAL_Cent0_10_histos")

    #print(hists)

    # THnSparse with matched jets
    matchedJets = hists["fHistMatching"]

    # General
    # Matching dist
    matchedJets.GetAxis(4).SetRangeUser(0.00001,0.05)

    # Embedded level
    # Embedded level jet pt
    matchedJets.GetAxis(0).SetRangeUser(0,150)
    # Embedded level jet area
    #matchedJets.GetAxis(2).SetRangeUser(0.00001,areacut); 
    # Embedded level lead particle pt or cluster
    matchedJets.GetAxis(7).SetRangeUser(5.9999,100.0)

    # Particle level
    # Particle level jet pt
    # Only restricting here to set a reasonable range for viewing
    matchedJets.GetAxis(1).SetRangeUser(0,150)
    # Particle level lead particle pt or cluster
    #matchedJets.GetAxis(8).SetRangeUser(5.9999,100.0);

    # Get the matrix
    matrix = matchedJets.Projection(1, 0)

    canvas = ROOT.TCanvas("temp", "temp")
    canvas.SetLogz()
    matrix.Draw("colz")
    matrix.SetTitle("Matching truth and det level jets")
    matrix.GetXaxis().SetTitle("p_{T}^{Det}")
    matrix.GetYaxis().SetTitle("p_{T}^{Truth}")
    matrix.SetStats(False)
    canvas.SaveAs(os.path.join("output", "embeddingExampleFigures", "matrix.pdf"))

    detLevel = collections.OrderedDict()
    for ptBinValue in range(30, 33):
        ptBin = matrix.GetXaxis().FindBin(ptBinValue)
        ptYProjection = matrix.ProjectionY("pt%0.fYProjection" % ptBinValue, ptBin, ptBin+1)

        # Normalize
        ptYProjection.Scale(1.0/ptYProjection.Integral())
        # Titles
        ptYProjection.SetTitle("Truth level jet p_{T} for detector level jets")
        ptYProjection.GetYaxis().SetTitle("Probability")
        ptYProjection.GetXaxis().SetRangeUser(0, 70)

        # Save in dict
        detLevel[ptBinValue] = ptYProjection

    #print(detLevel)
    drawResult(detLevel)

    # drawSelections
    drawSelections(detLevel[30], pt = 30)

def drawSelections(hist, pt):
    # Draw result
    canvas = ROOT.TCanvas("canvas", "canvas")
    canvas.SetLogy()

    hist.SetLineColor(ROOT.kRed +2)
    hist.SetTitle("Truth level jet p_{{T}} distribution for {0} GeV/c detector level jet".format(pt))
    hist.Draw()

    selections = [10, 20, 30, 40, 50, 80]

    clones = []
    # NOTE: Need underflow bins to equal 1!
    #print("integral: {0}".format(hist.Integral(0, hist.GetNbinsX()+1)))
    #val = 0
    leg = ROOT.TLegend(0.12, 0.55, 0.35, 0.87)
    for iSelectionBin in range(0, len(selections)-1):
        # TODO: Integrate these values to get some sense of it
        histClone = hist.Clone("{0}_{1}".format(hist.GetName(), iSelectionBin))
        print("Setting range: {0}-{1}".format(selections[iSelectionBin], selections[iSelectionBin+1]))
        histClone.GetXaxis().SetRangeUser(selections[iSelectionBin], selections[iSelectionBin+1])
        histClone.SetFillColorAlpha(colors[iSelectionBin], .30)
        histClone.Draw("same")
        histClone.SetDirectory(0)
        leg.AddEntry(histClone, "{0}<=p_{{T,jet}}^{{Truth}}<{1} Weight: {2:.3f}".format(selections[iSelectionBin], selections[iSelectionBin+1], histClone.Integral()), "F")
        #print("cloneIntegral: {0}".format(histClone.Integral()))
        #val = val + histClone.Integral()
        clones.append(histClone)

        # TEMP
        # Save canvas
        canvas.SaveAs(os.path.join("output", "embeddingExampleFigures", "truthPtSelections_SelectionBin{0}.pdf".format(iSelectionBin)))

    #print(val)

    leg.SetBorderSize(0)
    leg.SetFillColorAlpha(0, 0)
    leg.SetTextSize(0.025)
    leg.Draw("same")

    # Save canvas
    canvas.SaveAs(os.path.join("output", "embeddingExampleFigures", "truthPtSelections.pdf"))

def drawResult(inputDict):
    # Draw result
    canvas = ROOT.TCanvas("canvas", "canvas")
    canvas.SetLogy()

    # Create legend
    leg = ROOT.TLegend(0.65, 0.65, 0.85, 0.87);
    leg.SetFillColorAlpha(0, 0)

    drawFlag = False
    for (label, hist), color in zip(inputDict.iteritems(), colors):
        # Set hist options
        hist.SetStats(False)
        hist.SetLineColor(color)
        hist.SetMarkerColor(color)
        hist.SetLineWidth(2)

        hist.GetXaxis().SetTitle("p_{T}^{truth} (GeV/c)")

        # Add legend entry
        leg.AddEntry(hist, "p_{{T,jet}}^{{det}} = {0:.1f}".format(label))

        # Draw
        if not drawFlag:
            hist.Draw()
            drawFlag = True
        else:
            hist.Draw("same")

    leg.SetBorderSize(0)
    #leg.SetTextSize(1.1)
    leg.Draw("same")

    # Save canvas
    canvas.SaveAs(os.path.join("output", "embeddingExampleFigures", "detectorPtExample.pdf"))

    # Reset line width
    for hist in inputDict.values():
        hist.SetLineWidth(1)

    # Draw one on it's own
    (label, hist) = inputDict.iteritems().next()
    hist.SetLineColor(colors[0])

    canvas.Clear()
    canvas.SetLogy()
    hist.Draw()

    # Draw a line represnting the 30 GeV det level jet
    canvas.Update()
    canvas.Modified()
    # GetUymax() doesn't work on log scale....
    #line = ROOT.TLine(30, canvas.GetUymin(), 30, canvas.GetUymax())
    #line = ROOT.TLine(30, canvas.GetUymin(), 30, 1)
    line = drawVerticalLine(30)

    # Save canvas
    canvas.SaveAs(os.path.join("output", "embeddingExampleFigures", "singleDetectorBin.pdf"))

def drawVerticalLine(xValue):
    """ Handle drawing a vertical line on a log scale. (It doesn't work correctly using normal functions...)
    
    See: https://root.cern.ch/phpBB3/viewtopic.php?t=10745"""
    line = ROOT.TLine()
    lm = ROOT.gPad.GetLeftMargin()
    rm = 1. - ROOT.gPad.GetRightMargin()
    tm = 1. - ROOT.gPad.GetTopMargin()
    bm = ROOT.gPad.GetBottomMargin()
    xndc = (rm-lm)*((xValue-ROOT.gPad.GetUxmin())/(ROOT.gPad.GetUxmax()-ROOT.gPad.GetUxmin()))+lm
    line.SetLineWidth(2)
    line.SetLineColor(ROOT.kBlack)

    line.DrawLineNDC(xndc,bm,xndc,tm)

    ROOT.SetOwnership(line, False)

    return line

def plotEmbeddingExamples():
    functions = collections.OrderedDict()
    #func = ROOT.TF1(name, "TMath::Gaus(x, {0}, {1}, true)".format(30, 0.00000001), 0, 50)
    #functions["detector"] = func

    for i in range(0,3):
        name = "detector{0}".format(i)
        func = ROOT.TF1(name, "TMath::Gaus(x, {0}, {1}, true)".format(30+i, 3), 0, 50)
        functions[name] = func

    drawResult(functions)

colors = [ROOT.kRed + 2, ROOT.kBlue + 2, ROOT.kGreen + 2, ROOT.kMagenta + 2, ROOT.kBlack]

if __name__ == "__main__":
    # Setup output area
    if not os.path.exists(os.path.join("output", "embeddingExampleFigures")):
        os.makedirs(os.path.join("output", "embeddingExampleFigures"))

    # Naive example
    #plotEmbeddingExamples()

    # Example with data
    plotEmbeddingExmaplesData()
