{


  //THnSparse // Creates a TTree and fills it with the coordinates of all
  // filled bins. The tree will have one branch for each dimension,
  // and one for the bin content.


  for (int i = 1; i<=9; i++) { //nine pt-hard bins

    TString filename = "ptHard%d/AnalysisResults.root";
    Printf(filename.Data());
    filename = TString::Format(filename.Data(), i);
    Printf("Filename: %s", filename.Data());
    /*TString filename = "/AnalysisResults_146805_";
    char buffer[10]; 
    sprintf (buffer, "%d", i);
    filename = buffer + filename;
    filename += buffer;
    filename += ".root";*/

    TFile* f = new TFile(filename);
    TList* tlist = (TList*)f->Get("AliJetResponseMaker_detLevel_AKTFullR020_tracks_pT3000_caloClusters_E3000_pt_scheme_truthLevel_AKTFullR020_mcparticles_pT0150_pt_scheme_Bias0_BiasType0_EMCAL_histos");
    THnSparse* h = (THnSparse*)(tlist->At(15));

    TProfile* hweights = (TProfile*)( tlist->At(5) );
    Float_t pthardweight = hweights->GetMaximum();

    TProfile* hevents = (TProfile*)( tlist->At(4) );
    Float_t nevents = (Float_t)hevents->GetBinContent(i+1); //first bin is 0 hard pt
    cout<<"nevents = "<<nevents<<endl;

    Int_t dim = h->GetNdimensions();
    TString name(h->GetName()); name += "_tree";
    TString title(h->GetTitle()); title += " tree";


    TString fileoutname = "response_tree_";
    fileoutname += i;
    fileoutname += ".root";
    //char buffer[10]; 
    //sprintf (buffer, "%d", i);
    //filename = buffer + filename;
    TFile* fout = new TFile(fileoutname,"recreate");

    TTree* tree = new TTree(name, title);
    Double_t* x = new Double_t[dim + 1];
    memset(x, 0, sizeof(Double_t) * (dim + 1));

    TString branchname;
    for (Int_t d = 0; d < dim; ++d) {
      if (branchname.Length())
        branchname += ":";
      TAxis* axis = h->GetAxis(d);
      branchname += axis->GetName();
      branchname += "/D";
    }
    tree->Branch("coord", x, branchname);
    tree->Branch("bincontent", &x[dim], "bincontent/D");
    tree->Branch("pthardweight",&pthardweight,"pthardweight/F");
    tree->Branch("nevents",&nevents,"nevents/F");

    Int_t *bins = new Int_t[dim];
    for (Long64_t kk = 0; kk < h->GetNbins(); ++kk) {
      x[dim] = h->GetBinContent(kk, bins);
      for (Int_t d = 0; d < dim; ++d) {
        x[d] = h->GetAxis(d)->GetBinCenter(bins[d]);
      }
      tree->Fill();
    }

    fout->cd();

    //hzg1zg2->Write();
    //hist_exp_unfolded->Write();

    fout->Write();
    fout->Close(); 


  }//iter over hard pt bins
}
