/// \file runPPMatchign.C
/// \brief PP Matching run macro
///
/// \ingroup EMCALJETFW
/// PP matching run macro to create a response matrix
///
/// \author Raymond Ehlers <raymond.ehlers@cern.ch>, Yale University
/// \date Dec 15, 2016

class AliESDInputHandler;
class AliAODInputHandler;
class AliVEvent;
class AliAnalysisManager;
class AliPhysicsSelectionTask;
class AliCentralitySelectionTask;
class AliAnalysisGrid;

void LoadMacros();
void StartGridAnalysis(AliAnalysisManager* pMgr, const char* uniqueName, const char* cGridMode);
AliAnalysisGrid* CreateAlienHandler(const char* uniqueName, const char* gridDir, const char* gridMode, const char* runNumbers,
    const char* pattern, TString additionalCode, TString additionalHeaders, Int_t maxFilesPerWorker, Int_t workerTTL, Bool_t isMC);

//______________________________________________________________________________
AliAnalysisManager* runPPMatching(
    const char   *cDataType      = "AOD",                                   // set the analysis type, AOD or ESD
    const char   *cRunPeriod     = "LHC15g1a",                                // set the run period
    const char   *cLocalFiles    = "aodFiles.txt",                          // set the local list file
    const UInt_t  iNumEvents     = 1000,                                    // number of events to be analyzed
    const UInt_t  kPhysSel       = 0, //AliVEvent::kAny,                         // physics selection
    const char   *cTaskName      = "PPMatching",                     // sets name of analysis manager
    /*
     * Job names for LHC15g1a:
     * ptHard1_20161228_15359
     * 2_20161228_20613
     * 3_20161228_21020
     * 4_20161228_21329
     * 5_20161228_21636
     * 6_20161228_21923
     * 7_20161228_22247
     * 8_20161228_22612
     * 9_20161228_23032
     */
    // 0 = only prepare the analysis manager but do not start the analysis
    // 1 = prepare the analysis manager and start the analysis
    // 2 = launch a grid analysis
    Int_t         iStartAnalysis = 1,
    const UInt_t  iNumFiles      = 5,                                     // number of files analyzed locally
    const char   *cGridMode      = "full"
)
{
  // Setup period
  TString sRunPeriod(cRunPeriod);
  sRunPeriod.ToLower();

  // Set Run 2
  Bool_t bIsRun2 = kFALSE;
  if (sRunPeriod.Length() == 6 && (sRunPeriod.BeginsWith("lhc15") || sRunPeriod.BeginsWith("lhc16"))) bIsRun2 = kTRUE;

  // Set beam type
  AliAnalysisTaskEmcal::BeamType iBeamType = AliAnalysisTaskEmcal::kpp;
  if (sRunPeriod == "lhc10h" || sRunPeriod == "lhc11h" || sRunPeriod == "lhc15o") {
    iBeamType = AliAnalysisTaskEmcal::kAA;
  }
  else if (sRunPeriod == "lhc12g" || sRunPeriod == "lhc13b" || sRunPeriod == "lhc13c" ||
      sRunPeriod == "lhc13d" || sRunPeriod == "lhc13e" || sRunPeriod == "lhc13f") {
    iBeamType = AliAnalysisTaskEmcal::kpA;
  }

  // Ghost area
  Double_t kGhostArea = 0.01;
  if (iBeamType != AliAnalysisTaskEmcal::kpp) kGhostArea = 0.005;

  // Setup track container
  AliTrackContainer::SetDefTrackCutsPeriod(sRunPeriod);
  Printf("Default track cut period set to: %s", AliTrackContainer::GetDefTrackCutsPeriod().Data());

  TString newFrameworkTracks = "tracks";
  TString newFrameworkClusters = "caloClusters";
  TString newFrameworkCells = "emcalCells";

  // Set data file type
  enum eDataType { kAod, kEsd };

  eDataType iDataType;
  if (!strcmp(cDataType, "ESD")) {
    iDataType = kEsd;
  }
  else if (!strcmp(cDataType, "AOD")) {
    iDataType = kAod;
  }
  else {
    Printf("Incorrect data type option, check third argument of run macro.");
    Printf("datatype = AOD or ESD");
    return 0;
  }

  Printf("%s analysis chosen.", cDataType);

  TString sLocalFiles(cLocalFiles);
  if (iStartAnalysis == 1) {
    if (sLocalFiles == "") {
      Printf("You need to provide the list of local files!");
      return 0;
    }
    Printf("Setting local analysis for %d files from list %s, max events = %d", iNumFiles, sLocalFiles.Data(), iNumEvents);
  }

  // Load macros needed for the analysis
  LoadMacros();

  // Analysis manager
  AliAnalysisManager* pMgr = new AliAnalysisManager(cTaskName);

  if (iDataType == kAod) {
    AliAODInputHandler* pAODHandler = AddAODHandler();
  }
  else {  
    AliESDInputHandler* pESDHandler = AddESDHandler();
  }

  // Physics selection task
  if (iDataType == kEsd) {
    AliPhysicsSelectionTask *pPhysSelTask = AddTaskPhysicsSelection();
  }

  // Centrality task
  // The Run 2 condition is too restrictive, but until the switch to MultSelection is complete, it is the best we can do
  if (iDataType == kEsd && iBeamType != AliAnalysisTaskEmcal::kpp && bIsRun2 == kFALSE) {
    AliCentralitySelectionTask *pCentralityTask = AddTaskCentrality(kTRUE);
    pCentralityTask->SelectCollisionCandidates(AliVEvent::kAny);
  }
  // AliMultSelection
  // Works for both pp and PbPb for the periods that it is calibrated
  if (bIsRun2 == kTRUE) {
    AliMultSelection * pMultSelectionTask = AddTaskMultSelection();
    pMultSelectionTask->SelectCollisionCandidates(AliVEvent::kAny);
  }

  // CDBconnect task
  AliTaskCDBconnect *taskCDB = AddTaskCDBconnect();
  taskCDB->SetFallBackToRaw(kTRUE);

  // EMCal corrections
  // NOTE: kDebug = 4. Can go up or down from there!
  //AliLog::SetClassDebugLevel("AliEmcalCorrectionTask", AliLog::kDebug-2);
  AliEmcalCorrectionTask * correctionTask = AliEmcalCorrectionTask::AddTaskEmcalCorrectionTask();
  correctionTask->SelectCollisionCandidates(kPhysSel);
  correctionTask->SetRunPeriod(sRunPeriod.Data());
  // Local configuration
  correctionTask->SetUserConfigurationFilename("userConfiguration.yaml");
  // Grid configuration
  //correctionTask->SetUserConfigurationFilename("alien:///alice/cern.ch/user/r/rehlersi/AliEmcalCorrectionConfigurationTestComparison.yaml");
  correctionTask->Initialize();    

  // Jet finding
  const AliJetContainer::EJetAlgo_t jetAlgorithm = AliJetContainer::antikt_algorithm;
  const Double_t jetRadius = 0.2;
  const AliJetContainer::EJetType_t jetType = AliJetContainer::kFullJet;
  const Double_t minTrackPt = 0.15;//3.0;
  const Double_t minClusterPt = 0.30;//3.0;
  const AliJetContainer::ERecoScheme_t recoScheme = AliJetContainer::pt_scheme;
  const char * label = "Jet";
  const Double_t minJetPt = 1; // TEMP: Consider at 1! Usually: 0
  const Bool_t lockTask = kTRUE;
  const Bool_t fillGhosts = kFALSE;

  // Do not pass clusters if we are only looking at charged jets
  if (jetType == AliJetContainer::kChargedJet) {
    newFrameworkClusters = "";
  }

  AliEmcalJetTask * pFullJetTaskTruthLevelNew = 0;
  AliEmcalJetTask * pFullJetTaskNew = 0;
  ///////
  // Truth level PYTHIA jet finding
  ///////
  pFullJetTaskTruthLevelNew = AliEmcalJetTask::AddTaskEmcalJet("mcparticles", "",
          jetAlgorithm, jetRadius, jetType, minTrackPt, minClusterPt, kGhostArea, recoScheme, "truthLevel", minJetPt, lockTask, fillGhosts);
  pFullJetTaskTruthLevelNew->SelectCollisionCandidates(kPhysSel);

  ///////
  // Det level jet finding
  ///////
  //set 3 GeV both for tracks and clusters
  pFullJetTaskNew = AliEmcalJetTask::AddTaskEmcalJet(newFrameworkTracks.Data(), newFrameworkClusters.Data(),
          jetAlgorithm, jetRadius, jetType, 3.0, 3.0, kGhostArea, recoScheme, "detLevel", minJetPt, lockTask, fillGhosts);
  pFullJetTaskNew->SelectCollisionCandidates(kPhysSel);
  if (jetType != AliJetContainer::kChargedJet) {
    pFullJetTaskNew->GetClusterContainer(0)->SetDefaultClusterEnergy(AliVCluster::kHadCorr);
  }

  // TEMP
  //pMgr->AddClassDebug("AliEmcalJetTask", AliLog::kDebug+2);
  
  //////////////////////////////////////////
  // Jet Tasks
  //////////////////////////////////////////

  AliEmcalJet::JetAcceptanceType acceptanceType = AliEmcalJet::kEMCALfid;
  TString jetTag = "Jet";
  AliAnalysisTaskEmcalJetSample * sampleTaskTruthLevelNew = 0;
  AliAnalysisTaskEmcalJetSample * sampleTaskNew = 0;
  ///////
  // Truth level PYTHIA sample task
  ///////
  sampleTaskTruthLevelNew = AddTaskEmcalJetSample("mcparticles", "", "", "truthLevel");

  AliParticleContainer * partCont = sampleTaskTruthLevelNew->GetParticleContainer(0);
  partCont->SetParticlePtCut(0.15);
  sampleTaskTruthLevelNew->SetHistoBins(600, 0, 300);
  sampleTaskTruthLevelNew->SelectCollisionCandidates(kPhysSel);

  AliJetContainer* jetCont02 = sampleTaskTruthLevelNew->AddJetContainer(jetType, jetAlgorithm, recoScheme, jetRadius, acceptanceType, "truthLevel");

  ///////
  // Det level sample task
  ///////
  sampleTaskNew = AddTaskEmcalJetSample(newFrameworkTracks.Data(), newFrameworkClusters.Data(), newFrameworkCells.Data(), "detLevel");

  if (jetType != AliJetContainer::kChargedJet) {
    sampleTaskNew->GetClusterContainer(0)->SetClusECut(0.);
    sampleTaskNew->GetClusterContainer(0)->SetClusPtCut(0.);
    sampleTaskNew->GetClusterContainer(0)->SetClusNonLinCorrEnergyCut(0.);
    sampleTaskNew->GetClusterContainer(0)->SetClusHadCorrEnergyCut(0.30);
    sampleTaskNew->GetClusterContainer(0)->SetDefaultClusterEnergy(AliVCluster::kHadCorr);
  }
  sampleTaskNew->GetParticleContainer(0)->SetParticlePtCut(0.15);
  sampleTaskNew->SetHistoBins(600, 0, 300);
  sampleTaskNew->SelectCollisionCandidates(kPhysSel);

  // Set the name collection name using the task name due to the cluster and track constituent cuts
  // Otherwise, we would need to create particle and cluster containers to get the proper names...
  AliJetContainer* jetCont02 = sampleTaskNew->AddJetContainer(pFullJetTaskNew->GetName());

  // EMCal jet tagger
  // From documentation: http://alidoc.cern.ch/AliPhysics/master/_r_e_a_d_m_eembedding.html
  // Guessing at settings...?
  /*AliAnalysisTaskEmcalJetTagger * jetTag = AddTaskEmcalJetTagger(pFullJetTaskTruthLevelNew->GetName(), pFullJetTaskDetLevelNew->GetName(), jetRadius,
                  "", "", newFrameworkTracks.Data(), newFrameworkClusters.Data(), "TPC", "V0M", kPhysSel);
  jetTag->SetNCentBins(1);
  jetTag->SetJetTaggingType(AliAnalysisTaskEmcalJetTagger::kClosest);
  jetTag->SetMinFractionShared(0.5);*/

  // Jet Reponse Maker
  /*AliTrackContainer * trackCont = new AliTrackContainer(newFrameworkTracks.Data());
  AliClusterContainer * clusCont = new AliClusterContainer(newFrameworkClusters.Data());
  clusCont->SetClusECut(minClusterPt);
  TString detLevelJetName = AliJetContainer::GenerateJetName(jetType, jetAlgorithm, recoScheme, jetRadius, trackCont, clusCont, "detLevel");*/
  // Can also use the code above if you can't access the previous jet task (as in, on the lego train)
  TString detLevelJetName = pFullJetTaskNew->GetName();
  TString mcTracksName = "mcparticles";
  /*AliMCParticleContainer * mcPartCont = new AliMCParticleContainer(mcTracksName.Data());
  TString partLevelJetName = AliJetContainer::GenerateJetName(jetType, jetAlgorithm, recoScheme, jetRadius, mcPartCont, 0, "truthLevel");*/
  // Can also use the code above if you can't access the previous jet task (as in, on the lego train)
  TString partLevelJetName = pFullJetTaskTruthLevelNew->GetName();
  //std::cout << "partLevelJetName:  " << partLevelJetName << std::endl << "partLevelJetName2: " << partLevelJetName2 << std::endl;
  AliJetResponseMaker::MatchingType matchingType = AliJetResponseMaker::kGeometrical;
  const Double_t kJetPtCut            = 1.;
  const Double_t kJetAreaCut          = 0;
  // Since this applies equally to both Jet Containers and we are comparing against MC truth,
  // it is probably best to disable this and just use any bias applied at the jet finder level!
  Double_t kJetLeadingTrackBias = 0.0;      // Previously was 0! Should consider what we need here, but perhaps not huge since the jets are already biased.
  Int_t kLeadHadType = 0;                 // 0 = charged, 1 = neutral, 2 = both
  Double_t kMaxGeoDistance02 = 1.2;       // 02 corresponds to R=0.2 jets
  Int_t kNcent = 1;                       // TODO: Check this value!
  Int_t kHistoType = 1;                   // 1 = THnSparse, 0 = TH1/TH2/TH3
  AliJetResponseMaker * jetResponseMatrix = AddTaskJetResponseMaker(newFrameworkTracks, newFrameworkClusters, detLevelJetName, "", 0.2,
                                  mcTracksName, "", partLevelJetName, "", 0.2,
                                  kJetPtCut, kJetAreaCut, kJetLeadingTrackBias, kLeadHadType,
                                  matchingType, kMaxGeoDistance02, kMaxGeoDistance02, "EMCAL", // Cut type
                                  -999,-999,-999);
                                  //-999,-999,kNcent);
  //pMgr->AddClassDebug("AliEmcalJetTask", AliLog::kDebug+2);
  //pMgr->AddClassDebug("AliJetContainer", AliLog::kDebug+7);
  //pMgr->AddClassDebug("AliAnalysisTaskEmcalJetSample", AliLog::kDebug+2);
  //pMgr->AddClassDebug("AliClusterContainer", AliLog::kDebug+2);
  //pMgr->AddClassDebug("AliJetResponseMaker", AliLog::kDebug+2);
  // Ensure that they jets are in the EMCal
  jetResponseMatrix->GetJetContainer(0)->SetJetAcceptanceType(acceptanceType);
  jetResponseMatrix->GetJetContainer(1)->SetJetAcceptanceType(acceptanceType);
  // Set neutrals as the leading particle on the detector level
  jetResponseMatrix->GetJetContainer(0)->SetLeadingHadronType(1);

  // TEMP cluster type test
  //jetResponseMatrix->GetJetContainer(0)->GetClusterContainer()->SetDefaultClusterEnergy(AliVCluster::kHadCorr);
  // ENDTEMP

  // TEMP
  //jetResponseMatrix->GetJetContainer(0)->PrintCuts();
  //jetResponseMatrix->GetJetContainer(1)->PrintCuts();
  //jetResponseMatrix->GetJetContainer(0)->SetMinClusterPt(6); //neutral on the detector level
  // ENDTEMP

  // Sets bias on clusters (but also makes the leading hadron function return leading clusters)
  // __Don't__ set the min cluster pt!
  // Enable EP option (not yet commited as of 7 Dec)
  //jetResponseMatrix->SetJetRelativeEPAngleAxis(kTRUE);

  jetResponseMatrix->SelectCollisionCandidates(kPhysSel);
  jetResponseMatrix->SetHistoType(kHistoType);

  TObjArray *pTopTasks = pMgr->GetTasks();
  for (Int_t i = 0; i < pTopTasks->GetEntries(); ++i) {
    AliAnalysisTaskSE *pTask = dynamic_cast<AliAnalysisTaskSE*>(pTopTasks->At(i));
    if (!pTask) continue;
    if (pTask->InheritsFrom("AliAnalysisTaskEmcal")) {
      AliAnalysisTaskEmcal *pTaskEmcal = static_cast<AliAnalysisTaskEmcal*>(pTask);
      Printf("Setting beam type %d for task %s", iBeamType, pTaskEmcal->GetName());
      pTaskEmcal->SetForceBeamType(iBeamType);
    }
    if (pTask->InheritsFrom("AliEmcalCorrectionTask")) {
      AliEmcalCorrectionTask * pTaskEmcalCorrection = static_cast<AliEmcalCorrectionTask*>(pTask);
      Printf("Setting beam type %d for task %s", iBeamType, pTaskEmcalCorrection->GetName());
      pTaskEmcalCorrection->SetForceBeamType(iBeamType);
    }
  }

  if (!pMgr->InitAnalysis()) return 0;
  pMgr->PrintStatus();
    
  pMgr->SetUseProgressBar(kTRUE, 250);
  
  TFile *pOutFile = new TFile("train.root","RECREATE");
  pOutFile->cd();
  pMgr->Write();
  pOutFile->Close();
  delete pOutFile;

  if (iStartAnalysis == 1) { // start local analysis
    TChain* pChain = 0;
    if (iDataType == kAod) {
      gROOT->LoadMacro("$ALICE_PHYSICS/PWG/EMCAL/macros/CreateAODChain.C");
      pChain = CreateAODChain(sLocalFiles.Data(), iNumFiles, 0, kFALSE);
    }
    else {
      gROOT->LoadMacro("$ALICE_PHYSICS/PWG/EMCAL/macros/CreateESDChain.C");
      pChain = CreateESDChain(sLocalFiles.Data(), iNumFiles, 0, kFALSE);
    }

    // start analysis
    Printf("Starting Analysis...");
    pMgr->StartAnalysis("local", pChain, iNumEvents);
  }
  else if (iStartAnalysis == 2) {  // start grid analysis
    StartGridAnalysis(pMgr, cTaskName, cGridMode);
  }

  return pMgr;
}

void LoadMacros()
{
  // Aliroot macros
  gROOT->LoadMacro("$ALICE_ROOT/ANALYSIS/macros/train/AddAODHandler.C");
  gROOT->LoadMacro("$ALICE_ROOT/ANALYSIS/macros/train/AddESDHandler.C");
  gROOT->LoadMacro("$ALICE_PHYSICS/OADB/macros/AddTaskCentrality.C");
  gROOT->LoadMacro("$ALICE_PHYSICS/OADB/COMMON/MULTIPLICITY/macros/AddTaskMultSelection.C");
  gROOT->LoadMacro("$ALICE_PHYSICS/OADB/macros/AddTaskPhysicsSelection.C");
  gROOT->LoadMacro("$ALICE_PHYSICS/PWGPP/PilotTrain/AddTaskCDBconnect.C");
  gROOT->LoadMacro("$ALICE_PHYSICS/PWGJE/EMCALJetTasks/macros/AddTaskEmcalJetSample.C");
  gROOT->LoadMacro("$ALICE_PHYSICS/PWGJE/EMCALJetTasks/macros/AddTaskJetResponseMaker.C");
  gROOT->LoadMacro("$ALICE_PHYSICS/PWGJE/EMCALJetTasks/macros/AddTaskJetRespPtHard.C");
  gROOT->LoadMacro("$ALICE_PHYSICS/PWGJE/EMCALJetTasks/macros/AddTaskEmcalJetTagger.C");
}

void StartGridAnalysis(AliAnalysisManager* pMgr, const char* uniqueName, const char* cGridMode)
{
  Int_t maxFilesPerWorker = 4;
  Int_t workerTTL = 7200;
  // /alice/sim/2015/LHC15g1a/4/146805/AOD/363
  // Max is 376
  const char* runNumbers = "146805";
  // This will run approximately 111 files (1, 10-19, 100-199)
  const char* pattern = "AOD/1*/AliAOD.root";
  const char* gridDir = "/alice/sim/2015/LHC15g1a/1";
  const char* additionalCXXs = "";
  const char* additionalHs = "";

  AliAnalysisGrid *plugin = CreateAlienHandler(uniqueName, gridDir, cGridMode, runNumbers, pattern, additionalCXXs, additionalHs, maxFilesPerWorker, workerTTL, kTRUE);
  pMgr->SetGridHandler(plugin);

  // start analysis
  Printf("Starting GRID Analysis...");
  pMgr->SetDebugLevel(0);
  pMgr->StartAnalysis("grid");
}

AliAnalysisGrid* CreateAlienHandler(const char* uniqueName, const char* gridDir, const char* gridMode, const char* runNumbers,
    const char* pattern, TString additionalCode, TString additionalHeaders, Int_t maxFilesPerWorker, Int_t workerTTL, Bool_t isMC)
{
  TDatime currentTime;
  TString tmpName(uniqueName);

  // Only add current date and time when not in terminate mode! In this case the exact name has to be supplied by the user
  if (strcmp(gridMode, "terminate")) {
    tmpName += "_";
    tmpName += currentTime.GetDate();
    tmpName += "_";
    tmpName += currentTime.GetTime();
  }

  TString macroName("");
  TString execName("");
  TString jdlName("");
  macroName = Form("%s.C", tmpName.Data());
  execName = Form("%s.sh", tmpName.Data());
  jdlName = Form("%s.jdl", tmpName.Data());

  AliAnalysisAlien *plugin = new AliAnalysisAlien();
  plugin->SetOverwriteMode();
  plugin->SetRunMode(gridMode);

  // Here you can set the (Ali)PHYSICS version you want to use
  plugin->SetAliPhysicsVersion("vAN-20161214-1");
  //plugin->SetAliPhysicsVersion("v5-08-19-01-emcalEmbedding-1");

  plugin->SetGridDataDir(gridDir); // e.g. "/alice/sim/LHC10a6"
  plugin->SetDataPattern(pattern); //dir structure in run directory

  if (!isMC) plugin->SetRunPrefix("000");

  plugin->AddRunList(runNumbers);

  plugin->SetGridWorkingDir(Form("work/%s",tmpName.Data()));
  plugin->SetGridOutputDir("output"); // In this case will be $HOME/work/output

  plugin->SetAnalysisSource(additionalCode.Data());

  plugin->SetDefaultOutputs(kTRUE);
  plugin->SetAnalysisMacro(macroName.Data());
  plugin->SetSplitMaxInputFileNumber(maxFilesPerWorker);
  plugin->SetExecutable(execName.Data());
  plugin->SetTTL(workerTTL);
  plugin->SetInputFormat("xml-single");
  plugin->SetJDLName(jdlName.Data());
  plugin->SetPrice(1);
  plugin->SetSplitMode("se");

  // Description from Redmer:
  // merging via jdl: run with kTRUE to merge on grid
  // after re-running the jobs in SetRunMode("terminate")
  // mode, set SetMergeViaJDL(kFALSE)
  // to collect final results
  //
  // I think this means:
  // Run with kTRUE when running the analysis to get it to run on the grid.
  // Then, run with kTRUE  and terminate to merge on the grid
  // Tuen, run with kFALSE and terminate to retrieve the AnalysisResults.root file!
  plugin->SetMergeViaJDL(kTRUE);
  plugin->SetOneStageMerging(kFALSE);
  plugin->SetMaxMergeStages(2);

  return plugin;
}
