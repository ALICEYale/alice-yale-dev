#include <TFile.h>
#include <THnSparse.h>
#include <TROOT.h>
#include <TH1.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TStyle.h>
#include <TAxis.h>
#include <TSystem.h>
#include <TString.h>
#include <TCanvas.h>
#include <iostream>

void responseMatrix()
{
  // Turn off stats box
  gStyle->SetOptStat(0);

  TString inputBasePath = TString::Format("%s/code/alice/jetH", gSystem->Getenv("HOME"));
  TString outputBasePath = TString::Format("%s/code/alice/yaleDev/analyses/jetH/output/responseMatrix", gSystem->Getenv("HOME"));
  Printf("Input base path: %s\nOutput base path: %s", inputBasePath.Data(), outputBasePath.Data());

  // Pt Hard Scaling
  // Different values from the train page:
  // Double_t kPtHardBinsScaling[11] = {0.000000E+00, 5.206101E-05, 5.859497E-06, 4.444755E-07, 4.344664E-08, 5.154750E-09, 6.956634E-10, 1.149828E-10, 2.520137E-11, 6.222240E-12, 2.255832E-12}
  double ptHardScaling[11] = {0};
  ptHardScaling [0]  = 0;
  ptHardScaling [1]  = 5.135193e-05;
  ptHardScaling [2]  = 5.859497e-06;
  ptHardScaling [3]  = 4.444755e-07;
  ptHardScaling [4]  = 4.293118e-08;
  ptHardScaling [5]  = 5.154750e-09;
  ptHardScaling [6]  = 6.958612e-10;
  ptHardScaling [7]  = 1.149828e-10;
  ptHardScaling [8]  = 2.520137e-11;
  ptHardScaling [9]  = 6.222240e-12;
  ptHardScaling [10] = 2.255832e-12;

  // Centrality
  int minCent = 0;
  int maxCent = 10;

  // Define area cut
  double areacut=3.14*0.2*0.2*0.6;

  // Define the object we are interested in
  TString objectName = "AliJetResponseMaker_Jet_AKTFullR020_PicoTracks_pT3000_CaloClustersCorr_ET3000_pt_scheme_Jet_AKTFullR020_MCSelectedParticles_pT3000_pt_scheme_Bias0_BiasType1_EMCAL_Cent%i_%i_histos";

  // Filename
  TString filename = TString::Format("%s/train%s/results/AnalysisResults.root", inputBasePath.Data(), "%i");

  TH2D * responseMatrices[2] = { 0, 0};
  TH1D * jetPt1[2] = {0, 0};
  TH1D * jetPt2[2] = {0, 0};

  TCanvas * canvas = new TCanvas();
  canvas->SetLogz();

  // Loop over pt hard bins. Currently only have 4,5
  for (Int_t i = 4; i < 6; i++)
  {
    int trainLabel = 0;
    if (i == 4) { trainLabel = 936; }
    if (i == 5) { trainLabel = 939; }
    // Open and retrieve results
    TFile *finAA = TFile::Open(TString::Format(filename.Data(), trainLabel));
    TList* lAA = (TList*)gROOT->FindObject(TString::Format(objectName.Data(), minCent, maxCent));

    // Get nEvents
    TH1* hZvtx = (TH1*)lAA->FindObject("fHistZVertex");
    double nEvents = hZvtx->GetEntries();

    // Retrive THnSparse
    THnSparse* AAmatches = (THnSparse*)lAA->FindObject("fHistMatching");

    // General
    // Matching dist
    AAmatches->GetAxis(4)->SetRangeUser(0.00001,0.05); 

    // Embedded level
    // Embedded level jet pt
    AAmatches->GetAxis(0)->SetRangeUser(0,150); 
    // Embedded level jet area
    //AAmatches->GetAxis(2)->SetRangeUser(0.00001,areacut); 
    // Embedded level lead particle pt or cluster
    AAmatches->GetAxis(7)->SetRangeUser(5.9999,100.0);

    // Particle level
    // Particle level jet pt
    // Only restricting here to set a reasonable range for viewing
    AAmatches->GetAxis(1)->SetRangeUser(0,150);
    // Particle level lead particle pt or cluster
    //AAmatches->GetAxis(8)->SetRangeUser(5.9999,100.0);

    // Get the response matrix
    TH2D * matrix = (TH2D *) AAmatches->Projection(1, 0);

    // Unclear whether this is necessary
    matrix->Rebin2D(2,2);
    matrix->Scale(ptHardScaling[i]);
    matrix->SetTitle(TString::Format("Truth vs Det jet p_{T} for ptHardBin %i for %i-%i%%", i, minCent, maxCent));
    matrix->GetXaxis()->SetTitle("Jet p_{T}^{det}");
    matrix->GetYaxis()->SetTitle("Jet p_{T}^{truth}");
    matrix->Draw("colz");
    //std::cout << "i: " << i << " nEntries: " << matrix->GetEntries() << std::endl;
    canvas->Print(TString::Format("%s/matrix%i_cent%i-%i.pdf", outputBasePath.Data(), i-4, minCent, maxCent));

    responseMatrices[i-4] = matrix;
    responseMatrices[i-4]->SetDirectory(0);

    // Jet Spectra
    THnSparse * fHistJets1 = (THnSparse *) lAA->FindObject("fHistJets1");
    THnSparse * fHistJets2 = (THnSparse *) lAA->FindObject("fHistJets2");

    // Pt1 = det
    TH1D * jetPt1Temp = (TH1D *) fHistJets1->Projection(2);
    jetPt1Temp->GetXaxis()->SetRangeUser(0,70);
    // Enable logy
    canvas->SetLogy();
    jetPt1Temp->Draw();
    jetPt1Temp->SetTitle(TString::Format("Det level jet p_{T} for ptHardBin %i", i));
    jetPt1Temp->GetXaxis()->SetTitle("p_{T}^{det}");
    canvas->Print(TString::Format("%s/detLevelJetPtUnscaled%i_cent%i-%i.pdf", outputBasePath.Data(), i-4, minCent, maxCent));
    jetPt1Temp->SetTitle(TString::Format("ptHardBin scaled Det level jet p_{T} for ptHardBin %i",i));
    jetPt1Temp->Scale(ptHardScaling[i]);
    jetPt1Temp->Draw();
    canvas->Print(TString::Format("%s/detLevelJetPtScaled%i_cent%i-%i.pdf", outputBasePath.Data(), i-4, minCent, maxCent));
    // Disable logy
    canvas->SetLogy(kFALSE);

    jetPt1[i-4] = jetPt1Temp;
    jetPt1[i-4]->SetDirectory(0);

    // Pt2 = truth (part)
    TH1D * jetPt2Temp = (TH1D *) fHistJets2->Projection(2);
    // Enable logy
    jetPt2Temp->GetXaxis()->SetRangeUser(0,70);
    canvas->SetLogy();
    jetPt2Temp->Draw();
    jetPt2Temp->SetTitle(TString::Format("Truth level jet p_{T} for ptHardBin %i", i));
    jetPt2Temp->GetXaxis()->SetTitle("p_{T}^{truth}");
    canvas->Print(TString::Format("%s/partLevelJetPtUnscaled%i_cent%i-%i.pdf", outputBasePath.Data(), i-4, minCent, maxCent));
    jetPt1Temp->SetTitle(TString::Format("ptHardBin scaled Truth level jet p_{T} for ptHardBin %i",i));
    jetPt2Temp->Scale(ptHardScaling[i]);
    jetPt2Temp->Draw();
    canvas->Print(TString::Format("%s/partLevelJetPtScaled%i_cent%i-%i.pdf", outputBasePath.Data(), i-4, minCent, maxCent));
    // Disable logy
    canvas->SetLogy(kFALSE);

    jetPt2[i-4] = jetPt2Temp;
    jetPt2[i-4]->SetDirectory(0);

    finAA->Close();
  }

  // Response matrix
  responseMatrices[0]->Add(responseMatrices[1]);
  //std::cout << "all: nEntries: " << responseMatrices[0]->GetEntries() << std::endl;

  responseMatrices[0]->SetTitle(TString::Format("Truth vs Det jet p_{T} for %i-%i%%", minCent, maxCent));
  responseMatrices[0]->GetXaxis()->SetTitle("Jet p_{T}^{det}");
  responseMatrices[0]->GetYaxis()->SetTitle("Jet p_{T}^{truth}");
  responseMatrices[0]->Draw("colz");
  canvas->Print(TString::Format("%s/matrixCombined_cent%i-%i.pdf", outputBasePath.Data(), minCent, maxCent));

  // Examples
  std::vector <double> ptExampleBins = {30.0, 50.0};
  for (auto ptBinValue : ptExampleBins)
  {
    Int_t ptBin = responseMatrices[0]->GetXaxis()->FindBin(ptBinValue);
    // Tests for responseMatrix.py shows that it must be only one bin to select that one, since the upper
    // bin is inclusive
    TH1D * ptYProjection = responseMatrices[0]->ProjectionY(TString::Format("pt%0.fYProjection", ptBinValue), ptBin, ptBin);
    //TH1D * ptYProjection = responseMatrices[0]->ProjectionY(TString::Format("pt%0.fYProjection", ptBinValue), ptBin, ptBin+1);
    // Normalize to probability
    ptYProjection->Scale(1.0/ptYProjection->Integral());
    // Titles
    ptYProjection->SetTitle(TString::Format("Truth level jet p_{T} for %0.f GeV det level jet", ptBinValue));
    ptYProjection->GetYaxis()->SetTitle("Probability");
    ptYProjection->Draw();
    canvas->Print(TString::Format("%s/pt%0.fYProjection.pdf", outputBasePath.Data(), ptBinValue));
  }

  // Jet Spectra
  canvas->SetLogy();
  TH1D * tempJetPt1 = dynamic_cast<TH1D *>(jetPt1[0]->Clone("tempJetP1Combined"));
  tempJetPt1->SetTitle("Combined Det level jet p_{T}");
  tempJetPt1->Add(jetPt1[1]);
  tempJetPt1->Draw();
  // Plot the individual pt hard bins
  jetPt1[0]->SetLineColor(kBlack);
  jetPt1[0]->Draw("same");
  jetPt1[1]->SetLineColor(kRed);
  jetPt1[1]->Draw("same");
  canvas->Print(TString::Format("%s/detLevelJetPtCombined_cent%i-%i.pdf", outputBasePath.Data(), minCent, maxCent));
  TH1D * tempJetPt2 = dynamic_cast<TH1D *>(jetPt2[0]->Clone("tempJetP2Combined"));
  tempJetPt2->SetTitle("Combined Truth level jet p_{T}");
  tempJetPt2->Add(jetPt2[1]);
  tempJetPt2->Draw();
  // Plot the individual pt hard bins
  jetPt2[0]->SetLineColor(kBlack);
  jetPt2[0]->Draw("same");
  jetPt2[1]->SetLineColor(kRed);
  jetPt2[1]->Draw("same");
  canvas->Print(TString::Format("%s/partLevelJetPtCombined_cent%i-%i.pdf", outputBasePath.Data(), minCent, maxCent));

}
