#!/usr/bin/env python

# Create the response matrix, with proper scaling by pt hard bins
#
# Author: Raymond Ehlers <raymond.ehlers@cern.ch>, Yale University
# Date: 29 Dec 2016

from builtins import range

import os
import sys

import ROOT
try:
    from aliceYaleDevUtils import *
except ImportError:
    print("ERROR: Cannot import aliceYaleDevUtils. Please install it!\n")
# Setup color schemes
plottingUtils.colorSchemes.init()

import IPython

# Disable stats
ROOT.gStyle.SetOptStat(False)

def createResponseMatrix(clusterBias, createProductionRootFile):
    """ Create the response matrix

    """
    # NOTE: Refactor is needed substantially!!
    # Values are from: https://twiki.cern.ch/twiki/bin/viewauth/ALICE/JetMCProductionsCrossSections#LHC15g1a_Jet_Jet_events_pp_colli
    # Only used bins 1-9
    scaleFactors = [
                    0,                  # Offset so that we can count from 1!
                    4.43629,            # 1 5-11
                    0.49523,            # 2 11-21   
                    0.0394921,          # 3 21-36   
                    0.00383174,         # 4 36-57   
                    0.000446559,        # 5 57-84   
                    6.37374e-05,        # 6 84-117  
                    1.03134e-05,        # 7 117-152 
                    2.27012e-06,        # 8 152-191 
                    7.59281e-07,        # 9 >191    
                   ]

    # Using TH2Fs
    floatSwitch = True

    #####################################
    # Setup hists we are interested in
    #####################################

    # Jet Spectra
    jetSpectra = ROOT.TH1D("jetSpectra", "jetSpectra", 600, 0, 300)
    jetSpectraPtHard = []

    # Response matrix
    responseMatrix = ROOT.TH2F("embeddingCorrection_Clus{0}".format(clusterBias), "embeddingCorrection_Clus{0}".format(clusterBias), 250, 0, 250, 250, 0, 250)
    responseMatrixPtHard = []

    # Response matrix errors (only for the fully merged response matrix)
    responseMatrixErrors = ROOT.TH2F("responseMatrixErrors", "Response matrix relative errors",
                                      250, 0, 250, 250, 0, 250)

    # nEvents
    nEvents = []

    canvas = ROOT.TCanvas("canvas", "canvas")

    #####################################
    # Make hists available
    #####################################

    # +1 for starting from 1 and +1 for ensuring that we get the last bin
    # since range does not include the upper bound
    for ptHardBin in range(1, len(scaleFactors)):
        print("Processing pt hard bin {0}".format(ptHardBin))
        # Get the response maker
        filename = os.path.join("ppMatching", "ptHard{0}", "AnalysisResults.root")
        responseMakerHists = fileUtils.GetListOfHistograms(filename.format(ptHardBin), "AliJetResponseMaker_detLevel_AKTFullR020_tracks_pT3000_caloClusters_E3000_pt_scheme_truthLevel_AKTFullR020_mcparticles_pT0150_pt_scheme_Bias0_BiasType0_EMCAL_histos")

        # Get the particle level sample task (to check the jet spectra)
        sampleTaskParticleLevel = fileUtils.GetListOfHistograms(filename.format(ptHardBin), "AliAnalysisTaskEmcalJetSample_mcparticles_truthLevel_histos")
        # Get the sample task (to check the jet spectra)
        sampleTaskDetLevel = fileUtils.GetListOfHistograms(filename.format(ptHardBin), "AliAnalysisTaskEmcalJetSample_tracks_caloClusters_emcalCells_detLevel_histos")

        # Get N events
        nEvents.append(responseMakerHists["fHistZVertex"].GetEntries())
        
        # Jet spectra from Sample Task
        jetHists = sampleTaskDetLevel["detLevel_AKTFullR020_tracks_pT3000_caloClusters_E3000_pt_scheme"]
        spectra = jetHists.FindObject("histJetPt_0")
        spectra.SetName(spectra.GetName().replace("0","{}".format(ptHardBin)))
        jetSpectraPtHard.append(spectra)

        # Response matrix from Response Maker
        matching = responseMakerHists["fHistMatching"]
        # Set relevant cuts on THnSparse
        # TODO: Consider cut on matching distance?
        matching.GetAxis(7).SetRangeUser(clusterBias, matching.GetAxis(7).GetXmax())
        # Project
        matrix = matching.Projection(1, 0)
        matrix.SetName("{0}_ptHard{1}".format(matrix.GetName(), ptHardBin))
        responseMatrixPtHard.append(matrix)

    #####################################
    # Processing
    #####################################
    for i in range(0, len(jetSpectraPtHard)):
        # Check for outliers
        removeOutliers(jetSpectraPtHard[i])
        removeOutliers(responseMatrixPtHard[i])

        # Calculate scale factor
        # NOTE: +1 is necessary since scaleFactors is 1-indexed
        scaleFactor = scaleFactors[i+1]/nEvents[i]

        # Scale hists
        jetSpectraPtHard[i].Scale(scaleFactor)
        responseMatrixPtHard[i].Scale(scaleFactor)

        # Add to full hists
        jetSpectra.Add(jetSpectraPtHard[i])
        responseMatrix.Add(responseMatrixPtHard[i])

    # Normalize response matrix
    normalizeResponseMatrix(responseMatrix)

    # Check that the normalizaiton was successful
    checkNormalization(responseMatrix, floatSwitch)

    # Fill response matrix errors
    for x in range(responseMatrix.GetXaxis().GetFirst(), responseMatrix.GetXaxis().GetNbins() + 1):
        for y in range(responseMatrix.GetYaxis().GetFirst(), responseMatrix.GetYaxis().GetNbins() + 1):
            fillValue = responseMatrix.GetBinError(x, y)
            if responseMatrix.GetBinContent(x, y) > 0:
                fillValue = fillValue/responseMatrix.GetBinContent(x, y)
            else:
                if responseMatrix.GetBinError(x, y) > 0:
                    print("Warning: No bin content, but associated error is non-zero. Content: {0}, error: {1}".format(responseMatrix.GetBinContent(x, y), responseMatrix.GetBinError(x, y)))

            # Fill hist
            binNumber = responseMatrixErrors.Fill(responseMatrixErrors.GetXaxis().GetBinCenter(x), responseMatrixErrors.GetYaxis().GetBinCenter(y), fillValue)

            # Check to ensure that we filled where we expected
            if binNumber != responseMatrixErrors.GetBin(x, y):
                print("ERROR: Mismatch between fill bin number ({0}) and GetBin() ({1})".format(binNumber, responseMatrixErrors.GetBin(x, y)))

    #####################################
    # Plotting
    #####################################

    # Plot spectra
    canvas.SetLogy(1)
    plot1DPtHardHists(jetSpectra, jetSpectraPtHard, canvas)
    canvas.SetLogy(0)

    # Plot response matrix
    canvas.SetLogz(1)
    responseMatrix.Draw("colz")
    canvas.SaveAs("responseMatrix.pdf")
    canvas.SetLogz(0)

    # Plot response matrix errors
    responseMatrixErrors.Draw("colz")
    canvas.SaveAs("responseMatrixErrors.pdf")

    #####################################
    # Write out hists
    #####################################
    fOut = ROOT.TFile("embeddingCorrection.root", "UPDATE")
    responseMatrix.Write()
    if not createProductionRootFile:
        jetSpectra.Write()
        for hist in jetSpectraPtHard + responseMatrixPtHard:
            hist.Write()
        responseMatrixErrors.Write()
    fOut.Close()

def accessSetOfYBinValues(hist, xBin, yBinsContent, scaleFactor = -1):
    """ Access the y bins associated with and x bin

    Graphically, it looks like:

    a b c
    d e f
    g h i <- values (here and above)
    1 2 3 <- bin number

    For x bin 2, it would return values h, e, and b.
    """
    for index in range(1, hist.GetYaxis().GetNbins() + 1):
        yBinsContent.append(hist.GetBinContent(hist.GetBin(xBin, index)))

        if (scaleFactor >= 0):
            # -1 since index starts at 1
            hist.SetBinContent(hist.GetBin(xBin,index), yBinsContent[index-1]/scaleFactor)

def normalizeResponseMatrix(hist):
    """ Normalize response matrix

    For each given x bin (detector level pt), we take all associated y bins (truth leve), and normalize them to 1."""
    for index in range(1, hist.GetXaxis().GetNbins() + 1):
        yBinsContent = []
        norm = 0

        # Access bins
        accessSetOfYBinValues(hist, index, yBinsContent)
        norm = sum(yBinsContent)
        # NOTE: The upper bound on integrals is inclusive!
        ptYProjection = hist.ProjectionY("{0}_projection{1}".format(hist.GetName(), index), index, index)
        #ptYProjection = hist.ProjectionY("{0}_projection{1}".format(hist.GetName(), index), index, index+1)

        # Sanity checks
        # NOTE: The upper bound on integrals is inclusive!
        # NOTE: Integral() == Integral(1, proj.GetXaxis().GetNbins())
        if not generalUtils.isclose(norm, ptYProjection.Integral(1, ptYProjection.GetXaxis().GetNbins())):
            print("ERROR: Mismatch between sum and integral! norm: {0}, integral: {1}".format(norm, ptYProjection.Integral(1, ptYProjection.GetXaxis().GetNbins())))
        if not generalUtils.isclose(ptYProjection.Integral(), ptYProjection.Integral(1, ptYProjection.GetXaxis().GetNbins())):
            print("ERROR: Integral mismatch! Full: {0} 1-nBins: {1}".format(ptYProjection.Integral(), ptYProjection.Integral(1, ptYProjection.GetXaxis().GetNbins())))

        # Avoid scaling by 0
        if not norm > 0.0:
            continue

        # normalization by sum
        accessSetOfYBinValues(hist, index, yBinsContent, scaleFactor = norm)

def checkNormalization(hist, floatSwitch):
    """ Check to ensure that the normalization was successful. """
    for index in range(1, hist.GetXaxis().GetNbins() + 1):
        yBinsContent = []
        norm = 0

        # Access bins
        accessSetOfYBinValues(hist, index, yBinsContent)
        norm = sum(yBinsContent)

        if floatSwitch:
            # Otherwise, it will freqeuntly fail
            comparisonLimit = 1e-7
        else:
            # Same as in the default function defined in generalUtils
            comparisonLimit = 1e-9
        if not generalUtils.isclose(norm, 0, comparisonLimit) and not generalUtils.isclose(norm, 1, comparisonLimit):
            print("ERROR: Normalization not successful for bin {0}. Norm: {1}".format(index, norm))

def removeOutliers(hist, limit = 1, removeBelowLimits = True):
    """ Remove outliers from a given histogram. """
    if removeBelowLimits:
        # NOTE: GetNcells() is new in ROOT 6!!
        #nBins = hist.GetNcells()
        # nBins + 2 since range is exclusive on the upper limit.
        # 0 = underflow, 1-nBins = normal, nBins+1 is the overflow
        nBins = hist.GetXaxis().GetNbins() + 2
        if hist.InheritsFrom(ROOT.TH2.Class()):
            nBins = (nBins * (hist.GetYaxis().GetNbins() + 2))
        if hist.InheritsFrom(ROOT.TH3.Class()):
            print("Check the TH3 outliers code!! It may not be counted correctly!")
            nBins = (nBins * (hist.GetZaxis().GetNbins() + 2))

        #if nBins !=  hist.GetNcells():
        #    print("Mismatch! nBins: {0}, cells: {1}, histName: {2}".format(nBins, hist.GetNcells(), hist.GetName()))
        for index in range(0, nBins):
            # Check if the number of counts is below the limit
            if hist.GetBinContent(index) <= limit:
                # Remove the entry
                hist.SetBinContent(index, 0)
    else:
        lastCountSeen = 0
        #for index in range(0, hist.GetNcells()+1):
        for index in range(0, hist.GetXaxis().GetNbins()+1):
            if lastCountSeen > 10:
                print("Setting to 0 for index {}!".format(index))
                hist.SetBinContent(index, 0)
                #lastCountSeen = 0

            # Check if the number of counts is below the limit
            if hist.GetBinContent(index) > 0:
                lastCountSeen = 0
            else:
                lastCountSeen += 1

def plot1DPtHardHists(full, ptHardList, canvas):
    """ Basic plotting macro to show the contributions of pt hard bins on 1D hists """
    # Plot jet spectra
    full.Draw("same")
    ptHardBin = 1
    for spectra,color in zip(ptHardList, plottingUtils.colorSchemes.diverging + plottingUtils.colorSchemes.qualitative):
        #print("Color: {0}".format(color))
        spectra.SetMarkerColor(color)
        spectra.SetLineColor(color)
        spectra.SetMarkerSize(3)
        spectra.SetDirectory(0)

        print("ptHardBin: {0}, color: {1}".format(ptHardBin, color))
        ptHardBin += 1

        drawOptions = "same"
        spectra.Draw(drawOptions)

    canvas.SaveAs("{0}.pdf".format(full.GetName()))

if __name__ == "__main__":
    # Set the cluster bias to apply to the response matrix
    clusterBias = 6
    createProductionRootFile = True
    createResponseMatrix(clusterBias, createProductionRootFile)

    IPython.embed()
