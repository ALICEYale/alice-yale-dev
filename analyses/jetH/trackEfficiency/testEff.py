#!/usr/bin/env python

from __future__ import print_function

import ROOT
import os
import numpy
import IPython

# From: https://stackoverflow.com/a/33024979
def isclose(a, b, rel_tol=1e-09, abs_tol=0.0):
    return abs(a-b) <= max(rel_tol * max(abs(a), abs(b)), abs_tol)

def measuredEff():
    fIn = ROOT.TFile(os.path.join("fit", "trackEfficiency.lhc12f1a.root"))
    measured = fIn.Get("trackingEfficiency2D")
    measured.SetDirectory(0)
    fIn.Close()

    return measured

def fitFunction():
    # Extracted from fit!
    params = [
            -0.442232,
             0.501831,
            -0.252024,
             0.062964,
            -0.007681,
             0.000365,
             0.898052,
             0.402825,
            -2.213152,
             4.311098,
            -2.778200
            ]

    fit = ROOT.TF2("func","((1+[0]*x+[1]*x*x+[2]*x*x*x+[3]*x*x*x*x+[4]*x*x*x*x*x+[5]*x*x*x*x*x*x)*(x<6) + ([6]*(x>=6))) * (1+[7]*TMath::Abs(y)+[8]*y*y+[9]*TMath::Abs(y*y*y)+[10]*y*y*y*y)",0.99,10,-0.9,0.9);
    for i,val in enumerate(params):
        #print("{0}: {1}".format(i, val))
        fit.SetParameter(i, val)

    return fit

def testEffFromJetH(fitFunc):
    # Create JetH task
    jetH = ROOT.AliAnalysisTaskEmcalJetHMEC()
    # Set to pp
    jetH.SetForceBeamType(ROOT.AliAnalysisTaskEmcal.kpp)
    # Enable eff correction
    jetH.SetDoEffCorr(1)

    # Fill eff
    effPlot = ROOT.TH2D("testEff", "testEff", 100, 0, 10, 101, -1.01, 1.01)
    #effPlot = ROOT.TH2D("testEff", "testEff", 101, -0.05, 10.05, 101, -1.01, 1.01)
    #effPlot = ROOT.TH3D("testEff", "testEff", 101, -0.05, 10.05, 101, -1.01, 1.01, 100, 0, 1)
    #for pt in numpy.linspace(1, 10, 91):
    for pt in numpy.linspace(0, 10, 101):
        #print("pt: {0}".format(pt))
        for eta in numpy.linspace(-1, 1, 101):
            #print("eta: {0}, pt: {1}, Weight: {2}".format(eta, pt, jetH.EffCorrection(eta, pt)))
            effPlot.Fill(pt, eta, jetH.EffCorrection(eta, pt))

            if not isclose(jetH.EffCorrection(eta, pt), fitFunc.Eval(pt, eta)):
                print("Mismatch between jetH implementation ({2}) and fit func ({3})! pt: {0}, eta: {1}".format(pt, eta, jetH.EffCorrection(eta, pt), fitFunc.Eval(pt, eta)))

    #effPlot.SetDirectory(0)
    return [effPlot, jetH]

def checkPoint(pt, eta, measured, testEff, fitFunc, jetH):
    print("\nMeasured at ({0}, {1}): {2}".format(pt, eta, measured.GetBinContent(measured.GetXaxis().FindBin(pt), measured.GetYaxis().FindBin(eta))))
    print("TestEff  at ({0}, {1}): {2}".format(pt, eta, testEff.GetBinContent(testEff.GetXaxis().FindBin(pt), testEff.GetYaxis().FindBin(eta))))
    print("JetH     at ({0}, {1}): {2}".format(pt, eta, jetH.EffCorrection(eta, pt)))
    print("FitFunc  at ({0}, {1}): {2}".format(pt, eta, fitFunc.Eval(pt, eta)))

if __name__ == "__main__":
    measured = measuredEff()
    fitFunc = fitFunction()
    [testEff, jetH] = testEffFromJetH(fitFunc)

    # Save
    canvas = ROOT.TCanvas("canvas", "canvas")
    measured.GetXaxis().SetRangeUser(0, 10)
    measured.SetLineColor(ROOT.kRed)
    measured.Draw("surf")
    testEff.SetLineColor(ROOT.kBlue)
    testEff.Draw("same surf")
    fitFunc.SetLineColor(ROOT.kGreen+2)
    fitFunc.Draw("same surf")
    canvas.SaveAs("testEff.pdf")

    checkPoint(  3,  .3, measured = measured, testEff = testEff, fitFunc = fitFunc, jetH = jetH)
    checkPoint(  8, -.6, measured = measured, testEff = testEff, fitFunc = fitFunc, jetH = jetH)
    checkPoint(  6,   0, measured = measured, testEff = testEff, fitFunc = fitFunc, jetH = jetH)
    checkPoint(5.99, .1, measured = measured, testEff = testEff, fitFunc = fitFunc, jetH = jetH)
    checkPoint(  6,  .1, measured = measured, testEff = testEff, fitFunc = fitFunc, jetH = jetH)
    checkPoint(6.01, .1, measured = measured, testEff = testEff, fitFunc = fitFunc, jetH = jetH)
    checkPoint(5.99,-.4, measured = measured, testEff = testEff, fitFunc = fitFunc, jetH = jetH)
    checkPoint(  6, -.4, measured = measured, testEff = testEff, fitFunc = fitFunc, jetH = jetH)
    checkPoint(6.01,-.4, measured = measured, testEff = testEff, fitFunc = fitFunc, jetH = jetH)
    checkPoint(5.99,-.87, measured = measured, testEff = testEff, fitFunc = fitFunc, jetH = jetH)
    checkPoint(  6,-.87, measured = measured, testEff = testEff, fitFunc = fitFunc, jetH = jetH)
    checkPoint(6.01,-.87, measured = measured, testEff = testEff, fitFunc = fitFunc, jetH = jetH)
    checkPoint(6.2,-.87, measured = measured, testEff = testEff, fitFunc = fitFunc, jetH = jetH)
    checkPoint(9.2,   0, measured = measured, testEff = testEff, fitFunc = fitFunc, jetH = jetH)
    checkPoint(9.2,  .2, measured = measured, testEff = testEff, fitFunc = fitFunc, jetH = jetH)

    # Subtract
    canvas.SetRightMargin(0.15)

    measuredClone = measured.Clone(measured.GetName() + "Sub")
    measuredClone2 = measured.Clone(measured.GetName() + "Sub2")
    # Measured / fit
    measuredClone.Add(fitFunc, -1)
    measuredClone.GetXaxis().SetRangeUser(1, 10)
    measuredClone.GetYaxis().SetRangeUser(-.9, .9)
    measuredClone.Draw("colz")
    canvas.SaveAs("testEffMeasuredSubtracted.pdf")
    # Test Eff / fit
    testEffClone = testEff.Clone(testEff.GetName() + "Sub")
    testEffClone.Add(fitFunc, -1)
    testEffClone.GetXaxis().SetRangeUser(1, 9.95)
    testEffClone.GetYaxis().SetRangeUser(-.9, .89)
    testEffClone.Draw("colz")
    canvas.SaveAs("testEffJetHSubtractedFit.pdf")
    # Measred / testEff
    # NOTE: Binning doesn't match!!
    #measuredClone2.Add(testEff.Clone(testEff.GetName() + "Sub2"), -1)
    #measuredClone2.GetXaxis().SetRangeUser(1, 9.95)
    #measuredClone2.GetYaxis().SetRangeUser(-.9, .89)
    #measuredClone2.Draw("colz")
    #canvas.SaveAs("testEffJetHSubtractedMeasured.pdf")

    fOut = ROOT.TFile("testEff.root", "RECREATE")
    measured.Write()
    measuredClone.Write()
    fitFunc.Write()
    testEff.Write()
    testEffClone.Write()
    #measuredClone2.Write()
    fOut.Close()

    #IPython.embed()
