Alice Yale Dev Utils
====================

This contains python utilities for plotting, ROOT, LaTeX generation, etc...

To use, you'll need to install this module. To install the module while having it track
changes, install it with

```bash
cd alice-yale-dev/analyses/utilities/aliceYaleDevUtils
pip install -e .
```

Any changes you make to the files should automatically be available, as one would expect.

For advanced install options, see [below](#advanced-install)

Utilities Included
==================

- General Utilities
- Plotting Utilities
- File Utilities
- LaTeX Utilities

Advanced Install
================

If for some reason you don't want to install the module, you'll need to add the module to
your python path. There are two possible approaches:

- Add ``alice-yale-dev/analyses/utilities/aliceYaleDevUtils`` to your ``$PYTHONPATH`` environment variable
- Add the code below to your python code (note that you will need to adapt the path to ``utilsDirectory``
  to your own directory)

    ```python
    # Setup utilities module
    # Add utilties to python path.
    import sys
    import os
    # Determine the path to utilties module
    # ADAPT THIS PATH!!
    # We got back to the base of the repo to ensure that this can be imported from anywhere in the repo!
    utilsDirectory = os.path.realpath(os.path.join(os.getcwd(), "..", "..", "analyses", "utilities", "aliceYaleDevUtils"))
    # Insert into path
    if not utilsDirectory in sys.path:
        sys.path.insert(0, utilsDirectory)
    ```


