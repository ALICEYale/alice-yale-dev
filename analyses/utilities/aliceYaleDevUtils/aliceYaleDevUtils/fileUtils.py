#!/usr/bin/env python

import ROOT

def GetListOfHistograms(filename, listTaskName = "AliAnalysisTaskJetH_tracks_caloClusters_clusbias5R2GA"):
    """ Get histograms from the file and make them available in a dict """
    fIn = ROOT.TFile(filename)
    if not fIn or fIn.IsZombie():
        print("Could not find file '{0}".format(filename))
        return
    taskOutputList = fIn.Get(listTaskName)
    if not taskOutputList:
        print("Could not find list '{0}'".format(taskOutputList))
        return
    
    histList = dict()
    for obj in taskOutputList:
        histList[obj.GetName()] = obj
    
    return histList


