#!/usr/bin/env python

import sys
import colorama
# Setup colorama
colorama.init()

def printError(val, errorValue = 1):
    """ Print warning message in red with error label"""
    print(colorama.Fore.RED + colorama.Style.BRIGHT + "FATAL: {0}".format(val) + colorama.Style.RESET_ALL)
    sys.exit(errorValue)

def printError(val):
    """ Print warning message in red with error label"""
    print(colorama.Fore.RED + "ERROR: {0}".format(val) + colorama.Style.RESET_ALL)

def printWarning(val):
    """ Print warning message in yellow with warning label"""
    print(colorama.Fore.YELLOW + "WARNING: {0}".format(val) + colorama.Style.RESET_ALL)

def printInfo(val):
    """ Print warning message in yellow with warning label"""
    print(colorama.Fore.BLUE + colorama.Style.BRIGHT + "INFO: " + colorama.Style.RESET_ALL + "{0}".format(val))

def printSuccess(val):
    """ Print successful message in bright green """
    print(colorama.Fore.GREEN + colorama.Style.BRIGHT + "{0}".format(val) + colorama.Style.RESET_ALL)

def isclose(a, b, rel_tol=1e-09, abs_tol=0.0):
    """ Check if two floats are nearly equal (ie close).

    Note that math.isclose() is available in python 3.5.
    
    From: https://stackoverflow.com/a/33024979
    """
    return abs(a-b) <= max(rel_tol * max(abs(a), abs(b)), abs_tol)
