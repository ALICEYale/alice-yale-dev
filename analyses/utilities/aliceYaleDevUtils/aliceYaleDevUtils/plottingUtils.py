#!/usr/bin/env python

"""
Plotting utilities

Contains utilties for plotting such as:
    - Color Schemes
    - Helper functions
    - ...
"""

from generalUtils import printInfo
import ROOT

class colorSchemes(object):
    """
    Colors from Color Brewer 2: http://colorbrewer2.org/. Found from: https://stats.stackexchange.com/questions/118033/best-series-of-colors-to-use-for-differentiating-series-in-publication-quality .

    Collections include:
        - Qualitative 5 color
        - Sequential 5 color
        - Diverging 5 color

    The color values are stored as a list of root color numbers. They can be accessed as (Qualitative 5 color as an exmaple):

    >>> # Setup color schemes (Needed due to ROOT limitations)
    >>> plottingUtils.colorSchemes.init()
    INFO: Initializing color schemes
    >>> plottingUtils.colorSchemes.qualitiatve
    [2000, 2001, 2002, 2003, 2004]

    This is extremely easy to either use directly or loop over (using for example, zip()) to plot a range of values

    Colors are created using TColor::GetColor("#hexColor"). Note that it will create the new color if necessary.

    NOTE: TColor.GetFreeColorIndex() is new in root6 and thus cannot be used here! (In any case, it is not necessary
          when using the GetColor() approach.)
    """

    """ Create an empty color schemes static variables. They should be set through initializing.
    Initializing needs to be a distinct step because ROOT must be initialized first!"""
    qualitative = []
    sequential = []
    diverging = []

    @classmethod
    def init(cls):
        """ Initialize the color schemes in ROOT.

        This must be a separate step because automatic initialization seems to fail due to ROOT closing after creating the colors.
        It seems to be a general limitation of (Py)ROOT."""

        printInfo("Initializing color schemes")

        """
        Qualitative 5 color:
        - Type: qualitative
        - n = 5
        - Scheme = Set 3
        - Additional attributes:
            - Print friendly
        - http://colorbrewer2.org/#type=qualitative&scheme=Set3&n=5

        Purpose (quoting from color brewer):
        Qualitative schemes do not imply magnitude differences between legend classes, and hues are used to create
        the primary visual differences between classes. Qualitative schemes are best suited to representing nominal
        or categorical data.
        For more, see: http://colorbrewer2.org/learnmore/schemes_full.html#qualitative

        Colors: (hex -> rgb)
        1 = "#8dd3c7" -> 141,211,199
        2 = "#ffffb3" -> 255,255,179
        3 = "#bebada" -> 190,186,218
        4 = "#fb8072" -> 251,128,114
        5 = "#80b1d3" -> 128,177,211
        """
        qualitativeValues = ["#8dd3c7",
                             "#ffffb3",
                             "#bebada",
                             "#fb8072",
                             "#80b1d3"]

        for color in qualitativeValues:
            cls.qualitative.append( ROOT.TColor.GetColor(color) )

        """
        Sequential 5 color:
        - Type sequential
        - n = 5
        - Scheme = YlGnBu
        - Additional attributes:
            - Colorblind safe
            - Print friendly
        - http://colorbrewer2.org/#type=sequential&scheme=YlGnBu&n=5

        Purpose (quoting from color brewer):
        Sequential schemes are suited to ordered data that progress from low to high. Lightness steps dominate the
        look of these schemes, with light colors for low data values to dark colors for high data values.

        Colors: (hex -> rgb)
        1 = "#ffffcc" -> 255,255,204
        2 = "#a1dab4" -> 161,218,180
        3 = "#41b6c4" -> 65,182,196
        4 = "#2c7fb8" -> 44,127,184
        5 = "#253494" -> 37,52,148
        """
        sequentialValues = ["#ffffcc",
                            "#a1dab4",
                            "#41b6c4",
                            "#2c7fb8",
                            "#253494"]

        for color in sequentialValues:
            cls.sequential.append( ROOT.TColor.GetColor(color) )

        """
        Diverging 5 color:
        - Type diverging
        - n = 5
        - Scheme = RdYlBu
        - Additional attributes:
            - Colorblind safe
            - Print friendly
        - http://colorbrewer2.org/#type=diverging&scheme=RdYlBu&n=5

        Purpose (quoting from color brewer):
        Diverging schemes put equal emphasis on mid-range critical values and extremes at both ends of the data range.
        The critical class or break in the middle of the legend is emphasized with light colors and low and high extremes
        are emphasized with dark colors that have contrasting hues.
        For more, see: http://colorbrewer2.org/learnmore/schemes_full.html#diverging

        Colors:
        1 = "#d7191c" -> 215,25,28
        2 = "#fdae61" -> 253,174,97
        3 = "#ffffbf" -> 255,255,191
        4 = "#abd9e9" -> 171,217,233
        5 = "#2c7bb6" -> 44,123,182
        """
        divergingValues = ["#d7191c",
                           "#fdae61",
                           "#ffffbf",
                           "#abd9e9",
                           "#2c7bb6"]

        for color in divergingValues:
            cls.diverging.append( ROOT.TColor.GetColor(color) )
