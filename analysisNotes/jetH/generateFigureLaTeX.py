#!/usr/bin/env python

# Setup Jet-H utilities module
# Add utilties to python path. This a rather ugly hack, but it works for now
import sys
import os
# Determine the path to utilties module
jetHDirectory = os.path.realpath(os.path.join(os.getcwd(), "..", "..", "analyses", "jetH"))
# Insert into path
if not jetHDirectory in sys.path:
    sys.path.insert(0, jetHDirectory)
import jetHUtils

from aliceYaleDevUtils import *

def generateDPhiFigureLatex(collisionSystem):
    """ Generate deltaPhi figure code. """
    generalUtils.printInfo("Generating LaTeX for subtracted dPhi images")
    # Define properties

    # images/plotting/pp/jetHDPhiSubtracted_etaBin0_jetPtBin0_trackPtBin0.pdf
    image = os.path.join("images", "plotting", collisionSystem, "jetHDPhiSubtracted_etaBin0_jetPtBin%(jetPtBin)s_trackPtBin%(trackPtBin)s")
    caption = """$\\Delta\\varphi$ correlations in {\\pp} at {\\sqrts}=2.76 TeV. Jets were reconstructed using R=0.2.
    The plot show jets between %(jetPtRange)s with associated tracks
    between %(trackPtRange)s. The red curve is fit to two Gaussians with a pedestal, while
    the blue curve is fit to the pedestal and the green curve is the Gaussians with the
    pedestal subtracted."""
    label = "dPhi_trackPtBin%(trackPtBin)d_jetPtBin%(jetPtBin)d"

    output = ""
    for iJetPtBin in xrange(0, len(jetHUtils.jetPtBins)-1):
        for iTrackPtBin in xrange(0, len(jetHUtils.trackPtBins)-1):
            trackPtRange = jetHUtils.generateTrackPtRangeString(iTrackPtBin, latexCommands = True)
            jetPtRange = jetHUtils.generateJetPtRangeString(iJetPtBin, latexCommands = True)
            args = {"trackPtBin" : iTrackPtBin,
                    "trackPtMin" : jetHUtils.trackPtBins[iTrackPtBin],
                    "trackPtMax" : jetHUtils.trackPtBins[iTrackPtBin+1],
                    "trackPtRange": trackPtRange,
                    "jetPtBin" : iJetPtBin, 
                    "jetPtMin" : jetHUtils.jetPtBins[iJetPtBin],
                    "jetPtMax" : jetHUtils.jetPtBins[iJetPtBin+1],
                    "jetPtRange" : jetPtRange}
            #print("image: {0}, caption: {1}, label: {2}".format(image % args, caption % args, label % args))
            output += latexUtils.generateFigureEnvironmentLatex(image % args,
                                                          caption % args,
                                                          label % args,
                                                          fractionOfWidth = 0.75)

            if iTrackPtBin != 0 and iTrackPtBin % 5 == 0:
                # Needed to avoid the error "too many unprocessed floats".
                # See: https://tex.stackexchange.com/a/46514
                output += "\\clearpage"

    outputFilename = collisionSystem + "DPhiSubtracted.tex"
    generalUtils.printInfo("Writing {0} dPhi subtracted LaTeX to {1}".format(collisionSystem, outputFilename))
    with open(outputFilename, "wb") as fOut:
        fOut.write(output)

def generateTexForJoelsPlots(collisionSystem):
    """ Generate beamer figure stack example. """
    generalUtils.printInfo("Generating beamer figure stacks")
    unsubNames = ["PWfitBgExtract_Ass0.15-0.5_Trig20-40_C0-10v3free_rbX2.pdf",
             "PWfitBgExtract_Ass0.5-1.0_Trig20-40_C0-10v3free_rbX2.pdf",
             "PWfitBgExtract_Ass1.0-1.5_Trig20-40_C0-10v3free_rbX2.pdf",
             "PWfitBgExtract_Ass1.5-2.0_Trig20-40_C0-10v3free_rbX2.pdf",
             "PWfitBgExtract_Ass2.0-3.0_Trig20-40_C0-10v3free_rbX2.pdf",
             "PWfitBgExtract_Ass3.0-4.0_Trig20-40_C0-10v3free_rbX2.pdf",
             "PWfitBgExtract_Ass4.0-5.0_Trig20-40_C0-10v3free_rbX2.pdf",
             "PWfitBgExtract_Ass5.0-6.0_Trig20-40_C0-10v3free_rbX2L.pdf",
             "PWfitBgExtract_Ass6.0-10.0_Trig20-40_C0-10v3free_rbX2L.pdf"]

    dPhiNames = ["dPhiCorrelationsJ20-40_C0-10_0.15-0.5rebinX2BG.pdf",
             "dPhiCorrelationsJ20-40_C0-10_0.5-1.0rebinX2BG.pdf",
             "dPhiCorrelationsJ20-40_C0-10_1.0-1.5rebinX2BG.pdf",
             "dPhiCorrelationsJ20-40_C0-10_1.5-2.0rebinX2BG.pdf",
             "dPhiCorrelationsJ20-40_C0-10_2.0-3.0rebinX2BG.pdf",
             "dPhiCorrelationsJ20-40_C0-10_3.0-4.0rebinX2BG.pdf",
             "dPhiCorrelationsJ20-40_C0-10_4.0-5.0rebinX2BG.pdf",
             "dPhiCorrelationsJ20-40_C0-10_5.0-6.0rebinX2BGL.pdf",
             "dPhiCorrelationsJ20-40_C0-10_6.0-10.0rebinX2BGL.pdf",
            ]

    label = "Jet-H 1D $\\Delta\\varphi$ Correlations - 0-10\\% - \color{red} Only all angles!"
    outputText = ""
    for unsubName,dPhiName in zip(unsubNames, dPhiNames):
        images = [os.path.join("images", unsubName.replace(".pdf", "")),
                  os.path.join("images", dPhiName.replace(".pdf", ""))]
        outputText += latexUtils.generateStackedFigureEnvironmentBeamer(images = images,
                                                                        label = label,
                                                                        fractionOfWidth = 0.85)
        
    outputFilename = "joelsCentralImages.tex"
    generalUtils.printInfo("Writing Joel's central images beamer image stacks to {0}".format(outputFilename))
    with open(outputFilename, "wb") as fOut:
        fOut.write(outputText)

if __name__ == "__main__":
    collisionSystem = "pp"

    generateTexForJoelsPlots(collisionSystem = collisionSystem)

    #generateDPhiFigureLatex(collisionSystem)
