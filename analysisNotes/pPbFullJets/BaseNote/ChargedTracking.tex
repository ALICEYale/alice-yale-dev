\section{Charged Tracks}

\label{sect:Tracking}

ALICE's central barrel tracking detectors, Inner Tracking System ITS and Time Projection Chamber TPC, provide the track reconstruction for the charged jet component. The track selection described in this section follows a two-component hybrid approach to avoid an azimuth-dependent efficiency that would be caused by the non-uniform SPD response.
The two track classes used in this approach are \emph{global} and
\emph{complementary} tracks. Their selection criteria are given in
table~\ref{tab:trackcuts}. While for global tracks, at least one SPD
hit is required, complementary tracks cover the regions without SPD
response. In order to ensure a consistent momentum resolution even
without the SPD hits, the complementary tracks are constrained to the
main primary vertex. 
%The hybrid track cuts listed in Table ~\ref{tab:trackcuts} correspond to the 10041006 track cuts in CreateTrackCutsPWGJE.C

\begin{table}
\begin{center}
\caption{Overview of the hybrid track cuts.}
\label{tab:trackcuts}

\begin{tabular}{lll}
   \texttt{AliESDtrackCuts} function & Value & Comment \\
   \hline
   \hline
   \multicolumn{3}{l}{Global standard and global complementary tracks}  \\
   \hline
   \texttt{SetMinNCrossedRowsTPC}   & $70$                                & Number of crossed rows   \\
   \texttt{SetMinRatioCrossedRowsOver}   & $0.8$                          & Ratio between number of \\
   \texttt{\;FindableClustersTPC}     &                                     & crossed rows and   \\
                                    &                                     & findable clusters  \\
   \texttt{SetMaxChi2PerClusterTPC} & 4                                   & Maximum $\chi^2$ per TPC cluster \\
   \texttt{SetAcceptKinkDaughters}  & \texttt{kFALSE}                     & Reject tracks with kink \\
   \texttt{SetRequireITSRefit}      & \texttt{kTRUE}                      & Require ITS refit \\
   \texttt{SetRequireTPCRefit}      & \texttt{kTRUE}                      & Require TPC refit \\
   \texttt{SetMaxFractionSharedTPCClusters} & 0.4                         & Maximum fraction of shared \\
                                    &                                     & TPC clusters \\
   \texttt{SetMaxDCAToVertexXY}     & 2.4                                 & Maximum Distance of Closest \\
                                    &                                     & Approach (DCA) to the main \\
                                    &                                     & vertex in transverse direction \\
   \texttt{SetMaxDCAToVertexZ}      & 3.2                                 & Maximum DCA in longitudinal \\
                                    &                                     & direction \\
   \texttt{SetDCAToVertex2D}        & \texttt{kTRUE}                      & Cut on the quadratic sum of \\
                                    &                                     & DCA in XY- and Z-direction \\
   \texttt{SetMaxChi2PerClusterITS} & 36                                  & Maximum $\chi^2$ per ITS cluster \\
   \texttt{SetMaxChi2TPCConstrainedGlobal} & 36                           & Maximum $\chi^2$ between global \\
                                    &                                     & and TPC constrained tracks \\
   \texttt{SetRequireSigmaToVertex} & \texttt{kFALSE}                     & No sigma cut to vertex \\
   \texttt{SetEtaRange}             & -0.9,0.9                            & Pseudorapidity cut \\
   \texttt{SetPtRange}              & 0.15, 1E+15                         & Minimum $p_{\mathrm{T}}>150 \mathrm{MeV}/c$ \\
      \hline
   \hline
   \multicolumn{3}{l}{Only for global standard tracks}  \\
   \hline
   \texttt{SetClusterRequirementITS} & \texttt{AliESDtrackCuts::}         & Require at least one hit in SPD \\
                                     & \texttt{kSPD}, \texttt{kAny}       & \\
   \hline
   \hline
   \multicolumn{3}{l}{Only for complementary tracks}  \\
   \hline
   \texttt{Global track constrained to primary vertex} &                  & \\
   \hline
   
\end{tabular}
\end{center}
\end{table}

Hybrid track selection is optimized to recover the tracking acceptance when some parts of the SPD ladders are switched off. To ensure uniform distributions in the \mbox{($\eta$,$\varphi$)-plane}, an approach of hybrid tracks of the following types is used:
\begin{itemize}
 \item global tracks with measured space points in the SPD and a good fit in the full ITS
 \item global tracks without any space points in the SPD but with a good fit through the remaining ITS layers; track is constrained to the primary vertex
\end{itemize}
As far as available tracks of first type with SPD hits are used. Those give the best resolution in transverse momentum \pt. Tracks of the second type are constrained to the primary vertex of the event to improve the \pttrack{} resolution in spite of a missing hit in the SPD. 
\begin{figure}[!ht]
%\centering
\includegraphics[width=0.5\textwidth]{img/TrackQA/PhiCent10kINT7Run0LHC13b.eps}
\includegraphics[width=0.5\textwidth]{img/TrackQA/PhiCent10kINT7Run0LHC13c.eps}
\includegraphics[width=0.5\textwidth]{img/TrackQA/PhiCent10kINT7Run0LHC13d.eps}
\includegraphics[width=0.5\textwidth]{img/TrackQA/PhiCent10kINT7Run0LHC13e.eps}
\caption{\label{fig:TrackPhiMinBias}$\varphi$ distribution of hybrid tracks in different \pPb{} periods with minimum bias (kINT7) trigger. Upper left: LHC13b. Upper right: LHC13c. Lower left: LHC13d. Lower right: LHC13e.
}
\end{figure}
Fig. \ref{fig:TrackPhiMinBias} shows the azimuthal distribution of the two hybrid track categories in minimum bias events. In addition, the figure also shows the sum of the hybrid tracks resulting in a uniform azimuthal track multiplicity distribution. The primary vertex is estimated from all tracks in the event with SPD hits. The different panels in Fig. \ref{fig:TrackPhiMinBias} correspond to the different run periods used in the analysis. Runs from the LH13b period, upper left, are called good runs since the fraction of complementary tracks is small. The other three periods consist of semi-good runs in which the complementary tracks recover the tracks in the region where the SPD was inactive ($\varphi \sim 2$).

Fig. \ref{fig:TrackPhiTriggered} shows the azimuthal distribution of tracks for events triggered by the EMCal jet patch. In this case the azimuthal distribution of tracks is not flat due to the trigger. The enhancement of track multiplicity at $\varphi\sim2.2$ is due to the jets on which the event was triggered. At $\varphi\sim5.5$ the away-side jet is visible. This is not a problem for the jet finding since it doesn't create sharp artifical edges.
\begin{figure}[!ht]
\includegraphics[width=0.5\textwidth]{img/TrackQA/PhiCent10kEMCEJERun0LHC13d.eps}
\includegraphics[width=0.5\textwidth]{img/TrackQA/PhiCent10kEMCEJERun0LHC13e.eps}
\caption{\label{fig:TrackPhiTriggered}$\varphi$ distribution of hybrid tracks in different \pPb{} periods with jet trigger (kEMCEJE). Left: LHC13d. Right: LHC13e.
}
\end{figure}


\newpage