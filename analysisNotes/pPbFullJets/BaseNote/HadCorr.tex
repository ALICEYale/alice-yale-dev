\section{Hadronic Correction}

\label{sect:ChrgDoubleCount}

Electrons and positrons induce full EM showers in the EMCal. While
charged hadrons most often generate MIP energy deposition in the
EMCal, they can also generate partially-contained hadronic
showers. However, charged hadrons are also measured by the tracking
system, so correction to the EMCal clusters for showers due to charged
particles must be applied to avoid double-counting of this component
of the jet energy.

\subsubsection{Cluster-Track Matching}

In order to subtract the charged particle energy from the associated
clusters, charged track trajectories are propagated to the cluster positions in steps of 20cm and  
correcting for energy loss in the intervening
material. Fig. \ref{matchdphi} shows the residual distributions
for matched track-cluster pairs.  A
double-peaked structure in the $\Delta\phi$ distribution for the
data set is due to the opposite bending direction of positive and
negative charged particles in the magnetic field, which gives
significant displacement at the EMCal surface for low \pT\ tracks. Therefore we plot the positive and negative tracks separately.

Each propagated trajectory is matched to its closest EMCal cluster,
with the matching requirement. For this analysis we chose to use the
same matching criteria as was used in the pp fully reconstructed jet spectrum analysis of $|\Delta\eta| < 0.015$ and $|\Delta\phi|
< 0.03$, as illustrated by the blue vertical lines in
Fig. \ref{matchdphi} and \ref{matchdeta}. It is possible for multiple tracks to be
matched to the same cluster, however we require that each track can
only be matched to at most one cluster (i.e. the nearest cluster).

A shift in the peak was observed in earlier productions of this data
due to projecting the tracks to the EMCal surface at 430cm instead of
where the clusters actually deposit their energy within the EMCal. For
using a single position to project to, it
was determined that 440cm was a much better choice. This was
implemented for the AOD139 production and the resulting matching plots
included here now show a $\Delta\phi$ peak within our default matching
cuts. We note a tail in the lowest (0.5-1.0 GeV/c) momentum bin is
partially excluded by our cut. Because this is a small fraction of the
true matches and for the lowest momentum particles, this should have a negligible effect on
the spectra results. 

\begin{figure}[htp]
\centerline
{
   \includegraphics[width=0.9\textwidth]{img/HadCorr/pA_Phi_fits_0_281.png}
  }
  \caption{Track-cluster matching $\Delta\phi$ distributions for 0-10\% p-Pb events. The top row is for positive charged tracks and the bottom are negative tracks. The momentum of the tracks increases from left to right from 0.5 GeV/c to $ p > 8 GeV/c$.}
\label{matchdphi}
\end{figure}

\begin{figure}[htp]
\centerline
{
  \includegraphics[width=0.9\textwidth]{img/HadCorr/pA_Eta_fits_0_281.png}
  }
  \caption{Track-cluster matching $\Delta\eta$distributions for 0-10\%
    p-Pb events from lhc13b. The top row is for positive charged tracks and the
    bottom are negative tracks. The momentum of the tracks increases
    from left to right from 0.5GeV/c to $p > 8GeV/c$}
\label{matchdeta}
\end{figure}

\begin{figure}[htp]
\centerline
{
  \includegraphics[width=0.9\textwidth]{img/HadCorr/pA_Phi_fits_0_290.png}
  }
  \caption{Track-cluster matching $\Delta\phi$distributions for 0-10\%
    p-Pb events from lhc13e. The top row is for positive charged tracks and the
    bottom are negative tracks. The momentum of the tracks increases
    from left to right from 0.5GeV/c to $p > 8GeV/c$}
\label{matchdeta}
\end{figure}


%------------------------------------------------
\subsubsection{Correction procedure}

In addition to capturing the energy of neutral particles, EMCal
clusters also contain energy deposited by charged tracks.

Since EM showers due to electrons and positron are well-contained in the
EMCal, except at extremely high \pT\ ($>200$ GeV), their full
energy is measured by the EMCal with good resolution. However, hadronic
showers induced by charged hadrons are only partially contained within
the EMCal, and the response to charged hadrons is broader. This
response to simulated pions was presented in \cite{ppnote}.

In this analysis we take the same approach as in \cite{ppnote} to remove the
energy deposited by the charged particles. This correction is
neccessary to avoid doubling counting particles in our jet finding
procedure that leave both a
track in the TPC and deposit energy in the EMCal. The correction of cluster energy is
based on the following quantity, which is calculated  
for each cluster having matched tracks:

\begin{equation}
E_{subtracted}^{cluster} = max\{E_{reconstructed}^{cluster} - f * \sum(p_{matched}^{track}), 0\}
\end{equation}
 
\noindent
where $E_{reconstructed}^{cluster}$ is the cluster energy and
$\sum(p_{matched}^{track})$ is the sum of the 3-momentum magnitude for
all tracks matched to the cluster. For the pp baseline correction
 $f=1.0$ (``100\% correction'') was used. If
$E_{subtracted}^{cluster}<0$ the cluster is discarded, otherwise it is
assigned energy $E_{subtracted}^{cluster} >0$.

To further study the hadronic correction, we define the variable,
$R_{Corr}=(E_{reconstructed}^{cluster} -E_{subtracted}^{cluster})/\sum(p_{matched}^{track})$. We see no multiplicity
dependence in the pPb data and therefore only show one MB plot. In
Fig. \ref{fig:Rcorr}  we
compare the $R_{Corr}$ distribution for the p-Pb data to that measured
in the pp analysis at $\sqrt(2.76)$~TeV and the $\sqrt(5.02)$~TeV PYTHIA production. The agreement seems
reasonable between the p-Pb data and the corresponding MC. We
note that the event selection for the p--Pb data is set to kANY and that any triggered
events could have a different distribution.

\begin{figure}[htp]
\centerline
{
   \includegraphics[width=0.9\textwidth]{img/HadCorr/EsubComparedMC_lego281.png}
  }
  \caption{The $R_{corr}$ distribution for four different $\Sigma
    p_{T,track}$ slices. The data (blue points) is pPb from lhc13b, the black
    points are the corresponding MC production and the red line is the
  pp data from 2.76 TeV.}
\label{fig:Rcorr}
\end{figure}

For more details on the Hadronic Correction procedure, please see the
pp Full Jet Spectrum note by Ma et al and the 
Pb-Pb Full
Jet Spectra note by Reed et al.