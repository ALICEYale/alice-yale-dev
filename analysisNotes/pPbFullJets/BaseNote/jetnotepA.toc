\contentsline {section}{\numberline {1}Event Selection}{1}{section.1}
\contentsline {section}{\numberline {2}Charged Tracks}{2}{section.2}
\contentsline {section}{\numberline {3}EMCal Clustering and QA}{5}{section.3}
\contentsline {section}{\numberline {4}Hadronic Correction}{5}{section.4}
\contentsline {subsubsection}{\numberline {4.0.1}Cluster-Track Matching}{5}{subsubsection.4.0.1}
\contentsline {subsubsection}{\numberline {4.0.2}Correction procedure}{6}{subsubsection.4.0.2}
\contentsline {section}{\numberline {5}Jet selection}{8}{section.5}
\contentsline {subsection}{\numberline {5.1}Jet QA}{10}{subsection.5.1}
\contentsline {section}{\numberline {6}Underlying Event}{12}{section.6}
\contentsline {subsection}{\numberline {6.1}Methods to measure rho}{12}{subsection.6.1}
\contentsline {subsubsection}{\numberline {6.1.1}CMS-like method}{12}{subsubsection.6.1.1}
\contentsline {subsection}{\numberline {6.2}Scale Factor}{13}{subsection.6.2}
\contentsline {section}{\numberline {7}Underlying Event}{14}{section.7}
\contentsline {subsection}{\numberline {7.1}Methods to measure rho}{14}{subsection.7.1}
\contentsline {subsubsection}{\numberline {7.1.1}CMS-like method}{14}{subsubsection.7.1.1}
\contentsline {subsection}{\numberline {7.2}Scale Factor}{15}{subsection.7.2}
\contentsline {section}{\numberline {8}Background Fluctuations: \ensuremath {\delta \ensuremath {p_T}}}{16}{section.8}
\contentsline {section}{\numberline {9}Detector Response}{17}{section.9}
