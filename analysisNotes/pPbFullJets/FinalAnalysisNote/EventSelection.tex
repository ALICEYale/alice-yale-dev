
\section{Event, Track, Cluster and Jet Selections}

A brief summary of the event, track, cluster and jet cuts are included here for easy reference. 
For more details, please refer to the shared note \cite{basenote} or to the detailed note 
of Rongrong Ma \cite{ppnote}. 

\subsection{Used Period and Runs}
This analysis uses data from the LHC13b and LHC13c data periods which use the MB
trigger. The MB trigger in ALICE is based a coincidence signal in the
V0 detectors.

The MB triggered events include non-single diffractive (NSD) collisions as well as single-diffractive and electromagnetic interactions. The efficiency of the trigger and event selection is
$>99\%$ for NSD collisions. The contamination from single-diffractive and electro-
magnetic interactions are negligible. Additional details are included in Ref.~\cite{ALICE:2012xs}.

This analysis is run on the pPb JE lego train (Jets\_EMC\_pPb) over the AOD154
production for LHC13b and LHC13c. Because there is a dip in the $\phi$
distribution of global tracks for LHC13c, runs in that period are
called semi-good while runs in 
LHC13b are referred to as good. Note that this dip in the global track
distribution only effects the composition of the tracks used in this
analysis, since hybrid tracks are used to reconstruct the jets. The runs used in each period are listed here:

\begin{description}
\item[LHC13b\_AOD154 Good:]
195344, 195351, 195389, 195391, 195478, 195479, 195481, 195482, 195483 
\end{description}
\begin{description}
\item[LHC13c\_AOD154 Semi-Good (Runlist Full Jets):]
195677, 195675, 195673, 195644, 195635, 195633, 195593, 195592, 195568, 195567, 195566, 195531, 195529 
\end{description}

\subsection{Train Settings and Used Tag}
To assure consistency of the used code, each part of the analysis was performed with the same analysis tag from 20th May 2016 (vAN-20160520-1).
The trains and their settings are listed in Table \ref{tab:Trains}.
\begin{table}[htb]
\begin{center}
\caption{All trains used for the documented analysis} 
\begin{tabular}{rll}   
 \toprule
Train No. & Period & Comment \\
 \midrule
1183      & 13b\_AOD154 & Scale fac 1.55, HadCorr=2 \\
1182      & 13c\_AOD154 & Scale fac 1.55, HadCorr=2 \\
1207      & 13b\_AOD154 & Scale fac 1.55, HadCorr=1.7 \\
1208      & 13c\_AOD154 & Scale fac 1.55, HadCorr=1.7 \\
 \midrule
1218      & 13b\_AOD154 & Scale fac 1.52 \& 1.6, HadCorr=2 \\
1219      & 13c\_AOD154 & Scale fac 1.52 \& 1.6, HadCorr=2 \\
1230      & 13b\_AOD154 & Scale fac 1.52, HadCorr=1.7 \\
1231      & 13c\_AOD154 & Scale fac 1.52, HadCorr=1.7 \\
 \midrule
1179      & LHC13b4\_fix\_AOD & eff=100, HadCorr=2 \\
1184      & LHC13b4\_fix\_AOD & eff=100, HadCorr=2 (Chris wagon style) \\
1215      & LHC13b4\_fix\_AOD & eff=100, HadCorr=1.7 \\
1241      & LHC13b4\_fix\_AOD & eff=96, HadCorr=2 \\
%
\bottomrule
\end{tabular}
\label{tab:Trains} 
\end{center}
\end{table}

\subsection{Event Selection}

The  \texttt{AliEmcalPhysicsSelectionTask} was
used with its default settings for physics selection. Events with a reconstructed vertex with $|{V}_{z}|<$10 cm
were chosen.  In addition we require that $|zSPD=zTRK|<$1 mm to
exclude events where the SPD vertex was not properly reconstructed.
The centrality estimator used in this analysis is V0A. After the event
selection roughly 90 Million MB events were analyzed.
%
\subsection{Track Selection}
%
The track selection for this analysis is described in full detail in Refs.~\cite{basenote,ppnote}
here we repeat some general criteria. 
The track selection for this analysis follows a two-component hybrid approach to avoid an 
azimuth-dependent efficiency that would be caused by the non-uniform SPD response.
\par
Hybrid track selection is optimized to recover the tracking acceptance when some parts of the SPD
ladders are switched off. To ensure uniform distributions in the ($\eta$,$\varphi$)-plane, an approach of hybrid
tracks of the following types is used:
First, global tracks with measured space points in the SPD and a good fit in the full ITS. 
Second, global tracks without any space points in the SPD but with a good fit through the remaining ITS layers.
These tracks are additionally constrained to the primary vertex. 
As far as available, tracks of first type with SPD hits are used. Those give the best resolution in transverse
momentum $p_{T}$. Tracks of the second type are constrained to the primary vertex of the event to improve
the track $p_{T}$ resolution in spite of a missing hit in the SPD.
%
\subsection{Cluster Selection}
%
Each calorimeter cell is a $\Delta \eta$x$\Delta \phi$ = 0.014x0.014 tower.
For the analysis, all towers with an energy greater than 50 MeV were used for the clustering algorithm; the seed
threshold for the clusterizer was set to 100 MeV. The clusterization was performed with the v2 clusterizer.
This algorithm combines adjacent fired cells together until it reaches a local energy minima. 
\par
In this analysis full jets were analyzed and so tracks and clusters are taken into account. 
Since charged hadrons and leptons also deposit some energy in the EMCal this energy needs to 
be subtracted from the measured clusters, in order to avoid double counting the particle energy in
the jet measurement. This correction is called hadronic correction and is described in
Sec. 4 of Ref~\cite{basenote} and Sec. 4.3.2 of Ref.~\cite{ppnote}. 
For the p-Pb analysis a factor of 100\% correction was used (like for the pp case). 
%
\subsection{Jet Selection}
%
Jets are reconstructed by running a jet finding algorithm over the
charged tracks and the EMCal clusters which groups them together. The finding algorithms used in
this analysis are available from the FastJet package \cite{Cacciari:2011ma, Cacciari:2005hq}. The
anti-k$_{T}$ algorithm is used for the signal jets and k$_{T}$ for the
background estimation. 
\par
To reconstruct the jets, following criteria were applied:
tracks are required with $p_{T}>150$ MeV/c and clusters with a corrected energy $p_{T}>300$ MeV/c. 
We also exclude any jets that contain a track with $p_{T}>100$ GeV/c, due to the poor momentum resolution. 
To avoid edge effects due to the detector acceptance, only jets pointing within a distance R away from
the detector edges are accepted ($1.4+R<\varphi<\pi-R$ and $|\eta_{\mathrm{jet}}|<0.7-R$). 
Where R is the used resolution parameter (similar to a cone radius) which was set to R=0.2 and R=0.4 for this analysis.
To reduce the number of fake jets at low $p_{T}$, a cut on the jet area, $A_{jet}>0.6\pi R^{2}$, was made.



