\contentsline {section}{\numberline {1}Introduction}{3}{section.1}
\contentsline {section}{\numberline {2}Event, Track, Cluster and Jet Selections}{3}{section.2}
\contentsline {subsection}{\numberline {2.1}Used Period and Runs}{3}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Train Settings and Used Tag}{4}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Event Selection}{4}{subsection.2.3}
\contentsline {subsection}{\numberline {2.4}Track Selection}{5}{subsection.2.4}
\contentsline {subsection}{\numberline {2.5}Cluster Selection}{5}{subsection.2.5}
\contentsline {subsection}{\numberline {2.6}Jet Selection}{5}{subsection.2.6}
\contentsline {section}{\numberline {3}Underlying Event}{6}{section.3}
\contentsline {subsection}{\numberline {3.1}Scale Factor}{6}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Special case of underlying event in p+Pb}{7}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}Special case of underlying event in p+Pb}{7}{subsection.3.3}
\contentsline {section}{\numberline {4}Background Fluctuations: \ensuremath {\delta \ensuremath {p_T}}}{8}{section.4}
\contentsline {section}{\numberline {5}Unfolding}{11}{section.5}
\contentsline {section}{\numberline {6}Normalization}{12}{section.6}
\contentsline {section}{\numberline {7}Results}{13}{section.7}
\contentsline {subsection}{\numberline {7.1}Unfolded Spectra}{13}{subsection.7.1}
\contentsline {subsection}{\numberline {7.2}Jet Shape Ratio}{13}{subsection.7.2}
\contentsline {subsection}{\numberline {7.3}$R_{pPb}^{PYTHIA}$}{13}{subsection.7.3}
\contentsline {subsubsection}{\numberline {7.3.1}pp Reference spectrum}{15}{subsubsection.7.3.1}
\contentsline {subsubsection}{\numberline {7.3.2}Scaled ATLAS as a Reference}{17}{subsubsection.7.3.2}
\contentsline {subsection}{\numberline {7.4}Derived Plot}{17}{subsection.7.4}
\contentsline {section}{\numberline {8}Systematic Uncertainties}{17}{section.8}
\contentsline {subsection}{\numberline {8.1}Tracking Efficiency Uncertainty}{18}{subsection.8.1}
\contentsline {subsection}{\numberline {8.2}Hadronic Correction Uncertainty}{18}{subsection.8.2}
\contentsline {subsection}{\numberline {8.3}Uncertainty in the Background Estimation}{18}{subsection.8.3}
\contentsline {subsection}{\numberline {8.4}Unfolding Uncertainties}{18}{subsection.8.4}
\contentsline {subsection}{\numberline {8.5}Emcal Related Uncertainties}{25}{subsection.8.5}
\contentsline {subsection}{\numberline {8.6}Summary of Systematic Uncertainties}{30}{subsection.8.6}
\contentsline {section}{\numberline {9}Requested Plots QM14}{32}{section.9}
\contentsline {subsection}{\numberline {9.1}Technical Preliminary}{32}{subsection.9.1}
\contentsline {subsection}{\numberline {9.2}Physics Preliminary}{42}{subsection.9.2}
