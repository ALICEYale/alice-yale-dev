static  int      myDarkRed     = TColor::GetColor(128,0,0);
static  int      myDarkGreen   = TColor::GetColor(0,128,0);
static  int      myDarkBlue    = TColor::GetColor(0,0,128);

// Please get the ALICE logo at the locations below:
// Performance: https://aliceinfo.cern.ch/Figure/node/2016
// Preliminary: https://aliceinfo.cern.ch/Figure/node/2017
// then rename then accordingly (see lines 80 and 81)

void fig_PbPbSpectraCentR02Bias5_HQ_Old_final(Int_t rWrite = 1, Int_t rFigureTag = 0){
  // rWrite     = 0 / 1 / 2 -> unsaved / saved as .eps / saved as .png ;
  // rFigureTag = 0 / 1 / 2 -> no tag  /  performance  /  preliminary  ;

  bool HQ = true;
  bool New0to10 = false;
  bool New10to30 = true;
  bool pp = true;
  bool PP0to10 = false;
  bool drawbox = true; //switch for systematic error boxes in legend

  //Ncoll and TAA from https://twiki.cern.ch/twiki/bin/viewauth/ALICE/CentStudies
  float Ncoll0to10 = 1500.5;
  float TAA0to10 = 23.5;
  float Ncoll10to30 = 738.8;
  float TAA10to30 = 11.5;
  

  TFile *fpPb = TFile::Open("spectraoutput_prelim1.root");
  //TFile *fpPb = TFile::Open("Systematic_Error.root");
 TGraphAsymmErrors* pPbSys = (TGraphAsymmErrors*) fpPb->Get("R2_Spectrum_Sys");
 TH1D *histBase=(TH1D*) fpPb->Get("SpectrumR2");

 double NcollErr=0.08;
 for(int i=0; i<pPbSys->GetN(); i++){
   double xval, yval;
   pPbSys->GetPoint(i,xval,yval);

   double errup=pPbSys->GetErrorYhigh(i);
   double errlo=pPbSys->GetErrorYlow(i);

   double totup=sqrt(errup*errup+yval*yval*NcollErr*NcollErr);
   double totlo=sqrt(errlo*errlo+yval*yval*NcollErr*NcollErr);

   pPbSys->SetPointEYhigh(i,totup);
   pPbSys->SetPointEYhigh(i,totlo);


 }


  TFile *_file0 = TFile::Open("R02SpectraHQ.root");
  TGraphAsymmErrors* HQStat = gROOT->FindObject("JetPtStatR02");
  TGraphAsymmErrors* HQSys = gROOT->FindObject("JetPtSysR02");

  //value saved for HQ was not scaled by NColl!
  //checked and the errors scale properly

  for (int i = 0;i<HQStat->GetN();i++){
    Double_t x,y,ex,ey,ex;
    HQStat->GetPoint(i,x,y);
    ey = HQStat->GetErrorY(i);
    ex = HQStat->GetErrorX(i);
    y=y/Ncoll0to10;
    ey=ey/Ncoll0to10;
    HQStat->SetPoint(i,x,y);
    HQStat->SetPointError(i,ex,ex,ey,ey);
  }

  for (int i = 0;i<HQSys->GetN();i++){
    Double_t x,y,ex,ey,exlo;
    HQSys->GetPoint(i,x,y);
    ey = HQSys->GetErrorY(i);
    ex = 5;
    exlo = HQSys->GetErrorXlow(i);
    y=y/Ncoll0to10;
    ey=ey/Ncoll0to10;
    HQSys->SetPoint(i,x,y);
    HQSys->SetPointError(i,ex,ex,ey,ey);
  }

  // Use the following 2 lines for the old values (before new data was found)
  TFile *_file0 = TFile::Open("JetSpectraCent0.root");
  TH1* JetPt0to10CorOld = gROOT->FindObject("histNominalSpectrumR020Bias5Cent0");
  TFile *_file0 = TFile::Open("JetUnfoldPbPbOutputCent0.root");
  TList* l = gROOT->FindObject("Spectra");
  TH1* JetPt0to10Cor = l->FindObject("histSVDFinalSpectrum");
  TFile *_file0 = TFile::Open("JetUnfoldPbPbOutputCent1.root");
  TList* l2 = gROOT->FindObject("Spectra");
  TH1* JetPt10to30Cor = l2->FindObject("histSVDFinalSpectrum");

  
  JetPt0to10CorOld->Scale(1.0/Ncoll0to10);
  JetPt0to10Cor->Scale(1.0/Ncoll0to10);
  JetPt10to30Cor->Scale(1.0/Ncoll10to30);
  TFile *_file0 = TFile::Open("JetCrossSection.Combined.Indep.pass4.5GeVChBias.finebin.root");
  TH1* JetPtppR02 = (TH1*)gROOT->FindObject("Merged_0.2");
  TH1* JetPtppR02Sys = JetPtppR02->Clone();
  TFile *_file0 = TFile::Open("JetCrossSection.20120925.root");
  TGraphAsymmErrors* JetPtppR02systemp = gROOT->FindObject("SysUncertainty_Merged_0.2_all");
  int Npp = JetPtppR02systemp->GetN();

  
  //extrapolates pp sys error to the fine bins.
  for (int j = 1;j<=JetPtppR02Sys->GetNbinsX();j++){
    for (int i = 0;i<Npp;i++){
      Double_t x;Double_t y;
      JetPtppR02systemp->GetPoint(i,x,y);
      if (x+JetPtppR02systemp->GetErrorXhigh(i)<=JetPtppR02Sys->GetBinLowEdge(j))
	continue;
      if (x-JetPtppR02systemp->GetErrorXlow(i)>=JetPtppR02Sys->GetBinLowEdge(j+1))
	continue;
      //Y errors seem to be symmetric
      JetPtppR02Sys->SetBinError(j,JetPtppR02Sys->GetBinContent(j)*JetPtppR02systemp->GetErrorYlow(i)/y);
    }
  }
  JetPtppR02->Rebin(10);
  JetPtppR02->Scale(0.1);
  JetPtppR02Sys->Rebin(10);
  JetPtppR02Sys->Scale(0.1);
  JetPtppR02->Scale(TAA0to10/Ncoll0to10);
  JetPtppR02Sys->Scale(TAA0to10/Ncoll0to10);

  const int N = 12;

  Double_t pT[14]; Double_t pTwidth[14];
  Double_t jpt0to10[14]; Double_t jpt0to10hi[14];
  Double_t jpt0to10lo[14];
  Double_t jpt0to10old[14]; Double_t jpt0to10hiold[14];
  Double_t jpt0to10loold[14];
  Double_t jpt10to30[14]; Double_t jpt10to30hi[14];
  Double_t jpt10to30lo[14];
  Double_t unc[] = {19.4, 19.8, 20.5, 20.7, 20.1, 19.7, 26.2, 26.9}; //0-10
  Double_t unc2[] = {16.6,15.2, 14.8, 14.9, 13.3, 13.6, 15.7,100, 100}; //10-30 all
  //Double_t unc[] = {19.4, 19.8, 20.5, 20.7, 20.1, 19.7, 26.2, 26.9}; //0-10
  //  Double_t unc2[] = {26.3,15.0, 14.8, 14.8, 13.3, 13.6, 26.9,100, 100}; //10-30
  Double_t jpt0to10histat[14]; 
  Double_t jpt0to10histatold[14]; 
  Double_t jpt10to30histat[14]; 
  Double_t jptpp[14]; Double_t jptpphistat[14]; 
  Double_t jptppsys[14]; Double_t jptpphisys[14]; 

 for (int i = 0;i<N;i++){
    int offset = 3;
    if (i < offset) //stop at 30 GeV
      continue;
    if (i > 12)
      continue;
    pT[i-offset] = JetPt0to10Cor->GetBinCenter(i+1);
    pTwidth[i-offset] = JetPt0to10Cor->GetBinCenter(i+1)-JetPt0to10Cor->GetBinLowEdge(i+1);
    jptpp[i-offset] = JetPtppR02->GetBinContent(i+1);
    jptpphistat[i-offset] = JetPtppR02->GetBinError(i+1);
    jptppsys[i-offset] = JetPtppR02Sys->GetBinContent(i+1);
    jptpphisys[i-offset] = JetPtppR02Sys->GetBinError(i+1);
    if (i < 10){
      jpt10to30[i-offset] = JetPt10to30Cor->GetBinContent(i+1);
      jpt10to30histat[i-offset] = JetPt10to30Cor->GetBinError(i+1);
      jpt10to30lo[i-offset] = jpt10to30[i-offset]*unc2[i-offset]/100.0;
    }
    if (i == offset)
      continue;
    jpt0to10[i-offset] = JetPt0to10Cor->GetBinContent(i+1);
    jpt0to10histat[i-offset] = JetPt0to10Cor->GetBinError(i+1);
    jpt0to10lo[i-offset] = jpt0to10[i-offset]*unc[i-offset-1]/100.0;
    jpt0to10old[i-offset] = JetPt0to10CorOld->GetBinContent(i+1);
    jpt0to10histatold[i-offset] = JetPt0to10CorOld->GetBinError(i+1);
    jpt0to10loold[i-offset] = jpt0to10old[i-offset]*unc[i-offset-1]/100.0;
 }

  
  TGraphAsymmErrors* JetPt0to10SysOld = new TGraphAsymmErrors(N-3,pT,jpt0to10old,pTwidth,pTwidth,jpt0to10loold,jpt0to10loold);
  TGraphAsymmErrors* JetPt0to10Old = new TGraphAsymmErrors(N-3,pT,jpt0to10old,0,0,jpt0to10histatold,jpt0to10histatold);
  TGraphAsymmErrors* JetPt0to10Sys = new TGraphAsymmErrors(N-3,pT,jpt0to10,pTwidth,pTwidth,jpt0to10lo,jpt0to10lo);
  TGraphAsymmErrors* JetPt0to10 = new TGraphAsymmErrors(N-3,pT,jpt0to10,0,0,jpt0to10histat,jpt0to10histat);
  TGraphAsymmErrors* JetPt10to30Sys = new TGraphAsymmErrors(N-5,pT,jpt10to30,pTwidth,pTwidth,jpt10to30lo,jpt10to30lo);
  TGraphAsymmErrors* JetPt10to30 = new TGraphAsymmErrors(N-5,pT,jpt10to30,0,0,jpt10to30histat,jpt10to30histat);
  TGraphAsymmErrors* JetPtpp = new TGraphAsymmErrors(N-3,pT,jptpp,0,0,jptpphistat,jptpphistat);
  TGraphAsymmErrors* JetPtppSys = new TGraphAsymmErrors(N-3,pT,jptppsys,pTwidth,pTwidth,jptpphisys,jptpphisys);

  TGraphAsymmErrors* b1 = JetPt0to10Sys->Clone();
  TGraphAsymmErrors* b1old = JetPt0to10SysOld->Clone();
  TGraphAsymmErrors* b2 = JetPt10to30Sys->Clone();
  TGraphAsymmErrors* b1hq = HQSys->Clone();

  b1->SetLineColor(6);
  b1->SetFillStyle(0);
  b1old->SetFillStyle(0);
  b1old->SetLineColor(kGreen+1);
  b2->SetFillStyle(0);
  b2->SetLineColor(2);
  b1hq->SetFillStyle(0);
  b1hq->SetLineColor(4);
 
  JetPt0to10Sys->SetFillColor(6);
  JetPt0to10SysOld->SetFillColor(kGreen+1);
  JetPt0to10SysOld->SetFillStyle(3005);
  JetPt10to30Sys->SetFillColor(kRed-9);
  HQSys->SetFillColor(kBlue-9);
  HQSys->SetFillStyle(1001);
  JetPtppSys->SetFillColor(kGray);

  myOptions();
  gROOT->ForceStyle();

  TDatime now;
  int iDate = now.GetDate();
  int iYear=iDate/10000;
  int iMonth=(iDate%10000)/100;
  int iDay=iDate%100;
  char* cMonth[12]={"Jan","Feb","Mar","Apr","May","Jun",
		    "Jul","Aug","Sep","Oct","Nov","Dec"};
  char cStamp1[25],cStamp2[25];
  sprintf(cStamp1,"%i %s %i",iDay, cMonth[iMonth-1], iYear);
  sprintf(cStamp2,"%i/%.2d/%i",iDay, iMonth, iYear);


  TCanvas *myCan = new TCanvas("myCan",cStamp1,600,600);
  myCan->Draw();
  myCan->cd();

  TPad *myPad = new TPad("myPad", "The pad",0,0,1,1);
  myPadSetUp(myPad,0.22,0.04,0.04,0.15);
  myPad->Draw();
  myPad->SetLogy();
  myPad->cd();
 
  TH1F *myBlankHisto = new TH1F("myBlankHisto","Blank Histogram",20,0,130);
  //myBlankHisto->SetMaximum(5e-6);
  //myBlankHisto->SetMinimum(7e-10);
  myBlankHisto->SetMaximum(9e-6);
  myBlankHisto->SetMinimum(7e-10);
  myBlankHisto->SetTitleSize(0.05,"y");
  myBlankHisto->SetTitleSize(0.05,"x");
  myBlankHisto->SetTitleOffset(1.9,"y");
  myBlankHisto->SetYTitle("#frac{1}{#it{N}_{coll}} #frac{1}{#it{N}_{evt}} #frac{d^{2}#it{N}}{d#it{p}_{T,jet}d#eta_{jet}} (GeV/#it{c})^{-1}");
  myBlankHisto->SetXTitle("#it{p}_{T,jet} (GeV/#it{c})");
  myBlankHisto->SetNdivisions(505,"y");
  myBlankHisto->Draw();

  myGraphSetUp(JetPt0to10,1.5,21,6,1,6);
  myGraphSetUp(JetPtpp,1.5,20,1,1,1);
  myGraphSetUp(HQStat,1.5,21,4,1,4);
  myGraphSetUp(JetPt0to10Old,1.5,23,kGreen+1,1,kGreen+1);
  myGraphSetUp(JetPt10to30,1.5,34,2,1,2);
 
  if (pp){
    TGraphAsymmErrors* bpp = JetPtppSys->Clone();
    bpp->SetFillStyle(0);
    bpp->SetLineColor(1);
    JetPtppSys->Draw("E2same");
    JetPtpp->Draw("Psame");
    JetPtpp->SetLineWidth(3);
    bpp->Draw("E2Same");
    TBox* Boxpp = new TBox(10,1.25e-9,26,2.3e-9);
    Boxpp->SetFillColor(kGray);
    Boxpp->SetFillStyle(1001);
    Boxpp->SetLineColor(2);
    Boxpp->SetLineWidth(3);
    if (drawbox)
      Boxpp->Draw();
  } 
  if (HQ){
    HQSys->Draw("E2Same");
    HQSys->SetMarkerStyle(21);
    HQSys->SetMarkerColor(4);
    HQSys->SetLineColor(4);
    HQSys->SetLineWidth(3);
    HQStat->Draw("PE1X0Same");
    b1hq->Draw("E2same");
    TBox* B0to10HQ = new TBox(10,5.5e-9,26,1e-8);
    B0to10HQ->SetFillColor(kBlue-9);
    B0to10HQ->SetFillStyle(1001);
    B0to10HQ->SetLineColor(2);
    B0to10HQ->SetLineWidth(3);
    if (drawbox)
      B0to10HQ->Draw();
  }

   if (New0to10){
    JetPt0to10Sys->Draw("E2Same");
    JetPt0to10->SetLineWidth(3);
    // JetPt0to10Sys->SetFillStyle(3003);
    JetPt0to10Sys->SetFillColor(kMagenta-9);
    b1->Draw("E2Same");
    JetPt0to10->Draw("Psame");
    TBox* B0to10New = new TBox(10,5.5e-9,26,1e-8);
    B0to10New->SetFillColor(kMagenta-9);
    B0to10New->SetFillStyle(1001);
    B0to10New->SetLineColor(6);
    B0to10New->SetLineWidth(3);
    if (drawbox)
      B0to10New->Draw();
  }

  if (New10to30){
    JetPt10to30Sys->SetFillStyle(0);
    JetPt10to30Sys->Draw("E2Same");
    b2->SetLineWidth(3);
    b2->Draw("E2Same");
    JetPt10to30->SetLineWidth(3);
    TBox* B10to30 = new TBox(10,2.6e-9,26,4.7e-9);
    B10to30->SetFillColor(kRed-9);
    B10to30->SetFillStyle(0);
    B10to30->SetLineColor(2);
    B10to30->SetLineWidth(3);
    if (drawbox)
      B10to30->Draw();
  }


   if (PP0to10){
     JetPt0to10SysOld->Draw("E2Same");
     b1old->Draw("E2Same");
     JetPt0to10Old->Draw("PSame");
   }

   if (New10to30)
     JetPt10to30->Draw("Psame");


  cout << "making histo "<<endl;    
    TH1D* pPbStat=new TH1D("h","h",9,20,110);
    cout << "Formatting histo "<<endl;    
 
    for(int i=1; i<8; i++){
      int bincent=pPbStat->GetBinCenter(i);
      double bc=histBase->GetBinContent(histBase->FindBin(bincent));
      double berr=histBase->GetBinError(histBase->FindBin(bincent));
      pPbStat->SetBinContent(i,bc);
      pPbStat->SetBinError(i,berr);

    }


   pPbStat->SetMarkerColor(kGreen+3);
   pPbStat->SetMarkerStyle(33);
   pPbStat->SetMarkerSize(2.0);

   pPbSys->SetMarkerColor(kGreen+3);
   pPbSys->SetMarkerStyle(33);
   pPbSys->SetMarkerSize(2.0);

   pPbSys->SetLineColor(0);
   pPbSys->SetFillColor(kGreen - 8);
   pPbSys->Draw("E2Same");
   pPbStat->Draw("same,EX0");





   //TLatex *system = new TLatex(0.24,0.92,"ALICE Preliminary ");  //   Pb-Pb #sqrt{#it{s}_{NN}} = 2.76 TeV");
   TLatex *system = new TLatex(0.24,0.92,"ALICE Preliminary     anti-#it{k}_{T} #it{R} = 0.2 |#eta_{jet}|<0.5");
  system->SetNDC();
  system->SetTextSize(0.04);
  system->Draw();

  //TLegend *myLegend = new TLegend(0.5,0.78,0.7,0.9);
  TLegend *myLegend = new TLegend(0.51,0.83,0.71,0.95);
  myLegendSetUp(myLegend,0.04);
  //myLegend->AddEntry(myBlankHisto,"anti-#it{k}_{T} #it{R} = 0.2 |#eta_{jet}|<0.5","");    
  //myLegend->AddEntry(myBlankHisto,"#it{p}_{T,charged}^{leading} > 5 GeV/#it{c}","");
  TLegend *myLegend2 = new TLegend(0.27,0.20,0.7,0.38);
  myLegendSetUp(myLegend2,0.04);
  if (HQ){
    myLegend2->AddEntry(HQStat,"  0 - 10% ","P");
  }
  if (New0to10){
    myLegend2->AddEntry(JetPt0to10,"  0 - 10% New","P"); 
  }  
  if (New10to30){
    myLegend2->AddEntry(JetPt10to30," 10 - 30% ","P");  
  }
  if (pp){
    myLegend2->AddEntry(JetPtpp,"  pp #sqrt{#it{s}} = 2.76 TeV","P");
  }
 
  if (PP0to10){
    myLegend2->AddEntry(JetPt0to10Old,"  0 - 10% PP","P"); 
  }
  //myLegend->Draw();
  myLegend2->Draw();

  TLegend *myLegendpPb = new TLegend(0.52,0.77,0.95,0.87);
  myLegendSetUp(myLegendpPb,0.04);

  //myLegend2->AddEntry(pPbStat,"  pPb 5.02 TeV","PE");
  myLegendpPb->AddEntry(pPbSys,"  p-Pb #sqrt{#it{s}_{NN}}=5.02 TeV","FPE");
  //myLegend->Draw();
  //myLegend2->Draw();
  myLegendpPb->Draw();

   TLatex *bias = new TLatex(0.26,0.45,"#it{p}_{T,charged}^{leading} > 5 GeV/#it{c}");
    bias->SetNDC();
   bias->SetTextSize(0.035);
   bias->Draw();

 TLatex *system = new TLatex(0.26,0.40,"Pb-Pb #sqrt{#it{s}_{NN}} = 2.76 TeV");
   system->SetNDC();
   system->SetTextSize(0.035);
   system->Draw();


  if (rWrite == 1)  myCan->SaveAs("fig_PbPbSpectraR02Bias5.eps");
  if (rWrite == 2)  myCan->SaveAs("fig_PbPbSpectraR02Bias5.png");
 
  TFile::Open("PbPbSpectraR02Bias5.root","RECREATE");
  JetPtppR02Sys->SetName("ppR02Bias5Sys");
  JetPtppR02Sys->Write();
  JetPtppR02->SetName("ppR02Bias5");
  JetPtppR02->Write();
  JetPt0to10Sys->SetName("PbPb0to10R02Bias5Sys");
  JetPt0to10Sys->Write();
  JetPt0to10->SetName("PbPb0to10R02Bias5");
  JetPt0to10->Write();
  JetPt0to10SysOld->SetName("PbPb0to10R02Bias5SysOld");
  JetPt0to10SysOld->Write();
  JetPt0to10Old->SetName("PbPb0to10R02Bias5Old");
  JetPt0to10Old->Write();
  JetPt10to30Sys->SetName("PbPb10to30R02Bias5Sys");
  JetPt10to30Sys->Write();
  JetPt10to30->SetName("PbPb10to30R02Bias5");
  JetPt10to30->Write();
  HQStat->SetName("HQStat");
  HQSys->SetName("HQSys");
  HQStat->Write();
  HQSys->Write();
  
}

void myLegendSetUp(TLegend *currentLegend=0,float currentTextSize=0.07){
  currentLegend->SetTextFont(42);
  currentLegend->SetBorderSize(0);
  currentLegend->SetFillStyle(0);
  currentLegend->SetFillColor(0);
  currentLegend->SetMargin(0.25);
  currentLegend->SetTextSize(currentTextSize);
  currentLegend->SetEntrySeparation(0.5);
  return;
}

void myPadSetUp(TPad *currentPad, float currentLeft=0.11, float currentTop=0.04, float currentRight=0.04, float currentBottom=0.15){
  currentPad->SetLeftMargin(currentLeft);
  currentPad->SetTopMargin(currentTop);
  currentPad->SetRightMargin(currentRight);
  currentPad->SetBottomMargin(currentBottom);
  return;
}

void myGraphSetUp(TGraphAsymmErrors *currentGraph=0, Float_t currentMarkerSize = 1.0,
		  int currentMarkerStyle=20, int currentMarkerColor=0,
		  int currentLineStyle=1, int currentLineColor=0){
  currentGraph->SetMarkerSize(currentMarkerSize);
  currentGraph->SetMarkerStyle(currentMarkerStyle);
  currentGraph->SetMarkerColor(currentMarkerColor);
  currentGraph->SetLineStyle(currentLineStyle);
  currentGraph->SetLineColor(currentLineColor);
  return;
}

void myOptions(Int_t lStat=0){
  // Set gStyle
  int font = 42;
  // From plain
  gStyle->SetFrameBorderMode(0);
  gStyle->SetFrameFillColor(0);
  gStyle->SetCanvasBorderMode(0);
  gStyle->SetPadBorderMode(0);
  gStyle->SetPadColor(10);
  gStyle->SetCanvasColor(10);
  gStyle->SetTitleFillColor(10);
  gStyle->SetTitleBorderSize(1);
  gStyle->SetStatColor(10);
  gStyle->SetStatBorderSize(1);
  gStyle->SetLegendBorderSize(1);
  //
  gStyle->SetDrawBorder(0);
  gStyle->SetTextFont(font);
  gStyle->SetStatFont(font);
  gStyle->SetStatFontSize(0.05);
  gStyle->SetStatX(0.97);
  gStyle->SetStatY(0.98);
  gStyle->SetStatH(0.03);
  gStyle->SetStatW(0.3);
  gStyle->SetTickLength(0.02,"y");
  gStyle->SetEndErrorSize(3);
  gStyle->SetLabelSize(0.05,"xyz");
  gStyle->SetLabelFont(font,"xyz"); 
  gStyle->SetLabelOffset(0.01,"xyz");
  gStyle->SetTitleFont(font,"xyz");  
  gStyle->SetTitleOffset(1.2,"xyz");  
  gStyle->SetTitleSize(0.045,"xyz");  
  gStyle->SetMarkerSize(1); 
  gStyle->SetPalette(1,0); 
  if (lStat){
    gStyle->SetOptTitle(1);
    gStyle->SetOptStat(1111);
    gStyle->SetOptFit(1111);
    }
  else {
    gStyle->SetOptTitle(0);
    gStyle->SetOptStat(0);
    gStyle->SetOptFit(0);
  }
}
