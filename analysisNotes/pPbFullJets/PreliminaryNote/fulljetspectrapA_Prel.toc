\contentsline {section}{\numberline {1}Introduction}{2}{section.1}
\contentsline {section}{\numberline {2}Event, Track, cluster and Jet Selections}{2}{section.2}
\contentsline {subsection}{\numberline {2.1}Event Selection}{3}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Jet Selection}{3}{subsection.2.2}
\contentsline {section}{\numberline {3}Unfolding}{3}{section.3}
\contentsline {section}{\numberline {4}Normalization}{6}{section.4}
\contentsline {section}{\numberline {5}Results}{6}{section.5}
\contentsline {subsection}{\numberline {5.1}Unfolded Spectra}{6}{subsection.5.1}
\contentsline {subsection}{\numberline {5.2}Jet Shape Ratio}{6}{subsection.5.2}
\contentsline {subsection}{\numberline {5.3}$R_{pPb}^{PYTHIA}$}{6}{subsection.5.3}
\contentsline {subsubsection}{\numberline {5.3.1}pp Reference spectrum}{9}{subsubsection.5.3.1}
\contentsline {subsubsection}{\numberline {5.3.2}Scaled ATLAS as a Reference}{9}{subsubsection.5.3.2}
\contentsline {subsection}{\numberline {5.4}Derived Plot}{9}{subsection.5.4}
\contentsline {section}{\numberline {6}Systematic Uncertainties}{14}{section.6}
\contentsline {subsection}{\numberline {6.1}Tracking Efficiency Uncertainty}{14}{subsection.6.1}
\contentsline {subsection}{\numberline {6.2}Hadronic Correction Uncertainty}{14}{subsection.6.2}
\contentsline {subsection}{\numberline {6.3}Uncertainty in the Background Estimation}{18}{subsection.6.3}
\contentsline {subsection}{\numberline {6.4}Unfolding Uncertainties}{18}{subsection.6.4}
\contentsline {subsection}{\numberline {6.5}Emcal Related Uncertainties}{28}{subsection.6.5}
\contentsline {subsection}{\numberline {6.6}Summary of Systematic Uncertainties}{29}{subsection.6.6}
\contentsline {section}{\numberline {7}Requested Plots}{29}{section.7}
\contentsline {subsection}{\numberline {7.1}Technical Preliminary}{29}{subsection.7.1}
\contentsline {subsection}{\numberline {7.2}Physics Preliminary}{40}{subsection.7.2}
