#!/bin/bash

# screen to run multiple instances of phase1 of analysis in parallel
# does not actually analyze mass

# modified to take advantage of HEP TORQUE system


# eventGenerator main directory
JDIR=/home/hep/caines/mho8/eventGenerators
# notification email
JMAIL="michael.oliver@yale.edu"

if [ ! -f analysis/phase1 ]; then
    echo "Error: convert/Phase1 not found"
    echo "  Is it compiled?  Are you in main eventGenerators directory?"
    exit 1
fi

if [ "$#" -e 0 ]; then
    echo "Error: "
    echo "Proper usage:  "$0" <name>"
    exit 1
fi

JPATH=$1
JCONSTCUT=$2
JNAME=`echo $1 | sed "s/.*\///"`
JBIN='analysis/phase1'
LIST=`ls $JPATH.*.root | grep -v "hist"`
JNUM=`echo $LIST | wc -w`
echo "Will use command "$JBIN" on "$LIST
rm -f pbs/$JNAME.phase1.pbs
touch pbs/$JNAME.phase1.pbs

#source prep_env.sh
#PBS -m ae -M $JMAIL
echo "#PBS -q hep
#PBS -t 0-`expr $JNUM - 1`
#PBS -l nodes=1
#PBS -l walltime=24:00:00
cd $JDIR
./analysis/phase1 $JPATH.\$PBS_ARRAYID.root $JPATH.\$PBS_ARRAYID.hist.root -c $JCONSTCUT" > pbs/$JNAME.phase1.pbs
cat pbs/$JNAME.phase1.pbs
qsub pbs/$JNAME.phase1.pbs



exit 0

