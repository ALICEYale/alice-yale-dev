// Compares two outfiles from phase2, adds appropriate labels
//#include <boost/algorithm/string/replace.hpp>

#include <iostream>
#include <cstdlib>
#include <vector>
#include <algorithm>

#include <TMath.h>
#include <TH1.h>
#include <TH2.h>
#include <TFile.h>
#include <TString.h>
#include <TRegexp.h>
#include <TGraph.h>
#include <TGraphErrors.h>
#include <TMultiGraph.h>
#include <TCanvas.h>
#include <TLegend.h>
#include <TSystem.h>
#include <TSystemDirectory.h>
#include <TColor.h>
#include <TStyle.h>
#include <TKey.h>


#include "analysis_params.h"

void set_plot_style() {
    const Int_t NRGBs = 5;
    const Int_t NCont = 255;

    Double_t stops[NRGBs] = { 0.00, 0.34, 0.61, 0.84, 1.00 };
    Double_t red[NRGBs]   = { 0.00, 0.00, 0.87, 1.00, 0.51 };
    Double_t green[NRGBs] = { 0.00, 0.81, 1.00, 0.20, 0.00 };
    Double_t blue[NRGBs]  = { 0.51, 1.00, 0.12, 0.00, 0.00 };
    TColor::CreateGradientColorTable(NRGBs, stops, red, green, blue, NCont);
    gStyle->SetNumberContours(NCont);
}



using namespace std;

struct fileLabel {
  fileLabel(TString _filepath, TString _label): filepath(_filepath),label(_label) {}
  TString filepath;
  TString label;
};


void compare(vector<fileLabel> *fileLabels)  {

  Int_t colorList[7] = {kBlack,kRed,kOrange+7,kGreen-3,kBlue,kViolet,kMagenta-5};
  Int_t markerList[7] = {kFullSquare,kFullCircle,kFullDiamond,kOpenSquare,kOpenCircle,kOpenDiamond,kFullStar};

  double c_width = 600;
  double c_height = 600;

  vector<TFile *> *files = new vector<TFile *>;

  for (int i = 0 ; i < fileLabels->size(); i++ ) {
    fileLabel fl = fileLabels->at(i);
    printf("Opening File %s [%s] ... \n",fl.filepath.Data(),fl.label.Data());  
    TFile *f = TFile::Open(fl.filepath.Data(),"READ");
    if (!f) {
      fprintf(stderr,"Error: file %s not found!\nExiting.\n",fl.filepath.Data());
      exit(1);
    }
		TString nameNoSpace  = fl.label;
		nameNoSpace.ReplaceAll(" ","_");
//		printf("MHO nameNoSpace = %s\n",nameNoSpace.Data());
    f->SetTitle(fl.label.Data());
		f->SetName(nameNoSpace);
    files->push_back(f);
    
    printf("file %s, title %s\n",f->GetName(),f->GetTitle());

  }

  if ( !files->size()) exit(0);
  TFile *f0 = files->at(0);
   
  TCanvas *canvas = new TCanvas("canvas","canvas",c_width,c_height);
  gStyle->SetOptStat(0);
 
  TList * keys = f0->GetListOfKeys();

  TObject *obj,*obj2;
  TIter next(keys);
  while ((obj = next())) {
    if (!obj->InheritsFrom("TNamed")) continue;
    if (!obj->InheritsFrom("TKey")) continue;

    TKey *kobj = dynamic_cast<TKey*>(obj);
    obj2 = kobj->ReadObj();

//		TString objNameNoSpace = ((TNamed *) obj2)->GetName();
//		objNameNoSpace = TString::Format("%s_%s",objNameNoSpace.Data(),files->at(0)->GetTitle());
//		objNameNoSpace.ReplaceAll(" ","_");
//		printf("MHO objNameNoSpace = %s\n",objNameNoSpace.Data());
//		((TNamed *) obj2)->SetName(objNameNoSpace);

    vector<TObject *> * otherObj = new vector<TObject *>;

		TString objNameNoSpace;
	
    // Look for object in other files
    for (int j = 1 ; j < files->size(); j++) {
      TObject *o = files->at(j)->Get(obj2->GetName());
      if (o) {
        ((TNamed *) o)->SetTitle(files->at(j)->GetTitle());
				objNameNoSpace = ((TNamed *) o)->GetName();
				objNameNoSpace = TString::Format("%s_%s",objNameNoSpace.Data(),files->at(j)->GetTitle());
				objNameNoSpace.ReplaceAll(" ","_");
	//			printf("MHO objNameNoSpace = %s\n",objNameNoSpace.Data());
				((TNamed *) o)->SetName(objNameNoSpace);
        otherObj->push_back(o);
      } else {
        printf("Object %s not found in file %s.\n",obj2->GetName(),files->at(j)->GetName());
      }

    }
    
    // May need to do something about legend location
    // Hoping for the best for now
    TLegend * legend = new TLegend(0.50,0.65,0.90,0.85);



    if (obj2->InheritsFrom("TH2")) {
      // Nothing obvious to do.  Maybe calculate differences/ratios ??
 //     TH2 * h2obj = (TH2 *) obj2;
 //     printf("TH2 %s\n",h2obj->GetName());
//      h2obj->Draw("COLZ"); 
//      canvas->Print(TString::Format("output/cmp/%s.pdf",obj2->GetName()));

    } else if (obj2->InheritsFrom("TH1" )) {
      TH1 * hobj = (TH1 *) obj2;
      printf("TH1 %s\n",hobj->GetName());   
//			hobj->SetName(Form("%s_%s",hobj->GetName(),));

			// Creating File to store output in root files
			objNameNoSpace = obj->GetName();
			objNameNoSpace.ReplaceAll(" ","_");
			TFile *fOut = new TFile(Form("output/cmp/%s.root",objNameNoSpace.Data()),"RECREATE");




      double max_y, min_y;    
			objNameNoSpace = hobj->GetName();
			objNameNoSpace = TString::Format("%s_%s",objNameNoSpace.Data(),files->at(0)->GetTitle());
			objNameNoSpace.ReplaceAll(" ","_");
			//printf("MHO objNameNoSpace = %s\n",objNameNoSpace.Data());
			hobj->SetName(objNameNoSpace);

      //checking for specific histograms of interest
      if ( !strcmp(hobj->GetName(),"jetR_AA")) {
        hobj->GetXaxis()->SetRangeUser(10,40);
        hobj->SetTitle("Jet R_{AA}");
      }


      canvas->SetLogy(0);

      hobj->SetLineColor(1);
      hobj->SetMarkerColor(1);
      hobj->SetMarkerStyle(markerList[0]);
  //    hobj->SetMinimum(0.0000000001);
      hobj->Draw("PE");
      max_y = hobj->GetMaximum();
      min_y = hobj->GetMinimum();
      legend->AddEntry(hobj,f0->GetTitle(),"lp");

	//		printf("outfile name = %s, title = %s\n",fOut->GetName(),fOut->GetTitle());
			fOut->Add(hobj);

      for (int j = 0; j < otherObj->size(); j++) {  
        TH1 *otherH = (TH1 *) otherObj->at(j);
				fOut->Add(otherH);
        int color = colorList[j+1];
//        int color = j+2;
        otherH->SetLineColor(color);        
        otherH->SetMarkerColor(color);        
        otherH->SetMarkerStyle(markerList[j+1]);
//        otherH->SetMinimum(0.0000000001);
        otherH->Draw("SAME PE");
        max_y = TMath::Max(max_y,otherH->GetMaximum());
        min_y = TMath::Min(min_y,otherH->GetMinimum());

        legend->AddEntry(otherH,otherH->GetTitle(),"lp");
      }
      // slight adjust
     // max_y += 0.46714 * (max_y - min_y);
      max_y += 0.2 * (max_y - min_y);

      if ( !strcmp(hobj->GetName(),"jetR_AA")) { 
        max_y = 0.4; 
        min_y = 0.1;
        legend->SetY1(0.6);

        
      }
      hobj->GetYaxis()->SetRangeUser(min_y,max_y);
      legend->Draw("SAME");
//      canvas->SetLogy(1);
    // experiment: check if logY is appropriate
      double maxEntry = hobj->GetBinContent(hobj->GetMaximumBin());
      double rms = hobj->GetStdDev();
      double xRange = hobj->GetXaxis()->GetXmax() - hobj->GetXaxis()->GetXmin();

      if (rms < 0.01*xRange) canvas->SetLogy(1);

			fOut->Write();
//			fOut->Close();
//			delete fOut;
      canvas->Print(TString::Format("output/cmp/%s.pdf",obj2->GetName()));
 //     canvas->Print(TString::Format("output/cmp/%s.root",obj2->GetName()));
    //  canvas->Print(TString::Format("output/cmp/%s.C",obj2->GetName()));
    } else if (obj2->InheritsFrom("TGraph")) {  
      TGraph * gobj = (TGraph *) obj2;
      printf("TGraphErrors %s\n",gobj->GetName());
      double max_y, min_y;
      gobj->SetLineColor(1);  
			gobj->SetLineStyle(1);
      gobj->SetMarkerColor(1);
      gobj->SetMarkerStyle(markerList[0]);

      TMultiGraph *mg = new TMultiGraph();
      mg->Add(gobj);

     // gobj->Draw("ALP");
      max_y = gobj->GetMaximum();
      min_y = gobj->GetMinimum();
      legend->AddEntry(gobj,f0->GetTitle(),"lp");
      for (int j = 0; j < otherObj->size(); j++) {  
        TGraph *otherG = (TGraph *) otherObj->at(j);
        int color = colorList[j+1];
        otherG->SetLineColor(color);        
				otherG->SetLineStyle(1);
        otherG->SetMarkerColor(color);        
        otherG->SetMarkerStyle(markerList[j+1]);
  //      otherG->Draw("SAME LP");
        mg->Add(otherG);
        max_y = TMath::Max(max_y,otherG->GetMaximum());
        min_y = TMath::Min(min_y,otherG->GetMinimum());

        legend->AddEntry(otherG,otherG->GetTitle(),"lp");
      }
      // slight adjust
      max_y += 0.46714 * (max_y - min_y);
    //  max_y += 0.3 * (max_y - min_y);
      //not sure if this is working
      gobj->SetMinimum(min_y);
      gobj->SetMaximum(max_y);
 //     mg->GetYaxis()->SetRangeUser(min_y,max_y);
 //     mg->SetMaximum(0.6);
  
      mg->Draw("ALP");
      mg->GetXaxis()->SetTitle(gobj->GetXaxis()->GetTitle());
      mg->GetYaxis()->SetTitle(gobj->GetYaxis()->GetTitle());


      legend->Draw("SAME");
      canvas->Print(TString::Format("output/cmp/%s.pdf",obj2->GetName()));
//      canvas->Print(TString::Format("output/cmp/%s.C",obj2->GetName()));
    } else if (obj2->InheritsFrom("TF1")) {

      TF1 * fobj = (TF1 *) obj2;
      printf("TF1 %s\n",fobj->GetName());
			fobj->SetLineColor(colorList[0]);
			fobj->Draw();


      legend->AddEntry(fobj,f0->GetTitle(),"lp");
      for (int j = 0; j < otherObj->size(); j++) {  
        TF1 *otherG = (TF1 *) otherObj->at(j);
        int color = colorList[j+1];
        otherG->SetLineColor(color);        
//        otherG->SetMarkerColor(color);        
//        otherG->SetMarkerStyle(22);
  //      otherG->Draw("SAME LP");
				otherG->Draw("SAME");
  //      mg->Add(otherG);
      //  max_y = TMath::Max(max_y,otherG->GetMaximum());
     //   min_y = TMath::Min(min_y,otherG->GetMinimum());

        legend->AddEntry(otherG,otherG->GetTitle(),"l");
      }


			legend->Draw("SAME");
	//		canvas->SetLogy(1);
      canvas->Print(TString::Format("output/cmp/%s.pdf",obj2->GetName()));
		}
    legend->Clear();
    canvas->Clear();
  }
}

int main(int argc, char * argv[]) {

  if (argc == 1) {
    printf("Usage: %s [Filepath_1] [Label_1] [Filepath_2] [Label_2] ... \n",argv[0]);
//    printf("    Or %s -p [PATTERN] [Filepath_1] [Label_1] [Filepath_2] [Label_2] ... \n",argv[0]);
    exit(0);
  }


	
  int num = (argc - 1) / 2;
  vector<fileLabel> *fileLabels = new vector<fileLabel>;
  for (int i = 0; i < num; i++) {
    fileLabels->push_back(fileLabel(TString(argv[2*i + 1]),TString(argv[2*(i+1)])));
  }
  compare(fileLabels);
}




