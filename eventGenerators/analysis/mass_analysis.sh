#!/bin/bash

# screen to run multiple instances of phase1 of analysis in parallel
# does not actually analyze mass

if [ ! -f analysis/phase1 ]; then
    echo "Error: convert/Phase1 not found"
    echo "  Is it compiled?  Are you in main eventGenerators directory?"
    exit 1
fi

if [ "$#" == 0 ]; then
    echo "Error: "
    echo "Proper usage:  "$0" <path/name> [-r <JetResolutionParamter>] [-c <ConstituentCut>]"
    exit 1
fi


JPATH=$1
JBIN='analysis/phase1'
JNAME=`echo $1 | sed "s/.*\///"`
JOPT=$2 $3 $4 $5
LIST=`ls $JPATH.*.root | grep -v "hist"`
echo "Path "$JPATH
echo "Name "$JNAME
echo "Will use command "$JBIN" on "$LIST
for fl in $LIST
do
#creating screen name from param file: 
  j_screen_name=`echo $fl | sed -e 's/root\///' -e 's/.root//' -e 's/.*\///'`
  j_out_file=`echo $fl | sed -e 's/.root/.hist.root/'`
  echo screen -S $j_screen_name -d -m
  echo screen -S $j_screen_name -X stuff \"$JBIN -i $fl -o $j_out_file $JOPT\"
  echo screen -S $j_screen_name -X stuff \"exit 0\"
  echo ""
  screen -S $j_screen_name -d -m
  sleep 0.3
# sleeping to make sure screen exists when command is sent to it
	screen -S $j_screen_name -X stuff "$JBIN -i $fl -o $j_out_file $JOPT\n"

  # i don't really know why these sleeps are necessary
  # kind of like how we don't know why we need to sleep
  sleep 1.0
  screen -S $j_screen_name -X stuff "exit 0\n"
  sleep 0.3
done


