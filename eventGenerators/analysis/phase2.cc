// phase2 file
// Take merged histograms, plot, fit 
// Create sigma, D_AA, R_AA plots
// 
// Can compile with:
//  make phase2
//
//  or
//
//  g++ -Wl,--no-as-needed -std=c++11 phase2.cc -o phase2 -I`root-config --incdir` `root-config --libs` `fastjet-config --cxxflags --libs --plugins`
//
//  or (if you have readableStyle.h)
//
//  make USER_DEFINED=-DRE_STYLE=1 basicAnalysis

#ifdef RE_STYLE
#include <readableStyle.h>
#endif

#define PI TMath::Pi()

#include <vector>
#include <math.h>
#include <cmath>
#include <iostream>
#include <cstdlib>
#include <iomanip>


#include <fastjet/ClusterSequence.hh>

#include <TFile.h>
#include <TStyle.h>
#include <TColor.h>
#include <TString.h>
#include <TTree.h>
#include <TBranch.h>
#include <TLorentzVector.h>
#include <TH1F.h>
#include <TH2F.h>
#include <TH3F.h>
#include <TH1D.h>
#include <TF1.h>
#include <TF2.h>
#include <TMath.h>
#include <TCanvas.h>
#include <TAxis.h>
#include <TGraph.h>
#include <TGraphErrors.h>
#include <TLegend.h>
#include <TProfile.h>
#include <TText.h>
#include <TLatex.h>

#include "phase2.h"
#include "analysis_params.h"
//#include "fitAlgos.h"

using namespace std;


void set_plot_style() {
  const Int_t NRGBs = 5;
  const Int_t NCont = 255;

  Double_t stops[NRGBs] = { 0.00, 0.34, 0.61, 0.84, 1.00 };
  Double_t red[NRGBs]   = { 0.00, 0.00, 0.87, 1.00, 0.51 };
  Double_t green[NRGBs] = { 0.00, 0.81, 1.00, 0.20, 0.00 };
  Double_t blue[NRGBs]  = { 0.51, 1.00, 0.12, 0.00, 0.00 };
  TColor::CreateGradientColorTable(NRGBs, stops, red, green, blue, NCont);
  gStyle->SetNumberContours(NCont);
}


// General method to return a 2D-histogram whose columns have been normalized 
// to 1 (* bin size?)

TH2F * normalizeHistogramColumns(TH2F *inHist) {

  TH2F * normHist = (TH2F *) inHist->Clone();
  normHist->SetName(Form("%sNorm",inHist->GetName()));
  normHist->SetTitle(Form("%s (Normalized)",inHist->GetTitle()));
  TH2F * scaleHist = (TH2F *) inHist->Clone();
  scaleHist->SetName(Form("%sScale",inHist->GetName()));
  scaleHist->SetTitle(Form("%s (Scale)",inHist->GetTitle()));
  scaleHist->Sumw2(0);
  int nbinsx = inHist->GetNbinsX();
  int nbinsy = inHist->GetNbinsY();

  for (int i = 0; i < nbinsx+1; i++) {
    double integral = inHist->Integral(i,i,0,nbinsy+1,"width");
//    double integral = inHist->Integral(i,i+1,0,nbinsy+1,"width");
    integral = integral ? 1./integral : 0; // lol
    for (int j = 0; j < nbinsy+1; j++) {
      scaleHist->SetBinContent(i,j,integral);
      scaleHist->SetBinError(i,j,0);
    }
  }

  normHist->Multiply(scaleHist);
  delete scaleHist;
  return normHist;
}



void phase2(TString inputFilename = "root/pp.hist.root", TString
    outputFilename = "output/pp.final.root")
{
  // Open input file
  //	inputFilename = Form("output/%s.root", inputFilename.Data());
  TFile * fIn = TFile::Open(inputFilename.Data(),"READ");

  // Check if file is open
  if (fIn->IsOpen() == kFALSE)
  {
    Printf("inputFilename \"%s\" is not found!", inputFilename.Data());
    std::exit(1);
  }

  gStyle->SetOptStat(0);


  int nEvents = 0;
  int nEventsPerRun = 0;
  double nEventsPerRunDouble = 0;
  double totalWeight = 0.0;
  double totalWeightPerRun = 0.0;

  double averageCrossSection = 0;
  double crossSectionPerRun = 0;

  // Get runInfo TTree
  TTree * runInfo = (TTree *) fIn->Get("runInfo");
  if (!runInfo) {
    fprintf(stderr,"Error: TTree runInfo not found.  Exitting ... \n");
    exit(1);
  }
  // Get parameter TTree
  TTree * parameters = (TTree *) fIn->Get("parameters");
  if (!parameters) {
    printf("Parameters TTree not found!  Using parameters from analysis_params.h\n");
  } else {
    printf("Parameters TTree found!\n");
    if (parameters->GetEntries() ) {
      parameters->SetBranchAddress("jetR",&R);
      parameters->SetBranchAddress("constCut",&jet_constituent_cut);
      parameters->SetBranchAddress("etaCut",&eta_cut);
      parameters->SetBranchAddress("particlePtCut",&particle_pt_min);
      parameters->GetEntry(0);
      if (R< 0.01) { // FIXME temporary measure for float bug
        R = 0.3;
        jet_constituent_cut = 3;
        eta_cut = 1;
        particle_pt_min = 0;
        printf("Using temporary correction\n");
      }
    } else {
      printf("Parameters TTree empty!  Using parameters from analysis_params.h");
    }
  }
  printf("Jet Radius Parameter:  %f\n",R);
  printf("Constituent Cut     :  %f\n",jet_constituent_cut); 
  printf("Eta Cut             :  %f\n",eta_cut);
  printf("Particle Pt Cut     :  %f\n",particle_pt_min); 



  TBranch *bNEvents = runInfo->GetBranch("nEvents");
  TBranch *bTotalWeight = runInfo->GetBranch("totalWeight");
  TBranch *bCrossSection = runInfo->GetBranch("crossSection");

	// Check type of bNEvents for compatibility.  This is Kirill's fault.
	printf("DEBUG: bNEvents title = %s\n",bNEvents->GetTitle());
	bool useInt = !strcmp(bNEvents->GetTitle(),"nEvents/I");
	printf("useInt = %d\n",useInt);
	if (useInt)	bNEvents->SetAddress(&nEventsPerRun);
	else bNEvents->SetAddress(&nEventsPerRunDouble);
  bTotalWeight->SetAddress(&totalWeightPerRun);
  if ( bCrossSection) bCrossSection->SetAddress(&crossSectionPerRun);

  int nRuns = runInfo->GetEntries();
  for (int i = 0; i < nRuns; i++) {
    runInfo->GetEntry(i);
    nEvents += nEventsPerRun + (int) nEventsPerRunDouble;
    totalWeight += totalWeightPerRun;
    averageCrossSection += crossSectionPerRun;
  }
  // from picobarns to millibarns
  if (runInfo->GetEntries()) averageCrossSection = 1e-9 * averageCrossSection / runInfo->GetEntries();


  printf("According to the TTree, I have %d events from %d files, with total weight %f and average cross section %f millibarns.\n",nEvents,nRuns,totalWeight, averageCrossSection);



  // Get histograms
  TH2F * dEtaDPhi = (TH2F *) fIn->Get("dEtaDPhi");
  TH2F * etaPhi = (TH2F *) fIn->Get("etaPhi");
  TH1D * jetPt = (TH1D *) fIn->Get("jetPt");
  TH1D * recJetPt = (TH1D *) fIn->Get("recJetPt");
  TH1D * trackPt = (TH1D *) fIn->Get("trackPt");
  TH1D * gammaEt = (TH1D *) fIn->Get("gammaEt");
  TH1D * jetPtUnweighted = (TH1D *) fIn->Get("jetPtUnweighted");
  TH1D * particleEnergy = (TH1D *) fIn->Get("particleEnergy");
  TH2F * jetEtaPhi = (TH2F *) fIn->Get("jetEtaPhi");
  TH2F * leadingJetEtaPhi = (TH2F *) fIn->Get("leadingJetEtaPhi");
  TH1F * dijetAj = (TH1F *) fIn->Get("dijetAj");
  TH2F * dijetAjJetPt = (TH2F *) fIn->Get("dijetAjJetPt");
  TH1F * dijetXj = (TH1F *) fIn->Get("dijetXj");
  TH2F * dijetXjJetPt = (TH2F *) fIn->Get("dijetXjJetPt");
  TH1I * hWeight = (TH1I *) fIn->Get("hWeight");

  TH1F * vertexR = (TH1F *) fIn->Get("vertexR");
  TH1F * vertexRHardCore = (TH1F *) fIn->Get("vertexRHardCore");
  TH1F * vertexRNoHardCore = (TH1F *) fIn->Get("vertexRNoHardCore");
  TH1F * vertexRDijetCut = (TH1F *) fIn->Get("vertexRDijetCut");
  TH2F * vertexXY = (TH2F *) fIn->Get("vertexXY");
  TH2F * vertexXYHardCore = (TH2F *) fIn->Get("vertexXYHardCore");
  TH2F * vertexXYNoHardCore = (TH2F *) fIn->Get("vertexXYNoHardCore");
  TH2F * vertexXYRot = (TH2F *) fIn->Get("vertexXYRot");
  TH2F * vertexXYRotHardCore = (TH2F *) fIn->Get("vertexXYRotHardCore");
  TH2F * vertexXYRotNoHardCore = (TH2F *) fIn->Get("vertexXYRotNoHardCore");
  TH2F * vertexXYRotDijet = (TH2F *) fIn->Get("vertexXYRotDijet");
  TH2F * vertexHighestPtXRot = (TH2F *) fIn->Get("vertexHighestPtXRot");
  TH2F * vertexXRotJetPt = (TH2F *) fIn->Get("vertexXRotJetPt");
  TH2F * vertexXRotJetPtHardCore = (TH2F *) fIn->Get("vertexXRotJetPtHardCore");
  TH2F * vertexXRotTriggerPt = (TH2F *) fIn->Get("vertexXRotTriggerPt");


  TH2F * vertexJetPtR = (TH2F *) fIn->Get("vertexJetPtR");
  TH2F * vertexJetPtRHardCore = (TH2F *) fIn->Get("vertexJetPtRHardCore");
  TH2F * vertexJetPtRDijet = (TH2F *) fIn->Get("vertexJetPtRDijet");
  TH2F * vertexDijetAjR = (TH2F *) fIn->Get("vertexDijetAjR");
  TH2F * vertexHighestPtR = (TH2F *) fIn->Get("vertexHighestPtR");
  TH2F * vertexHighestJetZR = (TH2F *) fIn->Get("vertexHighestJetZR");
  TH2F * vertexHighestJetZXRot = (TH2F *) fIn->Get("vertexHighestJetZXRot");
  TH3F * vertexXYRotAjDijet = (TH3F *) fIn->Get("vertexXYRotAjDijet");

  TH2F * ptLossVertexRLeadingJet = (TH2F *) fIn->Get("ptLossVertexRLeadingJet");
  TH2F * ptLossVertexRSubleadingJet = (TH2F *) fIn->Get("ptLossVertexRSubleadingJet");

  TH1F * qqbar_pt = (TH1F *) fIn->Get("qqbar_pt");
  TH1F * qq_pt = (TH1F *) fIn->Get("qq_pt");
  TH1F * gg_pt = (TH1F *) fIn->Get("gg_pt");
  TH1F * qg_pt = (TH1F *) fIn->Get("qg_pt");

  TH1F * qqbar_PartonPtByJetBin[nJetPtBins];
  TH1F * qq_PartonPtByJetBin[nJetPtBins];
  TH1F * gq_PartonPtByJetBin[nJetPtBins];
  TH1F * qg_PartonPtByJetBin[nJetPtBins];
  TH1F * gg_PartonPtByJetBin[nJetPtBins];


  TH2F * hsPtLeadingJetPt = (TH2F *) fIn->Get("hsPtLeadingJetPt");
  TH2F * hsPtSubLeadingJetPt = (TH2F *) fIn->Get("hsPtSubLeadingJetPt");
  TH2F * hsDEtaDPhiLeadingJet = (TH2F *) fIn->Get("hsDEtaDPhiLeadingJet");
  TH2F * hsDEtaDPhiSubLeadingJet = (TH2F *) fIn->Get("hsDEtaDPhiSubLeadingJet");

  // Integral? GetEffectiveEntries? GetEntries?
  // StatOverflows?


  if (! (dEtaDPhi && etaPhi && jetPt && particleEnergy && jetEtaPhi && hWeight) ) {
    fprintf(stderr,"Error: a histogram was not found!\n");
    return;
  }

  double nevents = hWeight->GetEntries();
  double njets = jetPt->GetEntries();
  printf("This file has %.f events with %.f jets.\n",nevents,njets);

  // Jet-hadron is done more differentially

  TH2F * jetHadron[nJetPtBins][nParticlePtBins];
  TH2F * jetHadronRecoil[nJetPtBins][nParticlePtBins];
  TH2F * jetHadronNotRecoil[nJetPtBins][nParticlePtBins];
  TH2F * jetHadronOnlyJet[nJetPtBins][nParticlePtBins];
  TH2F * jetHadronExcludeJet[nJetPtBins][nParticlePtBins];
  TH1F * ptBinHadronPt[nJetPtBins];

  // for Swift background
  TH1F *swiftJetYield[nJetPtBins];
  TH1F *swiftParticleYield[nJetPtBins][nParticlePtBins];

  TString jetClass = "jetPt_%.0f_%.0f";
  TString particleClass = "particlePt_%.2f_%.2f";
  TString combinedClass = "";
  double njets_bin = 0;


  njets_bin = jetPtUnweighted->Integral(jetPt->FindBin(0),jetPt->FindBin(100));
  printf("I have %f binned jets.",njets_bin);
  njets_bin = jetPt->Integral(jetPt->FindBin(0),jetPt->FindBin(100));
  printf("I have %f binned jets after weighting..\n",njets_bin);


  for (unsigned int i = 0; i < nJetPtBins; i++)
  {
    njets_bin = jetPt->Integral(jetPt->FindBin(jetPtBins[i]),jetPt->FindBin(jetPtBins[i+1]));

    combinedClass = Form("ptBinHadronPt_" + jetClass,jetPtBins.at(i),jetPtBins.at(i+1));
    ptBinHadronPt[i] = (TH1F *) fIn->Get(combinedClass);

    combinedClass = Form("swiftJetYield_" + jetClass,jetPtBins.at(i),jetPtBins.at(i+1));
    swiftJetYield[i] = (TH1F *) fIn->Get(combinedClass);

    // Parton info
    combinedClass = Form("qqbar_PartonPtByJetBin_" + jetClass,jetPtBins.at(i),jetPtBins.at(i+1));
    qqbar_PartonPtByJetBin[i] = (TH1F *) fIn->Get(combinedClass);
    combinedClass = Form("qq_PartonPtByJetBin_" + jetClass,jetPtBins.at(i),jetPtBins.at(i+1));
    qq_PartonPtByJetBin[i] = (TH1F *) fIn->Get(combinedClass);
    combinedClass = Form("gq_PartonPtByJetBin_" + jetClass,jetPtBins.at(i),jetPtBins.at(i+1));
    gq_PartonPtByJetBin[i] = (TH1F *) fIn->Get(combinedClass);
    combinedClass = Form("qg_PartonPtByJetBin_" + jetClass,jetPtBins.at(i),jetPtBins.at(i+1));
    qg_PartonPtByJetBin[i] = (TH1F *) fIn->Get(combinedClass);
    combinedClass = Form("gg_PartonPtByJetBin_" + jetClass,jetPtBins.at(i),jetPtBins.at(i+1));
    gg_PartonPtByJetBin[i] = (TH1F *) fIn->Get(combinedClass);

    for (unsigned int j = 0; j < nParticlePtBins; j++)
    {
      combinedClass = Form("jetHadron_" + jetClass + "_" + particleClass, jetPtBins.at(i), jetPtBins.at(i+1), particlePtBins.at(j), particlePtBins.at(j+1));
      jetHadron[i][j] = (TH2F *) fIn->Get(combinedClass);
      jetHadron[i][j]->Scale(1./jetHadron[i][j]->GetXaxis()->GetBinWidth(1));

      // FIXME temporarily making the recoil, not recoil things duplicates of the
      // regular one, to have backwards compatibility for testing

      combinedClass = Form("jetHadronRecoil_" + jetClass + "_" + particleClass, jetPtBins.at(i), jetPtBins.at(i+1), particlePtBins.at(j), particlePtBins.at(j+1));
      jetHadronRecoil[i][j] = (TH2F *) jetHadron[i][j]->Clone();
      jetHadronRecoil[i][j]->SetName(combinedClass);

      //			jetHadronRecoil[i][j] = (TH2F *) fIn->Get(combinedClass);
      //	jetHadronRecoil[i][j]->Scale(1./jetHadronRecoil[i][j]->GetXaxis()->GetBinWidth(1));

      combinedClass = Form("jetHadronNotRecoil_" + jetClass + "_" + particleClass, jetPtBins.at(i), jetPtBins.at(i+1), particlePtBins.at(j), particlePtBins.at(j+1));
      jetHadronNotRecoil[i][j] = (TH2F *) jetHadron[i][j]->Clone();
      jetHadronNotRecoil[i][j]->SetName(combinedClass);
      //		jetHadronNotRecoil[i][j] = (TH2F *) fIn->Get(combinedClass);
      //			jetHadronNotRecoil[i][j]->Scale(1./jetHadronNotRecoil[i][j]->GetXaxis()->GetBinWidth(1));

      combinedClass = Form("jetHadronOnlyJet_" + jetClass + "_" + particleClass, jetPtBins.at(i), jetPtBins.at(i+1), particlePtBins.at(j), particlePtBins.at(j+1));
      jetHadronOnlyJet[i][j] = (TH2F *) fIn->Get(combinedClass);
      jetHadronOnlyJet[i][j]->Scale(1./jetHadronOnlyJet[i][j]->GetXaxis()->GetBinWidth(1));

      combinedClass = Form("jetHadronExcludeJet_" + jetClass + "_" + particleClass, jetPtBins.at(i), jetPtBins.at(i+1), particlePtBins.at(j), particlePtBins.at(j+1));
      jetHadronExcludeJet[i][j] = (TH2F *) fIn->Get(combinedClass);
      jetHadronExcludeJet[i][j]->Scale(1./jetHadronExcludeJet[i][j]->GetXaxis()->GetBinWidth(1));

      combinedClass = Form("swiftParticleYield_" + jetClass + "_" + particleClass, jetPtBins.at(i),jetPtBins.at(i+1), particlePtBins.at(j), particlePtBins.at(j+1));
      swiftParticleYield[i][j] = (TH1F *) fIn->Get(combinedClass);


    }
  }
  
  // Soft Drop histograms 
  TH2F * hJetPtZ = (TH2F *) fIn->Get("hJetPtZ");
  TH2F * hJetPtMass = (TH2F *) fIn->Get("hJetPtMass");
  TH1D * hSDJetPt = (TH1D *) fIn->Get("hSDJetPt");
  TH1D * hSDJetPtBkgSub = (TH1D *) fIn->Get("hSDJetPtBkgSub");
  

	TH1D * leadingJetPt = (TH1D *) fIn->Get("leadingJetPt"); // Might not exist for older files	




  // -----------------------------------------------------------------------------
  // NORMALIZATION
  // -----------------------------------------------------------------------------


  //now make these averages:
  double nEventsPerRun_f = nEvents/nRuns;
  totalWeightPerRun = totalWeight/nRuns;


  printf("nruns 			 	= %d\n",nRuns);
  printf("total events 	= %d\n",nEvents);
  printf("total weight 	= %f\n",totalWeight);
  printf("events/run 	 	= %f\n",nEventsPerRun_f);
  printf("weight/run   	= %f\n",totalWeightPerRun);


  //Normalizing all histograms by nfiles, so that we are effectively averaging 
  // between them all

  bool weightingOn = true; //?
  if (weightingOn) {
    double rescale = 1;
    if (totalWeight == nEvents) {
      printf("I think this is not weighted\n.");
      if (averageCrossSection != 0.) {
        rescale = averageCrossSection * (1.0/nRuns) * (1.0 / totalWeightPerRun); // =  1.0/totalWeight
      } else {
        rescale = (1.0/nRuns) * (1.0 / totalWeightPerRun); // =  1.0/totalWeight
      }

    } else {
      if (averageCrossSection != 0.) {
        rescale = averageCrossSection * (1.0/nRuns) * (1.0/totalWeightPerRun); // something about weighting?
      } else {
				printf("Ignoring Cross Section\n");
        rescale = (1.0/nRuns) * (1.0/totalWeightPerRun); // something about weighting?
      }	

      //note: it is actually the same for both cases
    }

    TObjLink *lnk = gDirectory->GetList()->FirstLink();
    TObject *obj;
    TH1 *hobj;
    while (lnk) {
      obj = gDirectory->FindObject(lnk->GetObject()->GetName());    
      lnk = lnk->Next();
      if (obj->InheritsFrom("TH1") && strcmp(obj->GetName(),"hWeight") && strcmp(obj->GetName(),"jetPtUnweighted")) {
        hobj = (TH1 *) obj;
        hobj->Scale(rescale);
      }    
    }
  } 

	// Creating leadingJetPt histogram for normalization
  if (!leadingJetPt) leadingJetPt = (TH1D *) hsPtLeadingJetPt->ProjectionY("leadingJetPt");

  for (unsigned int i = 0; i < nJetPtBins; i++) {
	// FIXME should use leading Jet pt to count triggers
 //   njets_bin = jetPt->Integral(jetPt->FindBin(jetPtBins[i]),jetPt->FindBin(jetPtBins[i+1]));
    njets_bin = leadingJetPt->Integral(jetPt->FindBin(jetPtBins[i]),jetPt->FindBin(jetPtBins[i+1]));
    if (njets_bin) {
      for (unsigned int j = 0; j < nParticlePtBins; j++) {
        jetHadron[i][j]->Scale(1./njets_bin);
        jetHadronRecoil[i][j]->Scale(1./njets_bin);
        jetHadronNotRecoil[i][j]->Scale(1./njets_bin);
        swiftParticleYield[i][j]->Scale(1./njets_bin);
      }
    }
  }
  // Normalizing histograms:
  // jetPt should be normalized by nevents
  // jet-h correlations should be normalized by njets
  //  jetPt->Scale(1./nevents);    
  //  recJetPt->Scale(1./nevents);    


  TH2F * hJetPtZNorm;
  if(hJetPtZ) {
		// Setting 0 bin in z to 0 for 
		for (int i = 0; i < hJetPtZ->GetNbinsX(); i++) {
	//		double x_cent = hJetPtZ->GetXaxis()->GetBinCenter(i+1);
			hJetPtZ->SetBinContent(i+1,1,0.0);
		}
    hJetPtZ->GetXaxis()->SetTitle("Groomed z");
    hJetPtZNorm = normalizeHistogramColumns(hJetPtZ);
    hJetPtZNorm->SetName(hJetPtZNorm->GetName());
    hJetPtZNorm->GetYaxis()->SetTitle("Groomed z");
    hJetPtZNorm->GetXaxis()->SetTitle("p_{T}^{jet} (GeV/c)");
  }
  TH2F * hJetPtMassNorm;
  if(hJetPtMass) {
    hJetPtMass->GetXaxis()->SetTitle("Mass (GeV/c^2)");
    hJetPtMassNorm = normalizeHistogramColumns(hJetPtMass);
    hJetPtMassNorm->SetName(hJetPtMassNorm->GetName());
    hJetPtMassNorm->GetYaxis()->SetTitle("Mass");
    hJetPtMassNorm->GetXaxis()->SetTitle("m^{jet} (GeV/c^2)");
  }

  // -----------------------------------------------------------------------------
  // END OF NORMALIZATION (or is it?)
  // -----------------------------------------------------------------------------

  TFile * fOut = TFile::Open(outputFilename.Data(),"RECREATE");
  if (fOut->IsOpen() == kFALSE)
  {
    Printf("outputFilename \"%s\" failed to open!", outputFilename.Data());
    std::exit(1);
  }
  fOut->Add(runInfo);
  if (parameters) fOut->Add(parameters);


  // Draw and save histograms
  TCanvas *canvas = new TCanvas("canvas","canvas",c_width,c_height);
  // Plot dEtaDPhi for multiple types of drawings
  dEtaDPhi->GetXaxis()->SetTitle("#Delta#eta");
  dEtaDPhi->GetYaxis()->SetTitle("#Delta#phi");
  dEtaDPhi->Draw("surf1");
  canvas->Print("output/dEtaDPhi.surf.pdf");
  dEtaDPhi->Draw("colz");
  canvas->Print("output/dEtaDPhi.pdf");

  fOut->Add(dEtaDPhi);

  // etaPhi plot
  etaPhi->GetXaxis()->SetRangeUser(-eta_cut,eta_cut);
  etaPhi->GetXaxis()->SetTitle("#eta");
  etaPhi->GetYaxis()->SetTitle("#phi");
  etaPhi->Draw("colz");
  canvas->Print("output/etaPhi.pdf");
  fOut->Add(etaPhi);
  // Overall energy of particle
  particleEnergy->GetXaxis()->SetTitle("E (GeV)");
  particleEnergy->Draw();
  canvas->Print("output/particleEnergy.pdf");
  fOut->Add(particleEnergy);
  // Jet spectra
  TH1F *jetPtCrossSection = (TH1F *) jetPt->Clone();
  jetPtCrossSection->SetName("jetPtCrossSection");
  jetPtCrossSection->GetXaxis()->SetTitle("P_{T} (GeV/c)");
  jetPtCrossSection->GetYaxis()->SetTitle("d^{2}#sigma/dp_{T}d#eta (mb c/GeV)");
  jetPtCrossSection->Scale(1.0/(eta_range * jetPtCrossSection->GetXaxis()->GetBinWidth(1)));
  jetPtCrossSection->Draw();
  canvas->SetLogy(1);
  canvas->Print("output/jetPtCrossSection.pdf");
  canvas->SetLogy(0);
  fOut->Add(jetPt);
  fOut->Add(jetPtUnweighted);

  // Reconstructed (Background Subtracted) Jet spectrum
  TH1F *recJetPtCrossSection = (TH1F *) recJetPt->Clone();
  recJetPtCrossSection->SetName("recJetPtCrossSection");
  recJetPtCrossSection->GetXaxis()->SetTitle("P_{T} (GeV/c)");
  recJetPtCrossSection->GetYaxis()->SetTitle("d^{2}#sigma/dp_{T}d#eta (mb c/GeV)");
  recJetPtCrossSection->Scale(1.0/(eta_range * recJetPtCrossSection->GetXaxis()->GetBinWidth(1)));
  recJetPtCrossSection->Draw();
  canvas->SetLogy(1);
  canvas->Print("output/recJetPtCrossSection.pdf");
  canvas->SetLogy(0);


  if (dijetAj) {
    dijetAj->Rebin(4);
    dijetAj->Scale(1.0/dijetAj->Integral("width"));
    canvas->Clear();
    dijetAj->Draw();
    canvas->Print("output/dijetAj.pdf");
    fOut->Add(dijetAj);
  }
  if (dijetAjJetPt) {
    fOut->Add(dijetAjJetPt);
  }

  if (dijetXj) {
    dijetXj->Rebin(4);
    dijetXj->Scale(1.0/dijetXj->Integral("width"));
    canvas->Clear();
    dijetXj->Draw();
    canvas->Print("output/dijetXj.pdf");
    fOut->Add(dijetXj);
  }
  if (dijetXjJetPt) {

    fOut->Add(dijetXjJetPt);
  }

  if (vertexR) {
    fOut->Add(vertexR);
    if (vertexXY) fOut->Add(vertexXY);
    if (vertexXYHardCore) fOut->Add(vertexXYHardCore);
    if (vertexXYNoHardCore) fOut->Add(vertexXYNoHardCore);
    if (vertexRDijetCut) fOut->Add(vertexRDijetCut);
    if (vertexXYRot) fOut->Add(vertexXYRot);
    if (vertexXYRotHardCore) fOut->Add(vertexXYRotHardCore);
    if (vertexXYRotNoHardCore) fOut->Add(vertexXYRotNoHardCore);
    if (vertexXYRotDijet) fOut->Add(vertexXYRotDijet);
    if (vertexJetPtR) fOut->Add(vertexJetPtR);
    if (vertexJetPtRHardCore) fOut->Add(vertexJetPtRHardCore);
    if (vertexJetPtRDijet) fOut->Add(vertexJetPtRDijet);
    if (vertexDijetAjR) fOut->Add(vertexDijetAjR);
    if (vertexXRotJetPt) fOut->Add(vertexXRotJetPt);
    if (vertexXRotJetPtHardCore) fOut->Add(vertexXRotJetPtHardCore);
    if (vertexXRotTriggerPt) {
      fOut->Add(vertexXRotTriggerPt);
      canvas->Clear();
      vertexXRotTriggerPt->Draw("COLZ");
      canvas->Print("output/vertexXRotTriggerPt.pdf");

      TH2F * vertexXRotTriggerPtNorm = normalizeHistogramColumns(vertexXRotTriggerPt);
      vertexXRotTriggerPtNorm->Draw("COLZ");
      canvas->SetLogz();
      canvas->Print("output/vertexXRotTriggerPtNorm.pdf");
      canvas->Clear();

    }


    TCanvas *cVertexCmp = new TCanvas("cVertexCmp","cVertexCmp",c_width,c_height);
    cVertexCmp->Divide(2,1);

    TH2F *vertexXYHCOverAll = (TH2F *) vertexXYHardCore->Clone();
    TH2F *vertexXYNoHCOverAll = (TH2F *) vertexXYNoHardCore->Clone();

    TH2F *vertexXY_Clone = (TH2F *) vertexXY->Clone();

    int rebinVertex = 2;	
    vertexXY_Clone->Rebin2D(rebinVertex,rebinVertex);
    vertexXYHCOverAll->Rebin2D(rebinVertex,rebinVertex);
    vertexXYNoHCOverAll->Rebin2D(rebinVertex,rebinVertex);

    vertexXY_Clone->Scale(1./vertexXY_Clone->Integral("width"));
    vertexXYHCOverAll->Scale(1./vertexXYHCOverAll->Integral("width"));
    vertexXYNoHCOverAll->Scale(1./vertexXYNoHCOverAll->Integral("width"));

    cVertexCmp->cd(1);
    vertexXYHCOverAll->Draw("COLZ");
    cVertexCmp->cd(2);
    vertexXYNoHCOverAll->Draw("COLZ");
    cVertexCmp->Print("output/vertexCMP.pdf");

    cVertexCmp->Clear();
    cVertexCmp->Divide(2,2);


    if(vertexXYRotDijet) {

      TH2F *vertexXYRot_Clone = (TH2F *) vertexXYRot->Clone();
      TH2F *vertexXYRotHC_Clone = (TH2F *) vertexXYRotHardCore->Clone();
      TH2F *vertexXYRotNoHC_Clone = (TH2F *) vertexXYRotNoHardCore->Clone();
      TH2F *vertexXYRotDijet_Clone = (TH2F *) vertexXYRotDijet->Clone();
      vertexXYRot_Clone->Rebin2D(rebinVertex,rebinVertex);
      vertexXYRotHC_Clone->Rebin2D(rebinVertex,rebinVertex);
      vertexXYRotNoHC_Clone->Rebin2D(rebinVertex,rebinVertex);
      vertexXYRotDijet_Clone->Rebin2D(rebinVertex,rebinVertex);
      vertexXYRot_Clone->Scale(1./vertexXYRot_Clone->Integral("width"));
      vertexXYRotHC_Clone->Scale(1./vertexXYRotHC_Clone->Integral("width"));
      vertexXYRotNoHC_Clone->Scale(1./vertexXYRotNoHC_Clone->Integral("width"));
      vertexXYRotDijet_Clone->Scale(1./vertexXYRotDijet_Clone->Integral("width"));

      cVertexCmp->cd(1);
      vertexXYRotHC_Clone->SetTitle("Hard Core Cut");
      vertexXYRotHC_Clone->Draw("COLZ");
      cVertexCmp->cd(2);
      vertexXYRotDijet_Clone->SetTitle("Dijet Cut");
      vertexXYRotDijet_Clone->Draw("COLZ");
      cVertexCmp->cd(3);
      //		vertexXYRotNoHC_Clone->Draw("COLZ");
      vertexXYRot_Clone->SetTitle("Inclusive");
      vertexXYRot_Clone->Draw("COLZ");
      cVertexCmp->Print("output/vertexRotCMP.pdf");


      //projecting:
      double N_near,N_away;
      TText *text = new TText();
      TLatex *latex = new TLatex();
      // Inclusive
      TH1F * vertexRot_Clone_px = (TH1F *) vertexXYRot_Clone->ProjectionX("_px");
      vertexRot_Clone_px->SetTitle("Jet Axis Projection");
      TH1F * vertexRot_Clone_py = (TH1F *) vertexXYRot_Clone->ProjectionY("_py");
      vertexRot_Clone_py->SetTitle("Perpendicular Axis Projection");

      //calculating s = N_{near} / N_{away}
      // including overflow, underflow bins
      N_near = vertexRot_Clone_px->Integral(0,vertexRot_Clone_px->GetXaxis()->FindBin(0.));
      N_away = vertexRot_Clone_px->Integral(vertexRot_Clone_px->GetXaxis()->FindBin(0.),vertexRot_Clone_px->GetNbinsX() + 1);

      cVertexCmp->Clear();
      cVertexCmp->Divide(2,2);
      cVertexCmp->cd(2);
      vertexXYRot_Clone->Draw("COLZ");
      cVertexCmp->cd(1);
      vertexRot_Clone_py->Draw("COLZ");
      cVertexCmp->cd(4);
      vertexRot_Clone_px->Draw("COLZ");
      cVertexCmp->cd(3);
      latex->DrawLatex(0.5,0.6,"s = N_{near}/N_{away}");
      text->DrawText(0.5,0.5,Form("s = %f",N_near / N_away));
      cVertexCmp->Print("output/vertexProjInc.pdf");

      // Hard Core Cut
      TH1F * vertexRotHC_Clone_px = (TH1F *) vertexXYRotHC_Clone->ProjectionX("_px");
      vertexRotHC_Clone_px->SetTitle("Jet Axis Projection");
      TH1F * vertexRotHC_Clone_py = (TH1F *) vertexXYRotHC_Clone->ProjectionY("_py");
      vertexRotHC_Clone_py->SetTitle("Perpendicular Axis Projection");

      N_near = vertexRotHC_Clone_px->Integral(0,vertexRotHC_Clone_px->GetXaxis()->FindBin(0.));
      N_away = vertexRotHC_Clone_px->Integral(vertexRotHC_Clone_px->GetXaxis()->FindBin(0.),vertexRotHC_Clone_px->GetNbinsX() + 1);

      cVertexCmp->Clear();
      cVertexCmp->Divide(2,2);
      cVertexCmp->cd(2);
      vertexXYRotHC_Clone->Draw("COLZ");
      cVertexCmp->cd(1);
      vertexRotHC_Clone_py->Draw("COLZ");
      cVertexCmp->cd(4);
      vertexRotHC_Clone_px->Draw("COLZ");
      cVertexCmp->cd(3);
      latex->DrawLatex(0.5,0.6,"s = N_{near}/N_{away}");
      text->DrawText(0.5,0.5,Form("s = %f",N_near / N_away));
      cVertexCmp->Print("output/vertexProjHC.pdf");

      //Dijet Cut
      TH1F * vertexRotDijet_Clone_px = (TH1F *) vertexXYRotDijet_Clone->ProjectionX("_px");
      vertexRotDijet_Clone_px->SetTitle("Jet Axis Projection");
      TH1F * vertexRotDijet_Clone_py = (TH1F *) vertexXYRotDijet_Clone->ProjectionY("_py");
      vertexRotDijet_Clone_py->SetTitle("Perpendicular Axis Projection");

      N_near = vertexRotDijet_Clone_px->Integral(0,vertexRotDijet_Clone_px->GetXaxis()->FindBin(0.));
      N_away = vertexRotDijet_Clone_px->Integral(vertexRotDijet_Clone_px->GetXaxis()->FindBin(0.),vertexRotDijet_Clone_px->GetNbinsX() + 1);

      cVertexCmp->Clear();
      cVertexCmp->Divide(2,2);
      cVertexCmp->cd(2);
      vertexXYRotDijet_Clone->Draw("COLZ");
      cVertexCmp->cd(1);
      vertexRotDijet_Clone_py->Draw("COLZ");
      cVertexCmp->cd(4);
      vertexRotDijet_Clone_px->Draw("COLZ");
      cVertexCmp->cd(3);
      latex->DrawLatex(0.5,0.6,"s = N_{near}/N_{away}");
      text->DrawText(0.5,0.5,Form("s = %f",N_near / N_away));
      cVertexCmp->Print("output/vertexProjDijet.pdf");
    }



    //now using R
    cVertexCmp->Clear();
    int rebinVertexR = 1;	

    vertexR->Rebin(rebinVertexR);
    vertexRHardCore->Rebin(rebinVertexR);
    //	vertexRNoHardCore->Rebin(rebinVertexR);
    vertexRDijetCut->Rebin(rebinVertexR);

    vertexR->Scale(1.0/vertexR->Integral("width"));
    vertexRHardCore->Scale(1.0/vertexRHardCore->Integral("width"));
    //		vertexRNoHardCore->Scale(1.0/vertexRNoHardCore->Integral("width"));
    vertexRDijetCut->Scale(1.0/vertexRDijetCut->Integral("width"));

    vertexR->SetLineColor(kBlack);
    vertexR->SetMarkerColor(kBlack);
    vertexR->SetMarkerStyle(kFullSquare);
    vertexRDijetCut->SetLineColor(kRed);
    vertexRDijetCut->SetMarkerColor(kRed);
    vertexRDijetCut->SetMarkerStyle(kFullStar);
    vertexRDijetCut->SetMarkerSize(2);
    //		vertexRNoHardCore->SetLineColor(kBlue);
    vertexRHardCore->SetLineColor(kBlue);
    vertexRHardCore->SetMarkerColor(kBlue);
    vertexRHardCore->SetMarkerStyle(8);
    vertexRHardCore->SetMarkerSize(1);

    vertexR->Draw("LP");
    vertexRHardCore->Draw("LP SAME");
    //	vertexRNoHardCore->Draw("LP SAME");
    vertexRDijetCut->Draw("LP SAME");


    TLegend * legVertex = new TLegend(0.50, 0.65, 0.90, 0.85);
    legVertex->AddEntry(vertexR, "All", "lp");
    legVertex->AddEntry(vertexRHardCore, "Hard Core (>6 GeV/c) Jets", "lp");
    //	legVertex->AddEntry(vertexRNoHardCore, "Removed Jets", "l");
    legVertex->AddEntry(vertexRDijetCut, "Dijet Cut","lp");
    legVertex->Draw("same");

    cVertexCmp->Print("output/vertexRCMP.pdf");


    // the vertexJetPtR is stored in a TH2, so I have to project it into different
    // histograms to easily scale it, and make useful plots.

    if (vertexJetPtR ) {

      int rebinVertexJetPtR = 1;	
      TH1F * vertexJetPtRBin[nJetPtBins];
      TH1F * vertexJetPtRHardCoreBin[nJetPtBins];
      TH1F * vertexJetPtRDijetBin[nJetPtBins];
      TLegend * legVertexJetPtRBin[nJetPtBins];
      for (int i = 0; i < nJetPtBins; i++) {
        combinedClass = Form("vertexR_" + jetClass, jetPtBins.at(i), jetPtBins.at(i+1));
        vertexJetPtRBin[i] = (TH1F *) vertexJetPtR->ProjectionY(combinedClass,i,i+1,"e");
        vertexJetPtRBin[i]->Rebin(rebinVertexJetPtR);
        double scale = vertexJetPtRBin[i]->Integral("width");
        scale = scale != 0 ? 1./scale : 0;
        vertexJetPtRBin[i]->Scale(scale);
        vertexJetPtRBin[i]->SetTitle(Form("%0.f < p_{T}^{jet} < %0.f",jetPtBins.at(i),jetPtBins.at(i+1)));			


        combinedClass = Form("vertexRHardCore_" + jetClass, jetPtBins.at(i), jetPtBins.at(i+1));
        vertexJetPtRHardCoreBin[i] = (TH1F *) vertexJetPtRHardCore->ProjectionY(combinedClass,i,i+1,"e");
        vertexJetPtRHardCoreBin[i]->Rebin(rebinVertexJetPtR);
        scale = vertexJetPtRHardCoreBin[i]->Integral("width");
        scale = scale != 0 ? 1./scale : 0;
        vertexJetPtRHardCoreBin[i]->Scale(scale);

        combinedClass = Form("vertexRDijet_" + jetClass, jetPtBins.at(i), jetPtBins.at(i+1));
        vertexJetPtRDijetBin[i] = (TH1F *) vertexJetPtRDijet->ProjectionY(combinedClass,i,i+1,"e");
        vertexJetPtRDijetBin[i]->Rebin(rebinVertexJetPtR);
        scale = vertexJetPtRDijetBin[i]->Integral("width");
        scale = scale != 0 ? 1./scale : 0;
        vertexJetPtRDijetBin[i]->Scale(scale);

        legVertexJetPtRBin[i] = new TLegend(0.50,0.65,0.90,0.85); 

      }
      TCanvas *cVertexJetPtR = new TCanvas("cVertexJetPtR","cVertexJetPtR",c_width,c_height);
      cVertexJetPtR->Divide(TMath::CeilNint(TMath::Sqrt(nJetPtBins)), 
          TMath::FloorNint(TMath::Sqrt(nJetPtBins)));


      //actually, should make it so it compares hard core to none?

      //		TLegend * legVertex2 = new TLegend(0.50, 0.65, 0.90, 0.85);
      //		TString style = "LP";
      for (int i = 0; i < nJetPtBins; i++) {
        cVertexJetPtR->cd(i+1);

        vertexJetPtRBin[i]->SetLineColor(1);
        vertexJetPtRBin[i]->SetMarkerStyle(33);
        vertexJetPtRBin[i]->SetMarkerColor(1);
        vertexJetPtRHardCoreBin[i]->SetLineColor(2);
        vertexJetPtRHardCoreBin[i]->SetMarkerStyle(33);
        vertexJetPtRHardCoreBin[i]->SetMarkerColor(2);
        vertexJetPtRDijetBin[i]->SetLineColor(3);
        vertexJetPtRDijetBin[i]->SetMarkerStyle(33);
        vertexJetPtRDijetBin[i]->SetMarkerColor(3);

        //			if (i == 1) style = "SAME LP";
        vertexJetPtRBin[i]->Draw("LP");
        vertexJetPtRHardCoreBin[i]->Draw("LP SAME");

        if (i>1) vertexJetPtRDijetBin[i]->Draw("LP SAME");
        //			vertexJetPtRBin[i]->GetYaxis()->SetRangeUser(0,1.1*max(vertexJetPtRBin[i]->GetBinContent(vertexJetPtRBin[i]->GetMaximumBin()),max(vertexJetPtRHardCoreBin[i]->GetBinContent(vertexJetPtRHardCoreBin[i]->GetMaximumBin()),vertexJetPtRDijetBin[i]->GetBinContent(vertexJetPtRDijetBin[i]->GetMaximumBin()))));			


        legVertexJetPtRBin[i]->AddEntry(vertexJetPtRBin[i],"All","lp");			
        legVertexJetPtRBin[i]->AddEntry(vertexJetPtRHardCoreBin[i],"Hard Core (>6 GeV/c)","lp");			
        if (i>1) legVertexJetPtRBin[i]->AddEntry(vertexJetPtRDijetBin[i],"Dijet Cut","lp");			

        legVertexJetPtRBin[i]->Draw("SAME");

      }
      cVertexJetPtR->Print("output/vertexJetPtRCmp.pdf");


      // Comparing R vs. jet Pt
      cVertexJetPtR->Clear();
      TLegend * legVertexJetPtR = new TLegend(0.50,0.65,0.90,0.85); 

      TString storeTitle = vertexR->GetTitle();
      vertexR->SetTitle("Vertex R by p^{jet}_{T}");
      vertexR->SetMarkerStyle(kOpenSquare);
      vertexR->SetMarkerColor(kBlack);
      vertexR->SetLineColor(kBlack);    
      vertexR->SetLineStyle(1);    


      vertexR->Draw("LP");
      legVertexJetPtR->AddEntry(vertexR,"All Vertices","lp");


      int colors[6] = {1,2,3,7,4,6};

      for (int i = 0; i < nJetPtBins; i++) {
        vertexJetPtRBin[i]->SetMarkerColor(colors[i+1]);
        vertexJetPtRBin[i]->SetLineColor(colors[i+1]);
        //        vertexJetPtRBin[i]->SetMarkerColor(i+2);
        //        vertexJetPtRBin[i]->SetLineColor(i+2);
        //    if (!i)  vertexJetPtRBin[i]->Draw("LP");
        vertexJetPtRBin[i]->Draw("LP SAME");
        legVertexJetPtR->AddEntry(vertexJetPtRBin[i],vertexJetPtRBin[i]->GetTitle(),"lp"); 
      }
      legVertexJetPtR->Draw("SAME");
      cVertexJetPtR->Print("output/vertexJetPtR.pdf");
      vertexR->SetTitle(storeTitle);



      // vertexR highest Pt stuff
      // FLEP
      //vertexHighestPtR
      TH2F *vertexHCCutR = (TH2F *) vertexHighestPtR->Clone();
      vertexHCCutR->SetName("vertexHCCutR");		
      vertexHCCutR->SetTitle("Vertex R vs Hard Core Cut");		

      TCanvas *cVertexHighestPtR = new TCanvas("cVertexHighestPtR","cVertexHighestPtR",c_width,c_height);
      cVertexHighestPtR->cd();
      vertexHighestPtR->GetXaxis()->SetRangeUser(2.,30.);
      vertexHighestPtR->Draw("COLZ");
      cVertexHighestPtR->SetLogz();
      cVertexHighestPtR->Print("output/vertexHighestPtR.pdf");
      vertexHighestPtR->GetXaxis()->SetRangeUser(0.,100.);

      //each bin (pt,R) in vertexHCCutR will be filled with the integral 
      // \int_{pt}^{\infty} d(pt') vertexHighestPtR(pt',R)

      // this is inefficient with respect to memory reading, but saves
      // time and memory.
      int nbinsx = vertexHighestPtR->GetNbinsX();
      int nbinsy = vertexHighestPtR->GetNbinsY();

      for (int j = 0; j < nbinsy; j++) {
        //manually do the integral starting from infinity
        // filling in vertexHCCutR along the way 
        double integral;
        // add in overflow
        integral = vertexHighestPtR->GetBinContent(vertexHighestPtR->GetBin(nbinsx+1,j));
        for (int i = nbinsx ; i >= 0; i--) {
          // do integral
          integral += vertexHighestPtR->GetBinContent(vertexHighestPtR->GetBin(i,j));
          // set value
          vertexHCCutR->SetBinContent(vertexHCCutR->GetBin(i,j),integral); 				
        }
      }

      cVertexHighestPtR->cd();
      vertexHCCutR->GetXaxis()->SetRangeUser(2.,30.);
      vertexHCCutR->Draw("COLZ");
      cVertexHighestPtR->Print("output/vertexHCCutR.pdf");
      vertexHCCutR->GetXaxis()->SetRangeUser(0.,100.);

      // Normalized version
      TH2F *vertexHCCutRNorm = (TH2F *) vertexHCCutR->Clone();
      vertexHCCutRNorm->SetName("vertexHCCutRNorm");
      vertexHCCutRNorm->SetTitle("Vertex R vs Hard Core Cut (normalized)");
      TH2F *vertexHCCutRScale = (TH2F *) vertexHCCutR->Clone();
      vertexHCCutRScale->SetName("vertexHCCutRScale");
      vertexHCCutRScale->SetTitle("vertexHCCutRScale");

      for (int i = 0; i < nbinsx+1; i++) {
        double integral = vertexHCCutR->Integral(i,i+1,0,nbinsy+1,"width");
        integral = integral ? 1./integral : 0; // lol
        for (int j = 0; j < nbinsy+1; j++) {
          vertexHCCutRScale->SetBinContent(i,j,integral);
        }
      }
      //scaling so each column is normalized
      vertexHCCutRNorm->Multiply(vertexHCCutRScale);
      //drawing the norms as a test
      cVertexHighestPtR->cd(); cVertexHighestPtR->Clear();
      // showing profile as well
      vertexHCCutRNorm->GetXaxis()->SetRangeUser(0.,20.);
      vertexHCCutRNorm->GetYaxis()->SetRangeUser(0.,10.);
      vertexHCCutRNorm->Draw("COLZ");
      cVertexHighestPtR->SetLogz(1);
      TProfile *vertexHCCutRNorm_px = vertexHCCutRNorm->ProfileX("_pfx",1,-1,"s");
      //vertexHCCutRNorm_px->SetMarkerStyle(kOpenSquare);
      vertexHCCutRNorm_px->SetLineWidth(2);
      vertexHCCutRNorm_px->SetLineColor(1);
      vertexHCCutRNorm_px->SetMarkerColor(1);
      vertexHCCutRNorm_px->Draw("SAME");

      cVertexHighestPtR->Print("output/vertexHCCutRNorm.pdf");
      vertexHCCutRNorm->GetXaxis()->SetRangeUser(0.,100.);


      // vertexHighestPtXRot stuff
      if (vertexHighestPtXRot) {

        TH2F *vertexHCCutXRot = (TH2F *) vertexHighestPtXRot->Clone();
        vertexHCCutXRot->SetName("vertexHCCutXRot");		
        vertexHCCutXRot->SetTitle("Vertex x_{rot} vs Hard Core Cut");		

        TCanvas *cVertexHighestPtXRot = new TCanvas("cVertexHighestPtXRot","cVertexHighestPtXRot",c_width,c_height);
        cVertexHighestPtXRot->cd();
        vertexHighestPtXRot->GetXaxis()->SetRangeUser(2.,30.);
        vertexHighestPtXRot->Draw("COLZ");
        cVertexHighestPtXRot->SetLogz();
        cVertexHighestPtXRot->Print("output/vertexHighestPtXRot.pdf");
        vertexHighestPtXRot->GetXaxis()->SetRangeUser(0.,100.);

        //each bin (pt,R) in vertexHCCutR will be filled with the integral 
        // \int_{pt}^{\infty} d(pt') vertexHighestPtR(pt',R)

        // this is inefficient with respect to memory reading, but saves
        // time and memory.
        nbinsx = vertexHighestPtXRot->GetNbinsX();
        nbinsy = vertexHighestPtXRot->GetNbinsY();

        for (int j = 0; j < nbinsy; j++) {
          //manually do the integral starting from infinity
          // filling in vertexHCCutR along the way 
          double integral;
          // add in overflow
          integral = vertexHighestPtXRot->GetBinContent(vertexHighestPtXRot->GetBin(nbinsx+1,j));
          for (int i = nbinsx ; i >= 0; i--) {
            // do integral
            integral += vertexHighestPtXRot->GetBinContent(vertexHighestPtXRot->GetBin(i,j));
            // set value
            vertexHCCutXRot->SetBinContent(vertexHCCutR->GetBin(i,j),integral); 				
          }
        }

        cVertexHighestPtXRot->cd();
        vertexHCCutXRot->GetXaxis()->SetRangeUser(2.,30.);
        vertexHCCutXRot->Draw("COLZ");
        cVertexHighestPtXRot->Print("output/vertexHCCutXRot.pdf");
        vertexHCCutXRot->GetXaxis()->SetRangeUser(0.,100.);

        // Normalized version
        TH2F *vertexHCCutXRotNorm = (TH2F *) vertexHCCutXRot->Clone();
        vertexHCCutXRotNorm->SetName("vertexHCCutXRotNorm");
        vertexHCCutXRotNorm->SetTitle("Vertex x_{rot} vs Hard Core Cut (normalized)");
        vertexHCCutXRotNorm->GetXaxis()->SetTitle("Const. Cut (GeV/c)");
        TH2F *vertexHCCutXRotScale = (TH2F *) vertexHCCutXRot->Clone();
        vertexHCCutXRotScale->SetName("vertexHCCutXRotScale");
        vertexHCCutXRotScale->SetTitle("vertexHCCutXRotScale");

        for (int i = 0; i < nbinsx+1; i++) {
          double integral = vertexHCCutXRot->Integral(i,i+1,0,nbinsy+1,"width");
          integral = integral ? 1./integral : 0; // lol
          for (int j = 0; j < nbinsy+1; j++) {
            vertexHCCutXRotScale->SetBinContent(i,j,integral);
          }
        }
        //scaling so each column is normalized
        vertexHCCutXRotNorm->Multiply(vertexHCCutXRotScale);
        //drawing the norms as a test
        cVertexHighestPtXRot->cd(); cVertexHighestPtXRot->Clear();
        // showing profile as well
        vertexHCCutXRotNorm->GetXaxis()->SetRangeUser(0.,20.);
        vertexHCCutXRotNorm->GetYaxis()->SetRangeUser(-10.,10.);
        vertexHCCutXRotNorm->Draw("COLZ");
        cVertexHighestPtXRot->SetLogz(1);
        TProfile *vertexHCCutXRotNorm_px = vertexHCCutXRotNorm->ProfileX("_pfx",1,-1,"s");
        //vertexHCCutRNorm_px->SetMarkerStyle(kOpenSquare);
        vertexHCCutXRotNorm_px->SetLineWidth(2);
        vertexHCCutXRotNorm_px->SetLineColor(1);
        vertexHCCutXRotNorm_px->SetMarkerColor(1);
        vertexHCCutXRotNorm_px->Draw("SAME");

        cVertexHighestPtXRot->Print("output/vertexHCCutXRotNorm.pdf");
        vertexHCCutXRotNorm->GetXaxis()->SetRangeUser(0.,100.);
      }



      if(vertexHighestJetZR) {
        fOut->Add(vertexHighestJetZR);
        cVertexHighestPtR->Clear();
        cVertexHighestPtR->SetLogz();
        vertexHighestJetZR->Draw("COLZ");
        cVertexHighestPtR->Print("output/vertexHighestJetZR.pdf");
        cVertexHighestPtR->Clear();
        TH2F * vertexHighestJetZRNorm = normalizeHistogramColumns(vertexHighestJetZR);
        TProfile * vertexHighestJetZRNorm_pfx = vertexHighestJetZRNorm->ProfileX("_pfx",1,-1,"s");
        vertexHighestJetZRNorm_pfx->SetLineColor(kBlack);
        vertexHighestJetZRNorm->Draw("COLZ");
        vertexHighestJetZRNorm_pfx->Draw("SAME");
        cVertexHighestPtR->Print("output/vertexHighestJetZRNorm.pdf");
      }

      //		legVertex2->AddEntry(vertexJetPtRBin[i],Form("%.0f < p_{T}^{jet} < %.0f",jetPtBins.at(i),jetPtBins.at(i+1)),"lp");
    }
    //	TString jetClass = "jetPt_%.0f_%.0f";
    //		legVertex2->Draw("SAME");

  }





  if (trackPt) fOut->Add(trackPt);
  if(gammaEt) fOut->Add(gammaEt);
  if(recJetPt) fOut->Add(recJetPt);

  //add swift?

  canvas->Clear();
  jetEtaPhi->GetXaxis()->SetRangeUser(-eta_cut,eta_cut);
  jetEtaPhi->GetXaxis()->SetTitle("#eta");
  jetEtaPhi->GetYaxis()->SetTitle("#phi");
  jetEtaPhi->Draw("colz");
  canvas->Print("output/jetEtaPhi.pdf");
  fOut->Add(jetEtaPhi);

  if (leadingJetEtaPhi) {
    canvas->Clear();
    leadingJetEtaPhi->GetXaxis()->SetRangeUser(-eta_cut,eta_cut);
    leadingJetEtaPhi->GetXaxis()->SetTitle("#eta");
    leadingJetEtaPhi->GetYaxis()->SetTitle("#phi");
    leadingJetEtaPhi->Draw("colz");
    canvas->Print("output/leadingJetEtaPhi.pdf");
    fOut->Add(leadingJetEtaPhi);
  }

  // JetH
  canvas->Clear();

  Int_t index = 1;

  for (unsigned int i = 0; i < nJetPtBins; i++)
  {
    index = 1;
    canvas->Clear();
    canvas->Divide(TMath::CeilNint(TMath::Sqrt(nParticlePtBins)), 
        TMath::FloorNint(TMath::Sqrt(nParticlePtBins)));
    combinedClass = Form("output/jetHadron_" + jetClass + ".pdf",
        jetPtBins.at(i),jetPtBins.at(i+1));

    for (unsigned int j = 0; j < nParticlePtBins; j++)
    {
      canvas->cd(index);
      jetHadron[i][j]->SetContour(20);     
      jetHadron[i][j]->Draw("COLZ");
      //		canvas->GetPad(index)->SetLogz();
      //	jetHadron[i][j]->Draw("colz");
      index++;
    }
    canvas->Print(combinedClass);
  }
  //	canvas->Print("output/jetHadron.pdf");
  // Reset the divide canvas;
  canvas->cd();

  // Fit projections and draw
  // 2 particle correlations
  // It has to be a TH1D according to root documentation
  TH1D * phiProjection = dEtaDPhi->ProjectionY("phiProjection");
  minMax nearSideLimits(-PI/4, PI/4);
  minMax awaySideLimits(3.0*PI/4, 5.0*PI/4);

  // Perform fit
  TF1 * fullFit = phiCorrFit(phiProjection, "fullFit");
  //	TF1 * nearSideFit = fitPeak(phiProjection, nearSideLimits, "nearSide");
  //	TF1 * awaySideFit = fitPeak(phiProjection, awaySideLimits, "awaySide");


  // NEW METHOD
  //	phiCorrFit_2Gaus

  TF1 * fullFit2 = phiCorrFit_2Gaus(phiProjection,"fullFit2");



  // Set fit parameters
  //	nearSideFit->SetLineColor(kRed);
  //	awaySideFit->SetLineColor(kBlue);

  fullFit->SetLineColor(kViolet);
  fullFit->SetLineStyle(2);
  fullFit2->SetLineColor(kViolet);

  // Draw projection and fit
  phiProjection->Draw();
  //	nearSideFit->Draw("same");
  //	awaySideFit->Draw("same");
  //  fullFit->Draw("SAME");
  fullFit2->Draw("SAME");


  // Save projection
  canvas->Print("output/phiProjection.pdf");

  // For eta within +/- 0.5
  TH1D * phiProjectionEtaCut = dEtaDPhi->ProjectionY("phiProjectionEtaCut", 25, 75);
  TF1 * etaCutFullFit = phiCorrFit(phiProjectionEtaCut, "etaCutFullFit");
  //	TF1 * nearSideEtaCutFit = fitPeak(phiProjectionEtaCut, nearSideLimits, "nearSideEtaCut");
  //	TF1 * awaySideEtaCutFit = fitPeak(phiProjectionEtaCut, awaySideLimits, "awaySideEtaCut");

  // Set fit parameters
  //	nearSideEtaCutFit->SetLineColor(kRed);
  //	awaySideEtaCutFit->SetLineColor(kBlue);

  etaCutFullFit->SetLineColor(kViolet);

  // Draw projection and fit
  phiProjectionEtaCut->Draw();
  //	nearSideEtaCutFit->Draw("same");
  //	awaySideEtaCutFit->Draw("same");
  etaCutFullFit->Draw("SAME");

  // Save projection
  canvas->Print("output/phiProjectionEtaCut.pdf");

  // Jet Hadron
  minMax nearSideJetHLimits(-PI/6, PI/6);
  minMax awaySideJetHLimits(3.0*PI/4, 5.0*PI/4);

  // Setup Canvas
  canvas->Clear();

  // Define Histograms
  TH1D * jetHProjection[nJetPtBins][nParticlePtBins];
  TH1D * jetHdEtaProjection[nJetPtBins][nParticlePtBins];
  TH1D * jetHRecoilProjection[nJetPtBins][nParticlePtBins];
  TH1D * jetHNotRecoilProjection[nJetPtBins][nParticlePtBins];

  // Fit Functions
  TF1 * jetHdEtaProjectionFit[nJetPtBins][nParticlePtBins];
  TF2 * jetHdEtadPhiFit[nJetPtBins][nParticlePtBins];
  TF2 * jetHFit[nJetPtBins][nParticlePtBins]; //one used to subtract
  TF1 * jetHProjectionFit[nJetPtBins][nParticlePtBins];
  TF1 * jetHProjectionFit2[nJetPtBins][nParticlePtBins];
  TH2F * jetHadronBkgSub[nJetPtBins][nParticlePtBins];

  TCanvas *cJetHBkgSub = new TCanvas("cJetBkgSub","cJetBkgSub",c_width,c_height);
  TCanvas *cJetHdEtaFit = new TCanvas("cJetHdEtaFit","cJetHdEtaFit",c_width,c_height);
  TCanvas *cJetHdEtadPhiFit = new TCanvas("cJetHdEtadPhiFit","cJetHdEtadPhiFit",c_width,c_height);


  TString combinedNames;
  // Vectors for widths





	// Awayside Variables
  vector < vector <double> > ptBinASWidths;
  vector < vector <double> > ptBinASWidthsErr;
  vector < vector <double> > ptBinASRms;
  vector < vector <double> > ptBinASRmsErr;
  vector < vector <double> > ptBinASFwhm;
  vector < vector <double> > ptBinASFwhmErr;
  vector < vector <double> > ptBinASFwhmRescaled;
  vector < vector <double> > ptBinASFwhmRescaledErr;
  vector < vector <double> > ptBinASYields;
  vector < vector <double> > ptBinASYieldsErr;
  vector < vector <double> > ptBinASIntegrals;
  vector < vector <double> > ptBinASIntegralsErr;
  vector < vector <double> > ptBinASMeans;
  vector < vector <double> > ptBinASMeansErr;
  vector < vector <double> > ptBinASBetas;
  vector < vector <double> > ptBinASBetasErr;

	// Background Variables
  vector < vector <double> > ptBinBs;
  vector < vector <double> > ptBinBsErr;

	// Goodness of Fit
	vector < vector <double> > ptBinChiSq;
	vector < vector <double> > ptBinChiSqOverNDF;


  for (int i = 0; i < nJetPtBins; i++ ) {
    vector <double> ASWidthsRow;
    vector <double> ASWidthsErrRow;
    vector <double> ASRmsRow;
    vector <double> ASRmsErrRow;
    vector <double> ASFwhmRow;
    vector <double> ASFwhmErrRow;
    vector <double> ASFwhmRescaledRow;
    vector <double> ASFwhmRescaledErrRow;
    vector <double> ASYieldsRow;
    vector <double> ASYieldsErrRow;
    vector <double> ASIntegralsRow;
    vector <double> ASIntegralsErrRow;
    vector <double> ASMeansRow;
    vector <double> ASMeansErrRow;
    vector <double> ASBetasRow;
    vector <double> ASBetasErrRow;
    vector <double> BsRow;
    vector <double> BsErrRow;
		vector <double> ChiSqRow;
		vector <double> ChiSqOverNDFRow;

    for(int j = 0; j < nParticlePtBins; j++) {
      ASWidthsRow.push_back(0.0);
      ASWidthsErrRow.push_back(0.0);
      ASRmsRow.push_back(0.0);
      ASRmsErrRow.push_back(0.0);
      ASFwhmRow.push_back(0.0);
      ASFwhmErrRow.push_back(0.0);
      ASFwhmRescaledRow.push_back(0.0);
      ASFwhmRescaledErrRow.push_back(0.0);
      ASMeansRow.push_back(0.0);
      ASMeansErrRow.push_back(0.0);
      ASYieldsRow.push_back(0.0);
      ASYieldsErrRow.push_back(0.0);
      ASIntegralsRow.push_back(0.0);
      ASIntegralsErrRow.push_back(0.0);
      ASBetasRow.push_back(0.0);
      ASBetasErrRow.push_back(0.0);
      BsRow.push_back(0.0);
      BsErrRow.push_back(0.0);
			ChiSqRow.push_back(0.0);
			ChiSqOverNDFRow.push_back(0.0);
    }

    ptBinASWidths.push_back(ASWidthsRow);
    ptBinASWidthsErr.push_back(ASWidthsErrRow);
    ptBinASRms.push_back(ASRmsRow);
    ptBinASRmsErr.push_back(ASRmsErrRow);
    ptBinASFwhm.push_back(ASFwhmRow);
    ptBinASFwhmErr.push_back(ASFwhmErrRow);
    ptBinASFwhmRescaled.push_back(ASFwhmRescaledRow);
    ptBinASFwhmRescaledErr.push_back(ASFwhmRescaledErrRow);
    ptBinASYields.push_back(ASYieldsRow);
    ptBinASYieldsErr.push_back(ASYieldsErrRow);
    ptBinASIntegrals.push_back(ASIntegralsRow);
    ptBinASIntegralsErr.push_back(ASIntegralsErrRow);
    ptBinASMeans.push_back(ASMeansRow);
    ptBinASMeansErr.push_back(ASMeansErrRow);
    ptBinASBetas.push_back(ASBetasRow);
    ptBinASBetasErr.push_back(ASBetasErrRow);
    ptBinBs.push_back(BsRow);
    ptBinBsErr.push_back(BsErrRow);
		ptBinChiSq.push_back(ChiSqRow);
		ptBinChiSqOverNDF.push_back(ChiSqOverNDFRow);
  }

  TF1 * fitSwiftJet[nJetPtBins];
  TF1 * fitSwiftParticle[nJetPtBins][nParticlePtBins];
  TF2 * swiftBackground[nJetPtBins][nParticlePtBins];

  TCanvas *cBkgSub = new TCanvas("cBkgSub","cBkgSub",c_width,c_height); // canvas for AS with bkgsub
  TCanvas *cProjCmp = new TCanvas("cProjCmp","cProjCmp",c_width,c_height); // canvas for comparing 
  // all, recoil, notRecoil dPhi projections
  TCanvas *cSwiftJet = new TCanvas("cSwiftJet","cSwiftJet",c_width,c_height);
  TCanvas *cSwiftParticle = new TCanvas("cSwiftParticle","cSwiftParticle",c_width,c_height);
  TCanvas *cSwiftBackground = new TCanvas("cSwiftBackground","cSwiftBackground",c_width,c_height);

  TString sJetHProjection2 = "";
  TString sBkgSub = "";
  TString sSwiftJet = "output/swiftJet.pdf";
  TString sSwiftParticle = "";
  TString sSwiftBackground = "";
  TString sJetHBkgSub = "";	
  TString sJetHdEtaFit = "";
  TString sJetHdEtadPhiFit = "";

  cSwiftJet->Divide(TMath::CeilNint(TMath::Sqrt(nJetPtBins)),
      TMath::FloorNint(TMath::Sqrt(nJetPtBins)));

  int n_x_SubDiv = TMath::CeilNint(TMath::Sqrt(nParticlePtBins));
  int n_y_SubDiv = TMath::FloorNint(TMath::Sqrt(nParticlePtBins));
  for (unsigned int i = 0; i < nJetPtBins; i++)
  {
    index = 0;
    canvas->Clear();
    canvas->Divide(n_x_SubDiv,n_y_SubDiv);
    cBkgSub->Clear();
    cBkgSub->Divide(n_x_SubDiv,n_y_SubDiv);
    cProjCmp->Clear();
    cProjCmp->Divide(n_x_SubDiv,n_y_SubDiv);
    cSwiftParticle->Clear();
    cSwiftParticle->Divide(n_x_SubDiv,n_y_SubDiv);
    cSwiftBackground->Clear();
    cSwiftBackground->Divide(n_x_SubDiv,n_y_SubDiv);
    cJetHBkgSub->Clear();
    cJetHBkgSub->Divide(n_x_SubDiv,n_y_SubDiv);
    cJetHdEtaFit->Clear();   
    cJetHdEtaFit->Divide(n_x_SubDiv,n_y_SubDiv);
    cJetHdEtadPhiFit->Clear();   
    cJetHdEtadPhiFit->Divide(n_x_SubDiv,n_y_SubDiv);

    combinedClass = Form("output/jetHProjection_" + jetClass + ".pdf",
        jetPtBins.at(i),jetPtBins.at(i+1));
    sBkgSub = Form("output/jetHProjectionBkgSub_" + jetClass + ".pdf",
        jetPtBins.at(i),jetPtBins.at(i+1));
    sSwiftParticle = Form("output/swiftParticle_" + jetClass + ".pdf",
        jetPtBins.at(i),jetPtBins.at(i+1));		
    sSwiftBackground = Form("output/swiftBackground_" + jetClass + ".pdf",
        jetPtBins.at(i),jetPtBins.at(i+1));		
    sJetHBkgSub = Form("output/jetHBkgSub_" + jetClass + ".pdf",
        jetPtBins.at(i),jetPtBins.at(i+1));		
    sJetHdEtaFit = Form("output/jetHdEtaFit_" + jetClass + ".pdf",
        jetPtBins.at(i),jetPtBins.at(i+1));		
    sJetHdEtadPhiFit = Form("output/jetHdEtadPhiFit_" + jetClass + ".pdf",
        jetPtBins.at(i),jetPtBins.at(i+1));		




    cSwiftJet->cd(i+1);
    swiftJetYield[i]->SetMarkerStyle(1);
    swiftJetYield[i]->SetMarkerColor(6);
    swiftJetYield[i]->SetMarkerSize(1);
    swiftJetYield[i]->SetLineColor(6);

    swiftJetYield[i]->SetTitle("");	
    swiftJetYield[i]->Draw();

    combinedNames = swiftJetYield[i]->GetName();
    combinedNames += "_fit";
   	fitSwiftJet[i] = swiftJetFit(swiftJetYield[i],combinedNames.Data());
    fitSwiftJet[i]->SetLineColor(9);
    fitSwiftJet[i]->SetLineStyle(9);
    fitSwiftJet[i]->Draw("SAME");



    for (unsigned int j = 0; j < nParticlePtBins; j++)
    {
      // Set appropraite part of canvas
      index++;

			printf("======================================================================\n");
      printf("|| Fitting JetHadron Jet %d (%.1f <= p_{T}^{jet} < %.1f GeV/c) Hadron %d (%.1f <= p_{T}^{assoc} < %.1f GeV/c) \n",i,jetPtBins.at(i),jetPtBins.at(i+1),j,particlePtBins.at(j),particlePtBins.at(j+1));
			printf("======================================================================\n");

      // SWIFT
      // -------------------------------------------------------
      cSwiftParticle->cd(index);
      swiftParticleYield[i][j]->SetMarkerStyle(1);
      swiftParticleYield[i][j]->SetMarkerColor(kBlue);
      swiftParticleYield[i][j]->SetMarkerSize(1);
      swiftParticleYield[i][j]->SetLineColor(kBlue);
      swiftParticleYield[i][j]->SetTitle(Form("Particle Yield for %.1f < p_{T}^{jet} < %.f GeV/c, %.2f < p_{T}^{part} < %.2f",jetPtBins.at(i),jetPtBins.at(i+1),particlePtBins.at(j),particlePtBins.at(j+1)));
      swiftParticleYield[i][j]->Draw();


      combinedNames = swiftParticleYield[i][j]->GetName();
      combinedNames += "_fit";
      			fitSwiftParticle[i][j] = swiftParticleFit(swiftParticleYield[i][j],combinedNames.Data());
      			fitSwiftParticle[i][j]->SetLineColor(kCyan);
      			fitSwiftParticle[i][j]->SetLineStyle(kCyan);
//      			fitSwiftParticle[i][j]->Draw("SAME");


      // Create the background functions.


      combinedNames = swiftParticleYield[i][j]->GetName();
      combinedNames += "_swiftBackground";
    //  			swiftBackground[i][j] = createSwiftBackground(fitSwiftJet[i],fitSwiftParticle[i][j],jetHadron[i][j],combinedNames.Data());
   //   		swiftBackground[i][j]->SetLineColor(6);
      //



      // -------------------------------------------------------

      // 2D fit to get combinatorial background
//      cJetHdEtadPhiFit->cd(index);


      //CURRENT
/*      if (use2DFit) {
        jetHdEtadPhiFit[i][j] = etaPhiCorr_fit(jetHadron[i][j], combinedNames.Data());
        jetHdEtadPhiFit[i][j]->SetContour(20);
        jetHdEtadPhiFit[i][j]->Draw("SURF2");
      }
*/

      double dPhi_Bkg_min = -PI/2;
      double dPhi_Bkg_max = PI/2;
      int dPhi_Bkg_min_bin = jetHadron[i][j]->GetYaxis()->FindFixBin(dPhi_Bkg_min);
      int dPhi_Bkg_max_bin = jetHadron[i][j]->GetYaxis()->FindFixBin(dPhi_Bkg_max);      
      combinedNames = jetHadron[i][j]->GetName();
      combinedNames += "_dEtaProjection";
      jetHdEtaProjection[i][j] = jetHadron[i][j]->ProjectionX(combinedNames.Data(),dPhi_Bkg_min_bin,dPhi_Bkg_max_bin,"e");
      jetHdEtaProjection[i][j]->GetXaxis()->SetTitle("#Delta#eta");
      jetHdEtaProjection[i][j]->GetYaxis()->SetTitle("1/N_{jet} dN/d#eta");
      jetHdEtaProjection[i][j]->SetTitle(combinedNames.Data());
      // averaging over number of bins.
      jetHdEtaProjection[i][j]->Scale(1./(1+dPhi_Bkg_max_bin - dPhi_Bkg_min_bin));



      //FIXME
/*      if (use2DFit) { //use 2d fit
        jetHFit[i][j] = (TF2 *) jetHdEtadPhiFit[i][j]->Clone(combinedNames);
        jetHFit[i][j]->SetParameter("Y_NS_1",0); 
        jetHFit[i][j]->SetParameter("Y_NS_2",0); 
        jetHFit[i][j]->SetParameter("Y_AS_2",0); 

      } else { // use 1d fit


        // FIXME: change to trapezoid
        jetHFit[i][j] = new TF2(combinedNames,"[0]*(1-TMath::Abs(x/2))+0*y",-2,2,-PI/2,3*PI/2);
        // jetHFit[i][j]->SetParameter(0,jetHdEtaProjectionFit[i][j]->GetParameter(0)/(dPhi_Bkg_max - dPhi_Bkg_min)*jetHadron[i][j]->GetYaxis()->GetBinWidth(1));
        //   jetHFit[i][j]->SetParameter(0,jetHdEtaProjectionFit[i][j]->GetParameter(0)/(1+ dPhi_Bkg_max_bin - dPhi_Bkg_min_bin));
        jetHFit[i][j]->SetParameter(0,jetHdEtaProjectionFit[i][j]->GetParameter(0));
      }
*/

      combinedNames = jetHadron[i][j]->GetName();
      combinedNames += "_2DFit";
      switch (bkg2DMethod) {
        case aNoBkgSub:
          jetHFit[i][j] = new TF2(combinedNames,"0*x+0*y",-2,2,-PI/2,3*PI/2);

/*          TString dEta_function_string = "[0] * (1 > ((TMath::Abs(x))+[6])/(2*[5]))";
          dEta_function_string += "*((TMath::Abs(x)<=[6])";
          dEta_function_string += "+(TMath::Abs(x)>[6])*(2.*[5]-[6] - TMath::Abs(x))/(2.*[5]-2*[6])  )";
        //  TString function_string = "[0] * (1 - TMath::Abs(x/2))";
          dEta_function_string += "+ [1]*TMath::Gaus(x,0,[2],1)";
          dEta_function_string += "+ [3]*TMath::Gaus(x,0,[2] + [4],1)";

          jetHdEtaProjectionFit[i][j] = new TF1("1d_fit",dEta_function_string,-2,2);
          jetHdEtaProjectionFit[i][j]->SetParameter(0,0); 
          jetHdEtaProjectionFit[i][j]->SetParameter(1,0); 
          jetHdEtaProjectionFit[i][j]->SetParameter(2,0); 
          jetHdEtaProjectionFit[i][j]->SetParameter(3,0); 
          jetHdEtaProjectionFit[i][j]->SetParameter(4,0); 
          jetHdEtaProjectionFit[i][j]->SetParameter(5,eta_cut); 
          jetHdEtaProjectionFit[i][j]->SetParameter(6,R); 
*/
          break;
        case a2DFitSub:
          cJetHdEtadPhiFit->cd(index);
          jetHdEtadPhiFit[i][j] = etaPhiCorr_fit(jetHadron[i][j], combinedNames.Data());
          jetHdEtadPhiFit[i][j]->SetContour(20);
          jetHdEtadPhiFit[i][j]->Draw("COLZ");

          jetHFit[i][j] = (TF2 *) jetHdEtadPhiFit[i][j]->Clone(combinedNames);
          jetHFit[i][j]->SetParameter("Y_NS_1",0); 
          jetHFit[i][j]->SetParameter("Y_NS_2",0); 
          jetHFit[i][j]->SetParameter("Y_AS_2",0); 

          break;
        case aDEtaFitSub:
          combinedNames = jetHadron[i][j]->GetName();
          combinedNames += "_dEtaFit";
          jetHdEtaProjectionFit[i][j] = dEta_fit(jetHdEtaProjection[i][j], combinedNames.Data(),true);

          // FIXME: change to trapezoid
          jetHFit[i][j] = new TF2(combinedNames,"[0]*(1-TMath::Abs(x/2))+0*y",-2,2,-PI/2,3*PI/2);
          // jetHFit[i][j]->SetParameter(0,jetHdEtaProjectionFit[i][j]->GetParameter(0)/(dPhi_Bkg_max - dPhi_Bkg_min)*jetHadron[i][j]->GetYaxis()->GetBinWidth(1));
          //   jetHFit[i][j]->SetParameter(0,jetHdEtaProjectionFit[i][j]->GetParameter(0)/(1+ dPhi_Bkg_max_bin - dPhi_Bkg_min_bin));
          jetHFit[i][j]->SetParameter(0,jetHdEtaProjectionFit[i][j]->GetParameter(0));

          break;
        case aDEtaFarSub:
          combinedNames = jetHadron[i][j]->GetName();
          combinedNames += "_dEtaFit";
          jetHdEtaProjectionFit[i][j] = dEta_fit(jetHdEtaProjection[i][j], combinedNames.Data(),false);

          // FIXME: change to trapezoid
          jetHFit[i][j] = new TF2(combinedNames,"[0]*(1-TMath::Abs(x/2))+0*y",-2,2,-PI/2,3*PI/2);
          // jetHFit[i][j]->SetParameter(0,jetHdEtaProjectionFit[i][j]->GetParameter(0)/(dPhi_Bkg_max - dPhi_Bkg_min)*jetHadron[i][j]->GetYaxis()->GetBinWidth(1));
          //   jetHFit[i][j]->SetParameter(0,jetHdEtaProjectionFit[i][j]->GetParameter(0)/(1+ dPhi_Bkg_max_bin - dPhi_Bkg_min_bin));
          jetHFit[i][j]->SetParameter(0,jetHdEtaProjectionFit[i][j]->GetParameter(0));

          break;
        default:
          fprintf(stderr,"Error: Somehow, invalid choice for dPhi-dEta 2d Background");
          exit(1);
          break;
      }

      cJetHdEtaFit->cd(index);
      jetHdEtaProjection[i][j]->Draw();

      if (bkg2DMethod == aDEtaFitSub || bkg2DMethod == aDEtaFarSub) {
        jetHdEtaProjectionFit[i][j]->SetLineColor(kViolet);
        jetHdEtaProjectionFit[i][j]->Draw("SAME");

        // Drawing subparts
        TF1 *doubleGauss_1 = (TF1 *) jetHdEtaProjectionFit[i][j]->Clone();
        doubleGauss_1->SetLineColor(kGreen);
        doubleGauss_1->SetLineStyle(2);
        doubleGauss_1->SetParameter(0,0);
        doubleGauss_1->SetParameter(3,0);
        doubleGauss_1->Draw("SAME");
        
        TF1 *doubleGauss_2 = (TF1 *) jetHdEtaProjectionFit[i][j]->Clone();
        doubleGauss_2->SetLineColor(kCyan);
        doubleGauss_2->SetLineStyle(2);
        doubleGauss_2->SetParameter(0,0);
        doubleGauss_2->SetParameter(1,0);
        doubleGauss_2->Draw("SAME");

        TF1 *tentFunc = (TF1 *) jetHdEtaProjectionFit[i][j]->Clone();
        tentFunc->SetLineColor(kRed);
        tentFunc->SetParameter(1,0);
        tentFunc->SetParameter(3,0);
        tentFunc->Draw("SAME");
      }




      jetHFit[i][j]->SetTitle(combinedNames);


      jetHadronBkgSub[i][j] = (TH2F *) jetHadron[i][j]->Clone();
      jetHadronBkgSub[i][j]->SetName(Form("%s_2DBkgSub",jetHadronBkgSub[i][j]->GetName()));

//      if (!useZYAM) {
//      }
      fOut->Add(jetHFit[i][j]);
      fOut->Add(jetHadron[i][j]);

			// Subtracting 2D Fit.
      jetHadronBkgSub[i][j]->Add(jetHFit[i][j],-1);
      cJetHBkgSub->cd(index);
      jetHadronBkgSub[i][j]->Draw("COLZ");

      // Perform projection in dPhi
      combinedNames = jetHadron[i][j]->GetName();
      combinedNames += "_Projection";
      jetHProjection[i][j] = jetHadronBkgSub[i][j]->ProjectionY(combinedNames.Data());
      //	jetHProjection[i][j] = jetHadron[i][j]->ProjectionY(combinedNames.Data());
      jetHProjection[i][j]->SetStats(kFALSE);
      jetHProjection[i][j]->GetXaxis()->SetTitle("#Delta#phi");
      jetHProjection[i][j]->GetYaxis()->SetTitle("1/N_{jet} dN/d#phi");

      // FIXME FIXME this could be bad
//      jetHProjection[i][j]->Rebin(2);
//      jetHProjection[i][j]->Scale(1./2);


//      combinedNames = jetHadronRecoil[i][j]->GetName();
//      combinedNames += "_Projection";
//      jetHRecoilProjection[i][j] = jetHadronRecoil[i][j]->ProjectionY(combinedNames.Data());
//      jetHRecoilProjection[i][j]->GetXaxis()->SetTitle("#Delta#phi");
//      jetHRecoilProjection[i][j]->GetYaxis()->SetTitle("1/N_{jet} dN/d#phi");

//      combinedNames = jetHadronNotRecoil[i][j]->GetName();
//      combinedNames += "_Projection";
//      jetHNotRecoilProjection[i][j] = jetHadronNotRecoil[i][j]->ProjectionY(combinedNames.Data());
//      jetHNotRecoilProjection[i][j]->GetXaxis()->SetTitle("#Delta#phi");
//      jetHNotRecoilProjection[i][j]->GetYaxis()->SetTitle("1/N_{jet} dN/d#phi");

      combinedNames = jetHadron[i][j]->GetName();
      combinedNames += "_fit";
      // jetHProjectionFit[i][j] = phiCorrFit(jetHProjection[i][j], combinedNames.Data());
      // jetHProjectionFit[i][j] = phiCorrFit_2Gaus(jetHProjection[i][j], combinedNames.Data());
 //     jetHProjectionFit2[i][j] = phiCorrFit_2Gaus_1GenGaus(jetHProjection[i][j], combinedNames.Data());
			printf("Executing sigma fit ...\n");
      switch (dPhiFitMethod) {
				case a1G1G:  // Simplest: 1 Gaussian for each peak
					jetHProjectionFit2[i][j] = phiCorrFit_1Gaus_1Gaus(jetHProjection[i][j],combinedNames.Data());
					break;
        case a1M1M: // 1 Mod Gaus for NS, 1 Mod Gaus for AS
          // FIXME
          jetHProjectionFit2[i][j] = phiCorrFit_1ModGaus_1ModGaus(jetHProjection[i][j],combinedNames.Data());
          break;
        case a2G1M: // 2 Gaus for NS, 1 Mod Gaus for AS
          // FIXME
          jetHProjectionFit2[i][j] = phiCorrFit_2Gaus_1ModGaus(jetHProjection[i][j],combinedNames.Data());
          break;
        case a2G1GG:
          jetHProjectionFit2[i][j] = phiCorrFit_2Gaus_1GenGaus(jetHProjection[i][j],combinedNames.Data());
          break;
        case a1GG1GG:
          jetHProjectionFit2[i][j] = phiCorrFit_1GenGaus_1GenGaus(jetHProjection[i][j],combinedNames.Data());
          break;
        case a2G1SH:
          jetHProjectionFit2[i][j] = phiCorrFit_2Gaus_SecH(jetHProjection[i][j],combinedNames.Data());
          break;
        default:
          fprintf(stderr,"Error: Somehow, invalid choice for dPhi Fit Method");
          exit(1);
          break;
      }

      jetHProjectionFit2[i][j]->SetLineColor(kRed);
      jetHProjectionFit2[i][j]->SetLineWidth(1);
      jetHProjectionFit2[i][j]->SetNpx(200);
      fOut->Add(jetHProjectionFit2[i][j]);

      TH1F * clone_proj = (TH1F *) jetHProjection[i][j]->Clone(Form("%s_BkgSub",jetHProjection[i][j]->GetName()));
 //     clone_proj->SetName(Form("%s_BkgSub",clone_proj->GetName()));
      clone_proj->GetXaxis()->SetRangeUser(PI/2, 3.*PI/2);
      //TF1 * bkg_near = (TF1 *) jetHProjectionFit[i][j]->Clone();
      TF1 * bkg_near_2 = (TF1 *) jetHProjectionFit2[i][j]->Clone();
      bkg_near_2->SetParameter("Y_AS",0);
      clone_proj->Add(bkg_near_2,-1);

      canvas->cd(index);
      // Save values to array
      double sigma_as = jetHProjectionFit2[i][j]->GetParameter("Sigma_AS");
      double sigma_as_err = jetHProjectionFit2[i][j]->GetParError(jetHProjectionFit2[i][j]->GetParNumber("Sigma_AS"));
      ptBinASWidths[i][j] = sigma_as;
      ptBinASWidthsErr[i][j] = sigma_as_err;
      ptBinASRms[i][j] = clone_proj->GetRMS();
      ptBinASRmsErr[i][j] = clone_proj->GetRMSError();
			Double_t integral_err = 0;
      ptBinASIntegrals[i][j] = clone_proj->IntegralAndError(clone_proj->GetXaxis()->FindBin(PI/2),clone_proj->GetXaxis()->FindBin(3.*PI/2),integral_err,"width");
     // ptBinASIntegrals[i][j] = clone_proj->Integral(clone_proj->GetXaxis()->FindBin(PI/2),clone_proj->GetXaxis()->FindBin(3.*PI/2),&integral_err,"width");
//MARKER
			ptBinASIntegralsErr[i][j]=integral_err;
	

      combinedNames += "_reparam";

      
      // Choose dPhi Fit Method for the main, Omega/FWHM analysis
			printf("Executing omega fit ... \n");
      switch (dPhiFitMethod) {
				case a1G1G: // 1 Gaus for each peak
					jetHProjectionFit[i][j] = phiCorrFit_1Gaus_1Gaus_Fwhm(jetHProjection[i][j],combinedNames.Data());
					break;
        case a1M1M: // 1 Mod Gaus for NS, 1 Mod Gaus for AS
          jetHProjectionFit[i][j] = phiCorrFit_1ModGaus_1ModGaus_Fwhm(jetHProjection[i][j],combinedNames.Data());
          break;
        case a2G1M: // 2 Gaus for NS, 1 Mod Gaus for AS
          jetHProjectionFit[i][j] = phiCorrFit_2Gaus_1ModGaus_Fwhm(jetHProjection[i][j],combinedNames.Data());
          break;
        case a2G1GG:
          jetHProjectionFit[i][j] = phiCorrFit_2Gaus_1GenGaus_Fwhm(jetHProjection[i][j],combinedNames.Data());
          break;
        case a1GG1GG:
					// FIXME leaving in the old function for fwhm until I finish the normal 1gg1gg
//          jetHProjectionFit[i][j] = phiCorrFit_2Gaus_1GenGaus_Fwhm(jetHProjection[i][j],combinedNames.Data());
          jetHProjectionFit[i][j] = phiCorrFit_1GenGaus_1GenGaus_Fwhm(jetHProjection[i][j],combinedNames.Data());
          break;
        case a2G1SH:
          jetHProjectionFit[i][j] = phiCorrFit_2Gaus_SecH_Fwhm(jetHProjection[i][j],combinedNames.Data());
          break;
        default:
          fprintf(stderr,"Error: Somehow, invalid choice for dPhi Fit Method");
          exit(1);
          break;
      }
      jetHProjectionFit[i][j]->SetName("fit1");
  

			double beta_as = 0;
			double beta_as_err = 0;	
			if (jetHProjectionFit[i][j]->GetParNumber("Beta_AS") >= 0) { // checking if this formula has a beta variable
				beta_as = jetHProjectionFit[i][j]->GetParameter("Beta_AS");
				beta_as_err = jetHProjectionFit[i][j]->GetParError(jetHProjectionFit[i][j]->GetParNumber("Beta_AS"));
			}
      double B_param = jetHProjectionFit[i][j]->GetParameter("B");
      double B_param_err = jetHProjectionFit[i][j]->GetParError(jetHProjectionFit[i][j]->GetParNumber("B"));


      jetHProjectionFit[i][j]->SetLineColor(kViolet);
      jetHProjectionFit[i][j]->SetLineWidth(1);
      jetHProjectionFit[i][j]->SetNpx(200);
      fOut->Add(jetHProjectionFit[i][j]); 
      ptBinASFwhm[i][j] = jetHProjectionFit[i][j]->GetParameter("Omega_AS");
      ptBinASFwhmErr[i][j] = jetHProjectionFit[i][j]->GetParError(jetHProjectionFit[i][j]->GetParNumber("Omega_AS"));

			printf("Found: ChiSq = %f \t\t NDF = %d\n",jetHProjectionFit[i][j]->GetChisquare(),jetHProjectionFit[i][j]->GetNDF());

			ptBinChiSq[i][j] = jetHProjectionFit[i][j]->GetChisquare();
			ptBinChiSqOverNDF[i][j] = ptBinChiSq[i][j] / jetHProjectionFit[i][j]->GetNDF();


      ptBinASFwhmRescaled[i][j] = ptBinASFwhm[i][j] / (2.* TMath::Sqrt(2.* TMath::Log(2.)));
      ptBinASFwhmRescaledErr[i][j] = ptBinASFwhmErr[i][j] / (2.* TMath::Sqrt(2.* TMath::Log(2.)));

      ptBinASYields[i][j] = jetHProjectionFit[i][j]->GetParameter("Y_AS"); 
      ptBinASYieldsErr[i][j] = jetHProjectionFit[i][j]->GetParError(jetHProjectionFit[i][j]->GetParNumber("Y_AS")); 
      ptBinASBetas[i][j] = beta_as;
      ptBinASBetasErr[i][j] = beta_as_err;

      ptBinBs[i][j] = B_param;
      ptBinBsErr[i][j] = B_param_err;


      ptBinHadronPt[i]->GetXaxis()->SetRangeUser(particlePtBins.at(j),particlePtBins.at(j+1));

      ptBinASMeans[i][j] = ptBinHadronPt[i]->GetMean();
      ptBinASMeansErr[i][j] = ptBinHadronPt[i]->GetMeanError();

      ptBinHadronPt[i]->GetXaxis()->UnZoom();  

      // Draw projection and fit
     // jetHProjection[i][j]->GetYaxis()->SetRangeUser(0,1.1*jetHProjection[i][j]->GetBinContent(jetHProjection[i][j]->GetMaximumBin()));
      jetHProjection[i][j]->GetYaxis()->SetRangeUser(-0.1*jetHProjection[i][j]->GetBinContent(jetHProjection[i][j]->GetMaximumBin()),1.1*jetHProjection[i][j]->GetBinContent(jetHProjection[i][j]->GetMaximumBin()));
      jetHProjection[i][j]->Draw();

      // Different fit methods have different parameters:
      // 1M1M has only one NS yield'

			//Sigma Constituents
//      TF1 *away_side_sigma = (TF1 *) jetHProjectionFit2[i][j]->Clone();
//      away_side_sigma->SetParameter("B",0);
//      TF1 *const_bg_sigma = (TF1 *) jetHProjectionFit2[i][j]->Clone();
 //     TF1 *near_side_1_sigma = (TF1 *) jetHProjectionFit2[i][j]->Clone();
//      TF1 *near_side_2_sigma = (TF1 *) jetHProjectionFit2[i][j]->Clone();
			
			TF1 *away_side, *const_bg, *near_side_1, *near_side_2;

			if (plotSigmaFits) {// use Sigma Constituents for plot
      	away_side = (TF1 *) jetHProjectionFit2[i][j]->Clone();
      	const_bg = (TF1 *) jetHProjectionFit2[i][j]->Clone();
      	near_side_1 = (TF1 *) jetHProjectionFit2[i][j]->Clone();
      	near_side_2 = (TF1 *) jetHProjectionFit2[i][j]->Clone();
			} else { // Omega Constituents
      	away_side = (TF1 *) jetHProjectionFit[i][j]->Clone();
      	const_bg = (TF1 *) jetHProjectionFit[i][j]->Clone();
      	near_side_1 = (TF1 *) jetHProjectionFit[i][j]->Clone();
      	near_side_2 = (TF1 *) jetHProjectionFit[i][j]->Clone();
			}
			away_side->SetParameter("B",0);

			


      switch (dPhiFitMethod) {
				case a1G1G:
				case a1GG1GG:
        case a1M1M:
          away_side->SetParameter("Y_NS",0);
          near_side_1->SetParameter("B",0);
          near_side_1->SetParameter("Y_AS",0);
          near_side_1->SetLineColor(kCyan);
          near_side_1->SetLineStyle(3);
          near_side_1->Draw("SAME");
          const_bg->SetParameter("Y_NS",0);

          break;
        default:
          away_side->SetParameter("Y_NS_1",0);
          away_side->SetParameter("Y_NS_2",0);

          near_side_1->SetParameter("B",0);
          near_side_1->SetParameter("Y_NS_2",0);
          near_side_1->SetParameter("Y_AS",0);
          near_side_1->SetLineColor(kCyan);
          near_side_1->SetLineStyle(3);
          near_side_1->Draw("SAME");

          near_side_2->SetParameter("B",0);
          near_side_2->SetParameter("Y_NS_1",0);
          near_side_2->SetParameter("Y_AS",0);
          near_side_2->SetLineColor(kRed);
          near_side_2->SetLineStyle(3);
          near_side_2->Draw("SAME");

          const_bg->SetParameter("Y_NS_1",0);
          const_bg->SetParameter("Y_NS_2",0);
        break;
      }
			
			if (plotSigmaFits)  jetHProjectionFit2[i][j]->Draw("SAME"); else jetHProjectionFit[i][j]->Draw("SAME");
      
      const_bg->SetParameter("Y_AS",0);
      const_bg->SetLineColor(15);
      const_bg->SetLineStyle(3);
      away_side->SetLineColor(kGreen);
      away_side->SetLineStyle(9);
      away_side->Draw("SAME");

//      const_bg->SetParameter("B",0.15);

      const_bg->Update();
      const_bg->Draw("SAME");
//      jetHProjectionFit2[i][j]->Draw("SAME");

      cBkgSub->cd(index);
      TH1F * clone_proj_2 = (TH1F *) jetHProjection[i][j]->Clone(Form("%s_BkgSub_2",jetHProjection[i][j]->GetName()));
 //     clone_proj_2->SetName(Form("%s_BkgSub_2",clone_proj_2->GetName()));
      TF1 *bkg_near = (TF1 *) jetHProjectionFit[i][j]->Clone();
      bkg_near->SetParameter("Y_AS",0);
      clone_proj_2->Add(bkg_near,-1);
			clone_proj_2->GetXaxis()->SetRangeUser(PI/2,3.*PI/2);

	//		if (plotSigmaFits) { // use Sigma Fits
				clone_proj->Draw("");
//			} else {// use Omega fits
//				clone_proj_2->Draw("");
//			}
			away_side->Draw("SAME");

      //			delete bkg_near;

      if (index == 
          TMath::CeilNint(TMath::Sqrt(nParticlePtBins))*
          TMath::FloorNint(TMath::Sqrt(nParticlePtBins))) 
        index++;

    }
    canvas->Print(combinedClass);
    cBkgSub->Print(sBkgSub);
    cSwiftParticle->Print(sSwiftParticle);
    cJetHBkgSub->Print(sJetHBkgSub);
    cJetHdEtaFit->Print(sJetHdEtaFit);
    cJetHdEtadPhiFit->Print(sJetHdEtadPhiFit);
  }
  cSwiftJet->Print(sSwiftJet);
  // Save projections
  //canvas->Print("output/jetHProjection.pdf");

  // Create vector for ptBins for TGraph
  std::vector <double> ptBinsForTGraph;	
  // Take the average of the two values for each bin location
  for (unsigned int i = 0; i < nParticlePtBins; i++) { ptBinsForTGraph.push_back((particlePtBins.at(i) + particlePtBins.at(i+1))/2); }

  // Graph for widths
  vector <TGraphErrors> ptBinASWidthsGraphs;
  vector <TGraphErrors> ptBinASRmsGraphs;
  vector <TGraphErrors> ptBinASIntegralsGraphs;
  vector <TGraphErrors> ptBinASFwhmGraphs;
  vector <TGraphErrors> ptBinASFwhmRescaledGraphs;
  vector <TGraphErrors> ptBinASYieldsGraphs;
  vector <TGraphErrors> ptBinASMeansGraphs;
  vector <TGraphErrors> ptBinASBetasGraphs;
  vector <TGraphErrors> ptBinBsGraphs;
	vector <TGraph> 			ptBinChiSqGraphs;
	vector <TGraph> 			ptBinChiSqOverNDFGraphs;



  for (int i = 0; i < nJetPtBins; i++) {
    TGraphErrors *graph = new TGraphErrors(nParticlePtBins,&ptBinsForTGraph[0], &ptBinASWidths[i][0],0,&ptBinASWidthsErr[i][0]);
    ptBinASWidthsGraphs.push_back(*graph);
    graph = new TGraphErrors(nParticlePtBins,&ptBinsForTGraph[0], &ptBinASRms[i][0],0,&ptBinASRmsErr[i][0]);
    ptBinASRmsGraphs.push_back(*graph);
    graph = new TGraphErrors(nParticlePtBins,&ptBinsForTGraph[0], &ptBinASIntegrals[i][0],0,&ptBinASIntegralsErr[i][0]);
    ptBinASIntegralsGraphs.push_back(*graph);
    graph = new TGraphErrors(nParticlePtBins,&ptBinsForTGraph[0], &ptBinASFwhm[i][0],0,&ptBinASFwhmErr[i][0]);
    ptBinASFwhmGraphs.push_back(*graph);
    graph = new TGraphErrors(nParticlePtBins,&ptBinsForTGraph[0], &ptBinASFwhmRescaled[i][0],0,&ptBinASFwhmRescaledErr[i][0]);
    ptBinASFwhmRescaledGraphs.push_back(*graph);
    graph = new TGraphErrors(nParticlePtBins,&ptBinsForTGraph[0], &ptBinASYields[i][0],0,&ptBinASYieldsErr[i][0]);
    ptBinASYieldsGraphs.push_back(*graph);
    graph = new TGraphErrors(nParticlePtBins,&ptBinsForTGraph[0], &ptBinASMeans[i][0],0,&ptBinASMeansErr[i][0]);
    ptBinASMeansGraphs.push_back(*graph);
    graph = new TGraphErrors(nParticlePtBins,&ptBinsForTGraph[0], &ptBinASBetas[i][0],0,&ptBinASBetasErr[i][0]);
    ptBinASBetasGraphs.push_back(*graph);
    graph = new TGraphErrors(nParticlePtBins,&ptBinsForTGraph[0], &ptBinBs[i][0],0,&ptBinBsErr[i][0]);
    ptBinBsGraphs.push_back(*graph);

		TGraph * graph2 = new TGraph(nParticlePtBins,&ptBinsForTGraph[0], &ptBinChiSq[i][0]);
		ptBinChiSqGraphs.push_back(*graph2);
		graph2 = new TGraph(nParticlePtBins,&ptBinsForTGraph[0], &ptBinChiSqOverNDF[i][0]);
		ptBinChiSqOverNDFGraphs.push_back(*graph2);
		

  }


  for (int i = 0; i < nJetPtBins; i++) {
    ptBinASWidthsGraphs[i].SetMarkerSize(1);

    //Width Graphs
    combinedClass = Form(jetClass + "_Widths",jetPtBins.at(i),jetPtBins.at(i+1));
    ptBinASWidthsGraphs[i].SetName(combinedClass.Data());
    ptBinASWidthsGraphs[i].SetTitle("Awayside Widths");
    ptBinASWidthsGraphs[i].GetXaxis()->SetTitle("p_{T}^{associated} GeV/c");
    ptBinASWidthsGraphs[i].GetYaxis()->SetTitle("#sigma_{as}");

    //RMS Graphs
    ptBinASRmsGraphs[i].SetMarkerSize(1);
    combinedClass = Form(jetClass + "_Rms",jetPtBins.at(i),jetPtBins.at(i+1));
    ptBinASRmsGraphs[i].SetName(combinedClass.Data());
    ptBinASRmsGraphs[i].SetTitle("Awayside RMS");
    ptBinASRmsGraphs[i].GetXaxis()->SetTitle("p_{T}^{associated} GeV/c");
    ptBinASRmsGraphs[i].GetYaxis()->SetTitle("RMS");

		//Integral Graphs
    ptBinASIntegralsGraphs[i].SetMarkerSize(1);
    combinedClass = Form(jetClass + "_Integrals",jetPtBins.at(i),jetPtBins.at(i+1));
    ptBinASIntegralsGraphs[i].SetName(combinedClass.Data());
    ptBinASIntegralsGraphs[i].SetTitle("Awayside Yields (Integral)");
    ptBinASIntegralsGraphs[i].GetXaxis()->SetTitle("p_{T}^{associated} GeV/c");
    ptBinASIntegralsGraphs[i].GetYaxis()->SetTitle("Yield");

    //FWHM Graphs
    ptBinASFwhmGraphs[i].SetMarkerSize(1);
    combinedClass = Form(jetClass + "_Fwhm",jetPtBins.at(i),jetPtBins.at(i+1));
    ptBinASFwhmGraphs[i].SetName(combinedClass.Data());
    ptBinASFwhmGraphs[i].SetTitle("Awayside FWHM");
    ptBinASFwhmGraphs[i].GetXaxis()->SetTitle("p_{T}^{associated} GeV/c");
    ptBinASFwhmGraphs[i].GetYaxis()->SetTitle("FWHM");
    // Rescaled Version
    ptBinASFwhmRescaledGraphs[i].SetMarkerSize(1);
    combinedClass = Form(jetClass + "_Fwhm_Rescaled",jetPtBins.at(i),jetPtBins.at(i+1));
    ptBinASFwhmRescaledGraphs[i].SetName(combinedClass.Data());
    ptBinASFwhmRescaledGraphs[i].SetTitle("Awayside FWHM");
    ptBinASFwhmRescaledGraphs[i].GetXaxis()->SetTitle("p_{T}^{associated} GeV/c");
    ptBinASFwhmRescaledGraphs[i].GetYaxis()->SetTitle("FWHM");


    //Yield Graphs
    combinedClass = Form(jetClass + "_Yields",jetPtBins.at(i),jetPtBins.at(i+1));
    ptBinASYieldsGraphs[i].SetName(combinedClass.Data());
    ptBinASYieldsGraphs[i].SetTitle("Awayside Yields");
    ptBinASYieldsGraphs[i].GetXaxis()->SetTitle("p_{T}^{associated} GeV/c");
    ptBinASYieldsGraphs[i].GetYaxis()->SetTitle("Y_{as}");

    //Mean Graphs
    combinedClass = Form(jetClass + "_Means",jetPtBins.at(i),jetPtBins.at(i+1));
    ptBinASMeansGraphs[i].SetName(combinedClass.Data());
    ptBinASMeansGraphs[i].SetTitle("Awayside Means");
    ptBinASMeansGraphs[i].GetXaxis()->SetTitle("p_{T}^{associated} GeV/c");
    ptBinASMeansGraphs[i].GetYaxis()->SetTitle("<p_{T}> GeV/c");

    //Beta Graphs
    combinedClass = Form(jetClass + "_Betas",jetPtBins.at(i),jetPtBins.at(i+1));
    ptBinASBetasGraphs[i].SetName(combinedClass.Data());
    ptBinASBetasGraphs[i].SetTitle("Awayside Betas");
    ptBinASBetasGraphs[i].GetXaxis()->SetTitle("p_{T}^{associated} GeV/c");
    ptBinASBetasGraphs[i].GetYaxis()->SetTitle("#beta");

    //Background Graphs
    combinedClass = Form(jetClass + "_Bs",jetPtBins.at(i),jetPtBins.at(i+1));
    ptBinBsGraphs[i].SetName(combinedClass.Data());
    ptBinBsGraphs[i].SetTitle("Background Parameter");
    ptBinBsGraphs[i].GetXaxis()->SetTitle("p_{T}^{associated} GeV/c");
    ptBinBsGraphs[i].GetYaxis()->SetTitle("B");


		// Chi Squared Graphs
    combinedClass = Form(jetClass + "_ChiSq",jetPtBins.at(i),jetPtBins.at(i+1));
    ptBinChiSqGraphs[i].SetName(combinedClass.Data());
    ptBinChiSqGraphs[i].SetTitle("Fit ChiSquared");
    ptBinChiSqGraphs[i].GetXaxis()->SetTitle("p_{T}^{associated} GeV/c");
    ptBinChiSqGraphs[i].GetYaxis()->SetTitle("#Chi^{2}");
    combinedClass = Form(jetClass + "_ChiSqOverNDF",jetPtBins.at(i),jetPtBins.at(i+1));
    ptBinChiSqOverNDFGraphs[i].SetName(combinedClass.Data());
    ptBinChiSqOverNDFGraphs[i].SetTitle("Fit ChiSquared Over NDF");
    ptBinChiSqOverNDFGraphs[i].GetXaxis()->SetTitle("p_{T}^{associated} GeV/c");
    ptBinChiSqOverNDFGraphs[i].GetYaxis()->SetTitle("#Chi^{2}/NDF");


  }


  canvas->Clear();

  // Adding some plots


  // Widths Method comparison plot
  // Sigma, RMS, FWHM_rescaled
  TLegend *legAS = new TLegend(0.50,0.65,0.90,0.85); 
  double max_y = 0;
  double min_y = 0;
  TString style = "ALP";

  for(int i = 0; i < nJetPtBins; i++) {
    max_y = (min_y = 0);

    ptBinASWidthsGraphs[i].SetLineColor(kBlack);
    ptBinASWidthsGraphs[i].SetMarkerColor(kBlack);
    ptBinASWidthsGraphs[i].SetMarkerStyle(33);

    ptBinASRmsGraphs[i].SetLineColor(kRed);
    ptBinASRmsGraphs[i].SetMarkerColor(kRed);
    ptBinASRmsGraphs[i].SetMarkerStyle(33);

    ptBinASFwhmRescaledGraphs[i].SetLineColor(kBlue);
    ptBinASFwhmRescaledGraphs[i].SetMarkerColor(kBlue);
    ptBinASFwhmRescaledGraphs[i].SetMarkerStyle(33);

    min_y = min(TMath::MinElement(ptBinASWidthsGraphs[i].GetN(),ptBinASWidthsGraphs[i].GetY()),TMath::MinElement(ptBinASRmsGraphs[i].GetN(),ptBinASRmsGraphs[i].GetY()));
    min_y = min(min_y,TMath::MinElement(ptBinASFwhmGraphs[i].GetN(),ptBinASFwhmGraphs[i].GetY()));
    max_y = max(TMath::MaxElement(ptBinASWidthsGraphs[i].GetN(),ptBinASWidthsGraphs[i].GetY()),TMath::MaxElement(ptBinASRmsGraphs[i].GetN(),ptBinASRmsGraphs[i].GetY()));
    max_y = max(max_y,TMath::MaxElement(ptBinASFwhmGraphs[i].GetN(),ptBinASFwhmGraphs[i].GetY()));

    legAS->AddEntry(&ptBinASWidthsGraphs[i],"sigma", "lp");
    legAS->AddEntry(&ptBinASRmsGraphs[i],"RMS after NS Subtraction", "lp");
    legAS->AddEntry(&ptBinASFwhmRescaledGraphs[i],"FWHM (Rescaled by 1/2.35 )", "lp");

    //ptBinASWidthsGraphs[i].GetYaxis()->SetRangeUser(min_y,max_y);
    ptBinASWidthsGraphs[i].GetYaxis()->SetRangeUser(0.,max_y);
    ptBinASWidthsGraphs[i].Draw("ALP");		
    ptBinASRmsGraphs[i].Draw("SAME LP");		
    ptBinASFwhmRescaledGraphs[i].Draw("SAME LP");		

    combinedClass = Form("output/as_Width_cmp_" + jetClass + ".pdf",jetPtBins.at(i),jetPtBins.at(i+1));
    legAS->Draw("SAME");
    canvas->Print(combinedClass);
    legAS->Clear();
    canvas->Clear();
  }
  canvas->SetLogy(0);





  // Widths Plot
  canvas->Clear();
  max_y = (min_y = 0);
  style = "ALP";
  for(int i = 0; i < nJetPtBins; i++) {
    ptBinASWidthsGraphs[i].SetLineColor(i+1);
    ptBinASWidthsGraphs[i].SetMarkerColor(i+1);
    ptBinASWidthsGraphs[i].SetMarkerStyle(33);
    min_y = min(min_y,TMath::MinElement(ptBinASWidthsGraphs[i].GetN(),ptBinASWidthsGraphs[i].GetY()));
    max_y = max(max_y,TMath::MaxElement(ptBinASWidthsGraphs[i].GetN(),ptBinASWidthsGraphs[i].GetY()));
    if (i == 1) style = "SAME LP";
    ptBinASWidthsGraphs[i].Draw(style);		
    combinedClass = Form("%0.f #leq p_{T}^{jet} #leq %0.f",jetPtBins.at(i),jetPtBins.at(i+1));
    legAS->AddEntry(&ptBinASWidthsGraphs[i],combinedClass, "lp");
  }
  ptBinASWidthsGraphs[0].GetYaxis()->SetRangeUser(min_y,max_y);
  //  canvas->SetLogy(1);
  legAS->Draw("SAME");
  canvas->Print("output/as_widths.pdf");
  canvas->Clear();
  legAS->Clear();
  canvas->SetLogy(0);

  // RMS plots
  max_y = (min_y = 0);
  style = "ALP";
  for(int i = 0; i < nJetPtBins; i++) {
    ptBinASRmsGraphs[i].SetLineColor(i+1);
    ptBinASRmsGraphs[i].SetMarkerColor(i+1);
    ptBinASRmsGraphs[i].SetMarkerStyle(33);
    min_y = min(min_y,TMath::MinElement(ptBinASRmsGraphs[i].GetN(),ptBinASRmsGraphs[i].GetY()));
    max_y = max(max_y,TMath::MaxElement(ptBinASRmsGraphs[i].GetN(),ptBinASRmsGraphs[i].GetY()));
    if (i == 1) style = "SAME LP";
    ptBinASRmsGraphs[i].Draw(style);		
    combinedClass = Form("%0.f #leq p_{T}^{jet} #leq %0.f",jetPtBins.at(i),jetPtBins.at(i+1));
    legAS->AddEntry(&ptBinASRmsGraphs[i],combinedClass, "lp");
  }
  ptBinASRmsGraphs[0].GetYaxis()->SetRangeUser(min_y,max_y);
  //  canvas->SetLogy(1);
  legAS->Draw("SAME");
  canvas->Print("output/as_RMS.pdf");
  canvas->Clear();
  legAS->Clear();
  canvas->SetLogy(0);


  // Yields Method comparison plot
  // Yield parameter, Integral of bkg-sub plot
  legAS = new TLegend(0.50,0.65,0.90,0.85); 
  max_y = 0;
  min_y = 0;
  style = "ALP";

  for(int i = 0; i < nJetPtBins; i++) {
    max_y = (min_y = 0);

    ptBinASYieldsGraphs[i].SetLineColor(kBlack);
    ptBinASYieldsGraphs[i].SetMarkerColor(kBlack);
    ptBinASYieldsGraphs[i].SetMarkerStyle(33);

    ptBinASIntegralsGraphs[i].SetLineColor(kRed);
    ptBinASIntegralsGraphs[i].SetMarkerColor(kRed);
    ptBinASIntegralsGraphs[i].SetMarkerStyle(33);

    min_y = min(TMath::MinElement(ptBinASYieldsGraphs[i].GetN(),ptBinASYieldsGraphs[i].GetY()),TMath::MinElement(ptBinASIntegralsGraphs[i].GetN(),ptBinASIntegralsGraphs[i].GetY()));
    max_y = max(TMath::MaxElement(ptBinASYieldsGraphs[i].GetN(),ptBinASYieldsGraphs[i].GetY()),TMath::MaxElement(ptBinASIntegralsGraphs[i].GetN(),ptBinASIntegralsGraphs[i].GetY()));

    legAS->AddEntry(&ptBinASYieldsGraphs[i],"Yield Parameter", "lp");
    legAS->AddEntry(&ptBinASIntegralsGraphs[i],"Integral after NS Subtraction", "lp");

    ptBinASYieldsGraphs[i].GetYaxis()->SetRangeUser(0.,max_y);
    ptBinASYieldsGraphs[i].Draw("ALP");		
    ptBinASIntegralsGraphs[i].Draw("SAME LP");		

    combinedClass = Form("output/as_Yield_cmp_" + jetClass + ".pdf",jetPtBins.at(i),jetPtBins.at(i+1));
    legAS->Draw("SAME");
    canvas->Print(combinedClass);
    legAS->Clear();
    canvas->Clear();
  }
  canvas->SetLogy(0);





  // Integral Yield plots
  max_y = (min_y = 0);
  style = "ALP";
  for(int i = 0; i < nJetPtBins; i++) {
    ptBinASIntegralsGraphs[i].SetLineColor(i+1);
    ptBinASIntegralsGraphs[i].SetMarkerColor(i+1);
    ptBinASIntegralsGraphs[i].SetMarkerStyle(33);
    min_y = min(min_y,TMath::MinElement(ptBinASIntegralsGraphs[i].GetN(),ptBinASIntegralsGraphs[i].GetY()));
    max_y = max(max_y,TMath::MaxElement(ptBinASIntegralsGraphs[i].GetN(),ptBinASIntegralsGraphs[i].GetY()));
    if (i == 1) style = "SAME LP";
    ptBinASIntegralsGraphs[i].Draw(style);		
    combinedClass = Form("%0.f #leq p_{T}^{jet} #leq %0.f",jetPtBins.at(i),jetPtBins.at(i+1));
    legAS->AddEntry(&ptBinASIntegralsGraphs[i],combinedClass, "lp");
  }
  ptBinASIntegralsGraphs[0].GetYaxis()->SetRangeUser(min_y,max_y);
  //  canvas->SetLogy(1);
  legAS->Draw("SAME");
  canvas->Print("output/as_Integrals.pdf");
  canvas->Clear();
  legAS->Clear();
  canvas->SetLogy(0);



  // FWHM plots
  max_y = (min_y = 0);
  style = "ALP";
  for(int i = 0; i < nJetPtBins; i++) {
    ptBinASFwhmGraphs[i].SetLineColor(i+1);
    ptBinASFwhmGraphs[i].SetMarkerColor(i+1);
    ptBinASFwhmGraphs[i].SetMarkerStyle(33);
    min_y = min(min_y,TMath::MinElement(ptBinASFwhmGraphs[i].GetN(),ptBinASFwhmGraphs[i].GetY()));
    max_y = max(max_y,TMath::MaxElement(ptBinASFwhmGraphs[i].GetN(),ptBinASFwhmGraphs[i].GetY()));
    if (i == 1) style = "SAME LP";
    ptBinASFwhmGraphs[i].Draw(style);		
    combinedClass = Form("%0.f #leq p_{T}^{jet} #leq %0.f",jetPtBins.at(i),jetPtBins.at(i+1));
    legAS->AddEntry(&ptBinASFwhmGraphs[i],combinedClass, "lp");
  }
  ptBinASFwhmGraphs[0].GetYaxis()->SetRangeUser(min_y,max_y);
  //  canvas->SetLogy(1);
  legAS->Draw("SAME");
  canvas->Print("output/as_FWHM.pdf");
  canvas->Clear();
  legAS->Clear();
  canvas->SetLogy(0);





  // Yields Plot
  max_y = (min_y = 0);
  style = "ALP";
  for(int i = 0; i < nJetPtBins; i++) {
    ptBinASYieldsGraphs[i].SetLineColor(i+1);
    ptBinASYieldsGraphs[i].SetMarkerColor(i+1);
    ptBinASYieldsGraphs[i].SetMarkerStyle(33);
    min_y = min(min_y,TMath::MinElement(ptBinASYieldsGraphs[i].GetN(),ptBinASYieldsGraphs[i].GetY()));
    max_y = max(max_y,TMath::MaxElement(ptBinASYieldsGraphs[i].GetN(),ptBinASYieldsGraphs[i].GetY()));
    if (i == 1) style = "SAME LP";
    ptBinASYieldsGraphs[i].Draw(style);		
    combinedClass = Form("%0.f #leq p_{T}^{jet} #leq %0.f",jetPtBins.at(i),jetPtBins.at(i+1));
    legAS->AddEntry(&ptBinASYieldsGraphs[i],combinedClass, "lp");
  }
  ptBinASYieldsGraphs[0].GetYaxis()->SetRangeUser(min_y,max_y);
  legAS->Draw("SAME");
  //	canvas->SetLogy(1);
  canvas->Print("output/as_yields.pdf");
  //	canvas->SetLogy(0);
  canvas->Clear();
  legAS->Clear();


  // Print Beta Parameters for the awayside
  max_y = 0;
  min_y = 1;
  style = "ALP";
  for(int i = 0; i < nJetPtBins; i++) {
    ptBinASBetasGraphs[i].SetLineColor(i+1);
    ptBinASBetasGraphs[i].SetMarkerColor(i+1);
    ptBinASBetasGraphs[i].SetMarkerStyle(33);
    min_y = min(min_y,TMath::MinElement(ptBinASBetasGraphs[i].GetN(),ptBinASBetasGraphs[i].GetY()));
    max_y = max(max_y,TMath::MaxElement(ptBinASBetasGraphs[i].GetN(),ptBinASBetasGraphs[i].GetY()));
    if (i == 1) style = "SAME LP";
    ptBinASBetasGraphs[i].Draw(style);		
    combinedClass = Form("%0.f #leq p_{T}^{jet} #leq %0.f",jetPtBins.at(i),jetPtBins.at(i+1));
    legAS->AddEntry(&ptBinASBetasGraphs[i],combinedClass, "lp");
  }
  ptBinASBetasGraphs[0].GetYaxis()->SetRangeUser(min_y,max_y);
  legAS->Draw("SAME");
  	canvas->SetLogy(1);
  canvas->Print("output/as_Betas.pdf");
  	canvas->SetLogy(0);
  canvas->Clear();

  // Print Background Parameters
  legAS->Clear();
  max_y = 0;
  min_y = 1;
  style = "ALP";
  for(int i = 0; i < nJetPtBins; i++) {
    ptBinBsGraphs[i].SetLineColor(i+1);
    ptBinBsGraphs[i].SetMarkerColor(i+1);
    ptBinBsGraphs[i].SetMarkerStyle(33);
//    min_y = min(min_y,TMath::MinElement(ptBinBsGraphs[i].GetN(),ptBinBsGraphs[i].GetY()));
    max_y = max(max_y,TMath::MaxElement(ptBinBsGraphs[i].GetN(),ptBinBsGraphs[i].GetY()));
    if (i == 1) style = "SAME LP";
    ptBinBsGraphs[i].Draw(style);		
    combinedClass = Form("%0.f #leq p_{T}^{jet} #leq %0.f",jetPtBins.at(i),jetPtBins.at(i+1));
    legAS->AddEntry(&ptBinBsGraphs[i],combinedClass, "lp");
  }
  min_y = 1e-4;
  ptBinBsGraphs[0].GetYaxis()->SetRangeUser(min_y,max_y);
  legAS->Draw("SAME");
  	canvas->SetLogy(1);
  canvas->Print("output/as_Bs.pdf");
  	canvas->SetLogy(0);
  canvas->Clear();






	if (leadingJetPt) fOut->Add(leadingJetPt);
	

  for (int i = 0; i < nJetPtBins; i++ ) {
    swiftJetYield[i]->SetName(Form("jetYield_jetPt_%.1f_%.1f",jetPtBins.at(i),jetPtBins.at(i)));
    fOut->Add(swiftJetYield[i]);
  }

  cSwiftParticle->Clear();
  cSwiftParticle->cd();
  for (int j = 0; j < nParticlePtBins; j++) {
    for (int i = 1; i < nJetPtBins; i++ ) {
      swiftParticleYield[0][j]->Add(swiftParticleYield[i][j]);
    }
    swiftParticleYield[0][j]->SetName(Form("particleYield_particlePt_%.2f_%.2f",particlePtBins.at(j),particlePtBins.at(j+1)));
    swiftParticleYield[0][j]->Draw();
    cSwiftParticle->Print(Form("output/%s.pdf",swiftParticleYield[0][j]->GetName()));
    fOut->Add(swiftParticleYield[0][j]);
  }



  for (int i = 0; i < nJetPtBins; i++) {
    fOut->Add(&ptBinASWidthsGraphs[i]);
    fOut->Add(&ptBinASRmsGraphs[i]);
    fOut->Add(&ptBinASIntegralsGraphs[i]);
    fOut->Add(&ptBinASFwhmGraphs[i]);
    fOut->Add(&ptBinASYieldsGraphs[i]);
    fOut->Add(&ptBinASMeansGraphs[i]);
    fOut->Add(&ptBinASBetasGraphs[i]);
    fOut->Add(&ptBinBsGraphs[i]);

		fOut->Add(&ptBinChiSqGraphs[i]);
		fOut->Add(&ptBinChiSqOverNDFGraphs[i]);
  }


  // Drawing, Saving Hard Scatter Things
  if (qqbar_pt) {
    fOut->Add(qqbar_pt);
    fOut->Add(qq_pt);
    fOut->Add(gg_pt);
    fOut->Add(qg_pt);
  }
  if (qqbar_PartonPtByJetBin[0]) {
    TH1F * partonPtByJetBin[nJetPtBins];
    for (int i = 0; i < nJetPtBins; i++ ) {

      partonPtByJetBin[i] = (TH1F *) qqbar_PartonPtByJetBin[i]->Clone();
      partonPtByJetBin[i]->SetName(Form("partonPtByJetBin_%.0f_%.0f",jetPtBins.at(i),jetPtBins.at(i+1)));
      partonPtByJetBin[i]->SetTitle(Form("Parton p_{T} for %.0f #leq p_{T}^{jet} < %.0f (GeV/c)",jetPtBins.at(i),jetPtBins.at(i+1)));
      partonPtByJetBin[i]->GetYaxis()->SetTitle("1/N_{jet} dN/dp_{T}");
      partonPtByJetBin[i]->SetLineColor(kBlue);
      partonPtByJetBin[i]->Add(qq_PartonPtByJetBin[i]);
      partonPtByJetBin[i]->Add(gq_PartonPtByJetBin[i]);
      partonPtByJetBin[i]->Add(qg_PartonPtByJetBin[i]);
      partonPtByJetBin[i]->Add(gg_PartonPtByJetBin[i]);

      // normalizing to unity
      if (double pIntegral = partonPtByJetBin[i]->Integral("width")) partonPtByJetBin[i]->Scale(1./pIntegral);
      partonPtByJetBin[i]->GetXaxis()->SetRangeUser(0,70.);
      // normalizing to 1/N_{jet}  ?
     // if (int nJetEntries = partonPtByJetBin[i]->GetEntries()) partonPtByJetBin[i]->Scale(1./nJetEntries);

    
      canvas->Clear();  
      partonPtByJetBin[i]->Draw();
      canvas->Print(Form("output/partonPtByJetBin_jetPt_%.0f_%.0f.pdf",jetPtBins.at(i),jetPtBins.at(i+1)));


    }



  }

  if (ptLossVertexRLeadingJet) {
    fOut->Add(ptLossVertexRLeadingJet);
    TH2F * ptLossVertexRLeadingJetNorm = normalizeHistogramColumns(ptLossVertexRLeadingJet);
    canvas->Clear();
    ptLossVertexRLeadingJetNorm->Draw("COLZ");
    canvas->Print("output/ptLossVertexRLeadingJetNorm.pdf");
  }
  if (ptLossVertexRSubleadingJet) {
    fOut->Add(ptLossVertexRSubleadingJet);
    TH2F * ptLossVertexRSubleadingJetNorm = normalizeHistogramColumns(ptLossVertexRSubleadingJet);
    canvas->Clear();
    ptLossVertexRSubleadingJetNorm->Draw("COLZ");
    canvas->Print("output/ptLossVertexRSubleadingJetNorm.pdf");
  }



  if (hsPtLeadingJetPt) {
    hsPtLeadingJetPt->Draw("COLZ");
    canvas->SetLogz();
    canvas->Print("output/hsPtLeadinJetPt.pdf");
    canvas->Clear();
    hsPtSubLeadingJetPt->Draw("COLZ");
    canvas->SetLogz();
    canvas->Print("output/hsPtSubLeadinJetPt.pdf");
    canvas->Clear();

    //renormalized version
    TH2F * hsPtLeadingJetPtNorm = normalizeHistogramColumns(hsPtLeadingJetPt);
    hsPtLeadingJetPtNorm->Draw("COLZ");
    canvas->SetLogz();
    canvas->Print("output/hsPtLeadingJetPtNorm.pdf");
    canvas->Clear();
    TH2F * hsPtSubLeadingJetPtNorm = normalizeHistogramColumns(hsPtSubLeadingJetPt);
    hsPtLeadingJetPtNorm->Draw("COLZ");
    canvas->SetLogz();
    canvas->Print("output/hsPtSubLeadingJetPtNorm.pdf");
    canvas->Clear();



    canvas->SetLogz(0);
    hsDEtaDPhiLeadingJet->Draw("COLZ");
    canvas->Print("output/hsDEtaDPhiLeadingJet.pdf");
    canvas->Clear();
    if(hsPtSubLeadingJetPt)  hsDEtaDPhiSubLeadingJet->Draw("COLZ");
    canvas->Print("output/hsDEtaDPhiSubLeadingJet.pdf");
    canvas->Clear();
  
	

    fOut->Add(hsPtLeadingJetPt);
    if(hsPtSubLeadingJetPt) fOut->Add(hsPtSubLeadingJetPt);
    fOut->Add(hsDEtaDPhiLeadingJet);
    fOut->Add(hsDEtaDPhiSubLeadingJet);
  }


  
  if(hJetPtZ) {
    fOut->Add(hJetPtZ);
    fOut->Add(hJetPtZNorm);
  }

  if (hJetPtMass) {
    fOut->Add(hJetPtMass);
    fOut->Add(hJetPtMassNorm);
    fOut->Add(hSDJetPt);
    if (hSDJetPtBkgSub) fOut->Add(hSDJetPtBkgSub);
  }



  fOut->Write();
  fOut->Close();
}

int main(int argc, char * argv[])
{
  // Defined to allow rehlersi to use a different style for printing
  // It can be enabled by passing USER_DEFINED=-DRE_STYLE=1 to the makefile
  #ifdef RE_STYLE
  // Setup TStyle
  TStyle * readableStyle = initializeReadableStyle();
  #endif
  set_plot_style();
  // Parse input
  TString inputFilename = "";
  TString outputFilename = "";

  parseInput(argc, argv, inputFilename, outputFilename);
 
  printf("Input File: %s \t\t Output File: %s \n",inputFilename.Data(),outputFilename.Data());
  printf("dPhidEta Background Method: %s\n",bkg2DMethodArray[bkg2DMethod]);
  printf("           dPhi Fit Method: %s\n",dPhiFitMethodArray[dPhiFitMethod]);
  printf("dPhi Fit Background Option: %s\n",dPhiBkgMethodArray[dPhiBkgMethod]);

//  exit(0);
 
  phase2(inputFilename,outputFilename);

  #ifdef RE_STYLE
  delete readableStyle;
  #endif

  return 0;
}

void parseInput(int argc, char * argv[], TString & inputFilename, TString & outputFilename)
{
  if (argc == 1)
  {
    std::cout << "Error: Must specify an option!" << std::endl;
    printHelp();
  }

  for (int i=1; i < argc; i++)
  {
    if ((argv[i] == std::string("--help")) || (argv[i] == std::string("-h")))
    {
      // Displays help
      printHelp();
    }
    if ((argv[i] == std::string("--inputFilename")) || (argv[i] == std::string("-i")) )
    {
      // Sets input filename
      if (argc > i+1)
      {
        inputFilename = argv[i+1];
        i++;
        continue;
      }
      else
      {
        std::cout << "An input filename must be passed!" << std::endl;
        printHelp();
      }
    }
    if ((argv[i] == std::string("--outputFilename")) || (argv[i] == std::string("-o")) )
    {
      // Sets output filename
      if (argc > i+1)
      {
        outputFilename = argv[i+1];
        i++;
        continue;
      }
      else
      {
        std::cout << "An output filename must be passed!" << std::endl;
        printHelp();
      }
    }
    if ((argv[i] == std::string("--background2D")) || (argv[i] == std::string("-b")) )
    {
      // Sets the combinatorial background method for dPhiDEta
      if (argc > i+1)
      {
        char *t;
        long n = strtol(argv[i+1],&t,10);
        if (*t) { // entry is not a number
          int cmp = 1;
          for (int j = 0; j < Nbkg2DMethods; j++) {
            cmp = strcmp(argv[i+1],bkg2DMethodArray[j]);
            if (!cmp) {
              printf("Recognized option %s = %d\n",bkg2DMethodArray[j],j);
              bkg2DMethod = j;
              break;   
            } 
          }
          if (cmp) {
            fprintf(stderr,"Error: Unrecognized bkd2DMethod option: %s\n",argv[i+1]);
            exit(1);
          }
        } else { //entry is a number
          if (0 <= n && n < Nbkg2DMethods ) bkg2DMethod = n;
          else {
            fprintf(stderr,"Error: invalid choice for background2D Method\n");
            exit(1);
          }
        }
        i++;
        continue;
      }
      else
      {
        std::cout << "A choice of method must be passed!" << std::endl;
        printHelp();
      }
    }
    if ((argv[i] == std::string("--fitDPhi")) || (argv[i] == std::string("-f")) )
    {
      // Sets the fit function for the dPhi Projection
      if (argc > i+1)
      {
        char *t;
        long n = strtol(argv[i+1],&t,10);
        if (*t) { // entry is not a number
          int cmp = 1;
          for (int j = 0; j < NdPhiFitMethods; j++) {
            cmp = strcmp(argv[i+1],dPhiFitMethodArray[j]);
            if (!cmp) {
              printf("recognized option %s = %d\n",dPhiFitMethodArray[j],j);
              dPhiFitMethod = j;
              break;   
            } 
          }
          if (cmp) {
            fprintf(stderr,"Error: Unrecognized dPhi fit option: %s\n",argv[i+1]);
            exit(1);
          }
        } else { //entry is a number
          if (0 <= n && n < NdPhiFitMethods ) dPhiFitMethod = n;
          else {
            fprintf(stderr,"Error: invalid choice for dPhi fit option\n");
            exit(1);
          }
        }
        i++;
        continue;
      }
      else
      {
        std::cout << "A choice of dphi fit function must be passed!" << std::endl;
        printHelp();
      }
    }
    if ((argv[i] == std::string("--backgroundDPhi")) || (argv[i] == std::string("-B")) )
    {
      // Sets the choice for the background offset of the dPhi fit
      if (argc > i+1)
      {
        char *t;
        long n = strtol(argv[i+1],&t,10);
        if (*t) { // entry is not a number
          int cmp = 1;
          for (int j = 0; j < NdPhiBkgMethods; j++) {
            cmp = strcmp(argv[i+1],dPhiBkgMethodArray[j]);
            if (!cmp) {
              printf("Recognized option %s = %d\n",dPhiBkgMethodArray[j],j);
              dPhiBkgMethod = j;
              break;   
            } 
          }
          if (cmp) {
            fprintf(stderr,"Error: Unrecognized dPhi Bkg option: %s\n",argv[i+1]);
            exit(1);
          }
        } else { //entry is a number
          if (0 <= n && n < NdPhiBkgMethods ) dPhiBkgMethod = n;
          else {
            fprintf(stderr,"Error: invalid choice for dPhi Bkg Method\n");
            exit(1);
          }
        }
        i++;
        continue;
      }
      else
      {
        std::cout << "A choice of method must be passed!" << std::endl;
        printHelp();
      }
    }
    if ((argv[i] == std::string("--special")) || (argv[i] == std::string("-s")) )
    {
      // Sets the combinatorial background method for dPhiDEta
      if (argc > i+1)
      {
        char *t;
        long n = strtol(argv[i+1],&t,10);
        if (*t) { // entry is not a number
          fprintf(stderr,"Error: Unrecognized special case option: %s\n",argv[i+1]);
          exit(1);
        } else { //entry is a number
          switch (n) {
            case 0:
              noNearSide = true;      
              break;
            case 1:
              noAwaySide = true;
              break;
						case 2:
							noFitting = true;
							break;
            default:
              fprintf(stderr,"Error: invalid choice for special case\n");
              exit(1);
          }
        }
        i++;
        continue;
      }
      else
      {
        std::cout << "A choice of method must be passed!" << std::endl;
        printHelp();
      }
    }



    else
    {
      printHelp(argv[i]);
    }
  }
}

void printHelp(std::string input)
{
  if (input != "")
  {
    std::cout << "Invalid option: " << input << std::endl;
  }

  // Prints help message
  // The formatting below is carefully written, but not nearly as fraigle as it used to be.
  std::cout << "Help: This output describes the possible parameters." << std::endl;
  std::cout << "Note: Any options that are not explicitly set are assumed to be default." << std::endl << std::endl;
  std::cout << "Valid options:" << std::endl
    << std::setw(5) << std::left << "\t-h" << "\t--help"
    << "\t\t\t\t-> Displays this help message" << std::endl
    << std::setw(5) << std::left << "\t-i" << "\t--intputFilename <filename>"
    << "\t-> Sets the input filename. Default: \"root/pp.root\"" << std::endl
    << std::setw(5) << std::left << "\t-o" << "\t--outputFilename <filename>"
    << "\t-> Sets the output filename. Default: \"root/pp.hist.root\"" << std::endl
    << std::setw(5) << std::left << "\t-b" << "\t--background2D <algo>"
    << "\t-> Sets the method for combinatorial bkg subtraction." << std::endl 
    << "\t\t Choices: \"NoBkgSub = 0\" --> Do not subtract from dPhidEta" << std::endl
    << "\t\t          \"2DBkgSub = 1\" --> Fit a trapezoid + NS Peak, subtract trapezoid" << std::endl
    << "\t\t          \"dEtaBkgSub = 2\" --> Project dEta, fit trapezoid + NS Peak, subtract trapezoid" << std::endl
    << "\t\t          \"dEtaFarSub = 3\" --> Project dEta, use far dEta to normalize trapezoid" << std::endl
    << "\t\t Default: \"NoBkgSub\"" << std::endl
    << std::setw(5) << std::left << "\t-f" << "\t--fitDPhi <algo>"
    << "\t-> Sets the fit function for the dPhi Projection." << std::endl 
    << "\t\t Choices: \"1G1G = 0\"  --> 1 Gaussian for each peak" << std::endl
    << "\t\t          \"1M1M = 1\"  --> 1 Modified Gaussian for each peak" << std::endl
    << "\t\t          \"2G1M = 2\"  --> 2 Gaus. for NS, 1 Mod. Gaus. for AS" << std::endl
    << "\t\t          \"2G1GG = 3\" --> 2 Gaus. for NS, 1 Gen. Gaus. for AS" << std::endl
    << "\t\t          \"1GG1GG = 4\" --> 1 Gen. Gaus. for NS, 1 Gen. Gaus. for AS" << std::endl
    << "\t\t          \"2G1SH = 5\" --> 2 Gaus. for NS, 1 SecH for AS" << std::endl
    << "\t\t Default: \"1M1M\"" << std::endl
    << std::setw(5) << std::left << "\t-B" << "\t--backgroundDPhi <algo>"
    << "\t-> Sets the background method for the dPhi Projection." << std::endl 
    << "\t\t          \"NoDPhiBkg = 0\"  --> dPhi Background Fixed to 0." << std::endl
    << "\t\t          \"FreeDPhiBkg = 1\"  --> dPhi Background a free parameter." << std::endl
    << "\t\t          \"ZYAM = 2\" --> dPhi Background fixed via ZYAM" << std::endl
    << "\t\t Default: \"FreeDPhiBkg\"" << std::endl
    << std::setw(5) << std::left << "\t-s" << "\t--special <n>"
    << "\t-> Sets a special mode." << std::endl
    << "\t\t          \"0\"  --> NearSide Peaks fixed to 0" << std::endl
    << "\t\t          \"1\"  --> Awayside Peaks fixed to 0" << std::endl
    << "\t\t          \"2\"  --> No fitting of dPhi correlations; just show pre-fits" << std::endl;
  std::exit(-1);
}




