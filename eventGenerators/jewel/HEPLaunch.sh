#!/bin/bash
# script to launch a batch of simulations on the HEP cluster

# jewel main directory
JDIR=`pwd`
# notification email
JMAIL="michael.oliver@yale.edu"


if [ "$#" -ne 2 ]; then
    echo "Error: "
    echo "Proper usage:  "$0" <name> <path-to-jewel-binary>"
    exit 1
fi

./clear_integration.sh 

#FIRST_JOB=true
JNAME=$1
JBIN=$2
LIST=`ls params/params.$JNAME.*.dat`
JNUM=`echo $LIST | wc -w`
echo "Will use command "$JBIN" on "$LIST
rm -f pbs/$JNAME.pbs
touch pbs/$JNAME.pbs

# 
#PBS -l nodes=$JNUM

echo "#PBS -q hep -oe -o /dev/null
#PBS -t 0-`expr $JNUM - 1`
#PBS -l nodes=1
#PBS -m a -M $JMAIL
#PBS -l walltime=24:00:00
cd $JDIR
source prep_env.sh
if [ ! \$PBS_ARRAYID = 0 ]; then
  until [ -f 'data/$JNAME.xsecs.dat' ]; do
    sleep 1
  done
fi 
./$JBIN params/params.$JNAME.\$PBS_ARRAYID.dat" > pbs/$JNAME.pbs
#cat pbs/$JNAME.pbs
qsub pbs/$JNAME.pbs


#
exit 0


for fl in $LIST
do
#creating screen name from param file: 
  j_screen_name=`echo $fl | sed -e 's/params\/params.//' -e 's/.dat//'`
#  echo screen -S $j_screen_name -d -m
#  echo $j_screen_name \"$JBIN $fl \&\& exit 0\"
#  echo ""
  echo touch $j_screen_name.pbs
  echo $j_screen_name: ./$JBIN $fl

  rm -f pbs/$j_screen_name.pbs
  touch pbs/$j_screen_name.pbs
  echo "#PBS -q hep\n #PBS -l nodes=1 \n #PBS -m abe" 


#  screen -S $j_screen_name -d -m
  sleep 0.1
# sleeping to make sure screen exists when command is sent to it
#  screen -S $j_screen_name -X stuff "$JBIN $fl && exit 0\n"
#
# not sure how to do this best on HEP
# will implement later/never.  It doesn't really save time, just processing
# power
#  if [ "$FIRST_JOB" = true ]; then 
#    until [ -f 'xsecs.dat' ]; do
#      sleep 1
#    done
#    echo "Numerical integration complete!"
#    FIRST_JOB=false
#  fi
#  sleep 0.1
done


