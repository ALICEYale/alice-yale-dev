#!/bin/bash

# screen to convert HepMC files to root files
# modified to take advantage of HEP's TORQUE bash system



if [ ! -f convert/HepMC2Root ]; then
    echo "Error: convert/HepMC2Root not found"
    echo "  Is it compiled?  Are you in main jewel directory?"
    exit 1
fi

if [ "$#" -ne 1 ]; then
    echo "Error: "
    echo "Proper usage:  "$0" <name>"
    exit 1
fi


# jewel main directory
JDIR=`pwd`
# notification email
JMAIL="michael.oliver@yale.edu"

JNAME=$1
JBIN='convert/HepMC2Root'
LIST=`ls eventfiles/$JNAME.*.hepmc`
JNUM=`echo $LIST | wc -w`
echo "Will use command "$JBIN" on "$LIST
rm -f pbs/$JNAME.convert.pbs
touch pbs/$JNAME.convert.pbs

#PBS -m ae -M $JMAIL
echo "#PBS -q hep
#PBS -t 0-`expr $JNUM - 1`
#PBS -l nodes=1
#PBS -l walltime=24:00:00
cd $JDIR
source prep_env.sh
./convert/HepMC2Root eventfiles/$JNAME.\$PBS_ARRAYID.hepmc ../root/$JNAME.\$PBS_ARRAYID.root" > pbs/$JNAME.convert.pbs
#cat pbs/$JNAME.convert.pbs
qsub pbs/$JNAME.convert.pbs



exit 0

for fl in $LIST
do
#creating screen name from param file: 
  j_screen_name=`echo $fl | sed -e 's/eventfiles\///' -e 's/.hepmc//'`
  j_out_file="../root/"$j_screen_name".root"
  echo screen -S $j_screen_name -d -m
  echo screen -S $j_screen_name -X stuff \"$JBIN $fl $j_out_file\"
  echo screen -S $j_screen_name -X stuff \"exit 0\"
  echo ""
  echo $JBIN $fl
  screen -S $j_screen_name -d -m
  sleep 0.3
# sleeping to make sure screen exists when command is sent to it
  screen -S $j_screen_name -X stuff "$JBIN $fl $j_out_file\n"
  sleep 0.3
  screen -S $j_screen_name -X stuff "exit 0\n"
#  screen -S $j_screen_name -X stuff "$JBIN $fl $j_out_file \&\& exit 0\n"
  sleep 0.3
done


