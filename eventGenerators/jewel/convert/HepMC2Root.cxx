/* -------------------------------------------------------
| Jim Henderson March 2015
| Convert HepMC to simple ROOT tree
| Code finds ALL ".hepmc" files in current directory and loops through
| James.Henderson@cern.ch
|
| May require something like: "export CPLUS_INCLUDE_PATH=${CPLUS_INCLUDE_PATH}:/data/atlas/atlasdata/henderson/HepMC_2.06.09_install/include/"
--------------------------------------------------------*/

#include "HepMC2Root.h"
#include <iostream>
#include "HepMC/PythiaWrapper.h"
#include "HepMC/IO_HEPEVT.h"
#include "HepMC/IO_GenEvent.h"
#include "HepMC/IO_AsciiParticles.h"
#include "HepMC/GenEvent.h"
#include "HepMC/Units.h"

#include "HepMC/GenVertex.h"

#include <TFile.h>




using namespace std;

int main( int argc, char** argv ){

  // Setup a situation where we can look at events that are qq->W(+/-)qq and veto on any gluon in the hard process
  // Use with caution - made for a very specific purpose
//  bool vetoOnGluons = false;
//  for ( int i = 0 ; i < argc ; i++ )
//    if ( string(argv[i]) == "-g" )
//      vetoOnGluons = true;

  TFile *outFile = 0;
  if (argc < 3) {
    fprintf(stderr,"Error: usage: ./program infile.hepmc outfile.root\n");
    exit(1);
  } 
  outFile = new TFile(argv[2],"RECREATE");
//  if ( argc < 2 ) outFile = new TFile( argv[2], "RECREATE" );
//  else outFile = new TFile( "hepmc.root", "RECREATE" );
//  TTree *output_tree = new TTree("Physics", "Physics");
  TTree *output_tree = new TTree("T", "JEWEL output");
  SetupTree( output_tree );
//  getdir(".", files );

  double total_weight = 0, passed_weight = 0, cross_section;
 
//FIXME 
// want to change: stop doing everyhing in .
// pass a directory?
// pass a pattern?
// this could avoid the later root merge
// but maybe we should convert to root as early as possible --> save space
// so just a simple usage (program)

//  for ( int f = 0; f < files.size()a; f++ ){
 // if ( files.at(f).find(".hepmc") == std::string::npos ) continue;
 // if ( argc > 1 && files.at(f) != string( argv[1] ) ) continue;
 // HepMC::IO_GenEvent ascii_io( files.at(f), std::ios::in );
 
  HepMC::IO_GenEvent ascii_io( argv[1], std::ios::in );
  cout << "Analysing file: " << argv[1] << endl;
 // cout << "Analysing file: " << files.at(f) << endl;
  int eventCount = 0;

  // Loop over events
  while ( true ){
    // gluon event veto
    bool vetoEvent = false;
    // Read event and set the units used
    HepMC::GenEvent* genEvt = ascii_io.read_next_event();
    // If at end of file, break event loop
    if ( !genEvt ) break;
    // Get the units for this file - default is GeV
    if ( !eventCount ){
      if ( genEvt->momentum_unit () == HepMC::Units::MEV ){
      //  JetPtCut = 20000;
       // ParticlePtCut = 10000;
      }
      else{
    //	  JetPtCut =20; 
       // ParticlePtCut = 10;
      }
    }
    // Reset output variables
    ResetTreeVars();
    // Get event weight
    HepMC::WeightContainer eventWeight ( genEvt->weights () );
    weight = *(eventWeight.begin());
    total_weight += weight;

		HepMC::GenEvent::vertex_const_iterator vertex_iter = genEvt->vertices_begin();
		vertex_x = (*vertex_iter)->position().x();
		vertex_y = (*vertex_iter)->position().y();

    HepMC::GenCrossSection *eventCrossSection ( genEvt->cross_section());
    if(eventCrossSection) cross_section = eventCrossSection->cross_section();

    if ( !eventCount && eventWeight.size() != 1 ) cerr << "Event Weight vector has size greater than one, bit weird..."  << endl << "Blindly assumming the first entry is the correct weight. Consider youself warned!!" << endl;
    // Loop over all particles in the event
    HepMC::GenEvent::particle_const_iterator pitr;
    for (pitr = genEvt->particles_begin(); pitr != genEvt->particles_end(); ++pitr ) {
      const HepMC::GenParticle* part = (*pitr);
//removing initial particles
      if ( part->status() == 2 ) continue;
      const HepMC::FourVector partMom = part->momentum();
      int pdgId = part->pdg_id();
    //	if ( vetoOnGluons && fabs(pdgId) == 21 ) vetoEvent = true;
      if ( vetoEvent ) break;
//  	if ( fabs(pdgId) == 12 || fabs(pdgId) == 14 || fabs(pdgId) == 16 ){
  // Add ALL neutrinos to missing eT
//  MET_p += fastjet::PseudoJet( partMom.px(), partMom.py(), partMom.pz(), partMom.e() );
//	}
//	else{
  // Fill all OTHER final state particles into a vector of (pseudo)jets for fastjet to cluster later
//  jetParticles.push_back( fastjet::PseudoJet( partMom.px(), partMom.py(), partMom.pz(), partMom.e() ) );
//	}
// Only store particles with pT above 10 GeV

//        if ( partMom.perp() > ParticlePtCut ){
          n_particles++;
          PID_v.push_back( pdgId );
          Status_v.push_back(  part->status()  );
          P_X_v.push_back( partMom.px() );
          P_Y_v.push_back( partMom.py() );
          P_Z_v.push_back( partMom.pz() );
         // P_T_v.push_back( partMom.perp() );
          E_v.push_back( partMom.e() );
          M_v.push_back( partMom.m() );
        //  Eta_v.push_back( partMom.eta() );
       //   Phi_v.push_back( partMom.phi() );
//        }
    }	
    if ( !vetoEvent ){
// Cluster jets and sort by pT
//	fastjet::ClusterSequence cs(jetParticles, jet_def);
/*	jets = fastjet::sorted_by_pt(cs.inclusive_jets());
for ( int j = 0; j < jets.size(); j++ ){
  // Only store jets with pT above 20 GeV
  if ( jets.at(j).perp() < JetPtCut ) continue;
  Jet_n++;
  Jet_pt_v.push_back( jets.at(j).perp() );
  Jet_eta_v.push_back( jets.at(j).eta() );
  Jet_phi_v.push_back( jets.at(j).phi() );
  Jet_E_v.push_back( jets.at(j).E() );
}
// Fill output MET variables
MET_et = MET_p.Et();
MET_phi = MET_p.phi();*/
// Fill tree





      output_tree->Fill();
      // Clean up
      delete genEvt;
      passed_weight += weight;
    }
          eventCount++;
          if (eventCount % 1000 == 0) printf("Finishing event %d\n",eventCount);
  }



  printf("cross section = %f pb.\n",cross_section);
  TTree *runInfo = new TTree("runInfo","runInfo");
  TBranch *bTotalWeight = runInfo->Branch("totalWeight", &total_weight, "totalWeight/D");
  TBranch *bNEvents = runInfo->Branch("nEvents", &eventCount, "nEvents/I");
  TBranch *bCrossSection = runInfo->Branch("crossSection", &cross_section, "crossSection/D");
  // to add: cross secion, sqrt(s), type
  // pt range?
  runInfo->Fill();
  runInfo->Write();




  //}
  // Write out and save
  outFile->cd();
  // I have no idea why, but this works, but for some files between 5000 and 
  // 6000 events two trees will be created.
  
	if (eventCount < 6000) output_tree->Write();

//  output_tree->Write();
  outFile->Close();
  cerr << "HepMC2Root Passed / Total weight = " << passed_weight << " / " << total_weight << " = " << passed_weight / total_weight << endl;
}

void ResetTreeVars(){
  n_particles = 0;

	vertex_x = 0;
	vertex_y = 0;

//  MET_et = 0;
//  MET_phi = 0;
 // Jet_n = 0;
  PID_v.clear();
  P_X_v.clear();
  P_Y_v.clear();
  P_Z_v.clear();
//  P_T_v.clear();
  E_v.clear();
  Status_v.clear();
  M_v.clear();
//  Eta_v.clear();
//  Phi_v.clear();
//  Jet_E_v.clear();
//  Jet_pt_v.clear();
//  Jet_eta_v.clear();
//  Jet_phi_v.clear();
//  jetParticles.clear();
//  jets.clear();
//  MET_p.reset(0,0,0,0);
}
