#!/bin/bash

# screen to convert HepMC files to root files

if [ ! -f convert/HepMC2Root ]; then
    echo "Error: convert/HepMC2Root not found"
    echo "  Is it compiled?  Are you in main jewel directory?"
    exit 1
fi

if [ "$#" -ne 1 ]; then
    echo "Error: "
    echo "Proper usage:  "$0" <name>"
    exit 1
fi

JNAME=$1
JBIN='convert/HepMC2Root'
LIST=`ls eventfiles/$JNAME.*.hepmc`
echo "Will use command "$JBIN" on "$LIST
for fl in $LIST
do
#creating screen name from param file: 
  j_screen_name=`echo $fl | sed -e 's/eventfiles\///' -e 's/.hepmc//'`
  j_out_file="../root/"$j_screen_name".root"
  echo screen -S $j_screen_name -d -m
  echo screen -S $j_screen_name -X stuff \"$JBIN $fl $j_out_file\"
  echo screen -S $j_screen_name -X stuff \"exit 0\"
  echo ""
  echo $JBIN $fl
  screen -S $j_screen_name -d -m
  sleep 0.3
# sleeping to make sure screen exists when command is sent to it
  screen -S $j_screen_name -X stuff "$JBIN $fl $j_out_file\n"
  sleep 0.3
  screen -S $j_screen_name -X stuff "exit 0\n"
#  screen -S $j_screen_name -X stuff "$JBIN $fl $j_out_file \&\& exit 0\n"
  sleep 0.3
done


