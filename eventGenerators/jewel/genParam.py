#!/usr/bin/python
import sys
import random
import os

usage = '''Usage:
    program name nfiles eventType (nevents)'''

nfiles = 1
name = 'test'
eventType = 'pp'
nevent = 10000


if len(sys.argv) <= 2:
  print 'Usage:'
  print '       ' + sys.argv[0] + ' name nfiles eventType [nevents] [sqrts (GeV)]'
  exit()
elif len(sys.argv) > 2:
  nfiles = int(sys.argv[2])
if len(sys.argv) > 3:
  eventType = sys.argv[3]
else:
  print 'Using default event type: ' + eventType
if len(sys.argv) > 4:
  nevent = int(sys.argv[4])
  if len(sys.argv) > 5:
    sqrts = float(sys.argv[5])
  else:
    sqrts = 200.
else:
  print "Using default number of events per file: %d" % nevents

name = sys.argv[1]

print "Using " + str(nfiles) + " files, " + str(nfiles*nevent) + ' ' + eventType + " type events ("+ str(nevent) +  " per file) will be simulated at energy " + str(sqrts) + " GeV"

if eventType == 'pp':
  rate = 1000
elif eventType == 'ee':
	rate = 1000
else:
  rate = 5


print "Estimated time to completion: %.2f hours" % (1.0 * nevent/rate/60/60),

print "(assuming default parameters)"


print "Checking for data directory for integration files ... "

if os.path.isdir("data"):
  print "Found!"
else:
  print "Data directory not found!  Creating it ... "
  os.mkdir("data")


params_prefix = 'params/params.' 
#params_name = 'test'



# default variables
njob = 0
logfilespath = 'logs/'
#hepmcfile = 'eventfiles/'
hepmcfilespath = 'eventfiles/'

ptmin = 8.
#ptmin = 15.
ptmax = -1
etamax = 2.5
mediumparams = 'params/medium.pp.dat'

weighted = 'T'
#wexpo = '3.5'
wexpo = '4'
#wexpo = '5'
angord = 'T'

if (weighted == 'T'):
  print "Weighting is on: ~(p_T)^%s" % wexpo
else:
  print "Weighting is off"





AuAu_medium_path = 'params/medium.AuAu.dat'
PbPb_medium_path = 'params/medium.PbPb.dat'
pp_medium_path = 'params/medium.pp.dat'


if (eventType=="AuAu" or eventType=="PbPb"):
  keep_recoils = 'F'
#  keep_recoils = 'T'
else:
  keep_recoils = 'F'

# start with some int, then +1 for each file
random.seed()

# os based random
os_rand = random.SystemRandom()

#seed_init = 0

for i in range(nfiles):
  filepath = params_prefix + name + '.' + str(i) + ".dat"
  log_path = logfilespath + name + '.' + str(i) + '.log'
  hepmc_path = hepmcfilespath + name + '.' + str(i) + '.hepmc'

  seed_init=os_rand.randint(1,800000000)

  param_text = "# %s %d/%d\n" % (name,i+1,nfiles) 
  param_text += "NJOB %d\n" % seed_init
  param_text += 'LOGFILE ' + log_path + "\n"
  param_text += "SQRTS %f\n" % sqrts
  param_text += 'HEPMCFILE ' + hepmc_path + "\n"

  param_text += "NEVENT %d\n" % nevent
  param_text += "PTMIN %f\n" % ptmin
  param_text += "PTMAX %f\n" % ptmax
  param_text += "ETAMAX %f\n" % etamax
  param_text += "WEIGHTED %s\n" % weighted
  param_text += "WEXPO %s\n" % wexpo
  param_text += "ANGORD %s\n" % angord

  param_text += "KEEPRECOILS %s\n" % keep_recoils


# integration files for first instance

  param_text += "SPLITINTFILE data/%s.splitint.dat\n" % name
  param_text += "PDFFILE data/%s.pdfs.dat\n" % name
  param_text += "XSECFILE data/%s.xsecs.dat\n" % name

# type dependent 
  if (eventType == "AuAu"):
    param_text += 'MEDIUMPARAMS ' + AuAu_medium_path + "\n"
    param_text += "NSET 1\n"
    param_text += "MASS 197\n"
  elif (eventType == "PbPb"):
    param_text += 'MEDIUMPARAMS ' + PbPb_medium_path + "\n"
    param_text += "NSET 1\n"
    param_text += "MASS 208\n"
  elif (eventType == 'pp'): 
    param_text += 'MEDIUMPARAMS ' + pp_medium_path + "\n"
    param_text += "NSET 0\n"
    param_text += "MASS 1\n"
  elif (eventType == 'ee'): 
    param_text += 'MEDIUMPARAMS ' + pp_medium_path + "\n"
    param_text += "NSET 0\n"
    param_text += "MASS 1\n"
    param_text += "SHORTHEPMC F\n"	
    param_text += "COMPRESS F\n"
  else:
    sys.stderr.write('Error: unrecognized event type.\n')
    exit()

  print "Opening: " + filepath
#  print "Writing: \n" + param_text
  f = open(filepath,'w')
  f.write(param_text)
  f.close()

exit()




