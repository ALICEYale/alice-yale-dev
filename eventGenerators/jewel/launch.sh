#!/bin/bash

# script to launch a batch of simulations

#development notes

#screen -S s1 -X stuff "jewel/jewel-2.0.2-simple params/params.AuAuTest.2.dat\n"
# need to include waiting time after launching first job, since that one
# will do the numerical integration for the splitting functions, partonic
# PDFs, and scattering cross sections
# wait () ?  while ?  check if files exist
# files are splitint.dat, pdfs.dat, xsecs.dat
# xsecs.dat is the last one that is made, so I check for that one

# could modify this script for hepmc conversion, or general commands

if [ "$#" -ne 2 ]; then
    echo "Error: "
    echo "Proper usage:  "$0" <name> <path-to-jewel-binary>"
    exit 1
fi

#./clear_integration.sh 



FIRST_JOB=true
JNAME=$1
JBIN=$2
LIST=`ls params/params.$JNAME.*.dat`
echo "Will use command "$JBIN" on "$LIST

rm data/$JNAME''.xsecs.dat
rm data/$JNAME''.pdfs.dat
rm data/$JNAME''.splitint.dat

for fl in $LIST
do
#creating screen name from param file: 
  j_screen_name=`echo $fl | sed -e 's/params\/params.//' -e 's/.dat//'`
  echo screen -S $j_screen_name -d -m
  echo screen -S $j_screen_name -X stuff \"$JBIN $fl \&\& exit 0\"
  echo ""
  echo $JBIN $fl
  screen -S $j_screen_name -d -m
  sleep 0.5
# sleeping to make sure screen exists when command is sent to it
  screen -S $j_screen_name -X stuff "$JBIN $fl && exit 0\n"
  if [ "$FIRST_JOB" = true ]; then 
    until [ -f 'data/'$JNAME'.xsecs.dat' ]; do
      sleep 1
    done
    echo "Numerical integration complete!"
    FIRST_JOB=false
  fi
  sleep 0.5
done


