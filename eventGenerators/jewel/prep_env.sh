#/bin/bash
# edit this to be your install path for lhapdf
MY_LHAPDF='/home/moliver/cern/lhapdf-5.9.1/install'


export LD_LIBRARY_PATH=`$MY_LHAPDF/bin/lhapdf-config --libdir`:$LD_LIBRARY_PATH
export LHAPATH=`$MY_LHAPDF/bin/lhapdf-config --datadir`
