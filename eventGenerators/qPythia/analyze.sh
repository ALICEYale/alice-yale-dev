#1/usr/bin/env bash

if [[ $HOME == *"hep"* ]]; then
    source /scratch/hep/caines/sa639/alicesw/alice-env.sh -n 1
else
    source $HOME/aliceSW/alice-env.sh -n 1
fi

if [[ -z $1 ]];
then
    echo "Must give path for phase1 analysis. Exiting"
    exit 1
fi

if [ ! -f ../analysis/phase1 ]; then
    echo "Error: ../analysis/Phase1 not found"
    echo "  Is it compiled?"
    exit 1
fi

# Gather paths
source enumeratePaths.sh

for path in ${paths[@]};
do
    echo -e "\n../analysis/phase1 --inputFilename "$path/qPythia.merge.root" --outputFilename "$path/phase1.merge.root" --ptHardBin"
    ../analysis/phase1 --inputFilename "$path/qPythia.merge.root" --outputFilename "$path/phase1.merge.root" --ptHardBin
done
