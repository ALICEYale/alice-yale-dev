#!/usr/bin/env bash

tempFiles=$(ls ${1%/}/qPythia.*.root | grep "qPythia.[0-9]\+.root")

fileIndex=()
for file in $tempFiles;
do
    if [[ -f "$file" ]];
    then
        fileNumber=$(echo "$file" | gsed -n 's/.*qPythia.\([0-9]\+\).root/\1/gp')
        fileIndex+=( "$fileNumber" )
    fi
done

# The array paths is now available with all of the pt hard bin folders
# It can be accessed with ${paths[@]}

#for file in ${fileIndex[@]};
#do
#    echo $file
#done
