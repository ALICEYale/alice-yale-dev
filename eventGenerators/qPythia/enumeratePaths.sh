#!/usr/bin/env bash

tempPaths="$(ls $1)"

paths=()
for path in $tempPaths;
do
    if [[ -d "$1/$path" ]];
    then
        paths+=( "${1%/}/$path")
    fi
done

# The array paths is now available with all of the pt hard bin folders
# It can be accessed with ${paths[@]}
