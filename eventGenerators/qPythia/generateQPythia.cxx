
#include <TRandom3.h>
#include <TString.h>
#include <TSystem.h>
#include <TFile.h>
#include <TTree.h>
#include <TParticle.h>

#include <AliRun.h>
#include <AliPythiaRndm.h>
#include <AliGenPythia.h>
#include <AliFastGlauber.h>
#include <AliRunLoader.h>
#include <AliStack.h>

void generateQPythia(Int_t nEvent = 50, Float_t ptHardMin = 10, Float_t ptHardMax = 100, Float_t qHat = 50., Float_t e_cms = 5125, Int_t jobIndex = 0, TString baseDir = "output")
{
    // NB: qHat is mean qhat in units 0.1 GeV^2/fm

    // example macro for on-the-fly generation of PYTHIA6 events
    // and analysis with new reader interface and FastJet
    // M. van Leeuwen
    // Adapted by Raymond Ehlers

    // Create random number generator and set seed
    AliPythiaRndm::SetPythiaRandom(new TRandom3());
    Double_t randomSeed = clock()+gSystem->GetPid();
    AliPythiaRndm::GetPythiaRandom()->SetSeed(randomSeed);
    Printf("Random seed for Pythia: %f", randomSeed);

    // Create pythia generator and configure
    AliGenPythia *pythia=new AliGenPythia(1);
    pythia->SetProcess(kPyJets);
    pythia->SetPtHard(ptHardMin, ptHardMax);
    pythia->SetEnergyCMS(e_cms);

    pythia->SetTune(103); //tune DW, standard choice for Q2 showers

    if (qHat != 0) { // Switch on quenching
        // Set qhat : <qhat> ~ k and <qhat>=1.7 GeV^2/fm for k=0.6*10^6 (default AliPythia)
        Float_t myk=qHat*0.6e6/17.;
        pythia->SetQuench(4); // 4 = q-PYTHIA in aliroot
        pythia->SetQhat(myk);
    }

    // Initialize pythia
    pythia->Init();

    // To change centrality
    //AliFastGlauber::Instance()->SetCentralityClass(0,0.30);

    // Setup output tree
    baseDir = Form("%s/qPythiaPtHard_%1.f_%1.f", baseDir.Data(), ptHardMin, ptHardMax);
    Printf("baseDir: %s", baseDir.Data());

    // Make directory
    gSystem->Exec(Form("mkdir -p %s", baseDir.Data()));
    
    // Setup output file
    // Buffer
    TString bufferLine = "";
    TString outputFilename = "%s/%s.%i.root";
    bufferLine.Form(outputFilename.Data(), baseDir.Data(), "qPythia", jobIndex);
    Printf("baseDir: %s, bufferLine: %s", baseDir.Data(), bufferLine.Data());
    TFile * fOut = TFile::Open(bufferLine.Data(), "RECREATE");
    
    // Check if file is open before preceeding
	if (fOut->IsOpen() == kFALSE)
	{
		Printf("outputFilename \"%s\" is not opened!", outputFilename.Data());
		std::exit(1);
	}

    // Create TTree
    TTree * tree = new TTree("T","Q-PYTHIA output");
    TTree * runInfo = new TTree("runInfo", "runInfo");
    
    // Run parameters
    // nEvent is defined in the function call
    double crossSection = 0;
    // weight is defined by crossSection/nEvents
    double weight = 0;

    // Event parameters
    double vx = 0;
    double vy = 0;

    // Vectors to store event properties in the tree
	std::vector <int> particleID;
	std::vector <double> px;
	std::vector <double> py;
	std::vector <double> pz;
	std::vector <double> energy;
	std::vector <double> mass;
	
	// Set branches in tree
    tree->Branch("vertex_x", &vx);
    tree->Branch("vertex_y", &vy);
	tree->Branch("particleID", &particleID);
	tree->Branch("px", &px);
	tree->Branch("py", &py);
	tree->Branch("pz", &pz);
	tree->Branch("energy", &energy);
	tree->Branch("mass", &mass);
    // And in runInfo
    runInfo->Branch("nEvents", &nEvent);
    runInfo->Branch("totalWeight", &weight);
    runInfo->Branch("crossSection", &crossSection);

    TParticle * tempParticle = 0;

    // Create galice dir for galice root files
    gSystem->Exec(Form("mkdir -p %s/galice", baseDir.Data()));
    // Format for output
    bufferLine.Form(outputFilename.Data(), baseDir.Data(), "galice/galice", jobIndex);
    Printf("baseDir: %s, bufferLine: %s", baseDir.Data(), bufferLine.Data());
    AliRunLoader *rl = rl = AliRunLoader::Open( bufferLine.Data(), AliConfig::GetDefaultEventFolderName(), "RECREATE");

    // Set nEvents
    rl->SetNumberOfEventsPerFile(nEvent);
    
    // Comment out to see event listing
    gAlice->SetRunLoader(rl);

    for (Int_t iEvent = 0; iEvent < nEvent; iEvent++)
    {
        // Clear vectors to reset for the next event
        particleID.clear();
        px.clear();
        py.clear();
        pz.clear();
        energy.clear();
        mass.clear();

        // Setup the run loader
        rl->SetEventNumber(iEvent);
        // The header is necessary to avoid a segfault
        rl->MakeHeader();
        if (rl->Stack()) { 
            rl->Stack()->Reset();
        }
        else {
            rl->MakeStack();
        }

        AliStack *stack = rl->Stack();
        pythia->SetStack(stack);
        
        //cout << "Event " << iEvent << endl;
        if ((iEvent%50)==0) { cout << "Event " << iEvent << endl; }

        // Generate events with (q-)pythia
        pythia->Generate();

        // Finish with particle stack
        stack->FinishEvent();

        // Access particles and save the information
        // TODO: Doubly defined! Check this out!!
        //AliStack * stack = pythia->GetStack();
        if (stack->GetNtrack() != stack->GetNprimary()) { ::Fatal("GenerateQPythia", "Ntrack and Nprimary are not equal!"); }
        for (int i = 0; i < stack->GetNtrack(); i++)
        {
            //tempParticle = particles[i];
            tempParticle = stack->Particle(i);
            // Only take final state particles
            if (tempParticle->GetStatusCode() != 1) { continue; }
            particleID.push_back(tempParticle->GetPdgCode());
            px.push_back(tempParticle->Px());
            py.push_back(tempParticle->Py());
            pz.push_back(tempParticle->Pz());
            energy.push_back(tempParticle->Energy());
            mass.push_back(tempParticle->GetMass());
        }

        //Printf("NParticles: %i, vector size: %i", stack->GetNtrack(), px.size());

        // Save vertex information
        AliFastGlauber * glaub = AliFastGlauber::Instance();
        double xy[] = {0, 0};
        glaub->GetSavedXY(xy);
        vx = xy[0];
        vy = xy[1];

        // Save information into tree
        tree->Fill();
    }

    // Finish pythia
    pythia->FinishRun();
    
    // Write gAlice (ie AliRun)
    gAlice->Write(0,TObject::kOverwrite);//write AliRun

    // nEvent is already set. Throw fatal warning if it is wrong!
    if (nEvent != tree->GetEntries()) { ::Fatal("GenerateQPythia", "nEvent and number of entries in the event tree are not equal!"); }
    crossSection = pythia->GetXsection();
    weight = crossSection/nEvent;

    // Save information into runInfo
    runInfo->Fill();

    fOut->Write();
}
