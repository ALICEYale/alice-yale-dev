#ifndef __CINT__

#include <TFileMerger.h>
#include <TString.h>
#include <TObjArray.h>
#include <TSystem.h>

#endif

void mergeFiles(const TString dir)
{
	// Create file merger
	TFileMerger merger;

	// Gathers files to process
	TString fBasedir(dir);

    // We don't want to merge our already merged file, so we ignore it using grep
    // ls %s/qPythia.*.root | grep "qPythia.[0-9]\+.root"
    // To count, use above with pipe to wc -l
	//TString s = gSystem->GetFromPipe(Form("ls %s/qPythia.*.root | grep \"qPythia.[0-9]\\+.root\"",fBasedir.Data()));
	TString s = gSystem->GetFromPipe(Form("ls %s/qPythiaPtHard_*_*/phase1.merge.root",fBasedir.Data()));
	TObjArray *arr=s.Tokenize("\n");

    //Printf("%s", s.Data());
    //gSystem->Exit(0);

	// Selects files
	for (Int_t ifile=0; ifile < arr->GetEntriesFast(); ++ifile)
	{
		merger.AddFile(arr->At(ifile)->GetName());
                Printf("File %s added.", arr->At(ifile)->GetName());
	}

	merger.OutputFile(Form("%s/phase1.final.root", fBasedir.Data()));

	merger.Merge();
}
