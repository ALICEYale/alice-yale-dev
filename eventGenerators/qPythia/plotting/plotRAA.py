#!/usr/env/bin python

from __future__ import print_function

import ROOT
from ROOT import TH1, TH1D, SetOwnership
import os
import sys

def plotRAA():
    # RHIC 200 GeV
    # PHENIX charged hadrons
    rhicN = 32
    rhicX = [1.26418, 1.75041, 2.23663, 2.75527, 3.24149, 3.72771, 4.24635, 4.73258, 5.2188, 5.73744, 6.22366, 6.70989, 7.26094, 7.74716, 8.23339, 8.71961, 9.23825, 9.72447, 10.2107, 10.7618, 11.248, 11.7018, 12.2204, 12.7391, 13.2253, 13.7439, 14.2301, 14.7164, 15.4943, 16.4668, 17.4716, 18.9627]
    rhicY = [0.399391, 0.374478, 0.339992, 0.294125, 0.246382, 0.223692, 0.206388, 0.184389, 0.190423, 0.184389, 0.172887, 0.181443, 0.190423, 0.190423, 0.190423, 0.184389, 0.187382, 0.231013, 0.209739, 0.209739, 0.213143, 0.213143, 0.184389, 0.238574, 0.133621, 0.121315, 0.242446, 0.318784, 0.119378, 0.125286, 0.234763, 0.334561]

    rhicXErrors = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    rhicYErrors = [0.0503358, 0.042985, 0.0390264, 0.00704771, 0.0264657, 0.0256768, 0.0221697, 0.0198066, 0.0225734, 0.021858, 0.0185711, 0.0208272, 0.0249967, 0.0232388, 0.0254025, 0.0238179, 0.0263554, 0.0319732, 0.0314996, 0.0295, 0.0355969, 0.0398282, 0.0382773, 0.0495256, 0.038028, 0.0411402, 0.0682884, 0.0940724, 0.0533219, 0.0669825, 0.12456, 0.203156]

    #rhicRAA = ROOT.TGraphErrors(rhicN, rhicX, rhicY, rhicXErrors, rhicYErrors)
    rhicRAA = ROOT.TGraphErrors(rhicN)
    rhicRAA.SetTitle("PHENIX h+/-")
    # Fill graph
    for i in xrange(0, rhicN):
        rhicRAA.SetPoint(i, rhicX[i], rhicY[i])
        rhicRAA.SetPointError(i, rhicXErrors[i], rhicYErrors[i])

    rhicRAA.SetMarkerStyle(25)
    rhicRAA.SetMarkerColor(1)
    rhicRAA.SetMarkerSize(0.6)

    # LHC 2.76 TeV
    # CMS hadron R_AA
    lhcN = 16
    lhcX = [5.18651, 5.96093, 6.75136, 8.34911, 10.7889, 13.1481, 16.7431, 21.4778, 26.3668, 31.898, 38.3079, 44.6776, 55.5, 66.8413, 79.6873, 95.0021]
    lhcY = [0.147844, 0.137577, 0.13963,   0.151951, 0.182752, 0.223819, 0.277207, 0.36961,   0.402464, 0.439425, 0.527721, 0.517454, 0.533881, 0.640657, 0.533881, 0.521561]

    lhcXErrors = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    lhcYErrors = [0.00821355, 0.00821355, 0.00410678, 0.00410678, 0.00410678, 0.00205339, 0.00205339, 0.0328542, 0.0410678, 0.0554415, 0.073922, 0.0882957, 0.064084, 0.0780287, 0.0657084, 0.0882957]

    #lhcRAA = ROOT.TGraphErrors(lhcN, lhcX, lhcY, lhcXErrors, lhcYErrors)
    lhcRAA = ROOT.TGraphErrors(lhcN)
    lhcRAA.SetTitle("CMS h+/-")
    # Fill graph
    for i in xrange(0, lhcN):
        lhcRAA.SetPoint(i, lhcX[i], lhcY[i])
        lhcRAA.SetPointError(i, lhcXErrors[i], lhcYErrors[i])
    lhcRAA.SetMarkerStyle(25)
    lhcRAA.SetMarkerColor(1)
    lhcRAA.SetMarkerSize(1.1)

    comparisonRAAs = []
    comparisonRAAs.append(lhcRAA)
    comparisonRAAs.append(rhicRAA)

    #lhcRAA.Draw("APE")
    #rhicRAA.Draw("PE same")

    return comparisonRAAs

def plotQPythiaRAA(dirPrefix, hists):
    ppRefPath = os.path.join(dirPrefix, "pp", "qHat0.00", "phase1.merge.root")
    qPythiaPath = os.path.join(dirPrefix, "PbPb", "qHat%.2f", "phase1.merge.root")
    qHatValues = [25, 50, 75]

    #hists = dict()

    fRefIn = ROOT.TFile(ppRefPath, "READ")
    refHist = fRefIn.Get("jetPt")
    refHist.SetDirectory(0)
    print("refHist: {0}".format(refHist))
    #hists[0] = refHist

    #c = ROOT.TCanvas("canvas", "canvas")

    for qHat in qHatValues:
        path = qPythiaPath % qHat
        print("Processing qHat={0}".format(qHat))

        fQPythiaIn = ROOT.TFile(path, "READ")
        qPythiaHist = fQPythiaIn.GetKey("jetPt").ReadObj()
        qPythiaHist.SetDirectory(0)
        qPythiaHist.SetTitle("qHat = {0}".format(qHat))
        print("qPythiaHist: {0}".format(qPythiaHist))
        qPythiaHist.Divide(refHist)
        # Scale by 1/T_AA
        # T_AA is 23.2 mb^-1 for 0-10%
        # See: https://arxiv.org/pdf/nucl-ex/0302016.pdf
        #qPythiaHist.Scale(1/.0232)
        #qPythiaHist.Draw()
        hists[qHat] = qPythiaHist
        print("hists: {0}".format(hists))

        #c.SaveAs("RAAQHat{0}.pdf".format(qHat))

        #raw_input("pause")

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Must past the path!")
        exit(0)

    # qPythia
    qPythiaHists = dict()
    plotQPythiaRAA(sys.argv[1], qPythiaHists)
    print("qPythiaHists: {0}".format(qPythiaHists))

    # RHIC and LHC RAAs
    comparisonRAAs = plotRAA()
    print("comparisonRAAs: {0}".format(comparisonRAAs))

    # Canvas
    canvas = ROOT.TCanvas("canvas", "canvas")

    # Set the ranges and titles properly...
    setupPlot = ROOT.TH1F("ugh", "ugh", 100, 0, 100)
    setupPlot.SetStats(0)
    setupPlot.GetYaxis().SetRangeUser(0, 1)
    setupPlot.SetTitle("R_{AA} at #sqrt{s_{NN}}=5.02 TeV")
    setupPlot.Draw()

    for qHat in sorted(qPythiaHists):
        print("qHat: {0}".format(qHat))
        qPythiaHists[qHat].SetMarkerStyle(ROOT.kFullCircle)
        qPythiaHists[qHat].SetMarkerSize(0.7)
        if qHat == 25:
            qPythiaHists[qHat].SetMarkerColor(ROOT.kBlue)
            qPythiaHists[qHat].SetLineColor(ROOT.kBlue)
        if qHat == 50:
            qPythiaHists[qHat].SetMarkerColor(ROOT.kRed)
            qPythiaHists[qHat].SetLineColor(ROOT.kRed)
        if qHat == 75:
            qPythiaHists[qHat].SetMarkerColor(ROOT.kGreen+2)
            qPythiaHists[qHat].SetLineColor(ROOT.kGreen+2)
        qPythiaHists[qHat].Draw("same")

    for graph in comparisonRAAs:
        print("graph: {0}".format(graph))
        graph.Draw("PE same")

    leg = canvas.BuildLegend()
    # Remove extra entry from setup plot...
    # Doesn't seem to work...
    #leg.RecursiveRemove(setupPlot)
    #leg.Draw("same")
    canvas.SaveAs("raa.pdf")

