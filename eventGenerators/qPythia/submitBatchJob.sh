#!/bin/bash

# Select type of batch submit
generateFlag=-1

echo "Deprecated! Exiting"
exit 0

if [[ "$1" == "-p" || "$1" == "--phase1" ]];
then
    generateFlag=false
    runType="phase1"
    if [ ! -f ../analysis/phase1 ]; then
        echo "ERROR: ../analysis/Phase1 not found! Is it compiled?"
        exit 1
    fi
    if [[ -z "$2" ]];
    then
        echo "ERROR: Must specify output directory for phase1!"
        exit 1
    else
        outputDir="$2"
    fi
else
    if [[ "$1" == "-g" || "$1" == "--generate" ]];
    then
        generateFlag=true
        runType="generate"
    else
        echo "Please select either generate [ -g | --generate ] or phase1 [ -p | --phase1 ] <directory>."
        exit 1
    fi
fi

if [[ $generateFlag -eq -1 ]];
then
    echo "ERROR: Need to specify genreate or phase1. Should not have gotten here..."
    exit 1
fi

# To trap a control-c during the log
# See: http://rimuhosting.com/knowledgebase/linux/misc/trapping-ctrl-c-in-bash
trap ctrl_c INT

function ctrl_c() {
# Remove the config file created earlier since it will not be used
numFilesLeft=$(ls $outputDir | wc -l)
if [[ "$numFilesLeft" -eq 1 ]];
then
    rm "$outputDir/config.txt"
    rmdir "$outputDir"
else
    echo -e "\n\nOutput dir $outputDir is not empty and cannot be cleaned up."
fi
echo ""
exit
}

# Define variables for the job
# Output directory will be distinct based on timing on when it is run so nothing is overwritten
# However, it only have to be defined for generation
if [[ -z "$outputDir" ]];
then
    if [[ "$generateFlag" == true ]];
    then
        if [[ "$HOME" == *"hep"* ]];
        then
            outputDir="/scratch/hep/caines/re239/qPythia/output/$(date +%s)"
        else
            outputDir="output/$(date +%s)"
        fi
    fi
fi

# Create necessary output directory
mkdir -p "$outputDir"

# Job variables
#ptHardBins=(0 5 11 21 36 57 84 117 152 191 234)
ptHardBins=(5 11 21 36 57 84 117 152)
cmEnergy=5020
qHat=50
nEvents=100000
#nEvents=10
eventsPerProcess=5000
#eventsPerProcess=10
nProcesses=$(( $nEvents / $eventsPerProcess ))

# ptHardBins can be reconstructed from the config file if desired using TString.Tokenize(" ");
echo "ptHardBins: ${ptHardBins[*]}
qHat: $qHat
nEvents: $nEvents
eventsPerProcess: $eventsPerProcess
nProcesses: $nProcesses
cmEnergy: $cmEnergy
generateFlag: $generateFlag
outputDir: $outputDir" | tee -a "$outputDir/config.txt"
#outputDir: $outputDir"

# Set the appropriate loop length
len=${#ptHardBins[@]}
lenMinus1=$(( $len - 1 ))

# Print an extra newline for clarity and then prompt to continue
echo ""
read -p "About to submit $(( $nProcesses * $lenMinus1 )) $runType processes! Press [enter] to continue"

for (( i=0; i<$lenMinus1; i++ )); do
    # Define the environment variables for each job
    environmentVariables="EVENTSPERPROCESS=$eventsPerProcess,PTHARDMIN=${ptHardBins[i]},PTHARDMAX=${ptHardBins[i+1]},QHAT=$qHat,CMENERGY=$cmEnergy,OUTPUTDIR=$outputDir,GENERATEFLAG=$generateFlag,NPROCESSES=$nProcesses"
    if [[ "$HOME" == *"hep"* ]];
    then
        echo "qsub -t 1-$nProcesses -v EVENTSPERPROCESS=$eventsPerProcess,PTHARDMIN=${ptHardBins[i]},PTHARDMAX=${ptHardBins[i+1]},QHAT=$qHat,CMENERGY=$cmEnergy,OUTPUTDIR=$outputDir batchGenerateEvents.pbs"
        qsub -t 1-$nProcesses -v EVENTSPERPROCESS=$eventsPerProcess,PTHARDMIN=${ptHardBins[i]},PTHARDMAX=${ptHardBins[i+1]},QHAT=$qHat,CMENERGY=$cmEnergy,OUTPUTDIR=$outputDir batchGenerateEvents.pbs
    else
        # This only makes any sense with nProcesses = 1
        if [[ $nProcesses -ne 1 ]];
        then
            echo "You probably shouldn't run more than one process per pt hard bin. Exiting."
            exit -1
        fi
        # Replace commas with spaces and export the variables, then run the commands locally
        export ${environmentVariables//,/ }
        echo "${environmentVariables} ./batchGenerateEvents.pbs"
        ./batchGenerateEvents.pbs
    fi
    sleep 1
done
