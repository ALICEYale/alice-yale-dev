#include <fstream>
#include <cstdlib>

#include <TFile.h>
#include <TGraph.h>
#include <TString.h>
#include <TObjArray.h>
#include <TObjString.h>
#include <TCanvas.h>

// Convenient function to simplify returning the value
TString findToken(TObjArray * input, Int_t index)
{
	return ((TObjString *) input->At(index))->String();
}

void convertProfileToRoot(TString inputFilename, TString outputFilename)
{
	// Note that they do not have the extnesion appended. That is performed below
	if (inputFilename == "")
	{
		inputFilename = "profile";
	}

	if (outputFilename == "")
	{
		outputFilename = "profile";
	}

	// Print the parameters of the conversion
	Printf("inputFilename: %s", inputFilename.Data());
	Printf("outputFilename: %s", outputFilename.Data());
	
	// Set input file
	inputFilename = Form("%s.dat", inputFilename.Data());

	// Set output file
	outputFilename = Form("output/%s.root", outputFilename.Data());

	// Setup input file
	std::ifstream inFile(inputFilename.Data());

	// Check if inFile is open before preceding
	if (inFile.is_open() == false)
	{
		Printf("inputFilename \"%s\" is not found!", inputFilename.Data());
		std::exit(1);
	}

	// Setup output file
	TFile * fOut = TFile::Open(outputFilename, "RECREATE");

	// Check if file is open before preceeding
	if (fOut->IsOpen() == kFALSE)
	{
		Printf("outputFilename \"%s\" is not opened!", outputFilename.Data());
		std::exit(1);
	}

	// Variables used for parsing input
	std::string line = "";
	TString input = "";

	// Vectors to store the density profile
	std::vector <double> times;
	std::vector <double> densities;

	while (std::getline(inFile, line))
	{
		input = line;
		
		// Ignore the empty line at the bottom of the file
		if (input == "") { continue; }

		// Split input and put it into the vectors
		TObjArray * tokenizedInput = input.Tokenize(" ");
		times.push_back( std::atof( findToken(tokenizedInput, 0) ) );
		densities.push_back( std::atof( findToken(tokenizedInput, 1) ) );
	}

	// Create the graph
	TGraph * density = new TGraph(times.size(), &times[0], &densities[0]);	
	density->SetName("parton_0_0");
	TGraph * density1 = dynamic_cast<TGraph *> (density->Clone("parton_1_0"));

	// Since this is a TGraph, we have to write it to the file explicitly
	density->Write();
	density1->Write();

	// TEMP until we find the appropriate way to do this!
	// sYajem hard codes 10000
	TGraph * temp = 0;
	TGraph * temp1 = 0;
	TString parton = "parton_0_%d";
	TString parton1 = "parton_1_%d";
	for (unsigned int i = 0; i < 10000; i++)
	{
		// Clone
		temp = dynamic_cast<TGraph *> (density->Clone(Form(parton, i)));
		temp1 = dynamic_cast<TGraph *> (density1->Clone(Form(parton1, i)));

		// Write
		temp->Write();
		temp1->Write();
	}
	// END TEMP
	
	// Since it is already written, we just have to close the file
	fOut->Close();
}

int main(int argc, char * argv[])
{
	TString inputFilename = "";
	TString outputFilename = "";

	if (argc == 3)
	{
		inputFilename = argv[1];
		outputFilename = argv[2];
	}
	else
	{
		if (argc != 1)
		{
			Printf("Must specify the filename of the input profile.dat and the output root file or use the default");
			Printf("Defaults: inputFilename = \"profile\"");
			Printf("          outputFilename = \"profile\"");
			std::exit(1);
		}
	}

	convertProfileToRoot(inputFilename, outputFilename);
}
