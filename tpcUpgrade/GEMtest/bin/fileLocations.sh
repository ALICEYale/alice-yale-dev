#! /usr/bin/env bash

calibDir="StandardCalib"

# Note that USER is defined by the shell!
if [[ "$HOME" == *"hep"* ]]; then
	rawData="/scratch/hep/caines/sa639/TPCupgrade/nov2014BeamTest"
	outputDir="/scratch/hep/caines/$USER/TPCupgrade"
elif [[ "$HOME" == *"fas"* ]]; then
	rawData="/scratch/fas/caines/jdm224/nov2014BeamTest"
	outputDir="/scratch/fas/caines/$USER/TPCupgrade"
elif [[ "$HOSTNAME" == "ray-desktop" ]]; then
	rawData="/media/testBeamData/PS-Data"
	outputDir="/home/$USER/TPCupgrade"
else
	# This is some random default - It probably won't work in a new place unless we specifically check
	rawData="/scratch/hep/caines/sa639/TPCupgrade/nov2014BeamTest"
	outputDir="/scratch/hep/caines/$USER/TPCupgrade"
	echo "Not running on a recognized system, so the paths in file locations are not well defined. Exiting!"
	exit 1;
fi

