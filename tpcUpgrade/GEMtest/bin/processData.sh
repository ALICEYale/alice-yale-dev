#! /usr/bin/env bash

# Load file locations
source fileLocations.sh

GEMTestDir=".."

# Define usage
usage() { echo "Usage: $0 -d[igits] -t[racks] [ -f <addedFolderName> -g[ainMapEnabled] -h[iroc] -b[lackEvents] -c <calibFolder> -o[utputTracksAndDigits] -e[ventDisplay (disables the alternative coordinate mapping)] ]"; exit 1; }

printValues()
{
	echo "useGainMap: $useGainMap"
	echo "runDigits: $runDigits"
	echo "runTracks: $runTracks"
	echo "additionalArguments: $additionalArguments"
	echo "eventDisplay: $eventDisplay"
	echo "folderName: $folderName"
	echo "rawData: $rawData"
	echo "outputDir: $outputDir"
	echo "outputDirTracks: $outputDirTracks"
	echo "outputDirDigits: $outputDirDigits"
	echo "calibMapDir: $calibDir"
	echo "gainMapDir: $gainMapDir"
	echo "GEMTestDir: $GEMTestDir"
}

# Parse command line options
while getopts "hdtgbf:c:oe" arg; do
	case "${arg}" in 
		g)
			useGainMap=1
			;;
		f)
			folderName="${OPTARG}"
			;;
		d)
			runDigits=1
			;;
		t)
			runTracks=1
			;;
		h)
			HIROC=1
			;;
		b)
			blackEvents=1
			;;
		c)
			calibDir="${OPTARG}"
			;;
		o)
			outputTracksAndDigits=1
			;;
		e)
			eventDisplay=1
			;;
		*)
			usage
			;;
	esac
done

if [[ ! -n "${runDigits}" && ! -n "${runTracks}" ]];
then
	echo "Must select digits or tracks!"
	usage
fi

#printValues

# Write this into configurationParameters
baseOutputDir="${outputDir}"

# Set pedestal file location (for black events)
if [[ ${blackEvents} == "1" ]];
then
	pedDir="--pedDir=${outputDir}/PedestalPlots"
	if [[ ! -d "${outputDir}/PedestalPlots" ]];
	then
		echo "Pedestal plots do not exist, but they are needed for black events! Run PlotPedestals.C!"
		exit -1
	fi
else
	pedDir=""
fi

# Setup outputDir
if [[ -n "${folderName}" ]];
then
	outputDir="${outputDir}/${folderName}"
fi

# A string of additional options is passed to makeTree
additionalArguments=""

# Setup the output directories
outputDirTracks="${outputDir}/Tracks"
outputDirDigits="${outputDir}/Digits"

# Gain map conditions
if [[ ${useGainMap} == "1" ]];
then
	gainMapDir="${outputDir}/CalibFiles/${calibDir}.root"
	outputDirTracks="${outputDirTracks}_${calibDir}"
	outputDirDigits="${outputDirDigits}_${calibDir}"
else
	gainMapDir=""
	outputDirTracks="${outputDirTracks}_NoGainMap"
	outputDirDigits="${outputDirDigits}_NoGainMap"
fi

# Needed for black event
if [[ ${blackEvents} == "1" ]];
then
	additionalArguments="${additionalArguments} --black"
	outputDirTracks="${outputDirTracks}_Black"
	outputDirDigits="${outputDirDigits}_Black"
fi

# Ensure we don't undo the coordinate mapping unless we are running tracking for an event display
if [[ ${eventDisplay} == "1" ]];
then
	additionalArguments="${additionalArguments} --eventDisplay"
fi

# Needed for running digits
if [[ ${runDigits} == "1" ]];
then
	additionalArguments="${additionalArguments} --bunches-only"
	outputDir=$outputDirDigits
else
	outputDir=$outputDirTracks
fi

# Allows saving bunches to the tracking output.
# This basically combines tracking and digits in one output.
# If "digit" is in the output filename, then it will not run tracking!!
if [[ ${outputTracksAndDigits} == "1" ]];
then
	additionalArguments="${additionalArguments} --bunches"
fi

# Ensure the directory is created
# By using -p, they are created if they do not exist, but are protected otherwise
mkdir -p "$outputDir"

# Check that configurationParameters exists! Create it if it does not
if [[ ! -e "${GEMTestDir}/code/Analysis/configurationParameters.cxx" ]];
then
	cp "${GEMTestDir}/code/Analysis/configurationParameters_stub.cxx" "${GEMTestDir}/code/Analysis/configurationParameters.cxx"
fi

printValues

#exit 0

if [[ ${HIROC} == "1" ]];
then
	RUNS=( 1475 1476 1477 1478 1479 1482 1485 1486 1487 1490 1491 1492 1496 1499 1502 )
elif [[ ${blackEvents} == "1" ]];
then
	RUNS=( 1629 1630 )
else
        RUNS=( 1646 )  # for the moment let's exclude 1645
fi

for RUN in ${RUNS[@]}; do
	if [[ ${RUN} == 1646 ]];
	then
		iMax=14
	else
		iMax=1
	fi

	for (( i=1; i<=${iMax}; i++))
	do
		if [[ ${RUN} == 1646 ]];
		then
			nEvents=50000
			max=$(expr $i \* $nEvents)
			min=$(expr $max - $nEvents)
			outfile=$(printf '%s/data%s_%s.root' $outputDir $RUN $i)
		else
			max=1000000
			min=0
			outfile=$(printf '%s/data%s.root' $outputDir $RUN)
		fi
		if [[ ${useGainMap} == 1 ]];
		then
			gainMap="--gainfile=${gainMapDir}"
		else
			gainMap=""
		fi

		${GEMTestDir}/bin/makeTree -i "${rawData}/data${RUN}.raw" \
									-o "$outfile" \
									-n "$min" -m "$max" $additionalArguments \
									"--mapdir=../code/files/mapping_PlanB_new/" \
									"$gainMap" \
									"${pedDir}"
	done
done


#echo ../bin/makeTree -i ${rawData}/data1645.raw \
#                -o ${outputDirTracks}/data1645.root \
#                -n 0 -m 1000000 \
#                --mapdir=../code/files/mapping_PlanB_new/ \
#                --gainfile=/scratch/fas/caines/sa639/Digits_NoGainMap/plots1645.root

# Set outputDirTracks or outputDirDigits in the configurationParameters.cxx file so that it is appropriate in the file
# TODO: This could be combined into one operation with a little clever work, but it is not important right now
if [[ ${runDigits} == "1" ]];
then
	sed -i.bak -e "s|\(const TString configurationParameters::fDigitsLocation = \)".*"|\1\"$outputDirDigits\";|" ${GEMTestDir}/code/Analysis/configurationParameters.cxx 
else
	sed -i.bak -e "s|\(const TString configurationParameters::fTracksLocation = \)".*"|\1\"$outputDirTracks\";|" ${GEMTestDir}/code/Analysis/configurationParameters.cxx
fi

# Always write the input and output directory to configurationParameters
sed -i.bak -e "s|\(const TString configurationParameters::fRawDataLocation = \)".*"|\1\"$rawData\";|" ${GEMTestDir}/code/Analysis/configurationParameters.cxx
sed -i.bak -e "s|\(const TString configurationParameters::fAnalysisOutputLocation = \)".*"|\1\"$baseOutputDir/%s\";|" ${GEMTestDir}/code/Analysis/configurationParameters.cxx

# Run from GEMtest directory to merge all of the tracks
if [[ ${blackEvents} != "1" ]];
then
	echo "Merging files"
	cd ..
	if [[ ${runTracks} == "1" ]];
	then
		root -l -q -b code/macros/loadlibs.C code/Analysis/mergeFiles.cxx\(\"$outputDirTracks\"\)
	else
		root -l -q -b code/macros/loadlibs.C code/Analysis/mergeFiles.cxx\(\"$outputDirDigits\"\)
	fi

	# Move old files out of the outputDir Tracks/Digits directory
	echo "Moving old files used for merging to bak"
	if [[ ${runTracks} == "1" ]];
	then
		cd "${outputDirTracks}"
	else
		cd "${outputDirDigits}"
	fi
	echo $(pwd)
	mkdir -p bak
	mv "data1646_"*.root bak/.
fi
