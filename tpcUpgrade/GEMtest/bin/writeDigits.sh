#! /bin/bash

GEMTestDir=".."

usage() { echo "Usage: $0 [ -h[iroc] -b[lackEvents] ]"; exit 1; }

while getopts "hb" arg; do
    case "${arg}" in 
		h)
			HIROC=1
			;;
		b)
			blackEvents=1 ;; *)
            usage
            ;;
    esac
done

if [[ $HIROC == "1" ]];
then
	RUNS=( 1475 1476 1477 1478 1479 1482 1485 1486 1487 1490 1491 1492 1496 1499 1502 )
elif [[ $blackEvents == "1" ]];
then
	RUNS=( 1629 1630 )
else
	RUNS=( 1597 1605 1607 1608 1609 1610 1611 1612 1614 1618 1620 1621 1622 1631 1632 1635 1636 1638 1639 1640 1641 1642 1645 1646 )
fi

# Check that configurationParameters exists! Create it if it does not
if [[ ! -e "${GEMTestDir}/code/Analysis/configurationParameters.cxx" ]];
then
	cp "${GEMTestDir}/code/Analysis/configurationParameters_stub.cxx" "${GEMTestDir}/code/Analysis/configurationParameters.cxx"
fi

outputDirDigits=$(sed -e "s|\(const TString configurationParameters::fDigitsLocation = \)\"\(.*\)\";|\2|;tx;d;:x" ${GEMTestDir}/code/Analysis/configurationParameters.cxx)

#echo $outputDirDigits

echo $PWD

for RUN in ${RUNS[@]};
do
	root.exe -l -q -b ${GEMTestDir}/code/macros/loadlibs.C\(\"$GEMTestDir\"\) ${GEMTestDir}/code/macros/RunDigits.C\(\"${outputDirDigits}/data${RUN}.root\",10\)
	#echo ./startGEMtest --root --bgnd --quit ${GEMTestDir}/code/macros/RunDigits.C\(\"\)
done

