#include "TH1D.h"
#include "TH2S.h"
#include "TROOT.h"
#include "TString.h"

#include "AliTPCROC.h"
#include "AliTPCCalROC.h"
#include "AliLog.h"
#include "AliRawReader.h"

#include "AliTPCSimpleEventDisplay.h"

ClassImp(AliTPCSimpleEventDisplay)

AliTPCSimpleEventDisplay::AliTPCSimpleEventDisplay() :
 AliTPCCalibRawBase(),
fHnDataIROC(0x0),
fHnDataOROC(0x0),
fPadMax("Max","Max"),
fHSigIROC(0x0),
fHSigOROC(0x0),
fCurrentChannel(-1),
fCurrentSector(-1),
fLastSector(-1),
fSelectedSector(-1),
fLastSelSector(-1),
fCurrentRow(-1),
fCurrentPad(-1),
fMaxPadSignal(-1),
fMaxTimeBin(-1),
fSectorLoop(kFALSE),
fRawReader(0x0),
fTPCmapper(0x0)
{
  //
  //
  //
  Int_t binsIROC[3] = {36, 5504, 1000};
  Double_t xminIROC[3] = {0., 0, 0};
  Double_t xmaxIROC[3] = {36., 5504, 1000};
  Int_t binsOROC[3] = {36, 9984, 1000};
  Double_t xminOROC[3] = {0.,0.,0.};
  Double_t xmaxOROC[3] = {36, 9984, 1000};
  fFirstTimeBin=1;
  fLastTimeBin=1011;
  fHnDataIROC = new THnSparseS("fHnDataIROC","IROC data", 3, binsIROC, xminIROC, xmaxIROC);
  fHnDataOROC = new THnSparseS("fHnDataOROC","OROC data", 3, binsOROC, xminOROC, xmaxOROC);
  fHSigIROC = new TH2D("PadSigIROC","Pad Signals IROC",1030,0,1030,5504,0,5504);
  fHSigOROC = new TH2D("PadSigOROC","Pad Signals IROC",1030,0,1030,9984,0,9984);
}

//_____________________________________________________________________
Int_t AliTPCSimpleEventDisplay::Update(const Int_t icsector,
                                       const Int_t icRow,
                                       const Int_t icPad,
                                       const Int_t icTimeBin,
                                       const Float_t csignal)
{
  //
  // Signal filling methode on the fly pedestal and time offset correction if necessary.
  // no extra analysis necessary. Assumes knowledge of the signal shape!
  // assumes that it is looped over consecutive time bins of one pad
  //
  if (icRow<0) return 0;
  if (icPad<0) return 0;
  if (icTimeBin<0) return 0;
  if ( (icTimeBin>fLastTimeBin) || (icTimeBin<fFirstTimeBin)   ) return 0;
  if (fSectorLoop && icsector%36!=fSelectedSector%36) return 0;
  
  if ( icRow<0 || icPad<0 ){
    AliWarning("Wrong Pad or Row number, skipping!");
    return 0;
  }
  
  Int_t iChannel  = fROC->GetRowIndexes(icsector)[icRow]+icPad; //  global pad position in sector
  
    //init first pad and sector in this event
  if ( fCurrentChannel == -1 ) {
    fCurrentChannel = iChannel;
    fCurrentSector  = icsector;
    fCurrentRow     = icRow;
    fCurrentPad     = icPad;
  }
  
    //process last pad if we change to a new one
  if ( iChannel != fCurrentChannel ){
    ProcessPad();
    fLastSector=fCurrentSector;
    fCurrentChannel = iChannel;
    fCurrentSector  = icsector;
    fCurrentRow     = icRow;
    fCurrentPad     = icPad;
    fMaxPadSignal=0;
  }
//  if (csignal>0) printf("%02d:%03d:%03d:%05d: %.3f\n",fCurrentSector,fCurrentRow,fCurrentPad,fCurrentChannel,csignal);
  //fill signals for current pad
  if (fCurrentSector%36==fSelectedSector%36){
    const Int_t nbins = 1030;
    const Int_t offset = (nbins+2)*(iChannel+1)+icTimeBin+1;
    
    if ((UInt_t)icsector<fROC->GetNInnerSector()){
      fHSigIROC->GetArray()[offset]=csignal;
    }else{
      fHSigOROC->GetArray()[offset]=csignal;
    }
  }
  if ( csignal > fMaxPadSignal ){
    fMaxPadSignal = csignal;
    fMaxTimeBin   = icTimeBin;
  }
  return 0;
}
//_____________________________________________________________________
void AliTPCSimpleEventDisplay::ProcessPad()
{
  //
  //
  //
  if (!fSectorLoop) fPadMax.GetCalROC(fCurrentSector)->SetValue(fCurrentRow,fCurrentPad,fMaxPadSignal);
}
//_____________________________________________________________________
TH1D* AliTPCSimpleEventDisplay::MakePadSignals(Int_t sec, Int_t row, Int_t pad)
{
  if (sec<0||sec>=(Int_t)fROC->GetNSectors()) return 0x0;
  if (row<0||row>=(Int_t)fROC->GetNRows(sec)) return 0x0;
  if (pad<0||pad>=(Int_t)fROC->GetNPads(sec,row)) return 0x0;
  const Int_t channel = fROC->GetRowIndexes(sec)[row]+pad; //  global pad position in sector
  
  fSelectedSector=sec;
//  fLastSelSector=sec;
  //attention change for if event has changed
  if (fSelectedSector%36!=fLastSelSector%36){
    fSectorLoop=kTRUE;
    ProcessEvent();
    fLastSelSector=fSelectedSector;
    fSectorLoop=kFALSE;
  }
  TH1D *h=0x0;
  const Int_t nbins = 1030;
  if (nbins<=0) return 0x0;
  const Int_t offset = (nbins+2)*(channel+1);
  Double_t *arrP=0x0;

  TString title("#splitline{#lower[.1]{#scale[.5]{");
  
  if (sec<(Int_t)fROC->GetNInnerSector()){
    h=(TH1D*)gROOT->FindObject("PadSignals_IROC");
    if (!h) h=new TH1D("PadSignals_IROC","PadSignals_IROC",1020,0,1020);
    h->SetFillColor(kBlue-10);
    arrP=fHSigIROC->GetArray()+offset;
//     title+="IROC "; 
  } else {
    h=(TH1D*)gROOT->FindObject("PadSignals_OROC");
    if (!h) h=new TH1D("PadSignals_OROC","PadSignals_OROC",1020,0,1020);
    h->SetFillColor(kBlue-10);
    arrP=fHSigOROC->GetArray()+offset;
    title+="OROC ";
  }
//   title+=(sec/18%2==0)?"A":"C";
//   title+=Form("%02d (%02d) row: %02d, pad: %03d, globalpad: %05d}}}{#scale[.5]{br: %d, FEC: %02d, Chip: %02d, Chn: %02d = HW: %d}}",
//               sec%18,sec,row,pad,channel,
  title+=Form("row: %02d, pad: %03d, cpad: %03d, globalpad: %05d}}}{#scale[.5]{br: %d, FEC: %02d, Chip: %02d, Chn: %02d = HW: %d}}",
              row,pad,pad-fROC->GetNPads(sec,row)/2,channel,
              fTPCmapper.GetBranch(sec,row,pad),
              fTPCmapper.GetFEChw(sec,row,pad),
              fTPCmapper.GetChip(sec,row,pad),
              fTPCmapper.GetChannel(sec,row,pad),
              fTPCmapper.GetHWAddress(sec,row,pad)
            );
  h->SetTitle(title.Data());
  Int_t entries=0;
  for ( Int_t i=0; i<nbins; i++ ) entries+=(Int_t)arrP[i+1];
  h->Set(nbins+2,arrP);
  h->SetEntries(entries);
  return h;
}
//_____________________________________________________________________
void AliTPCSimpleEventDisplay::ResetEvent()
{
  //
  //
  //
  if (fRawReader) fRawReader->Reset();
  fHnDataIROC->Reset();
  fHnDataOROC->Reset();
  if (!fSectorLoop) fPadMax.Multiply(0.);
  fHSigIROC->Reset();
  fHSigOROC->Reset();
}

