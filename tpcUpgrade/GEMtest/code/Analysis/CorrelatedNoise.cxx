// CorrelationNoise.cxx
// Looks for noise correlated around tracks

#include "GEMCombinedTrack.h"
#include "GEMTrack.h"
#include "GEMCluster.h"
#include "GEMEvent.h"
#include "GEMBunch.h"

#include <TString.h>
#include <TSystem.h>
#include <TTree.h>
#include <TClonesArray.h>
#include <TH2F.h>
#include <TH3F.h>
#include <TAxis.h>
#include <TCanvas.h>
#include <TMath.h>
#include <TFile.h>
#include <TProfile.h>
#include <TProfile2D.h>
#include <TRandom3.h>
#include <TLegend.h>
#include <TLegendEntry.h>
#include <TLine.h>

#include <vector>

#include "GEMBeamTestPlotter.h"

#include "CorrelatedNoise.h"

#include "configurationParameters.h"

// Read in the tracking information, probbably cut on only 1 track
void CorrelatedNoise(TTree * tree)
{
	// Determine output directory
	TString outDir = Form(configurationParameters::fAnalysisOutputLocation, "GEMoutput");

	// Create directory for output
	gSystem->Exec(Form("mkdir -p %s", outDir.Data()));
	printf("Output directory: %s\n", outDir.Data());

	// Save the hists
	TFile * fOutput = TFile::Open(Form("%s/correlatedNoise.root", outDir.Data()), "RECREATE");

	GEMEvent * event = 0;
	TClonesArray * combinedTracks = 0;
	GEMCombinedTrack * combinedTrack = 0;
	const GEMTrack * track = 0;
	const GEMCluster * cluster = 0;

	TClonesArray * bunches = 0;
	GEMBunch * bunch = 0;

	// Bunch length and start time
	TH1F * bunchStartTime = new TH1F("bunchStartTime", "bunchStartTime", 500, 0, 500);
	TH1F * bunchLength = new TH1F("bunchLength", "bunchLength", 500, 0, 500);
	// FirstStartBin + 300 ~ 315, so this should be beyond the normal timing window for an event set by GEMclusterer
	TH1F * bunchSignalAt310 = new TH1F("bunchSignalAt310", "bunchSignalAt310", 200, -100, 100);

	TH2F * clusterLocation = new TH2F("clusterLocation", "Cluster Location",  70, -2, 68, 12, 27, 39);
	TH1F * signalDistance = new TH1F("signalDistance", "Bunch Signal Distance", 7, -3.5, 3.5);
	TH2F * signalDistanceVsPad = new TH2F("signalDistanceVsPad", "signalDistanceVsPad", 7, -3.5, 3.5, 12, 27, 39); 
	TH1F * signalDistanceTimeWindow = new TH1F("signalDistanceTimeWindow", "Bunch Signal Distance within Time Window", 7, -3.5, 3.5);
	TH2F * signalDistanceVsPadTimeWindow = new TH2F("signalDistanceVsPadTimeWindow", "signalDistanceVsPad within Time Window", 7, -3.5, 3.5, 12, 27, 39); 
	TH2F * signalDistanceVsSignal = new TH2F("signalDistanceVsSignal", "signalDsitanceVsSignal", 7, -3.5, 3.5, 100, -50, 50); 

	TH2F * timeVsSignal = new TH2F("timeVsSignal", "timeVsSignal", 100, -50, 50, 150, -50, 100);
	TH1F * bunchSignalTiming = new TH1F("bunchSignalTiming", "bunchSignalTiming", 100, -50, 50);

	// Bunch plots
	std::vector <TH1F *> bunchPlots;
	TRandom3 random(0);

	// Track counter
	Int_t trackCounter = 0;

	// Cluster timing min and max
	Float_t minClusterTime = 300;
	Float_t maxClusterTime = 0;

	// Pedestal Map
	std::vector <TH3F *> pedestalMap;
	for (Int_t roc = 1; roc < 3; roc++)
	{
		pedestalMap.push_back(new TH3F(Form("ROC%d_pedestalMap", roc), Form("ROC%d_pedestalMap", roc), 32, 0, 32, 8, 29, 37, 200, -100, 100));
	}

	// SLOPE DIST TEMP 
	//TH1F * slopeDifferences = new TH1F("slopeDifferences", "slopeDifferences", 100, 0, .2);
	// END TEMP

	tree->SetBranchAddress("Events", &event);
	tree->SetBranchAddress("Bunches", &bunches);

	for (int iEvent = 0; iEvent < tree->GetEntriesFast(); iEvent++)
	{
		if (iEvent % 10000 == 0) { Printf("Event number: %d", iEvent); }
		tree->GetEntry(iEvent);

		if (event == 0) { Printf("Didn't assign event correctly."); continue; }
		if (bunches == 0) { Printf("Didn't assign bunches correctly."); continue; }

		combinedTracks = event->GetCombinedTracks();

		if (combinedTracks == 0) { Printf("Didn't assign combinedTracks correctly."); continue; }

		// This will only loop if we have a non-zero number of combined tracks
		for (int iCombinedTrack = 0; iCombinedTrack < combinedTracks->GetEntriesFast(); iCombinedTrack++)
		{
			// Cannot get here unless combinedTracks is valid
			combinedTrack = static_cast<GEMCombinedTrack *> (combinedTracks->At(iCombinedTrack));

			if (combinedTrack == 0) { Printf("Didn't assign combinedTrack correctly."); continue; }
			// Only look at combined tracks where there are two subtracks
			if (combinedTrack->GetNumberOfSubTracks() != 2)
			{
				//Printf("Only %d subtracks! Continuing", combinedTrack->GetNumberOfSubTracks());
				continue;
			}

			if (!GEMBeamTestPlotter::IsTrackAccepted(combinedTrack)) { continue; }

			trackCounter++;

			// SLOPE DIST TEMP
			/*const GEMTrack * temp1 = combinedTrack->GetTrack(0);
			  const GEMTrack * temp2 = combinedTrack->GetTrack(1);

			  slopeDifferences->Fill(TMath::Abs(temp1->GetSlope() - temp2->GetSlope()));*/
			// END TEMP

			// Get tracks here
			for (int iTrack = 0; iTrack < combinedTrack->GetNumberOfSubTracks(); iTrack++)
			{
				track = combinedTrack->GetTrack(iTrack);

				if (track == 0) { Printf("Didn't assign track correctly."); continue; }

				minClusterTime = 300;
				maxClusterTime = 0;
				for (int iCluster = 0; iCluster < track->GetNumberOfClusters(); iCluster++)
				{
					// Cannot get here unless track is valid
					cluster = track->GetCluster(iCluster);

					if (cluster == 0) { Printf("Didn't assign cluster correctly."); continue; }

					if (minClusterTime > cluster->GetTimeBinW()) { minClusterTime = cluster->GetTimeBinW(); }
					if (maxClusterTime < cluster->GetTimeBinW()) { maxClusterTime = cluster->GetTimeBinW(); }

					// Around each track, go row by row , and look at the surrounding pads in the same time bin.
					for (int iBunch = 0; iBunch < bunches->GetEntriesFast(); iBunch++)
					{
						bunch = static_cast<GEMBunch *> (bunches->At(iBunch));

						// Bunch length, start time, and distrbution at large time
						bunchLength->Fill(bunch->GetSignalArrayLength());
						bunchStartTime->Fill(bunch->GetFirstTimeBin());
						if (bunch->GetSignalArrayLength() > 290)
						{
							bunchSignalAt310->Fill((bunch->GetSignalArray())[290]);
						}

						if ((bunch->GetROC() == track->GetROC()) && (bunch->GetRow() == cluster->GetRow()) )
						{
							/*if (iEvent < 5)
							  {
							  Printf("------");
							  Printf("cluster -> pad: %d\trow: %d\t    timeW: %.4f", cluster->GetMaxPad(), cluster->GetRow(), cluster->GetTimeBinW());
							  Printf("bunch   -> pad: %d\trow: %d\tfirstTime: %d", bunch->GetPad(), bunch->GetRow(), bunch->GetFirstTimeBin());
							  Int_t * signalArray = bunch->GetSignalArray();
							  for (int i = 0; i < bunch->GetSignalArrayLength(); i++)
							  {
							  printf("signalArray[%d] = %d, ", i, signalArray[i]);
							  }
							  printf("\n");
							  }*/

							// Check timing
							if ( (cluster->GetTimeBinW() >= bunch->GetFirstTimeBin() ) && (cluster->GetTimeBinW() < (bunch->GetFirstTimeBin() + bunch->GetSignalArrayLength()) ) )
							{
								// Get the signal on the bunch at the appropraite time bin
								Int_t signal = 0;
								Int_t signalTimeWindow = 0;
								Int_t * signalArray = bunch->GetSignalArray();
								const Int_t signalIndex = cluster->GetTimeBinW() - bunch->GetFirstTimeBin();

								// Same time signal for bunch and cluster
								signal = signalArray[signalIndex];

								// Distance from cluster pad weighted by signal
								signalDistance->Fill(bunch->GetPad() - cluster->GetMaxPad(), signal);
								signalDistanceVsPad->Fill(bunch->GetPad() - cluster->GetMaxPad(), cluster->GetMaxPad(), signal);

								// +/- time window signal approach
								Int_t counter = 0;
								for (Int_t i = (signalIndex - configurationParameters::fWindowTime); i <= (signalIndex + configurationParameters::fWindowTime); i++)
								{
									if ( (i >= 0) && (i < bunch->GetSignalArrayLength()) )
									{
										signalTimeWindow += signalArray[i];
										// % 2 looks at even or odd, since we see asymmetry around pads
										if (cluster->GetMaxPad() % 2 == 0)
										{
											signalDistanceVsSignal->Fill( bunch->GetPad() - cluster->GetMaxPad(), signalArray[i]);
										}
										counter++;
									}
								}

								//if (counter != 5) { Printf("Bunch signal counter not large enough! iEvent: %d, iBunch: %d", iEvent, iBunch); }

								signalDistanceTimeWindow->Fill(bunch->GetPad()- cluster->GetMaxPad(), signalTimeWindow);
								signalDistanceVsPadTimeWindow->Fill(bunch->GetPad()- cluster->GetMaxPad(), cluster->GetMaxPad(), signalTimeWindow);

								//signalDistanceVsSignal->Fill( bunch->GetPad() - cluster->GetMaxPad(), signalTimeWindow);

								// Timing vs Signal 
								if ((bunch->GetPad() - cluster->GetMaxPad()) != 0)
									//if (true)
								{
									for (Int_t i = 0; i < bunch->GetSignalArrayLength(); i++)
									{
										// 2D
										timeVsSignal->Fill(i - (cluster->GetTimeBinW() - bunch->GetFirstTimeBin()), signalArray[i]);
										// 1D
										bunchSignalTiming->Fill(i - (cluster->GetTimeBinW() - bunch->GetFirstTimeBin()), signalArray[i]);
									}
								}

								// Plotting individual bunches
								bool plotFlag = false;
								for (Int_t i = 0; i < bunch->GetSignalArrayLength(); i++)
								{
									//if ( signalArray[i] < - 15 && random.Rndm() > 0.97 && ((bunch->GetPad() - cluster->GetMaxPad()) == 3) && (cluster->GetMaxPad() % 2 == 0))
									if ( signalArray[i] < - 10 && random.Rndm() > 0.97)
									{
										plotFlag = true;
									}
								}

								TH1F * bunchPlot = 0;
								if (plotFlag == true && bunchPlots.size() < 10)
								{	
									bunchPlot = new TH1F(Form("bunchPlotEvent%dBunch%d", iEvent, iBunch), Form("bunchPlot Event %d, Bunch %d", iEvent, iBunch), 200, -100, 100);
									bunchPlots.push_back(bunchPlot);
									for (Int_t i = 0; i < bunch->GetSignalArrayLength(); i++)
									{
										bunchPlot->Fill(i - (cluster->GetTimeBinW() - bunch->GetFirstTimeBin()), signalArray[i]);
									}
								}

							}
						}
					}

					// Only after above to ensure that the pad values are not distorted
					// The offset is intentional to show the transition from ROC2 (upstream) to ROC1 (downstream)
					Double_t clusterRow = cluster->GetRow();
					Double_t clusterPad = cluster->GetMaxPad();
					if (track->GetROC() == 1) { clusterRow += (32 + 2); }

					clusterLocation->Fill(clusterRow, clusterPad);
				}

				/*if (trackCounter < 10)
				  {
				  Printf("ROC: %d: Min cluster time: %f, max cluster time: %f, diff: %f", track->GetROC(), minClusterTime, maxClusterTime, maxClusterTime - minClusterTime);
				  }*/

			}

			// Pedestal Map
			// We will build this by staying far away from the cluster
			for (int iBunch = 0; iBunch < bunches->GetEntriesFast(); iBunch++)
			{
				bunch = static_cast<GEMBunch *> (bunches->At(iBunch));

				for (int iTrack = 0; iTrack < combinedTrack->GetNumberOfSubTracks(); iTrack++)
				{
					track = combinedTrack->GetTrack(iTrack);

					if (track == 0) { Printf("Didn't assign track correctly."); continue; }

					if (bunch->GetROC() == track->GetROC())
					{
						minClusterTime = 300;
						maxClusterTime = 0;

						Bool_t doNotFill = kFALSE;
						for (int iCluster = 0; iCluster < track->GetNumberOfClusters(); iCluster++)
						{
							// Cannot get here unless track is valid
							cluster = track->GetCluster(iCluster);

							if (cluster == 0) { Printf("Didn't assign cluster correctly."); continue; }

							if (minClusterTime > cluster->GetTimeBinW()) { minClusterTime = cluster->GetTimeBinW(); }
							if (maxClusterTime < cluster->GetTimeBinW()) { maxClusterTime = cluster->GetTimeBinW(); }

							// Cut out bunches that likely conribute to the cluster
							if ((bunch->GetRow() == cluster->GetRow()) && TMath::Abs(bunch->GetPad() - cluster->GetMaxPad()) <= 1)
							{
								doNotFill = kTRUE;
								break;
							}
						}

						if (doNotFill == kFALSE)
						{
							Int_t bunchFirstTime = bunch->GetFirstTimeBin();
							Int_t * signalArray = bunch->GetSignalArray();
							Int_t roc = bunch->GetROC();

							for (Int_t i = 0; i < bunch->GetSignalArrayLength(); i++)
							{
								// Cut 10 time bins around the cluster to be safe
								if ((TMath::Abs(i - minClusterTime) <= 10) || (TMath::Abs(i - maxClusterTime) <= 10) )
								{
									continue;
								}
								else
								{
									pedestalMap.at(roc-1)->Fill(bunch->GetRow(), bunch->GetPad(), signalArray[i]);
								}
							}
						}
					}
				}
			}

		}

	}

	Printf("Accepted %d combined tracks!", trackCounter);

	TCanvas c;
	TH1D * tempProjection = 0;

	// Pedestal Map
	for (Int_t roc = 1; roc < 3; roc++)
	{
		TProfile2D * pedestalMapProfile = pedestalMap.at(roc-1)->Project3DProfile(Form("yx%d", roc));
		pedestalMapProfile->Draw("colz");
		pedestalMapProfile->SetTitle(Form("Pedestal Map ROC %d", roc));
		pedestalMapProfile->SetName(Form("ROC%d_Extra_Pedestals", roc));
		pedestalMapProfile->GetXaxis()->SetTitle("Row");
		pedestalMapProfile->GetYaxis()->SetTitle("Pad");
		c.SaveAs(Form("%s/pedestalMapProfileROC%d.pdf", outDir.Data(), roc));
		pedestalMapProfile->Write();

		// Plot RMS
		TH2F * pedestalMapRMS = new TH2F(Form("pedestalMapRMSROC%d", roc), Form("pedestalMapRMSROC%d", roc), 32, 0, 32, 8, 29, 36);
		for (Int_t row = 0; row <= 32; row++)
		{
			for (Int_t pad = 29; pad <= 36; pad++)
			{
				Int_t xBin = pedestalMapProfile->GetXaxis()->FindBin((Float_t) row);
				Int_t yBin = pedestalMapProfile->GetYaxis()->FindBin((Float_t) pad);
				pedestalMapRMS->Fill(row, pad, pedestalMapProfile->GetBinError(xBin, yBin));
			}
		}
		pedestalMapRMS->Draw("colz");
		pedestalMapRMS->GetXaxis()->SetTitle("Row");
		pedestalMapRMS->GetYaxis()->SetTitle("Pad");
		// Alternative way to get the errors, but difficult to read
		//pedestalMapProfile->Draw("e");
		c.SaveAs(Form("%s/pedestalMapRMSROC%d.pdf", outDir.Data(), roc));
		pedestalMapRMS->Write();

		// Project onto pads
		/*TProfile * pedestalMapRowProfile = pedestalMapProfile->ProfileX("pedestalMapRowProfile");
		pedestalMapRowProfile->Draw();
		pedestalMapRowProfile->SetTitle("Pedestal Map Row Profile");
		pedestalMapRowProfile->GetXaxis()->SetTitle("Row");
		pedestalMapRowProfile->GetYaxis()->SetTitle("Mean pedestal");
		c.SaveAs(Form("%s/pedestalMapRowProfile.pdf", outDir.Data()));
		pedestalMapRowProfile->Write();*/
	}

	// Correlated noise
	bunchLength->Draw();
	bunchLength->GetXaxis()->SetTitle("Bunch length");
	bunchLength->GetYaxis()->SetTitle("Entries");
	c.SaveAs(Form("%s/bunchLength.pdf", outDir.Data()));
	bunchLength->Write();

	bunchStartTime->Draw();
	bunchStartTime->GetXaxis()->SetTitle("Bunch start time");
	bunchStartTime->GetYaxis()->SetTitle("Entries");
	c.SaveAs(Form("%s/bunchStartTime.pdf", outDir.Data()));
	bunchStartTime->Write();

	bunchSignalAt310->Draw();
	bunchSignalAt310->GetXaxis()->SetTitle("Bunch Signal at 310 (First +~ 290)");
	bunchSignalAt310->GetYaxis()->SetTitle("Entries");
	c.SaveAs(Form("%s/bunchSignalAt310.pdf", outDir.Data()));
	bunchSignalAt310->Write();

	clusterLocation->Draw("colz");
	clusterLocation->GetXaxis()->SetTitle("Row");
	clusterLocation->GetYaxis()->SetTitle("Pad");
	c.SaveAs(Form("%s/clusterLocation.pdf", outDir.Data()));
	clusterLocation->Write();	

	// SLOPE DIST TEMP
	/*slopeDifferences->Draw();
	  slopeDifferences->GetXaxis()->SetTitle("slope difference");
	  slopeDifferences->GetYaxis()->SetTitle("N");
	  c.SaveAs(Form("%s/slopeDifferences.pdf", outDir.Data()));*/
	// END TEMP

	signalDistance->Draw();
	signalDistance->GetXaxis()->SetTitle("Pad away from track");
	signalDistance->GetYaxis()->SetTitle("Signal from bunches");
	c.SaveAs(Form("%s/signalDistance.pdf", outDir.Data()));
	signalDistance->Write();

	signalDistanceVsPad->Draw("colz");
	signalDistanceVsPad->GetXaxis()->SetTitle("Pad away from track");
	signalDistanceVsPad->GetYaxis()->SetTitle("Pad number");
	c.SaveAs(Form("%s/signalDistanceVsPad.pdf", outDir.Data()));
	signalDistanceVsPad->Write();

	signalDistanceTimeWindow->Draw();
	signalDistanceTimeWindow->GetXaxis()->SetTitle("Pad away from track");
	signalDistanceTimeWindow->GetYaxis()->SetTitle("Signal from bunches");
	c.SaveAs(Form("%s/signalDistanceTimeWindow.pdf", outDir.Data()));
	signalDistanceTimeWindow->Write();

	signalDistanceVsPadTimeWindow->Draw("colz");
	signalDistanceVsPadTimeWindow->GetXaxis()->SetTitle("Pad away from track");
	signalDistanceVsPadTimeWindow->GetYaxis()->SetTitle("Pad number");
	c.SaveAs(Form("%s/signalDistanceVsPadTimeWindow.pdf", outDir.Data()));
	signalDistanceVsPadTimeWindow->Write();

	signalDistanceVsSignal->Draw("colz");
	signalDistanceVsSignal->GetXaxis()->SetTitle("Pad away from track");
	signalDistanceVsSignal->GetYaxis()->SetTitle("Bunch Signal");
	c.SaveAs(Form("%s/signalDistanceVsSignal.pdf", outDir.Data()));
	signalDistanceVsSignal->Write();

	timeVsSignal->Draw("colz");
	timeVsSignal->GetXaxis()->SetTitle("Bunch time");
	timeVsSignal->GetYaxis()->SetTitle("Bunch signal");
	c.SaveAs(Form("%s/timeVsSignal.pdf", outDir.Data()));
	timeVsSignal->Write();

	bunchSignalTiming->Draw();
	bunchSignalTiming->GetXaxis()->SetTitle("Bunch time");
	bunchSignalTiming->GetYaxis()->SetTitle("Bunch signal");
	c.SaveAs(Form("%s/bunchSignalTiming.pdf", outDir.Data()));
	bunchSignalTiming->Write();

	TProfile * tempProfile = timeVsSignal->ProfileX("bunchSignalProfile");
	tempProfile->Draw();
	c.SaveAs(Form("%s/bunchSignalTimingProfile.pdf", outDir.Data()));
	tempProfile->Write();	

	/*signalDistanceVsPad->ProjectionX()->Draw();
	  c.SaveAs(Form("%s/signalDistanceVsPadProjection.pdf", outDir.Data()));*/

	gSystem->Exec(Form("mkdir -p %s/signalDistanceProjections", outDir.Data()));
	for (Int_t i = 29; i < 37; i++)
	{
		// We are going bin by bin
		//Int_t bin = signalDistanceVsPadTimeWindow->GetYaxis()->FindBin(i);
		//tempProjection = signalDistanceVsPadTimeWindow->ProjectionX(Form("signalDistanceAtPad%i", i), bin, bin);
		Int_t bin = signalDistanceVsPad->GetYaxis()->FindBin(i);
		tempProjection = signalDistanceVsPad->ProjectionX(Form("signalDistanceAtPad%i", i), bin, bin);
		tempProjection->Draw();
		tempProjection->SetTitle(Form("signalDistanceAtPad%i", i));
		c.SaveAs(Form("%s/signalDistanceProjections/signalDistanceAtPad%i.pdf", outDir.Data(), i));
		tempProjection->Write();
	}

	// Create directory
	gSystem->Exec(Form("mkdir -p %s/signalProjections", outDir.Data()));
	// Legend
	TLegend * leg = new TLegend(0.6, 0.7, 0.8, 0.85);
	leg->SetTextSize(0.04);
	TLegendEntry * legEntry = leg->AddEntry((TObject*) 0, "Integral = ", "");
	// Plot the hists
	for (Int_t i = -3; i <= 3 ; i++)
	{
		// We are going bin by bin
		Int_t bin = signalDistanceVsSignal->GetXaxis()->FindBin(i);
		tempProjection = signalDistanceVsSignal->ProjectionY(Form("signalAtDistance%i", i), bin, bin);
		tempProjection->SetTitle(Form("signalAtDistance%i", i));

		// Setup plot
		legEntry->SetLabel(Form("Mean = %f #pm %f", tempProjection->GetMean(), tempProjection->GetMeanError()));
		tempProjection->Draw();
		leg->Draw();

		// Save
		c.SaveAs(Form("%s/signalProjections/signalAtDistance%i.pdf", outDir.Data(), i));
		tempProjection->Write();
	}

	// Create directory
	gSystem->Exec(Form("mkdir -p %s/bunchPlot", outDir.Data()));
	// Temporary variable
	TH1F * bunchPlot = 0;
	for (unsigned int i = 0; i < bunchPlots.size(); i++)
	{
		bunchPlot = bunchPlots.at(i);
		if (bunchPlot == 0) { Printf("Bunch plot at(%d) is 0! Something is wrong", i); continue; }
		bunchPlot->Draw();
		bunchPlot->GetXaxis()->SetTitle("Time");
		bunchPlot->GetYaxis()->SetTitle("Signal");
		c.SaveAs(Form("%s/bunchPlot/bunchPlot%i.pdf", outDir.Data(), i));
		bunchPlot->Write();
	}

	delete clusterLocation;
	delete signalDistance;
	delete signalDistanceVsPad;
	delete signalDistanceTimeWindow;
	delete signalDistanceVsPadTimeWindow;
	delete signalDistanceVsSignal;
	delete timeVsSignal;
	delete bunchSignalTiming;
	delete tempProfile;

	fOutput->Close();
}

