// EventDisplay.cxx

#include "GEMCombinedTrack.h"
#include "GEMTrack.h"
#include "GEMCluster.h"
#include "GEMEvent.h"

#include <TString.h>
#include <TSystem.h>
#include <TTree.h>
#include <TClonesArray.h>
#include <TH2F.h>
#include <TAxis.h>
#include <TCanvas.h>
#include <TF1.h>
#include <TRandom3.h>

#include <vector>

#include "EventDisplay.h"
#include "GEMBeamTestPlotter.h"

#include "configurationParameters.h"

void printEventDisplays(TString type, std::vector <TH2F *> & eventDisplays, std::vector <TF1 *> & roc1TrackLines, std::vector <TF1 *> & roc2TrackLines, TString outDir)
{
	TCanvas c;
	
	// Ensure the the directory exists
	gSystem->Exec(Form("mkdir -p %s/eventDisplay", outDir.Data()));

	TH2F * eventDisplay = 0;

	for(unsigned int i = 0; i < eventDisplays.size(); i++)
	{
		// Draw event
		eventDisplay = eventDisplays.at(i);
		eventDisplay->Draw("colz");
		eventDisplay->GetXaxis()->SetTitle("Row");
		eventDisplay->GetYaxis()->SetTitle("Pad");

		// Draw lines
		Printf("roc1 slope: %f\toffset: %f", roc1TrackLines.at(i)->GetParameter(0), roc1TrackLines.at(i)->GetParameter(1));
		Printf("roc2 slope: %f\toffset: %f", roc2TrackLines.at(i)->GetParameter(0), roc2TrackLines.at(i)->GetParameter(1));
		/*// TEMP
		  TH1F temp("temp", "temp", 10, 0, 60);
		  temp.GetYaxis()->SetRangeUser(-10, 0);
		  temp.Draw();
		// END TEMP*/
		roc1TrackLines.at(i)->Draw("same");
		roc2TrackLines.at(i)->Draw("same");
		// TEMP
		//c.SaveAs(Form("%s/eventDisplay/lines%d.pdf", outDir.Data(), i));
		// END TEMP
		c.SaveAs(Form("%s/eventDisplay/eventDisplay%d%s.pdf", outDir.Data(), i, type.Data()));
	}
}

void EventDisplay(TTree * tree, displayProperties * properties)
{
	// Create the display properties with defaults if they do not exist
	Bool_t createdDisplayProperties = kFALSE;
	if (properties == 0)
	{
		properties = new displayProperties();
		createdDisplayProperties = kTRUE;
	}

	// Determine output directory
	TString outDir = Form(configurationParameters::fAnalysisOutputLocation, "GEMoutput");

	// Create directory for output
	gSystem->Exec(Form("mkdir -p %s", outDir.Data()));
	printf("Output directory: %s\n", outDir.Data());

	// Tracks and clusters
	GEMEvent * event = 0;
	TClonesArray * combinedTracks = 0;
	GEMCombinedTrack * combinedTrack = 0;
	const GEMTrack * track = 0;
	const GEMCluster * cluster = 0;
	
	// Bunches
	TClonesArray * bunches = 0;
	GEMBunch * bunch = 0;

	// Event display
	TString eventDisplayClusterName = "event%dWithClusters";
	TString eventDisplayBunchName = "event%dWithBunches";
	std::vector <TF1 *> roc1TrackLines;
	std::vector <TF1 *> roc2TrackLines;
	std::vector <TH2F *> eventDisplaysClusters;
	std::vector <TH2F *> eventDisplaysBunches;
	TRandom3 rand(0);

	TH2F * eventDisplayCluster = 0;
	TH2F * eventDisplayBunch = 0;
	TF1 * combinedLine = 0;
	TF1 * roc1Line = 0;
	TF1 * roc2Line = 0;

	tree->SetBranchAddress("Events", &event);
	tree->SetBranchAddress("Bunches", &bunches);

	for (int iEvent = 0; iEvent < tree->GetEntriesFast(); iEvent++)
	{
		// Clusters and bunches should be the same size
		if (eventDisplaysClusters.size() >= static_cast<unsigned int>(properties->numberOfEventDisplays)) { break; }
		if (properties->maxEventNumber <= iEvent) { break; }
		if ( (properties->useRandomEvents == kTRUE) && (rand.Rndm() < 0.8) ) { continue; }
		if (properties->minEventNumber >= iEvent) { continue; }

		tree->GetEntry(iEvent);
		
		if (event == 0) { Printf("Didn't assign event correctly."); continue; }

		combinedTracks = event->GetCombinedTracks();

		if (combinedTracks == 0) { Printf("Didn't assign combinedTracks correctly."); continue; }

		// This will only loop if we have a non-zero number of combined tracks
		for (int iCombinedTrack = 0; iCombinedTrack < combinedTracks->GetEntriesFast(); iCombinedTrack++)
		{
			// Cannot get here unless combinedTracks is valid
			combinedTrack = static_cast<GEMCombinedTrack *> (combinedTracks->At(iCombinedTrack));

			if (combinedTrack == 0) { Printf("Didn't assign combinedTrack correctly."); continue; }
			
			// Only look at combined tracks where there are two subtracks
			if (combinedTrack->GetNumberOfSubTracks() != 2)
			{
				//Printf("Only %d subtracks! Continuing", combinedTrack->GetNumberOfSubTracks());
				continue;
			}

			if (!GEMBeamTestPlotter::IsTrackAccepted(combinedTrack)) { continue; }

			bool createdHist = false;
			// Get tracks here
			for (int iTrack = 0; iTrack < combinedTrack->GetNumberOfSubTracks(); iTrack++)
			{
				track = combinedTrack->GetTrack(iTrack);

				if (track == 0) { Printf("Didn't assign track correctly."); continue; }

				if (!GEMBeamTestPlotter::IsTrackAccepted(track)) { continue; }

				if (createdHist == false)
				{
					eventDisplayCluster = new TH2F(Form(eventDisplayClusterName, iEvent), Form(eventDisplayClusterName, iEvent), 70, -2, 68, 12, 27, 39);
					eventDisplaysClusters.push_back(eventDisplayCluster);
					eventDisplayBunch = new TH2F(Form(eventDisplayBunchName, iEvent), Form(eventDisplayBunchName, iEvent), 70, -2, 68, 12, 27, 39);
					eventDisplaysBunches.push_back(eventDisplayBunch);
					createdHist = true;
				}

				for (int iCluster = 0; iCluster < track->GetNumberOfClusters(); iCluster++)
				{
					// Cannot get here unless track is valid
					cluster =track->GetCluster(iCluster);

					if (cluster == 0) { Printf("Didn't assign cluster correctly."); continue; }

					// Only after above to ensure that the pad values are not distorted
					Double_t clusterRow = cluster->GetRow();
					Double_t clusterPad = cluster->GetMaxPad();
					// Move the cluster down to the location of roc1
					if (track->GetROC() == 1) { clusterRow += 32; }

					eventDisplayCluster->Fill(clusterRow, clusterPad, cluster->GetQMax());
				}

				if (track->GetROC() == 1)
				{
					roc1Line = new TF1(Form("roc1%d", iEvent), "[0]*x+[1]", 0+32, 32+32);
					roc1Line->SetParameter(0, track->GetSlope());
					//roc1Line->SetParameter(1, track->GetOffset());
					// We shift the offset by slope*32, because we are shifting it down 32 when we actually view it
					roc1Line->SetParameter(1, track->GetOffset() - (track->GetSlope()*32) );
					roc1Line->SetLineColor(kBlack);
					roc1TrackLines.push_back(roc1Line);
				}
				else
				{
					roc2Line = new TF1(Form("roc2%d", iEvent), "[0]*x+[1]", 0, 32);
					roc2Line->SetParameter(0, track->GetSlope());
					roc2Line->SetParameter(1, track->GetOffset());
					roc2Line->SetLineColor(kRed);
					roc2TrackLines.push_back(roc2Line);
				}
			}

			// Write bunches to event display
			for (int iBunch = 0; iBunch < bunches->GetEntriesFast(); iBunch++)
			{
				bunch = static_cast<GEMBunch *> (bunches->At(iBunch));

				if (bunch == 0) { Printf("Didn't assign bunch correctly"); continue; }

				Double_t bunchRow = bunch->GetRow();
				Double_t bunchPad = bunch->GetPad();	
				// Move the bunch down to the location of roc1
				if (bunch->GetROC() == 1) { bunchRow += 32; }

				// First pass: just integrate the signal for +/- timeWindow
				Int_t signal = 0;
				Int_t * signalArray = bunch->GetSignalArray();

				for (int i = 0; i < bunch->GetSignalArrayLength(); i++)
				{
					signal += signalArray[i];
				}

				eventDisplayBunch->Fill(bunchRow, bunchPad, signal);
			}

		}
	}

	// Print out the event displays
	printEventDisplays("Clusters", eventDisplaysClusters, roc1TrackLines, roc2TrackLines, outDir);
	printEventDisplays("Bunches", eventDisplaysBunches, roc1TrackLines, roc2TrackLines, outDir);

	if (createdDisplayProperties == kTRUE)
	{
		delete properties;
	}
	// Root will delete everything automatically, so don't risk calling delete
}
