#ifndef EVENTDISPLAY_H
#define EVENTDISPLAY_H

struct displayProperties
{
	// Constructors
	displayProperties(): useRandomEvents(kTRUE), numberOfEventDisplays(10), minEventNumber(-1), maxEventNumber(INT_MAX) {}
	displayProperties(Bool_t randomEvents, Int_t nEventDisplays, Int_t nMinEvent, Int_t nMaxEvent):
		useRandomEvents(randomEvents), numberOfEventDisplays(nEventDisplays), minEventNumber(nMinEvent), maxEventNumber(nMaxEvent) {}

	// Fields
	Bool_t useRandomEvents;
	Int_t numberOfEventDisplays;
	Int_t minEventNumber;
	Int_t maxEventNumber;
};

void printEventDisplays(TString type, std::vector <TH2F *> & eventDisplays, std::vector <TF1 *> & roc1TrackLines, std::vector <TF1 *> & roc2TrackLines, TString outDir);
void EventDisplay(TTree * tree, displayProperties * properties = 0);

#endif /* EventDisplay.h */
