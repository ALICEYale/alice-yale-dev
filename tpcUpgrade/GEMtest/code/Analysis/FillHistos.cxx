// FillHistos.cxx
// Makes dE/dx plots, as well various cluster plots
// Run from code/macros: root RunFillHistos.C

#include "TMath.h"
#include "TH2.h"
#include "TChain.h"
#include "TStyle.h"
#include "TLegend.h"
#include "TCanvas.h"
#include "TFile.h"
#include "TF1.h"
#include "TPaveText.h"
#include "TTree.h"
#include "TH1.h"
#include "TROOT.h"
#include "TSystem.h"

#include "GEMEvent.h"
#include "GEMTrack.h"
#include "GEMCluster.h"

#include "FillHistos.h"

#include "../Analysis/configurationParameters.h"
#include "../Analysis/GEMBeamTestPlotter.h"

//dEdx
TObjArray *arrHistdEdxMaxEle = 0x0;
TObjArray *arrHistdEdxTotEle = 0x0;
TObjArray *arrHistdEdxMaxPio = 0x0;
TObjArray *arrHistdEdxTotPio = 0x0;

TObjArray *arrHistClustersQMaxEle = 0x0;
TObjArray *arrHistClustersQMaxPio = 0x0;
TObjArray *arrHistNcl             = 0x0;

TString outdir;

GEMEvent *fEvent=0x0;

//__________________________________________________________________________
void FillHistos(TTree *t, Int_t roc)
{
	// Determine output directory
	outdir = Form(configurationParameters::fAnalysisOutputLocation, "GEMoutput");

	// Create directory for output
	gSystem->Exec(Form("mkdir -p %s", outdir.Data()));
	printf("Output directory: %s\n", outdir.Data());

	EPartType partType=kEle;

	InitHistogramArrays();

	TObjArray *arrHistdEdxMax=0x0;
	TObjArray *arrHistdEdxTot=0x0;

	t->SetBranchAddress("Events",&fEvent);
	
	gROOT->cd();

	Int_t selectedTracks=0;
	TString lastID;

	for (Int_t iev=0; iev<t->GetEntries(); ++iev){
	  if(iev%10000 == 0) std::cout<<"Processing event: "<<iev<<std::endl;
		t->GetEntry(iev);
		Int_t run=fEvent->GetRunNumber();

		TString ID = Form("ROC%02d_Run%d",roc,fEvent->GetRunNumber());

		if (!lastID.IsNull() && ID!=lastID && selectedTracks) { Process(lastID); }
		selectedTracks=0;

		//================================================
		//apply cuts
		// This works for combined tracks, as we always have one
		if (fEvent->GetNumberOfTracks(roc)!=1) continue;

		const GEMTrack *track=0x0;
		Int_t numberOfTracks = 0;
		if (roc == 255) {
			numberOfTracks = fEvent->GetNumberOfCombinedTracks();
		}
		else {
			numberOfTracks = fEvent->GetNumberOfTracks();
		}

		for (Int_t itrack=0; itrack < numberOfTracks; ++itrack)	{
		  if (roc == 255) {
		    track=fEvent->GetCombinedTrack(itrack);
		  }
		  else {
		    track=fEvent->GetTrack(itrack); 
		  }
		  // only analyse one readout chamber
		  if ( track->GetROC() == roc ) break;      
		}

		if ( track->GetROC() != roc ) continue;

		//----- PID
		if (fEvent->GetCherenkovValue()<40){ // PIONS
			arrHistdEdxMax=arrHistdEdxMaxPio;
			arrHistdEdxTot=arrHistdEdxTotPio;
			partType=kPio;
		} else if (fEvent->GetCherenkovValue()>50){ // Electrons
			arrHistdEdxMax=arrHistdEdxMaxEle;
			arrHistdEdxTot=arrHistdEdxTotEle;
			partType=kEle;
		} else {
			continue;
		}

		if(!GEMBeamTestPlotter::IsTrackAccepted(track)) continue;
		++selectedTracks;

		//================================================
		// Fill dEdx histograms
		TH1F *hNcl = GetHistNcl(ID, partType);
		hNcl->Fill(track->GetNumberOfClusters());

		TH1F *dEdxMax = GetdEdxHisto(arrHistdEdxMax, ID.Data(), partType, 0);
		TH1F *dEdxTot = GetdEdxHisto(arrHistdEdxTot, ID.Data(), partType, 1);

		// Only use tracks that have fewer than nThresh clusters below qThresh
		Bool_t doAPSthresholds = false;
		Int_t nLowQClusters = 0; 
		Int_t nThresh = 0;
		Int_t qThresh = 0;
		if(partType==kPio) nThresh = 6;
		if(partType==kPio) qThresh = 6;
		if(partType==kEle) nThresh = 6;
		if(partType==kEle) qThresh = 15;
		for (Int_t i=0; i<track->GetNumberOfClusters(); ++i){
		  const GEMCluster *c=track->GetCluster(i);
		  if(c->GetQMax() < qThresh) nLowQClusters++;
		}
		Double_t dEdxTotTruncMean=0;
		Double_t dEdxMaxTruncMean = track->GetTruncatedMean(0.0, configurationParameters::fTruncatedMeanFraction,0);
		dEdxTotTruncMean = track->GetTruncatedMean(0.0, configurationParameters::fTruncatedMeanFraction, 1);	
		if(doAPSthresholds){
		  if(nLowQClusters < nThresh){
		    dEdxMax->Fill(dEdxMaxTruncMean);
		    dEdxTot->Fill(dEdxTotTruncMean);
		  }
		} else {
		  dEdxMax->Fill(dEdxMaxTruncMean);
		  dEdxTot->Fill(dEdxTotTruncMean);
		}

		//================================================
		// cluster QMax histograms
		TH1F *fClQMax = GetHistoTrack1D(ID,partType,0);           // Histogram max cluster charge, all clusters
		TH1F *hLowQClusters = GetHistoTrack1D(ID,partType,1);     // Histogram tracks of given number QMax<10 clusters
		TH1F *hLowQClustersTail = GetHistoTrack1D(ID,partType,2); // Only consider tracks in low-dE/dx tail
		TH2F *hLowQOccupancy = GetHistoTrack2D(ID,partType,0);    // Histogram pad occupancy for QMax<10 clusters
		TH2F *hLowQOccupancyTail = GetHistoTrack2D(ID,partType,1);// Only consider tracks in low-dE/dx tail
		TH2F *hHighQOccupancy = GetHistoTrack2D(ID,partType,2);   // Histogram pad occupancy for QMax>40 clusters

		Int_t nClusters = track->GetNumberOfClusters();
		Int_t lowQclusters = 0;
		for (Int_t icl=0; icl<nClusters; ++icl){
		  const GEMCluster *c=track->GetCluster(icl);
		  Int_t row=c->GetRow();
		  Int_t pad=c->GetPad();
		  Int_t rowType=0;
		
		  // Fill various cluster histograms
		  if(doAPSthresholds){
		    if(nLowQClusters < nThresh) fClQMax->Fill(c->GetQMax());
		  } else{
		    fClQMax->Fill(c->GetQMax());
		  }
		  
		  if (c->GetQMax() < 10) {
		    lowQclusters++;
		    hLowQOccupancy->Fill(row,pad);
		    if(dEdxTotTruncMean<150) hLowQOccupancyTail->Fill(row,pad);
		  }
		  if (c->GetQMax() > 10) {
		    hHighQOccupancy->Fill(row,pad);
		  }
		}
		hLowQClusters->Fill(lowQclusters);
		if(dEdxTotTruncMean<150) hLowQClustersTail->Fill(lowQclusters);
		
		lastID=ID;
	}

	printf("Final\n");
	Process(lastID);

        // Write GEM_histos.root                                                                                  
        TFile fout(Form("%s/GEM_histos.root",outdir.Data()),"recreate");

	arrHistdEdxMaxEle       -> Write(0,TObjArray::kSingleKey);
	arrHistdEdxTotEle       -> Write(0,TObjArray::kSingleKey);
	arrHistdEdxMaxPio       -> Write(0,TObjArray::kSingleKey);
	arrHistdEdxTotPio       -> Write(0,TObjArray::kSingleKey);
	arrHistClustersQMaxEle  -> Write(0,TObjArray::kSingleKey);
	arrHistClustersQMaxPio  -> Write(0,TObjArray::kSingleKey);
	arrHistNcl              -> Write(0,TObjArray::kSingleKey);

	fout.Close();
}

//__________________________________________________________________________
void Process(const TString &ID)
{
	ProcessdEdx(ID);
	ProcessClQMax(ID);
	//ProcessClQMax_LowQ(ID,4);
	//ProcessClQMax_LowQ(ID,5);
	//ProcessClQMax_Occupancy(ID,0);
	//ProcessClQMax_Occupancy(ID,1);
	//ProcessClQMax_Occupancy(ID,2);
}

//====================================================================================
//====================================  dEdx stuff ===================================
//====================================================================================

//__________________________________________________________________________
TH1F* GetHistoTrack1D(const TString &ID, EPartType part, Int_t type, Bool_t force)
{
	//
	// part: 0=electron; 1=pion
	//       3=ClusterQMax
	//       4=ClusterQMax_LowQ
        //       5=ClusterQMax_LowQTail

	TObjArray *arr=0x0;
	Int_t nbins=100;
	Double_t min=-1;
	Double_t max=1;
	TString typeName;

	switch (type) {
		case 0:
			nbins=400;
			typeName="ClQMax";
			arr=(part==kEle)?arrHistClustersQMaxEle:arrHistClustersQMaxPio;
			min=0; max=400;
			break;
	case 1:
	  nbins=20;
	  typeName="ClQMax_LowQ";
	  arr=(part==kEle)?arrHistClustersQMaxEle:arrHistClustersQMaxPio;
	  min=0; max=20;
	  break;
	case 2:
	  nbins=20;
	  typeName="ClQMax_LowQTail";
	  arr=(part==kEle)?arrHistClustersQMaxEle:arrHistClustersQMaxPio;
	  min=0; max=20;
	  break;
	}

	TString name=Form("%s%s_%s",typeName.Data(),part==kEle?"Ele":"Pion",ID.Data());
	//   printf("Search for '%s'\n",name.Data());
	TH1F *dEdxTot = (TH1F*)arr->FindObject(name.Data());
	if (dEdxTot || !force) return dEdxTot;

	dEdxTot = new TH1F(name.Data(),name.Data(),nbins,min,max);

	arr->Add(dEdxTot);
	return dEdxTot;
}

//__________________________________________________________________________
TH2F* GetHistoTrack2D(const TString &ID, EPartType part, Int_t type, Bool_t force)
{
	//
	// part: 0=electron; 1=pion
	// type: 0=Low occupancy
	//       1=Low occ tail
        //       2=High occ

	TObjArray *arr=0x0;
	TString typeName;
	Int_t xmin = 0;
	Int_t xmax = 32;
	Int_t ymin = 29;
	Int_t ymax = 37;

	switch (type) {
	case 0:
	  typeName="ClQMax_LowQOccupancy";
	  arr=(part==kEle)?arrHistClustersQMaxEle:arrHistClustersQMaxPio;
	  break;
	case 1:
	  typeName="ClQMax_LowQOccupancyTail";
	  arr=(part==kEle)?arrHistClustersQMaxEle:arrHistClustersQMaxPio;
	  break;
	case 2:
	  typeName="ClQMax_HighOccupancy";
	  arr=(part==kEle)?arrHistClustersQMaxEle:arrHistClustersQMaxPio;
	  break;
	}

	TString name=Form("%s_%s_%s",typeName.Data(),part==kEle?"Ele":"Pion",ID.Data());
	//   printf("Search for '%s'\n",name.Data());
	TH2F *dEdxTot = (TH2F*)arr->FindObject(name.Data());
	if (dEdxTot || !force) return dEdxTot;

	dEdxTot = new TH2F(name.Data(),name.Data(),xmax-xmin,xmin,xmax,ymax-ymin,ymin,ymax);

	arr->Add(dEdxTot);
	return dEdxTot;
}

//__________________________________________________________________________
TH1F* GetdEdxHisto(TObjArray *arr, const char* addName, EPartType part, Int_t type, Bool_t force)
{
	//
	// part: 0=electron; 1=pion
	// type: 0=Max; 1=Tot
	//

	TString name=Form("dEdx_%s_%s_%s",part==kEle?"Ele":"Pion",(type==0)?"Max":"Tot",addName);
	//   printf("Search for '%s'\n",name.Data());
	TH1F *dEdxTot = (TH1F*)arr->FindObject(name.Data());
	if (dEdxTot || !force) return dEdxTot;

	Int_t nbins=100;
	Double_t max=120.;
	if (type==1) {
		nbins=200;
		max=400.;
	}

	dEdxTot = new TH1F(name.Data(),
			Form("dE/dx %s %s: %s Charge;%s dE/dx %s",
				part==kEle?"Ele":"Pion",
				addName,
				(type==0)?"Max":"Tot",
				(type==0)?"Max":"Tot",
				part==kEle?"Ele":"Pio"
				),
			nbins,0,max);

	arr->Add(dEdxTot);
	return dEdxTot;
}

//__________________________________________________________________________
void GetBinMinMax(const TH1 *hist, const Float_t frac, Int_t &bin1, Int_t &bin2)
{

	const Int_t binMax=hist->GetMaximumBin();
	const Double_t contMax=hist->GetBinContent(binMax);
	bin1=binMax;
	bin2=binMax;
	while ( (bin1--)>binMax/3. ) if (hist->GetBinContent(bin1)<frac*contMax) break;
	while ( (bin2++)<binMax*3. ) if (hist->GetBinContent(bin2)<frac*contMax) break;
}

//__________________________________________________________________________
TPaveText* Fit(TH1 *histEle, TH1 *histPio, Int_t type, Int_t fitType, const TString &ID)
{
	//
	// fit gaus to histograms and add text to the pave
	//
  TPaveText *text=new TPaveText(0.6,.7,.99,.9,"NDC");
  text->SetBorderSize(1);
  text->SetFillColor(10);
  text->SetDrawOption("same");
  
  TF1 *fEle=new TF1("gEle",(fitType==0)?"gaus":"landau",histEle->GetXaxis()->GetXmin(),histEle->GetXaxis()->GetXmax());
  TF1 *fPio=new TF1("gPio",(fitType==0)?"gaus":"landau",histPio->GetXaxis()->GetXmin(),histPio->GetXaxis()->GetXmax());
  fEle->SetLineColor(histEle->GetLineColor());
  fPio->SetLineColor(histPio->GetLineColor());
  fEle->SetLineWidth(2);
  fPio->SetLineWidth(2);
  const Float_t frac=0.2;
  Int_t bin1=0,bin2=0;
  
  GetBinMinMax(histEle,frac,bin1,bin2);
  histEle->Fit(fEle,"QN0","",histEle->GetXaxis()->GetBinLowEdge(bin1),histEle->GetXaxis()->GetBinUpEdge(bin2));
  histEle->GetListOfFunctions()->Add(fEle);
  
  GetBinMinMax(histPio,frac,bin1,bin2);
  histPio->Fit(fPio,"QN0","",histPio->GetXaxis()->GetBinLowEdge(bin1),histPio->GetXaxis()->GetBinUpEdge(bin2));
  histPio->GetListOfFunctions()->Add(fPio);
  
  Double_t posEle=fEle->GetParameter(1);
  Double_t resEle=fEle->GetParameter(2);
  Double_t posPio=fPio->GetParameter(1);
  Double_t resPio=fPio->GetParameter(2);
  text->AddText(Form("e: %.2f #pm %.2f (%.2f%%)",posEle,resEle, resEle/posEle*100));
  text->AddText(Form("#pi: %.2f #pm %.2f (%.2f%%)",posPio,resPio,resPio/posPio*100));
  text->AddText(Form("Separation: %.2f#sigma", TMath::Abs(posEle-posPio)/( (resEle+resPio)/2.)));

	histEle->SetMaximum(1.2*TMath::Max(histEle->GetBinContent(histEle->GetMaximumBin()),histPio->GetBinContent(histPio->GetMaximumBin())));
	histPio->SetMaximum(1.2*TMath::Max(histEle->GetBinContent(histEle->GetMaximumBin()),histPio->GetBinContent(histPio->GetMaximumBin())));

	TH1F *hNclEle = GetHistNcl(ID, kEle);
	Double_t nclEle = hNclEle->GetMean();
	TH1F *hNclPio = GetHistNcl(ID, kPio);
	Double_t nclPio = hNclPio->GetMean();

	return text;
}

//__________________________________________________________________________
void ProcessdEdx(const TString &ID)
{
	gStyle->SetOptStat(0);
	TH1F *dEdxMaxEle = GetdEdxHisto(arrHistdEdxMaxEle, ID.Data(), kEle, 0, kFALSE);
	if (!dEdxMaxEle) return;

	TH1F *dEdxTotEle = GetdEdxHisto(arrHistdEdxTotEle, ID.Data(), kEle, 1, kFALSE);
	if (!dEdxTotEle) return;

	TH1F *dEdxMaxPio = GetdEdxHisto(arrHistdEdxMaxPio, ID.Data(), kPio, 0, kFALSE);
	if (!dEdxMaxPio) return;

	TH1F *dEdxTotPio = GetdEdxHisto(arrHistdEdxTotPio, ID.Data(), kPio, 1, kFALSE);
	if (!dEdxTotPio) return;

	dEdxMaxEle->SetLineColor(kRed); dEdxMaxPio->SetLineColor(kBlue);
	dEdxTotEle->SetLineColor(kRed); dEdxTotPio->SetLineColor(kBlue);

	TLegend *leg=new TLegend(0.1,0.78,0.5,.9);
	leg->SetBorderSize(1);
	leg->SetFillColor(10);
	leg->AddEntry(dEdxMaxEle,"electrons","l");
	leg->AddEntry(dEdxMaxPio,"pions","l");

	TCanvas *cPH = new TCanvas(Form("cPH_%s",ID.Data()),Form("Pulse Height Distributions; run %s",ID.Data()));

	TPaveText *t1=Fit(dEdxMaxEle,dEdxMaxPio, 0, 0, ID);
	TPaveText *t2=Fit(dEdxTotEle,dEdxTotPio, 1, 0, ID);

	cPH->Clear();
	cPH->Divide(2,1);


	cPH->cd(1);
	dEdxMaxEle->Draw();
	dEdxMaxPio->Draw("same");
	leg->Draw("same");
	t1->Draw("same");

	cPH->cd(2);
	dEdxTotEle->Draw();
	dEdxTotPio->Draw("same");
	t2->Draw("same");

	cPH->SaveAs(Form("%s/dEdx_%s.pdf",outdir.Data(), ID.Data()));
	delete cPH;
}

//__________________________________________________________________________
void ProcessClQMax(const TString &ID)
{
	TH1F *hclQmaxEle = GetHistoTrack1D(ID.Data(), kEle, 0, kFALSE);
	TH1F *hclQmaxPio = GetHistoTrack1D(ID.Data(), kPio, 0, kFALSE);
	
	hclQmaxEle->SetLineColor(kRed); hclQmaxPio->SetLineColor(kBlue);

	TLegend *leg=new TLegend(0.1,0.78,0.5,.9);
	leg->SetBorderSize(1);
	leg->SetFillColor(10);
	leg->AddEntry(hclQmaxEle,"electrons","l");
	leg->AddEntry(hclQmaxPio,"pions","l");

	TCanvas *cPH = new TCanvas(Form("cQmax_%s",ID.Data()),Form("Qmaxt Distributions; run %s",ID.Data()));

	TPaveText *t1=Fit(hclQmaxEle,hclQmaxPio, 0, 1, ID);

	cPH->Clear();

	cPH->cd();
	gStyle->SetOptStat(0);
	hclQmaxEle->Draw();
	hclQmaxPio->Draw("same");
	//leg->Draw("same");
	t1->Draw("same");

	cPH->SaveAs(Form("%s/ClQMax_%s.pdf",outdir.Data(), ID.Data()));
	delete cPH;

}

//__________________________________________________________________________
void ProcessClQMax_LowQ(const TString &ID, Int_t type)
{
  TH1F *hclQmaxEle = GetHistoTrack1D(ID.Data(), kEle, type, kFALSE);
  TH1F *hclQmaxPio = GetHistoTrack1D(ID.Data(), kPio, type, kFALSE);

  hclQmaxEle->SetLineColor(kRed); hclQmaxPio->SetLineColor(kBlue);

  TLegend *leg=new TLegend(0.6,0.78,0.9,.9);
  leg->SetBorderSize(1);
  leg->SetFillColor(10);
  leg->AddEntry(hclQmaxEle,"electrons","l");
  leg->AddEntry(hclQmaxPio,"pions","l");

  TCanvas *cPH = new TCanvas(Form("cQmax_%s",ID.Data()),Form("Qmaxt Distributions; run %s",ID.Data()));
  cPH->Clear();
  cPH->cd();
  hclQmaxEle->Draw();
  hclQmaxPio->Draw("same");
  leg->Draw("same");

  if(type==4) cPH->SaveAs(Form("%s/ClQMax_LowQ_%s.pdf",outdir.Data(), ID.Data()));
  if(type==5) cPH->SaveAs(Form("%s/ClQMax_LowQTail_%s.pdf",outdir.Data(), ID.Data()));
  delete cPH;
}

//__________________________________________________________________________
void ProcessClQMax_Occupancy(const TString &ID, Int_t type)
{
  // draw for electrons only
  TH2F *hclQmaxEle = GetHistoTrack2D(ID.Data(), kEle, type, kFALSE);
  hclQmaxEle->GetZaxis()->SetRangeUser(0,5000);
  TCanvas *cPH = new TCanvas("c","c",900,500);
  cPH->cd();
  hclQmaxEle->Draw("colz");

  if(type==0) cPH->SaveAs(Form("%s/ClQMax_LowQOccupancy_%s.pdf",outdir.Data(), ID.Data()));
  if(type==1) cPH->SaveAs(Form("%s/ClQMax_LowQOccupancyTail_%s.pdf",outdir.Data(), ID.Data()));
  if(type==2) cPH->SaveAs(Form("%s/ClQMax_HighQOccupancy_%s.pdf",outdir.Data(), ID.Data()));
  delete cPH;
}

//__________________________________________________________________________
TH1F* GetHistNcl(const TString &ID, EPartType part, Bool_t force/*=kTRUE*/)
{
	TString name=Form("ncl_%s_%s",part==kEle?"Ele":"Pion",ID.Data());
	//   printf("Search for '%s'\n",name.Data());
	TH1F *hNcl = (TH1F*)arrHistNcl->FindObject(name.Data());
	if (hNcl || !force) return hNcl;

	hNcl=new TH1F(name.Data(),name.Data(),100,10,30);
	arrHistNcl->Add(hNcl);

	return hNcl;
}

//__________________________________________________________________________
void InitHistogramArrays()
{
	// initialise the arrays to store the histograms per run

	arrHistdEdxMaxEle       = new TObjArray;
	arrHistdEdxTotEle       = new TObjArray;
	arrHistdEdxMaxPio       = new TObjArray;
	arrHistdEdxTotPio       = new TObjArray;
	arrHistClustersQMaxEle  = new TObjArray;
	arrHistClustersQMaxPio  = new TObjArray;
	arrHistNcl              = new TObjArray;

	arrHistdEdxMaxEle       -> SetName("dEdx_Ele_Max");
	arrHistdEdxTotEle       -> SetName("dEdx_Ele_Tot");
	arrHistdEdxMaxPio       -> SetName("dEdx_Pio_Max");
	arrHistdEdxTotPio       -> SetName("dEdx_Pio_Tot");
	arrHistClustersQMaxEle  -> SetName("Ele_ClustersQMax");
	arrHistClustersQMaxPio  -> SetName("Pio_ClustersQMax");
	arrHistNcl              -> SetName("Ncl");
}
