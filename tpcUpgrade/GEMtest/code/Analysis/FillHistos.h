#ifndef FILLHISTOS_H
#define FILLHISTOS_H


enum EPartType { kEle=0, kPio=1 };

void FillHistos(TTree *t, Int_t roc);

TH1F* GetdEdxHisto(TObjArray *arr, const char* addName, EPartType partType, Int_t type, Bool_t force=kTRUE);
TH1F* GetHistoTrack1D(const TString &ID, EPartType part, Int_t type, Bool_t force=kTRUE);
TH2F* GetHistoTrack2D(const TString &ID, EPartType part, Int_t type, Bool_t force=kTRUE);
TH1F* GetHistNcl(const TString &ID, EPartType part, Bool_t force=kTRUE);
TPaveText* Fit(TH1 *histEle, TH1 *histPio, Int_t type, Int_t fitType, const TString &ID);
void Process(const TString &ID);

void ProcessdEdx(const TString &ID);
void ProcessClQMax(const TString &ID);
void ProcessClQMax_LowQ(const TString &ID, Int_t type); 
void ProcessClQMax_Occupancy(const TString &ID, Int_t type); 
void InitHistogramArrays();

#endif /* FillHistos.h */
