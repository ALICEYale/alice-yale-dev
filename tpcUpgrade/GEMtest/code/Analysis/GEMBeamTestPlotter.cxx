#include <TChain.h>
#include <TFile.h>
#include <TSystem.h>
#include <TSystemFile.h>
#include <TSystemDirectory.h>
#include <TList.h>
#include <TTree.h>
#include <TRegexp.h>
#include <TCanvas.h>
#include <TH1I.h>
#include <TH1F.h>
#include <TH2F.h>
#include <TStyle.h>
#include <TMath.h>
#include <TPaveText.h>
#include <TPaletteAxis.h>
#include <TObject.h>
#include <TKey.h>
#include <TLegend.h>
#include <TF1.h>
#include <TProfile.h>

#include "GEMBunch.h"
#include "GEMEvent.h"
#include "GEMCluster.h"
#include "GEMTrack.h"

#include "GEMBeamTestPlotter.h"

#include "configurationParameters.h"

ClassImp(GEMBeamTestPlotter);

// Define static debug level
Int_t GEMBeamTestPlotter::fDebugLevel = 2;

//______________________________________________________
GEMBeamTestPlotter::GEMBeamTestPlotter() :
  TNamed("GEMBeamTestPlotter","GEMBeamTestPlotter"),
  fOutputPath(),
  fFileOutName("plots.root"),
  fNumberOfROCs(3),
  fMaxPad(70),
  fMaxRow(70),
  fSaveOutput(kTRUE),
  fSavePlots(kTRUE),
  fPlotFormat("pdf"),
  fPlotHistograms(kTRUE),
  fPlotSignalPerPad(kFALSE),
  fMinADC(3),
  fMinEvent(0),
  fMaxEvent(-1),
  fTruncatedMeanFraction(0.70),
  fRenormalizeROC2(kFALSE),
  fTree(0),
  fRun(-1),
  fEvent(0),
  fBunches(0),
  fHistograms(new TList()),
  fCanvases(new TList()),
  fOutputFile(0),
  fRunChanged(kFALSE),
  fRunList(0),
  fNRuns(0),
  fCalibFileName(),
  fCalibFile(0),
  fNSelCombTracks(0)
{
  // Default constructor.

  for (Int_t roc = 0; roc < kNROC; roc++) {
    fGainMap[roc] = 0;
    fDeadChannelMap[roc] = 0;
    fNSelTracks[roc] = 0;
  }
}

//______________________________________________________
GEMBeamTestPlotter::GEMBeamTestPlotter(const char* outputPath) :
  TNamed("GEMBeamTestPlotter","GEMBeamTestPlotter"),
  fOutputPath(outputPath),
  fFileOutName("plots.root"),
  fNumberOfROCs(3),
  fMaxPad(70),
  fMaxRow(70),
  fSaveOutput(kTRUE),
  fSavePlots(kTRUE),
  fPlotFormat("pdf"),
  fPlotHistograms(kTRUE),
  fPlotSignalPerPad(kFALSE),
  fMinADC(3),
  fMinEvent(0),
  fMaxEvent(-1),
  fTruncatedMeanFraction(0.70),
  fRenormalizeROC2(kFALSE),
  fTree(0),
  fRun(-1),
  fEvent(0),
  fBunches(0),
  fHistograms(new TList()),
  fCanvases(new TList()),
  fOutputFile(0),
  fRunChanged(kFALSE),
  fRunList(0),
  fNRuns(0),
  fCalibFileName(),
  fCalibFile(0),
  fNSelCombTracks(0)
{
  // Constructor.

  for (Int_t roc = 0; roc < kNROC; roc++) {
    fGainMap[roc] = 0;
    fDeadChannelMap[roc] = 0;
    fNSelTracks[roc] = 0;
  }
}

//______________________________________________________
GEMBeamTestPlotter::~GEMBeamTestPlotter()
{
  // Destructor.
  
  if (fOutputFile) {
    fOutputFile->Close();
    delete fOutputFile;
  }

  if (fHistograms) {
    fHistograms->Delete();
    delete fHistograms;
  }

  if (fCanvases) {
    fCanvases->Delete();
    delete fCanvases;
  }

  if (fCalibFile) {
    fCalibFile->Close();
    delete fCalibFile;
  }

  if (fTree) delete fTree;
}

//______________________________________________________
Bool_t GEMBeamTestPlotter::Start(TTree* tree)
{
  // Start the analysis.

  Bool_t res = kFALSE;
  
  if (tree == 0) { // Do only the plotting using previously generated histograms.
    fSaveOutput = kFALSE;
    res = LoadOutput();
    if (res) res = RunLoop(kFALSE);
    return res;
  }

  // Generate the histograms and then plot them.

  // Init the analysis.
  res = Init(tree);

  // Start the event loop, where most of the analysis happens (event-by-event).
  if (res) res = EventLoop();

  // Start the run loop, where the analysis is terminated and the plotting is done (run-by-run).
  if (res) res = RunLoop(kTRUE);
  fRun = -1;
  
  if (res) {
    if (fSaveOutput) {
      // Save the generated histograms, if requested.
      res = SaveOutput();
      if (!res) Printf("***** An error occured while trying to save the output! *****");
    }
  }

  return res;
}

//______________________________________________________
Bool_t GEMBeamTestPlotter::LoadOutput()
{
  // Load the output file.

  TString fname = fOutputPath + "/" + fFileOutName;
  fOutputFile = TFile::Open(fname);
  if (!fOutputFile || fOutputFile->IsZombie()) {
    Printf("***** An error occured while trying to open the file '%s'! *****", fname.Data());
    return kFALSE;
  }

  TList* keys = fOutputFile->GetListOfKeys();
  if (!keys) {
    Printf("***** An error occured while trying to the list of keys from file '%s'! *****", fname.Data());
    return kFALSE;
  }

  TIter nextkey(keys);
  TKey *key = 0;
  while (key = static_cast<TKey*>(nextkey())) {
    TObject *obj = key->ReadObj();
    fHistograms->Add(obj);
    TString name(obj->GetName());
    if (name == "RunList") fRunList = dynamic_cast<TH1I*>(obj);
  }
  
  fRun = -1;
  fNRuns = 0;

  if (!fRunList) {
    Printf("***** Unable to find the run list in '%s'! *****", fname.Data());
    return kFALSE;
  }

  for (Int_t i = 1; i <= fRunList->GetNbinsX(); i++) {
    if (fRunList->GetBinContent(i) >= 0) {
      Printf("Found run %d", (Int_t)fRunList->GetBinContent(i));
      fNRuns++;
    }
  }

  return kTRUE;
}

//______________________________________________________
Bool_t GEMBeamTestPlotter::Init(TTree *tree)
{
  // Init the tree, by setting the branch address to pointers, plus some other analysis initializations.
  
  TH1::AddDirectory(kFALSE);

  if (!tree) {
    Printf("Unable to load chain. Returning...");
    return kFALSE;
  }

  fTree = tree;
  if (fTree->GetBranch("Bunches")) fTree->SetBranchAddress("Bunches", &fBunches);
  if (fTree->GetBranch("Events")) fTree->SetBranchAddress("Events", &fEvent);

  fRun = -1;
  if (!fRunList) {
    fRunList = new TH1I("RunList", "RunList", 10, 0, 10);
    fHistograms->Add(fRunList);
  }
  fRunList->TArrayI::Reset(-1);
  fRunChanged = kFALSE;

  if (!fCalibFileName.IsNull()) {
    fCalibFile = TFile::Open(fCalibFileName);
  }

  return kTRUE;
}

//______________________________________________________
void GEMBeamTestPlotter::RecalibrateTracks()
{
  RecalibrateTracks(kFALSE);
  RecalibrateTracks(kTRUE);
}

//______________________________________________________
void GEMBeamTestPlotter::RecalibrateTracks(Bool_t combTracks)
{
  Int_t ntracks = 0;

  if (combTracks) {
    ntracks = fEvent->GetNumberOfCombinedTracks();
  }
  else {
    ntracks = fEvent->GetNumberOfTracks();
  }

  for (Int_t itrack = 0; itrack < ntracks; itrack++) {
    GEMTrack* track = 0;
    
    if (combTracks) {
      track = fEvent->GetCombinedTrack(itrack);
    }
    else {
      track = fEvent->GetTrack(itrack);
    }
    
    if (!IsTrackAccepted(track)) continue;

    Int_t nclusters = track->GetNumberOfClusters();
    for (Int_t icluster = 0; icluster < nclusters; icluster++) {
      GEMCluster* cluster = const_cast<GEMCluster*>(track->GetCluster(icluster));
      if (!cluster) continue;

      Int_t roc = cluster->GetROC();

      if (roc >= kNROC || roc < 0) continue;
      if (!fGainMap[roc]) continue;
      
      Int_t row = cluster->GetRow();
      Int_t pad = cluster->GetPad();
      
      Double_t gainFactor = fGainMap[roc]->GetBinContent(row+1, pad+1);
      
      cluster->CorrectCharge(gainFactor);
    }
  }
}

//______________________________________________________
void GEMBeamTestPlotter::LoadCalib(Int_t run)
{
  if (fCalibFile && !fCalibFile->IsZombie()) {
    Printf("Info: loading calibration objects for run %d", run);
    fCalibOk = kTRUE;
    
    for (Int_t roc = 0; roc < kNROC; roc++) {
      fGainMap[roc] = dynamic_cast<TH2*>(fCalibFile->Get(Form("Run%d_ROC%d_PadGainMap", run, roc)));
      if (fGainMap[roc]) Printf("Info: gain map loaded for ROC %d", roc);
      fDeadChannelMap[roc] = dynamic_cast<TH2*>(fCalibFile->Get(Form("Run%d_ROC%d_BadChannelMap", run, roc)));
      if (fDeadChannelMap[roc]) Printf("Info: bad channel map loaded for ROC %d", roc);
    }
  }
}

//______________________________________________________
Bool_t GEMBeamTestPlotter::EventLoop()
{
  // Starts the event loop. Here most the analysis happens, event-by-event.
  
  Int_t maxevent = fTree->GetEntries();
  if (fMaxEvent > 0 && fMaxEvent < maxevent) maxevent = fMaxEvent;
  
  for (Int_t iev = fMinEvent; iev < maxevent; iev++) {
    if (fDebugLevel > 3 ||  (fDebugLevel > 0 && iev % 100 == 0)) Printf("Processing event %6d", iev);
    fTree->GetEntry(iev);
    if (!fEvent) Printf("**** An error occurred while processing event %d ****", iev);

    Int_t prevRun = fRun;
    fRun = fEvent->GetRunNumber();
    if (prevRun != fRun) fRunChanged = kTRUE;
    else fRunChanged = kFALSE;

    if (fRunChanged && prevRun > 0) {
      for (Int_t roc = 0; roc < kNROC; roc++) {
        Printf("Total number of tracks in run %d for ROC %d: %d", prevRun, roc, fNSelTracks[roc]);
        fNSelTracks[roc] = 0;
      }

      Printf("Total number of combined tracks in run %d: %d", prevRun, fNSelCombTracks);
      fNSelCombTracks = 0;
    }

    Bool_t res = Exec();
    if (!res) Printf("**** An error occurred while processing event %d ****", iev);
  }

  for (Int_t roc = 0; roc < kNROC; roc++) {
    Printf("Total number of tracks in run %d for ROC %d: %d", fRun, roc, fNSelTracks[roc]);
    fNSelTracks[roc] = 0;
  }

  Printf("Total number of combined tracks in run %d: %d", fRun, fNSelCombTracks);
  fNSelCombTracks = 0;
    
  return kTRUE;
}

//______________________________________________________
Bool_t GEMBeamTestPlotter::Exec()
{
  // Execute the analysis for the current event.
  
  if (fRunChanged) {
    Printf("Now processing run %d...", fRun);
    AddRun();
    LoadCalib(fRun);
  }

  if (fCalibOk) {
    RecalibrateTracks();
  }
  
  if (fBunches) {
    ExecGainMap();
  }
  if (fEvent) {
    ExecClusterGainMap();
    ExecDEDX();
    ExecTrackQA();
  }

  // Add more event-by-event analysis here.

  return kTRUE;
}

//______________________________________________________
void GEMBeamTestPlotter::AddRun()
{
  // Add a run to the list of analyzed runs.
  
  for (Int_t i = 1; i <= fNRuns; i++) { // Check if the run has been added already
    if (fRunList->GetBinContent(i) == fRun) return;
  }
  
  fNRuns++;
  if (fNRuns > fRunList->GetNbinsX()) {
    fRunList->Set(fRunList->GetNbinsX()*2+1);
    for (Int_t i = fNRuns; i <= fRunList->GetNbinsX()+1; i++) fRunList->SetBinContent(i, -1);
  }
  fRunList->SetBinContent(fNRuns, fRun);
}

//______________________________________________________
Bool_t GEMBeamTestPlotter::RunLoop(Bool_t terminate)
{
  // Starts the run loop. Here most the analysis is terminated (if requested) and the plotting is done, run-by-run.
  
  if (!fRunList) return kFALSE;
  
  for (Int_t i = 1; i <= fRunList->GetNbinsX(); i++) {
    if (fRunList->GetBinContent(i) < 0) continue;
    fRun = fRunList->GetBinContent(i);
    if (terminate) Terminate();
    if (fPlotHistograms) Plot();
  }

  return kTRUE;
}

//______________________________________________________
Bool_t GEMBeamTestPlotter::Terminate()
{
  // Terminate analysis.

  Bool_t res = kTRUE;

  if (fBunches) {
    res = TerminateGainMap();
  }

  if (fEvent) {
    res = TerminateClusterGainMap();
    res = TerminateDEDX();
    res = TerminateTrackQA();
  }

  // Add more analysis termination procedures here.

  return res;
}

//______________________________________________________
Bool_t GEMBeamTestPlotter::Plot()
{
  // Plotting function.

  Bool_t res = kTRUE;
  
  res = PlotGainMap();
  res = PlotClusterGainMap();
  res = PlotDEDX();
  res = PlotTrackQA();

  if (fSavePlots) {
    SaveAllPlots();
  }

  // Add more plotting procedures here.

  return res;
}

//______________________________________________________
Bool_t GEMBeamTestPlotter::SaveAllPlots()
{
  // Save all plots.
  
  if (!fCanvases) return kFALSE;
  TIter next(fCanvases);
  TCanvas *c = 0;
  while ((c = dynamic_cast<TCanvas*>(next()))) {
    SavePlot(c);
  }

  return kTRUE;
}

//______________________________________________________
Bool_t GEMBeamTestPlotter::SaveOutput() 
{
  // Save all histograms contained in fHistograms into fOutputFile.

  TString fname = fOutputPath + "/" + fFileOutName;
  gSystem->mkdir(fOutputPath, kTRUE);
  fOutputFile = TFile::Open(fname, "recreate");
  if (!fOutputFile || fOutputFile->IsZombie()) return kFALSE;
  fOutputFile->cd();
  fHistograms->Write();

  return kTRUE;
}

//______________________________________________________
TH1* GEMBeamTestPlotter::GetHistogram(const char* hname, const char* htitle, Int_t nbins, Double_t xmin, Double_t xmax, Bool_t overwrite)
{
  // Look for histogram in fHistograms; if it does not find it, creates one and add it to fHistograms.

  TH1* hist = static_cast<TH1*>(fHistograms->FindObject(hname));
  if (overwrite && hist) {
    fHistograms->Remove(hist);
    delete hist;
    hist = 0;
  }
  if (!hist) {
    if (nbins > 0) {
      if (fDebugLevel > 2) Printf("Creating histogram %s...", hname);
      hist = new TH1F(hname, htitle, nbins, xmin, xmax);
      fHistograms->Add(hist);
    }
    else {
      if (fDebugLevel > 1) Printf("Could not find histogram '%s'!", hname);
    }
  }
  else {
    if (fDebugLevel > 2) Printf("Histogram '%s' is being returned...", hname);
  }

  return hist;
}

//______________________________________________________
TH2* GEMBeamTestPlotter::GetHistogram(const char* hname, const char* htitle, Int_t nbinsx, Double_t xmin, Double_t xmax, Int_t nbinsy, Double_t ymin, Double_t ymax, Bool_t overwrite)
{
  // Look for histogram in fHistograms; if it does not find it, creates one and add it to fHistograms.

  TH2* hist = static_cast<TH2*>(fHistograms->FindObject(hname));
  if (overwrite && hist) {
    fHistograms->Remove(hist);
    delete hist;
    hist = 0;
  }
  if (!hist && nbinsx > 0 && nbinsy > 0) {
    if (fDebugLevel > 2) Printf("Creating histogram %s...", hname);
    hist = new TH2F(hname, htitle, nbinsx, xmin, xmax, nbinsy, ymin, ymax);
    fHistograms->Add(hist);
  }

  return hist;
}

//______________________________________________________
TH1* GEMBeamTestPlotter::DuplicateHistogram(const char* hname, const char* htitle, TH1* orig, Bool_t overwrite)
{
  // Look for histogram in fHistograms; if it does not find it, creates one by cloning orig and add it to fHistograms.

  TH1* hist = static_cast<TH1*>(fHistograms->FindObject(hname));
  if (overwrite && hist) {
    fHistograms->Remove(hist);
    delete hist;
    hist = 0;
  }
  if (!hist && orig) {
    if (fDebugLevel > 2) Printf("Creating histogram %s...", hname);
    hist = static_cast<TH1*>(orig->Clone(hname));
    hist->SetTitle(htitle);
    fHistograms->Add(hist);
  }

  return hist;
}

//______________________________________________________
TCanvas* GEMBeamTestPlotter::GetCanvas(const char* cname, const char* ctitle, Int_t w, Int_t h, Bool_t overwrite)
{
  // Look for canvas in fCanvases; if it does not find it, creates one and add it to fCanvases.

  TCanvas* canv = static_cast<TCanvas*>(fCanvases->FindObject(cname));
  if (overwrite && canv) {
    fCanvases->Remove(canv);
    delete canv;
    canv = 0;
    if (fDebugLevel > 2) Printf("Deleting canvas %s...", cname);
  }
  if (!canv) {
    canv = new TCanvas(cname, ctitle, w, h);
    fCanvases->Add(canv);
    if (fDebugLevel > 2) Printf("Creating canvas %s...", cname);
  }
  canv->cd();
  return canv;
}

//______________________________________________________
void GEMBeamTestPlotter::SavePlot(TCanvas *c)
{
  // Save a plot from a canvas.
  
  TString dirName(Form("%s/Plots", fOutputPath.Data()));
  if (gSystem->AccessPathName(dirName)) gSystem->mkdir(dirName,kTRUE);
  TString fname(Form("%s/%s.%s", dirName.Data(), c->GetName(), fPlotFormat.Data()));
  c->Update();
  c->SaveAs(fname);
}

//______________________________________________________
TTree* GEMBeamTestPlotter::GenerateChain(const char* dataPath, const char* fileName, Int_t run)
{
  // If run == 0, generate a TChain using all files contained in dataPath that follows the pattern specified in fileName;
  // if run != 0, generate return the TTree contained in the data file of one run.

  if (run == 0) {
    TSystemDirectory dir(".", dataPath);
    TList *files = dir.GetListOfFiles();

    TString strRegexp(fileName);
    strRegexp.ReplaceAll("%d","*");
    TRegexp regexp(strRegexp,kTRUE);

    TChain *chain = new TChain("GEM");
  
    if (files) {
      TSystemFile *file = 0;
      TString fname;
      TIter next(files);
      while ((file=(TSystemFile*)next())) {
        fname = file->GetName();
        if (!file->IsDirectory() && fname.Index(regexp) != kNPOS) {
          TString fullName(Form("%s/%s",dataPath,fname.Data()));
          chain->AddFile(fullName);
        }
      }
    }

    delete files;
    files = 0;

    return chain;
  }
  else {
    TString fname(Form(fileName, run));
    TString fullName(Form("%s/%s",dataPath,fname.Data()));
    TFile* inputFile = TFile::Open(fullName);
    if (!inputFile || inputFile->IsZombie()) return 0;
    TTree* tree = dynamic_cast<TTree*>(inputFile->Get("GEM"));
    return tree;
  }
}

//______________________________________________________
Bool_t GEMBeamTestPlotter::ExecTrackQA()
{
  // Track QA analysis.

  ExecTrackQA(kFALSE);
  ExecTrackQA(kTRUE);
}

//______________________________________________________
Bool_t GEMBeamTestPlotter::ExecTrackQA(Bool_t combTracks, const GEMTrack* track, TString partTypeStr, TString suffix)
{
  // Track QA analysis.

  TString hname;
  TString htitle;
  
  Int_t roc = track->GetROC();
  Int_t nclusters = track->GetNumberOfClusters();
  Double_t avgPad = track->GetAveragePad();
  Double_t dEdxTotTruncMean = track->GetTruncatedMean(0.0, configurationParameters::fTruncatedMeanFraction, 1);

  hname = Form("Run%d_ROC%d_%s%s_nClusInTrack", fRun, roc, partTypeStr.Data(), suffix.Data());
  htitle = Form("Run %d, ROC %d (%s) %s, Number of clusters in a track;Number of clusters;counts", fRun, roc, partTypeStr.Data(), suffix.Data());    
  TH1* nClusInTrack = GetHistogram(hname, htitle, kNROW, 0, kNROW);

  hname = Form("Run%d_ROC%d_%s%s_TrackMeanVsnClus", fRun, roc, partTypeStr.Data(), suffix.Data());
  htitle = Form("Run %d, ROC %d (%s) %s, Track mean vs number of clusters;Number of clusters;mean (Q_{tot});counts", fRun, roc, partTypeStr.Data(), suffix.Data());    
  TH2* trackMeanVsnClus = GetHistogram(hname, htitle, kNROW, 0, kNROW, 200, 0, 400);

  hname = Form("Run%d_ROC%d_%s%s_TrackOffset", fRun, roc, partTypeStr.Data(), suffix.Data());
  htitle = Form("Run %d, ROC %d (%s) %s, Track offset;Offset;counts", fRun, roc, partTypeStr.Data(), suffix.Data());    
  TH1* trackOffset = GetHistogram(hname, htitle, kNPAD, 0, kNPAD);

  hname = Form("Run%d_ROC%d_%s%s_TrackMeanVsOffset", fRun, roc, partTypeStr.Data(), suffix.Data());
  htitle = Form("Run %d, ROC %d (%s) %s, Track mean vs offset;Offset;mean (Q_{tot});counts", fRun, roc, partTypeStr.Data(), suffix.Data());    
  TH2* trackMeanVsOffset = GetHistogram(hname, htitle, kNPAD, 0, kNPAD, 200, 0, 400);

  Double_t minSlope = 0.;
  Double_t maxSlope = 0.;
  if (roc == 255) {
    minSlope = 0.;
    maxSlope = 0.005;
  }
  else {
    minSlope = -0.2;
    maxSlope = 0.2;
  }

  hname = Form("Run%d_ROC%d_%s%s_TrackSlope", fRun, roc, partTypeStr.Data(), suffix.Data());
  htitle = Form("Run %d, ROC %d (%s) %s, Track slope;Slope;counts", fRun, roc, partTypeStr.Data(), suffix.Data());
  TH1* trackSlope = GetHistogram(hname, htitle, 800, minSlope, maxSlope);

  hname = Form("Run%d_ROC%d_%s%s_TrackMeanVsSlope", fRun, roc, partTypeStr.Data(), suffix.Data());
  htitle = Form("Run %d, ROC %d (%s) %s, Track mean vs slope;Slope;mean (Q_{tot});counts", fRun, roc, partTypeStr.Data(), suffix.Data());    
  TH2* trackMeanVsSlope = GetHistogram(hname, htitle, 800, minSlope, maxSlope, 200, 0, 400);

  hname = Form("Run%d_ROC%d_%s%s_TrackChi2", fRun, roc, partTypeStr.Data(), suffix.Data());
  htitle = Form("Run %d, ROC %d (%s) %s, Track #chi^{2};#chi^{2};counts", fRun, roc, partTypeStr.Data(), suffix.Data());    
  TH1* trackChi2 = GetHistogram(hname, htitle, 500, 0, 20);

  hname = Form("Run%d_ROC%d_%s%s_TrackMeanVsChi2", fRun, roc, partTypeStr.Data(), suffix.Data());
  htitle = Form("Run %d, ROC %d (%s) %s, Track mean vs #chi^{2};#chi^{2};mean (Q_{tot});counts", fRun, roc, partTypeStr.Data(), suffix.Data());    
  TH2* trackMeanVsChi2 = GetHistogram(hname, htitle, 500, 0, 20, 200, 0, 400);

  hname = Form("Run%d_ROC%d_%s%s_TrackPad", fRun, roc, partTypeStr.Data(), suffix.Data());
  htitle = Form("Run %d, ROC %d (%s) %s, Track pad;pad;counts", fRun, roc, partTypeStr.Data(), suffix.Data());    
  TH1* trackPad = GetHistogram(hname, htitle, kNPAD, 0, kNPAD);

  hname = Form("Run%d_ROC%d_%s%s_TrackMeanVsPad", fRun, roc, partTypeStr.Data(), suffix.Data());
  htitle = Form("Run %d, ROC %d (%s) %s, Track mean vs pad;pad;mean (Q_{tot});counts", fRun, roc, partTypeStr.Data(), suffix.Data());    
  TH2* trackMeanVsPad = GetHistogram(hname, htitle, kNPAD, 0, kNPAD, 200, 0, 400);

  hname = Form("Run%d_ROC%d_%s%s_TrackOffsetVsPad", fRun, roc, partTypeStr.Data(), suffix.Data());
  htitle = Form("Run %d, ROC %d (%s) %s, Track offset vs pad;pad;Offset);counts", fRun, roc, partTypeStr.Data(), suffix.Data());    
  TH2* trackOffsetVsPad = GetHistogram(hname, htitle, kNPAD, 0, kNPAD, kNPAD, 0, kNPAD);

  hname = Form("Run%d_ROC%d_%s%s_TracknClusVsChi2", fRun, roc, partTypeStr.Data(), suffix.Data());
  htitle = Form("Run %d, ROC %d (%s) %s, Track number of clusters vs #chi^{2};#chi^{2};Number of clusters;counts", fRun, roc, partTypeStr.Data(), suffix.Data());    
  TH2* tracknClusVsChi2 = GetHistogram(hname, htitle, 500, 0, 20, kNROW, 0, kNROW);

  nClusInTrack->Fill(nclusters);
  trackMeanVsnClus->Fill(nclusters, dEdxTotTruncMean);
  trackOffset->Fill(track->GetOffset());
  trackMeanVsOffset->Fill(track->GetOffset(), dEdxTotTruncMean);
  trackSlope->Fill(track->GetSlope());
  trackMeanVsSlope->Fill(track->GetSlope(), dEdxTotTruncMean);
  trackChi2->Fill(track->GetChi2());
  trackMeanVsChi2->Fill(track->GetChi2(), dEdxTotTruncMean);
  trackPad->Fill(avgPad);
  trackMeanVsPad->Fill(avgPad, dEdxTotTruncMean);

  trackOffsetVsPad->Fill(avgPad, track->GetOffset());
  tracknClusVsChi2->Fill(track->GetChi2(), nclusters);

  if (combTracks) {
    const GEMCombinedTrack* ctrack = static_cast<const GEMCombinedTrack*>(track);
    for (Int_t isubtrack = 0; isubtrack < ctrack->GetNumberOfSubTracks(); isubtrack++) {
      const GEMTrack* subtrack = ctrack->GetTrack(isubtrack);
      if (!subtrack) continue;
      ExecTrackQA(kFALSE, subtrack, partTypeStr, "Matched");
    }
  }
  else {
    hname = Form("Run%d_ROC%d_%s%s_TrackMeanVsClusterPos", fRun, roc, partTypeStr.Data(), suffix.Data());
    htitle = Form("Run %d, ROC %d (%s) %s, Track mean vs cluster position;(Pad-29) + Row*8;mean (Q_{tot});counts", fRun, roc, partTypeStr.Data(), suffix.Data());    
    TH2* trackMeanVsClusterPos = GetHistogram(hname, htitle, 256, 0, 256, 200, 0, 400);
      
    hname = Form("Run%d_ROC%d_%s%s_TrackMeanVsClusterRow", fRun, roc, partTypeStr.Data(), suffix.Data());
    htitle = Form("Run %d, ROC %d (%s) %s, Track mean vs cluster row;Row;mean (Q_{tot});counts", fRun, roc, partTypeStr.Data(), suffix.Data());    
    TH2* trackMeanVsClusterRow = GetHistogram(hname, htitle, kNROW, 0, kNROW, 200, 0, 400);

    hname = Form("Run%d_ROC%d_%s%s_TrackChi2VsClusterRow", fRun, roc, partTypeStr.Data(), suffix.Data());
    htitle = Form("Run %d, ROC %d (%s) %s, Track #chi^{2} vs cluster row;Row;#chi^{2};counts", fRun, roc, partTypeStr.Data(), suffix.Data());    
    TH2* trackChi2VsClusterRow = GetHistogram(hname, htitle, kNROW, 0, kNROW, 500, 0, 20);

    hname = Form("Run%d_ROC%d_%s%s_TrackOffsetVsSlope", fRun, roc, partTypeStr.Data(), suffix.Data());
    htitle = Form("Run %d, ROC %d (%s) %s, Track offset vs slope;Slope;Offset);counts", fRun, roc, partTypeStr.Data(), suffix.Data());    
    TH2* trackOffsetVsSlope = GetHistogram(hname, htitle, 800, minSlope, maxSlope, kNPAD, 0, kNPAD);

    hname = Form("Run%d_ROC%d_%s%s_ClusterQtotVsRow", fRun, roc, partTypeStr.Data(), suffix.Data());
    htitle = Form("Run %d, ROC %d (%s) %s, Cluster Q_{tot} vs row ;Row;Q_{tot};counts", fRun, roc, partTypeStr.Data(), suffix.Data());    
    TH2* clusterQtotVsRow = GetHistogram(hname, htitle, kNROW, 0, kNROW, 200, 0, 400);

    hname = Form("Run%d_ROC%d_%s%s_ClusterQmaxVsRow", fRun, roc, partTypeStr.Data(), suffix.Data());
    htitle = Form("Run %d, ROC %d (%s) %s, Cluster Q_{max} vs row ;Row;Q_{max};counts", fRun, roc, partTypeStr.Data(), suffix.Data());    
    TH2* clusterQmaxVsRow = GetHistogram(hname, htitle, kNROW, 0, kNROW, 200, 0, 400);

    hname = Form("Run%d_ROC%d_%s%s_ClusterQtotVsPad", fRun, roc, partTypeStr.Data(), suffix.Data());
    htitle = Form("Run %d, ROC %d (%s) %s, Cluster Q_{tot} vs pad ;Pad;Q_{tot};counts", fRun, roc, partTypeStr.Data(), suffix.Data());    
    TH2* clusterQtotVsPad = GetHistogram(hname, htitle, kNPAD, 0, kNPAD, 200, 0, 400);

    hname = Form("Run%d_ROC%d_%s%s_ClusterQmaxVsPad", fRun, roc, partTypeStr.Data(), suffix.Data());
    htitle = Form("Run %d, ROC %d (%s) %s, Cluster Q_{max} vs pad ;Pad;Q_{max};counts", fRun, roc, partTypeStr.Data(), suffix.Data());    
    TH2* clusterQmaxVsPad = GetHistogram(hname, htitle, kNPAD, 0, kNPAD, 200, 0, 400);

    hname = Form("Run%d_ROC%d_%s%s_TrackChi2VsSlope", fRun, roc, partTypeStr.Data(), suffix.Data());
    htitle = Form("Run %d, ROC %d (%s) %s, Track #chi^{2} vs slope;Slope;#chi^{2};counts", fRun, roc, partTypeStr.Data(), suffix.Data());    
    TH2* trackChi2VsSlope = GetHistogram(hname, htitle, 800, minSlope, maxSlope, 500, 0, 20);

    trackOffsetVsSlope->Fill(track->GetSlope(), track->GetOffset());
    
    for (Int_t icluster = 0; icluster < nclusters; icluster++) {
      const GEMCluster* cluster = track->GetCluster(icluster);
      if (!cluster) continue;

      trackMeanVsClusterPos->Fill((cluster->GetMaxPad()-29)+cluster->GetRow()*8, dEdxTotTruncMean);
      trackMeanVsClusterRow->Fill(cluster->GetRow(), dEdxTotTruncMean);
      trackChi2VsClusterRow->Fill(cluster->GetRow(), track->GetChi2());

      clusterQtotVsRow->Fill(cluster->GetRow(), cluster->GetQTot());
      clusterQmaxVsRow->Fill(cluster->GetRow(), cluster->GetQMax());

      clusterQtotVsPad->Fill(cluster->GetMaxPad(), cluster->GetQTot());
      clusterQmaxVsPad->Fill(cluster->GetMaxPad(), cluster->GetQMax());
    }

    trackChi2VsSlope->Fill(track->GetSlope(), track->GetChi2());
  }

  return kTRUE;
}

//______________________________________________________
Bool_t GEMBeamTestPlotter::ExecTrackQA(Bool_t combTracks)
{
  // Track QA analysis.
  
  TString partTypeStr;

  if (fEvent->GetCherenkovValue() < 40) {
    // Pions
    partTypeStr = "Pion";
  }
  else if (fEvent->GetCherenkovValue() > 50) {
    // Electrons
    partTypeStr = "Electron";
  }
  else {
    return kTRUE;
  }

  Int_t ntracks = 0;
  if (combTracks) {
    ntracks = fEvent->GetNumberOfCombinedTracks();
  }
  else {
    ntracks = fEvent->GetNumberOfTracks();
  }
  
  for (Int_t itrack = 0; itrack < ntracks; itrack++) {
    GEMTrack* track = 0;
    if (combTracks) {
      track = fEvent->GetCombinedTrack(itrack);
    }
    else {
      track = fEvent->GetTrack(itrack);
    }
    
    if (!IsTrackAccepted(track)) continue;
    
    Int_t roc = track->GetROC();
    
    if (roc < kNROC) {
      fNSelTracks[roc]++;
    }
    else {
      fNSelCombTracks++;
    }
    
    ExecTrackQA(combTracks, track, partTypeStr);
  }

  return kTRUE;
}

//______________________________________________________
Bool_t GEMBeamTestPlotter::TerminateTrackQA()
{
  return kTRUE;
}

//______________________________________________________
Bool_t GEMBeamTestPlotter::PlotTrackQA()
{
  PlotTrackQA("");
  PlotTrackQA("Matched");
}

//______________________________________________________
Bool_t GEMBeamTestPlotter::PlotTrackQA(TString suffix)
{
  TString hname;
  TString cname;
  TString partTypeStr[2] = {"Electron", "Pion"};

  for (Int_t ipart = 0; ipart < 2; ipart++) {
    for (Int_t roc = 0; roc < kNROC; roc++) {
      hname = Form("Run%d_ROC%d_%s%s_TrackMeanVsSlope", fRun, roc, partTypeStr[ipart].Data(), suffix.Data());
      TH2* trackMeanVsSlope = static_cast<TH2*>(GetHistogram(hname));
      PlotAndProfile(trackMeanVsSlope);

      hname = Form("Run%d_ROC%d_%s%s_TrackMeanVsChi2", fRun, roc, partTypeStr[ipart].Data(), suffix.Data());
      TH2* trackMeanVsChi2 = static_cast<TH2*>(GetHistogram(hname));
      PlotAndProfile(trackMeanVsChi2);

      hname = Form("Run%d_ROC%d_%s%s_TrackMeanVsnClus", fRun, roc, partTypeStr[ipart].Data(), suffix.Data());
      TH2* trackMeanVsnClus = static_cast<TH2*>(GetHistogram(hname));
      PlotAndProfile(trackMeanVsnClus);

      hname = Form("Run%d_ROC%d_%s%s_TrackMeanVsPad", fRun, roc, partTypeStr[ipart].Data(), suffix.Data());
      TH2* trackMeanVsPad = static_cast<TH2*>(GetHistogram(hname));
      PlotAndProfile(trackMeanVsPad);
      
      hname = Form("Run%d_ROC%d_%s%s_TrackMeanVsClusterRow", fRun, roc, partTypeStr[ipart].Data(), suffix.Data());
      TH2* trackMeanVsClusterRow = static_cast<TH2*>(GetHistogram(hname));
      PlotAndProfile(trackMeanVsClusterRow);

      hname = Form("Run%d_ROC%d_%s%s_TrackChi2VsClusterRow", fRun, roc, partTypeStr[ipart].Data(), suffix.Data());
      TH2* trackChi2VsClusterRow = static_cast<TH2*>(GetHistogram(hname));
      PlotAndProfile(trackChi2VsClusterRow);

      hname = Form("Run%d_ROC%d_%s%s_TracknClusVsChi2", fRun, roc, partTypeStr[ipart].Data(), suffix.Data());
      TH2* tracknClusVsChi2 = static_cast<TH2*>(GetHistogram(hname));
      PlotAndProfile(tracknClusVsChi2);      

      hname = Form("Run%d_ROC%d_%s%s_TrackOffsetVsSlope", fRun, roc, partTypeStr[ipart].Data(), suffix.Data());
      TH2* trackOffsetVsSlope = static_cast<TH2*>(GetHistogram(hname));
      if (trackOffsetVsSlope) {
        cname = trackOffsetVsSlope->GetName();
        cname += "_canv";
        TCanvas* trackOffsetVsSlope_canvas = GetCanvas(cname, trackOffsetVsSlope->GetTitle());
        trackOffsetVsSlope_canvas->cd();
        trackOffsetVsSlope->Draw("colz");
      }

      hname = Form("Run%d_ROC%d_%s%s_ClusterQtotVsRow", fRun, roc, partTypeStr[ipart].Data(), suffix.Data());
      TH2* clusterQtotVsRow = static_cast<TH2*>(GetHistogram(hname));
      PlotAndProfile(clusterQtotVsRow);

      hname = Form("Run%d_ROC%d_%s%s_ClusterQmaxVsRow", fRun, roc, partTypeStr[ipart].Data(), suffix.Data());
      TH2* clusterQmaxVsRow = static_cast<TH2*>(GetHistogram(hname));
      PlotAndProfile(clusterQmaxVsRow);

      hname = Form("Run%d_ROC%d_%s%s_ClusterQtotVsPad", fRun, roc, partTypeStr[ipart].Data(), suffix.Data());
      TH2* clusterQtotVsPad = static_cast<TH2*>(GetHistogram(hname));
      PlotAndProfile(clusterQtotVsPad);

      hname = Form("Run%d_ROC%d_%s%s_ClusterQmaxVsPad", fRun, roc, partTypeStr[ipart].Data(), suffix.Data());
      TH2* clusterQmaxVsPad = static_cast<TH2*>(GetHistogram(hname));
      PlotAndProfile(clusterQmaxVsPad);

      hname = Form("Run%d_ROC%d_%s%s_TrackChi2VsSlope", fRun, roc, partTypeStr[ipart].Data(), suffix.Data());
      TH2* trackChi2VsSlope = static_cast<TH2*>(GetHistogram(hname));
      PlotAndProfile(trackChi2VsSlope);

      hname = Form("Run%d_ROC%d_%s%s_TrackSlope", fRun, roc, partTypeStr[ipart].Data(), suffix.Data());
      TH1* trackSlope = GetHistogram(hname);
      if (trackSlope) {
        cname = trackSlope->GetName();
        cname += "_canv";
        TCanvas* trackSlope_canvas = GetCanvas(cname, trackSlope->GetTitle());
        trackSlope_canvas->cd();
        trackSlope->Draw();
      }
    }
  }
  
  return kTRUE;
}

//______________________________________________________
TProfile* GEMBeamTestPlotter::PlotAndProfile(TH2* histo)
{
  // Plot+profile of TH2.

  if (!histo) return 0;

  TString cname;
      
  cname = histo->GetName();
  cname += "_canv";

  TCanvas* canvas = GetCanvas(cname, histo->GetTitle());
  canvas->cd();
  histo->Draw("colz");

  TProfile* histo_prof = histo->ProfileX();

  cname = histo_prof->GetName();
  cname += "_canv";
    
  TCanvas* canvas_prof = GetCanvas(cname, histo_prof->GetTitle());
  canvas_prof->cd();
  histo_prof->Draw();

  fHistograms->Add(histo_prof);

  return histo_prof;
}

//______________________________________________________
Bool_t GEMBeamTestPlotter::ExecDEDX()
{
  // Execute dEdx analysis.

  ExecDEDX(kFALSE);
  ExecDEDX(kTRUE);

  return kTRUE;
}

//______________________________________________________
Bool_t GEMBeamTestPlotter::ExecDEDX(Bool_t combTracks)
{
  // Execute dEdx analysis.
  
  TString hname;
  TString htitle;
  TString partTypeStr;
  EPartType partType = kPion;

  Int_t ntracks = 0;

  if (combTracks) {
    ntracks = fEvent->GetNumberOfCombinedTracks();
    if (fDebugLevel > 2) Printf("The number of combined tracks is %d.", ntracks);
  }
  else {
    ntracks = fEvent->GetNumberOfTracks();
  }

  if (fEvent->GetCherenkovValue() < 40) {
    // PIONS
    partTypeStr = "Pion";
    partType = kPion;
  }
  else if (fEvent->GetCherenkovValue() > 50) {
    // Electrons
    partTypeStr = "Electron";
    partType = kElectron;
  }
  else {
    //Printf("Event not identified as either pion or electron!");
    return kTRUE;
  }

  for (Int_t itrack = 0; itrack < ntracks; itrack++) {

    GEMTrack* track = 0;
    if (combTracks) {
      track = fEvent->GetCombinedTrack(itrack);
    }
    else {
      track = fEvent->GetTrack(itrack);
    }
    
    if (!IsTrackAccepted(track)) continue;
    
    Int_t roc = track->GetROC();

    Double_t dEdxMaxTruncMean = 0.;
    Double_t dEdxTotTruncMean = 0.;

    Double_t dEdxMaxFit = 0.;
    Double_t dEdxTotFit = 0.;

    for (Int_t i = 0; i < 5; i++) {
      dEdxMaxTruncMean = track->GetTruncatedMean(0.05*i, configurationParameters::fTruncatedMeanFraction, 0);
      dEdxTotTruncMean = track->GetTruncatedMean(0.05*i, configurationParameters::fTruncatedMeanFraction, 1);

      hname = Form("Run%d_ROC%d_%s_dEdx_%d_%d_Max", fRun, roc, partTypeStr.Data(), 5*i, TMath::FloorNint(configurationParameters::fTruncatedMeanFraction*100));
      htitle = Form("Run = %d, ROC = %d, %s dEdx %.2f-%.2f%% (Max); dEdx Max; counts", fRun, roc, partTypeStr.Data(), 5.*i, configurationParameters::fTruncatedMeanFraction*100);
      TH1* dEdxMax = GetHistogram(hname, htitle, 100, 0, 200);
      dEdxMax->Fill(dEdxMaxTruncMean);
      
      hname = Form("Run%d_ROC%d_%s_dEdx_%d_%d_Tot", fRun, roc, partTypeStr.Data(), 5*i, TMath::FloorNint(configurationParameters::fTruncatedMeanFraction*100));
      htitle = Form("Run = %d, ROC = %d, %s dEdx %.2f-%.2f%% (Tot); dEdx Tot; counts", fRun, roc, partTypeStr.Data(), 5.*i, configurationParameters::fTruncatedMeanFraction*100);
      TH1* dEdxTot = GetHistogram(hname, htitle, 200, 0, 400);
      dEdxTot->Fill(dEdxTotTruncMean);
    }

    for (Int_t i = -3; i <= 3; i++) {
      if (i==0) continue; // already done in the previous for cycle
      
      dEdxMaxTruncMean = track->GetTruncatedMean(0., configurationParameters::fTruncatedMeanFraction + i * 0.05, 0);
      dEdxTotTruncMean = track->GetTruncatedMean(0., configurationParameters::fTruncatedMeanFraction + i * 0.05, 1);

      hname = Form("Run%d_ROC%d_%s_dEdx_%d_%d_Max", fRun, roc, partTypeStr.Data(), 0, TMath::FloorNint(configurationParameters::fTruncatedMeanFraction*100) + i * 5);
      htitle = Form("Run = %d, ROC = %d, %s dEdx %.2f-%.2f%% (Max); dEdx Max; counts", fRun, roc, partTypeStr.Data(), 0., configurationParameters::fTruncatedMeanFraction*100 + i * 5);
      TH1* dEdxMax = GetHistogram(hname, htitle, 100, 0, 200);
      dEdxMax->Fill(dEdxMaxTruncMean);
      
      hname = Form("Run%d_ROC%d_%s_dEdx_%d_%d_Tot", fRun, roc, partTypeStr.Data(), 0, TMath::FloorNint(configurationParameters::fTruncatedMeanFraction*100) + i * 5);
      htitle = Form("Run = %d, ROC = %d, %s dEdx %.2f-%.2f%% (Tot); dEdx Tot; counts", fRun, roc, partTypeStr.Data(), 0., configurationParameters::fTruncatedMeanFraction*100 + i * 5);
      TH1* dEdxTot = GetHistogram(hname, htitle, 200, 0, 400);
      dEdxTot->Fill(dEdxTotTruncMean);
    }

    dEdxMaxFit = track->GetTruncatedMean(0., 1., 10);
    dEdxTotFit = track->GetTruncatedMean(0., 1., 11);

    if (dEdxMaxFit > 0) {
      hname = Form("Run%d_ROC%d_%s_dEdx_MPV_Max", fRun, roc, partTypeStr.Data());
      htitle = Form("Run = %d, ROC = %d, %s dEdx MPV (Max); dEdx Max; counts", fRun, roc, partTypeStr.Data());
      TH1* dEdxMax = GetHistogram(hname, htitle, 100, 0, 200);
      dEdxMax->Fill(dEdxMaxFit);
    }

    if (dEdxTotFit > 0) {
      hname = Form("Run%d_ROC%d_%s_dEdx_MPV_Tot", fRun, roc, partTypeStr.Data());
      htitle = Form("Run = %d, ROC = %d, %s dEdx MPV (Tot); dEdx Tot; counts", fRun, roc, partTypeStr.Data());
      TH1* dEdxTot = GetHistogram(hname, htitle, 200, 0, 400);
      dEdxTot->Fill(dEdxTotFit);
    }
    
    hname = Form("Run%d_ROC%d_%s_Cluster_Max", fRun, roc, partTypeStr.Data());
    htitle = Form("Run = %d, ROC = %d, %s Cluster (Max); Cluster Max; counts", fRun, roc, partTypeStr.Data());
    TH1* clusterMax = GetHistogram(hname, htitle, 200, 0, 400);

    hname = Form("Run%d_ROC%d_%s_Cluster_Tot", fRun, roc, partTypeStr.Data());
    htitle = Form("Run = %d, ROC = %d, %s Cluster (Tot); Cluster Tot; counts", fRun, roc, partTypeStr.Data());
    TH1* clusterTot = GetHistogram(hname, htitle, 400, 0, 800);
    
    Int_t nclusters = track->GetNumberOfClusters();
    for (Int_t icluster = 0; icluster < nclusters; icluster++) {
      const GEMCluster* cluster = track->GetCluster(icluster);
      if (!cluster) continue;

      clusterMax->Fill(cluster->GetQMax());
      clusterTot->Fill(cluster->GetQTot());
    }
  }

  return kTRUE;
}

//______________________________________________________
Bool_t GEMBeamTestPlotter::IsTrackAccepted(const GEMTrack* track)
{
  // Check whether a track is accepted.
  
  if (!track) return kFALSE;
  
  if (track->InheritsFrom("GEMCombinedTrack")) {
    if (track->GetNumberOfClusters() < configurationParameters::fMinClustersCombined) return kFALSE;
    if (track->GetSlope() > configurationParameters::fSlopeCut) return kFALSE;

    const GEMCombinedTrack* ctrack = static_cast<const GEMCombinedTrack*>(track);
    Int_t nsubtracks = ctrack->GetNumberOfSubTracks();
    if (fDebugLevel > 2) Printf("The number of subtracks in the combined track is %d", nsubtracks);
    for (Int_t itrack = 0; itrack < nsubtracks; itrack++) {
      const GEMTrack* subtrack = ctrack->GetTrack(itrack);
      if (!subtrack) continue;
      if (!IsTrackAccepted(subtrack)) {
        if (fDebugLevel > 2) Printf("Combined track rejected because sub-track in ROC %d does not pass the cuts!", subtrack->GetROC());
        return kFALSE;
      }
    }

    return kTRUE;
  }
  else {
    Int_t roc = track->GetROC();
    Int_t nclusters = track->GetNumberOfClusters();

    if (fDebugLevel > 2) Printf("ROC %d: track has %d clusters.", roc, nclusters);
    
    if (roc >=0 && roc < configurationParameters::kNumberOfROC) {
      if (nclusters < configurationParameters::fMinClusters[roc]) {
        if (fDebugLevel > 2) Printf("Track is rejected because of the number of clusters.");
        return kFALSE;
      }

      if (track->GetChi2() > configurationParameters::fMaxChi2[roc]) {
        if (fDebugLevel > 2) Printf("Track is rejected because of the chi2.");
        return kFALSE;
      }

      if (track->GetSlope() < configurationParameters::fMinSlope[roc] || track->GetSlope() > configurationParameters::fMaxSlope[roc]) {
        if (fDebugLevel > 2) Printf("Track is rejected because of the slope.");
        return kFALSE;
      }
    }

    Bool_t skipTime = kFALSE;
    Bool_t skipEdge = kFALSE;
    //----- Time bin & pad edge cut
    Int_t nclEdge = 0;
    for (Int_t icluster = 0; icluster < nclusters; icluster++) {
      const GEMCluster* cluster = track->GetCluster(icluster);
      if (!cluster) continue;
      
      //TODO check if a cut for low time bins is needed
      if (cluster->GetTimeMax() < configurationParameters::fTimeCutMin[roc] || cluster->GetTimeMax() > configurationParameters::fTimeCutMax[roc] ) {
        skipTime = kTRUE;
      }
      if (cluster->GetPad() < configurationParameters::fEdgePadMin || cluster->GetPad() > configurationParameters::fEdgePadMax) {
        nclEdge++;
      }
    }

    skipEdge = ((Double_t)nclEdge / nclusters) > configurationParameters::fMaxEdgeClusterFraction;
    if (skipEdge == kTRUE) {
      //Printf("nclEdge: %d", nclEdge);
    }

    return !skipTime && !skipEdge;
  }
}

//______________________________________________________
Bool_t GEMBeamTestPlotter::TerminateDEDX()
{
  // Terminate dEdx analysis.

  return kTRUE;
}

//______________________________________________________
Bool_t GEMBeamTestPlotter::PlotDEDX()
{
  // Plot results of dEdx analysis.

  gStyle->SetOptStat(0);

  for (Int_t roc = 0; roc < fNumberOfROCs; roc++) {
    if (fDebugLevel > 0) Printf("ROC = %d", roc);

    PlotDEDX(roc);
  }
  
  PlotDEDX(255);

  return kTRUE;
}

//______________________________________________________
Bool_t GEMBeamTestPlotter::PlotDEDX(Int_t roc)
{
  // Plot results of dEdx analysis for a ROC.

  TString hname;
  TString cname;
  TString ctitle;

  Int_t minTruncMean = 0;
  Int_t maxTruncMean = TMath::FloorNint(configurationParameters::fTruncatedMeanFraction*100);

  for (Int_t i = 0; i <= 20; i+=5) {
    hname = Form("Run%d_ROC%d_Electron_dEdx_%d_%d_Max", fRun, roc, minTruncMean+i, maxTruncMean);
    TH1* dEdxMaxEle = GetHistogram(hname);
    if (!dEdxMaxEle) return kFALSE;

    hname = Form("Run%d_ROC%d_Pion_dEdx_%d_%d_Max", fRun, roc, minTruncMean+i, maxTruncMean);
    TH1* dEdxMaxPion = GetHistogram(hname);
    if (!dEdxMaxPion) return kFALSE;

    cname = Form("Run%d_ROC%d_dEdx_%d_%d_Max", fRun, roc, minTruncMean+i, maxTruncMean);
    ctitle = dEdxMaxEle->GetTitle();
    ctitle.ReplaceAll("Electron ", "");
    PlotSeparation(dEdxMaxEle, dEdxMaxPion, cname, ctitle, "gaus");

    hname = Form("Run%d_ROC%d_Electron_dEdx_%d_%d_Tot", fRun, roc, minTruncMean+i, maxTruncMean);
    TH1* dEdxTotEle = GetHistogram(hname);
    if (!dEdxTotEle) return kFALSE;

    hname = Form("Run%d_ROC%d_Pion_dEdx_%d_%d_Tot", fRun, roc, minTruncMean+i, maxTruncMean);
    TH1* dEdxTotPion = GetHistogram(hname);
    if (!dEdxTotPion) return kFALSE;

    cname = Form("Run%d_ROC%d_dEdx_%d_%d_Tot", fRun, roc, minTruncMean+i, maxTruncMean);
    ctitle = dEdxTotEle->GetTitle();
    ctitle.ReplaceAll("Electron ", "");
    PlotSeparation(dEdxTotEle, dEdxTotPion, cname, ctitle, "gaus");
  }
  
  for (Int_t i = -15; i <= 15; i+=5) {
    if (i==0) continue; // already done in previous for cycle
    
    hname = Form("Run%d_ROC%d_Electron_dEdx_%d_%d_Max", fRun, roc, minTruncMean, maxTruncMean+i);
    TH1* dEdxMaxEle = GetHistogram(hname);
    if (!dEdxMaxEle) return kFALSE;

    hname = Form("Run%d_ROC%d_Pion_dEdx_%d_%d_Max", fRun, roc, minTruncMean, maxTruncMean+i);
    TH1* dEdxMaxPion = GetHistogram(hname);
    if (!dEdxMaxPion) return kFALSE;

    cname = Form("Run%d_ROC%d_dEdx_%d_%d_Max", fRun, roc, minTruncMean, maxTruncMean+i);
    ctitle = dEdxMaxEle->GetTitle();
    ctitle.ReplaceAll("Electron ", "");
    PlotSeparation(dEdxMaxEle, dEdxMaxPion, cname, ctitle, "gaus");

    hname = Form("Run%d_ROC%d_Electron_dEdx_%d_%d_Tot", fRun, roc, minTruncMean, maxTruncMean+i);
    TH1* dEdxTotEle = GetHistogram(hname);
    if (!dEdxTotEle) return kFALSE;

    hname = Form("Run%d_ROC%d_Pion_dEdx_%d_%d_Tot", fRun, roc, minTruncMean, maxTruncMean+i);
    TH1* dEdxTotPion = GetHistogram(hname);
    if (!dEdxTotPion) return kFALSE;

    cname = Form("Run%d_ROC%d_dEdx_%d_%d_Tot", fRun, roc, minTruncMean, maxTruncMean+i);
    ctitle = dEdxTotEle->GetTitle();
    ctitle.ReplaceAll("Electron ", "");
    PlotSeparation(dEdxTotEle, dEdxTotPion, cname, ctitle, "gaus");
  }

  hname = Form("Run%d_ROC%d_Electron_dEdx_MPV_Max", fRun, roc);
  TH1* dEdxMaxEle = GetHistogram(hname);
  if (!dEdxMaxEle) return kFALSE;

  hname = Form("Run%d_ROC%d_Pion_dEdx_MPV_Max", fRun, roc);
  TH1* dEdxMaxPion = GetHistogram(hname);
  if (!dEdxMaxPion) return kFALSE;

  cname = Form("Run%d_ROC%d_dEdx_MPV_Max", fRun, roc);
  ctitle = dEdxMaxEle->GetTitle();
  ctitle.ReplaceAll("Electron ", "");
  PlotSeparation(dEdxMaxEle, dEdxMaxPion, cname, ctitle, "gaus");

  hname = Form("Run%d_ROC%d_Electron_dEdx_MPV_Tot", fRun, roc);
  TH1* dEdxTotEle = GetHistogram(hname);
  if (!dEdxTotEle) return kFALSE;

  hname = Form("Run%d_ROC%d_Pion_dEdx_MPV_Tot", fRun, roc);
  TH1* dEdxTotPion = GetHistogram(hname);
  if (!dEdxTotPion) return kFALSE;

  cname = Form("Run%d_ROC%d_dEdx_MPV_Tot", fRun, roc);
  ctitle = dEdxTotEle->GetTitle();
  ctitle.ReplaceAll("Electron ", "");
  PlotSeparation(dEdxTotEle, dEdxTotPion, cname, ctitle, "gaus");
  
  hname = Form("Run%d_ROC%d_Electron_Cluster_Max", fRun, roc);
  TH1* clusterMaxEle = GetHistogram(hname);
  if (!clusterMaxEle) return kFALSE;

  hname = Form("Run%d_ROC%d_Pion_Cluster_Max", fRun, roc);
  TH1* clusterMaxPion = GetHistogram(hname);
  if (!clusterMaxPion) return kFALSE;

  cname = Form("Run%d_ROC%d_Cluster_Max", fRun, roc);
  ctitle = Form("Run %d, ROC %d, Cluster Max", fRun, roc);
  PlotSeparation(clusterMaxEle, clusterMaxPion, cname, ctitle, "landau");

  hname = Form("Run%d_ROC%d_Electron_Cluster_Tot", fRun, roc);
  TH1* clusterTotEle = GetHistogram(hname);
  if (!clusterTotEle) return kFALSE;

  hname = Form("Run%d_ROC%d_Pion_Cluster_Tot", fRun, roc);
  TH1* clusterTotPion = GetHistogram(hname);
  if (!clusterTotPion) return kFALSE;

  cname = Form("Run%d_ROC%d_Cluster_Tot", fRun, roc);
  ctitle = Form("Run %d, ROC %d, Cluster Tot", fRun, roc);
  PlotSeparation(clusterTotEle, clusterTotPion, cname, ctitle, "landau");

  return kTRUE;
}

//______________________________________________________
void GEMBeamTestPlotter::GetBinMinMax(const TH1 *hist, const Float_t frac, Int_t &bin1, Int_t &bin2)
{
  // Caluclate the limits that correspond to bins with a fraction of the signal collected at the MPV.
  
  const Int_t binMax = hist->GetMaximumBin();
  const Double_t goal = hist->GetBinContent(binMax) * frac;
  
  bin1 = binMax;
  bin2 = binMax;
  
  while (hist->GetBinContent(bin1) > goal && bin1 > 0) bin1--;
  while (hist->GetBinContent(bin2) > goal && bin2 < hist->GetNbinsX()) bin2++;

  bin1++;
  bin2--;
}

//______________________________________________________
Bool_t GEMBeamTestPlotter::PlotSeparation(TH1* histo1, TH1* histo2, const char* cname, const char* ctitle, const char* fitFormula)
{
  // Plot separation for histograms histo1 and histo2.
  
  TCanvas* dEdxMaxCanv = GetCanvas(cname, ctitle);
  dEdxMaxCanv->cd();

  histo1->Sumw2();
  histo2->Sumw2();

  Double_t max = TMath::Max(histo1->GetMaximum(), histo2->GetMaximum()) * 1.2;
  
  TH1* myBlankHisto = new TH1F(cname, ctitle, histo1->GetNbinsX(), histo1->GetXaxis()->GetXmin(), histo1->GetXaxis()->GetXmax());
  myBlankHisto->GetYaxis()->SetRangeUser(0, max);
  myBlankHisto->Draw();

  TString fname1(Form("%s_fit", histo1->GetName()));
  TString fname2(Form("%s_fit", histo2->GetName()));
  
  TF1* funct1 = new TF1(fname1, fitFormula, histo1->GetXaxis()->GetXmin(), histo1->GetXaxis()->GetXmax());
  TF1* funct2 = new TF1(fname2, fitFormula, histo2->GetXaxis()->GetXmin(), histo2->GetXaxis()->GetXmax());

  funct1->SetLineColor(kRed+1);
  funct1->SetLineWidth(2);
  histo1->SetLineColor(kRed+1);
  histo1->SetMarkerColor(kRed+1);
  histo1->SetMarkerStyle(kOpenCircle);
  histo1->SetMarkerSize(0.8);

  funct2->SetLineColor(kBlue+1);
  funct2->SetLineWidth(2);
  histo2->SetLineColor(kBlue+1);
  histo2->SetMarkerColor(kBlue+1);
  histo2->SetMarkerStyle(kOpenSquare);
  histo2->SetMarkerSize(0.8);
  
  histo1->Draw("same");
  histo2->Draw("same");

  Int_t minBin = 0;
  Int_t maxBin = 0;

  GetBinMinMax(histo1, 0.2, minBin, maxBin);
  histo1->Fit(funct1, "N", "", histo1->GetXaxis()->GetBinLowEdge(minBin), histo1->GetXaxis()->GetBinUpEdge(maxBin));
  histo1->GetListOfFunctions()->Add(funct1);
  
  GetBinMinMax(histo2, 0.2, minBin, maxBin);
  histo2->Fit(funct2, "N", "", histo2->GetXaxis()->GetBinLowEdge(minBin), histo2->GetXaxis()->GetBinUpEdge(maxBin));
  histo2->GetListOfFunctions()->Add(funct2);

  TString fitStr1(Form("#mu = %.2f #pm %.2f, #sigma = %.2f #pm %.2f (%.2f%%)",
                       funct1->GetParameter(1), funct1->GetParError(1), funct1->GetParameter(2), funct1->GetParError(2),
                       funct1->GetParameter(2) / funct1->GetParameter(1) * 100));

  TString fitStr2(Form("#mu = %.2f #pm %.2f, #sigma = %.2f #pm %.2f (%.2f%%)",
                       funct2->GetParameter(1), funct2->GetParError(1), funct2->GetParameter(2), funct2->GetParError(2),
                       funct2->GetParameter(2) / funct2->GetParameter(1) * 100));

  TString sepStr(Form("Separation = %.2f #sigma", TMath::Abs(funct1->GetParameter(1) - funct2->GetParameter(1)) / ((funct1->GetParameter(2) + funct2->GetParameter(2)) / 2)));

  TLegend* leg = new TLegend(0.37, 0.6, 0.91, 0.88);
  leg->SetBorderSize(0);
  leg->SetFillStyle(0);
  leg->AddEntry(histo1, "Electrons", "pe");
  leg->AddEntry((TObject*)0, fitStr1, "");
  leg->AddEntry(histo2, "Pions", "pe");
  leg->AddEntry((TObject*)0, fitStr2, "");
  leg->AddEntry((TObject*)0, sepStr, "");
  leg->Draw();
  
  return kTRUE;
}

//______________________________________________________
Bool_t GEMBeamTestPlotter::ExecClusterGainMap()
{
  // Generate the gain map using cluster information.
  
  // static C-arrays for fast access
  static TH2* padOccupancy[kNROC]                    = {0};
  static TH2* padSignalWeigthedOccupancy[kNROC]      = {0};
  static TH1* padSig[kNROC][kNROW][kNPAD]            = {{{0}}};
  static TH2* padSignalRedWeigthedOccupancy[kNROC]   = {0};
  static TH1* padSigRed[kNROC][kNROW][kNPAD]         = {{{0}}};

  if (fRunChanged) {
    for (Int_t roc = 0; roc < kNROC; roc++) {
      padOccupancy[roc] = 0;
      padSignalWeigthedOccupancy[roc] = 0;
      padSignalRedWeigthedOccupancy[roc] = 0;
      for (Int_t row = 0; row < kNROW; row++) {
        for (Int_t pad = 0; pad < kNROW; pad++) {
          padSig[roc][row][pad] = 0;
          padSigRed[roc][row][pad] = 0;
        }
      }
    }
  }

  TString hname;
  TString htitle;
  TString partTypeStr;
  EPartType partType = kPion;

  Int_t ntracks = fEvent->GetNumberOfTracks();

  if (fEvent->GetCherenkovValue() < 40) {
    // Pions
    partTypeStr = "Pion";
    partType = kPion;
  }
  else if (fEvent->GetCherenkovValue() > 50) {
    // Electrons
    partTypeStr = "Electron";
    partType = kElectron;
  }
  else {
    return kTRUE;
  }
  
  for (Int_t itrack = 0; itrack < ntracks; itrack++) {

    GEMTrack* track = fEvent->GetTrack(itrack);
    if (!track) continue;

    if (!IsTrackAccepted(track)) continue;

    Double_t dEdxTotTruncMean = track->GetTruncatedMean(0.0, configurationParameters::fTruncatedMeanFraction, 1);
    if (dEdxTotTruncMean < 1e-12) continue;  // to avoid division by 0
    
    Int_t roc = track->GetROC();

    if (fEvent->GetNumberOfTracks(roc) != 1) continue;

    if (!padOccupancy[roc]) {
      hname = Form("Run%d_ROC%d_PadOccupancyClusterBased", fRun, roc);
      htitle = Form("Run %d, ROC %d, Pad occupancy ClusterBased;Pad row;Pad no.;counts", fRun, roc);    
      padOccupancy[roc] = GetHistogram(hname, htitle, fMaxRow, 0, fMaxRow, fMaxPad, 0, fMaxPad);
    }
      
    if (!padSignalWeigthedOccupancy[roc]) {
      hname = Form("Run%d_ROC%d_PadSignalWeigthedOccupancyClusterBased", fRun, roc);
      htitle = Form("Run %d, ROC %d, Weighted Pad occupancy ClusterBased;Pad row;Pad no.;counts", fRun, roc);    
      padSignalWeigthedOccupancy[roc] = GetHistogram(hname, htitle, fMaxRow, 0, fMaxRow, fMaxPad, 0, fMaxPad);
    }

    if (!padSignalRedWeigthedOccupancy[roc]) {
      hname = Form("Run%d_ROC%d_PadSignalRedWeigthedOccupancyClusterBased", fRun, roc);
      htitle = Form("Run %d, ROC %d, Weighted (red) Pad occupancy ClusterBased;Pad row;Pad no.;counts", fRun, roc);    
      padSignalRedWeigthedOccupancy[roc] = GetHistogram(hname, htitle, fMaxRow, 0, fMaxRow, fMaxPad, 0, fMaxPad);
    }

    Int_t nclusters = track->GetNumberOfClusters();

    for (Int_t icluster = 0; icluster < nclusters; icluster++) {
      const GEMCluster* cluster = track->GetCluster(icluster);
      if (!cluster) continue;

      Int_t row = cluster->GetRow();
      Int_t pad = cluster->GetMaxPad();

      if (!padSig[roc][row][pad]) {
        hname = Form("Run%d_ROC%d_Row%d_Pad%d_SignalClusterBased", fRun, roc, row, pad);
        htitle = Form("Run = %d, ROC = %d, Row = %d, Pad = %d ClusterBased Signal; ADC signal; counts", fRun, roc, row, pad);
        padSig[roc][row][pad] = GetHistogram(hname, htitle, 500, 0, 20);
      }

      if (!padSigRed[roc][row][pad]) {
        hname = Form("Run%d_ROC%d_Row%d_Pad%d_SignalRedClusterBased", fRun, roc, row, pad);
        htitle = Form("Run = %d, ROC = %d, Row = %d, Pad = %d ClusterBased Signal (red); ADC signal; counts", fRun, roc, row, pad);
        padSigRed[roc][row][pad] = GetHistogram(hname, htitle, 4096, 0, 4096);
      }

      padSig[roc][row][pad]->Fill(cluster->GetQTot() / dEdxTotTruncMean);
      padSigRed[roc][row][pad]->Fill(cluster->GetQTot());
      padOccupancy[roc]->Fill(row, pad);
      padSignalWeigthedOccupancy[roc]->Fill(row, pad, cluster->GetQTot() / dEdxTotTruncMean);
      padSignalRedWeigthedOccupancy[roc]->Fill(row, pad, cluster->GetQTot());
    }
  }
  
  return kTRUE;
}

//______________________________________________________
Bool_t GEMBeamTestPlotter::TerminateClusterGainMap()
{
  // Terminate the cluster gain map analysis using cluster information.

  return TerminateGainMap("ClusterBased", 2);
}

//______________________________________________________
Bool_t GEMBeamTestPlotter::PlotClusterGainMap()
{
  // Plotting function for the gain map analysis using cluster information.

  return PlotGainMap("ClusterBased");
}

//______________________________________________________
Bool_t GEMBeamTestPlotter::ExecGainMap()
{
  // Generate the gain map.

  // static C-arrays for fast access
  static TH2* padOccupancy[kNROC]                    = {0};
  static TH2* padWeigthedOccupancy[kNROC]            = {0};
  static TH2* padSignalWeigthedOccupancy[kNROC]      = {0};
  static TH1* padSig[kNROC][kNROW][kNPAD]            = {{{0}}};
  static TH1* padTime[kNROC][kNROW][kNPAD]           = {{{0}}};
  static TH2* padSigVsTime[kNROC][kNROW][kNPAD]      = {{{0}}};

  if (fRunChanged) {
    for (Int_t roc = 0; roc < kNROC; roc++) {
      padOccupancy[roc] = 0;
      padWeigthedOccupancy[roc] = 0;
      padSignalWeigthedOccupancy[roc] = 0;
      for (Int_t row = 0; row < kNROW; row++) {
        for (Int_t pad = 0; pad < kNROW; pad++) {
          padSig[roc][row][pad] = 0;
          padTime[roc][row][pad] = 0;
          padSigVsTime[roc][row][pad] = 0;
        }
      }
    }
  }
  
  TString hname;
  TString htitle;

  const Int_t nbunches = fBunches->GetEntriesFast();
  for (Int_t ibunch = 0; ibunch < nbunches; ibunch++) {
    GEMBunch* bunch = static_cast<GEMBunch*>(fBunches->At(ibunch));

    if (bunch->fLength == 0) continue;

    Int_t roc = bunch->fROC;
    Int_t row = bunch->fRow;
    Int_t pad = bunch->fPad;

    if (!padSig[roc][row][pad]) {
      hname = Form("Run%d_ROC%d_Row%d_Pad%d_Signal", fRun, roc, row, pad);
      htitle = Form("Run = %d, ROC = %d, Row = %d, Pad = %d; ADC signal; counts", fRun, roc, row, pad);
      padSig[roc][row][pad] = GetHistogram(hname, htitle, 4096, 0, 4096);
    }

    if (!padTime[roc][row][pad]) {
      hname = Form("Run%d_ROC%d_Row%d_Pad%d_FirstTimeBin", fRun, roc, row, pad);
      htitle = Form("Run = %d, ROC = %d, Row = %d, Pad = %d; First time bin; counts", fRun, roc, row, pad);
      padTime[roc][row][pad] = GetHistogram(hname, htitle, 500, 0, 500);
    }

    if (!padSigVsTime[roc][row][pad]) {
      hname = Form("Run%d_ROC%d_Row%d_Pad%d_SignalVsFirstTimeBin", fRun, roc, row, pad);
      htitle = Form("Run = %d, ROC = %d, Row = %d, Pad = %d; First time bin; ADC signal; counts", fRun, roc, row, pad);
      padSigVsTime[roc][row][pad] = GetHistogram(hname, htitle, 75, 0, 150, 256, 0, 512);
    }
      
    Int_t sum = 0;
    Int_t sigOverTh = 0;
    Bool_t skip = kTRUE;
      
    for (Int_t isig = 0; isig < bunch->fLength; isig++) {
      //if (bunch->fArrSig[isig] / bunch->GetGainFactor() > fMinADC) skip = kFALSE;
      // For the moment apply the cut to the uncorrected ADC signal, need to think more...
      if (bunch->fArrSig[isig] > fMinADC) skip = kFALSE;
      sum += bunch->fArrSig[isig];
      sigOverTh++;
    }

    if (sigOverTh == 0) continue;

    Float_t calibSum = sum / bunch->GetGainFactor();

    padSigVsTime[roc][row][pad]->Fill(bunch->fFirstTimeBin, calibSum);

    if (bunch->fFirstTimeBin < configurationParameters::fTimeCutMin[roc] || bunch->fFirstTimeBin > configurationParameters::fTimeCutMax[roc]) continue;
    if (skip) continue;
      
    padSig[roc][row][pad]->Fill(calibSum);
    padTime[roc][row][pad]->Fill(bunch->fFirstTimeBin);

    if (!padOccupancy[roc]) {
      hname = Form("Run%d_ROC%d_PadOccupancy", fRun, roc);
      htitle = Form("Run %d, ROC %d, Pad occupancy;Pad row;Pad no.;counts", fRun, roc);    
      padOccupancy[roc] = GetHistogram(hname, htitle, fMaxRow, 0, fMaxRow, fMaxPad, 0, fMaxPad);
    }
      
    if (!padWeigthedOccupancy[roc]) {
      hname = Form("Run%d_ROC%d_PadWeigthedOccupancy", fRun, roc);
      htitle = Form("Run %d, ROC %d, Pad occupancy;Pad row;Pad no.;counts", fRun, roc);    
      padWeigthedOccupancy[roc] = GetHistogram(hname, htitle, fMaxRow, 0, fMaxRow, fMaxPad, 0, fMaxPad);
    }
      
    if (!padSignalWeigthedOccupancy[roc]) {
      hname = Form("Run%d_ROC%d_PadSignalWeigthedOccupancy", fRun, roc);
      htitle = Form("Run %d, ROC %d, Pad occupancy;Pad row;Pad no.;counts", fRun, roc);    
      padSignalWeigthedOccupancy[roc] = GetHistogram(hname, htitle, fMaxRow, 0, fMaxRow, fMaxPad, 0, fMaxPad);
    }
      
    padOccupancy[roc]->Fill(row, pad);
    padWeigthedOccupancy[roc]->Fill(row, pad, sigOverTh);
    padSignalWeigthedOccupancy[roc]->Fill(row, pad, calibSum);

  } // bunch loop

  return kTRUE;
}

//______________________________________________________
Bool_t GEMBeamTestPlotter::TerminateGainMap(const char* suffix, Int_t rescale)
{
  // Terminate the gain map analysis.
  // rescale: 0 = do not rescale, 1 = rescale by the average of the map and by the ROC 1-2 ratio, 2 = rescale by the ROC 1-2 ratio using the *Red* histogram
  
  Double_t  padMeanSigVal[kNROC][kNPAD*kNROW] = {{0}};
  Int_t     nonZeroPads[kNROC]                = {0};
  Double_t  medianSignal[kNROC]               = {0};

  Double_t  padMeanSigValRed[kNROC][kNPAD*kNROW] = {{0}};
  Int_t     nonZeroPadsRed[kNROC]                = {0};
  Double_t  medianSignalRed[kNROC]               = {0};
  
  TString hname;
  TString htitle;

  TPaletteAxis *palette = 0;
  
  for (Int_t roc = 0; roc < fNumberOfROCs; roc++) {
    if (fDebugLevel > 0) Printf("ROC = %d", roc);

    hname = Form("Run%d_ROC%d_PadOccupancy%s", fRun, roc, suffix);
    if (fDebugLevel > 0) Printf("Getting histogram %s...", hname.Data());    
    TH2* padOccupancy = static_cast<TH2*>(GetHistogram(hname));

    if (padOccupancy == 0) {
      if (fDebugLevel > 0) Printf("This ROC has 0 occupancy, skipping!");
      continue;
    }
 
    if (fDebugLevel > 2) Printf("padOccupancy->GetEntries(): %d", (Int_t)padOccupancy->GetEntries());

    hname = Form("Run%d_ROC%d_PadMeanSig%s", fRun, roc, suffix);
    htitle = Form("Run %d, ROC %d, pad mean signal %s;Pad row;Pad no.;mean signal", fRun, roc, suffix);    
    TH2* padMeanSig = GetHistogram(hname, htitle, fMaxRow, 0, fMaxRow, fMaxPad, 0, fMaxPad, kTRUE);
    
    for (Int_t row = 0; row < fMaxRow; row++) {
      for (Int_t pad = 0; pad < fMaxPad; pad++) {

        if (fDebugLevel > 1) Printf("Row = %d, Pad = %d, BinContent = %f", row, pad, padOccupancy->GetBinContent(pad+1,row+1));
        
        hname = Form("Run%d_ROC%d_Row%d_Pad%d_Signal%s", fRun, roc, row, pad, suffix);
        TH1* padSig = GetHistogram(hname);
      
        if (padSig) { // Only process channels that have some signal
          
          Double_t pos = 0;
          padSig->GetQuantiles(1, &pos, &fTruncatedMeanFraction);
          
          if (fDebugLevel > 2) Printf("The truncated mean for row = %d, pad = %d will be calculated up to %.2f", row, pad, pos);
          padSig->GetXaxis()->SetRangeUser(0, pos);
        
          if (fDebugLevel > 2) Printf("Filling padMeanSig with row = %d, pad = %d, padSig[roc][row][pad]->GetMean() = %f", row, pad, padSig->GetMean());
          
          padMeanSig->Fill(row, pad, padSig->GetMean());
          padMeanSigVal[roc][nonZeroPads[roc]] = padSig->GetMean();
          nonZeroPads[roc]++;
        }

        hname = Form("Run%d_ROC%d_Row%d_Pad%d_SignalRed%s", fRun, roc, row, pad, suffix);
        TH1* padSigRed = GetHistogram(hname);
      
        if (padSigRed) { // Only process channels that have some signal
          Double_t pos = 0;
          padSigRed->GetQuantiles(1, &pos, &fTruncatedMeanFraction);
          
          if (fDebugLevel > 2) Printf("The (red) truncated mean for row = %d, pad = %d will be calculated up to %.2f", row, pad, pos);
          padSigRed->GetXaxis()->SetRangeUser(0, pos);
        
          if (fDebugLevel > 2) Printf("The red signal of row = %d, pad = %d is = %f", row, pad, padSigRed->GetMean());
          
          padMeanSigValRed[roc][nonZeroPadsRed[roc]] = padSigRed->GetMean();
          nonZeroPadsRed[roc]++;
        }
      } // pad loop
    } // row loop

    
    hname = Form("Run%d_ROC%d_PadGainMap%s", fRun, roc, suffix);
    htitle = Form("Run %d, ROC %d, pad gain map %s", fRun, roc, suffix);    
    TH2* padGainMap = static_cast<TH2*>(DuplicateHistogram(hname, htitle, padMeanSig, kTRUE));
    padGainMap->GetZaxis()->SetTitle("rel. gain");

    if (fDebugLevel > 2) Printf("padGainMap[roc]->GetEntries(): %d", (Int_t)padGainMap->GetEntries());
    if (fDebugLevel > 2) Printf("padMeanSig[roc]->GetEntries(): %d", (Int_t)padMeanSig->GetEntries());
    
    medianSignal[roc] = TMath::Median(nonZeroPads[roc], padMeanSigVal[roc]);
    if (fDebugLevel > 0) Printf("Median for ROC %d is %.4f", roc, medianSignal[roc]);

    if (rescale == 2) {
      medianSignalRed[roc] = TMath::Median(nonZeroPadsRed[roc], padMeanSigValRed[roc]);
      if (fDebugLevel > 0) Printf("Median (red) for ROC %d is %.4f", roc, medianSignalRed[roc]);
    }
  } // ROC loop

  
  for (Int_t roc = 0; roc < fNumberOfROCs; roc++) {
    Double_t rescalingFactor = 1.;
    
    if (medianSignal[roc] > 1e-12 && rescale > 0) {
      if (rescale == 1) {
        rescalingFactor /= medianSignal[roc];
        if (fRenormalizeROC2 && roc == 1 && medianSignal[2] > 1e-12) {
          rescalingFactor *= medianSignal[roc] / medianSignal[2];
        }
      }
      else if (rescale == 2) {
        if (fRenormalizeROC2 && roc == 1 && medianSignalRed[1] > 1e-12) {
          rescalingFactor *= medianSignalRed[roc] / medianSignalRed[2];
        }
      }

      if (fDebugLevel > 0) Printf("Scaling factor of ROC %d is %.4f", roc, rescalingFactor);
      if (rescalingFactor == 1.) {
        if (fDebugLevel > 0) Printf("No scaling applied!");
      }
      else {
        hname = Form("Run%d_ROC%d_PadGainMap%s", fRun, roc, suffix);
        TH2* padGainMap = static_cast<TH2*>(GetHistogram(hname));
        padGainMap->Scale(rescalingFactor);
        if (fDebugLevel > 0) Printf("Scaling has been applied!");
      }
    }

    hname = Form("Run%d_ROC%d_GainDist%s", fRun, roc, suffix);
    htitle = Form("Run %d, ROC %d, Gain distribution %s; rel. gain; counts", fRun, roc, suffix);    
    TH1* gainDist = GetHistogram(hname, htitle, 125, -0.020, 4.980, kTRUE);  // so that 1 is at the center of its bin
    for (Int_t pad = 0; pad < nonZeroPads[roc]; pad++) gainDist->Fill(padMeanSigVal[roc][pad] * rescalingFactor);
    
  } // ROC loop

  return kTRUE;
}

//______________________________________________________
Bool_t GEMBeamTestPlotter::PlotGainMap(const char* suffix)
{
  // Plotting function for the gain map analysis.

  gStyle->SetOptStat(0);
  
  TString hname;
  TString cname;
  TString ctitle;

  TPaletteAxis *palette = 0;
  
  for (Int_t roc = 0; roc < fNumberOfROCs; roc++) {
    if (fDebugLevel > 0) Printf("ROC = %d", roc);

    hname = Form("Run%d_ROC%d_PadOccupancy%s", fRun, roc, suffix);
    TH2* padOccupancy = static_cast<TH2*>(GetHistogram(hname));
    if (!padOccupancy) continue;

    hname = Form("Run%d_ROC%d_PadMeanSig%s", fRun, roc, suffix);
    TH2* padMeanSig = static_cast<TH2*>(GetHistogram(hname));
    if (!padMeanSig) continue;
    
    hname = Form("Run%d_ROC%d_PadGainMap%s", fRun, roc, suffix);
    TH2* padGainMap = static_cast<TH2*>(GetHistogram(hname));
    if (!padGainMap) continue;
    
    hname = Form("Run%d_ROC%d_GainDist%s", fRun, roc, suffix);
    TH1* gainDist = GetHistogram(hname);
    if (!gainDist) continue;
        
    if (roc > 0) {
      padOccupancy->GetXaxis()->SetRangeUser(0,32);
      padOccupancy->GetYaxis()->SetRangeUser(26,40);

      padMeanSig->GetXaxis()->SetRangeUser(0,32);
      padMeanSig->GetYaxis()->SetRangeUser(26,40);
      
      padGainMap->GetXaxis()->SetRangeUser(0,32);
      padGainMap->GetYaxis()->SetRangeUser(26,40);
    }
    
    cname = Form("Run%d_ROC%d_PadOccupancy%sCanv", fRun, roc, suffix);
    ctitle = Form("Run %d, ROC %d, pad occupancy %s", fRun, roc, suffix);
    TCanvas* padOccupancyCanv = GetCanvas(cname, ctitle);

    padOccupancy->Draw("colz");
    gPad->Update();
    palette = static_cast<TPaletteAxis*>(padOccupancy->GetListOfFunctions()->FindObject("palette"));
    if (palette) palette->SetX2NDC(0.93);
    
    cname = Form("Run%d_ROC%d_PadMeanSig%sCanv", fRun, roc, suffix);
    ctitle = Form("Run %d, ROC %d, pad mean signal %s", fRun, roc, suffix);
    TCanvas* padMeanSigCanvs = GetCanvas(cname, ctitle);

    padMeanSig->Draw("colz");
    gPad->Update();
    palette = static_cast<TPaletteAxis*>(padMeanSig->GetListOfFunctions()->FindObject("palette"));
    if (palette) palette->SetX2NDC(0.93);

    cname = Form("Run%d_ROC%d_PadGainMap%sCanv", fRun, roc, suffix);
    ctitle = Form("Run %d, ROC %d, pad gain map %s", fRun, roc, suffix);
    TCanvas* padGainMapCanvs = GetCanvas(cname, ctitle);

    padGainMap->GetZaxis()->SetRangeUser(0,2.0);
    padGainMap->Draw("colz");
    gPad->Update();
    palette = static_cast<TPaletteAxis*>(padGainMap->GetListOfFunctions()->FindObject("palette"));
    if (palette) palette->SetX2NDC(0.93);

    cname = Form("Run%d_ROC%d_GainDist%sCanv", fRun, roc, suffix);
    ctitle = Form("Run %d, ROC %d, Gain distribution %s", fRun, roc, suffix);
    TCanvas* padGainDistCanvs = GetCanvas(cname, ctitle);
    
    gainDist->GetXaxis()->SetRangeUser(0,2.0);
    gainDist->Draw();
    TPaveText *pave = new TPaveText(0.15, 0.85, 0.4, 0.7, "nbNDC");
    pave->SetBorderSize(0);
    pave->SetFillStyle(0);
    pave->SetTextFont(43);
    pave->SetTextSize(17);
    pave->SetTextAlign(12);
    pave->AddText(Form("Entries = %.0f", gainDist->GetEntries()));
    pave->AddText(Form("Mean = %.4f", gainDist->GetMean()));
    pave->AddText(Form("Sigma = %.4f", gainDist->GetRMS()));
    pave->Draw();

    if (fPlotSignalPerPad) {
      for (Int_t row = 0; row < fMaxRow; row++) {
        for (Int_t pad = 0; pad < fMaxPad; pad++) {

          hname = Form("Run%d_ROC%d_Row%d_Pad%d_Signal%s", fRun, roc, row, pad, suffix);
          TH1* padSig = GetHistogram(hname);

          if (padSig) {
            cname = Form("Run%d_ROC%d_Row%d_Pad%d_Signal_%sCanv", fRun, roc, row, pad, suffix);
            ctitle = Form("Run = %d, ROC = %d, Row = %d, Pad = %d %s", fRun, roc, row, pad, suffix);
            TCanvas* arrSigCanvs = GetCanvas(cname, ctitle);
            padSig->Draw();
          }

          hname = Form("Run%d_ROC%d_Row%d_Pad%d_FirstTimeBin%s", fRun, roc, row, pad, suffix);
          TH1* padTime = GetHistogram(hname);
          
          if (padTime) {
            cname = Form("Run%d_ROC%d_Row%d_Pad%d_Time_%sCanv", fRun, roc, row, pad, suffix);
            ctitle = Form("Run = %d, ROC = %d, Row = %d, Pad = %d %s", fRun, roc, row, pad, suffix);
            TCanvas* arrTimeCanvs = GetCanvas(cname, ctitle);
            padTime->Draw();
          }
        } // pad loop
      } // row loop
    }
  } // ROC loop
}
