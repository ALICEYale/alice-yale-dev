#include <TFile.h>
#include <TH2C.h>
#include <TCanvas.h>
#include <TStyle.h>

#include <vector>

#define kNPAD  70
#define kNROW  70

#define kMinPad 29
#define kMaxPad 36

enum { kGoodChannel = 0, kDeadChannel = 1, kWarmChannel = 2, kHotChannel = 3 };

struct BadChannel
{
  Int_t row;
  Int_t pad;
  Char_t val;
  
  BadChannel(Int_t r, Int_t p, Char_t v) : row(r), pad(p), val(v) {}
};

TH2* GenerateMapROC1();
TH2* GenerateMapROC2();
TH2* GenerateMap(std::vector<BadChannel>& bc, const char* name);

void GenerateBadChannelMaps(const char* outFileName = "BadChannelMaps.root")
{ 
  TH2* badChannelsROC1 = GenerateMapROC1();
  TH2* badChannelsROC2 = GenerateMapROC2();

  badChannelsROC1->GetXaxis()->SetRangeUser(0, 33);
  badChannelsROC1->GetYaxis()->SetRangeUser(28, 38);
  
  badChannelsROC2->GetXaxis()->SetRangeUser(0, 33);
  badChannelsROC2->GetYaxis()->SetRangeUser(28, 38);
  
  gStyle->SetOptStat(0);
  
  TCanvas* c1 = new TCanvas("c1", "c1");
  c1->cd();
  badChannelsROC1->DrawCopy("colz");

  TCanvas* c2 = new TCanvas("c2", "c2");
  c2->cd();
  badChannelsROC2->DrawCopy("colz");

  TFile* outFile = TFile::Open(outFileName, "recreate");
  outFile->cd();
  badChannelsROC1->Write();
  badChannelsROC2->Write();
  outFile->Close();
  delete outFile;
  outFile = 0;
}

TH2* GenerateMapROC1()
{
  std::vector<BadChannel> bc;
  bc.push_back(BadChannel(0, -1, kWarmChannel));
  bc.push_back(BadChannel(1, -1, kWarmChannel));
  bc.push_back(BadChannel(2, -1, kWarmChannel));
  bc.push_back(BadChannel(3, -1, kDeadChannel));
  bc.push_back(BadChannel(20, -1, kDeadChannel));
  bc.push_back(BadChannel(21, -1, kHotChannel));
  bc.push_back(BadChannel(29, -1, kWarmChannel));
  
  return GenerateMap(bc, "ROC1_BadChannelMap");
}

TH2* GenerateMapROC2()
{
  std::vector<BadChannel> bc;
  bc.push_back(BadChannel(10, -1, kDeadChannel));
  bc.push_back(BadChannel(28, -1, kWarmChannel));
  bc.push_back(BadChannel(29, -1, kWarmChannel));
  bc.push_back(BadChannel(30, -1, kWarmChannel));
  bc.push_back(BadChannel(31, -1, kWarmChannel));
  
  return GenerateMap(bc, "ROC2_BadChannelMap");
}

TH2* GenerateMap(std::vector<BadChannel>& bc, const char* name)
{
  TH2C* map = new TH2C(name, name, kNROW, 0, kNROW, kNPAD, 0, kNPAD);
  for (UInt_t i = 0; i < bc.size(); i++) {
    if (bc.at(i).pad < 0) {
      for (UInt_t pad = kMinPad; pad <= kMaxPad; pad++) {
        map->SetBinContent(bc.at(i).row+1, pad+1, bc.at(i).val);
      }
    }
    else {
      map->SetBinContent(bc.at(i).row+1, bc.at(i).pad+1, bc.at(i).val);
    }
  }

  return map;
}
