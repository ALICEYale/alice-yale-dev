// IndepTracking.cxx
// Run from code/macros: root RunIndepTracking.C
// Outputs plots to user's TPCupgrade/IndepTracking
//
// Make sure to include ROC0 in data reconstruction, by commenting out
// c.SetSkipROC(0) in macros/makeTree.C


#include "TMath.h"
#include "TH2.h"
#include "TChain.h"
#include "TStyle.h"
#include "TLegend.h"
#include "TCanvas.h"
#include "TFile.h"
#include "TF1.h"
#include "TPaveText.h"
#include "TTree.h"
#include "TH1.h"
#include "TROOT.h"
#include "TSystem.h"

#include "GEMEvent.h"
#include "GEMTrack.h"
#include "GEMCluster.h"

#include "IndepTracking.h"

#include "../Analysis/configurationParameters.h"
#include "../Analysis/GEMBeamTestPlotter.h"

TString outdir;
GEMEvent *fEvent=0x0;
TObjArray *arrHistdEdxTotEle = 0x0;
TObjArray *arrHistdEdxTotPio = 0x0;

//__________________________________________________________________________
void IndepTracking(TTree *t)
{
  // Determine output directory and create it
  outdir = Form(configurationParameters::fAnalysisOutputLocation.Data(),"IndepTracking");
  gSystem->Exec(Form("mkdir -p %s", outdir.Data()));
  printf("Output directory: %s\n", outdir.Data());

  EPartType partType=kEle;

  InitHistogramArrays();
  TObjArray *arrHistdEdxTot=0x0;
  TString ID_matched;
  TString ID_extraneous;

  t->SetBranchAddress("Events",&fEvent);
  gROOT->cd();

  Int_t numMatchedTracks = 0;
  Int_t numIROCTracks = 0;
  Int_t numYaleTracks = 0;
  TH2F* hOccIROC = new TH2F("OccIROC", "IROC Track Occupancy; Row; Pad", 100, 0, 100, 100, 0, 100);

  // Event loop
  for (Int_t iev=0; iev<t->GetEntries(); ++iev){
    if(iev%5000==0) Printf("Processing event %d-----------------------------",iev);
    t->GetEntry(iev);
    Int_t run=fEvent->GetRunNumber();

    const GEMTrack *arrTracks[2] = {0, 0};    // fill IROC track in slot 0, Yale combined track in slot 1 

    // iterate over roc 0 and roc 255
    for(int i=0; i<2; i++){
      Int_t roc = 255*i; 
      
      // select events with single track
      if (fEvent->GetNumberOfTracks(roc)!=1) continue;

      Int_t numberOfTracks = 0;
      if(roc == 255) {
	numberOfTracks = fEvent->GetNumberOfCombinedTracks(); //always = 1
      } else {
	numberOfTracks = fEvent->GetNumberOfTracks(); // may be >1, since ROCs 1,2 may have tracks
      }
      
      // get track
      const GEMTrack* track = 0x0;
      for (Int_t itrack=0; itrack < numberOfTracks; ++itrack){
	if (roc == 255) {
	  track=fEvent->GetCombinedTrack(itrack);
	} else {
	  track=fEvent->GetTrack(itrack);
	}
	if ( track->GetROC() == roc ) break; // if we find a track belonging to current roc, proceed with this track
      }
      if ( track->GetROC() != roc ) continue; // if there is no track belonging to current roc, go to next roc/event
      
      if(roc==0){
	for(Int_t icl=0; icl<track->GetNumberOfClusters(); icl++){
	  const GEMCluster* cluster = track->GetCluster(icl);
	  hOccIROC->Fill(cluster->GetRow(), cluster->GetPad());
	}
      }
      
      //----- PID                                                                                                    
      if (fEvent->GetCherenkovValue()<40){ // PIONS                                                                  
	arrHistdEdxTot=arrHistdEdxTotPio;
	partType=kPio;
      } else if (fEvent->GetCherenkovValue()>50){ // Electrons                                                       
	arrHistdEdxTot=arrHistdEdxTotEle;
	partType=kEle;
      } else {
	continue;
      }

      if(!GEMBeamTestPlotter::IsTrackAccepted(track)) continue;
     
      //----- PID                                                                                                    
      if (fEvent->GetCherenkovValue()<40){ // PIONS                                                                  
	partType=kPio;
	arrHistdEdxTot=arrHistdEdxTotPio;
      } else if (fEvent->GetCherenkovValue()>50){ // Electrons                                                       
	partType=kEle;
	arrHistdEdxTot=arrHistdEdxTotEle;
      } else {
	continue;
      }

      // The track is now accepted
      arrTracks[i] = track;
      if(i==0) numIROCTracks++;
      if(i==1) numYaleTracks++;

      //_______________________________________________________________
      // do matching of tracks if there is a track in both ROC0, ROC255
      if(arrTracks[0] != 0 && arrTracks[1] != 0) {
	numMatchedTracks++;    
	
	// if matched track, plot dE/dx for ROC255      
	const GEMTrack* track = arrTracks[1];
	ID_matched = Form("ROC%02d_Run%d_Matched",roc,fEvent->GetRunNumber());

	TH1F *hdEdxTotMatched = GetdEdxHisto(arrHistdEdxTot, ID_matched.Data(), partType, 1);
	hdEdxTotMatched->Fill(track->GetTruncatedMean(0.0, configurationParameters::fTruncatedMeanFraction,1));
      }
      
      //_______________________________________________________________
      // analyze Yale tracks with no matched IROC track
      if(arrTracks[0] == 0 && arrTracks[1] != 0){

	// if extraneous ROC255 track, plot dE/dx for ROC255      
	const GEMTrack* track = arrTracks[1];
	ID_extraneous = Form("ROC%02d_Run%d_Extraneous",roc,fEvent->GetRunNumber());

	TH1F *hdEdxTotMatched = GetdEdxHisto(arrHistdEdxTot, ID_extraneous.Data(), partType, 1);
	hdEdxTotMatched->Fill(track->GetTruncatedMean(0.0, configurationParameters::fTruncatedMeanFraction,1));
      }    
    }
  }
  
  std::cout<<"number IROC tracks: "<<numIROCTracks<<std::endl;
  std::cout<<"number Yale combined tracks: "<<numYaleTracks<<std::endl;
  std::cout<<"number matched tracks: "<<numMatchedTracks<<std::endl;

  ProcessdEdx(ID_matched);
  ProcessdEdx(ID_extraneous);

  TFile* fout = new TFile(Form("%s/IndepTracking_histos.root",outdir.Data()),"recreate");
  hOccIROC->Write(); 
  arrHistdEdxTotEle       -> Write(0,TObjArray::kSingleKey);
  arrHistdEdxTotPio       -> Write(0,TObjArray::kSingleKey);
  fout->Close();

}

//__________________________________________________________
TH1F* GetdEdxHisto(TObjArray *arr, const char* addName, EPartType part, Int_t type, Bool_t force){
  // part: 0=electron; 1=pion                                                                                              
  // type: 0=Max; 1=Tot                                                                                                     
  
  TString name=Form("dEdx_%s_%s_%s",part==kEle?"Ele":"Pion",(type==0)?"Max":"Tot",addName);
  
  TH1F *dEdxTot = (TH1F*)arr->FindObject(name.Data());
  if (dEdxTot || !force) return dEdxTot;
  
  Int_t nbins=100;
  Double_t max=120.;
  if (type==1) {
    nbins=200;
    max=400.;
  }
  
  dEdxTot = new TH1F(name.Data(),
		     Form("dE/dx %s %s: %s Charge;%s dE/dx %s",
			  part==kEle?"Ele":"Pion",
			  addName,
			  (type==0)?"Max":"Tot",
			  (type==0)?"Max":"Tot",
			  part==kEle?"Ele":"Pio"
			  ),
		     nbins,0,max);
  
  arr->Add(dEdxTot);
  return dEdxTot;
}

//__________________________________________________________________________                                                 
void GetBinMinMax(const TH1 *hist, const Float_t frac, Int_t &bin1, Int_t &bin2)
{
  const Int_t binMax=hist->GetMaximumBin();
  const Double_t contMax=hist->GetBinContent(binMax);
  bin1=binMax;
  bin2=binMax;
  while ( (bin1--)>binMax/3. ) if (hist->GetBinContent(bin1)<frac*contMax) break;
  while ( (bin2++)<binMax*3. ) if (hist->GetBinContent(bin2)<frac*contMax) break;
}

//__________________________________________________________
TPaveText* Fit(TH1 *histEle, TH1 *histPio, Int_t type, Int_t fitType, const TString &ID){
    // fit gaus to histograms and add text to the pave                                                                        

    TPaveText *text=new TPaveText(0.6,.7,.99,.9,"NDC");
    text->SetBorderSize(1);
    text->SetFillColor(10);
    text->SetDrawOption("same");

    TF1 *fEle=new TF1("gEle",(fitType==0)?"gaus":"landau",histEle->GetXaxis()->GetXmin(),histEle->GetXaxis()->GetXmax());
    TF1 *fPio=new TF1("gPio",(fitType==0)?"gaus":"landau",histPio->GetXaxis()->GetXmin(),histPio->GetXaxis()->GetXmax());
    fEle->SetLineColor(histEle->GetLineColor());
    fPio->SetLineColor(histPio->GetLineColor());
    fEle->SetLineWidth(2);
    fPio->SetLineWidth(2);
    const Float_t frac=0.2;
    Int_t bin1=0,bin2=0;

    GetBinMinMax(histEle,frac,bin1,bin2);
    histEle->Fit(fEle,"QN0","",histEle->GetXaxis()->GetBinLowEdge(bin1),histEle->GetXaxis()->GetBinUpEdge(bin2));
    histEle->GetListOfFunctions()->Add(fEle);

    GetBinMinMax(histPio,frac,bin1,bin2);
    histPio->Fit(fPio,"QN0","",histPio->GetXaxis()->GetBinLowEdge(bin1),histPio->GetXaxis()->GetBinUpEdge(bin2));
    histPio->GetListOfFunctions()->Add(fPio);

    Double_t posEle=fEle->GetParameter(1);
    Double_t resEle=fEle->GetParameter(2);
    Double_t posPio=fPio->GetParameter(1);
    Double_t resPio=fPio->GetParameter(2);
    text->AddText(Form("e: %.2f #pm %.2f (%.2f%%)",posEle,resEle, resEle/posEle*100));
    text->AddText(Form("#pi: %.2f #pm %.2f (%.2f%%)",posPio,resPio,resPio/posPio*100));
    text->AddText(Form("Separation: %.2f#sigma", TMath::Abs(posEle-posPio)/( (resEle+resPio)/2.)));

    histEle->SetMaximum(1.2*TMath::Max(histEle->GetBinContent(histEle->GetMaximumBin()),histPio->GetBinContent(histPio->GetMaximumBin())));
    histPio->SetMaximum(1.2*TMath::Max(histEle->GetBinContent(histEle->GetMaximumBin()),histPio->GetBinContent(histPio->GetMaximumBin())));

    return text;
}

//__________________________________________________________
void ProcessdEdx(const TString &ID){

  gStyle->SetOptStat(0);
  TH1F *dEdxTotEle = GetdEdxHisto(arrHistdEdxTotEle, ID.Data(), kEle, 1, kFALSE);
  if (!dEdxTotEle) return;

  TH1F *dEdxTotPio = GetdEdxHisto(arrHistdEdxTotPio, ID.Data(), kPio, 1, kFALSE);
  if (!dEdxTotPio) return;

  dEdxTotEle->SetLineColor(kRed); dEdxTotPio->SetLineColor(kBlue);

  TLegend *leg=new TLegend(0.1,0.78,0.5,.9);
  leg->SetBorderSize(1);
  leg->SetFillColor(10);
  leg->AddEntry(dEdxTotEle,"electrons","l");
  leg->AddEntry(dEdxTotPio,"pions","l");

  TCanvas *cPH = new TCanvas(Form("cPH_%s",ID.Data()),Form("Pulse Height Distributions; run %s",ID.Data()));

  TPaveText *t2=Fit(dEdxTotEle,dEdxTotPio, 1, 0, ID);

  cPH->Clear();
  cPH->cd();
  dEdxTotEle->Draw();
  dEdxTotPio->Draw("same");
  leg->Draw("same");
  t2->Draw("same");

  cPH->SaveAs(Form("%s/dEdx_%s.pdf",outdir.Data(), ID.Data()));
  delete cPH;
}

//__________________________________________________________
void InitHistogramArrays(){
  // initialise the arrays to store the histograms per run                                                               

  arrHistdEdxTotEle       = new TObjArray;
  arrHistdEdxTotPio       = new TObjArray;
  arrHistdEdxTotEle       -> SetName("dEdx_Ele_Tot");
  arrHistdEdxTotPio       -> SetName("dEdx_Pio_Tot");

}
