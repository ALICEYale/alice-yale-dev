#ifndef INDEPTRACKING_H
#define INDEPTRACKING_H

enum EPartType { kEle=0, kPio=1 };

void IndepTracking(TTree* t);

TH1F* GetdEdxHisto(TObjArray *arr, const char* addName, EPartType partType, Int_t type, Bool_t force=kTRUE);
TPaveText* Fit(TH1 *histEle, TH1 *histPio, Int_t type, Int_t fitType, const TString &ID);
void ProcessdEdx(const TString &ID);
void InitHistogramArrays();

#endif 
