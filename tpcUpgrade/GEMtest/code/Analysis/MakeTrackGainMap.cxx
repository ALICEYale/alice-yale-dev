// MakeTrackGainMap.cxx
// Makes track-based gain map (uses the same cuts as FillHistos, e.g. tracking thresholds)
// Run from code/macros: root RunMakeTrackGainMap.C
// Outputs track-based gain map to user's TPCupgrade/TrackGainMaps/

#include "TMath.h"
#include "TH2.h"
#include "TChain.h"
#include "TStyle.h"
#include "TCanvas.h"
#include "TFile.h"
#include "TF1.h"
#include "TTree.h"
#include "TH1.h"
#include "TROOT.h"
#include "TSystem.h"

#include "AliTPCCalROC.h"

#include "GEMEvent.h"
#include "GEMTrack.h"
#include "GEMCluster.h"

#include "MakeTrackGainMap.h"

#include "../Analysis/configurationParameters.h"

TObjArray *arrPadGains = 0x0;
TObjArray *arrGainMaps = 0x0;
TString outdir;
GEMEvent *fEvent=0x0;
const Int_t rocMin = 1;
const Int_t rocMax = 2;
TString ID[rocMax+1];              // pad gain ID
TString nameGainMap[rocMax+1];     // gain map name
Double_t avgChamberGain[rocMax+1]; // average chamber gain (truncated mean)

//__________________________________________________________________________
void MakeTrackGainMap(TTree *t)
{
  // Determine output directory and create it
  outdir = Form(configurationParameters::fAnalysisOutputLocation.Data(),"TrackGainMaps");
  gSystem->Exec(Form("mkdir -p %s", outdir.Data()));
  printf("Output directory: %s\n", outdir.Data());

  TFile* foutGainMaps = new TFile(Form("%s/StandardCalib.root",outdir.Data()),"recreate");      // gain map output
  TFile* foutPadGains = new TFile(Form("%s/padGains.root",outdir.Data()),"recreate");   // pad gain, NEntires, AvgGain output

  arrPadGains = new TObjArray;
  arrPadGains -> SetName("Pad Gains");
  arrGainMaps = new TObjArray;
  arrGainMaps -> SetName("Gain Maps");

  t->SetBranchAddress("Events",&fEvent);
  gROOT->cd();

  // Produce gain map
  computePadGains(t);                 // event loop: histograms gain for each pad (for each roc), untruncated
  for(Int_t roc=rocMin; roc<rocMax+1; roc++){
    computeGainMaps(roc);             // do truncated mean on cluster distribution for each pad, and compute gain map
  }
  scaleGainMaps();                    //scale ROC1 gain map according to truncated mean cluster charge

  // Write and plot histograms
  for(Int_t roc=rocMin; roc<rocMax+1; roc++){
    writeGainMaps(roc, foutGainMaps);       // write and plot gain maps
    writePadGains(roc, foutPadGains);       // write pad gain histograms, as well as entries per pad and avg gain per pad
  }

  foutGainMaps->Close();
  foutPadGains->Close();
}

//__________________________________________________________________________
void computePadGains(TTree* t)
{
  Int_t numTracks[2] = {0};
  // Event loop
  for (Int_t iev=0; iev<t->GetEntries(); ++iev){
    if(iev%5000==0) Printf("Processing event %d",iev);
    t->GetEntry(iev);
    Int_t run=fEvent->GetRunNumber();
    for(Int_t roc=rocMin; roc<rocMax+1; roc++){
      ID[roc]=Form("ROC%02d_Run%d",roc,fEvent->GetRunNumber());

      // Apply cuts
      Int_t numberOfTracks = 0;
      Int_t clusterCutValue = 0;
      numberOfTracks = fEvent->GetNumberOfTracks();
      clusterCutValue = configurationParameters::fMinClusters[roc];
      
      if (fEvent->GetNumberOfTracks(roc)!=1) continue;
      
      const GEMTrack *track = 0x0;
      for (Int_t itrack=0; itrack < numberOfTracks; ++itrack){
	track=fEvent->GetTrack(itrack); 
	if ( track->GetROC() == roc ) break; // if we find a track belonging to current roc, proceed with this track
      }
      if ( track->GetROC() != roc ) continue; // if there is no track belonging to current roc, go to next roc/event
      
      // Cluster number cut
      if ( track->GetNumberOfClusters() < clusterCutValue ) continue;
      
      //----- Time bin cut                                                                                           
      const Int_t nclTrack=track->GetNumberOfClusters();
      Bool_t skip=kFALSE;
      Int_t timeROC[3];
      // For ROC 1,2 loop through clusters of track, and skip if outside time cut                                    
      if(roc == 1 || roc == 2){
	for (Int_t icl=0; icl<nclTrack; ++icl){
	  timeROC[roc] = track->GetCluster(icl)->GetTimeMax();
	  if (timeROC[roc] < configurationParameters::fTimeCutMin[roc] || timeROC[roc] > configurationParameters::fTimeCutMax[roc]) {
	    skip = kTRUE;
	  }
	}
      }
      // For combined track, get subtracks and loop through clusters of each, and set skip if outside time cut       
      if(roc == 255){
	const GEMCombinedTrack* ctrack = static_cast<const GEMCombinedTrack*>(track);
	Int_t nsubtracks = ctrack->GetNumberOfSubTracks();
	for (Int_t itrack = 0; itrack < nsubtracks; itrack++) {
	  const GEMTrack* subtrack = ctrack->GetTrack(itrack);
	  for(Int_t icl=0; icl<subtrack->GetNumberOfClusters(); icl++){
	    timeROC[itrack+1] = subtrack->GetCluster(icl)->GetTimeMax();
	    if (timeROC[itrack+1] < configurationParameters::fTimeCutMin[itrack+1] || timeROC[itrack+1] > configurationParameters::fTimeCutMax[itrack+1]) {
	      skip = kTRUE;
	    }
	  }
	}
      }
      if(skip) continue;

      // PID cut (since we don't analyze unidentified particles)
      if (fEvent->GetCherenkovValue()<40){
	// PIONS                                                                                               
      } else if (fEvent->GetCherenkovValue()>50 ){
	// Electrons                                                                                           
      } else {
	continue;
      }

      // The track is now accepted; get dE/dx for track
      Double_t dEdxTotTruncMean = track->GetTruncatedMean(0.0, configurationParameters::fTruncatedMeanFraction,1);
      if(dEdxTotTruncMean < 1e-12) continue;
      numTracks[roc-1]++;
      GetHistoNumClusPerTrack(ID[roc])->Fill(track->GetNumberOfClusters());

      // loop through clusters in track and histogram relative gain
      for (Int_t icl=0; icl<track->GetNumberOfClusters(); ++icl){
	const GEMCluster *c=track->GetCluster(icl);
	Int_t row=c->GetRow();
	Int_t pad=c->GetMaxPad();

	
	// don't include dead rows in gain map, since they will be excluded
	// rows run [0,31], pads run [29,36]
	if(roc==1 && (row==3 || row==20 || row==21) ) continue;
	if(roc==2 && (row==10 || row==29) ) continue;

	// get histogram for this pad (and roc) and fill rel gain
	GetHistoPadGains(ID[roc],row,pad)->Fill(c->GetQTot()/dEdxTotTruncMean); 
	
	// get non-normalized histogram (for computing relative gain between chambers)
	GetHistoClusterQTot(ID[roc])->Fill(c->GetQTot());
      }
    } 
  }
  std::cout<<"Tracks in ROC 1: "<<numTracks[0]<<std::endl;
  std::cout<<"Tracks in ROC 2: "<<numTracks[1]<<std::endl;
}

//__________________________________________________________________________                                                 
void computeGainMaps(Int_t roc)
{ 
  // for each roc, loop through pads and fill gain map with average relative gain
  nameGainMap[roc] = Form("Run%d_ROC%d_PadGainMap", fEvent->GetRunNumber(), roc);	// gain map name
  for(int i=0;i<8;i++){
    for(int j=0;j<32;j++){
      // truncate the pad gain histograms
      TH1F* hPadGains = GetHistoPadGains(ID[roc],j,i+29);
      if(hPadGains->GetEntries() > 0) {
	truncateHisto(hPadGains);
	
	// don't include dead rows in gain map, since they will be excluded
	//if(roc==1 && (j==3 || j==20 || j==21) ) continue;
	//if(roc==2 && (j==10 || j==29) ) continue;
	// rows run [0,31], pads run [29,36]; In fGainMap implementation in GEMclusterer, rows run [1,32], pads run [30,37]
	GetHistoGainMaps(nameGainMap[roc])->SetBinContent(j+1,i+30,hPadGains->GetMean());
      }
    }
  }

  // truncate clusters for chamber gain calibration
  truncateHisto(GetHistoClusterQTot(ID[roc]));
  avgChamberGain[roc] = GetHistoClusterQTot(ID[roc])->GetMean();
}

//__________________________________________________________________________                                                 
void scaleGainMaps()
{
  Double_t gainFactor = avgChamberGain[2]/avgChamberGain[1];
  std::cout<<"Chamber gains: ROC2/ROC1 = "<<gainFactor<<std::endl;
  std::cout<<"ROC 1: "<<avgChamberGain[1]<<", ROC 2: "<<avgChamberGain[2]<<std::endl;
  
  // scale ROC2 up (it has ~17% larger gain; alternately can scale ROC1 down)
  GetHistoGainMaps(nameGainMap[2].Data())->Scale(gainFactor);
}

//__________________________________________________________________________                                                 
void writeGainMaps(Int_t roc, TFile* f)
{
  f->ReOpen("update");

  TH2F* hGainMap;
  TCanvas *c = new TCanvas();
  hGainMap = GetHistoGainMaps(nameGainMap[roc]);
  
  hGainMap->SetTitle(nameGainMap[roc].Data());
  hGainMap->GetZaxis()->SetRangeUser(0,2);
  hGainMap->GetYaxis()->SetRange(30,37);
  hGainMap->SetStats(0);
  hGainMap->Write();   
  
  c->cd();
  c->Clear();
  hGainMap->Draw("colz");
  c->SaveAs(Form("%s/hGainMap_ROC%d.pdf",outdir.Data(),roc));
  delete c;
}

//__________________________________________________________________________                                                 
void writePadGains(Int_t roc, TFile* f)
{
  f->ReOpen("update");

  TH1F* hPadGain;         // gain distribution for a single pad (1 histo per pad, per roc)
  TH2F* hNEntries;        // number of entries in gain distribution of each pad (1 histo per roc)
  TH1F* hAvgGain;         // avg gain of each pad (1 histo per roc)
  TH1F* hClusterQ;        // distribution of total cluster charge (1 histo per roc)
  TCanvas *c = new TCanvas();

  // occupancy histogram
  TString nameNEntries=Form("NEntries_%s",ID[roc].Data());    
  Int_t xmin = 0; Int_t xmax = 32; Int_t ymin = 0; Int_t ymax = 37;
  hNEntries = new TH2F(nameNEntries.Data(),nameNEntries.Data(),xmax-xmin,xmin,xmax,ymax-ymin,ymin,ymax);
  hNEntries->GetXaxis()->SetTitle("Number of clusters per pad");  
  hNEntries->SetTitle(nameNEntries.Data());
  hNEntries->GetYaxis()->SetRange(30,37);
  
  // avg gain histogram
  Int_t nbinsAvgGain=40;
  Double_t minAvgGain=0;
  Double_t maxAvgGain=2;
  TString nameAvgGain=Form("AvgGain_%s",ID[roc].Data());
  hAvgGain = new TH1F(nameAvgGain.Data(),nameAvgGain.Data(),nbinsAvgGain,minAvgGain,maxAvgGain);
  hAvgGain->GetXaxis()->SetTitle("Average relative gain per pad");

  // cluster Q histogram
  hClusterQ = GetHistoClusterQTot(ID[roc]);
  hClusterQ->GetXaxis()->SetRangeUser(0,3000);
  hClusterQ->Write();

  for(int i=0;i<8;i++){
    for(int j=0;j<32;j++){
      // rows run [0,31], pads run [29,36]
      hPadGain = GetHistoPadGains(ID[roc],j,i+29);
      hPadGain->SetStats(1);
      
      hNEntries->SetBinContent(j+1, i+30, hPadGain->GetEntries());
      if(hPadGain->GetEntries() > 0) hAvgGain->Fill(hPadGain->GetMean());

      hPadGain->GetXaxis()->SetRangeUser(0,20);
      hPadGain->Write();		
    }
  }
  hNEntries->SetStats(0);
  hNEntries->Write();
  hAvgGain->SetStats(1);
  hAvgGain->Write();

  c->cd();
  c->Clear();
  hAvgGain->Draw();
  c->SaveAs(Form("%s/hAvgGain_ROC%d.pdf", outdir.Data(), roc));
  
  c->cd();
  c->Clear();
  hNEntries->Draw("colz");
  c->SaveAs(Form("%s/NEntries_ROC%d.pdf", outdir.Data(), roc));    

  GetHistoNumClusPerTrack(ID[roc])->Write();

  delete c;
}

//__________________________________________________________________________
TH1F* GetHistoPadGains(const TString &id, Int_t row, Int_t pad)
{
  TObjArray *arr=0x0;
  Int_t nbins=500; Double_t min=0; Double_t max=20;
  TString typeName = Form("GainCorrections_Row%d_Pad%d",row,pad);
  TString name=Form("%s_%s",typeName.Data(),id.Data());
  
  arr=arrPadGains;
  TH1F *RelGain = (TH1F*)arr->FindObject(name.Data());
  if (RelGain) return RelGain;
  RelGain = new TH1F(name.Data(),name.Data(),nbins,min,max);
  arr->Add(RelGain);
  return RelGain;
}


//__________________________________________________________________________
TH1F* GetHistoClusterQTot(const TString &id)
{
  TObjArray *arr=0x0; 
  Int_t nbins=500; Double_t min=0; Double_t max=2500;
  TString typeName = Form("ClusterQTot");
  TString name=Form("%s_%s",typeName.Data(),id.Data());
  
  arr=arrPadGains;
  TH1F *RelGain = (TH1F*)arr->FindObject(name.Data());
  if (RelGain) return RelGain;
  RelGain = new TH1F(name.Data(),name.Data(),nbins,min,max);
  arr->Add(RelGain);
  return RelGain;
}


//__________________________________________________________________________
TH1F* GetHistoNumClusPerTrack(const TString &id)
{
  TObjArray *arr=0x0; 
  Int_t nbins=40; Double_t min=0; Double_t max=40;
  TString typeName = Form("ClustersPerTrack");
  TString name=Form("%s_%s",typeName.Data(),id.Data());
  
  arr=arrPadGains;
  TH1F *RelGain = (TH1F*)arr->FindObject(name.Data());
  if (RelGain) return RelGain;
  RelGain = new TH1F(name.Data(),name.Data(),nbins,min,max);
  arr->Add(RelGain);
  return RelGain;
}

//__________________________________________________________________________
TH2F* GetHistoGainMaps(const TString &name)
{
  TObjArray *arr=0x0;
  Int_t xmin = 0; Int_t xmax = 32; Int_t ymin = 0; Int_t ymax = 37;

  arr=arrGainMaps;
  TH2F *RelGain = (TH2F*)arr->FindObject(name.Data());
  if (RelGain) return RelGain;
  RelGain = new TH2F(name.Data(),name.Data(),xmax-xmin,xmin,xmax,ymax-ymin,ymin,ymax);
  arr->Add(RelGain);
  return RelGain;
}

//__________________________________________________________________________                                                 
void truncateHisto(TH1F* h)
{ 
  Double_t xTrunc;
  h->GetQuantiles(1,&xTrunc,&configurationParameters::fTruncatedMeanFraction);
  h->GetXaxis()->SetRangeUser(0,xTrunc);
}
