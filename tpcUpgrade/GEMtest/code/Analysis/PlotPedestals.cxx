// Read a raw pedestal file and plot pedestals and their widths, and write to .root file
// To run: root runPlotPedestals.C from macros directory
// Output can be found in user's scratch/PedestalPlots
//
// For further info on pedestal data, see aliroot/master/src/TPC/TPCbase/AliTPCCalibPedestal.cxx

#include <TString.h>
#include <TH2F.h>
#include <TSystem.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TFile.h>
#include <TObject.h>

#include <AliTPCAltroMapping.h>
#include <AliRawReaderDate.h>
#include <AliGEMRawStreamV3.h>
#include <AliTPCCalibPedestal.h>
#include <AliTPCCalROC.h>
#include <AliLog.h>

#include "configurationParameters.h"

#include "PlotPedestals.h"

void PlotPedestals(Int_t pedRunNumber){

  AliLog::SetClassDebugLevel("AliTPCAltroMapping",-3);
  
  AliTPCAltroMapping *fMapping[6];
  TString path = "../files/mapping_PlanB_new/Patch%d.data";
  for(Int_t i = 0; i < 6; i++) {
    fMapping[i] = new AliTPCAltroMapping(Form(path, i));
  }
  
  // specify path of raw pedestal data
  TString pedPath = configurationParameters::fRawDataLocation;
  pedPath += Form("/pedestal%d.raw", pedRunNumber);

  // create the raw reader
  AliRawReader* rd = new AliRawReaderDate(pedPath);
  AliGEMRawStreamV3* rs = new AliGEMRawStreamV3(rd,(AliAltroMapping**)fMapping);
 
  // create the pedestal calibration class
  AliTPCCalibPedestal ped;
  ped.SetAltroMapping(fMapping);
  ped.SetRangeAdc(0,200);
  
  // process all events
  while (rd->NextEvent()) ped.ProcessEvent((AliTPCRawStreamV3*)rs);
 
  // analyse the processed events
  ped.Analyse();
  
  // Create save location                                                                                                            
  TString outdir = Form(configurationParameters::fAnalysisOutputLocation, "PedestalPlots");

  // Create directory for output                                                                                                     
  gSystem->Exec(Form("mkdir -p %s", outdir.Data()));
  printf("Output directory: %s\n", outdir.Data());

  // Create and configure canvas                                                                                                     
  gStyle->SetOptStat(0);
  TCanvas c("c","c",1300,600);

  // draw and save

  // pedestals: gaussian fit mean (alternatively use GetCalRocMean for trunc mean)
  // noise: gaussian fit sigma (alternatively use GetCalRocRMS for trunc mean)
  // Note: this produces plots for ROCs 0,1,2
  Int_t nSectors = 3;
  AliTPCCalROC* pedArr[nSectors];
  AliTPCCalROC* noiseArr[nSectors];
  TH2F* hArrPeds[nSectors];
  TH2F* hArrNoise[nSectors];
  TFile* f = new TFile(Form("%s/pedestals%d.root",outdir.Data(), pedRunNumber),"RECREATE");
  TString namePed;
  TString nameNoise;
  UInt_t npads;
  for(int i=0; i<nSectors; i++){
    pedArr[i] = ped.GetCalRocPedestal(i);
    noiseArr[i] = ped.GetCalRocSigma(i);
    namePed = Form("ROC%d_Mean_Pedestals_Run%d",i,pedRunNumber);
    nameNoise = Form("ROC%d_Noise_Run%d",i,pedRunNumber);

    if(i==0) {
      hArrPeds[i] = new TH2F(namePed.Data(),namePed.Data(),35,0,35,70,0,70);
      hArrNoise[i] = new TH2F(nameNoise.Data(),nameNoise.Data(),35,0,35,70,0,70);
    } else {
      hArrPeds[i] = new TH2F(namePed.Data(),namePed.Data(),35,0,35,45,0,45);
      hArrNoise[i] = new TH2F(nameNoise.Data(),nameNoise.Data(),35,0,35,45,0,45);
    }

    /*
    if(i==0) {
      hArrPeds[i] = new TH2F(namePed.Data(),namePed.Data(),40,-5,35,40,30,70);
      hArrNoise[i] = new TH2F(nameNoise.Data(),nameNoise.Data(),40,-5,35,40,30,70);
    } else {
      hArrPeds[i] = new TH2F(namePed.Data(),namePed.Data(),40,-5,35,25,20,45);
      hArrNoise[i] = new TH2F(nameNoise.Data(),nameNoise.Data(),40,-5,35,25,20,45);
    }
    */
    for (UInt_t irow=0; irow<pedArr[i]->GetNrows(); irow++){
      npads = (Int_t)pedArr[i]->GetNPads(irow);
      for (UInt_t ipad=0; ipad<npads; ipad++){
	hArrPeds[i]->Fill(irow+0.5,Int_t(ipad)+0.5,pedArr[i]->GetValue(irow,ipad));
	hArrNoise[i]->Fill(irow+0.5,Int_t(ipad)+0.5,noiseArr[i]->GetValue(irow,ipad));
      }
    }
    hArrPeds[i]->SetMaximum(135);
    hArrPeds[i]->SetMinimum(70);
    hArrPeds[i]->GetXaxis()->SetTitle("Row");
    hArrPeds[i]->GetYaxis()->SetTitle("Pad");
    hArrPeds[i]->SetName(namePed.Data());
    hArrPeds[i]->Draw("colz");
    c.SaveAs(Form("%s/%s.pdf", outdir.Data(), namePed.Data()));

    if(i==0) {
      hArrNoise[i]->SetMaximum(20);
    } else {
      hArrNoise[i]->SetMaximum(5);
    }
    hArrNoise[i]->SetMinimum(0);
    hArrNoise[i]->GetXaxis()->SetTitle("Row");
    hArrNoise[i]->GetYaxis()->SetTitle("Pad");
    hArrNoise[i]->SetName(nameNoise.Data());
    hArrNoise[i]->Draw("colz");
    c.SaveAs(Form("%s/%s.pdf", outdir.Data(), nameNoise.Data()));
  }
  f->Write();
  f->Close();
  delete f;
}
