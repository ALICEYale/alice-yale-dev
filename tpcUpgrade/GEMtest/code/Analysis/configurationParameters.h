#ifndef CONFIGURATIONPARAMETERS_H
#define CONFIGURATIONPARAMETERS_H

#include <TObject.h>
#include <TString.h>

class configurationParameters
{
	public:
	configurationParameters() { printf("You really shouldn't be initializing configurationParameters!");}
	//configurationParameters() { kNumberOfROC = 3; } 
	//configurationParameters(const configurationParameters & config);
	~configurationParameters() {}

	// File locations
	static const TString fRawDataLocation;
	//static TString fROCMapLocation;
	//static TString fGainMapLocation;
	static const TString fDigitsLocation;
	static const TString fTracksLocation;
	static const TString fAnalysisOutputLocation;

	// Number of ROCs (ignoring the combined ROC, so only the real ROCs)
	static const Int_t kNumberOfROC = 3;

	// Parameters for GEMclusterer
	static const Double_t fWindowPad;              // Search window in pad direction
	static const Double_t fWindowTime;             // Search window in time direction
	static const Int_t fADCMin;                    // Min adc channel for cluster qmax
	static const Int_t fNAllowedMisses;            // Number of row misses allowed in track
	static const Int_t fNMinClusters;              // Minimum number of clusters in track

	// Parameters for FillHistos/GEMBeamTestPlotter
	static const Int_t fMinClusters[kNumberOfROC]; // Number of minimum clusters
        static const Double_t fMaxChi2[kNumberOfROC];  // Maximum chi2
        static const Double_t fMinSlope[kNumberOfROC]; // Minimum slope
        static const Double_t fMaxSlope[kNumberOfROC]; // Maximum slope
	static const Int_t fMinClustersCombined;       // Number of minimum clusters for combined tracks
	static const Int_t fTimeCutMin[kNumberOfROC];  // Minimum time for time cuts
	static const Int_t fTimeCutMax[kNumberOfROC];  // Maximum time for time cuts
	static const Int_t fEdgePadMin;                // Min pad edge to cut (ie left edge pad number)
	static const Int_t fEdgePadMax;                // Max pad edge to cut (ie right edge pad number)
	static const Double_t fSlopeCut;               // Slope cut for how wide of angle to allow combined tracks to match
	static const Double_t fTruncatedMeanFraction;  // Fraction of truncated mean to keep for dE/dx
        static const Double_t fMaxEdgeClusterFraction; // Maximum fraction of clusters in a track allowed to be close to the edges
};

#endif /* CONFIGURATIONPARAMETERS_H */
