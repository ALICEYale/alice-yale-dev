// makeQA.C
// Run with:
// root code/macros/loadlibs.C
// .x code/Analysis/makeQA.C+
#include "TStyle.h"
#include "TString.h"
#include "TCanvas.h"
#include "TChain.h"
#include "TFile.h"
#include "TH2D.h"
#include "TObjArray.h"
#include "TObjString.h"
#include "TProof.h"
#include "TVectorD.h"

#include "../Analysis/configurationParameters.h"

#include "makeQA.h"

void makeQA()
{
	FillRunArrays();

	// Create save location
	TString outdir = Form(configurationParameters::fAnalysisOutputLocation, "GEMoutput");

	// Create directory for output
	gSystem->Exec(Form("mkdir -p %s", outdir.Data()));
	printf("Output directory: %s\n", outdir.Data());

	//   TProof::Open("");
	//   gProof->Exec("gSystem->Load(\"/hera/alice/wiechula/software/GEMtest/code/libGEMEvent.so\")");

	// create the file chain
	TString s=gSystem->GetFromPipe(Form("ls %s/*.root",fBasedir.Data()));
	TObjArray *arr=s.Tokenize("\n");
	TChain t("GEM");
	for (Int_t ifile=0; ifile<arr->GetEntriesFast(); ++ifile)
	{
		t.AddFile(arr->At(ifile)->GetName());
	}
	//   t.SetProof();

	// Create and configure canvas
	gStyle->SetOptStat(0);
	TCanvas c("c","c",1300,600);
	c.SetLogz();
	c.SetGridx();
	c.SetGridy();

	TObjArray arrHists;

	// Draw hist based on values in the trees
	TH2D *hCherenkovRun = GetSparse2D("hCherenkovRun",";;Cherenkov",100,0,200);
	t.Draw("fCherenkov:fRunNumber>>hCherenkovRun","","colz");
	arrHists.Add(hCherenkovRun);

	TH2D *hToverPRun = GetSparse2D("hToverPRun",";;T/P",100,0.01,0.05);
	t.Draw("fTemperature/fPressure:fRunNumber>>hToverPRun","","colz");
	arrHists.Add(hToverPRun);

	TH1D *hSlopeOffset = new TH1D("hSlopeOffset","hSlopeOffset",50,0,0.004);
	t.Draw("fCombinedTracks.GetSlope()>>hSlopeOffset","fCombinedTracks.GetROC()==255&&fNtracks[1]==1&&fNtracks[2]==1","");
	c.SaveAs(Form("%s/%s.pdf", outdir.Data(), hSlopeOffset->GetName()));

	/*
	TH1D *hNTracksCombined = new TH1D("hNTracksCombined","hNTracksCombined",10,0,20);
	t.Draw("fCombinedTracks.fNtracks>>hNTracksCombined","","");
	c.SaveAs(Form("%s/%s.pdf", outdir.Data(), hNTracksCombined->GetName()));
	*/

	TH2D *hNTrackPlanB1 = GetSparse2D("hNTrackPlanB1",";;#tracks",50,0,50);
	t.Draw("fNtracks[1]:fRunNumber>>hNTrackPlanB1","","colz");
	TH2D *hNTrackPlanB2 = GetSparse2D("hNTrackPlanB2",";;#tracks",50,0,50);
	t.Draw("fNtracks[2]:fRunNumber>>hNTrackPlanB2","","colz");
	TH2D *hNTrackPlanBCombined = GetSparse2D("hNTrackPlanBCombined",";;#tracks",50,0,50);
	t.Draw("fNtracks[1]+fNtracks[2]:fRunNumber>>hNTrackPlanBCombined","","colz");
	arrHists.Add(hNTrackPlanB1);
	arrHists.Add(hNTrackPlanB2);
	arrHists.Add(hNTrackPlanBCombined);

	TH2D *hNClustersPlanB1 = GetSparse2D("hNClustersPlanB1",";;#clusters",100,0,800);
	t.Draw("fNclusters[1]:fRunNumber>>hNClustersPlanB1","","colz");
	TH2D *hNClustersPlanB2 = GetSparse2D("hNClustersPlanB2",";;#clusters",100,0,800);
	t.Draw("fNclusters[2]:fRunNumber>>hNClustersPlanB2","","colz");
	TH2D *hNClustersPlanBCombined = GetSparse2D("hNClustersPlanBCombined",";;#clusters",100,0,800);
	t.Draw("fNclusters[1]+fNclusters[2]:fRunNumber>>hNClustersPlanBCombined","","colz");
	arrHists.Add(hNClustersPlanB1);
	arrHists.Add(hNClustersPlanB2);
	arrHists.Add(hNClustersPlanBCombined);

	TH2D *hNClustersUsedPlanB1 = GetSparse2D("hNClustersUsedPlanB1",";;#clustersUsed",100,0,800);
	t.Draw("fNclustersUsed[1]:fRunNumber>>hNClustersUsedPlanB1","","colz");
	TH2D *hNClustersUsedPlanB2 = GetSparse2D("hNClustersUsedPlanB2",";;#clustersUsed",100,0,800);
	t.Draw("fNclustersUsed[2]:fRunNumber>>hNClustersUsedPlanB2","","colz");
	TH2D *hNClustersUsedPlanBCombined = GetSparse2D("hNClustersUsedPlanBCombined",";;#clustersUsed",100,0,800);
	t.Draw("fNclustersUsed[1]+fNclustersUsed[2]:fRunNumber>>hNClustersUsedPlanBCombined","","colz");
	arrHists.Add(hNClustersUsedPlanB1);
	arrHists.Add(hNClustersUsedPlanB2);
	arrHists.Add(hNClustersUsedPlanBCombined);

	TH2D *hNClustersUsedPlanB1oneTrack = GetSparse2D("hNClustersUsedPlanB1oneTrack","One track events;;#clustersUsed",100,0,800);
	t.Draw("fNclustersUsed[1]:fRunNumber>>hNClustersUsedPlanB1oneTrack","fNtracks[1]==1","colz");
	TH2D *hNClustersUsedPlanB2oneTrack = GetSparse2D("hNClustersUsedPlanB2oneTrack","One track events;;#clustersUsed",100,0,800);
	t.Draw("fNclustersUsed[2]:fRunNumber>>hNClustersUsedPlanB2oneTrack","fNtracks[2]==1","colz");
	TH2D *hNClustersUsedPlanBCombinedoneTrack = GetSparse2D("hNClustersUsedPlanBCombinedoneTrack","One track events;;#clustersUsed",100,0,800);
	t.Draw("fNclustersUsed[1]+fNclustersUsed[2]:fRunNumber>>hNClustersUsedPlanBCombinedoneTrack","fNtracks[1]==1&&fNtracks[2]==1","colz");
	arrHists.Add(hNClustersUsedPlanB1oneTrack);
	arrHists.Add(hNClustersUsedPlanB2oneTrack);
	arrHists.Add(hNClustersUsedPlanBCombinedoneTrack);

	TH2D *hNClustersFracPlanB1 = GetSparse2D("hNClustersFracPlanB1",";;#clustersFrac",50,0,1);
	t.Draw("fNclustersUsed[1]/fNclusters[1]:fRunNumber>>hNClustersFracPlanB1","","colz");
	TH2D *hNClustersFracPlanB2 = GetSparse2D("hNClustersFracPlanB2",";;#clustersFrac",50,0,1);
	t.Draw("fNclusters[2]/fNclustersUsed[2]:fRunNumber>>hNClustersFracPlanB2","","colz");
	TH2D *hNClustersFracPlanBCombined = GetSparse2D("hNClustersFracPlanBCombined",";;#clustersFrac",50,0,1);
	t.Draw("(fNclusters[1]+fNclusters[2])/(fNclustersUsed[1]+fNclustersUsed[2]):fRunNumber>>hNClustersFracPlanBCombined","","colz");
	arrHists.Add(hNClustersFracPlanB1);
	arrHists.Add(hNClustersFracPlanB2);
	arrHists.Add(hNClustersFracPlanBCombined);

	TH2D *hNClustersElePlanB1 = GetSparse2D("hNClustersElePlanB1",";;#clustersEle",63,0,63);
	t.Draw("fTracks.GetNumberOfClusters():fRunNumber>>hNClustersElePlanB1","fTracks.fROC==1&&fCherenkov>40&&fNtracks[1]==1","colz");
	TH2D *hNClustersPioPlanB1 = GetSparse2D("hNClustersPioPlanB1",";;#clustersPio",63,0,63);
	t.Draw("fTracks.GetNumberOfClusters():fRunNumber>>hNClustersPioPlanB1","fTracks.fROC==1&&fCherenkov<40&&fNtracks[1]==1","colz");
	arrHists.Add(hNClustersElePlanB1);
	arrHists.Add(hNClustersPioPlanB1);

	TH2D *hNClustersElePlanB2 = GetSparse2D("hNClustersElePlanB2",";;#clustersEle",63,0,63);
	t.Draw("fTracks.GetNumberOfClusters():fRunNumber>>hNClustersElePlanB2","fTracks.fROC==2&&fCherenkov>40&&fNtracks[2]==1","colz");
	TH2D *hNClustersPioPlanB2 = GetSparse2D("hNClustersPioPlanB2",";;#clustersPio",63,0,63);
	t.Draw("fTracks.GetNumberOfClusters():fRunNumber>>hNClustersPioPlanB2","fTracks.fROC==2&&fCherenkov<40&&fNtracks[2]==1","colz");
	arrHists.Add(hNClustersElePlanB2);
	arrHists.Add(hNClustersPioPlanB2);

	TH2D *hNClustersElePlanBCombined = GetSparse2D("hNClustersElePlanBCombined",";;#clustersEle",63,0,63);
	t.Draw("fCombinedTracks.GetNumberOfClusters():fRunNumber>>hNClustersElePlanBCombined","fCombinedTracks.GetROC()==255&&fCherenkov>40&&fNtracks[1]==1&&fNtracks[2]==1","colz");
	TH2D *hNClustersPioPlanBCombined = GetSparse2D("hNClustersPioPlanBCombined",";;#clustersPio",63,0,63);
	t.Draw("fCombinedTracks.GetNumberOfClusters():fRunNumber>>hNClustersPioPlanBCombined","fCombinedTracks.GetROC()==255&&fCherenkov<40&&fNtracks[1]==1&&fNtracks[2]==1","colz");
	arrHists.Add(hNClustersElePlanBCombined);
	arrHists.Add(hNClustersPioPlanBCombined);

	// Test for timeBins
	/*TH2D *hNClustersFracPlanB1 = GetSparse2D("hNClustersTimeBinsB1",";;#QMax",200,0,200);
	t.Draw("fTracks.GetClusters().GetQMax():fRunNumber>>hNClustersTimeBinsB1","","colz");
	TH2D *hNClustersFracPlanB2 = GetSparse2D("hNClustersTimeBinsB2",";;#QMax",200,0,200);
	t.Draw("fNclusters[2]/fNclustersUsed[2]:fRunNumber>>hNClustersFracPlanB2","","colz");
	TH2D *hNClustersFracPlanBCombined = GetSparse2D("hNClustersTimeBinsBCombined",";;#QMax",200,0,200);
	t.Draw("(fNclusters[1]+fNclusters[2])/(fNclustersUsed[1]+fNclustersUsed[2]):fRunNumber>>hNClustersFracPlanBCombined","","colz");
	arrHists.Add(hNClustersFracPlanB1);
	arrHists.Add(hNClustersFracPlanB2);
	arrHists.Add(hNClustersFracPlanBCombined);*/

	// Save histograms after writing axis
	for (Int_t ihist=0; ihist<arrHists.GetEntriesFast(); ++ihist) {
		TH2D *h = (TH2D*)arrHists.At(ihist);
		RebinSparse2D(h);
		h->Draw("colz");
		c.SaveAs(Form("%s/%s.pdf", outdir.Data(), h->GetName()));
	}

	// Write file and cleanup
	TFile fout(Form("%s/histosQA.root",outdir.Data()),"recreate");
	arrHists.Write();
	fout.Close();

}

// Finds all of the run numbers based on what is available in the directory.
// This list is saved to fVRuns, which is a vector of doubles (but it actually contains ints)
// The last element of fVRuns is incremented one past the last run. I'm not sure of why.
//_________________________________________________________________________________________________
void FillRunArrays()
{
	// fArrRuns is a TObject
	if (fArrRuns) delete fArrRuns;
	{
		fArrRuns=0x0;
	}

	TString sruns=gSystem->GetFromPipe(Form("ls %s/*.root | sed 's|.*data||;s|.root||'",fBasedir.Data()));
	fArrRuns=sruns.Tokenize("\n");

	const Int_t nruns=fArrRuns->GetEntriesFast();

	// fVRuns is a TVectorD
	fVRuns.ResizeTo(nruns+1);
	fVRuns.Zero();

	for (Int_t irun=0; irun<nruns;++irun) {
		fVRuns(irun)=((TObjString*)fArrRuns->At(irun))->String().Atoi();
	}
	fVRuns(nruns)=fVRuns(nruns-1)+1;
}

// Creates a TH2D with bins labeled by the run number
//_________________________________________________________________________________________________
TH2D* GetSparse2D(const char* name, const char* title, Int_t nbinsy, Double_t ymin, Double_t ymax, Int_t addInfo)
{
	//   TH2D *h = new TH2D(name,title,fVRuns.GetNrows()-1,fVRuns(0),fVRuns(fVRuns.GetNrows()-1),nbinsy,ymin,ymax);
	TH2D *h = new TH2D(name,title,fVRuns.GetNrows()-1,fVRuns.GetMatrixArray(),nbinsy,ymin,ymax);
	for (Int_t ibin=0; ibin<h->GetNbinsX(); ++ibin) {
		h->GetXaxis()->SetBinLabel(ibin+1,fArrRuns->At(ibin)->GetName());
	}

	h->SetUniqueID(addInfo);
	return h;
}

// Initializes the axis with fixed bins.
// I'm not entirely sure why this is needed. Perhaps it has to do with how the hists are created?
//_________________________________________________________________________________________________
void RebinSparse2D(TH2D *h)
{
	printf("%p: %s\n",h,h->GetName());
	const Int_t nbinsx=h->GetNbinsX();
	h->GetXaxis()->Set(nbinsx,0.,(Double_t)nbinsx);

	if (h->GetUniqueID()==1) {
		// scale run sliced to integral
	}
}
