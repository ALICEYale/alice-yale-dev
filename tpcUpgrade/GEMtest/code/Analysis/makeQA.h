#ifndef MAKEQA_H
#define MAKEQA_H

// Contains the tracks for planB
TString fBasedir(configurationParameters::fTracksLocation);

TH2D* GetSparse2D(const char* name, const char* title, Int_t nbinsy, Double_t ymin, Double_t ymax, Int_t addInfo=0);
void RebinSparse2D(TH2D *h);
void FillRunArrays();

TObjArray *fArrRuns=0x0;
TVectorD fVRuns;

#endif /* makeQA.h */
