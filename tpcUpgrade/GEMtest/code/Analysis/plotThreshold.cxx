// Future user: I wrote this as quickly as possible, sorry.
//

#include <TTree.h>
#include <TChain.h>
#include <TList.h>
#include <TString.h>
#include <TFile.h>
#include <TCanvas.h>
#include <TSystem.h>
#include <TSystemDirectory.h>
#include <TRegexp.h>
#include <TClonesArray.h>
#include <TH2F.h>

#include "configurationParameters.h"

#include "plotThreshold.h"
#include "../Event/GEMEvent.h"
#include "../Event/GEMBunch.h"

GEMEvent *fEvent=0x0;
TClonesArray * fBunch=0x0;

void plotThreshold(Int_t run, Int_t threshold)
{
	TTree *chain = 0;
	if (run == 0) chain = GenerateFullChain();
	else chain = LoadTree(run);

	if (!chain) {
		Printf("Unable to load chain. Returning...");
		return;
	}

	chain->SetBranchAddress("Events",&fEvent);
	chain->SetBranchAddress("Bunches",&fBunch);

	TH2F* padOccupancy[gNROC] = {0};
	TCanvas* padOccupancyCanvs[gNROC] = {0};

	TString hname = "";
	TString htitle = "";

	GEMBunch * bunch = 0x0;
	UShort_t * fSigArr = 0x0;
	Int_t fSigArrLength = 0;

	// Define hists
	for (Int_t roc = 0; roc < gNROC; roc++)
	{
		Printf("ROC = %d", roc+1);

		hname = Form("ROC%dPadOccupancyThreshold%d", roc+1);
		htitle = Form("Pad occupancy, ROC %d, Threshold=%d;Pad no.;Pad row;counts", roc+1, threshold);    
		padOccupancy[roc] = new TH2F(hname, htitle, gNMAXPADN, 0, gNMAXPADN, gNMAXPADN, 0, gNMAXPADN);
	}

	for (Int_t iev=0; iev < chain->GetEntries(); ++iev)
	{
		chain->GetEntry(iev);

		/*if (fBunch->GetEntriesFast() > 0)
		  {
		  printf("fBunch: %d\n", fBunch->GetEntriesFast());
		  }*/

		for (Int_t roc = 0; roc < gNROC; roc++)
		{
			for (int j = 0; j < fBunch->GetEntriesFast(); j++)
			{
				bunch = (GEMBunch *) fBunch->At(j);
				fSigArr = (UShort_t *) bunch->fArrSig;
				fSigArrLength = (Int_t) bunch->fLength;

				if (bunch->fROC != (roc+1))
				{
					continue;
				}

				// Check that we have something above the threshold
				for (int k = 0; k < fSigArrLength; k++)
				{
					if (fSigArr[k] > threshold)
					{
						//printf("fSigArr[%d] = %d", fSigArrLength-1, fSigArr[fSigArrLength]);
						padOccupancy[roc]->Fill(bunch->fPad,bunch->fRow);
						break;
					}
				}
			}

			//printf("\n");

			/*if (iev > 5)
			{
				break;
			}*/
		}
	}

	//padOccupancy[1]->Draw("colz");
	TString cname = "";
	TString ctitle = "";

	for (Int_t roc = 0; roc < gNROC; roc++)
	{
		cname = Form("ROC%dPadOccupancyCanvThreshold%d", roc+1, threshold);
		ctitle = Form("ROC %d, pad occupancy, threshold=%d", roc+1, threshold);
		padOccupancyCanvs[roc] = new TCanvas(cname, ctitle);

		padOccupancy[roc]->GetXaxis()->SetRangeUser(25,50);
		padOccupancy[roc]->GetYaxis()->SetRangeUser(0,40);
		padOccupancy[roc]->GetZaxis()->SetRangeUser(0, 5000);
		padOccupancy[roc]->Draw("colz");

		if (gSavePlots) SavePlot(padOccupancyCanvs[roc]);
	}

}

//______________________________________________________
void SavePlot(TCanvas *c)
{
	TString fname(Form("%s.%s", c->GetName(), gPlotFormat.Data()));
	c->Update();
	c->SaveAs(fname);
}

//______________________________________________________
TChain* GenerateFullChain()
{
	// Generate a TChain using all files contained in gDataPath that follows the pattern specified in gFileName.

	TSystemDirectory dir(".", gDataPath);
	TList *files = dir.GetListOfFiles();

	TString strRegexp(gFileName);
	strRegexp.ReplaceAll("%d","*");
	TRegexp regexp(strRegexp,kTRUE);

	TChain *chain = new TChain("GEM");

	if (files) {
		TSystemFile *file = 0;
		TString fname;
		TIter next(files);
		while ((file=(TSystemFile*)next())) {
			fname = file->GetName();
			if (!file->IsDirectory() && fname.Index(regexp) != kNPOS) {
				TString fullName(Form("%s/%s",gDataPath.Data(),fname.Data()));
				chain->AddFile(fullName);
			}
		}
	}

	delete files;
	files = 0;

	return chain;
}


//______________________________________________________
TTree* LoadTree(Int_t run)
{
	// Return the TTree contained in the data file of one run.

	TString fname(Form(gFileName, run));
	TString fullName(Form("%s/%s",gDataPath.Data(),fname.Data()));
	TFile *file = TFile::Open(fullName);
	if (!file || file->IsZombie()) return 0;
	TTree *tree = dynamic_cast<TTree*>(file->Get("GEM"));
	return tree;
}
