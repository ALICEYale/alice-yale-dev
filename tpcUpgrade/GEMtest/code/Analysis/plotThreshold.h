#ifndef PLOTTHRESHOLD_H
#define PLOTTHRESHOLD_H

class TChain;
class TString;
class TTree;
class TCanvas;

const TString gDataPath(configurationParameters::fDigitsLocation);
const TString gFileName("data%d.root");
const TString gFileOutName("PadPlots.root");
const Int_t gNROC = 2;
const Int_t gNMAXPADN = 50;
const Bool_t gSaveRootFile = kTRUE;
const Bool_t gSavePlots = kTRUE;
const TString gPlotFormat("pdf");
const Bool_t gPlotSignalPerPad = kFALSE;

void plotThreshold(Int_t run = 0, Int_t threshold = 3);  // if run == 0 use all files in gDataPath
void SavePlot(TCanvas *c);

TChain* GenerateFullChain();
TTree* LoadTree(Int_t run);

#endif /* PLOTTHRESHOLD_H */
