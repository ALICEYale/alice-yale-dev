#ifndef PLOTTIMEBINS_H 
#define PLOTTIMEBINS_H 

class TChain;
class TString;
class TTree;
class TCanvas;

void plotTimeBins(Int_t run = 0);  // if run == 0 use all files in gDataPath
void SavePlot(TCanvas *c);

TChain* GenerateFullChain();
TTree* LoadTree(Int_t run);

#endif /* PLOTTIMEBINS_H */
