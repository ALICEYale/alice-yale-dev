
#include "GEMMapper.h"

#include "GEMBunch.h"

ClassImp(GEMBunch);

GEMBunch::GEMBunch() : TObject(), fROC(0), fRow(0), fPad(0), fFirstTimeBin(0), fLength(0), fArrSig(0x0) {;}

GEMBunch::GEMBunch(UChar_t roc, UChar_t row, UChar_t pad, Int_t length, Int_t *sig, UShort_t startTime)
: TObject()
, fROC(roc)
, fRow(row)
, fPad(pad)
, fFirstTimeBin(startTime)
, fLength(length)
, fArrSig(new Int_t[length])
, fGainFactor(1.)
{
  for (Int_t i=0; i<length; ++i){
    fArrSig[i]=sig[i];
  }
}

//________________________________________________________________________________
GEMBunch::GEMBunch(const GEMBunch &bunch)
: TObject(bunch)
, fROC(bunch.fROC)
, fRow(bunch.fRow)
, fPad(bunch.fPad)
, fFirstTimeBin(bunch.fFirstTimeBin)
, fLength(bunch.fLength)
, fArrSig(new Int_t[bunch.fLength])
, fGainFactor(bunch.fGainFactor)
{
  for (Int_t i=0; i<bunch.fLength; ++i){
    fArrSig[i]=bunch.fArrSig[i];
  }
}

//________________________________________________________________________________
GEMBunch::~GEMBunch()
{
  // dtor
  delete [] fArrSig;
}

//________________________________________________________________________________
void GEMBunch::Clear(Option_t* /*option = ""*/)
{
  // clear signals (needed for clones array clear function)
  delete [] fArrSig;
  fArrSig=0x0;
}

//________________________________________________________________________________

void GEMBunch::GetPadPosition(Double_t pos[3])
{
  //get pad pos
  GEMMapper::GetPadPosition(fRow,fPad,pos);
}

//________________________________________________________________________________
Double_t GEMBunch::GetPadPositionX()
{
  // get x pos of pad
  return GEMMapper::GetPadX(fRow);
}

//________________________________________________________________________________
Double_t GEMBunch::GetPadPositionY()
{
  // get y pos of pad
  return GEMMapper::GetPadY(fRow,fPad);
}

//________________________________________________________________________________
Float_t GEMBunch::GetCalibSum() const
{
  Float_t sum = 0;
  for (Int_t i = 0; i < fLength; i++) sum += fArrSig[i];
  if (fGainFactor>0) sum /= fGainFactor;
  return sum;
}
