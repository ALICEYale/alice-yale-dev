#ifndef GEMBunch_H
#define GEMBunch_H

#include <TVectorF.h>
#include <GEMMapper.h>

#include <TObject.h>

class GEMBunch : public TObject {
public:
  GEMBunch();
  GEMBunch(UChar_t roc, UChar_t row, UChar_t pad, Int_t length, Int_t *sig, UShort_t startTime);
  GEMBunch(const GEMBunch &bunch);
  virtual ~GEMBunch();

  virtual void Clear(Option_t* option = "");

  void SetGainFactor(Float_t g) { fGainFactor = g; }
  Float_t GetGainFactor() const { return fGainFactor; }
  
  void GetPadPosition(Double_t pos[3]);
  Double_t GetPadPositionX();
  Double_t GetPadPositionY();

  Float_t GetCalibSum() const;

  Float_t GetCPad() { return fPad-GEMMapper::GetNPads(fRow)/2; }

  Int_t GetROC() { return static_cast<UChar_t> (fROC); }
  Int_t GetPad() { return static_cast<UChar_t> (fPad); }
  Int_t GetRow() { return static_cast<UChar_t> (fRow); }
  Int_t GetFirstTimeBin() { return static_cast<UChar_t> (fFirstTimeBin); }
  Int_t GetSignalArrayLength() { return fLength; }
  Int_t * GetSignalArray() const { return fArrSig; }

// private:
  UChar_t   fROC;         //sector
  UChar_t   fRow;            //row
  UChar_t   fPad;            //pad
  UShort_t  fFirstTimeBin;   //first time bin
  Int_t     fLength;         //length
  Int_t *fArrSig;         //[fLength] array of signals
  Float_t   fGainFactor;     // gain factor
  
  ClassDef(GEMBunch,4);
};

#endif
