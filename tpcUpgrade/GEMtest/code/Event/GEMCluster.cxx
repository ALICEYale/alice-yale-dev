
#include <TMath.h>

#include "GEMMapper.h"
#include "GEMCluster.h"
#include "GEMTrack.h"

ClassImp(GEMCluster);

GEMCluster::GEMCluster()
: TObject()
, fROC(0)
, fRow(0)
, fPad(0.)
, fTimeBinW(0.)
, fPadRMS(0.)
, fTimeBinWRMS(0.)
, fTimeMax(0)
, fNPads(0)
, fQMax(0.)
, fQTot(0.)

, fCPad(0.)
, fX(0.)
, fY(0.)
, fResY(0.)
, fResYUnBiased(0.)
, fResTime(0.)

, fTrackNr(0)
, fTrack()
{
  // defrault ctor
}

//________________________________________________________________________________
GEMCluster::GEMCluster(UChar_t roc, UChar_t row, Double_t wpad, Double_t wtime, Double_t rmspad, Double_t rmswtime, UShort_t tmax, Double_t qmax, Double_t qtot, UChar_t npads)
: TObject()
, fROC(roc)
, fRow(row)
, fPad(wpad)
, fTimeBinW(wtime)
, fPadRMS(rmspad)
, fTimeBinWRMS(rmswtime)
, fTimeMax(tmax)
, fNPads(npads)
, fQMax(qmax)
, fQTot(qtot)

, fCPad(0.)
, fX(0.)
, fY(0.)
, fResY(0.)
, fResYUnBiased(0.)
, fResTime(0.)

, fTrackNr(0)
, fTrack()
{
  // defrault ctor
  const Int_t npadsRow=GEMMapper::GetNPads(fRow, fROC);
  fCPad=fPad-npadsRow/2;

  fX=GetX();
  fY=GetY();
}

//________________________________________________________________________________
GEMCluster::GEMCluster(const GEMCluster &cluster)
: TObject(cluster)
, fROC(cluster.fROC)
, fRow(cluster.fRow)
, fPad(cluster.fPad)
, fTimeBinW(cluster.fTimeBinW)
, fPadRMS(cluster.fPadRMS)
, fTimeBinWRMS(cluster.fTimeBinWRMS)
, fTimeMax(cluster.fTimeMax)
, fNPads(cluster.fNPads)
, fQMax(cluster.fQMax)
, fQTot(cluster.fQTot)

, fCPad(cluster.fCPad)
, fX(cluster.fX)
, fY(cluster.fY)
, fResY(cluster.fResY)
, fResYUnBiased(cluster.fResYUnBiased)
, fResTime(cluster.fResTime)

, fTrackNr(cluster.fTrackNr)
, fTrack(cluster.fTrack)
{
  // defrault ctor
  
  const Int_t npadsRow=GEMMapper::GetNPads(fRow, fROC);
  fCPad=fPad-npadsRow/2;
  
  fX=GetX();
  fY=GetY();
}

//________________________________________________________________________________
GEMCluster& GEMCluster::operator = (const GEMCluster &cluster)
{
  // assignment operator
  if (&cluster == this) return *this;
  new (this) GEMCluster(cluster);
  
  return *this;
}

//________________________________________________________________________________
Double_t GEMCluster::GetX() const
{
  //
  return GEMMapper::GetPadX(fRow, fROC);
}

//________________________________________________________________________________
Double_t GEMCluster::GetY() const
{
  //
  const Int_t pad=Int_t(TMath::Floor(fPad));
  Double_t y = GEMMapper::GetPadY(fRow,UShort_t(pad),fROC);
  if (GEMMapper::GetUseAlternativeMapping() && fROC > 0) {
    y += (fPad-pad)*GEMMapper::GetPadWidth(fROC);
  }
  else {
    y += (fPad-pad-0.5)*GEMMapper::GetPadWidth(fROC);
  }

  return y;
}

//________________________________________________________________________________
Double_t GEMCluster::GetZ() const
{
  //
  return fTimeBinW;
}

//________________________________________________________________________________
Int_t GEMCluster::GetMaxPad()   const
{
  //
  // pad with maximum charge
  //
  return Int_t(TMath::Floor(fPad));
}

//________________________________________________________________________________
Double_t GEMCluster::GetCPad() const
{
  //
  // get pad number centered around 0 for better printing
  //
  const Int_t npadsRow=GEMMapper::GetNPads(fRow, fROC);
  return fPad-npadsRow/2;
}

//________________________________________________________________________________
Double_t GEMCluster::GetResidualY() const
{
  //
  // calculate residual in y direction
  //
  
  const GEMTrack *track=static_cast<const GEMTrack*>(fTrack.GetObject());
  if (!track) return 0.;
  
  return track->EvalY(GetX()) - GetY();
  
}

//________________________________________________________________________________
Double_t GEMCluster::GetResidualPad() const
{
  //
  // calculate residual in y direction
  //
  
  const GEMTrack *track=static_cast<const GEMTrack*>(fTrack.GetObject());
  if (!track) return 0.;
  
  return track->EvalPad(fRow) - fPad;
  
}


//________________________________________________________________________________
Double_t GEMCluster::GetResidualTime() const
{
  //
  // calculate residual in y direction
  //
  
  const GEMTrack *track=static_cast<const GEMTrack*>(fTrack.GetObject());
  if (!track) return 0.;
  
  return track->EvalTime(GetX()) - fTimeBinW;
  
}

//________________________________________________________________________________
void GEMCluster::SetTrack(GEMTrack *track)
{
  // set track reference
  fTrack = track;
  fResY=GetResidualY();
  fResTime=GetResidualTime();
}
