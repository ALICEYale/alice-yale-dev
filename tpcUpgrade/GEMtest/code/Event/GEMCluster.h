#ifndef GEMCluster_H
#define GEMCluster_H

#include <TRef.h>
#include <TClonesArray.h>
#include <TObject.h>

class GEMTrack;

class GEMCluster : public TObject {
public:
  GEMCluster();
  GEMCluster(UChar_t roc, UChar_t row, Double_t wpad, Double_t wtime, Double_t rmspad, Double_t rmswtime, UShort_t tmax, Double_t qmax, Double_t qtot, UChar_t npads);
  GEMCluster(const GEMCluster &cluster);
  virtual ~GEMCluster() {;}
  
  GEMCluster& operator = (const GEMCluster &cluster);

  void SetTrackNr(UInt_t nr) { fTrackNr=nr; }
  UInt_t GetTrackNr() const { return fTrackNr; }
  
  
  Double_t GetX() const;
  Double_t GetY() const;
  Double_t GetZ() const;

  UChar_t   GetROC()         const { return fROC;         }
  UChar_t   GetRow()         const { return fRow;         }
  Double_t  GetPad()         const { return fPad;         }
  Double_t  GetTimeBinW()    const { return fTimeBinW;    }
  Double_t  GetPadRMS()      const { return fPadRMS;      }
  Double_t  GetTimeBinWRMS() const { return fTimeBinWRMS; }
  UShort_t  GetTimeMax()     const { return fTimeMax;     }
  Double_t  GetCPad()        const;
  Int_t     GetMaxPad()      const;
  UChar_t   GetNPads()       const { return fNPads;       }
  
  Double_t GetQMax()         const { return fQMax;        }
  Double_t GetQTot()         const { return fQTot;        }
  
  void CorrectCharge(Double_t corrFactor) { fQMax/=corrFactor; fQTot/=corrFactor; }
  
  Double_t GetResidualY()    const; 
  Double_t GetResidualPad()  const;
  Double_t GetResidualTime() const;

  Double_t GetResidualYUnBiased() const { return fResYUnBiased;}
  void SetResidualYUnBiased(Double_t value) {fResYUnBiased=value;}
  
  void SetTrack(GEMTrack *track);
  
private:
  UChar_t    fROC;         // ROC
  UChar_t    fRow;         // row
  Double32_t fPad;         // weighted pad position
  Double32_t fTimeBinW;    // weighted time bin
  Double32_t fPadRMS;      // rms of in pad direction
  Double32_t fTimeBinWRMS; // rms of in time direction
  UShort_t   fTimeMax;     // maximum time bin
  UChar_t    fNPads;       // no of pads in cluster
  Double32_t fQMax;        // Q max
  Double32_t fQTot;        // Q tot
  
  Double32_t fCPad;         // to be removed
  Double32_t fX;            // to be removed
  Double32_t fY;            // to be removed
  Double32_t fResY;         // to be removed
  Double32_t fResYUnBiased; // to be removed
  Double32_t fResTime;      // to be removed
  
  UInt_t     fTrackNr;  //! Track number associated during the tracking
  
  TRef       fTrack;    // reference to the track
  ClassDef(GEMCluster,2);
};

#endif
