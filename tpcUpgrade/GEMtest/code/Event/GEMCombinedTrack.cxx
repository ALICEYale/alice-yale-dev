#include <TMath.h>
#include <TBits.h>
#include <TLinearFitter.h>
#include <TObjArray.h>
#include <TObjString.h>
#include <Riostream.h>

#include "GEMCombinedTrack.h"

ClassImp(GEMCombinedTrack);

GEMCombinedTrack::GEMCombinedTrack() :
  GEMTrack(),
  fNtracks(0)
{
  for (Int_t i = 0; i < kNMaxTracks; i++) fTracks[i] = 0;
  fROC = 255;
}

//________________________________________________________________________________
GEMCombinedTrack::GEMCombinedTrack(const GEMCombinedTrack &track) :
  GEMTrack(track),
  fNtracks(track.fNtracks)
{
  // copy ctor
  for (Int_t i = 0; i < kNMaxTracks; i++) fTracks[i] = track.fTracks[i];
}

//________________________________________________________________________________
GEMCombinedTrack& GEMCombinedTrack::operator=(const GEMCombinedTrack &track)
{
  // assignment operator
  if (&track == this) return *this;
  new (this) GEMCombinedTrack(track);
  
  return *this;
}

//________________________________________________________________________________
GEMCombinedTrack::GEMCombinedTrack(GEMTrack *track1, GEMTrack *track2) :
  GEMTrack(),
  fNtracks(0)
{
  if (track1) AddTrack(track1);
  if (track2) AddTrack(track2);
  fROC = 255;
}

//________________________________________________________________________________
void GEMCombinedTrack::AddTrack(GEMTrack* track)
{
  if (!track) return;
  
  if (fNtracks == kNMaxTracks) {
    Printf("ERROR: Maximum number of tracks reached! (fNtracks = %d)", fNtracks);
    return;
  }

  fTracks[fNtracks] = track;
  fNtracks++;

  AddClustersFromTrack(track);

  Refit(0,0);
}

//________________________________________________________________________________
void GEMCombinedTrack::Refit(Int_t minNPads, Int_t maxNPads, TBits* badClusters/*=0*/)
{
  // Perform the refit of the track parameter
  
  // The chi2 is just the average of the chi2 of the tracks... not sure it makes any sense
  fChi2        = fTracks[0]->GetChi2();

  // The following fields will contain the sum of the squares of the differences between the corresponding variables in the list of tracks.
  // The differences are taken with respect to the first track
  fOffset      = 0;
  fSlope       = 0;
  fOffsetTime  = 0;
  fSlopeTime   = 0;

  // These two fields will not be set
  fSlopeError  = 0;
  fOffsetError = 0;
  
  for (Int_t i = 1; i < fNtracks; i++) {
    if (minNPads != 0 || maxNPads != 0) fTracks[i]->Refit(minNPads, maxNPads, badClusters);
    
    fChi2        += fTracks[i]->GetChi2();
    
    fOffset      += (fTracks[i]->GetOffset() - fTracks[0]->GetOffset()) * (fTracks[i]->GetOffset() - fTracks[0]->GetOffset());
    fSlope       += (fTracks[i]->GetSlope() - fTracks[0]->GetSlope()) * (fTracks[i]->GetSlope() - fTracks[0]->GetSlope());
    fOffsetTime  += (fTracks[i]->GetOffsetTime() - fTracks[0]->GetOffsetTime()) * (fTracks[i]->GetOffsetTime() - fTracks[0]->GetOffsetTime());
    fSlopeTime   += (fTracks[i]->GetSlopeTime() - fTracks[0]->GetSlopeTime()) * (fTracks[i]->GetSlopeTime() - fTracks[0]->GetSlopeTime());
  }
  
  fChi2 /= fNtracks;
}
