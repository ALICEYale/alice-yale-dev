#ifndef GEMCombinedTrack_H
#define GEMCombinedTrack_H

#include "GEMTrack.h"

class GEMCombinedTrack : public GEMTrack {
public:
  GEMCombinedTrack();
  GEMCombinedTrack(const GEMCombinedTrack &track);
  GEMCombinedTrack(GEMTrack *track1, GEMTrack *track2);
  virtual ~GEMCombinedTrack() {;}

  GEMCombinedTrack& operator=(const GEMCombinedTrack &track);

  void Refit(Int_t minNpads, Int_t maxNpads, TBits *badClusters=0x0);
  void AddTrack(GEMTrack* track);
  Int_t GetNumberOfSubTracks() const { return fNtracks; }

  Double_t GetOffset(const Int_t i) const { if (i < fNtracks) return fTracks[i]->GetOffset(); else return -999; }
  Double_t GetSlope(const Int_t i)  const { if (i < fNtracks) return fTracks[i]->GetSlope() ; else return -999; }
  Double_t GetOffsetError(const Int_t i) const { if (i < fNtracks) return fTracks[i]->GetOffsetError(); else return -999; }
  Double_t GetSlopeError(const Int_t i) const { if (i < fNtracks) return fTracks[i]->GetSlopeError(); else return -999; }
  Double_t GetOffsetTime(const Int_t i) const { if (i < fNtracks) return fTracks[i]->GetOffsetTime(); else return -999; }
  Double_t GetSlopeTime(const Int_t i) const { if (i < fNtracks) return fTracks[i]->GetSlopeTime(); else return -999; }
  Double_t GetChi2(const Int_t i) const { if (i < fNtracks) return fTracks[i]->GetChi2(); else return -999; }
  UChar_t  GetROC(const Int_t i) const { if (i < fNtracks) return fTracks[i]->GetROC(); else return 255; }
  const GEMTrack * GetTrack(const Int_t i) const { return i < kNMaxTracks && i >= 0 ? static_cast<const GEMTrack*>(fTracks[i]) : 0; }

  static const Int_t kNMaxTracks = 10;  // maximum number of tracks that the array can support
  
protected:
  GEMTrack* fTracks[kNMaxTracks]  ; // tracks
  Int_t     fNtracks              ; // number of tracks
    
  ClassDef(GEMCombinedTrack,1);
};

#endif

