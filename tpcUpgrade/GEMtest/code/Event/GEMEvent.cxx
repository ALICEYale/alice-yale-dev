#include <TList.h>
#include <TObject.h>
#include <TVectorF.h>
#include <TClonesArray.h>

#include "GEMEvent.h"

ClassImp(GEMEvent);

GEMEvent::GEMEvent()
: TObject()
, fRunNumber(-1)
, fEventNumber(-1)
, fTimeStamp(0)
, fNclusters()
, fNclustersUsed()
, fNtracks()
, fCherenkov(0.)
, fPressure(0.)
, fTemperature(0.)
, fRelativeHumidity(0.)
, fBeam(0)
, fIBF(0.)
, fSF(0)
, fGain(0)
, fTF3(0)
, fG3G4(0.)
, fTracks()
, fCombinedTracks()
{
  //
  //
  //
  for (Int_t iroc=0; iroc<72; ++iroc) {
    fNclusters    [iroc]=0;
    fNclustersUsed[iroc]=0;
    fNtracks      [iroc]=0;
  }
  fTracks.SetClass("GEMTrack");
  fCombinedTracks.SetClass("GEMCombinedTrack");
}

//________________________________________________________________________________
GEMEvent::GEMEvent(const GEMEvent &event)
: TObject(event)
, fRunNumber(event.fRunNumber)
, fEventNumber(event.fEventNumber)
, fTimeStamp(event.fTimeStamp)
, fNclusters()
, fNclustersUsed()
, fNtracks()
, fCherenkov(event.fCherenkov)
, fPressure(event.fPressure)
, fTemperature(event.fTemperature)
, fRelativeHumidity(event.fRelativeHumidity)
, fBeam(event.fBeam)
, fIBF(event.fIBF)
, fSF(event.fSF)
, fGain(event.fGain)
, fTF3(event.fTF3)
, fG3G4(event.fG3G4)
, fTracks(event.fTracks)
, fCombinedTracks(event.fCombinedTracks)
{
  //
  //
  //
  for (Int_t iroc=0; iroc<72; ++iroc) {
    fNclusters    [iroc]=event.fNclusters    [iroc];
    fNclustersUsed[iroc]=event.fNclustersUsed[iroc];
    fNtracks      [iroc]=event.fNtracks      [iroc];
  }
  
}

//________________________________________________________________________________
GEMEvent::~GEMEvent()
{
  // dtor
}


//________________________________________________________________________________
GEMEvent& GEMEvent::operator = (const GEMEvent &event)
{
  // assignment operator
  if (&event == this) return *this;
  new (this) GEMEvent(event);
  
  return *this;
}

//________________________________________________________________________________
void GEMEvent::NewEvent(Int_t eventNumber, Int_t cherenkov, Double_t temperature, Double_t relativeHum, Double_t pressure)
{
  fEventNumber=eventNumber;
  fCherenkov=cherenkov;
  fPressure=pressure;
  fTemperature=temperature;
  fRelativeHumidity=relativeHum;
  for (Int_t iroc=0; iroc<72; ++iroc) {
    fNclusters    [iroc]=0;
    fNclustersUsed[iroc]=0;
    fNtracks      [iroc]=0;
  }
  fTracks.Clear("C");
  fCombinedTracks.Clear("C");
}

//________________________________________________________________________________
GEMTrack* GEMEvent::AddTrack(const GEMTrack &track)
{
  // Add a track

  if (fTracks.GetEntriesFast()>=fTracks.Capacity()) fTracks.Expand(2*fTracks.Capacity());
  return new(fTracks[fTracks.GetEntriesFast()]) GEMTrack(track);
}

//________________________________________________________________________________
GEMCombinedTrack* GEMEvent::AddCombinedTrack(const GEMCombinedTrack &track)
{
  // Add a track

  if (fCombinedTracks.GetEntriesFast()>=fCombinedTracks.Capacity()) fCombinedTracks.Expand(2*fCombinedTracks.Capacity());
  return new(fCombinedTracks[fCombinedTracks.GetEntriesFast()]) GEMCombinedTrack(track);
}


//________________________________________________________________________________
void GEMEvent::Reset()
{
  //
  // reset data
  //
  fRunNumber=-1;
  fEventNumber=-1;
  fTimeStamp=0;
  fCherenkov=0.;
  fPressure=0.;
  fTemperature=0.;
  fRelativeHumidity=0.;
  fBeam=0;
  fIBF=0.;
  fSF=0;
  fGain=0;
  fTF3=0;
  fG3G4=0.;

  for (Int_t iroc=0; iroc<72; ++iroc) {
    fNclusters    [iroc]=0;
    fNclustersUsed[iroc]=0;
    fNtracks      [iroc]=0;
  }
  fTracks.Clear("C");
  fCombinedTracks.Clear("C");
  
}

//________________________________________________________________________________
void GEMEvent::Print(Option_t* /*option = ""*/) const
{
  // print event information
  printf("====== Event information - Run %d, Event %d ======\n",fRunNumber,fEventNumber);
  printf("  Counter information - Cherenkov: %.0f (ADC)\n", fCherenkov);
  printf("  Beam: %d (GeV/c)\n", fBeam);
  printf("  GEM settings - IB: %.2f, SF %u [%%], TF3: %u [V/cm], G3/G4: %.2f\n",fIBF,fSF,fTF3,fG3G4);
  printf("   Number of Tracks: %d\n",GetNumberOfTracks());
  printf("  Cluster information\n");
  for (Int_t iroc=0; iroc<72; ++iroc) 
    printf("    ROC %02d - Total found: %d, Used in Tracks: %d, Number of Tracks: %d\n", iroc, fNclusters[iroc], fNclustersUsed[iroc], fNtracks[iroc]);
}
