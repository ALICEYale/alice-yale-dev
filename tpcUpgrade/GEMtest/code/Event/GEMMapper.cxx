#include "GEMMapper.h"
#include <cstdio>

ClassImp(GEMMapper)

Bool_t GEMMapper::fUseAlternativeMapping = kFALSE;

//________________________________________________________________________________
UChar_t GEMMapper::GetNPads(UChar_t row, UChar_t roc)
{
  if (fUseAlternativeMapping && roc > 0) {
    return 37;   // the pads are actually 8 but they go from 29 to 36 included!
  }
  else {
    return (row==0) ? 68 : 2 *UChar_t(Double_t(row)/3. +33.67);
  }
}

//________________________________________________________________________________
Bool_t GEMMapper::GetPadPosition(UChar_t row, UChar_t pad, Double_t pos[3], UChar_t roc)
{
  // get pad position
  pos[0] = pos[1] = pos[2] = 0;
  if (row >= GetNRows(roc) || pad >= GetNPads(row, roc)) {
    printf("GEMMapper-Error: Pad %d in row %d does not exists! This should not happen...\n", (Int_t)pad, (Int_t)row);
    return kFALSE;
  }

  pos[0] = GetPadX(row, roc);
  pos[1] = GetPadY(row, pad, roc);

  return kTRUE;
}

//________________________________________________________________________________
Double_t GEMMapper::GetPadX(UChar_t row, UChar_t roc)
{
  //pad x
  if (row >= GetNRows(roc)) {
    printf("GEMMapper-Error: Row %d does not exists! This should not happen...\n", (Int_t)row);
    return 0.;
  }
  if (fUseAlternativeMapping && roc > 0) {
    //printf("row = %d, padx = %.3f\n", (Int_t)row, (Float_t)row);
    return (Float_t)row;
  }
  else {
    const Float_t kInnerRadiusLow = 83.65;
    const Float_t firstrow = kInnerRadiusLow + 1.575;
   
    return firstrow + GetPadLength(roc)*(Float_t)row;
  }
}

//________________________________________________________________________________
Double_t GEMMapper::GetPadY(UChar_t row, UChar_t pad, UChar_t roc)
{
  // pad y
  if (row >= GetNRows(roc) || pad >= GetNPads(row, roc)) {
    printf("GEMMapper-Error: Pad %d in row %d of roc %d  does not exists! This should not happen...\n", (Int_t)pad, (Int_t)row, (Int_t) roc);
    return 0.;
  }
  if (fUseAlternativeMapping && roc > 0) {
    return pad;
  }
  else {
    return GetPadWidth(roc)*(Int_t(pad)+0.5-Int_t(GetNPads(row, roc))/2);
  }
}
