#ifndef GEMMapper_H
#define GEMMapper_H

#include "TObject.h"

class GEMMapper : public TObject {
public:
  GEMMapper() : TObject() {;}

  static void    SetUseAlternativeMapping(Bool_t b=kTRUE) { fUseAlternativeMapping = b   ; }
  static Bool_t  GetUseAlternativeMapping()               { return fUseAlternativeMapping; }

  static Float_t GetPadWidth(UChar_t roc=0)  { return roc > 0 && fUseAlternativeMapping ? 1. : 0.40; }
  static Float_t GetPadLength(UChar_t roc=0) { return roc > 0 && fUseAlternativeMapping ? 1. : 0.75; }

  static Bool_t  GetPadPosition(UChar_t row, UChar_t pad, Double_t pos[3], UChar_t roc=0);
  static Double_t GetPadX(UChar_t row, UChar_t roc=0);
  static Double_t GetPadY(UChar_t row, UChar_t pad, UChar_t roc=0);

  static UChar_t GetNRows(UChar_t roc=0) { return roc > 0 && fUseAlternativeMapping ? 32 : 63; }
  static UChar_t GetNPads(UChar_t row, UChar_t roc=0);

 private:
  static Bool_t fUseAlternativeMapping; //

  ClassDef(GEMMapper,0)  // simple mapper
};

#endif
