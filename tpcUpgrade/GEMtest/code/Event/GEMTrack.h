#ifndef GEMTrack_H
#define GEMTrack_H

#include <TClonesArray.h>
#include <TObject.h>
#include <TExMap.h>

#include "GEMCluster.h"

class TBits;
class AliTPCCalROC;

class GEMTrack : public TObject {
public:
  GEMTrack();
  GEMTrack(const GEMTrack &track);
  GEMTrack(UChar_t roc, Double_t offset, Double_t slope, Double_t offsetError, Double_t slopeError, Double_t chi2, Double_t offsetTime, Double_t slopeTime);
  virtual ~GEMTrack() {;}

  GEMTrack& operator = (const GEMTrack &track);
  
  Int_t GetNumberOfClusters() const {return fClusters.GetEntriesFast();}
  
  const GEMCluster* GetCluster(Int_t i) const { return i >= 0 && i < GetNumberOfClusters() ? static_cast<const GEMCluster*>(fClusters.At(i)) : 0; }
  Double_t GetTruncatedMean(Double_t low=0.05, Double_t high=0.7, Int_t type=1) const;

  Double_t GetAveragePad(Bool_t w=kTRUE) const;
  
  GEMCluster* AddCluster(const GEMCluster &cluster);
  void AddClustersFromTrack(GEMTrack *track);

  Double_t EvalY(Double_t x)      const { return fOffset+fSlope*x; }
  Double_t EvalPad(Double_t row)  const { return fOffset+fSlope*row; }
  Double_t EvalTime(Double_t x) const { return fOffsetTime+fSlopeTime*x; }

  Double_t GetOffset() const { return fOffset; }
  Double_t GetSlope()  const { return fSlope ; }
  Double_t GetOffsetError() const { return fOffsetError; }
  Double_t GetSlopeError()  const { return fSlopeError ; }
  Double_t GetOffsetTime() const { return fOffsetTime; }
  Double_t GetSlopeTime()  const { return fSlopeTime ; }

  Double_t GetChi2()  const { return fChi2 ; }

  UChar_t  GetROC()    const { return fROC;    }
  
  virtual void Refit(Int_t minNpads, Int_t maxNpads, TBits *badClusters=0x0);
  
  virtual void   Clear      (Option_t *option="") { fClusters.Clear(option); }

//   static SetGainMap(AliTPCCalROC *map) { fgGainMap=map; }
  static void SetRemoveRowsdEdx(TString rows);
  static Bool_t GetRemoveRowdEdx(UChar_t row) { return fRemoveRows(row)==1; }
  
protected:
  //track parameters
  Double32_t fOffset;       // offset parameter of linear fit in pad:row space
  Double32_t fSlope;        // slope parameter of linear fit in pad:row space
  Double32_t fSlopeError;   // slope error of linear fit in pad:row space
  Double32_t fOffsetError;  // offset error of linear fit in pad:row space
  Double_t fChi2;           // chi2 of linear fit in pad:row space
  
  
  Double32_t fOffsetTime;   // offset parameter of linear fit in time:row space
  Double32_t fSlopeTime;    // slope parameter of linear fit in time:row space

  UChar_t    fROC;          // ROC in which the track was found
  
  TClonesArray fClusters;   // array of clusters associated to the track
  static TExMap fRemoveRows; // maps of rows to remove

//   static AliTPCCalROC *fgGainMap; //! gain map to be used for dE/dx
  
  const GEMCluster* GetClusterFast(Int_t i) const { return static_cast<const GEMCluster*>(fClusters.UncheckedAt(i)); }
  
  ClassDef(GEMTrack,2);
};

#endif

