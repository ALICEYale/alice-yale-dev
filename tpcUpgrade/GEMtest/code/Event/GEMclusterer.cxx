/**************************************************************************
 * Copyright(c) 1998-1999, ALICE Experiment at CERN, All rights reserved. *
 *                                                                        *
 * Author: The ALICE Off-line Project.                                    *
 * Contributors are mentioned in the code where appropriate.              *
 *                                                                        *
 * Permission to use, copy, modify and distribute this software and its   *
 * documentation strictly for non-commercial purposes is hereby granted   *
 * without fee, provided that the above copyright notice appears in all   *
 * copies and that both the copyright notice and this permission notice   *
 * appear in the supporting documentation. The authors make no claims     *
 * about the suitability of this software for any purpose. It is          *
 * provided "as is" without express or implied warranty.                  *
 **************************************************************************/


//Root includes
#include <TSystem.h>
#include <TObjString.h>
#include <TString.h>
#include <TMath.h>
#include <TError.h>
#include <TObjArray.h>
#include <TLinearFitter.h>
#include <TFile.h>
#include <TTree.h>
#include <TGraph.h>
#include <TH2.h>
#include <TROOT.h>
#include <Riostream.h>

//AliRoot includes
#include "AliGEMRawStreamV3.h"
#include "AliTPCCalROC.h"
#include "AliTPCROC.h"
#include "AliMathBase.h"

//header file
#include "GEMclusterer.h"
#include "AliLog.h"
/*

  root.exe code/loadlibs.C

  r=new AliRawReaderGEM("data/merge_687.txt")
  GEMclusterer c
  c.SetOutputFileName("/tmp/test.root")
  r->NextEvent()
  Int_t nevMax=1000;
  while (r->NextEvent() && r->GetEventIndex()<nevMax) c.ProcessEvent(r);
  c.CloseFile()
 
 */

ClassImp(GEMclusterer)

GEMclusterer::GEMclusterer() :
  fFirstTimeBin(0),
  fLastTimeBin(300),
  fAdcMin(3),
  fAdcMax(1040),
  fWindowPad(2.),
  fWindowTime(2.),
  fNAllowedMisses(3.),
  fNMinClusters(25.),
  fSkipROC(72),
  fMapping(NULL),
  fArrClusterArrays(72),
  fBunches(0x0),
  fGEMEvent(0x0),
  fTree(0x0),
  fFile(0x0),
  fOutFileName(),
  fLogFile(),
  fPressureValuesFile(),
  fCalibFile(),
  fLoadCalib(kTRUE),
  fExcludeHotChannels(kTRUE),
  fExcludeWarmChannels(kFALSE),
  fExcludeDeadChannels(kTRUE),
  fLogData(0x0),
  fBlackEvent(kFALSE),
  fPedDir(),
  fEventCounter(0),
  fSignalCounter(0),            // Signal counter
  fClusterCounter(0),           // Cluster counter
  fTPCRawStream(0),
  fRawReaderGEM(0),
  fDateMode(kFALSE),
  fAllBins(0),
  fAllSigBins(0),
  fAllNSigBins(0),
  fRowsMax(0),
  fPadsMax(0),
  fTimeBinsMax(0),
  fRunResUBiased(0),
  fStoreBunches(kFALSE),
  fClusterAndTracking(kTRUE),
  fDebugLevel(0),
  fPressureValues(0x0),
  fNcombineROC(0)
{
  //
  // default constructor
  //

  for (Int_t i = 0; i < GEMCombinedTrack::kNMaxTracks; i++) fCombineROC[i] = 255;

  for (Int_t i = 0; i < kNumberOfROC; i++) {
    fGainMap[i] = 0;
    fDeadChannelMap[i] = 0;
    fPedestalMap[i] = 0;
  }
}

//_____________________________________________________________________
GEMclusterer::GEMclusterer(const GEMclusterer &ped) :
  TObject(ped),
  fFirstTimeBin(ped.GetFirstTimeBin()),
  fLastTimeBin(ped.GetLastTimeBin()),
  fAdcMin(ped.GetAdcMin()),
  fAdcMax(ped.GetAdcMax()),
  fMapping(NULL),
  fArrClusterArrays(ped.fArrClusterArrays),
  fBunches(0x0),
  fGEMEvent(0x0),
  fTree(0x0),
  fFile(0x0),
  fOutFileName(ped.fOutFileName),
  fLogFile(),
  fPressureValuesFile(),
  fCalibFile(),
  fLoadCalib(kTRUE),
  fExcludeHotChannels(kTRUE),
  fExcludeWarmChannels(kFALSE),
  fExcludeDeadChannels(kTRUE),
  fLogData(0x0),
  fEventCounter(ped.GetEventCounter()),
  fSignalCounter(ped.GetSignalCounter()),
  fClusterCounter(ped.GetClusterCounter()),
  fTPCRawStream(ped.fTPCRawStream),
  fRawReaderGEM(ped.fRawReaderGEM),
  fDateMode(ped.fDateMode),
  fAllBins(0),
  fAllSigBins(0),
  fAllNSigBins(0),
  fRowsMax(0),
  fPadsMax(0),
  fTimeBinsMax(0),
  fRunResUBiased(ped.fRunResUBiased),
  fStoreBunches(ped.fStoreBunches),
  fClusterAndTracking(ped.fClusterAndTracking),
  fDebugLevel(ped.fDebugLevel),
  fPressureValues(0x0),
  fNcombineROC(ped.fNcombineROC)
{
  //
  // copy constructor
  //

  for (Int_t i = 0; i < GEMCombinedTrack::kNMaxTracks; i++) fCombineROC[i] = ped.fCombineROC[i];
  
  for (Int_t i = 0; i < kNumberOfROC; i++) {
    fGainMap[i] = 0;
    fDeadChannelMap[i] = 0;
    fPedestalMap[i] = 0;
  }
}

//_____________________________________________________________________
GEMclusterer& GEMclusterer::operator = (const  GEMclusterer &source)
{
  //
  // assignment operator
  //
  if (&source == this) return *this;
  new (this) GEMclusterer(source);

  return *this;
}


//_____________________________________________________________________
GEMclusterer::~GEMclusterer()
{
  //
  // destructor
  //
  
  for (Int_t iRow = 0; iRow < fRowsMax; iRow++) {
    delete [] fAllBins[iRow];
    delete [] fAllSigBins[iRow];
  }  
  delete [] fAllBins;
  delete [] fAllSigBins;
  delete [] fAllNSigBins;

  CloseFile();

  delete fLogData;
  delete fPressureValues;
  
  for (Int_t i = 0; i < kNumberOfROC; i++) {
    delete fGainMap[i];
    delete fDeadChannelMap[i];
    delete fPedestalMap[i];
  }
}

//_____________________________________________________________________
Bool_t GEMclusterer::ProcessEvent()
{
  //
  // Process the next event
  //

  Bool_t res = kFALSE;
  
  if (fDateMode) {
    res = ProcessEventGEMDate(static_cast<AliRawReaderGEMDate*>(fRawReaderGEM));
  }
  else {
    res = ProcessEventGEM(static_cast<AliRawReaderGEM*>(fRawReaderGEM));
  }

  if (res && fTPCRawStream) res = ProcessEventTPC(fTPCRawStream);

  if (res) {
    ++fEventCounter; // only increment event counter if there is TPC data
    if (fTree && (fGEMEvent->GetNumberOfTracks() > 0 || fStoreBunches)) fTree->Fill();
  }

  //if (fGEMEvent->GetNumberOfTracks() == 0 && fDebugLevel > 0) Printf("Event with 0 tracks: %d", fRawReaderGEM->GetEventFromTag());
  
  return res;
}

//_____________________________________________________________________
Bool_t GEMclusterer::ProcessAllEvents(Int_t min, Int_t max)
{
  // In place until we are consident that everything is working correctly
  Bool_t res = kTRUE;
  while (fRawReaderGEM->NextEvent() && (max < 0 || fRawReaderGEM->GetEventIndex() < max)) {
    if (fRawReaderGEM->GetEventIndex() >= min) {
      const Int_t iev = fRawReaderGEM->GetEventIndex();
      if (iev%1000 == 0) Printf("Processing event %6d", iev);
      //      Printf("Processing event %6d", iev);
      res = ProcessEvent() && res;
    }
  }

  return res;
}

//_____________________________________________________________________
Bool_t GEMclusterer::ProcessEventTPC(AliGEMRawStreamV3 *const rawStreamV3)
{
  //
  // Event Processing loop - AliGEMRawStreamV3
  //
  Bool_t withInput = kFALSE;
  Int_t nSignals = 0;
  Int_t lastSector = -1;

  if (!fAllBins) MakeArrays();
  while (rawStreamV3->NextDDL()) {
    const Int_t iRoc = rawStreamV3->GetSector(); //  current read out chamber
    if (iRoc < 0) continue;
    if (fSkipROC(iRoc)) continue;
    //std::cout<<"iRoc = "<<iRoc<<std::endl;
    while (rawStreamV3->NextChannel()) {
    
      const Int_t iRow    = rawStreamV3->GetRow();         //  current row
      const Int_t iPad    = rawStreamV3->GetPad();         //  current pad
      const Int_t iPatch  = rawStreamV3->GetPatchIndex();  //  current patch
      const Int_t iBranch = rawStreamV3->GetBranch();      //  current branch

      //Printf("iRow = %d, iPad = %d, iPatch = %d, iBranch = %d", iRow, iPad, iPatch, iBranch);
      
      if (iRow < 0 || iPad < 0) continue;
      // Call local maxima finder if the data is in a new sector
      if (iRoc != lastSector) {
        
        if (nSignals > 0 && fClusterAndTracking) FindLocalMaxima(lastSector);
        
        CleanArrays();
        lastSector = iRoc;
        nSignals = 0;
      }
      
      while (rawStreamV3->NextBunch()) {

        const Int_t  startTbin    = Int_t(rawStreamV3->GetStartTimeBin());
        const Int_t  bunchlength  = Int_t(rawStreamV3->GetBunchLength());
        const UShort_t *sig = rawStreamV3->GetSignals();

        Int_t startTbinRunning=startTbin;
        for (Int_t iTimeBin = 0; iTimeBin<bunchlength; iTimeBin++){
          Float_t signal=(Float_t)sig[iTimeBin];
          nSignals += Update(iRoc,iRow,iPad,startTbinRunning--,signal, iPatch, iBranch);
          withInput = kTRUE;
        }

        // check if we should write the bunches
        if (fStoreBunches) {
          // from the electronics the time bins come in reverse order, from the last to the first
          // so we turn them before storing
          Int_t *sigSort=new Int_t[bunchlength];
          for (Int_t iTimeBin = 0; iTimeBin<bunchlength; iTimeBin++){
            sigSort[iTimeBin]=sig[bunchlength-iTimeBin-1];
	    if(fBlackEvent){
	      sigSort[iTimeBin] += 50;    // Black event offset
	      sigSort[iTimeBin] -= fPedestalMap[iRoc]->GetBinContent(iRow+1,iPad+1);    // subtract mean pedestal for each pad
	    } 
        if(fExtraPedestal){
	      sigSort[iTimeBin] -= fExtraPedestalMap[iRoc]->GetBinContent(iRow+1,iPad+1);    // subtract mean pedestal for each pad
	    } 
		// Subtrack pedestal
	  }
          //fill bunch
          AddBunch(UChar_t(iRoc), UChar_t(iRow),UChar_t(iPad),bunchlength,sigSort,UShort_t(startTbin-bunchlength+1));

          delete [] sigSort;
        }
      }
    }
  }

  if (!fClusterAndTracking) {
    // if no tracking cleanup and return
    CleanArrays();
    return withInput;
  }

  if (lastSector >= 0 && nSignals > 0)
    FindLocalMaxima(lastSector);

  CleanArrays();
  
  //Int_t nTries = 10; //number of tries with a new seed cluster for tracking
  Int_t nTries = -1; // defaults to number of clusters in the roc
  Bool_t reverse = kTRUE; //start search from pad row opposite to beam entry side

  for (Int_t iRoc=0; iRoc<72; ++iRoc) {
    TrackFinder(iRoc, nTries, reverse);
  }
  //std::cout<<"returned to ProcessEventTPC..."<<std::endl;
  if (fNcombineROC > 0) DoCombineTracks();

  return withInput;
}

//_____________________________________________________________________
Bool_t GEMclusterer::ProcessEventGEM(AliRawReaderGEM *const rawReader)
{
  //
  //  Event processing loop - AliRawReader
  //

  // create file and tree if an output name is set
  Init(rawReader->GetRunNumber());

  if (fGEMEvent){
    fGEMEvent->SetRunNumber(rawReader->GetRunNumber());
    fGEMEvent->NewEvent(Int_t(rawReader->GetEventFromTag()),
                        Int_t(rawReader->GetCamacData(AliRawReaderGEM::kCamacCherenkov)),
                        rawReader->GetCamacData(AliRawReaderGEM::kCamacTemperature),
                        rawReader->GetCamacData(AliRawReaderGEM::kCamacRelativeHumidity),
                        rawReader->GetCamacData(AliRawReaderGEM::kCamacPressure)
                       );
    if (fLogData && !fGEMEvent->GetGain() && !fGEMEvent->GetTF3()){
      fLogData->Print();
      fGEMEvent->SetBeam(fLogData->At(1)?(Char_t)  ((TObjString*)fLogData->At(1))->String().Atoi():0);
      fGEMEvent->SetIBF (fLogData->At(2)?(Double_t)((TObjString*)fLogData->At(2))->String().Atof():0.);
      fGEMEvent->SetSF  (fLogData->At(3)?(UChar_t) ((TObjString*)fLogData->At(3))->String().Atoi():0);
      fGEMEvent->SetTF3 (fLogData->At(4)?(UShort_t)((TObjString*)fLogData->At(4))->String().Atoi():0);
      fGEMEvent->SetG3G4(fLogData->At(5)?(Double_t)((TObjString*)fLogData->At(5))->String().Atof():0.);
      fGEMEvent->SetSigma(fLogData->At(6)?(Double_t)((TObjString*)fLogData->At(6))->String().Atof():0.);
      fGEMEvent->SetGain (fLogData->At(7)?(UShort_t) ((TObjString*)fLogData->At(7))->String().Atoi():0);
      fGEMEvent->SetCompensated (fLogData->At(8)?(Bool_t) ((TObjString*)fLogData->At(8))->String().Atoi():0);
    }
    if (fBunches) fBunches->Clear("C");

    return kTRUE;
  }

  return kFALSE;
}

//_____________________________________________________________________
Bool_t GEMclusterer::ProcessEventGEMDate(AliRawReaderGEMDate *const rawReader)
{
  //
  //  Event processing loop - AliRawReader
  //
  
  // create file and tree if an output name is set
  Init(rawReader->GetRunNumber());
  
  if (fGEMEvent){
    fGEMEvent->SetRunNumber(rawReader->GetRunNumber());
    fGEMEvent->NewEvent(Int_t(rawReader->GetEventFromTag()),
                        Int_t(rawReader->GetCamacData(AliRawReaderGEM::kCamacCherenkov)),
                        rawReader->GetCamacData(AliRawReaderGEM::kCamacTemperature),
                        rawReader->GetCamacData(AliRawReaderGEM::kCamacRelativeHumidity),
                        rawReader->GetCamacData(AliRawReaderGEM::kCamacPressure)
    );
    const UInt_t timeStamp = rawReader->GetTimestamp();
    fGEMEvent->SetTimeStamp(timeStamp);

    if (fLogData && !fGEMEvent->GetGain() && !fGEMEvent->GetTF3()) {
      fLogData->Print();
      fGEMEvent->SetBeam(fLogData->At(1)?(Char_t)  ((TObjString*)fLogData->At(1))->String().Atoi():0);
      fGEMEvent->SetIBF (fLogData->At(2)?(Double_t)((TObjString*)fLogData->At(2))->String().Atof():0.);
      fGEMEvent->SetSF  (fLogData->At(3)?(UChar_t) ((TObjString*)fLogData->At(3))->String().Atoi():0);
      fGEMEvent->SetTF3 (fLogData->At(4)?(UShort_t)((TObjString*)fLogData->At(4))->String().Atoi():0);
      fGEMEvent->SetG3G4(fLogData->At(5)?(Double_t)((TObjString*)fLogData->At(5))->String().Atof():0.);
      fGEMEvent->SetSigma(fLogData->At(6)?(Double_t)((TObjString*)fLogData->At(6))->String().Atof():0.);
      fGEMEvent->SetGain (fLogData->At(7)?(UShort_t) ((TObjString*)fLogData->At(7))->String().Atoi():0);
      fGEMEvent->SetCompensated (fLogData->At(8)?(Bool_t) ((TObjString*)fLogData->At(8))->String().Atoi():0);
    }

    /*if (fPressureValues){
      Double_t pressure=fPressureValues->Eval(timeStamp);
      if (timeStamp<fPressureValues->GetX()[0]) pressure=fPressureValues->GetY()[0];
      if (timeStamp>fPressureValues->GetX()[fPressureValues->GetN()-1]) pressure=fPressureValues->GetY()[fPressureValues->GetN()-1];
      fGEMEvent->SetPressure(pressure);
    }*/
    
    if (fBunches) fBunches->Clear("C");

    return kTRUE;
  }
  return kFALSE;
}

//_____________________________________________________________________
Int_t GEMclusterer::Update(const Int_t iRoc,
			   const Int_t iRow,
			   const Int_t iPad,
			   const Int_t iTimeBin,
			   Float_t signal,
			   const Int_t /*iPatch*/,
			   const Int_t /*iBranch*/)
{
  //
  // Signal filling method
  //
  
  //
  // TimeBin cut
  //
  if (iTimeBin < fFirstTimeBin) return 0;
  if (iTimeBin > fLastTimeBin) return 0;

  // Require at least 2 ADC channels
  if (signal < 2.0) return 0;
  
  //
  // Dead Channel Map
  //
  if (fDeadChannelMap[iRoc]) {
    if ((fDeadChannelMap[iRoc]->GetBinContent(iRow+1, iPad+1) == kDeadChannel && fExcludeDeadChannels) ||
        (fDeadChannelMap[iRoc]->GetBinContent(iRow+1, iPad+1) == kWarmChannel && fExcludeWarmChannels) ||
        (fDeadChannelMap[iRoc]->GetBinContent(iRow+1, iPad+1) == kHotChannel && fExcludeHotChannels)) {
      return 0;
    }
  }
  
  //
  // Apply black event offset
  //
  if(fBlackEvent){
    //std::cout<<"pre: "<<signal<<std::endl;
    signal += 50;    // Black event offset
    signal -= fPedestalMap[iRoc]->GetBinContent(iRow+1,iPad+1);    // subtract mean pedestal for each pad
    //std::cout<<"post: "<<signal<<std::endl; 
  }

  //
  // Apply extra pedestal offset
  //
  if(fExtraPedestal){
    //std::cout<<"pre: "<<signal<<std::endl;
    signal -= fExtraPedestalMap[iRoc]->GetBinContent(iRow+1,iPad+1);    // subtract mean pedestal for each pad
    //std::cout<<"post: "<<signal<<std::endl; 
  }


  //
  // Apply gain correction
  //
  if (fGainMap[iRoc]) {
    const Float_t gainFactor = fGainMap[iRoc]->GetBinContent(iRow+1, iPad+1);
    if (gainFactor > 0) signal /= gainFactor;
    //if(iRoc==1) signal *= 1.36; // normalize chamber gains
  }
  
  //
  // This signal is ok and we store it in the cluster map
  //
  SetExpandDigit(iRow, iPad, iTimeBin, signal);

  ++fSignalCounter;
  
  return 1; // signal was accepted
}

//_____________________________________________________________________
void GEMclusterer::FindLocalMaxima(const Int_t roc)
{
  //
  // This method is called after the data from each sector has been
  // exapanded into an array
  // Loop over the signals and identify local maxima and fill the
  // calibration objects with the information
  //

  Int_t nLocalMaxima = 0;
  const Int_t maxTimeBin = fTimeBinsMax+4; // Used to step between neighboring pads 
                                           // Because we have tha pad-time data in a 
                                           // 1d array

  for (UChar_t iRow = 0; iRow < fRowsMax; iRow++) {

    Float_t* allBins = fAllBins[iRow];
    Int_t* sigBins   = fAllSigBins[iRow];
    const Int_t nSigBins   = fAllNSigBins[iRow];

    for (Int_t iSig = 0; iSig < nSigBins; iSig++) {

      Int_t bin  = sigBins[iSig];
      Float_t *b = &allBins[bin];

      //
      // Now we check if this is a local maximum
      //

      Float_t qMax = b[0];

      // First check that the charge is bigger than the threshold
      if (qMax<fAdcMin) continue;

      // Require at least one neighboring pad with signal
//       if (b[-maxTimeBin]+b[maxTimeBin]<=0) continue;

      // Require at least one neighboring time bin with signal
      if (b[-1]+b[1]<=0) continue;

      //
      // Check that this is a local maximum
      // Note that the checking is done so that if 2 charges has the same
      // qMax then only 1 cluster is generated 
      // (that is why there is BOTH > and >=)
      //
      if (b[-maxTimeBin]   >= qMax) continue;
      if (b[-1  ]          >= qMax) continue; 
      if (b[+maxTimeBin]   > qMax)  continue; 
      if (b[+1  ]          > qMax)  continue; 
      if (b[-maxTimeBin-1] >= qMax) continue;
      if (b[+maxTimeBin-1] >= qMax) continue; 
      if (b[+maxTimeBin+1] > qMax)  continue; 
      if (b[-maxTimeBin+1] >= qMax) continue;

      //
      // Now we accept the local maximum and fill the calibration/data objects
      //
      //printf("cluster found \n");
      ++nLocalMaxima;

      Int_t iPad, iTimeBin;
      GetPadAndTimeBin(bin, iPad, iTimeBin);

      //
      // Calculate the total charge as the sum over the region:
      //
      //    o o o o o
      //    o i i i o
      //    o i C i o
      //    o i i i o
      //    o o o o o
      //
      // with qmax at the center C.
      //
      // The inner charge (i) we always add, but we only add the outer
      // charge (o) if the neighboring inner bin (i) has a signal.
      //

      // use pad and time bin center for COG and RMS
      const Double_t iPadC  = iPad+0.5;
      const Double_t iTimeBinC = iTimeBin+0.5;
      Int_t minP = 0, maxP = 0, minT = 0, maxT = 0;
      Double_t weightedPad = iPadC*qMax;
      Double_t weightedTime = iTimeBinC*qMax;
      UChar_t nPads = 1;
      Double_t weightedPadSq = iPadC*iPadC*qMax;
      Double_t weightedTimeSq = iTimeBinC*iTimeBinC*qMax;
      //variables for calc rms
 
      Double_t qTot = qMax;
      for(Int_t i = -1; i<=1; i++) {
        for(Int_t j = -1; j<=1; j++) {
          if(i==0 && j==0)
            continue;

          Float_t charge1 = GetQ(b, i, j, maxTimeBin, minT, maxT, minP, maxP);
          if(charge1>0) {
            qTot += charge1; // Changed to ensure that tracking works for black events. Should not affect normal tracking
            // see if the next neighbor is also above threshold
            weightedPad+=(iPadC+j)*charge1;
            weightedTime+=(iTimeBinC+i)*charge1;
            weightedPadSq+=(iPadC+j)*(iPadC+j)*charge1;
            weightedTimeSq+=(iTimeBinC+i)*(iTimeBinC+i)*charge1;

            if(i==0 && j!=0)nPads++;

            if(i*j==0) {
              Float_t chargeNeighb = GetQ(b, 2*i, 2*j, maxTimeBin, minT, maxT, minP, maxP);
			  if (chargeNeighb < 0) { continue; } // Changed to ensure that tracking works for black events
              qTot += chargeNeighb;

              weightedPad+=(iPadC+2*j)*chargeNeighb;
              weightedTime+=(iTimeBinC+2*i)*chargeNeighb;
              weightedPadSq+=(iPadC+2*j)*(iPadC+2*j)*chargeNeighb;
              weightedTimeSq+=(iTimeBinC+2*i)*(iTimeBinC+2*i)*chargeNeighb;
              if(chargeNeighb>0)  //only add to nPads if there is signal
                if(i==0 && j!=0)nPads++;

            } else {
              // we are in a diagonal corner
              Float_t charge12 = GetQ(b,   i, 2*j, maxTimeBin, minT, maxT, minP, maxP);
              Float_t charge21 = GetQ(b, 2*i,   j, maxTimeBin, minT, maxT, minP, maxP);
              Float_t charge22 = GetQ(b, 2*i, 2*j, maxTimeBin, minT, maxT, minP, maxP);
			  if ((charge12 < 0) || (charge21 < 0) || (charge22 < 0)) { continue; } // Changed to esure that tracking works for black events
              qTot += charge12;
              qTot += charge21;
              qTot += charge22;

              weightedPad+=(iPadC+2*j)*charge12;
              weightedTime+=(iTimeBinC+i)*charge12;
              weightedPadSq+=(iPadC+2*j)*(iPadC+2*j)*charge12;
              weightedTimeSq+=(iTimeBinC+i)*(iTimeBinC+i)*charge12;

              weightedPad+=(iPadC+j)*charge21;
              weightedTime+=(iTimeBinC+2*i)*charge21;
              weightedPadSq+=(iPadC+j)*(iPadC+j)*charge21;
              weightedTimeSq+=(iTimeBinC+2*i)*(iTimeBinC+2*i)*charge21;

              weightedPad+=(iPadC+2*j)*charge22;
              weightedTime+=(iTimeBinC+2*i)*charge22;
              weightedPadSq+=(iPadC+2*j)*(iPadC+2*j)*charge22;
              weightedTimeSq+=(iTimeBinC+2*i)*(iTimeBinC+2*i)*charge22;

            }
          }
        }
      }
      weightedPad/=qTot;
      weightedTime/=qTot;
      weightedPadSq/=qTot;
      weightedTimeSq/=qTot;
      
      weightedPadSq=TMath::Sqrt(TMath::Abs(weightedPad*weightedPad-weightedPadSq));
      weightedTimeSq=TMath::Sqrt(TMath::Abs(weightedTime*weightedTime-weightedTimeSq));
      //TODO: how to deal with one pad clusters [RMS]
      AddCluster(GEMCluster(roc, iRow, weightedPad, weightedTime, weightedPadSq, weightedTimeSq, iTimeBin, qMax, qTot, nPads));
      //AddCluster(GEMCluster(iRow, iPad, iTimeBin, weightedPadSq, weightedTimeSq, iTimeBin, qMax, qTot, nPads));
      
    } // end loop over signals
  } // end loop over rows
  
  fClusterCounter += nLocalMaxima;
}

//_____________________________________________________________________
void GEMclusterer::AddCluster(const GEMCluster &cluster)
{
  //
  //
  //
  //Printf("Adding cluster ROC=%d, Row=%d, Pad=%.3f", cluster.GetROC(), cluster.GetRow(), cluster.GetPad());
  TClonesArray *pclusterArray = GetClusterArray(cluster.GetROC());
  if (!pclusterArray) {
    pclusterArray = new TClonesArray("GEMCluster");
    fArrClusterArrays.AddAt(pclusterArray,cluster.GetROC());
  }
  TClonesArray &clusterArray=*pclusterArray;
  new(clusterArray[clusterArray.GetEntriesFast()]) GEMCluster(cluster);
}

//_____________________________________________________________________
void GEMclusterer::MakeArrays(){
  //
  // The arrays for expanding the raw data are defined and 
  // som parameters are intialised
  //
  AliTPCROC * roc = AliTPCROC::Instance();
  //
  // To make the array big enough for all sectors we take 
  // the dimensions from the outer row of an OROC (the last sector)
  //
  fRowsMax     = roc->GetNRows(roc->GetNSector()-1);
  fPadsMax     = roc->GetNPads(roc->GetNSector()-1,fRowsMax-1);
  fTimeBinsMax = fLastTimeBin - fFirstTimeBin +1; 

  //
  // Since we have added 2 pads (TimeBins) before and after the real pads (TimeBins) 
  // to make sure that we can always query the exanded table even when the 
  // max is on the edge
  //

 
  fAllBins = new Float_t*[fRowsMax];
  fAllSigBins = new Int_t*[fRowsMax];
  fAllNSigBins = new Int_t[fRowsMax];

  for (Int_t iRow = 0; iRow < fRowsMax; iRow++) {
    //
    Int_t maxBin = (fTimeBinsMax+4)*(fPadsMax+4);  
    fAllBins[iRow] = new Float_t[maxBin];
    memset(fAllBins[iRow],0,sizeof(Float_t)*maxBin); // set all values to 0
    fAllSigBins[iRow] = new Int_t[maxBin];
    fAllNSigBins[iRow] = 0;
  }
}


//_____________________________________________________________________
void GEMclusterer::CleanArrays(){
  //
  //
  //

  for (Int_t iRow = 0; iRow < fRowsMax; iRow++) {

    // To speed up the performance by a factor 2 on cosmic data (and
    // presumably pp data as well) where the ocupancy is low, the
    // memset is only called if there is more than 1000 signals for a
    // row (of the order 1% occupancy)
    if(fAllNSigBins[iRow]<1000) {
      
      Float_t* allBins = fAllBins[iRow];
      Int_t* sigBins   = fAllSigBins[iRow];
      const Int_t nSignals = fAllNSigBins[iRow];
      for(Int_t i = 0; i < nSignals; i++)
	allBins[sigBins[i]]=0;      
    } else {

      Int_t maxBin = (fTimeBinsMax+4)*(fPadsMax+4); 
      memset(fAllBins[iRow],0,sizeof(Float_t)*maxBin);
    }

    fAllNSigBins[iRow]=0;
  }
}

//_____________________________________________________________________
void GEMclusterer::GetPadAndTimeBin(Int_t bin, Int_t& iPad, Int_t& iTimeBin){
  //
  // Return pad and timebin for a given bin
  //
  
  //  Int_t bin = iPad*(fTimeBinsMax+4)+iTimeBin;
  iTimeBin  = bin%(fTimeBinsMax+4);
  iPad      = (bin-iTimeBin)/(fTimeBinsMax+4);

  iPad     -= 2;
  iTimeBin -= 2;
  iTimeBin += fFirstTimeBin;

  R__ASSERT(iPad>=0 && iPad<=fPadsMax);
  R__ASSERT(iTimeBin>=fFirstTimeBin && iTimeBin<=fLastTimeBin);
}

//_____________________________________________________________________
void GEMclusterer::SetExpandDigit(const Int_t iRow, Int_t iPad, 
				  Int_t iTimeBin, const Float_t signal) 
{
  //
  // 
  //
  R__ASSERT(iRow>=0 && iRow<fRowsMax);
  R__ASSERT(iPad>=0 && iPad<=fPadsMax);
  R__ASSERT(iTimeBin>=fFirstTimeBin && iTimeBin<=fLastTimeBin);
  
  iTimeBin -= fFirstTimeBin;
  iPad     += 2;
  iTimeBin += 2;
  
  Int_t bin = iPad*(fTimeBinsMax+4)+iTimeBin;
  fAllBins[iRow][bin] = signal;
  fAllSigBins[iRow][fAllNSigBins[iRow]] = bin;
  fAllNSigBins[iRow]++;
}

//______________________________________________________________________________
Float_t GEMclusterer::GetQ(const Float_t* adcArray, const Int_t time, 
			   const Int_t pad, const Int_t maxTimeBins, 
			   Int_t& timeMin, Int_t& timeMax, 
			   Int_t& padMin,  Int_t& padMax) const
{
  //
  // This methods return the charge in the bin time+pad*maxTimeBins
  // If the charge is above 0 it also updates the padMin, padMax, timeMin
  // and timeMax if necessary
  //
  Float_t charge = adcArray[time + pad*maxTimeBins];
  if(charge > 0) {
    timeMin = TMath::Min(time, timeMin); timeMax = TMath::Max(time, timeMax);
    padMin = TMath::Min(pad, padMin); padMax = TMath::Max(pad, padMax);
  }
  return charge; 
}


//____________________________________________________________________________________________
void GEMclusterer::ResetData()
{
  //
  // reset all data
  //
  
  fEventCounter = 0;
  fClusterCounter = 0;
}

//_____________________________________________________________________
void GEMclusterer::DoCombineTracks()
{
  // Look for tracks with the larger number of clusters in each ROC, then combine them
  
  static GEMTrack* tracks[72];

  for (Int_t i = 0; i < 72; i++) tracks[i] = 0;

  Int_t ntracks = fGEMEvent->GetNumberOfTracks();
  
  for (Int_t i = 0; i < ntracks; i++) {
    GEMTrack* track = fGEMEvent->GetTrack(i);
    UChar_t roc = track->GetROC();
    Int_t nclusters = track->GetNumberOfClusters();
    if (!tracks[roc] || tracks[roc]->GetNumberOfClusters() < nclusters) {
      tracks[roc] = track;
    }
  }

  GEMCombinedTrack* newTrack = fGEMEvent->AddCombinedTrack(GEMCombinedTrack());
  
  for (Int_t i = 0; i < fNcombineROC; i++) {
    if (tracks[fCombineROC[i]] == 0) continue;
    //Printf("Number of tracks %d", newTrack->GetNumberOfSubTracks());
    newTrack->AddTrack(tracks[fCombineROC[i]]);
  }
}

//____________________________________________________________________________________________
void GEMclusterer::TrackFinder(Int_t roc, Int_t nTries, Bool_t reverse)
{
  //
  // actual track finder
  //
  //std::cout<<"Entered TrackFinder -- ROC "<<roc<<std::endl;  
  TClonesArray *pclusterArray = GetClusterArray(roc);
  if (!pclusterArray) return;
  TClonesArray &clusterArray=*pclusterArray;
  if (nTries<0) nTries=clusterArray.GetEntriesFast();

  // For use below
  Int_t searchIndexArraySize = 5000;
  
  Int_t nTracks = 0;
  if(clusterArray.GetEntriesFast()>=searchIndexArraySize) {
    printf("Very large event, no tracking will be performed!!");
    return;
  }

  if (fGEMEvent) fGEMEvent->SetNclusters(roc, clusterArray.GetEntriesFast());
  UInt_t nclustersUsed=0;

  if (clusterArray.GetEntriesFast() < fNMinClusters) return;

  //loop over the n (given bu nTries) clusters and use iFirst as starting point for tracking 
  for(Int_t iFirst = 0; (iFirst<nTries&&iFirst<clusterArray.GetEntriesFast()); iFirst++){
    //std::cout<<"ROC "<<roc<<", cluster seed "<<iFirst+1<<"/"<<clusterArray.GetEntriesFast()<<std::endl;

    Int_t nClusters = clusterArray.GetEntriesFast();
    Int_t searchIndexArray[searchIndexArraySize]; //array of cluster indices to be used in the tracking
    for(Int_t i = 0; i < searchIndexArraySize; i++){
      searchIndexArray[i] = -1;
    }
    Int_t freeClusters = 0; //no of clusters not already associated to a track

    for(Int_t iCluster = iFirst; iCluster<nClusters;iCluster++) {
      GEMCluster *cluster = 0;
      if (reverse) cluster =  (GEMCluster*) clusterArray.At(clusterArray.GetEntriesFast()-1-iCluster);
      else cluster =  (GEMCluster*) clusterArray.At(iCluster);
      if(cluster->GetTrackNr() ==0) {
	if (reverse) searchIndexArray[freeClusters] =clusterArray.GetEntriesFast()-1- iCluster;
	else searchIndexArray[freeClusters] = iCluster;  
	freeClusters++;
      }
    }
    if(freeClusters < fNMinClusters) continue;

    Int_t iStartCluster = -1;
    Int_t trackIndexArray[100];
    Int_t nClustersInTrackCand = 0;
  
    TGraph *trackCandidateGraph = new TGraph();
    TGraph *zGraph = new TGraph();

    
    for(Int_t i = 0; i<100;i++) trackIndexArray[i] = -1;
    
    iStartCluster = searchIndexArray[0];
    GEMCluster *startCluster = (GEMCluster*) clusterArray.At(iStartCluster);
    //Printf("Start Cluster (x,y) = (%.3f, %.3f), (row, pad) = (%d, %.3f)", startCluster->GetX(), startCluster->GetY(), startCluster->GetRow(), startCluster->GetPad());
    // GEMCluster *test = 0;
    //if(reverse) test =(GEMCluster*) clusterArray.At(clusterArray.GetEntriesFast()-1-iFirst);
    //else test = (GEMCluster*) clusterArray.At(iFirst);
    if(startCluster->GetTrackNr() != 0) continue;


//     Double_t oldX = startCluster->GetRow();
    Double_t oldY = startCluster->GetPad();
    Double_t oldZ = startCluster->GetTimeBinW();
    Int_t oldRow = Int_t(startCluster->GetRow());
    trackIndexArray[0] = iStartCluster;
    nClustersInTrackCand = 1;
    Int_t totalMisses = 0;
    Int_t currentSearchRow = oldRow + 1;

    trackCandidateGraph->SetPoint(0,startCluster->GetX(),startCluster->GetY());
    zGraph->SetPoint(0,startCluster->GetX(),startCluster->GetTimeBinW());
    Bool_t candidateFound = kFALSE;
    const Double_t searchWindow = fWindowPad;
    const Double_t timeWindow = fWindowTime;


    for(Int_t iCluster = 1; iCluster < freeClusters; iCluster++) {

      //std::cout<<"ROC "<<roc<<", current cluster "<<iCluster+1<<"/"<<freeClusters<<std::endl;  
      GEMCluster *currentCluster = (GEMCluster*) clusterArray.At(searchIndexArray[iCluster]);
      //Printf("Current Cluster (x,y) = (%.3f, %.3f), (row, pad) = (%d, %.3f)", currentCluster->GetX(), currentCluster->GetY(), currentCluster->GetRow(), currentCluster->GetPad());
    
      Int_t clusterRow = Int_t(currentCluster->GetRow());
      if(clusterRow == oldRow) continue;
      //if(clusterRow > currentSearchRow) {
      if(clusterRow != currentSearchRow) {
        totalMisses=totalMisses+(TMath::Abs(clusterRow-oldRow)-1);
        currentSearchRow = clusterRow;
      }
      // I'm not entirely sure if this is the right assignment, but it matched with above
      Double_t closest = (Double_t) searchIndexArraySize;
      Double_t zClosest = (Double_t) searchIndexArraySize;
      Int_t iClosest = -1;

      while (clusterRow == currentSearchRow && iCluster < freeClusters ) { //search currentSearchRow to find the cluster closest to the last accepted cluster


        Double_t currentZ = currentCluster -> GetTimeBinW();
        Double_t currentY = currentCluster -> GetPad();

        Double_t distance;
        distance = (TMath::Abs(oldY - currentY));

        Double_t zDistance = (TMath::Abs(oldZ - currentZ));

        if(distance < closest && zDistance<=timeWindow) {

          closest = distance;
          zClosest = zDistance;
          iClosest = iCluster;
        }
        if (searchIndexArray[iCluster+1]==-1) break;
        currentCluster = (GEMCluster*) clusterArray.At(searchIndexArray[iCluster+1]);
        //std::cout<<"ROC "<<roc<<", current cluster (inside while) "<<iCluster+1<<"/"<<freeClusters<<std::endl;  
        clusterRow = Int_t(currentCluster->GetRow());
        if (clusterRow == currentSearchRow) iCluster++;
      }

      Int_t miss = TMath::Abs(currentSearchRow-oldRow)-1;
      if(miss>0) totalMisses++;
      if(nClustersInTrackCand<6 && miss>1) break;//seems to lower risk for bad seed cluster being accepted
      if(miss>fNAllowedMisses)  break;


      //Now check if cluster is close enough
      if( closest <=searchWindow && zClosest <=timeWindow){
        trackIndexArray[nClustersInTrackCand] = searchIndexArray[iClosest];
        nClustersInTrackCand++;

        GEMCluster *acceptedCluster = (GEMCluster*) clusterArray.At(searchIndexArray[iClosest]);
        // 	oldX = acceptedCluster->GetRow();
        oldY = acceptedCluster->GetPad();
        oldZ = acceptedCluster->GetTimeBinW();;
        oldRow = Int_t(acceptedCluster->GetRow());
        trackCandidateGraph->SetPoint(trackCandidateGraph->GetN(),acceptedCluster->GetX(),acceptedCluster->GetY());
        //Printf("Adding cluster (x,y) = (%.3f, %.3f)", acceptedCluster->GetX(),acceptedCluster->GetY());
        zGraph->SetPoint(zGraph->GetN(),acceptedCluster->GetX(),acceptedCluster->GetTimeBinW());
      }

      if(reverse) currentSearchRow--;
      else currentSearchRow++;

      if(nClustersInTrackCand>=fNMinClusters) candidateFound = kTRUE;
    }

    if(candidateFound) {

      Double_t slope=0., offset=0., chi2=0., slopeError=0., offsetError=0.;
      Double_t slopeZ=0., offsetZ=0., chi2Z=0., slopeErrorZ=0., offsetErrorZ=0.;
      GetFit(trackCandidateGraph, slope, offset, slopeError, offsetError, chi2);
      GetFit(zGraph, slopeZ, offsetZ, slopeErrorZ, offsetErrorZ, chi2Z);
      GEMTrack *newTrack = 0x0;
      if (fGEMEvent) newTrack=fGEMEvent->AddTrack(GEMTrack(roc, offset, slope, offsetError, slopeError, chi2,offsetZ, slopeZ));

      for(Int_t i = 0; i<nClustersInTrackCand;i++) {


        GEMCluster *chosen = (GEMCluster*) clusterArray.At(trackIndexArray[i]);
        chosen->SetTrackNr(nTracks+1);


        if(fRunResUBiased){

          TGraph tempGraphXY(*trackCandidateGraph);
          TGraph tempGraphXZ(*zGraph);
          tempGraphXY.RemovePoint(i);
          tempGraphXZ.RemovePoint(i);

          Double_t slopeTemp=0., offsetTemp=0., chi2Temp=0., slopeErrorTemp=0., offsetErrorTemp=0.;
          Double_t slopeZTemp=0., offsetZTemp=0., chi2ZTemp=0., slopeErrorZTemp=0., offsetErrorZTemp=0.;

          GetFit(&tempGraphXY, slopeTemp, offsetTemp, slopeErrorTemp, offsetErrorTemp, chi2Temp);
          GetFit(&tempGraphXZ, slopeZTemp, offsetZTemp, slopeErrorZTemp, offsetErrorZTemp, chi2ZTemp);

          chosen->SetResidualYUnBiased(slopeTemp*chosen->GetX() + offsetTemp - chosen->GetY());

        }

        newTrack->AddCluster(*chosen);

        ++nclustersUsed;
      }
     
      nTracks++;
    }
    delete zGraph;
    delete trackCandidateGraph;
  }//end candidate loop

  if (fGEMEvent) {
    fGEMEvent->SetNclustersUsed(roc, nclustersUsed);
    fGEMEvent->SetNtracks(roc, nTracks);
  }
  //std::cout<<"Exiting TrackFinder, ROC "<<roc<<"..."<<std::endl;
}

//____________________________________________________________________________________________
void GEMclusterer::GetFit(TGraph *trackGraph, Double_t& slope, Double_t& offset, Double_t& slopeError, Double_t& offsetError, Double_t& chi2)
{
   TLinearFitter fitter(1,"pol1","D");
   fitter.AssignData(trackGraph->GetN(), 1, trackGraph->GetX(), trackGraph->GetY());
   fitter.Eval();//Robust(0.8); //at least 80% good points, return 0 if fit is ok
   fitter.Chisquare();
   chi2 = fitter.GetChisquare();
   offset = fitter.GetParameter(0);
   slope = fitter.GetParameter(1);
   slopeError = fitter.GetParError(1);
   offsetError = fitter.GetParError(0);
}

//____________________________________________________________________________________________
void GEMclusterer::Init(Int_t run)
{
  //
  // Do some initialization: init output files and load the TPC stream reader, if needed
  //

  TH1::AddDirectory(kFALSE);
  
  if (!fFile) {
    if (!fOutFileName.IsNull()){
      fFile = new TFile(fOutFileName.Data(),"recreate");
      fTree = new TTree("GEM","GEM event data");
      fGEMEvent = new GEMEvent;
      fTree->Branch("Events","GEMEvent",&fGEMEvent,1000000);
      if (fStoreBunches){
        fBunches = new TClonesArray("GEMBunch");
        fTree->Branch("Bunches","TClonesArray",&fBunches,1000000);
      }
    }
  }

  for (Int_t iroc = 0; iroc < 72; ++iroc) {
    if (GetClusterArray(iroc)) GetClusterArray(iroc)->Clear();
  }

  if (fGEMEvent && !fLogFile.IsNull() && !fLogData){
    TString s = gSystem->GetFromPipe(Form("cat %s",fLogFile.Data()));
    TObjArray* arr = s.Tokenize("\n");

    for (Int_t i = 0; i < arr->GetEntriesFast(); ++i) {
      TString s(arr->At(i)->GetName());
      if (!s.BeginsWith(Form("%d", run))) continue;
      fLogData = s.Tokenize(" \t");
      break;
    }
    if (!fLogData) {
      Printf("Error: run '%d' not found in log file '%s'", run, fLogFile.Data());
      fLogFile = "";
    }

    delete arr;
  }

  if (fLoadCalib && !fCalibFile.IsNull()) {
    TFile f(fCalibFile);
    if (f.IsOpen()) {
      for (Int_t roc = 0; roc < kNumberOfROC; roc++) {
        TObject* obj = 0;

        // Loading gain map
        TString gainMapName(Form("Run%d_ROC%d_PadGainMap", run, roc));
        obj = f.Get(gainMapName);
        gainMapName += "_copy";
        if (obj) fGainMap[roc] = dynamic_cast<TH2*>(obj->Clone(gainMapName));
        if (!fGainMap[roc]) {
          Printf("Warning: gain map for ROC %d not found in file '%s'", roc, fCalibFile.Data());
        }
        else {
          Printf("Info: using gain map for ROC %d from file '%s'", roc, fCalibFile.Data());
        }

        // Loading bad channel map
        TString badMapName(Form("Run%d_ROC%d_BadChannelMap", run, roc));
        obj = f.Get(badMapName);
        badMapName += "_copy";
        if (obj) fDeadChannelMap[roc] = dynamic_cast<TH2*>(obj->Clone(badMapName));
        if (!fDeadChannelMap[roc]) {
          Printf("Warning: bad channel map for ROC %d not found in file '%s'", roc, fCalibFile.Data());
        }
        else {
          Printf("Info: using bad channel map for ROC %d from file '%s'", roc, fCalibFile.Data());
        }
      }
      f.Close();
    } 
    else {
      Printf("Error: Could not open calib file '%s'", fCalibFile.Data());
      fCalibFile = "";
    }
  }
  fLoadCalib = kFALSE;

  // If black event, load appropriate pedestal map
  // pedestal files pedestals1603.root, pedestals1624.root should be in user's scratch/PedestalPlots
  if(fBlackEvent){
    Int_t pedRun;
    // set most recent pedestal
    if( run == 1623 ) pedRun = 1603;
    if( run == 1626 || run == 1629 || run == 1630 ) pedRun = 1624;
    TString pedFile = Form("%s/pedestals%d.root", fPedDir.Data(), pedRun);
    TFile f(pedFile);
    if(f.IsOpen()){
      // for each roc, open the file and get the pedestal histogram
      for (Int_t roc = 0; roc < kNumberOfROC; roc++) {
	TString pedName(Form("ROC%d_Mean_Pedestals_Run%d", roc, pedRun));
	TObject *obj = f.Get(pedName);
	gROOT->cd();
	if (obj) fPedestalMap[roc] = dynamic_cast<TH2*>(obj->Clone(pedName));
	if (!fPedestalMap[roc]) {
	  Printf("Warning: pedestal map for ROC %d not found in file '%s'", roc, pedFile.Data());
	}
	else {
	  //Printf("Info: using pedestal map for ROC %d from file '%s'", roc, pedFile.Data());
	}
      }
    }
    else {
      Printf("Error: Could not open pedestal file '%s'", pedFile.Data());
    }
  }

  // Apply basically the same as black events, but the pedestal is derived from black events
  if (fExtraPedestal)
  {
    TFile f(fExtraPedestalFile);
    if(f.IsOpen()){
      // for each roc, open the file and get the pedestal histogram
      for (Int_t roc = 0; roc < kNumberOfROC; roc++) {
	TString pedName(Form("ROC%d_Extra_Pedestals", roc));
	TObject *obj = f.Get(pedName);
	gROOT->cd();
	if (obj) fExtraPedestalMap[roc] = dynamic_cast<TH2*>(obj->Clone(pedName));
	if (!fExtraPedestalMap[roc]) {
	  Printf("Warning: extra pedestal map for ROC %d not found in file '%s'", roc, fExtraPedestalFile.Data());
	}
	else {
	  //Printf("Info: using extra pedestal map for ROC %d from file '%s'", roc, fExtraPedestalFile.Data());
	}
      }
    }
    else {
      Printf("Error: Could not open extra pedestal file '%s'", fExtraPedestalFile.Data());
    }

  }


  if (!fPressureValuesFile.IsNull() && !fPressureValues){
    TString pvalues = gSystem->GetFromPipe(Form("cat %s",fPressureValuesFile.Data()));
    TObjArray *arrPvals = pvalues.Tokenize("\n");
    if (!arrPvals || arrPvals->GetEntriesFast()==0 ){
      fPressureValuesFile="";
    } else {
      fPressureValues = new TGraph;
      // first line contains header
      for (Int_t i=1; i<arrPvals->GetEntriesFast(); ++i){
        TObjArray *arrTmp=((TObjString*)arrPvals->At(i))->String().Tokenize(" \t");
        if (arrTmp->GetEntriesFast()==2){
          const Double_t xVal = ((TObjString*)arrTmp->At(0))->String().Atof();
          const Double_t yVal = ((TObjString*)arrTmp->At(1))->String().Atof();
          fPressureValues->SetPoint(fPressureValues->GetN(), xVal, yVal);
        }
        delete arrTmp;
      }
    }
    delete arrPvals;
  }

  if (!fTPCRawStream) fTPCRawStream = new AliGEMRawStreamV3(fRawReaderGEM, (AliAltroMapping**)fMapping);
}

//____________________________________________________________________________________________
void GEMclusterer::CloseFile()
{
  //
  // close the current data file
  //
  
  if (fFile) {
    fFile->Write();
    fFile->Close();
    delete fFile;
    fFile=0x0;
  }

}

//________________________________________________________________________________
GEMBunch* GEMclusterer::AddBunch(UChar_t roc, UChar_t row, UChar_t pad, UShort_t length, Int_t *sig, UShort_t startTime)
{
  // Add a bunch

  if (!fBunches) return 0x0;

  if (fDeadChannelMap[roc]) {
    
    if ((fDeadChannelMap[roc]->GetBinContent(row+1, pad+1) == kDeadChannel && fExcludeDeadChannels) ||
        (fDeadChannelMap[roc]->GetBinContent(row+1, pad+1) == kWarmChannel && fExcludeWarmChannels) ||
        (fDeadChannelMap[roc]->GetBinContent(row+1, pad+1) == kHotChannel && fExcludeHotChannels)) {
      return 0x0;
    }
  }

  if (fBunches->GetEntriesFast() >= fBunches->Capacity()) fBunches->Expand(2 * fBunches->Capacity());
  TClonesArray &arr = *fBunches;
  GEMBunch* bunch = new(arr[fBunches->GetEntriesFast()]) GEMBunch(roc, row, pad, length, sig, startTime);
  
  if (fGainMap[roc]) {
    Float_t gainFactor = fGainMap[roc]->GetBinContent(row+1, pad+1);
    bunch->SetGainFactor(gainFactor);
  }
    
  return bunch;
}
