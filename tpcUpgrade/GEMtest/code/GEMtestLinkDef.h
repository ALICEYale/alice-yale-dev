#ifdef __CINT__
/* Copyright(c) 1998-1999, ALICE Experiment at CERN, All rights reserved. *
 * See cxx source for full Copyright notice                               */

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

#pragma link C++ class GEMclusterer+;
#pragma link C++ class AliGEMRawStreamV3+;
#pragma link C++ class AliRawReaderGEMBase+;
#pragma link C++ class AliRawReaderGEMDate+;
#pragma link C++ class AliRawReaderGEM+;
#pragma link C++ class AliTPCSimpleEventDisplay+;
