///////////////////////////////////////////////////////////////////////////////
///
/// This class provides access to TPC digits in raw data.
/// Version adapted for 2014 beam test of 2GEM+MMG.
///
/// It loops over all TPC digits in the raw data given by the AliRawReader.
/// The NextChannel method loads the data for the next pad. If there is no pad left
/// it returns kFALSE.
///
///////////////////////////////////////////////////////////////////////////////

#include <TSystem.h>

#include "AliGEMRawStreamV3.h"
#include "AliRawReader.h"
#include "AliLog.h"
#include "AliTPCAltroMapping.h"

ClassImp(AliGEMRawStreamV3)

//_____________________________________________________________________________
AliGEMRawStreamV3::AliGEMRawStreamV3(AliRawReader* rawReader, AliAltroMapping **mapping) :
AliTPCRawStreamV3(rawReader, mapping)
{
  // create an object to read TPC raw digits
}

//_____________________________________________________________________________
AliGEMRawStreamV3::~AliGEMRawStreamV3()
{
// destructor

  if (fIsMapOwner)
    for(Int_t i = 0; i < 6; i++) delete fMapping[i];
}

//_____________________________________________________________________________
Bool_t AliGEMRawStreamV3::NextDDL()
{
  // Take the DDL index,
  // calculate the patch number and
  // set the sector number
  fPrevSector = fSector;
  fSector     = -1;
  if (!AliAltroRawStreamV3::NextDDL()) return kFALSE;
  
  Int_t ddlNumber = GetDDLNumber();
  fSector = ddlNumber / 2;
  // Set ROC 0 to use patches 0,1 (since it has ddl=0,1), which are copied from aliroot default (see TPC/mapping)
  if(fSector == 0) fPatchIndex = ddlNumber%2;
  // Set ROCs 1,2 to use patches 2,3 (PlanB mappings copied to these) 
  if(fSector == 1 || fSector == 2) fPatchIndex = fSector + 1;

  //Printf("DDL = %d, sector = %d, patch = %d", ddlNumber, fSector, fPatchIndex);

  return kTRUE;
}
//_____________________________________________________________________________
void AliGEMRawStreamV3::ApplyAltroMapping()
{
  Int_t ddlNumber = GetDDLNumber();

  fSector = ddlNumber / 2;
  Int_t patchIndex = fSector;
 
  Short_t hwAddress = GetHWAddress();
  fRow = fMapping[patchIndex]->GetPadRow(hwAddress);
  fPad = fMapping[patchIndex]->GetPad(hwAddress);
}
