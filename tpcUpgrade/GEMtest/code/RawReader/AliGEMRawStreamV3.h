#ifndef ALIGEMRAWSTREAMV3_H
#define ALIGEMRAWSTREAMV3_H

///////////////////////////////////////////////////////////////////////////////
///
/// This class provides access to TPC digits in raw data (version adapted for 2014 beam test of 2GEM+MMG).
///
///////////////////////////////////////////////////////////////////////////////

#include <AliAltroRawStreamV3.h>
#include <AliTPCRawStreamV3.h>

class AliRawReader;
class AliAltroMapping;
class AliTPCRawStreamV3;

class AliGEMRawStreamV3: public AliTPCRawStreamV3 {
  public :
    AliGEMRawStreamV3(AliRawReader* rawReader, AliAltroMapping **mapping = NULL);
    virtual ~AliGEMRawStreamV3();

    virtual Bool_t           NextDDL();
  
  protected :
    AliGEMRawStreamV3& operator = (const AliGEMRawStreamV3& stream);
    AliGEMRawStreamV3(const AliGEMRawStreamV3& stream);

    virtual void ApplyAltroMapping();

    ClassDef(AliGEMRawStreamV3, 0)    // base class for reading TPC raw digits using the fast algorithm
};

#endif
