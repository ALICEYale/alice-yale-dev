/*


.L AliRawReaderGEM.cxx+
fRawReader=new AliRawReaderGEM("merge")
fRawReader->NextEvent()
fRawReader->DumpHeader()


AliLog::SetGlobalLogLevel(10)
.L AliRawReaderGEM.cxx+g
fRawReader=new AliRawReaderGEM("singleEvent.orig")
AliTPCRawStreamV3 r(fRawReader)
fRawReader->NextEvent()
r.NextDDL()


.L AliRawReaderGEM.cxx+g
.L /data/Work/software/aliroot/mycode/AliTPCSimpleEventDisplay/AliTPCSimpleEventDisplay.cxx+g
.L /data/Work/software/aliroot/mycode/AliTPCSimpleEventDisplay/TestSimpleEvDisp.C+g
fRawReader=new AliRawReaderGEM("merge")
fEvDisp.SetRawReader(fRawReader);
Init()
Next()

*/

#define long32 int
#define long64 long long
#define ALI_DATE

//standard includes
#include <stdio.h>
#include <stdlib.h>

//ROOT includes

//AliRoot includes
#include <Rtypes.h>
#include <event.h>
#include <AliRawDataHeader.h>

#include "AliRawReaderGEM.h"

AliRawReaderGEM::AliRawReaderGEM(const char* fileName, Int_t /*eventNumber*/ /*= -1*/) :
  AliRawReaderGEMBase(),
  AliRawReader(),
  fFile(0x0),
  fEquipment(0x0),
  fCDH(0x0),
  fData(0x0),
  fPosition(0x0),
  fEnd(0x0),
  fCount(0),
  fCurrentData(0),
  fRunNumber(0)
{
  //ctor
  fFile = fopen(fileName,"r");
  if (!fFile){
    printf("Error: Could not open file '%s'\n",fileName);
  }
  fHeader=new AliRawDataHeader;
  //RCU fw 2 workaround
  fHeader->SetAttribute(1);

  for (Int_t i=0; i<kNMaxEquipment; ++i){
    fEquipmentPtrs[i]=0x0;
    fCDHPtrs[i]=0x0;
    fDataPtrs[i]=0x0;
    fPositionPtrs[i]=0x0;
    fEndPtrs[i]=0x0;
  }

  TString runNr(fileName);
  runNr.Remove(0,runNr.Last('_')+1);
  runNr.ReplaceAll(".txt","");

  fRunNumber=runNr.Atoi();
}

//____________________________________________________________________
AliRawReaderGEM::~AliRawReaderGEM()
{
  // dtor

//   delete [] fData;
  for (Int_t i=0;i<kNMaxEquipment;++i){
    delete [] fDataPtrs[i];
  }
  delete fHeader;
}
//____________________________________________________________________
Bool_t AliRawReaderGEM::NextEvent()
{
  // read the next event
  if (!FindNextEvent()) return kFALSE;
  ++fEventInFile;
  LoadEventData();
  return kTRUE;
}

//____________________________________________________________________
Bool_t AliRawReaderGEM::LoadEventData()
{
  // load DAQ and CAMAC data
  if (!(LoadAllDaqData() && ReadCamacData())) {
    ResetData();
    return kFALSE;
  }
  return kTRUE;
}

//____________________________________________________________________
Bool_t AliRawReaderGEM::GotoEvent(Int_t event)
{
  //
  // Jump to an event number in file
  //

  Bool_t valid=kFALSE;
  while ( (valid=FindNextEvent()) ) {
    if (Int_t(fEventFromTag)==event) break;
    ++fEventInFile;
  }
  if (!valid){
    printf("Event %d not found\n",event);
    return kFALSE;
  }
  ++fEventInFile;
  LoadEventData();
  
  return kTRUE;
}

//____________________________________________________________________
Bool_t AliRawReaderGEM::Reset()
{
  // reset current event
//  if (!fData) return kFALSE;
//  fPosition=fData+(7+8)*4;
  fCurrentData=0;
  return kTRUE;
}
//____________________________________________________________________
Bool_t AliRawReaderGEM::RewindEvents()
{
  // reset reader
  if (!fFile) return kFALSE;
  fseek(fFile, 0, SEEK_SET);

  fEventInFile=0;
  
  return kTRUE;
}
//____________________________________________________________________
Int_t AliRawReaderGEM::GetEquipmentSize() const
{
  // get the size of the equipment (including the header)
  
  #ifdef ALI_DATE
  if (!fEquipment) return -1;
  return fEquipment->equipmentSize;
  #else
  return 0;
  #endif
}
//____________________________________________________________________
Int_t AliRawReaderGEM::GetEquipmentSize(Int_t ieq) const
{
  // get the size of the equipment (including the header)

  if (ieq<0||ieq>kNMaxEquipment) return 0;
  #ifdef ALI_DATE
  if (!fEquipmentPtrs[ieq]) return 0;
  return fEquipmentPtrs[ieq]->equipmentSize;
  #else
  return 0;
  #endif
}

//____________________________________________________________________
Int_t AliRawReaderGEM::GetEquipmentType() const
{
  // get the type from the equipment header
  
  #ifdef ALI_DATE
  if (!fEquipment) return -1;
  return fEquipment->equipmentType;
  #else
  return 0;
  #endif
}

//____________________________________________________________________
Int_t AliRawReaderGEM::GetEquipmentId() const
{
  // get the ID from the equipment header
  
  #ifdef ALI_DATE
  if (!fEquipment) return -1;
  return fEquipment->equipmentId;
  #else
  return 0;
  #endif
}

//____________________________________________________________________
const UInt_t* AliRawReaderGEM::GetEquipmentAttributes() const
{
  // get the attributes from the equipment header
  
  #ifdef ALI_DATE
  if (!fEquipment) return NULL;
  return fEquipment->equipmentTypeAttribute;
  #else
  return 0;
  #endif
}

//____________________________________________________________________
Int_t AliRawReaderGEM::GetEquipmentElementSize() const
{
  // get the basic element size from the equipment header
  
  #ifdef ALI_DATE
  if (!fEquipment) return 0;
  return fEquipment->equipmentBasicElementSize;
  #else
  return 0;
  #endif
}

//____________________________________________________________________
Int_t AliRawReaderGEM::GetEquipmentHeaderSize() const
{
  // Get the equipment header size
  // 28 bytes by default
  #ifdef ALI_DATE
  return sizeof(equipmentHeaderStruct);
  #else
  return 0;
  #endif
}

//____________________________________________________________________
Bool_t AliRawReaderGEM::FindNextEvent()
{
  if (!fFile) return kFALSE;
  char str[255];
  while (fgets(str,255,fFile)){
//     printf("%s,\n",str);
    //new event starts with a'='
    TString line(str);
    if (line.BeginsWith("Event")){
      //go back to the hex number 8 char + cr = 9
//       fseek(fFile, -9 , SEEK_CUR);
      line.ReplaceAll("Event","");
      fEventFromTag=(UInt_t)line.Atoi();
      return kTRUE;
    }
  }
  printf("No next event found\n");
  return kFALSE;
}

//____________________________________________________________________
void AliRawReaderGEM::ResetData()
{
  // reset the daq data
  
  for (Int_t i=0;i<kNMaxEquipment;++i){
    delete [] fDataPtrs[i];
    fDataPtrs[i]=0x0;
    fEquipmentPtrs[i]=0x0;
    fCDHPtrs[i]=0x0;
    fEndPtrs[i]=0x0;
    fPositionPtrs[i]=0x0;
  }
  fData=0x0;
  fCurrentData=0;
  fEnd=0x0;
  fPosition=0x0;

  for (Int_t i=0; i<kMaxCAMAC; ++i) fCamacData[i]=0;
  fNCamacData=0;
}

//____________________________________________________________________
Bool_t AliRawReaderGEM::LoadAllDaqData()
{
  //
  // We have up to 2 DDLs try to read them
  //

  ResetData();

  Bool_t lastEqOk=kFALSE;
  while (!(lastEqOk=IsLastEquipment()) && fCurrentData<kNMaxEquipment) {
//    printf("Load DAQ data %d (%d)\n",fCurrentData,lastEqOk);
    ReadDaqData();
    ++fCurrentData;
  }
  
  if (!lastEqOk) {
    printf("Event %d: DATE end of event not found\n",fEventFromTag);
    return kFALSE;
  }

  fCurrentData=0;
  
  return kTRUE;
}

//____________________________________________________________________
Bool_t AliRawReaderGEM::ReadDaqData()
{
  // read the daq part of the data
  // this assumes that the file strem is already in the correct position.

  fEquipment = 0x0;
  fCDH       = 0x0;
//   if (fData)      delete [] fData;
  
  UInt_t eqHeadData[7]={0};
  long pos1,pos2;

  pos1=ftell(fFile);
  //read equipment header 7*8 byte
  for (Int_t i=0; i<7; ++i) {
    fscanf(fFile,"%x",&(eqHeadData[i]));
  }
  pos2=ftell(fFile);
  
  fseek(fFile, -(long)(pos2-pos1), SEEK_CUR);
  // set preliminary header data
  fEquipment=(equipmentHeaderStruct*)eqHeadData;
//   printf("diff: %ld, %ld, %d\n",-(long)(pos2-pos1),ftell(fFile), GetEquipmentSize());
  
  //get data size
  const Int_t dataLength=GetEquipmentSize();
  const Int_t n32bitwords=dataLength/4;
  
  //create data buffer
  fData=new UChar_t[dataLength];
  fPosition=fData;
  //read the data
  UInt_t data=0;
  for (Int_t iData=0; iData<n32bitwords; ++iData) {
    fscanf(fFile,"%x",&data);
    // convert 32 bit to 8 bit words
    fData[iData*4+3]=(data >> 24) & 0xff;
    fData[iData*4+2]=(data >> 16) & 0xff;
    fData[iData*4+1]=(data >>  8) & 0xff;
    fData[iData*4+0]=(data >>  0) & 0xff;
  }

  // assign headers
  fEquipment=(equipmentHeaderStruct*)fPosition;
  fPosition+=7*4;
  fCDH      =(commonDataHeaderStruct*)fPosition;
  fPosition+=8*4;

  fEnd=fPosition+GetEquipmentElementSize();

  fDataPtrs[fCurrentData]=fData;
  fEquipmentPtrs[fCurrentData]=fEquipment;
  fCDHPtrs[fCurrentData]=fCDH;
  fPositionPtrs[fCurrentData]=fPosition;
  fEndPtrs[fCurrentData]=fEnd;
  
  return kTRUE;
}

//____________________________________________________________________
Bool_t AliRawReaderGEM::ReadCamacData()
{
  // read camac part of the event

  //rest data
  fNCamacData=0;
  memset(fCamacData,0,kMaxCAMAC*sizeof(fCamacData[0]));

  // read all camac values
  char str[255];
  Int_t i=0;
  for (; i<kMaxCAMAC; ++i){
    fgets(str,255,fFile);
    Int_t len = strlen(str);
    if( str[len-1] == '\n' ) str[len-1] = '\0';
    // printf("%s\n",str);
    if (!strcmp(str, "... CAMAC event decoded ")) break;
    fCamacData[fNCamacData++]=atoi(str);
  }
  if (i==kMaxCAMAC){
    printf("Event %d: CAMAC end of event not found\n",fEventFromTag);
    return kFALSE;
  }
  if (fNCamacData<=1){
    printf("Event %d: CMAC data not complete\n",fEventFromTag);
    return kFALSE;
  }
  
  return kTRUE;
}

//____________________________________________________________________
Bool_t AliRawReaderGEM::ReadNextData(UChar_t*& data)
{
  //get data
  
  if (fCurrentData>=kNMaxEquipment) return kFALSE;
  fData=fDataPtrs[fCurrentData];
  fEnd=fEndPtrs[fCurrentData];
  fPosition=fPositionPtrs[fCurrentData];
  fCDH=fCDHPtrs[fCurrentData];
  fEquipment=fEquipmentPtrs[fCurrentData];
  
  if (fPosition>=fEnd) return kFALSE;
  data=fPosition;
  fPosition=fEnd;

  ++fCurrentData;
  
  return kTRUE;
}

//____________________________________________________________________
Bool_t AliRawReaderGEM::ReadNext(UChar_t* /*data*/, Int_t /*size*/)
{
  // next data size
  return kFALSE;  
}


//____________________________________________________________________
void AliRawReaderGEM::DumpHeader() const
{
  // dump headers

  equipmentHeaderStruct *eq=fEquipment;
  commonDataHeaderStruct *cdh=fCDH;
  if (eq){
    printf("Equipment Header Info:\n");
    printf("equipmentSize: %u\n",eq->equipmentSize);
    printf("equipmentType: %u\n",eq->equipmentType);
    printf("equipmentId: %u\n",eq->equipmentId);
    printf("equipmentTypeAttribute: %u\n",*eq->equipmentTypeAttribute);
    printf("equipmentBasicElementSize: %u\n",eq->equipmentBasicElementSize);
    printf("\n");
  }
  if (cdh){
    printf("CDH info:\n");
    printf("cdhBlockLength: %u\n",cdh->cdhBlockLength);
    printf("cdhEventId1: %u\n",cdh->cdhEventId1);
    printf("cdhMBZ1: %u\n",cdh->cdhMBZ1);
    printf("cdhL1TriggerMessage: %u\n",cdh->cdhL1TriggerMessage);
    printf("cdhMBZ0: %u\n",cdh->cdhMBZ0);
    printf("cdhVersion: %u\n",cdh->cdhVersion);
    printf("cdhEventId2: %u\n",cdh->cdhEventId2);
    //printf("cdhMBZ2: %u\n",cdh->cdhMBZ2);
    printf("cdhParticipatingSubDetectors: %u\n",cdh->cdhParticipatingSubDetectors);
    printf("cdhBlockAttributes: %u\n",cdh->cdhBlockAttributes);
    printf("cdhMiniEventId: %u\n",cdh->cdhMiniEventId);
    printf("cdhStatusErrorBits: %u\n",cdh->cdhStatusErrorBits);
//     printf("cdhMBZ3: %u\n",cdh->cdhMBZ3);
    printf("cdhTriggerClassesLow: %u\n",cdh->cdhTriggerClassesLow);
    printf("cdhTriggerClassesHigh: %u\n",cdh->cdhTriggerClassesHigh);
    printf("cdhMBZ4: %u\n",cdh->cdhMBZ4);
    printf("cdhRoiLow: %u\n",cdh->cdhRoiLow);
    printf("cdhRoiHigh: %u\n",cdh->cdhRoiHigh);    
  }
}

//____________________________________________________________________
Bool_t AliRawReaderGEM::IsLastEquipment()
{
  //
  //
  //
  long pos1,pos2;
  char str[255];
  //while eol reading
  pos1=ftell(fFile);
  while (1) {
    fgets(str,255,fFile);
    if (str[0]!='\n') break;
  }
  Int_t len = strlen(str);
  if( str[len-1] == '\n' ) str[len-1] = '\0';
  TString line(str);
  if (!line.BeginsWith("DATE Event decoded ...")) {
    pos2=ftell(fFile);
    fseek(fFile, -(long)(pos2-pos1), SEEK_CUR);
    
    return kFALSE;
  }
  return kTRUE;
}

