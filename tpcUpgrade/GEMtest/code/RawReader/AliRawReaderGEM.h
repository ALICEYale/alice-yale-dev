#ifndef ALIRAWREADERGEM_H
#define ALIRAWREADERGEM_H

#include <AliRawReader.h>

#include "AliRawReaderGEMBase.h"

struct equipmentHeaderStruct;
struct commonDataHeaderStruct;

class AliRawReaderGEM : public AliRawReaderGEMBase, public AliRawReader {
public:
  AliRawReaderGEM(const char* fileName, Int_t eventNumber = -1);
  virtual ~AliRawReaderGEM();

  UInt_t   GetRunNumber() const         {return fRunNumber;}
  const UInt_t* GetEventId() const      {return 0;}
  Int_t    GetEventIndex() const        {return fEventInFile;}
  
  UInt_t   GetType() const              {return 0;}
  const UInt_t* GetTriggerPattern()     const {return 0;}
  const UInt_t* GetDetectorPattern()    const {return 0;}
  const UInt_t* GetAttributes()         const {return 0;}
  const UInt_t* GetSubEventAttributes() const {return 0;}
  UInt_t   GetLDCId()                   const {return 768;}
  UInt_t   GetGDCId()                   const {return 0;}

  UInt_t   GetTimestamp() const        {return 0;}
  
  Int_t    GetEquipmentSize() const ;
  Int_t    GetEquipmentSize(Int_t ieq) const ;
  Int_t    GetEquipmentType() const ;
  Int_t    GetEquipmentId() const ;
  const UInt_t* GetEquipmentAttributes() const;
  Int_t    GetEquipmentElementSize() const;
  Int_t    GetEquipmentHeaderSize() const;
  
  Bool_t   ReadHeader(){return 0;}
  Bool_t   ReadNextData(UChar_t*& data);
  Bool_t   ReadNext(UChar_t* data, Int_t size);
  
  Bool_t   Reset();
  
  Bool_t   NextEvent() ;
  Bool_t   GotoEvent(Int_t event);
  Bool_t   RewindEvents();

  const UInt_t* GetCamacData()     const { return fCamacData; }
  UInt_t GetCamacData(UInt_t data) const { return (data<fNCamacData)?fCamacData[data]:0; }
  UInt_t GetNCamacData()           const { return fNCamacData; }

  void DumpHeader() const;

private:
  enum { kNMaxEquipment=2 };
  
  FILE                   *fFile;           // data file
  equipmentHeaderStruct  *fEquipment;      // equipment header of current event
  commonDataHeaderStruct *fCDH;            // CDH of current event
  UChar_t                *fData;           // data pointer
  UChar_t                *fPosition;       // current position in the raw data
  UChar_t                *fEnd;            // end position of the current data block
  UInt_t                  fCount;          // read data
  UInt_t                  fCamacData[kMaxCAMAC]; //camac values
  UInt_t                  fNCamacData;     // number of camac values

  equipmentHeaderStruct  *fEquipmentPtrs[kNMaxEquipment];      // equipment header of current event
  commonDataHeaderStruct *fCDHPtrs[kNMaxEquipment];            // CDH of current event
  UChar_t                *fDataPtrs[kNMaxEquipment];           // data pointer
  UChar_t                *fPositionPtrs[kNMaxEquipment];       // data pointer
  UChar_t                *fEndPtrs[kNMaxEquipment];            // data pointer
  
  UInt_t                  fCurrentData;           // current DDL number
  UInt_t                  fRunNumber;             // run number

  Bool_t FindNextEvent();
  Bool_t LoadEventData();
  Bool_t LoadAllDaqData();
  Bool_t ReadCamacData();
  Bool_t ReadDaqData();
  void   ResetData();

  Bool_t IsLastEquipment();
  
  ClassDef(AliRawReaderGEM,0);      // Raw data reader for GEM test 2012  
};

#endif

