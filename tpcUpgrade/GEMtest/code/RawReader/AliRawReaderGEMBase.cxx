#define long32 int
#define long64 long long
#define ALI_DATE

//standard includes
#include <stdio.h>
#include <stdlib.h>

//ROOT includes
#include <TString.h>

//AliRoot includes
#include <Rtypes.h>
#include <event.h>
#include <AliRawDataHeader.h>

#include "AliRawReaderGEMBase.h"

AliRawReaderGEMBase::AliRawReaderGEMBase() :
//   AliRawReaderDate((void*)0x0),
  fEventInFile(0),
  fEventFromTag(0),
  fNCamacData(0)
{
  memset(fCamacData,0,kMaxCAMAC*sizeof(fCamacData[0]));
}

//____________________________________________________________________
const char* AliRawReaderGEMBase::GetCamacDataName(UInt_t data)
{
  //
  // name of camac data
  //
  
  switch(data) {
    case 0: return "Event_Number";
    case kCamacPbGlass:          return "Pb_Glass";
    case kCamacCherenkov:        return "Cherenkov";
    case kCamacTemperature:      return "Temperature";
    case kCamacRelativeHumidity: return "Relative_Humidity";
    case kCamacPressure:         return "Pressure";
  }
  return Form("%u",data);
}

