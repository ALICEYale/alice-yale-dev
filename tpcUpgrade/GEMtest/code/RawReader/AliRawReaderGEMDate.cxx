#define long32 int
#define long64 long long
#define ALI_DATE

//standard includes
#include <fstream>

//ROOT includes
#include <Riostream.h>
#include <TString.h>
#include <TObjString.h>
#include <TObjArray.h>
#include <TSystem.h>

#include "AliRawReaderGEMDate.h"

AliRawReaderGEMDate::AliRawReaderGEMDate(const char* fileName, Int_t eventNumber /*= -1*/) :
  AliRawReaderGEMBase(),
  AliRawReaderDate(fileName,eventNumber),
  fArrCamacData(0x0)
{
  //ctor

  TString dirname(gSystem->DirName(fileName));
  TString basename(gSystem->BaseName(fileName));
  basename.ReplaceAll("data","CAMAC_");
  basename.ReplaceAll(".raw",".txt");
  TString camacFile(dirname);
  camacFile+="/";
  camacFile+=basename;
  ifstream istr(camacFile.Data());
  TString camacData;
  camacData.ReadFile(istr);
  fArrCamacData = camacData.Tokenize("\n");
  Printf("AliRawReaderGEMDate: Using files '%s' '%s'", fileName, camacFile.Data());
  Printf("Entries in CAMAC file: %d", fArrCamacData->GetEntriesFast());
}

//____________________________________________________________________
AliRawReaderGEMDate::~AliRawReaderGEMDate()
{
  // dtor
  delete fArrCamacData;
}

//____________________________________________________________________
Bool_t AliRawReaderGEMDate::NextEvent()
{
  Bool_t date = kFALSE;
  //skip non physics events (Start of data, start of run, ...)
  do {
    date = AliRawReaderDate::NextEvent();
    //Printf("type: %u", AliRawReaderDate::GetType());
  } while (date && (AliRawReaderDate::GetType() != 7));
  if (!date) return kFALSE;

  Bool_t camac = SetCamacData();
  //Printf("Processing: %d, %d, %4.f", fEventInFile, fEventNumber, fCamacData[kCamacEvent]);
  if (!camac) return kFALSE;

  ++fEventInFile;
  // read the next event
  return kTRUE;
}

//____________________________________________________________________
Bool_t AliRawReaderGEMDate::GotoEvent(Int_t event)
{
  Bool_t date = AliRawReaderDate::GotoEvent(event);
  fEventInFile = event;
  Bool_t camac = SetCamacData();
  if (!date || !camac) return kFALSE;
  ++fEventInFile;
  // read the next event
  return kTRUE;
}

//____________________________________________________________________
Bool_t AliRawReaderGEMDate::SetCamacData()
{
  // set camacData
  
  memset(fCamacData, 0, kMaxCAMAC * sizeof(fCamacData[0]));
  
  if (!fArrCamacData || !fArrCamacData->At(fEventInFile)) return kFALSE;
  TString &camacData = ((TObjString*)fArrCamacData->At(fEventInFile))->String();
  TObjArray *arrData = camacData.Tokenize(",");
  const UInt_t ncamac = arrData->GetEntriesFast();
  if (ncamac <= 1){
    Printf("Event %d: CAMAC data not complete", fEventInFile);
    fNCamacData = 0;
    delete arrData;
    return kFALSE;
  }
  
  if (fNCamacData > 0 && (fNCamacData != ncamac)){
    printf("Event %d: CAMAC data number of values has changed!", fEventInFile);
    fNCamacData = 0;
    delete arrData;
    return kFALSE;
  }
  
  for (UInt_t icamac = 0; icamac < ncamac; ++icamac) {
    const TString &data = ((TObjString*)arrData->UncheckedAt(icamac))->String();
    fCamacData[icamac] = data.Atof();
  }
  
  fEventFromTag = (UInt_t)fCamacData[kCamacEvent];
  
  if (fEventFromTag != fEventInFile + 1) {
    Printf("Event %d: Mismatch betwen camac event number (%d) and event number (%d)", fEventInFile, fEventFromTag, fEventInFile + 1);
  }
  
  fNCamacData = ncamac;
  delete arrData;
  
  return kTRUE;
}

//____________________________________________________________________
Bool_t AliRawReaderGEMDate::RewindEvents()
{
  //
  // rewind raw date events and reset internal event counter
  //
  fEventInFile = 0;
  return AliRawReaderDate::RewindEvents();
}
