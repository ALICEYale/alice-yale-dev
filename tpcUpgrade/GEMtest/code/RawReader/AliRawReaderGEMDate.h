#ifndef ALIRAWREADERGEMDATE_H
#define ALIRAWREADERGEMDATE_H

#include <AliRawReaderDate.h>

#include "AliRawReaderGEMBase.h"

class TObjArray;

class AliRawReaderGEMDate : public AliRawReaderGEMBase, public AliRawReaderDate {
public:
  AliRawReaderGEMDate(const char* fileName, Int_t eventNumber = -1);
  virtual ~AliRawReaderGEMDate();

  UInt_t   GetLDCId()                   const {return 768;}
  UInt_t   GetGDCId()                   const {return 0;}
  
  Int_t    GetEventIndex() const        {return fEventInFile;}

  Bool_t   NextEvent() ;
  Bool_t   GotoEvent(Int_t event);
  Bool_t   RewindEvents();

private:
  TObjArray *fArrCamacData;             // array with camac data

  Bool_t SetCamacData();
  
  ClassDef(AliRawReaderGEMDate,0);      // Raw data reader for GEM test 2012  
};

#endif

