void CopyGainMap(Int_t nruns, Int_t* runs, TFile* gainMapFile, TFile* calibFile, Bool_t clusterBased);
void CopyBadChannelMap(Int_t nruns, Int_t* runs, TFile* badChannelFile, TFile* calibFile);

#define MAXROC 2;

void GenerateCalibFile(Bool_t doBadChannels = kTRUE, Bool_t doGainMaps = kTRUE, Bool_t clusterBased = kTRUE, const char* suffix="")
{
  TH1::AddDirectory(kFALSE);
  
  gROOT->LoadMacro("loadlibs.C");
  
  loadlibs("../..");

  const Int_t nruns = 2;
  Int_t runs[2] = { 1645, 1646 };
  
  TString str1;
  TString str2;
  if (!clusterBased) {
    str1 = "Digits";
  }
  else {
    str1 = "Tracks";
  }
  if (doBadChannels) {
    str2 = "BadChannelsOnly";
  }
  else {
    str2 = "NoGainMap";
  }
  TString str(Form("%s_%s_output/plots.root", str1.Data(), str2.Data()));
  TString gainMapLocation(Form(configurationParameters::fAnalysisOutputLocation.Data(), str.Data()));
  
  TString badChannelLocation(Form("../Analysis/BadChannelMaps%s.root",suffix));
  TString calibLocation(Form(configurationParameters::fAnalysisOutputLocation.Data(), "CalibFiles/"));
  gSystem->mkdir(calibLocation, kTRUE);

  if (doBadChannels) {
    if (doGainMaps) {
      if (clusterBased) {
        calibLocation += "ClusterBased";
      }
      else {
        calibLocation += "PadBased";
      }
    }
    else {
      calibLocation += "BadChannelsOnly";
    }
  }
  else {
    if (clusterBased) {
      calibLocation += "ClusterBasedGainMapOnly";
    }
    else {
      calibLocation += "PadBasedGainMapOnly";
    }
  }

  calibLocation += suffix;
  calibLocation += ".root";

  TFile* gainMapFile = 0;
  TFile* badChannelFile = 0;

  Printf("Calib file will be stored in %s", calibLocation.Data());
  TFile* calibFile = TFile::Open(calibLocation, "recreate");
  
  if (doGainMaps) {
    Printf("Opening gain maps %s", gainMapLocation.Data());
    gainMapFile = TFile::Open(gainMapLocation);
    CopyGainMap(nruns, runs, gainMapFile, calibFile, clusterBased);
  }

  if (doBadChannels) {
    Printf("Opening bad channel maps %s", badChannelLocation.Data());
    badChannelFile = TFile::Open(badChannelLocation);
    CopyBadChannelMap(nruns, runs, badChannelFile, calibFile);
  }

  calibFile->Close();
  delete calibFile;
  calibFile = 0;
  
  if (badChannelFile) {
    badChannelFile->Close();
    delete badChannelFile;
    badChannelFile = 0;
  }
  
  if (gainMapFile) {
    gainMapFile->Close();
    delete gainMapFile;
    gainMapFile = 0;
  }
}

void CopyGainMap(Int_t nruns, Int_t* runs, TFile* gainMapFile, TFile* calibFile, Bool_t clusterBased)
{
  for (Int_t irun = 0; irun < nruns; irun++) {
    for (Int_t roc = 0; roc <= MAXROC; roc++) {
      TString histName;
      if (!clusterBased) {
        histName = Form("Run%d_ROC%d_PadGainMap", runs[irun], roc);
      }
      else {
        histName = Form("Run%d_ROC%d_PadGainMapClusterBased", runs[irun], roc);
      }
      
      TH2* map = dynamic_cast<TH2*>(gainMapFile->Get(histName));
      if (!map) continue;

      TString histName_copy(Form("Run%d_ROC%d_PadGainMap", runs[irun], roc));
      TH2* map_copy = static_cast<TH2*>(map->Clone(histName_copy));
      
      calibFile->cd();
      map_copy->Write();
    }
  }
}

void CopyBadChannelMap(Int_t nruns, Int_t* runs, TFile* badChannelFile, TFile* calibFile)
{
  if (!badChannelFile || badChannelFile->IsZombie()) {
    Printf("Could not read bad channel map file!");
  }
  for (Int_t roc = 0; roc <= MAXROC; roc++) {
    TString histName(Form("ROC%d_BadChannelMap", roc));
    TH2* map = dynamic_cast<TH2*>(badChannelFile->Get(histName));
    if (!map) {
      Printf("Could not load histogram %s!", histName.Data());
      continue;
    }
      
    for (Int_t irun = 0; irun < nruns; irun++) {
      TString copyName(Form("Run%d_%s", runs[irun], histName.Data()));
      TH2* copyHist = static_cast<TH2*>(map->Clone(copyName));

      calibFile->cd();
      Printf("Writing histogram %s...", copyHist->GetName());
      copyHist->Write();
    }
  }
}
