// RunCorrelatedNoise.C
// Run CorrelatedNoise.cxx easily

void RunCorrelatedNoise(UChar_t roc=2)
{
	// Load loadLibs
	gROOT->LoadMacro("loadlibs.C");

	loadlibs("../..");

	// Load GEMBeamTestPlotter for AcceptTrack() function
	gROOT->LoadMacro("../Analysis/GEMBeamTestPlotter.cxx+");

	// Load CorrelatedNoise
	gROOT->LoadMacro("../Analysis/CorrelatedNoise.cxx+");

	// Gathers files to process
	TString fBasedir(configurationParameters::fTracksLocation);

	TString s=gSystem->GetFromPipe(Form("ls %s/*.root",fBasedir.Data()));
	TObjArray *arr=s.Tokenize("\n");

	// Selects the GEM tree, which is the one that we want!
	TChain * t = new TChain("GEM");
	for (Int_t ifile=0; ifile < arr->GetEntriesFast(); ++ifile)
	{
		t->AddFile(arr->At(ifile)->GetName());
	}

	CorrelatedNoise(t);
	
	delete t;
}
