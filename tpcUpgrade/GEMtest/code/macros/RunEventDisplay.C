// RunEventDisplay.C
// Run EventDisplay.cxx easily

void RunEventDisplay()
{
	// Load loadLibs
	gROOT->LoadMacro("loadlibs.C");

	loadlibs("../..");

	// Load GEMBeamTestPlotter for AcceptTrack() function
	gROOT->LoadMacro("../Analysis/GEMBeamTestPlotter.cxx+");

	// Load EventDisplay
	gROOT->LoadMacro("../Analysis/EventDisplay.cxx+");

	// Gathers files to process
	TString fBasedir(configurationParameters::fTracksLocation);

	TString s=gSystem->GetFromPipe(Form("ls %s/*.root",fBasedir.Data()));
	TObjArray *arr=s.Tokenize("\n");

	// Selects the GEM tree, which is the one that we want!
	TChain * t = new TChain("GEM");
	for (Int_t ifile=0; ifile < arr->GetEntriesFast(); ++ifile)
	{
		t->AddFile(arr->At(ifile)->GetName());
	}

	// Settings for eventDisplay
	displayProperties * properties = new displayProperties(kFALSE, 10, -1, 10000);	

	EventDisplay(t, properties);
	
	delete t;
	delete properties;
}
