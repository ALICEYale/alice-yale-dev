// RunIndepTracking.C

void RunIndepTracking()
{
	// Load loadLibs
	gROOT->LoadMacro("loadlibs.C");

	loadlibs("../..");

	// Load GEMBeamTestPlotter for IsTrackAccepted function
	gROOT->LoadMacro("../Analysis/GEMBeamTestPlotter.cxx+");

	// Load IndepTracking
	gROOT->LoadMacro("../Analysis/IndepTracking.cxx+");

	// Gathers files to process
	TString fBasedir(configurationParameters::fTracksLocation);

	TString s=gSystem->GetFromPipe(Form("ls %s/*.root",fBasedir.Data()));
	TObjArray *arr=s.Tokenize("\n");

	// Selects the GEM tree, which is the one that we want!
	TChain * t = new TChain("GEM");
	for (Int_t ifile=0; ifile < arr->GetEntriesFast(); ++ifile)
	{
		t->AddFile(arr->At(ifile)->GetName());
	}

	IndepTracking(t);
	
	delete t;
}
