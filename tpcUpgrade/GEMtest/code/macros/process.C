/* type = 0: Monitor; type = 1: simple; type = 2: cuts*/
void process(Int_t type, const char *file, Int_t neventsMax)
{
  fMaxEvents=neventsMax;
  if (type==0){
    TestSimpleEvDisp(file);
  }
  if (type==1||type==2){
    SimpleProcessing(file);
  }
}
