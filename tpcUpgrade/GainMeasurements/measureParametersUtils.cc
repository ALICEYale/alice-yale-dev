#include <iostream>
#include <fstream>
#include <vector>
#include <sstream>
#include <algorithm>

#include <TFile.h>
#include <TF1.h>
#include <TH1D.h>
#include <TCanvas.h>
#include <TStyle.h>
#include <TLegend.h>
#include <TGraph.h>
#include <TGraphErrors.h>
#include <TText.h>

#define N 20

// To use with std::transform below
inline char transformCharacters(char ch)
{
  return ch == '-' ? '_' : ch;
}

struct minMax
{
  minMax(double _min, double _max): min(_min), max(_max) {}
  void set(double _min, double _max) {min = _min; max = _max;}
  double min;
  double max;
};

struct measurement
{
  measurement() : 
    filename(""),
    mmg(0),
    topGem(0),
    midGem(0),
    peakLocation(0,0),
    name(""),
    mean(0),
    width(0),
    energyResolution(0),
    gasAmplification(0),
    ibf(0),
    meanUnc(0),
    widthUnc(0),
    calibMeanUnc(0),
    calibWidthUnc(0),
    energyResolutionUnc(0),
    gasAmplificationUnc(0),
    ibfUnc(0),
    spectrum(0),
    fit(0) {}

  measurement(std::string _filename, double _mmg, double _topGem, double _midGem, double _min=0, double _max=0) : 
    filename(_filename),
    mmg(_mmg),
    topGem(_topGem),
    midGem(_midGem),
    peakLocation(_min,_max),
    name(""),
    mean(0),
    width(0),
    energyResolution(0),
    gasAmplification(0),
    ibf(0),
    meanUnc(0),
    widthUnc(0),
    calibMeanUnc(0),
    calibWidthUnc(0),
    energyResolutionUnc(0),
    gasAmplificationUnc(0),
    ibfUnc(0),
    spectrum(0),
    fit(0) {}

  std::string filename;
  double mmg;
  double topGem;
  double midGem;
  minMax peakLocation;
  std::string name;
  double mean;
  double width;
  double calibMean;
  double calibWidth;
  double energyResolution;
  double gasAmplification;
  double ibf;
  double meanUnc;
  double widthUnc;
  double calibMeanUnc;
  double calibWidthUnc;
  double energyResolutionUnc;
  double gasAmplificationUnc;
  double ibfUnc;
  TH1* spectrum;
  TF1* fit;
};

struct dataSet
{
  dataSet(const std::string _name="", const std::string _title="") : 
    name(_name),
    title(_title),
    calibA(0),
    calibB(0),
    calibAunc(0),
    calibBunc(0),
    K(0),
    color(kBlack),
    gainVsRes(0),
    gainVsIbf(0),
    resVsIbf(0) {}

  std::string name;
  std::string title;
  std::string calibFileName;
  std::vector<measurement> measurements;
  double calibA;
  double calibB;
  double calibAunc;
  double calibBunc;
  double K;
  Color_t color;
  TGraphErrors* gainVsRes;
  TGraphErrors* gainVsIbf;
  TGraphErrors* resVsIbf;
  std::string gas;
  std::string Edrift;
  std::string Etransf;
  std::string Eind;
  std::string note;
  std::string date;
};

std::string localPath = "";
bool saveRootFile = true;
bool savePdfFiles = true;
bool saveTxtFile = true;
bool plotSpectra = true;
bool printPointLabels = true;
bool printLines = false;

ofstream fout;

TH1* readFileIntoHist(std::string filename);
TF1* fitPeak(TH1* hist, std::string name, measurement &m);
void generateFileList(std::vector<measurement> &measurements, int dataSetNumber);
int measureParametersForDataset(dataSet &ds, bool draw=true);
int measureIbfForDataset(dataSet &ds);
int readParametersForDataset(dataSet& ds);
int readInfoSectionForDataset(dataSet& ds, std::ifstream& inFile);
int readDataSectionForDataset(dataSet& ds, std::ifstream& inFile);
void printGainVsRes(std::vector<dataSet> &dataSets);
void printGainVsIbf(std::vector<dataSet> &dataSets);
void printResVsIbf(std::vector<dataSet> &dataSets);
void saveDatasets(std::vector<dataSet> &dataSets);
void calibration(dataSet& ds);

void saveDatasets(std::vector<dataSet> &dataSets)
{
  std::string fullPath(localPath);
  fullPath += "/GainResIBF.root";
  TFile *outFile = TFile::Open(fullPath.c_str(), "recreate");
  outFile->cd();

  for (unsigned i = 0; i < dataSets.size(); i++) {
    for (unsigned j = 0; j < dataSets.at(i).measurements.size(); j++) {
      dataSets.at(i).measurements.at(j).spectrum->Write();
      dataSets.at(i).measurements.at(j).fit->Write();
    }

    dataSets.at(i).gainVsRes->Write();
    dataSets.at(i).gainVsIbf->Write();
    dataSets.at(i).resVsIbf->Write();
  }
}

void printGainVsRes(std::vector<dataSet> &dataSets)
{
  TCanvas *canvasGainVsRes = new TCanvas("canvasGainVsRes","canvasGainVsRes");
  canvasGainVsRes->cd();

  TH1* myBlankHistogram = new TH1F("myBlankHistogram","myBlankHistogram",5000,0,10000);
  myBlankHistogram->GetXaxis()->SetTitle("<GA>");
  myBlankHistogram->GetYaxis()->SetTitle("Energy Resolution (%)");
  myBlankHistogram->Draw();

  double minx = 10000;
  double maxx = 0;

  double miny = 100;
  double maxy = 0;

  TLegend *leg = new TLegend(0.15,0.70,0.45,0.85);
  leg->SetBorderSize(0);
  leg->SetFillStyle(0);

  std::stringstream pointLabelStream;
  pointLabelStream.precision(0);
  pointLabelStream.setf(std::ios::fixed, std:: ios::floatfield);

  for (unsigned int i = 0; i < dataSets.size(); i++) {
    if (!dataSets.at(i).gainVsRes) continue;

    dataSets.at(i).gainVsRes->SetMarkerStyle(kFullCircle);
    dataSets.at(i).gainVsRes->SetMarkerColor(dataSets.at(i).color);
    dataSets.at(i).gainVsRes->Draw("P same");

    leg->AddEntry(dataSets.at(i).gainVsRes, dataSets.at(i).title.c_str(), "p");

    unsigned int k = 0;

    for (unsigned int j = 0; j < dataSets.at(i).measurements.size(); j++) {
      pointLabelStream.str("");
      pointLabelStream << dataSets.at(i).measurements.at(j).mmg << ", " << dataSets.at(i).measurements.at(j).midGem << ", " << dataSets.at(i).measurements.at(j).topGem;

      double x = dataSets.at(i).measurements.at(j).gasAmplification;
      double y = dataSets.at(i).measurements.at(j).energyResolution*100;

      if (x < minx) minx = x;
      if (x > maxx) maxx = x;
      if (y < miny) miny = y;
      if (y > maxy) maxy = y;

      if (printPointLabels) {

        if (i == 1 && dataSets.at(i).measurements.at(j).mmg == 515.) k++;

        if (i == 1 && dataSets.at(i).measurements.at(j).mmg == 520.) {
          x -= 63;
          y += 0.07;
        }
        else if (k%2==0) {
          x -= 63;
          y -= 0.20;
        }
        else {
          x += 3.2;
          y += 0.058;
        }

        if (i == 1 && dataSets.at(i).measurements.at(j).mmg == 515.) k++;

        TText *pointLabel = new TText(x, y, pointLabelStream.str().c_str());
        pointLabel->SetTextColor(dataSets.at(i).color);
        pointLabel->SetTextSize(0.03);
        pointLabel->Draw();

        k++;
      }
    }
  }

  minx -= 200;
  maxx += 200;

  if (miny < 2.) miny = 0.;
  else miny -= 2.;
  
  maxy += 2.;
  
  myBlankHistogram->GetXaxis()->SetRangeUser(minx, maxx);
  myBlankHistogram->GetYaxis()->SetRangeUser(miny, maxy);

  leg->Draw();

  if (savePdfFiles) {
    std::string pdfFileName(localPath);
    pdfFileName = pdfFileName + "/gainVsRes.pdf";
    canvasGainVsRes->SaveAs(pdfFileName.c_str());
  }
}

void printGainVsIbf(std::vector<dataSet> &dataSets)
{
  TCanvas *canvasGainVsIbf = new TCanvas("canvasGainVsIbf","canvasGainVsIbf");
  canvasGainVsIbf->cd();

  TH1* myBlankHistogram = new TH1F("myBlankHistogram","myBlankHistogram",5000,0,10000);
  myBlankHistogram->GetXaxis()->SetTitle("<GA>");
  myBlankHistogram->GetYaxis()->SetTitle("IBF (%)");
  myBlankHistogram->Draw();

  double minx = 10000;
  double maxx = 0;

  double miny = 10;
  double maxy = 0;

  TLegend *leg = new TLegend(0.15,0.70,0.45,0.85);
  leg->SetBorderSize(0);
  leg->SetFillStyle(0);

  std::stringstream pointLabelStream;
  pointLabelStream.precision(0);
  pointLabelStream.setf(std::ios::fixed, std:: ios::floatfield);

  for (unsigned int i = 0; i < dataSets.size(); i++) {
    if (!dataSets.at(i).gainVsIbf) continue;

    dataSets.at(i).gainVsIbf->SetMarkerStyle(kFullCircle);
    dataSets.at(i).gainVsIbf->SetMarkerColor(dataSets.at(i).color);
    dataSets.at(i).gainVsIbf->Draw("P same");

    leg->AddEntry(dataSets.at(i).gainVsIbf, dataSets.at(i).title.c_str(), "p");

    unsigned int k = 0;

    for (unsigned int j = 0; j < dataSets.at(i).measurements.size(); j++) {
      if (dataSets.at(i).measurements.at(j).ibf == 0) continue;
            
      pointLabelStream.str("");
      pointLabelStream << dataSets.at(i).measurements.at(j).mmg << ", " << dataSets.at(i).measurements.at(j).midGem << ", " << dataSets.at(i).measurements.at(j).topGem;

      double x = dataSets.at(i).measurements.at(j).gasAmplification;
      double y = dataSets.at(i).measurements.at(j).ibf*100;

      if (x < minx) minx = x;
      if (x > maxx) maxx = x;
      if (y < miny) miny = y;
      if (y > maxy) maxy = y;

      if (printPointLabels) {
        if (k%2==0) {
          x -= 15;
          y -= 0.0165;
        }
        else {
          x += 5;
          y += 0.0015;
        }

        TText *pointLabel = new TText(x, y, pointLabelStream.str().c_str());
        pointLabel->SetTextColor(dataSets.at(i).color);
        pointLabel->SetTextSize(0.03);
        pointLabel->Draw();

        k++;
      }
    }
  }

  minx -= 200;
  maxx += 200;
  
  if (miny < 0.06) miny = 0.01;
  else miny -= 0.05;
  
  if (maxy > 9.95) maxy = 10.;
  else maxy += 0.05;
  
  myBlankHistogram->GetXaxis()->SetRangeUser(minx, maxx);
  myBlankHistogram->GetYaxis()->SetRangeUser(miny, maxy);
         
  leg->Draw();

  if (savePdfFiles) {
    std::string pdfFileName(localPath);
    pdfFileName = pdfFileName + "/gainVsIbf.pdf";
    canvasGainVsIbf->SaveAs(pdfFileName.c_str());
  }
}

void printResVsIbf(std::vector<dataSet> &dataSets)
{
  TCanvas *canvasResVsIbf = new TCanvas("canvasResVsIbf","canvasResVsIbf");
  canvasResVsIbf->cd();

  TH1* myBlankHistogram = new TH1F("myBlankHistogram","myBlankHistogram",3000,0,10);
  myBlankHistogram->GetXaxis()->SetTitle("IBF (%)");
  myBlankHistogram->GetYaxis()->SetTitle("Energy Resolution (%)");
  myBlankHistogram->Draw();

  double minx = 10;
  double maxx = 0;

  double miny = 100;
  double maxy = 0;

  TLegend *leg = new TLegend(0.60,0.65,0.89,0.84);
  leg->SetBorderSize(0);
  leg->SetFillStyle(0);

  std::stringstream pointLabelStream;
  pointLabelStream.precision(0);
  pointLabelStream.setf(std::ios::fixed, std:: ios::floatfield);

  for (unsigned int i = 0; i < dataSets.size(); i++) {
    if (!dataSets.at(i).resVsIbf) continue;

    dataSets.at(i).resVsIbf->SetMarkerStyle(kFullCircle);
    dataSets.at(i).resVsIbf->SetMarkerColor(dataSets.at(i).color);
    dataSets.at(i).resVsIbf->SetLineColor(dataSets.at(i).color);
    if (printLines) dataSets.at(i).resVsIbf->Draw("LP same");
    else dataSets.at(i).resVsIbf->Draw("P same");

    leg->AddEntry(dataSets.at(i).resVsIbf, dataSets.at(i).title.c_str(), "p");

    unsigned int k = 0;

    for (unsigned int j = 0; j < dataSets.at(i).measurements.size(); j++) {
      if (dataSets.at(i).measurements.at(j).ibf == 0) continue;
      
      pointLabelStream.str("");
      pointLabelStream << dataSets.at(i).measurements.at(j).mmg << ", " << dataSets.at(i).measurements.at(j).midGem << ", " << dataSets.at(i).measurements.at(j).topGem;

      double x = dataSets.at(i).measurements.at(j).ibf*100;
      double y = dataSets.at(i).measurements.at(j).energyResolution*100;

      if (x < minx) minx = x;
      if (x > maxx) maxx = x;
      if (y < miny) miny = y;
      if (y > maxy) maxy = y;

      if (printPointLabels) {
        if (k%2==0) {
          x -= 0.0165;
          y -= 0.3;
        }
        else {
          x += 0.0015;
          y += 0.1;
        }

        TText *pointLabel = new TText(x, y, pointLabelStream.str().c_str());
        pointLabel->SetTextColor(dataSets.at(i).color);
        pointLabel->SetTextSize(0.03);
        pointLabel->Draw();
      }
      
      k++;
    }
  }

  if (minx < 0.06) minx = 0.01;
  else minx -= 0.05;
  
  if (maxx > 9.95) maxx = 10.;
  else maxx += 0.05;

  if (miny < 2.) miny = 0.;
  else miny -= 2.;
  
  maxy += 2.;
  
  myBlankHistogram->GetXaxis()->SetRangeUser(minx, maxx);
  myBlankHistogram->GetYaxis()->SetRangeUser(miny, maxy);

  leg->Draw();

  if (savePdfFiles) {
    std::string pdfFileName(localPath);
    pdfFileName = pdfFileName + "/resVsIbf.pdf";
    canvasResVsIbf->SaveAs(pdfFileName.c_str());
  }
}

int generateTGraphsForDataset(dataSet &ds)
{
  // Generate the TGraph objects for the dataset ds.
  
  int n = 0;

  Double_t *gain = new Double_t[ds.measurements.size()];
  Double_t *res = new Double_t[ds.measurements.size()];
  Double_t *ibf = new Double_t[ds.measurements.size()];

  Double_t *gainUnc = new Double_t[ds.measurements.size()];
  Double_t *resUnc = new Double_t[ds.measurements.size()];
  Double_t *ibfUnc = new Double_t[ds.measurements.size()];

  
  for (unsigned int i = 0; i < ds.measurements.size(); i++) {
    gain[n] = ds.measurements.at(i).gasAmplification;
    res[n] = ds.measurements.at(i).energyResolution*100;
    ibf[n] = ds.measurements.at(i).ibf*100;
        
    gainUnc[n] = ds.measurements.at(i).gasAmplificationUnc;
    resUnc[n] = ds.measurements.at(i).energyResolutionUnc*100;
    ibfUnc[n] = ds.measurements.at(i).ibfUnc*100;
        
    n++;
  }

  // Now create the graphs

  ds.gainVsRes = new TGraphErrors(ds.measurements.size(), gain, res, gainUnc, resUnc);
  std::string name;
  name = ds.name + "_gainVsRes";
  std::transform(name.begin(), name.end(), name.begin(), transformCharacters);
  ds.gainVsRes->SetName(name.c_str());
  ds.gainVsRes->SetTitle(name.c_str());
  ds.gainVsRes->GetXaxis()->SetTitle("<GA>");
  ds.gainVsRes->GetYaxis()->SetTitle("Energy Resolution (%)");

  std::string gainVsIbfName(ds.name);
  std::transform(gainVsIbfName.begin(), gainVsIbfName.end(), gainVsIbfName.begin(), transformCharacters);
  gainVsIbfName += "_gainVsIbf";
  ds.gainVsIbf = new TGraphErrors(n, gain, ibf, gainUnc, ibfUnc);
  ds.gainVsIbf->SetName(gainVsIbfName.c_str());
  ds.gainVsIbf->SetTitle(gainVsIbfName.c_str());
  ds.gainVsIbf->GetXaxis()->SetTitle("<GA>");
  ds.gainVsIbf->GetYaxis()->SetTitle("IBF (%)");

  std::string resVsIbfName(ds.name);
  std::transform(resVsIbfName.begin(), resVsIbfName.end(), resVsIbfName.begin(), transformCharacters);
  resVsIbfName += "_resVsIbf";
  ds.resVsIbf = new TGraphErrors(n, ibf, res, ibfUnc, resUnc);
  ds.resVsIbf->SetName(resVsIbfName.c_str());
  ds.resVsIbf->SetTitle(resVsIbfName.c_str());
  ds.resVsIbf->GetXaxis()->SetTitle("IBF (%)");
  ds.resVsIbf->GetYaxis()->SetTitle("Energy Resolution (%)");

  return 0;
}

int measureIbfForDataset(dataSet &ds)
{
  std::string fullPath(localPath);
  fullPath =  fullPath + "/ibf/"  + ds.name + "_IBF.txt";
  std::ifstream inFile(fullPath.c_str());

  char buffer[200] = {0};
  // skip the first line
  inFile >> buffer;
  inFile >> buffer;
  inFile >> buffer;
  
  double bkgElectrons = 0;
  double bkgIons = 0;

  std::cout << std::endl;
  if (saveTxtFile) fout << std::endl;

  while (inFile.good()) {
    std::string fname;
    double electrons = 0;
    double ions = 0;

    inFile >> fname;
    inFile >> electrons;
    inFile >> ions;

    if (fname == "") continue;

    std::cout << "File: " << fname << "\tElectrons: " << electrons << "\tIons: " << ions << std::endl;
    if (saveTxtFile) fout << "File: " << fname << "\tElectrons: " << electrons << "\tIons: " << ions << std::endl;
    
    if (fname == "Background") {
      bkgElectrons = electrons;
      bkgIons = ions;
      continue;
    }

    electrons -= bkgElectrons;
    ions -= bkgIons;

    for (unsigned int i = 0; i < ds.measurements.size(); i++) {
      if (fname == ds.measurements.at(i).filename) {
        ds.measurements.at(i).ibf = ions / electrons;
        ds.measurements.at(i).ibfUnc = sqrt(1. / electrons / electrons + 0.03 * 0.03 / ions / ions) * ds.measurements.at(i).ibf;  // just a guess
        std::cout << "\tMeasurement n.: " << i << "\tElectrons: " << electrons << "\tIons: " << ions << "\tIBF (%): " << ds.measurements.at(i).ibf*100 << std::endl;
        std::cout << "\tTop GEM: " << ds.measurements.at(i).topGem << "\tMid GEM: " << ds.measurements.at(i).midGem << "\tMMG: " << ds.measurements.at(i).mmg  << std::endl;
        if (saveTxtFile) {
          fout << "\tMeasurement n.: " << i << "\tElectrons: " << electrons << "\tIons: " << ions << "\tIBF (%): " << ds.measurements.at(i).ibf*100 << std::endl;
          fout << "\tTop GEM: " << ds.measurements.at(i).topGem << "\tMid GEM: " << ds.measurements.at(i).midGem << "\tMMG: " << ds.measurements.at(i).mmg  << std::endl;
        }
        break;
      }
    }
  }

  std::cout << std::endl;
  if (saveTxtFile) fout << std::endl;

  inFile.close();

  return 0;
}

int measureParametersForDataset(dataSet &ds, bool draw)
{
  // Perform actual analysis

  for (unsigned int i = 0; i < ds.measurements.size(); i++) {
    ds.measurements.at(i).spectrum = readFileIntoHist(ds.measurements.at(i).filename);
    
    std::cout << "Name: " << ds.measurements.at(i).spectrum->GetName() <<  "\tEntries: " << ds.measurements.at(i).spectrum->GetEntries() << std::endl;
    if (saveTxtFile) fout << "Name: " << ds.measurements.at(i).spectrum->GetName() <<  "\tEntries: " << ds.measurements.at(i).spectrum->GetEntries() << std::endl;

    ds.measurements.at(i).fit = fitPeak(ds.measurements.at(i).spectrum, ds.measurements.at(i).spectrum->GetName(), ds.measurements.at(i));

    ds.measurements.at(i).calibMean = ds.calibA + ds.calibB*ds.measurements.at(i).mean;
    ds.measurements.at(i).calibWidth = ds.calibB*ds.measurements.at(i).width;
    ds.measurements.at(i).energyResolution = ds.measurements.at(i).calibWidth / ds.measurements.at(i).calibMean;
    ds.measurements.at(i).gasAmplification = ds.measurements.at(i).calibMean*ds.K;

    ds.measurements.at(i).calibMeanUnc = sqrt(ds.calibAunc*ds.calibAunc +
                                              ds.calibBunc*ds.calibBunc*ds.measurements.at(i).mean*ds.measurements.at(i).mean +
                                              ds.measurements.at(i).meanUnc*ds.measurements.at(i).meanUnc*ds.calibB*ds.calibB);
    ds.measurements.at(i).calibWidthUnc = sqrt(ds.calibBunc*ds.calibBunc*ds.measurements.at(i).width*ds.measurements.at(i).width +
                                               ds.calibB*ds.calibB*ds.measurements.at(i).widthUnc*ds.measurements.at(i).widthUnc);
    ds.measurements.at(i).energyResolutionUnc = sqrt(ds.measurements.at(i).calibWidthUnc*ds.measurements.at(i).calibWidthUnc/
                                                     ds.measurements.at(i).calibMean/ds.measurements.at(i).calibMean +
                                                     ds.measurements.at(i).calibMeanUnc*ds.measurements.at(i).calibMeanUnc/
                                                     ds.measurements.at(i).calibMean/ds.measurements.at(i).calibMean/
                                                     ds.measurements.at(i).calibMean/ds.measurements.at(i).calibMean*
                                                     ds.measurements.at(i).calibWidth*ds.measurements.at(i).calibWidth);
    ds.measurements.at(i).gasAmplificationUnc = ds.measurements.at(i).calibMeanUnc*ds.K;

    std::cout << "\tFitting range: [" << ds.measurements.at(i).peakLocation.min << ", " << ds.measurements.at(i).peakLocation.max << "]" << std::endl;
    std::cout << "\tMean: " << ds.measurements.at(i).mean << "\tWidth: " << ds.measurements.at(i).width << std::endl;
    std::cout << "\tMean error: " << ds.measurements.at(i).meanUnc << "\tWidth error: " << ds.measurements.at(i).widthUnc << std::endl;
    std::cout << "\tCalibrated Mean: " << ds.measurements.at(i).calibMean << " mV \tCalibrated Width: " << ds.measurements.at(i).calibWidth << " mV" << std::endl;
    std::cout << "\tCalibrated Mean error: " << ds.measurements.at(i).calibMeanUnc <<
      " mV \tCalibrated Width error: " << ds.measurements.at(i).calibWidthUnc << " mV" << std::endl;
    std::cout << "\tEnergy res (%): " << ds.measurements.at(i).energyResolution*100 << std::endl;
    std::cout << "\tEnergy res error (%): " << ds.measurements.at(i).energyResolutionUnc*100 << std::endl;
    std::cout << "\tGas amplificadtion: " << ds.measurements.at(i).gasAmplification << std::endl;
    std::cout << "\tGas amplificadtion error: " << ds.measurements.at(i).gasAmplificationUnc << std::endl;

    if (saveTxtFile) {
      fout << "\tFitting range: [" << ds.measurements.at(i).peakLocation.min << ", " << ds.measurements.at(i).peakLocation.max << "]" << std::endl;
      fout << "\tMean: " << ds.measurements.at(i).mean << "\tWidth: " << ds.measurements.at(i).width << std::endl;
      fout << "\tMean error: " << ds.measurements.at(i).meanUnc << "\tWidth error: " << ds.measurements.at(i).widthUnc << std::endl;
      fout << "\tCalibrated Mean: " << ds.measurements.at(i).calibMean << " mV \tCalibrated Width: " << ds.measurements.at(i).calibWidth << " mV" << std::endl;
      fout << "\tCalibrated Mean error: " << ds.measurements.at(i).calibMeanUnc <<
        " mV \tCalibrated Width error: " << ds.measurements.at(i).calibWidthUnc << " mV" << std::endl;
      fout << "\tEnergy res (%): " << ds.measurements.at(i).energyResolution*100 << std::endl;
      fout << "\tEnergy res error (%): " << ds.measurements.at(i).energyResolutionUnc*100 << std::endl;
      fout << "\tGas amplificadtion: " << ds.measurements.at(i).gasAmplification << std::endl;
      fout << "\tGas amplificadtion error: " << ds.measurements.at(i).gasAmplificationUnc << std::endl;
    }
    
    if (draw) {
      std::stringstream cname;
      cname << ds.measurements.at(i).spectrum->GetName() << "_canvas";
      TCanvas *c = new TCanvas(cname.str().c_str(),cname.str().c_str());
      c->cd();
      TH1* hc = ds.measurements.at(i).spectrum->DrawCopy();
      hc->GetXaxis()->SetRangeUser(0,512);
      hc->Rebin(2);
      hc->Scale(1./2);
      hc->SetMarkerStyle(kFullCircle);
      hc->SetMarkerSize(0.6);
      hc->SetMarkerColor(kBlue+2);
      hc->SetLineColor(kBlue+2);
      TF1* fc = ds.measurements.at(i).fit->DrawCopy("same");

      TLegend *leg = new TLegend(0.50,0.85,0.85,0.55);
      leg->SetFillStyle(0);
      leg->SetBorderSize(0);
      leg->AddEntry(hc, ds.measurements.at(i).spectrum->GetName(), "pe");
      leg->AddEntry(fc,"Gaussian fit","l");

      std::stringstream myStream;
      myStream.precision(2);
      myStream.setf(std::ios::fixed, std:: ios::floatfield);

      myStream << "#mu = " << fc->GetParameter("Mean") << ", #sigma = " << fc->GetParameter("Sigma");
      leg->AddEntry((TObject*)0,myStream.str().c_str(),"");

      myStream.str("");
      myStream << "<GA> = " << ds.measurements.at(i).gasAmplification;
      leg->AddEntry((TObject*)0,myStream.str().c_str(),"");

      myStream.str("");
      myStream << "Energy resolution = " << ds.measurements.at(i).energyResolution*100 << "%";
      leg->AddEntry((TObject*)0,myStream.str().c_str(),"");

      leg->Draw();

      if (savePdfFiles) {
        std::stringstream pdfFileName;
        pdfFileName << localPath << "/" << ds.measurements.at(i).spectrum->GetName() << ".pdf";
        c->SaveAs(pdfFileName.str().c_str());
      }
    }
  }

  return 0;
}

int readParametersForDataset(dataSet& ds)
{
  std::string fullPath(localPath);
  fullPath += "/";
  fullPath += ds.name;
  fullPath += ".txt";

  std::cout << "Opening file '" << fullPath << "'" << std::endl;
  
  std::ifstream inFile(fullPath.c_str());

  bool skipSection = false;
  
  while (inFile.good()) {
    std::string line;
    std::getline(inFile, line);
    std::cout << line << std::endl;

    if (line.empty()) continue;
    if (line.front() == '#' && line.back() == '#') {
      skipSection = false;
      if (line == "#info_start#") {
        readInfoSectionForDataset(ds, inFile);
      }
      else if (line == "#info_end#") {
        continue;
      }
      else if (line == "#data_start#") {
        std::cout << "Now reading data section" << std::endl;
        readDataSectionForDataset(ds, inFile);
      }      
      else if (line == "#data_end#") {
        continue;
      }
      else if (line == "#ignore_start#") {
        skipSection = true;
      }
      else if (line == "#ignore_end#") {
        continue;
      }
      else {
        std::cout << "Error parsing file '" << fullPath <<  "'.\n" <<
          "Section '" << line << "' not recognized. This section will be skipped." << std::endl;
        skipSection = true;
      }
    }
    else {
      if (!skipSection) {
        std::cout << "Error parsing file '" << fullPath <<  "'.\n" <<
          "Each section should start with a section header in the format '#section#'. The line that could not be parsed is printed below.\n" <<
          line << std::endl;
      }
      else {
        std::cout << "This line is skipped." << std::endl;
      }
    }
  }
  
  return 0;
}

int readInfoSectionForDataset(dataSet& ds, std::ifstream& inFile)
{
  std::string line;
  while (inFile.good() || line == "#info_end#") {
    std::getline(inFile, line);
    if (line.empty()) continue;
    if (line.front() == '#') break;
    
    size_t pos = line.find_first_of('=');
    if (pos > line.length()) {
      std::cout << "Could not parse line (see below)" << std::endl <<
        line << std::endl;
      continue;
    }
    std::string field = line.substr(0, pos-1);
    std::string value = line.substr(pos+2);
    if (field == "Gas") {
      ds.gas = value;
    }
    else if (field == "Edrift") {
      ds.Edrift = value;
    }
    else if (field == "Etransf") {
      ds.Etransf = value;
    }
    else if (field == "Eind") {
      ds.Eind = value;
    }
    else if (field == "Note") {
      ds.note = value;
    }
    else if (field == "Date") {
      ds.date = value;
    }
  }
  return 0;
}

int readDataSectionForDataset(dataSet& ds, std::ifstream& inFile)
{
  char c = 0;
  int i = 0;
  while (inFile.good()) {
    inFile.get(c);
    inFile.unget();
    if (c == '#') break;
    
    double mmg = 0;
    double topGem = 0;
    double midGem = 0;
    double gasAmplification = 0;
    double energyResolution = 0;
    double ibf = 0;
    inFile >> mmg;
    if (!inFile.good()) break;
    inFile >> topGem;
    if (!inFile.good()) break;
    inFile >> midGem;
    if (!inFile.good()) break;
    inFile >> gasAmplification;
    if (!inFile.good()) break;
    inFile >> energyResolution;
    if (!inFile.good()) break;
    inFile >> ibf;
    
    cout << "Reading measurement n. " << i << ". MMG = " << mmg << ", TopGem = " << topGem << ", MidGem = " << midGem << std::endl <<
      "Gain = " << gasAmplification << ", Res = " << energyResolution << ", IBF = " << ibf << std::endl;

    std::stringstream measName;
    measName << ds.name << '_' << mmg << '_' << topGem << '_' << midGem;
    ds.measurements.push_back(measurement(measName.str(), mmg, topGem, midGem));
    ds.measurements.at(i).gasAmplification = gasAmplification;
    ds.measurements.at(i).energyResolution = energyResolution/100;
    ds.measurements.at(i).ibf = ibf/100;
    i++;
  }
  
  return 0;
}

TF1* fitPeak(TH1* hist, std::string name, measurement &m)
{
  // Set name
  name = "fit_" + name;
  // Fit to the gain curves
  TF1 * peakFit = new TF1(name.c_str(), "gaus", m.peakLocation.min, m.peakLocation.max);
  peakFit->SetTitle(name.c_str());

  // Fit to histogram
  // R uses the range defined in the fit function
  // 0 ensures that the fit isn't drawn
  // Q ensures minimum printing
  // + adds the fit to function list to ensure that it is not deleted on the creation of a new fit
  hist->Fit(peakFit, "RQ0+");
  //hist->Fit(peakFit, "R0+");

  // Determine the current gain
  m.mean = peakFit->GetParameter("Mean");
  m.width =  peakFit->GetParameter("Sigma");

  m.meanUnc = peakFit->GetParError(peakFit->GetParNumber("Mean"));
  m.widthUnc =  peakFit->GetParError(peakFit->GetParNumber("Sigma"));

  m.name = name;

  return peakFit;
}

// File in a histogram from the gain input file
TH1* readFileIntoHist(std::string filename)
{
  // Using: http://stackoverflow.com/a/7868998
  std::stringstream fullPath;
  fullPath << localPath << "/combinedData/" << filename;
  std::ifstream inFile(fullPath.str().c_str());
  // find("/")+1 ensures that the "/" isn't included
  filename = filename.substr(filename.find("/")+1);
  filename = filename.substr(0, filename.find(".txt"));
  filename = "gain-" + filename;

  std::transform(filename.begin(), filename.end(), filename.begin(), transformCharacters);

  TH1* hist = new TH1D(filename.c_str(), filename.c_str(), 1024, 0, 1024);
  hist->GetXaxis()->SetTitle("ADC Channel");
  hist->GetYaxis()->SetTitle("Entries");
  
  int channel = 0;
  while (inFile >> channel) {
    hist->Fill(channel);
  }

  hist->Sumw2();

  inFile.close();

  return hist;
}

void generateFileList(std::vector <measurement> &measurements, const int dataSetNumber)
{
  if (dataSetNumber == 0) { // 2014-07-18 - use for Ne+CO2+CH4 data
    measurements.push_back(measurement("ne-co2-ch4-495.txt", 495, 210, 220, 110, 150));
    measurements.push_back(measurement("ne-co2-ch4-500.txt", 500, 210, 220, 110, 200));
    measurements.push_back(measurement("ne-co2-ch4-505.txt", 505, 210, 220, 135, 250));
    measurements.push_back(measurement("ne-co2-ch4-505-250.txt", 505, 215, 250, 220, 350));
    measurements.push_back(measurement("ne-co2-ch4-510.txt", 510, 215, 235, 185, 300));
    measurements.push_back(measurement("ne-co2-ch4-510-245.txt", 510, 215, 245, 220, 350));
    measurements.push_back(measurement("ne-co2-ch4-510-250.txt", 510, 215, 250, 245, 400));
    measurements.push_back(measurement("ne-co2-ch4-515.txt", 515, 215, 240, 230, 350));
    measurements.push_back(measurement("ne-co2-ch4-515-245.txt", 515, 215, 245, 245, 400));
    measurements.push_back(measurement("ne-co2-ch4-520.txt", 520, 215, 240, 250, 400));
  }
  else if (dataSetNumber == 1) { // 2014-07-22 - use for Ne+CO2+CF4 data
    measurements.push_back(measurement("ne-co2-cf4_510_1.txt", 510, 230, 260, 225, 400));
    measurements.push_back(measurement("ne-co2-cf4_515_4.txt", 515, 240, 245, 225, 400));
    measurements.push_back(measurement("ne-co2-cf4_515_5.txt", 515, 230, 255, 230, 400));
    measurements.push_back(measurement("ne-co2-cf4_520_1.txt", 520, 225, 255, 235, 400));
    measurements.push_back(measurement("ne-co2-cf4_530_1.txt", 530, 225, 255, 240, 400));
    measurements.push_back(measurement("ne-co2-cf4_535_1.txt", 535, 225, 250, 245, 400));
    measurements.push_back(measurement("ne-co2-cf4_540_1.txt", 540, 225, 245, 250, 400));
    measurements.push_back(measurement("ne-co2-cf4_545_1.txt", 545, 225, 240, 255, 400));
    if (0) { // This data was taken with different conditions, see log book for details
      measurements.push_back(measurement("ne-co2-cf4_515_1.txt", 515, 250, 245, 50, 150));
      measurements.push_back(measurement("ne-co2-cf4_515_2.txt", 515, 250, 0, 50, 150));
      measurements.push_back(measurement("ne-co2-cf4_515_3.txt", 515, 0, 0, 50, 150));
      measurements.push_back(measurement("ne-co2-cf4_530_2.txt", 530, 225, 255, 50, 150));
      measurements.push_back(measurement("ne-co2-cf4_530_3.txt", 530, 225, 0, 50, 150));
      measurements.push_back(measurement("ne-co2-cf4_530_4.txt", 530, 0, 0, 50, 150));
    }
  }
  else {
    std::cout << "generateFileList - Wrong dataSetNumber!" << std::endl;
  }
}

void calibration(dataSet& ds)
{
  gStyle->SetOptTitle(kFALSE);

  std::string fname(localPath);
  fname += "/";
  fname += ds.calibFileName;
  
  ifstream fin(fname);
  
  char buffer[200] = {0};

  // skip the first line
  fin >> buffer;
  fin >> buffer;
  fin >> buffer;
  fin >> buffer;

  Double_t pulser[N] = {0};
  Double_t adc[N] = {0};
  Double_t errPulser[N] = {0};
  Double_t errAdc[N] = {0};
  Int_t n = 0;

  while (fin.good() && n < N) {
    fin >> pulser[n];
    fin >> errPulser[n];
    fin >> adc[n];
    fin >> errAdc[n];

    if (pulser[n] == 0) break;
    
    Printf("%d, %.2f  %.2f",n, pulser[n], adc[n]);
    if (pulser[n] == 0 || adc[n] == 0) continue;
    n++;
  }

  if (n == N) {
    Printf("Warning: not all data was read! Increase N.");
  }

  if (n < 1) return;

  TGraphErrors *graph = new TGraphErrors(n,adc,pulser,errAdc,errPulser);

  std::string graphName(ds.name);
  graphName += "_calib";
  
  graph->SetName(graphName.c_str());
  graph->SetTitle(graphName.c_str());
  
  graph->GetXaxis()->SetTitle("ADC Channel");
  graph->GetYaxis()->SetTitle("Pulser (mV)");

  graph->SetMarkerStyle(kFullCircle);
  graph->SetMarkerSize(1.);
  graph->SetMarkerColor(kBlue+1);

  std::string canvName(graphName);
  canvName += "_canvas";

  TCanvas *c = new TCanvas(canvName.c_str(),canvName.c_str(),600,600);
  c->cd();

  graph->Draw("PA");

  TF1 *f = new TF1("f","pol1",0,500);
  graph->Fit(f,"");

  TLegend *leg = new TLegend(0.45,0.35,0.85,0.15);
  leg->SetBorderSize(0);
  leg->SetFillStyle(0);

  TString legLabel(Form("a = %.4f, b = %.4f", f->GetParameter(0), f->GetParameter(1)));

  leg->AddEntry(graph, "Data", "p");
  leg->AddEntry(f, "Fit", "l");
  leg->AddEntry((TObject*)0, "a + b #times Channel","");
  leg->AddEntry((TObject*)0, legLabel,"");

  ds.calibA = f->GetParameter(0);
  ds.calibB = f->GetParameter(1);

  ds.calibAunc = f->GetParError(0);
  ds.calibBunc = f->GetParError(1);

  leg->Draw();
}
