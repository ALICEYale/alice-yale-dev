#include "measureParametersUtils.cc"

int measureParameters_march2015()
{
  localPath = "/Users/saiola/Google Drive/MMG_Measurements/GainEnergyRes/march2015";
  saveRootFile = false;
  savePdfFiles = true;
  saveTxtFile = false;
  plotSpectra = false;
  printPointLabels = false;
  printLines = true;
  
  TH1::AddDirectory(kFALSE);

  std::vector <dataSet> dataSets;

  gStyle->SetOptStat(0);
  gStyle->SetOptTitle(0);

  if (saveTxtFile) {
    std::string txtFileName(localPath);
    txtFileName += "/GainResIBF.txt";
    fout.open(txtFileName);
  }

  
  dataSets.push_back(dataSet("ne-co2_midGem190", "dV_{Mid-GEM} = 190 V"));
  dataSets.at(0).color = kBlue+2;
  readParametersForDataset(dataSets.at(0));
  generateTGraphsForDataset(dataSets.at(0));
  
  dataSets.push_back(dataSet("ne-co2_midGem210", "dV_{Mid-GEM} = 210 V"));
  dataSets.at(1).color = kRed+2;
  readParametersForDataset(dataSets.at(1));
  generateTGraphsForDataset(dataSets.at(1));

  dataSets.push_back(dataSet("ne-co2_midGem220", "dV_{Mid-GEM} = 220 V"));
  dataSets.at(2).color = kGreen+2;
  readParametersForDataset(dataSets.at(2));
  generateTGraphsForDataset(dataSets.at(2));

  dataSets.push_back(dataSet("ne-co2_midGem230", "dV_{Mid-GEM} = 230 V"));
  dataSets.at(3).color = kOrange+2;
  readParametersForDataset(dataSets.at(3));
  generateTGraphsForDataset(dataSets.at(3));
  
  printGainVsRes(dataSets);
  printGainVsIbf(dataSets);
  printResVsIbf(dataSets);
  
  if (saveRootFile) saveDatasets(dataSets);

  return 0;
}

int main()
{
  return measureParameters_march2015();
}
