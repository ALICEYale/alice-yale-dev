#include <fstream>
#include <stdlib.h>

#include <TFile.h>
#include <TSystemDirectory.h>
#include <TSystemFile.h>
#include <TList.h>
#include <TString.h>
#include <Riostream.h>
#include <TObjArray.h>
#include <TObjString.h>
#include <TNtuple.h>

using namespace std;

void ConvertToRoot()
{
  const Int_t NRUNS = 3;
  const Int_t STARTINDEX = 6;
  const Int_t buf_size = 4000000;

  TFile *out = new TFile("2GemMMGGain-14-04-15.root","recreate");
  TNtuple *ntuple[NRUNS] = {0};
  

  char buffer[buf_size];
  memset(buffer,0,buf_size);

  for (Int_t j = 0; j < NRUNS; j++) {
    TString runName(Form("Run%d",j+STARTINDEX));
    ntuple[j] = new TNtuple(runName,runName,"c");
    TString fname(Form("2GemMMGGain-%s-14-04-15.txt",runName.Data()));

    Printf("Now working on %s",fname.Data());
    ifstream in(fname);
    in.seekg(0,ios::end);
    Int_t size = in.tellg();
    in.seekg(0);
    if (size > buf_size) {
      Printf("File size %d exceeds allocated buffer!",size);
      size = buf_size; 
    }
    in.read(buffer,size);
    TString strBuffer(buffer);
    TObjArray* lines = strBuffer.Tokenize("\n");
    Int_t nlines = lines->GetEntries();
    Printf("%d lines found to be processed", nlines);
    Double_t val = 0;
    for (Int_t i = 0; i < nlines; i++) {
      TObjString *objline = (TObjString*)lines->At(i);
      TString line(objline->GetString());
      TObjArray* values = line.Tokenize(" ");
      //Int_t nvals = values->GetEntries();
      //Printf("%d values found to be processed", nvals);
      TObjString *objval = (TObjString*)values->At(16);
      TString strval(objval->GetString());
      val = strval.Atof();
      ntuple[j]->Fill(val);
      //Printf("%f",val);

      delete values;
      values = 0;
    }
    in.close();

    delete lines;
    lines = 0;

    memset(buffer,0,buf_size);
  }
    
  out->cd();
  for (Int_t i = 0; i < NRUNS; i++) ntuple[i]->Write();
  out->Close();
}
