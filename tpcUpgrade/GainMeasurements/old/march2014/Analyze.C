#include <TFile.h>
#include <TNtuple.h>
#include <TString.h>
#include <TH1F.h>
#include <TH2F.h>
#include <TCanvas.h>
#include <TF1.h>
#include <TGraph2D.h>
#include <TStyle.h>
#include <TLegend.h>

void Analyze()
{
  gStyle->SetOptStat(0);
  gStyle->SetOptTitle(0);

  TFile *file = TFile::Open("data.root");
  TNtuple *ntuple[16];
  TH1F *histos[16];
  Color_t cols[16] = { kBlack, kGray, kRed, kGreen, kBlue, kYellow, kMagenta, kCyan, kOrange, kSpring, kTeal, kAzure, kViolet, kPink, 41, 28};
  
  Double_t total[16];
  Double_t mmg[16];
  Double_t gem[16];

  TCanvas *c1 = new TCanvas("c1","c1");
  c1->cd();

  for (Int_t x = 0; x < 4; x++) {
    for (Int_t y = 0; y < 4; y++) {
      TString name(Form("X%dY%d",x,y));
      ntuple[x+y*4] = (TNtuple*)file->Get(name);
      TString hname("hist");
      hname += name;
      histos[x+y*4] = new TH1F(hname,hname,1024,0,4095);
      histos[x+y*4]->GetXaxis()->SetTitle("channel");
      histos[x+y*4]->GetYaxis()->SetTitle("entries");
      histos[x+y*4]->GetXaxis()->SetRangeUser(0,1500);
      TString varexp("x>>");
      varexp += hname;
      ntuple[x+y*4]->Draw(varexp.Data(),"","goff");
      histos[x+y*4]->SetLineColor(cols[x+y*4]);
      histos[x+y*4]->Scale(16-(x+y*4));
      if (x==0 && y==0)
	histos[x+y*4]->Draw();
      else
	histos[x+y*4]->Draw("same");

      Printf("Fitting X=%d, Y=%d",x,y);
      Printf("Region 150-250");
      histos[x+y*4]->Fit("gaus","0+","",150,250);
      Printf("Region 600-1200");
      histos[x+y*4]->Fit("gaus","0+","",600,1200);

      TList *functs = histos[x+y*4]->GetListOfFunctions();
      TF1* gaus1 = (TF1*)functs->At(0);
      TF1* gaus2 = (TF1*)functs->At(1);

      mmg[x+y*4] = gaus1->GetParameter("Mean");      
      total[x+y*4] = gaus2->GetParameter("Mean");
      gem[x+y*4] = total[x+y*4] / mmg[x+y*4];
    }
  }

  TLegend *leg = c1->BuildLegend(0.666667,0.209302,0.87931,0.881607);
  leg->SetLineColor(kWhite);
  leg->SetFillColor(kWhite);

  Double_t regionsY[16] = {0, 0, 0, 0, 1, 1, 1, 1, 2, 2, 2, 2, 3, 3, 3, 3};
  Double_t regionsX[16] = {0, 1, 2, 3, 0, 1, 2, 3, 0, 1, 2, 3, 0, 1, 2, 3};

  TGraph2D *gemGains = new TGraph2D(16,regionsX,regionsY,gem);
  gemGains->GetXaxis()->SetTitle("X");
  gemGains->GetYaxis()->SetTitle("Y");

  TCanvas *c2 = new TCanvas("c2","c2",600,600);
  c2->cd();

  TH2* myBlankHistogram = new TH2F("myBlankHistogram","myBlankHistogram",1000,0,5,1000,0,5);
  myBlankHistogram->GetXaxis()->SetTitle("X");
  myBlankHistogram->GetYaxis()->SetTitle("Y");
  myBlankHistogram->GetXaxis()->SetRangeUser(0.,3.);
  myBlankHistogram->GetYaxis()->SetRangeUser(0.,3.);
  myBlankHistogram->Draw();

  gemGains->Draw("same colz");

  Printf("X Y G");
  for (Int_t x = 0; x < 4; x++) {
    for (Int_t y = 0; y < 4; y++) {
      Printf("%d %d %.2f",x,y,gem[x+y*4]);
    }
  }
}
