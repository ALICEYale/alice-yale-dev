#include <TCanvas.h>
#include <TVirtualPad.h>
#include <TPad.h>
#include <TGraph.h>
#include <TH1F.h>
#include <TStyle.h>

void SetUpMyGraph(TGraph* g, Style_t marker, Double_t msize, Color_t color);

void plot1()
{
  gStyle->SetOptStat(0);
  gStyle->SetOptTitle(0);

  const Int_t n = 5;
  Double_t Etransf[n] = {1.5, 2.0, 2.5, 3.0, 3.5};
  Double_t ibf[n] = {0.45, 0.4, 0.36, 0.31, 0.3};
  Double_t Eres[n] = {12.6, 12.65, 12.6, 12.1, 11.9};
  Double_t gain[n] = {1553., 1500., 1615., 2154., 2760.}; 

  //"star" data with V MMG + 10/15 V
  const Int_t nstar = 3;
  Double_t Etransfstar[nstar] = {1.5, 2.0, 2.5};  
  Double_t ibfstar[nstar] = {0.35, 0.3, 0.3};  
  Double_t Eresstar[nstar] = {12.5, 12.7, 12.5};
  Double_t gainstar[nstar] = {2168., 2102., 1946.};

  TGraph* gibf = new TGraph(n, Etransf, ibf);
  TGraph* gEres = new TGraph(n, Etransf, Eres);
  TGraph* ggain = new TGraph(n, Etransf, gain);

  SetUpMyGraph(gibf, kFullCircle, 1.3, kRed+1);
  SetUpMyGraph(gEres, kFullCircle, 1.3, kBlue+1);
  SetUpMyGraph(ggain, kFullCircle, 1.3, kBlack);

  TGraph* gibfstar = new TGraph(nstar, Etransf, ibfstar);
  TGraph* gEresstar = new TGraph(nstar, Etransf, Eresstar);
  TGraph* ggainstar = new TGraph(nstar, Etransf, gainstar);

  SetUpMyGraph(gibfstar, kOpenCircle, 1.6, kRed+1);
  SetUpMyGraph(gEresstar, kOpenCircle, 1.6, kBlue+1);
  SetUpMyGraph(ggainstar, kOpenCircle, 1.6, kBlack);

  TCanvas *c = new TCanvas("c","c",700,800);
  c->Divide(1,3);

  Double_t leftMargin = 0.14;
  Double_t rightMargin = 0.02;
  Double_t topMargin = 0.04;
  Double_t bottomMargin = 0.15;

  //Double_t xFrac = (1 - leftMargin - rightMargin) / 2 + leftMargin;
  Double_t yFrac = (1 - bottomMargin - topMargin) / 3;

  TVirtualPad *pad1 = c->cd(1);
  TVirtualPad *pad2 = c->cd(2);
  TVirtualPad *pad3 = c->cd(3);

  pad1->SetPad(0., 0., 1., yFrac+bottomMargin);
  pad2->SetPad(0, yFrac+bottomMargin, 1., yFrac*2+bottomMargin);
  pad3->SetPad(0, yFrac*2+bottomMargin, 1., 1.);

  pad1->SetMargin(leftMargin, rightMargin, bottomMargin/(yFrac+bottomMargin), 0.);
  pad2->SetMargin(leftMargin, rightMargin, 0., 0.);
  pad3->SetMargin(leftMargin, rightMargin, 0, topMargin/(yFrac+topMargin));

  TH1* myBlankHisto1 = new TH1F("blank1","blank1",1000,0,5);
  myBlankHisto1->GetXaxis()->SetRangeUser(1, 4);
  myBlankHisto1->GetYaxis()->SetTitle("Energy Res. (%)");
  myBlankHisto1->GetYaxis()->SetRangeUser(8.0001, 16);
  myBlankHisto1->GetYaxis()->SetLabelFont(43);
  myBlankHisto1->GetYaxis()->SetLabelSize(23);
  myBlankHisto1->GetYaxis()->SetTitleFont(43);
  myBlankHisto1->GetYaxis()->SetTitleSize(25);
  myBlankHisto1->GetYaxis()->SetTitleOffset(1.8);
  myBlankHisto1->GetYaxis()->SetLabelOffset(0.013);

  TH1* myBlankHisto2 = new TH1F("blank2","blank2",1000,0,5);
  myBlankHisto2->GetXaxis()->SetRangeUser(1, 4);
  myBlankHisto2->GetYaxis()->SetTitle("IBF (%)      ");
  myBlankHisto2->GetYaxis()->SetRangeUser(0.001, 0.59);
  myBlankHisto2->GetYaxis()->SetLabelFont(43);
  myBlankHisto2->GetYaxis()->SetLabelSize(23);
  myBlankHisto2->GetYaxis()->SetTitleFont(43);
  myBlankHisto2->GetYaxis()->SetTitleSize(25);
  myBlankHisto2->GetYaxis()->SetTitleOffset(1.8);
  myBlankHisto2->GetYaxis()->SetLabelOffset(0.013);

  TH1* myBlankHisto3 = new TH1F("blank3","blank3",1000,0,5);
  myBlankHisto3->GetXaxis()->SetTitle("E_{transf} kV/cm");
  myBlankHisto3->GetXaxis()->SetRangeUser(1, 4);
  myBlankHisto3->GetYaxis()->SetTitle("<GA>       ");
  myBlankHisto3->GetYaxis()->SetRangeUser(1400, 2899.999);
  
  myBlankHisto3->GetYaxis()->SetLabelFont(43);
  myBlankHisto3->GetYaxis()->SetLabelSize(23);
  myBlankHisto3->GetYaxis()->SetTitleFont(43);
  myBlankHisto3->GetYaxis()->SetTitleSize(25);
  myBlankHisto3->GetYaxis()->SetTitleOffset(1.8);
  myBlankHisto3->GetYaxis()->SetLabelOffset(0.013);

  myBlankHisto3->GetXaxis()->SetLabelFont(43);
  myBlankHisto3->GetXaxis()->SetLabelSize(23);
  myBlankHisto3->GetXaxis()->SetTitleFont(43);
  myBlankHisto3->GetXaxis()->SetTitleSize(25);
  myBlankHisto3->GetXaxis()->SetTitleOffset(3.2);
  myBlankHisto3->GetXaxis()->SetLabelOffset(0.025);

  pad3->cd();
  pad3->SetGridx();
  pad3->SetGridy();
  myBlankHisto1->Draw();
  gEres->Draw("LP same");
  gEresstar->Draw("LP same");

  pad2->cd();
  pad2->SetGridx();
  pad2->SetGridy();
  myBlankHisto2->Draw();
  gibf->Draw("LP same");
  gibfstar->Draw("LP same");

  pad1->cd();
  pad1->SetGridx();
  pad1->SetGridy();
  myBlankHisto3->Draw();
  ggain->Draw("LP same");
  ggainstar->Draw("LP same");

  c->Update();
  c->SaveAs("~/EtransfScan.pdf");
}

void SetUpMyGraph(TGraph* g, Style_t marker, Double_t msize, Color_t color)
{
  g->SetMarkerStyle(marker);
  g->SetMarkerColor(color);
  g->SetLineColor(color);
  g->SetMarkerSize(msize);
}
