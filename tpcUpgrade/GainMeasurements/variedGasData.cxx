// Import data manually, because root has no better mechanism
// These should all be loaded ito TGraphs
// All gas totals add up to 100, unless otherwise specified. Thus, Ne makes up the rest of most mixtures
//
// Compile and run with:
//  g++ createGraphs.cxx -std=c++11 -g `root-config --libs --cflags` -I$HOME/install/include -o createGraphs && ./createGraphs 

#include <vector>
#include <map>
#include <algorithm>

#include <readableStyle.h>

#include <TString.h>
#include <TGraph.h>
#include <TCanvas.h>
#include <TAxis.h>
#include <TH1F.h>
#include <TLegend.h>
#include <TLegendEntry.h>
#include <TText.h>
#include <TArrow.h>

void addGraph(std::map<TString, TGraph *> * graphData, TGraph * graph, Double_t * voltage, Double_t * gain, Int_t length, TString name)
{
	graph = new TGraph(length, voltage, gain);
	graph->SetTitle(name.Data());

	// String manipulation takes "90% Ar 10% CO2" -> "90Ar_10CO2"
	graphData->insert( std::pair<TString, TGraph *> (name.ReplaceAll("% ", "").ReplaceAll(" ","_"), graph));	
	//(*graphData)[name.ReplaceAll("% ", "").ReplaceAll(" ","_")] = graph;	
}

// This assumes that a canvas is open. Be careful!!
void drawGraphs(std::map<TString, TGraph *> * graphData, TString format, Int_t loopMin, Int_t loopMax, Int_t increment, Int_t gasMixtureTotal, Int_t color = -1, Int_t markerStyle = -1)
{
	for (int i = loopMin; i <= loopMax; i+= increment)
	{
		printf("%s\n", Form(format, gasMixtureTotal-i,i));
		TGraph * graph = (*graphData)[Form(format, gasMixtureTotal-i,i)];
		if (graph == 0) { printf("Null\n"); continue; }
		if (color == -1)
		{
			graph->SetMarkerColor(i/loopMin);
			graph->SetLineColor(i/loopMin);
		}
		else
		{
			graph->SetMarkerColor(color);
			graph->SetLineColor(color);
		}
		//graph->SetMarkerStyle(i/loopMin + 1);
		//graph->SetMarkerSize(2);
		if (markerStyle == -1)
		{
			graph->SetMarkerStyle(kFullCircle + (i/loopMin - 1));
		}
		else
		{
			graph->SetMarkerStyle(markerStyle);
			if (markerStyle == kFullStar)
			{
				graph->SetMarkerSize(2);
			}
		}
		graph->Draw("LP same");
	}
}

Double_t compensatePoorCalibration(Double_t val)
{
	// Tuned to match the values at V=430 for Ne-10% CO_2
	return val*1607/55;
}

int main()
{
	// Store all TGraphs and easily lookup
	std::map<TString, TGraph *> graphData;

	// Contain voltage and gain data for each test
	std::vector<Double_t> gain;
	std::vector<Double_t> voltage;
	Int_t length = 0;
	TString name = "";

	// Needed for plotting and storing data
	TCanvas * canvas = new TCanvas();
	TGraph * graph = 0;

	// Ar CO2
	// 10% CO2
	name = "90% Ar 10% CO2";
	gain 	= { 234, 270, 376, 483, 653, 896, 1288, 1531};
	voltage = { 525, 530, 540, 550, 560, 570, 580, 585};
	length = 8;
	std::transform(gain.begin(), gain.end(), gain.begin(), compensatePoorCalibration);
	addGraph(&graphData, graph, &voltage[0], &gain[0], length, name);

	// 20% CO2
	name = "80% Ar 20% CO2";
	gain 	= { 131, 196, 261, 298, 339, 435, 579, 783, 1048, 1436};
	voltage = { 560, 570, 580, 585, 590, 600, 610, 620, 630, 640};
	length = 10;
	std::transform(gain.begin(), gain.end(), gain.begin(), compensatePoorCalibration);
	addGraph(&graphData, graph, &voltage[0], &gain[0], length, name);
	
	// 30% CO2
	name = "70% Ar 30% CO2";
	gain 	= { 215, 293, 383, 491, 630, 852, 1104};
	voltage = { 630, 640, 650, 660, 670, 680, 690};
	length = 7;
	std::transform(gain.begin(), gain.end(), gain.begin(), compensatePoorCalibration);
	addGraph(&graphData, graph, &voltage[0], &gain[0], length, name);

	
	// Ne CO2 CF4
	// 10% CO2 5% CF4
	name = "85% Ne 10% CO2 5% CF4";
	gain 	= { 62, 73, 100, 116, 180, 227, 287, 364, 475, 582, 783, 989, 1284, 1742, 2333, 3039, 3638};
	voltage = { 470, 480, 490, 500, 510, 520, 530, 540, 550, 560, 570, 580, 590, 600, 610, 620, 630};
	length = 17;
	std::transform(gain.begin(), gain.end(), gain.begin(), compensatePoorCalibration);
	addGraph(&graphData, graph, &voltage[0], &gain[0], length, name);

	// 10% CO2 1O% CF4
	name = "80% Ne 10% CO2 10% CF4";
	gain 	= { 173, 217, 280, 346, 439, 541, 688, 896, 1127, 1491, 1908, 2530, 3240};
	voltage = { 530, 540, 550, 560, 570, 580, 590, 600, 610, 620, 630, 640, 650};
	length = 13;
	std::transform(gain.begin(), gain.end(), gain.begin(), compensatePoorCalibration);
	addGraph(&graphData, graph, &voltage[0], &gain[0], length, name);

	// 10% CO2 15% CF4 
	name = "75% Ne 10% CO2 15% CF4";
	gain 	= { 75, 96, 126, 153, 202, 243, 323, 395, 494, 669, 854, 1042, 1312, 1783, 2269, 2830, 3976};
	voltage = { 520, 530, 540, 550, 560, 570, 580, 590, 600, 610, 620, 630, 640, 650, 660, 670, 680};
	length = 17;
	std::transform(gain.begin(), gain.end(), gain.begin(), compensatePoorCalibration);
	addGraph(&graphData, graph, &voltage[0], &gain[0], length, name);

	// 5% CO2 5% CF4
	name = "90% Ne 5% CO2 5% CF4";
	gain 	= { 88, 103, 142, 168, 232, 284, 370, 486, 650, 831, 1119, 1420, 2005, 2883, 4733};
	voltage = { 430, 440, 450, 460, 470, 480, 490, 500, 510, 520, 530, 540, 550, 560, 570};
	length = 15;
	std::transform(gain.begin(), gain.end(), gain.begin(), compensatePoorCalibration);
	addGraph(&graphData, graph, &voltage[0], &gain[0], length, name);

	// 5% CO2 10% CF4
	name = "85% Ne 5% CO2 10% CF4";
	gain 	= { 115, 150, 194, 242, 308, 401, 511, 643, 833, 1093, 1357, 1798, 2454, 3173, 4362};
	voltage = { 470, 480, 490, 500, 510, 520, 530, 540, 550, 560, 570, 580, 590, 600, 610};
	length = 15;
	std::transform(gain.begin(), gain.end(), gain.begin(), compensatePoorCalibration);
	addGraph(&graphData, graph, &voltage[0], &gain[0], length, name);

	// 5% CO2 15% CF4
	name = "80% Ne 5% CO2 15% CF4";
	gain 	= { 83, 100, 129, 156, 205, 277, 348, 446, 589, 758, 959, 1195, 1588, 2096, 2787, 3679, 5262};
	voltage = { 480, 490, 500, 510, 520, 530, 540, 550, 560, 570, 580, 590, 600, 610, 620, 630, 640};
	length = 17;
	std::transform(gain.begin(), gain.end(), gain.begin(), compensatePoorCalibration);
	addGraph(&graphData, graph, &voltage[0], &gain[0], length, name);


	// Ne CF4
	// 10 CF4
	name = "90% Ne 10% CF4";
	gain 	= { 108, 136, 166, 220, 268, 354, 441, 562, 717, 1003, 1305, 1923, 2896};
	voltage = { 420, 430, 440, 450, 460, 470, 480, 490, 500, 510, 520, 530, 540};
	length = 13;
	std::transform(gain.begin(), gain.end(), gain.begin(), compensatePoorCalibration);
	addGraph(&graphData, graph, &voltage[0], &gain[0], length, name);


	// Ne CO2 CH4
	// 5% CO2 5% CH4
	name = "90% Ne 5% CO2 5% CH4";
	gain 	= { 101, 115, 148, 202, 253, 333, 442, 564, 792, 952, 1326, 1869};
	voltage = { 420, 430, 440, 450, 460, 470, 480, 490, 500, 510, 520, 530};
	length = 12;
	std::transform(gain.begin(), gain.end(), gain.begin(), compensatePoorCalibration);
	addGraph(&graphData, graph, &voltage[0], &gain[0], length, name);

	// 5% CO2 10% CH4
	name = "85% Ne 5% CO2 10% CH4";
	gain 	= { 82, 91, 122, 160, 201, 270, 367, 443, 611, 751, 987, 1231, 1644, 2235};
	voltage = { 440, 450, 460, 470, 480, 490, 500, 510, 520, 530, 540, 550, 560, 570};
	length = 14;
	std::transform(gain.begin(), gain.end(), gain.begin(), compensatePoorCalibration);
	addGraph(&graphData, graph, &voltage[0], &gain[0], length, name);

	// 5% CO2 15% CH4
	name = "80% Ne 5% CO2 15% CH4";
	gain 	= { 71, 88, 113, 133, 167, 217, 284, 363, 459, 596, 792, 980, 1326, 1681, 2219};
	voltage = { 460, 470, 480, 490, 500, 510, 520, 530, 540, 550, 560, 570, 580, 590, 600};
	length = 15;
	std::transform(gain.begin(), gain.end(), gain.begin(), compensatePoorCalibration);
	addGraph(&graphData, graph, &voltage[0], &gain[0], length, name);

	// 10% CO2 5% CH4
	name = "85% Ne 10% CO2 5% CH4";
	gain 	= { 77, 90, 117, 151, 188, 226, 310, 377, 477, 628, 838, 1064};
	voltage = { 460, 470, 480, 490, 500, 510, 520, 530, 540, 550, 560, 570};
	length = 12;
	std::transform(gain.begin(), gain.end(), gain.begin(), compensatePoorCalibration);
	addGraph(&graphData, graph, &voltage[0], &gain[0], length, name);

	// 10% CO2 10% CH4
	name = "80% Ne 10% CO2 10% CH4";
	gain 	= { 110, 140, 183, 225, 297, 395, 501, 662, 849, 1104, 1324, 1817};
	voltage = { 500, 510, 520, 530, 540, 550, 560, 570, 580, 590, 600, 610};
	length = 12;
	std::transform(gain.begin(), gain.end(), gain.begin(), compensatePoorCalibration);
	addGraph(&graphData, graph, &voltage[0], &gain[0], length, name);

	// 10% CO2 15% CH4
	name = "75% Ne 10% CO2 15% CH4";
	gain 	= { 60, 83, 104, 133, 179, 238, 289, 387, 510, 650, 812, 978, 1339, 1734, 2253};
	voltage = { 500, 510, 520, 530, 540, 550, 560, 570, 580, 590, 600, 610, 620, 630, 640};
	length = 15;
	std::transform(gain.begin(), gain.end(), gain.begin(), compensatePoorCalibration);
	addGraph(&graphData, graph, &voltage[0], &gain[0], length, name);

	// 77.5 Ne 10 CO2 2.5 CH4
	name = "77.5% Ne 10% CO2 2.5% CH4";
	gain 	= { 115, 138, 178, 242, 301, 395, 505, 624, 840, 1124, 1527};
	voltage = { 480, 490, 500, 510, 520, 530, 540, 550, 560, 570, 580};
	length = 11;
	std::transform(gain.begin(), gain.end(), gain.begin(), compensatePoorCalibration);
	addGraph(&graphData, graph, &voltage[0], &gain[0], length, name);


	// Ne CH4
	// 5% CH4
	name = "95% Ne 5% CH4";
	gain 	= { 70, 89, 115, 163, 234, 330};
	voltage = { 350, 360, 370, 380, 390, 400};
	length = 6;
	std::transform(gain.begin(), gain.end(), gain.begin(), compensatePoorCalibration);
	addGraph(&graphData, graph, &voltage[0], &gain[0], length, name);

	// 10% CH4
	name = "90% Ne 10% CH4";
	gain 	= { 84, 109, 142, 188, 255};
	voltage = { 390, 400, 410, 420, 430};
	length = 5;
	std::transform(gain.begin(), gain.end(), gain.begin(), compensatePoorCalibration);
	addGraph(&graphData, graph, &voltage[0], &gain[0], length, name);

	// 15% CH4
	name = "85% Ne 15% CH4";
	gain 	= { 78, 107, 132, 173, 226};
	voltage = { 420, 430, 440, 450, 460};
	length = 5;
	std::transform(gain.begin(), gain.end(), gain.begin(), compensatePoorCalibration);
	addGraph(&graphData, graph, &voltage[0], &gain[0], length, name);

	// 15% CH4 Test 2
	name = "85% Ne 15% CH4 Test 2";
	gain 	= { 75, 96, 126, 183, 256};
	voltage = { 420, 430, 440, 450, 460};
	length = 5;
	std::transform(gain.begin(), gain.end(), gain.begin(), compensatePoorCalibration);
	addGraph(&graphData, graph, &voltage[0], &gain[0], length, name);


	// Ne CO2 C2H4
	// 90 Ne 10 CO2 10 C2H4
	name = "80% Ne 10% CO2 10% C2H4";
	gain 	= { 88, 115, 162, 176, 192, 233, 278, 356, 477, 607, 782, 979, 1134, 1230, 1443, 1573, 1862, 2008, 2468, 2845, 3514};
	voltage = { 500, 510, 520, 525, 530, 540, 550, 560, 570, 580, 590, 600, 605, 610, 615, 620, 625, 630, 635, 640, 645};
	length = 21;
	std::transform(gain.begin(), gain.end(), gain.begin(), compensatePoorCalibration);
	addGraph(&graphData, graph, &voltage[0], &gain[0], length, name);


	// Ne CO2
	// 10% CO2 April / May
	name = "90% Ne 10% CO2 1";
	gain 	= { 55, 73, 86, 109, 145, 180, 224, 298, 376, 483, 548, 613, 836, 1059, 1412, 2067, 2941};
	voltage = { 430, 440, 450, 460, 470, 480, 490, 500, 510, 520, 525, 530, 540, 550, 560, 570, 580};
	length = 17;
	std::transform(gain.begin(), gain.end(), gain.begin(), compensatePoorCalibration);
	addGraph(&graphData, graph, &voltage[0], &gain[0], length, name);

	// 10% CO2 Early June
	name = "90% Ne 10% CO2 2";
	gain 	= { 94, 129, 158, 228, 289, 390, 503, 561, 628, 746, 838, 964, 1106};
	voltage = { 460, 470, 480, 490, 500, 510, 520, 525, 530, 535, 540, 545, 550};
	length = 13;
	std::transform(gain.begin(), gain.end(), gain.begin(), compensatePoorCalibration);
	addGraph(&graphData, graph, &voltage[0], &gain[0], length, name);

	// 10% CO2 Late June
	name = "90% Ne 10% CO2 3";
	gain 	= { 105, 171, 272, 456, 828, 953, 1066, 1275, 1505, 1777, 2225};
	voltage = { 460, 480, 500, 520, 540, 545, 550, 555, 560, 565, 570};
	length = 11;
	std::transform(gain.begin(), gain.end(), gain.begin(), compensatePoorCalibration);
	addGraph(&graphData, graph, &voltage[0], &gain[0], length, name);

	// New data from Nikolai
	// No transform here since the calibration is actually correct. It probably needs a factor of 2.5 * 4.5
	name = "90% Ne 10% CO2 4";
	gain 	= { 366, 451.5, 550, 681, 841.5, 1024, 1295, 1607};
	voltage = { 360, 370, 380, 390, 400, 410, 420, 430};
	length = 8;
	addGraph(&graphData, graph, &voltage[0], &gain[0], length, name);
	
	// No transform here since the calibration is actually correct. It probably needs a factor of 2.5 * 4.5
	name = "90% Ar 10% CO2 2";
	gain 	= { 414, 526.5, 679, 876, 1136.5, 1490.5, 1955};
	voltage = { 440, 450, 460, 470, 480, 490, 500};
	length = 7;
	addGraph(&graphData, graph, &voltage[0], &gain[0], length, name);

	// Print all entries
	/*for (std::map<TString, TGraph*>::iterator it = graphData.begin(); it != graphData.end(); it++)
	{
		printf("First: %s\n", it->first.Data());
	}*/

	//////////////////////
	// Printing out Graphs
	//////////////////////
	TStyle * readableStyle = initializeReadableStyle();	
	
	// Root sucks...
	// This is the only way to plot multiple graphs together and control the limits of the plots in any sensible way...
	TH1F* myBlankHistogram = new TH1F("myBlankHistogram","myBlankHistogram", 1000, 0, 1000);
	myBlankHistogram->GetXaxis()->SetTitle("MMG Voltage (V)");
	myBlankHistogram->GetYaxis()->SetTitle("<GA>");
	myBlankHistogram->GetXaxis()->SetRangeUser(350, 700);
	myBlankHistogram->GetYaxis()->SetRangeUser(300, 200000);

	// Legend for many plots
	//TLegend * leg = new TLegend(0.15, 0.60, 0.45, 0.85);
	TLegend * leg = new TLegend(0.60, 0.15, 0.90, 0.40);
	leg->SetBorderSize(0);
	leg->SetFillStyle(0);

	/*
	// Basic entries, that vary for each plot
	TLegendEntry * legEntryFirst = leg->AddEntry((TObject*) 0, "10% CO_{2}            ", "p");
	TLegendEntry * legEntrySecond = leg->AddEntry((TObject*) 0, "20% CO_{2}            ", "p");
	TLegendEntry * legEntryThird = leg->AddEntry((TObject*) 0, "30% CO_{2}            ", "p");
	TLegendEntry * legEntryFourth = leg->AddEntry((TObject*) 0, "10% CO_{2} #2         ", "p");
	legEntryFirst->SetMarkerColor(kBlack);
	legEntrySecond->SetMarkerColor(kBlack);
	legEntryThird->SetMarkerColor(kBlack);
	legEntryFourth->SetMarkerColor(kBlack);
	legEntryFirst->SetMarkerStyle(kFullCircle);
	legEntrySecond->SetMarkerStyle(kFullCircle+1);
	legEntryThird->SetMarkerStyle(kFullCircle+2);
	legEntryFourth->SetMarkerStyle(kFullStar);
	legEntryFourth->SetMarkerSize(2);

	// Ar-CO2
	myBlankHistogram->Draw();
	TString format = "%dAr_%dCO2";
	Int_t loopMin = 10;
	Int_t loopMax = 30;
	Int_t increment = 10;
	Int_t gasMixtureTotal = 100;
	TString title = "Ar-CO_{2}";

	myBlankHistogram->SetTitle(title.Data());
	drawGraphs(&graphData, format, loopMin, loopMax, increment, gasMixtureTotal, kCyan+2);

	format = "%dAr_%dCO2_2";
	loopMin = 10;
	loopMax = 10;
	drawGraphs(&graphData, format, loopMin, loopMax, increment, gasMixtureTotal, kCyan+2, kFullStar);
	
	// Legend entries set above
	leg->Draw();

	canvas->SetLogy();
	canvas->SaveAs("Ar-CO2.pdf");
	canvas->Clear();


	// Ne CO2
	// April-March
	myBlankHistogram->Draw();
	format = "%dNe_%dCO2_1";
	loopMin = 10;
	loopMax = 10;
	increment = 10;
	gasMixtureTotal = 100;
	title = "90% Ne-10% CO_{2}";

	myBlankHistogram->SetTitle(title.Data());
	drawGraphs(&graphData, format, loopMin, loopMax, increment, gasMixtureTotal, kBlue, kFullCircle);

	// Early Juane
	format = "%dNe_%dCO2_2";
	drawGraphs(&graphData, format, loopMin, loopMax, increment, gasMixtureTotal, kBlue+1, kFullCircle+1);
	
	// Late June
	format = "%dNe_%dCO2_3";
	drawGraphs(&graphData, format, loopMin, loopMax, increment, gasMixtureTotal, kBlue+2, kFullCircle+2);

	// Nikolai
	format = "%dNe_%dCO2_4";
	drawGraphs(&graphData, format, loopMin, loopMax, increment, gasMixtureTotal, kBlue+3, kFullStar);

	// Set legend entries
	legEntryFirst->SetLabel("Measurement 1"); // "April-March 2014"
	legEntrySecond->SetLabel("Measurement 2"); // "Early June 2014"
	legEntryThird->SetLabel("Measurement 3"); // "Late June 2014"
	legEntryFourth->SetLabel( "Measurement 4"); // From Nikolai
	leg->Draw();

	canvas->SetLogy();
	canvas->SaveAs("Ne-CO2.pdf");
	canvas->Clear();
	*/

	// Combined Ar, Ne CO2
	myBlankHistogram->Draw();

	// Ar
	TString format = "%dAr_%dCO2";
	Int_t loopMin = 10;
	Int_t loopMax = 30;
	Int_t increment = 10;
	Int_t gasMixtureTotal = 100;
	TString title = "Ne-CO_{2} & Ar-CO_{2}";

	myBlankHistogram->SetTitle(title.Data());
	drawGraphs(&graphData, format, loopMin, loopMax, increment, gasMixtureTotal, kCyan+2);

	// Ne
	format = "%dNe_%dCO2_1";
	loopMin = 10;
	loopMax = 10;
	increment = 10;
	gasMixtureTotal = 100;

	// April-May
	drawGraphs(&graphData, format, loopMin, loopMax, increment, gasMixtureTotal, kBlue, kFullStar);

	// Early Juane
	format = "%dNe_%dCO2_2";
	drawGraphs(&graphData, format, loopMin, loopMax, increment, gasMixtureTotal, kBlue+1, 33); // Diamond
	
	// Late June
	format = "%dNe_%dCO2_3";
	drawGraphs(&graphData, format, loopMin, loopMax, increment, gasMixtureTotal, kBlue+2, 34); // Cross

	// Basic entries, that vary for each plot
	TLegendEntry * legEntryFirst = leg->AddEntry(graphData["90Ar_10CO2"], "90% Ar-10% CO_{2}      ", "p");
	TLegendEntry * legEntrySecond = leg->AddEntry(graphData["80Ar_20CO2"], "80% Ar-20% CO_{2}      ", "p");
	TLegendEntry * legEntryThird = leg->AddEntry(graphData["70Ar_30CO2"], "70% Ar-30% CO_{2}      ", "p");
	TLegendEntry * legEntryFourth = leg->AddEntry(graphData["90Ne_10CO2_1"], "90% Ne-10% CO_{2} #1 ", "p");
	TLegendEntry * legEntryFifth = leg->AddEntry(graphData["90Ne_10CO2_2"], "90% Ne-10% CO_{2} #2 ", "p");
	TLegendEntry * legEntrySixth = leg->AddEntry(graphData["90Ne_10CO2_3"], "90% Ne-10% CO_{2} #3 ", "p");
	
	leg->Draw();

	// Draw preliminary label
	TText * preliminary = new TText(0.2, 0.75, "Preliminary");
	preliminary->SetNDC();
	preliminary->SetTextSize(0.05);
	preliminary->Draw();

	canvas->SetLogy();
	canvas->SaveAs("Ne-CO2_Ar-CO2.pdf");
	canvas->Clear();


	// Ne-CO2-CF4
	// 5 CO2
	myBlankHistogram->Draw();
	format = "%dNe_5CO2_%dCF4";
	loopMin = 5;
	loopMax = 15;
	increment = 5;
	gasMixtureTotal = 100 - 5;
	title = "Ne-CO_{2}-CF_{4}";
	myBlankHistogram->SetTitle(title.Data());
	drawGraphs(&graphData, format, loopMin, loopMax, increment, gasMixtureTotal, kRed);

	// 10 CO2
	format = "%dNe_10CO2_%dCF4";
	gasMixtureTotal = 100 - 10;
	drawGraphs(&graphData, format, loopMin, loopMax, increment, gasMixtureTotal, kBlue);

	// No CO2
	format = "%dNe_%dCF4";
	loopMin = 10;
	loopMax = 10;
	gasMixtureTotal = 100;
	drawGraphs(&graphData, format, loopMin, loopMax, increment, gasMixtureTotal, kGreen+2, kFullCircle+1);

	// Legend entries
	// Use black for markers, but colored lines for CO2 concentration
	// Set label
	legEntryFirst->SetLabel("0% CO_{2}");
	legEntrySecond->SetLabel("5% CO_{2}");
	legEntryThird->SetLabel("10% CO_{2}");
	legEntryFourth->SetLabel("5% CF_{4}");
	legEntryFifth->SetLabel("10% CF_{4}");
	legEntrySixth->SetLabel("15% CF_{4}");
	// Set object
	legEntryFirst->SetObject(graphData["90Ne_10CF4"]);
	legEntrySecond->SetObject(graphData["90Ne_5CO2_5CF4"]);
	legEntryThird->SetObject(graphData["85Ne_10CO2_5CF4"]);
	legEntryFourth->SetObject((TObject*) 0);
	legEntryFifth->SetObject((TObject*) 0);
	legEntrySixth->SetObject((TObject*) 0);
	// Set option
	legEntryFirst->SetOption("l");
	legEntrySecond->SetOption("l");
	legEntryThird->SetOption("l");
	legEntryFourth->SetOption("p");
	legEntryFifth->SetOption("p");
	legEntrySixth->SetOption("p");
	// Set Marker colors
	legEntryFourth->SetMarkerColor(kBlack);
	legEntryFifth->SetMarkerColor(kBlack);
	legEntrySixth->SetMarkerColor(kBlack);
	// Set Marker style
	legEntryFourth->SetMarkerStyle(kFullCircle);
	legEntryFifth->SetMarkerStyle(kFullCircle+1);
	legEntrySixth->SetMarkerStyle(kFullCircle+2);
	// Set Marker size
	legEntryFourth->SetMarkerSize(1);
	legEntryFifth->SetMarkerSize(1);
	legEntrySixth->SetMarkerSize(1);

	/*legEntryFourth->SetObject(graphData["90Ne_10CF4"]);
	legEntryFourth->SetOption("l");
	leg->AddEntry(, "5% CO_{2}", "l");
	leg->AddEntry(graphData["85Ne_10CO2_5CF4"], "10% CO_{2}", "l");*/
	leg->Draw();

	preliminary->Draw();
	
	canvas->SetLogy();

	// Save canvas
	canvas->SaveAs("Ne-CO2-CF4.pdf");
	canvas->Clear();


	// Ne-CO2-CH4
	// 5 CO2
	myBlankHistogram->Draw();
	format = "%dNe_5CO2_%dCH4";
	loopMin = 5;
	loopMax = 15;
	increment = 5;
	gasMixtureTotal = 100 - 5;
	title = "Ne-CO_{2}-CH_{4}";
	myBlankHistogram->SetTitle(title.Data());
	drawGraphs(&graphData, format, loopMin, loopMax, increment, gasMixtureTotal, kRed);

	// 10 CO2
	format = "%dNe_10CO2_%dCH4";
	gasMixtureTotal = 100 - 10;
	drawGraphs(&graphData, format, loopMin, loopMax, increment, gasMixtureTotal, kBlue);

	// No CO2
	format = "%dNe_%dCH4";
	gasMixtureTotal = 100;
	drawGraphs(&graphData, format, loopMin, loopMax, increment, gasMixtureTotal, kGreen+2);

	// No CO2, 15 CH4 Test 2
	format = "%dNe_%dCH4_Test_2";
	loopMin = 15;
	loopMax = 15;
	gasMixtureTotal = 100;
	drawGraphs(&graphData, format, loopMin, loopMax, increment, gasMixtureTotal, kGreen+2, kFullCircle+2);

	// C2H4
	format = "%dNe_10CO2_%dC2H4";
	loopMin = 10;
	loopMax = 10;
	gasMixtureTotal = 100 - 10;
	drawGraphs(&graphData, format, loopMin, loopMax, increment, gasMixtureTotal, kMagenta+2, kFullStar);
	
	// Legend and labels
	// Even though this is associated with another TGraph, this should still work fine, since the classifcations are the same
	/*TText * pointLabel5 = new TText(0.43, 0.57, "5%");
	TText * pointLabel10 = new TText(0.53, 0.57, "10%");
	TText * pointLabel15 = new TText(0.62, 0.57, "15%");
	pointLabel5->SetNDC();
	pointLabel10->SetNDC();
	pointLabel15->SetNDC();
	pointLabel5->SetTextSize(0.04);
	pointLabel10->SetTextSize(0.04);
	pointLabel15->SetTextSize(0.04);
	pointLabel5->Draw();
	pointLabel10->Draw();
	pointLabel15->Draw();*/

	
	legEntryFourth->SetLabel("5% CH_{4}");
	legEntryFifth->SetLabel("10% CH_{4}");
	legEntrySixth->SetLabel("15% CH_{4}");
	leg->AddEntry(graphData["80Ne_10CO2_10C2H4"], "10% CO_{2} 10% C_{2}H_{4}", "p");
	leg->Draw();

	preliminary->Draw();
	
	canvas->SetLogy();

	// Save canvas
	canvas->SaveAs("Ne-CO2-CH4.pdf");
	canvas->Clear();

	delete myBlankHistogram;
	delete readableStyle;
}
