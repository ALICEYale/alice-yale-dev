# Active Feedback

## Setting up and Running the Program

### Program Setup/Compilation
If the program has never been run before, or the code has been edited, then setup.sh should be run via `./setup.sh`. This will create any necessary variables, configure the code, and then compile the code and install it. The script is fairly simply, so the steps should be fairly clear.

The build system is based on automake, with the primary files defined by `autogen.sh`, `configure.ac`, and `Makefile.am`. `autogen.sh` is a short script which handles setting everything up, as well as running configure with the appropriate arguments. I have tried to make it as clear as possible, so anyone with a passing familiarity should be able to handle basic changes. Regardless, any need for changes should be extremely rare.

To compile the code (ie, if it has already been setup), one may simply run the setup script again. Alternatively, one make run:

```bash
cd build
make install
cd -
```

### Configuration Setup
Once the program is setup, the necessary configuration files should be setup. At the very least, `gainConstants.txt` and `peakLocations.txt` must be setup. More information about the particulars could be found below in the section regarding configuration information. In short, determine the gain constants for the elements and set those in `gainConstants.txt`. Then look at output from the ADC and determine the rough position of the peak(s) and set the values in `peakLocations.txt`.

### Running the Program
For simple operation (it will run assuming a single peak with relative gain):

**NOTE: AS OF 7/15/2014, ONLY THE SINGLE PEAK WITH RELATIVE GAIN MODE HAS BEEN TESTED ENTIRELY!**

1. **Configure the appropriate configuration files, as described in the section above!**
2. Start the monitoring program on the ADC computer by navigating to the appropriate directory and running `python monitor.py`.
3. Run the script `runActiveFeedback.sh` via `./runActiveFeedback.sh`
  - This runs the python server `receiveFiles.py` to receive files from the ADC computer
  - That python server then runs `activeFeedback` with the appropriate options at the appropriate time, including ensuring that initialization is completed with appropriate.
4. That's it

When complete, press `Ctrl-c` to close both programs on the respective computers. Note that the voltages may have changed quite a bit, so be careful when reversing voltages back down to 0 (ie. do not just blindly run `./mpod.sh voltage -r -f <file>`. Look to see how much the voltage will change.)

NOTE: This script relies on being run from the directory in which it is placed. If it is elsewhere, then it will not monitor the correct files.

### Program Options

While the above script works, `activeFeedback` can also be called directly. To see all possible options, run `activeFeedback --help`. However, nearly all relevant options can be run via `runActiveFeedback.sh`. Those options will be described below, or can be viewed via `python src/receiveFiles.py --help`.

The general usage can be described as:
```bash
./runActiveFeedback [-d] [-t]
```
Where: `-d == --disableRelativeGain`, `-t == --triplePeak`

or
```bash
python src/receiveFiles.py [-d] [-t]
```
Where: `-d == --disableRelativeGain`, `-t == --triplePeak`

or
```bash
activeFeedback [-i] [-r] [-s] [-f] filename
```
Where: `-i == --initialize`, `-r == --relativeGain`, `-s == --singlePeak`, `-f == --filename`


Note: This executable assumes triple peak, absolute gain mode unless otherwise specified.

#### Relative Gain
This mode is used when the gain characteristics of the system are not understood or are not available. For instance, if the gain from the electronics components is not know. In this mode, fractional gains are used, with the assumption that each element contributed equally to the change in gain (ex. a 27% change was caused by a 3% gain change on each element).

Relative gain is invoked by default when running via `runActiveFeedback.sh`

If running `activeFeedback` directly, then one must pass `-r` or `--relativeGain` to enable relative gain.

#### Absolute Gain
This mode is used when the gain characteristics of the system are well understood. In this case, the specific gas composition, along with the electronics gain, is used to create the gain directly. Since we are always aware of the precise values of the gain, the correction is performed more directly. However, it is also substantially more complicated than the relative gain. In addition, it makes some assumptions utilizing user input (in `initialAbsoluteGainConfig.txt`) about the nominal gain over each element.

Absolute gain is invoked by running `runActiveFeedback.sh` with the `-d` or `--disableRelativeGain` switch.

If running `activeFeedback` directly, absolute gain is default.

#### Single Peak
This mode is used when the characteristics of the decays ensure that all decays are before the top GEM. In this case, there will only be a single peak in the ADC corresponding to amplification through the MMG and 2 GEMs. Since we obtain less information about the performance of each individual element, assumptions about the performance of the elements must be made. Those assumptions vary by the gain mode, and are described above. Nonetheless, this mode should still be effective for controlling the overall gain.

Single peak analysis is invoked by default when running via `runActiveFeedback.sh`

If running `activeFeedback` directly, then one must pass `-s` or `--singlePeak` to enable the single peak analysis.

#### Triple Peak
This mode is used when the characteristics of the decays ensure that they are spaced throughout the chamber. In this case, there will be multiple peaks, corresponding to amplification by the MMG alone, by the MMG and the mid GEM, and by the MMG, mid GEM and top GEM. Because we have each peak, we know the gain of each element, and we can correct each element individually.

**NOTE: AS OF 7/15/2014, IT IS ASSUMED THAT EACH ELEMENT HAS GAIN > 1! (IE. ASSUMING MMG IS THE LOWEST PEAK). A FLAG TO ADDRESS THIS SHOULD BE A FAIRLY TRIVIAL CHANGE, BUT IT HAS NOT YET BEEN IMPLEMENTED. BE CAREFUL.**

Triple peak analysis is invoked by running `runActiveFeedback.sh` with the `-t` or `--triplePeak` switch.

If running `activeFeedback` directly, triple peak is default.

---

## Configuration Information
Configuration files are in the config directory.

Note: The configuration files read values as \<name\>: \<value\>. It begins reading the value after the ":\<space\>". The name is irrelevant - it is the relative order that matters. Any line can be a comment, **as long as it doesn't include a colon!**

The available files are:

- `gainConstants.txt`
- `peakLocations.txt`
- `initialAbsoluteGainConfig.txt`
- `.initialRelativeGainConfig.txt`

## `gainConstants.txt`
This sets the appropriate gain factor for each element. These is fairly well described at the top of activeFeedback.cc, which is quoted below:

> The expression is G=A * exp(V/C) -> V_1-V_2 = C' * ln(G_1/G_2) where C' is different for the MMG, GEMs. By determining the slope of the gain curve, we can determine the constant.

> For ballpark values:

> - a normal MMG (at ~500 V) gets a 2x Gain for 25 V difference -> C'=25/ln(2)
> - a normal GEM (at ~200 V?) gets a 2x Gain for 36 V difference -> C'=36/ln(2). This is extracted from a triple GEM stack, so this may not be entirely accurate! The triple GEM had 2x Gain for 100 V difference over the 3 elements. A value of 45 is extracted from testing, as 36/ln(2) appears to overshoot the correction.

An example configuration is listed below:

```
mmg: 36.0674
midGEM: 45.00
topGEM: 45.00
```

## `peakLocations.txt`

To appropriately fit any peak, the fitting algorithm must be seeded with the approximate location of the peak. In testing, the values do not appear to need to be particularly precise, but they do need to include the peak. Therefore, min and max values must be set in this configuration file.

When running in single peak mode, the only values that matter are the singlePeak. The other values have no effect on the analysis.

When running in triple peak mode, the mmg, mmgGEM and mmg2GEM peaks are the relevant variables. The single peak variable can be ignored. The mmg peak is the smallest peak, while the mmgGEM peak is located in the middle (where the peak corresponds to amplification by the middle GEM and the MMG), and the mmg2GEM peak is the largest.

Example values are listed below:

- mmgPeak min, max: 70, 130
- mmgGEMPeak min, max: 160, 220
- mmg2GEMPeak min, max: 360, 420
- singlePeak min, max: 150, 600

An example configuration is listed below:

```
For the triple peak analysis, set the min and max values for the 3 peaks (order matters)
mmgPeak.min: 70
mmgPeak.max: 130
mmgGEMPeak.min: 160
mmgGEMPeak.max: 220
mmg2GEMPeak.min: 360
mmg2GEMPeak.max: 420
For the single peak analysis, set the min and max values for the single peak (order matters)
singlePeak.min: 150
singlePeak.max: 600
```

## `initialAbsoluteGainConfig.txt`

This configures the initial gain parameters when working with absolute gain. To perform in this operation mode, values for the gain from the electronics must be obtain (most likely via a test pulse). The electronics gain must be set in activeFeedback::calculateAbsoluteGainFactor(). In addition, the gas mixture and decay energy (in keV) must be entered. Lastly, the desired gains for each element must be entered. If operating in single peak mode, these will be used as the nominal values of the gain. Thus, the product of the three should be equal to the total expected gain. The corrections will then be made to return each element to the nominal gain.

An example configuration file is listed below:

```
mmgGain: 400
midGEM: 1
topGEM: 5
Neon: 90
CO2: 5
CH4: 5
C2H4: 0
CF4: 0
Decay Energy (keV): 6
```

## `.initialRelativeGainConfig.txt`

This file is used to record gains determined during initialization during relative gain operation. The file is proceeded by a . to hide it, as **the user should never have to manually edit the file!** However, if interested, the user may view the values.

For completeness, an example is below (assuming a single peak analysis):

```
singlePeakAnaylsis
mmg: 0
midGEM: 0
topGEM: 0
total (not necessarily equal): 371.438
```
