#!/usr/bin/env bash

# Check if already in path
# Functions
# Inspired by http://superuser.com/a/39995 
addToEnvironmentVariables()
{
	if [[ -d "$1" ]] && [[ ":$2:" != *":$1:"* ]]; then
		echo "$1:$2"
	else
		echo "$2"
	fi
}

addToPath()
{
	PATH=$(addToEnvironmentVariables "$1" "$PATH")
}

export MYINSTALL="$HOME/install"

addToPath "$MYINSTALL/bin"

# Runs the python script, passing all options passed to the shell script
python src/receiveFiles.py "$@"
