#/usr/bin/env bash

source "$HOME/aliceSetup/alice-env.sh" -q -n

# Set appropriate variables
export MYINSTALL="$HOME/install"

# Configure and build
cd build
./autogen.sh --prefix="$MYINSTALL"
make clean
make install
