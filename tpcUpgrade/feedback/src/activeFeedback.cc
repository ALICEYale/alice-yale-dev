// activeFeedback.cc
#include "activeFeedback.h"

namespace activeFeedback
{
	//
	// Constant definitions - Define here because rootcint can't deal with std::log in the header
	//  (because rootcint ignores namespaces...)
	//
	
	// Find the appropriate gain value of each element
	// The expression is g=A*exp(V/C) -> V_1-V_2 = C'*ln(g_1/g_2) where C' is different for the MMG, GEMs. 
	// By determining the slope of the gain curve, we can determine the constant.
	// For ballpark values:
	// 		a normal MMG (at ~500 V) gets a 2xGain for 25 V difference -> C'=25/ln(2)
	// 		a normal GEM (at ~200 V?) gets a 2xGain for 36 V difference -> C'=36/ln(2). This is extracted from a triple GEM stack,
	// 		 so this may not be entirely accurate! The triple GEM had 2xGain for 100 V difference over the 3 elements
	// These values are reasonable, but should be set by the use in gainConstants.txt
	elements gainConstants(25.0/std::log(2), 100.0/std::log(2), 100.0/std::log(2));
	
	// Set the expected area for the fit
	// These values are currently guesses
	minMax mmg2GEMPeak  = {360, 420};
	minMax mmgGEMPeak = {160, 220};
	minMax mmgPeak = {70, 130};
	minMax singlePeak = {300, 450};

	//
	// Main analysis functions
	//
	
	double fitPeak(TH1D * hist, minMax peak, std::string name, const double absoluteGainConversionFactor = 1)
	{
		// Fit to the gain curves
		TF1 * peakFit = new TF1("peakFit", "gaus", peak.min, peak.max);

		// Fit to histogram
		// R uses the range defined in the fit function
		// 0 ensures that the fit isn't drawn
		// Q ensures minimum printing
		// + adds the fit to function list to ensure that it is not deleted on the creation of a new fit
		//hist->Fit(peakFit, "RQ0+");
		hist->Fit(peakFit, "R0+");

		// Determine the current gain 
		double peakMean = peakFit->GetParameter("Mean") * absoluteGainConversionFactor;

		return peakMean;
	}

	double calculateRelativeGainCorrection(double currentGain, double desiredGain, bool singlePeak = false)
	{
		double fractionalDG = desiredGain/currentGain;
		double dG = 0;

		if (singlePeak == true)
		{	
			dG = std::pow(std::abs(fractionalDG), 1.0/3);
		}
		else
		{
			dG = fractionalDG;
		}

		return dG;
	}

	void singlePeakVoltageFromGain(double currentGain, double initialGain, const elements & initialGains, elements & voltageAdjustments, bool relativeGain)
	{
		double mmgDG = 0;
		double midGEMDG = 0;
		double topGEMDG = 0;

		if (relativeGain == false)
		{
			double dG = calculateSinglePeakGain(currentGain, initialGains);

			mmgDG = initialGains.mmg/(initialGains.mmg + dG);
			midGEMDG = initialGains.midGEM/(initialGains.midGEM + dG);
			topGEMDG = initialGains.topGEM/(initialGains.topGEM + dG);
		}
		else
		{
			double dG = calculateRelativeGainCorrection(currentGain, initialGain, true);

			mmgDG = dG;
			midGEMDG = dG;
			topGEMDG = dG;
		}

		// Find the appropriate gain value of each element
		// The expression is g=A*exp(V/C) -> V_1-V_2 = C'*ln(g_1/g_2) where C' is different for the MMG, GEMs
		// The constant gain factors are defined in the header
		voltageAdjustments.mmg = gainConstants.mmg * std::log(mmgDG);
		voltageAdjustments.midGEM = gainConstants.midGEM * std::log(midGEMDG);
		voltageAdjustments.topGEM = gainConstants.topGEM * std::log(topGEMDG);
	}


	// Single is for when we can only get one peak, while triple is for when we have a stronger source
	void singlePeakAnalysis(TH1D * hist, const double initialGain, const elements & initialGains, const double absoluteGainConversionFactor, const bool relativeGain, const bool initialize)
	{
		// Determine the current gain and the change from the nominal gain
		double currentGain = fitPeak(hist, singlePeak, "peakFit", absoluteGainConversionFactor);

		if (initialize == true)
		{
			// Save the data
			// Implicitly, (double, 0)
			saveInitialRelativeGainData(currentGain, initialGains);
		}
		else
		{
			elements voltageAdjustments;
			singlePeakVoltageFromGain(currentGain, initialGain, initialGains, voltageAdjustments, relativeGain);

			/*std::cout << "mmgVoltageAdjustment: " << voltageAdjustments.mmg << std::endl;
			std::cout << "midGEMVoltageAdjustment: " << voltageAdjustments.midGEM << std::endl;
			std::cout << "topGEMVoltageAdjustment: " << voltageAdjustments.topGEM << std::endl;*/

			// Set the elements with std::system calls to mpod
			setVoltages(voltageAdjustments.mmg, voltageAdjustments.midGEM, voltageAdjustments.topGEM);
		}
	}

	void triplePeakVoltageFromGain(const elements & initialGains, const elements & currentGains, elements & voltageAdjustments, bool relativeGain)
	{
		double mmgDG = 0;
		double midGEMDG = 0;
		double topGEMDG = 0;

		if (relativeGain == false)
		{
			mmgDG = initialGains.mmg/currentGains.mmg;
			midGEMDG = initialGains.midGEM/currentGains.midGEM;
			topGEMDG = initialGains.topGEM/currentGains.topGEM;
		}
		else
		{
			mmgDG = calculateRelativeGainCorrection(currentGains.mmg, initialGains.mmg);
			midGEMDG = calculateRelativeGainCorrection(currentGains.midGEM, initialGains.midGEM);
			topGEMDG = calculateRelativeGainCorrection(currentGains.topGEM, initialGains.topGEM);
		}
		
		// Find the appropriate gain value of each element
		// The expression is g=A*exp(V/C) -> V_1-V_2 = C'*ln(g_1/g_2) where C' is different for the MMG, GEMs
		// The constant gain factors are defined in the header
		voltageAdjustments.mmg = gainConstants.mmg * std::log(mmgDG);
		voltageAdjustments.midGEM = gainConstants.midGEM * std::log(midGEMDG);
		voltageAdjustments.topGEM = gainConstants.topGEM * std::log(topGEMDG);
	}

	void triplePeakAnalysis(TH1D * hist, const elements & initialGains, const double absoluteGainConversionFactor, const bool relativeGain, const bool initialize)
	{
		// Determine the gain on each individual element
		// The MMG is only relative to the ADC channel, so it must be converted to absolute gain
		// May need to fit the first peak as well...?
		elements currentGains;
		currentGains.mmg = fitPeak(hist, mmgPeak, "mmg", absoluteGainConversionFactor); 
		currentGains.midGEM = fitPeak(hist, mmgGEMPeak, "mmgGEM", absoluteGainConversionFactor); 
		currentGains.topGEM = fitPeak(hist, mmg2GEMPeak, "mmg2GEM", absoluteGainConversionFactor);

		// Note: initialize == true && relativeGain == false should never get to this point, so this check is perhaps overly specific.
		// If initialization becomes meaingful for absolute, then this will matter.
		if (initialize == true) 
		{
			// Save the data!
			saveInitialRelativeGainData(0, currentGains);
		}
		else
		{
			elements voltageAdjustments;
			triplePeakVoltageFromGain(initialGains, currentGains, voltageAdjustments, relativeGain);

			std::cout << "mmg: " << currentGains.mmg << std::endl;
			std::cout << "midGem: " << currentGains.midGEM << std::endl;
			std::cout << "topGem: " << currentGains.topGEM << std::endl;

			/*std::cout << "mmgVoltageAdjustment: " << voltageAdjustments.mmg << std::endl;
			std::cout << "midGEMVoltageAdjustment: " << voltageAdjustments.midGEM << std::endl;
			std::cout << "topGEMVoltageAdjustment: " << voltageAdjustments.topGEM << std::endl;*/
			
			// Set the elements with std::system calls to mpod
			setVoltages(voltageAdjustments.mmg, voltageAdjustments.midGEM, voltageAdjustments.topGEM);
		}
	}

	//
	// Helper functions for calculating gain 
	//
	
	// Calculating the single peak gain is rather involved, because the expression is multiplicative
	// It can be performed as follows:
	/*
	   dG1 = MMG, dG2 = midGEM, dG3 = topGEM 
	   deltaG = dG1*G2*G3 + G1*dG2*G3 + G1*G2*dG3 + G1*dG2*dG3 + dG1*G2*dG3 + dG1*dG2*G3 + dG1*dG2*dG3
	   For dG1=dG2=dG3=dG, dG^2(G1+G2+G3) + dG(G1*G2 + G2*G3 + G3*G1) - deltaG = 0 -> Quadratic Equation

	   For conveience, I'm usnig this to solve the quadratic equation: http://stackoverflow.com/a/900119
	   This is mainly used to avoid the computational issues associated with solving the quadratic equation
	   on a computer.
		 temp = -0.5 * (b + sign(b) * sqrt(b*b - 4*a*c));
		 x1 = temp / a;
		 x2 = c / temp;
	*/
	double calculateSinglePeakGain(double finalGain, const elements & initialGains)
	{
		double dG = 0;
		double initialGain = initialGains.mmg*initialGains.midGEM*initialGains.topGEM;
		//double initialGain = initialMMGGain*initialMidGEMGain*initialTopGEMGain;
		
		// Solve quadratic equation
		// Variables
		double a = initialGains.mmg + initialGains.midGEM + initialGains.topGEM;
		double b = initialGains.mmg*initialGains.midGEM + initialGains.midGEM*initialGains.topGEM + initialGains.topGEM*initialGains.mmg;
		/*double a = initialMMGGain + initialMidGEMGain + initialTopGEMGain;
		double b = initialMMGGain*initialMidGEMGain + initialMidGEMGain*initialTopGEMGain + initialTopGEMGain*initialMMGGain;*/
		// *-1 because c is negative, while the solution assumes it to be positive (see the expression above)
		double c = -1*(finalGain - initialGain);
		// Solution
		double temp = -0.5 * (b + sgn(b) * std::sqrt(b*b - 4*a*c));
		double x1 = temp / a;
		double x2 = c / temp;

		if (std::abs(x1) > std::abs(x2))
		{
			dG = x2;
		}
		else
		{
			dG = x1;
		}

		if (std::abs(dG) >= 1)
		{
			// This may not actually be the case, although it seems plausible.
			std::cout << "\t\tWARNING! Unphysical estimate of the gain! dG = " << dG << std::endl;
		}
		std::cout << "dG: " << dG << std::endl;

		return dG;
	}

	//
	// Voltage setting functions
	//
	
	// Check voltages for appropriate values (ie not too large), and then then set them as appropriate
	void setVoltages(double mmgVoltageAdjustment, double midGEMVoltageAdjustment, double topGEMVoltageAdjustment)
	{
		// Check voltage is within limits reasonable limits before changing
		bool mmgSafeVoltage = checkSafeVoltage("MMG", mmgVoltageAdjustment, 12.5);
		bool midGEMSafeVoltage = checkSafeVoltage("Mid GEM", midGEMVoltageAdjustment, 50);
		bool topGEMSafeVoltage = checkSafeVoltage("Top GEM", topGEMVoltageAdjustment, 50);

		// Only set voltages if they are reasonable
		if ((mmgSafeVoltage == true) && (midGEMSafeVoltage == true) && (topGEMSafeVoltage == true))
		{
			// Set MMG
			addVoltage(1, 1, mmgVoltageAdjustment);

			// Set mid GEM
			addVoltage(1, 2, mmgVoltageAdjustment);
			addVoltage(1, 3, (mmgVoltageAdjustment + midGEMVoltageAdjustment));
			
			// Set top GEM
			addVoltage(1, 4, (mmgVoltageAdjustment + midGEMVoltageAdjustment));
			addVoltage(1, 5, (mmgVoltageAdjustment + midGEMVoltageAdjustment + topGEMVoltageAdjustment));

			// Set Screen and Cath voltage
			addVoltage(1, 6, (mmgVoltageAdjustment + midGEMVoltageAdjustment + topGEMVoltageAdjustment));
			addVoltage(1, 7, (mmgVoltageAdjustment + midGEMVoltageAdjustment + topGEMVoltageAdjustment));
			addVoltage(1, 8, (mmgVoltageAdjustment + midGEMVoltageAdjustment + topGEMVoltageAdjustment));
		}
	}

	// Perform a check of the voltages to ensure that they are reasonable.
	bool checkSafeVoltage(std::string element, double voltage, double voltageLimit)
	{
		bool returnValue = false;
		if (std::abs(voltage) > voltageLimit)
		{
			std::cout << "\t\tWARNING: " << element << " voltage adjustment is larger than " << voltageLimit << " V, suggesting an adjustment of the gain by greater than 50%. Aborting setting of the voltages!" << std::endl;
			returnValue = false;
		}
		else
		{
			returnValue = true;
		}
		
		return returnValue;
	}

	// Perform the system call to change the voltage
	void addVoltage(int slot, int channel, double voltage)
	{
		std::stringstream shellCommand;
		shellCommand << "cd ../; ./mpod.sh voltage -a -s " << slot << " -c " << channel << " -v " << voltage;
		std::cout << "shellCommand: " << shellCommand.str() << std::endl;

		if(std::system(shellCommand.str().c_str()) != 0)
		{
			std::cout << "Failed to set voltage" << std::endl;
		}
	}

	//
	// Helper functions for retreiving data
	//
	
	void extractValuesFromFile(std::string filename, std::vector <double> & values)
	{
		std::ifstream inFile(filename.c_str());

		std::string line;

		if (inFile.is_open())
		{
			// Grab the values from the file and put them into an vector 
			while(std::getline(inFile, line))
			{
				std::stringstream sLine(line);
				std::string tempString = "";
				double tempNumber = 0;

				tempString = sLine.str();
				// If there is no colon, then the line is ignored.
				unsigned long colonPosition = tempString.find(":");
				if (colonPosition != std::string::npos)
				{
					tempString = tempString.substr(colonPosition + 1);
					sLine.str(tempString);
					sLine >> tempNumber;

					values.push_back(tempNumber);
				}
			}

			inFile.close();
		}
		else
		{
			std::cout << "Failed to open file " << filename << " at line " << __LINE__ << " in " << __PRETTY_FUNCTION__ << std::endl;
			std::exit(-1);
		}
	}

	// Grab the nominal gain settings from a file.
	// Each setting is given as "Name: Value"
	void loadInitialAbsoluteGainData(elements & initialGains, gasComponents & gas, double & decayEnergy, std::string filename)
	{
		std::vector <double> values;
		extractValuesFromFile(filename, values);
		
		// Only fill if the vector is the right size. Otherwise, print error and exit
		if (values.size() == 9)
		{
			initialGains.mmg = values.at(0);
			initialGains.midGEM = values.at(1);
			initialGains.topGEM = values.at(2);
			gas.ne = values.at(3);
			gas.co2 = values.at(4);
			gas.ch4 = values.at(5);
			gas.c2h4 = values.at(6);
			gas.cf4 = values.at(7);
			decayEnergy = values.at(8);
		}
		else
		{
			std::cout << "Could not determine initial gain and gas component values. Exiting!" << std::endl;
			std::exit(-1);
		}
	}

	// This is mostly duplicated from the function above, just with different variables
	void loadInitialRelativeGainData(double & initialTotalGain, elements & initialGains, bool singlePeak, std::string filename)
	{
		std::vector <double> values;
		extractValuesFromFile(filename, values);

		// Only fill if the vector is the right size. Otherwise, print error and exit
		if (values.size() == 4)
		{
			initialGains.mmg = values.at(0);
			initialGains.midGEM = values.at(1);
			initialGains.topGEM = values.at(2);
			initialTotalGain = values.at(3);
		}
		else
		{
			std::cout << "Could not determine initial gain values in file " << filename << ". Exiting!" << std::endl;
			std::exit(-1);
		}
	}

	void saveInitialRelativeGainData(const double currentGain, const elements currentGains, std::string filename)
	{
		std::ofstream outFile(filename.c_str());

		if (outFile.is_open())
		{
			if (currentGain == 0)
			{
				outFile << "triplePeakAnalysis" << std::endl;
			}
			else
			{
				outFile << "singlePeakAnaylsis" << std::endl;
			}
			
			outFile << "mmg: " << currentGains.mmg << std::endl;
			outFile << "midGEM: " << currentGains.midGEM << std::endl;
			outFile << "topGEM: " << currentGains.topGEM << std::endl;
			outFile << "total (not necessarily equal): " << currentGain << std::endl;

			outFile.close();
		}
		else
		{
			std::cout << "Failed to open file " << filename << " at line " << __LINE__ << " in " << __PRETTY_FUNCTION__ << std::endl;
			std::exit(-1);
		}
	}

	void loadGainConstants(std::string filename)
	{
		std::vector <double> values;
		extractValuesFromFile(filename, values);

		if (values.size() == 3)
		{
			gainConstants.mmg = values.at(0);
			gainConstants.midGEM = values.at(1);
			gainConstants.topGEM = values.at(2);
		}
		else
		{
			std::cout << "Could not determine initial gain values in file " << filename << ". Exiting!" << std::endl;
			std::exit(-1);
		}
	}

	void loadPeakLocations(std::string filename)
	{
		std::vector <double> values;
		extractValuesFromFile(filename, values);

		if (values.size() == 8)
		{
			mmgPeak.min = values.at(0);
			mmgPeak.max = values.at(1);
			mmgGEMPeak.min = values.at(2);
			mmgGEMPeak.max = values.at(3);
			mmg2GEMPeak.min = values.at(4);
			mmg2GEMPeak.max = values.at(5);
			singlePeak.min = values.at(6);
			singlePeak.max = values.at(7);
		}
		else
		{
			std::cout << "Could not determine peak locations in file " << filename << ". Exiting!" << std::endl;
			std::exit(-1);
		}
	}

	// Used to calculate the absolute gain factor, which is dependent on the gas composition
	double calculateAbsoluteGainConversionFactor(const gasComponents & gas, double decayEnergy)
	{
		// This needs to be determined with a test pulse!
		const double electronicsGain = 1;

		// Conversion factors from Michael's excel calculations, which came from the table of values
		const double neFactor = 50.0/1.45;
		const double co2Factor = 100.0/3.35;
		const double ch4Factor = 54.0/1.61;
		const double c2h4Factor = 112/2.91;
		const double cf4Factor = 120.0/6.38;

		// Total gas flow
		double gasTotal = gas.ne + gas.co2 + gas.ch4 + gas.c2h4 + gas.cf4;

		// Determine the gas factor using all of the components.
		// It is fine to include all of the gases, because any gas not used will be set to 0, and therefore will not contribute.
		double gasFactor = gas.ne/gasTotal*neFactor + gas.co2/gasTotal*co2Factor + gas.ch4/gasTotal*ch4Factor + gas.c2h4/gasTotal*c2h4Factor + gas.cf4/gasTotal*cf4Factor;

		// Obtain the total gas gain factor
		gasFactor = gasFactor * decayEnergy;
		
		double gainConversionFactor = electronicsGain + gasFactor;
		return gainConversionFactor;
	}

	// File in a histogram from the gain input file
	TH1D * readFileIntoHist(std::string filename)
	{
		// Using: http://stackoverflow.com/a/7868998
		std::ifstream inFile(filename.c_str());
		
		TH1D * hist = new TH1D("gain","Gain", 1024, 0, 1024);

		int channel = 0;
		while (inFile >> channel)
		{
			hist->Fill(channel);
		}

		inFile.close();

		return hist;
	}
}
