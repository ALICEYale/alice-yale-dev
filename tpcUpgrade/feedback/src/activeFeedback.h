#ifndef ACTIVEFEEDBACK_H
#define ACTIVEFEEDBACK_H

#include <cmath>
#include <string>
#include <sstream>
#include <iostream>

#include <fstream>
#include <cstdlib>

// Root
#include <TH1D.h>
#include <TF1.h>
#include <TCanvas.h>
#include <TMath.h>

namespace activeFeedback
{
	struct elements 
	{
		elements(): mmg(0), midGEM(0), topGEM(0) {}
		elements(double _mmg, double _midGEM, double _topGEM): mmg(_mmg), midGEM(_midGEM), topGEM(_topGEM) {}
		double mmg;
		double midGEM;
		double topGEM;
	};

	struct gasComponents
	{
		gasComponents() : ne(90), co2(10), ch4(0), c2h4(0), cf4(0) {}
		double ne;
		double co2;
		double ch4;
		double c2h4;
		double cf4;
	};

	// Main analysis functions
	// Single is for when we can only get one peak, while triple is for when we have a stronger source
	void singlePeakAnalysis(TH1D * hist, const double initialGain, const elements & initialGains, const double absoluteGainConversionFactor, const bool relativeGain, const bool initialize);
	void triplePeakAnalysis(TH1D * hist, const elements & initialGains, const double absoluteGainConversionFactor, const bool relativeGain, const bool initialize);

	// Helper functions for calculating gain 
	double calculateSinglePeakGain(double totalGain, const elements & initialGains);

	// Voltage setting functions
	void setVoltages(double mmgVoltageAdjustment, double midGEMVoltageAdjustment, double topGEMVoltageAdjustment);
	bool checkSafeVoltage(std::string element, double voltage, double voltageLimit);
	void addVoltage(int slot, int channel, double voltage);

	// Helper functions for retreiving data
	void loadInitialAbsoluteGainData(elements & initialGains, gasComponents & gas, double & decayEnergy, std::string filename = "config/initialAbsoluteGainConfig.txt");
	void loadInitialRelativeGainData(double & initialTotalGain, elements & initialGains, bool singlePeak, std::string filename = "config/.initialRelativeGainConfig.txt");
	void saveInitialRelativeGainData(const double currentGain, const elements currentGains, std::string filename = "config/.initialRelativeGainConfig.txt");
	void loadGainConstants(std::string filename = "gainConstants.txt");
	void loadPeakLocations(std::string filename = "peakLocations.txt");
	// Ignoring Argon for now
	double calculateAbsoluteGainConversionFactor(const gasComponents & gas, const double decayEnergy);
	TH1D * readFileIntoHist(std::string filename);

	// Helper objects
	// From: http://stackoverflow.com/a/4609795
	template <typename T> int sgn(T val)
	{
		return (T(0) < val) - (val < T(0));
	}

	struct minMax
	{
		int min;
		int max;
	};
}

#endif /* ACTIVEFEEDBACK_H */
