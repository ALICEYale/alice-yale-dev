/*
 * Active Feedback program for controlling the gain of the 2GEMs+MMG setup.
 * It is already assumed the files have been moved locally somehow.
*/

#include "activeFeedback.h"

#include <string>
#include <iomanip>

#include <iostream>
#include <fstream>

// For ROOT
#include <TH1D.h>

// For testing purposes - Can be removed
//#include <TStyle.h>
//#include <TCanvas.h>
// Include the readableStyle.h file in the include directory by installing into the include path.
// It will make printing in root more pleasent. However, it is not required. 
//#include <readableStyle.h>

void printHelp(std::string input = "");
void parseInput(int argc, char * argv[], bool & initialize, bool & relativeGain, bool & singlePeak, std::string & filename);

int main(int argc, char * argv[])
{
	// Define variables and parse them from the command line options
	std::string filename = "";
	bool initialize = false;
	bool relativeGain = false;
	bool singlePeak = false;
	parseInput(argc, argv, initialize, relativeGain, singlePeak, filename);

	if (filename == "")
	{
		std::cout << "Filename must be set!" << std::endl;
		printHelp();
	}

	// Read the file into a root file
	TH1D * hist = activeFeedback::readFileIntoHist(filename);
	
	// Just code here for test purposes
	/*TStyle * readableStyle = initialzieReadableStyle();
	TCanvas canvas("canvas", "Canvas");
	hist->Draw();
	canvas.Print("test.pdf");
	delete readableStyle;
	readableStyle = 0;*/

	std::string initialFilename = "";
	double initialTotalGain = 0;
	activeFeedback::elements initialGains;
	activeFeedback::gasComponents gas;
	double decayEnergy = 0;
	
	// Load constants determined by gain curves
	activeFeedback::loadGainConstants("config/gainConstants.txt");
	// Load location of peaks
	activeFeedback::loadPeakLocations("config/peakLocations.txt");

	double absoluteGainConversionFactor = 1;
	if (relativeGain == false)
	{
		initialFilename = "config/initialAbsoluteGainConfig.txt";
		activeFeedback::loadInitialAbsoluteGainData(initialGains, gas, decayEnergy, initialFilename);
		absoluteGainConversionFactor = activeFeedback::calculateAbsoluteGainConversionFactor(gas, decayEnergy);

		// Not really a proper initialization function. Rather, it just prints information when it is called.
		if (initialize == true)
		{
			std::cout << "WARNING!: Calling initialization without relative gain, which is meaningless (not implemented)! This will always show up during the initialization phase when using absolute gain. Printing settings and exiting!" << std::endl;

			// Print settings
			std::cout << "Relative gain?: "; if (relativeGain == true) {std::cout << "True" << std::endl;}
											 else { std::cout << "False" << std::endl; }
			std::cout << "Initial Gain settings: " << std::endl;
			std::cout << "\tMMG Gain: " << initialGains.mmg << std::endl;
			std::cout << "\tMid GEM Gain: " << initialGains.midGEM << std::endl;
			std::cout << "\tTop GEM Gain: " << initialGains.topGEM << std::endl;
			std::cout << "Gas Components: " << std::endl;
			std::cout << "\tNeon: " << gas.ne << std::endl;
			std::cout << "\tCO2: " << gas.co2 << std::endl;
			std::cout << "\tCH4: " << gas.ch4 << std::endl;
			std::cout << "\tC2H4: " << gas.c2h4 << std::endl;
			std::cout << "\tCF4: " << gas.cf4 << std::endl;
			std::cout << "Decay Energy (KeV): " << decayEnergy << std::endl;
			std::cout << "Gain Conversion Factor: " << absoluteGainConversionFactor << std::endl;

			std::exit(-1);
		}
	}
	else
	{
		initialFilename = "config/.initialRelativeGainConfig.txt";
		if (initialize == true)
		{
			// Print settings
			std::cout << "Relative gain?: "; if (relativeGain == true) {std::cout << "True" << std::endl;}
											 else { std::cout << "False" << std::endl; }
			std::cout << "Initial Gain settings: " << std::endl;
			std::cout << "\tMMG Gain: " << initialGains.mmg << std::endl;
			std::cout << "\tMid GEM Gain: " << initialGains.midGEM << std::endl;
			std::cout << "\tTop GEM Gain: " << initialGains.topGEM << std::endl;
			std::cout << "\tInitial Total Gain: " << initialTotalGain << std::endl;
			std::cout << "\t\tAll these factors should be 0" << std::endl;
			std::cout << "Gain Conversion Factor: " << absoluteGainConversionFactor << std::endl;
			std::cout << "\t\tThis factor should be 1 when using relative gain" << std::endl;
		}
		else
		{
			activeFeedback::loadInitialRelativeGainData(initialTotalGain, initialGains, singlePeak, initialFilename);
		}
	}

	if (singlePeak == true)
	{
		activeFeedback::singlePeakAnalysis(hist, initialTotalGain, initialGains, absoluteGainConversionFactor, relativeGain, initialize);
	}
	else
	{
		activeFeedback::triplePeakAnalysis(hist, initialGains, absoluteGainConversionFactor, relativeGain, initialize);
	}

	delete hist;

	return 0;
}

// Process command line inputs. This is not nearly as robust as using something like Boost::program_options, but that seemed like 
// a bit heavy for this instance, making program_options less useful. The code here is simple, but it worked well and allowed 
// for quick development, while getting the job done.
void parseInput(int argc, char * argv[], bool & initialize, bool & relativeGain, bool & singlePeak, std::string & filename)
{
	if (argc == 1)
	{
		std::cout << "Error: Must specify an option!" << std::endl;
		printHelp();
	}
	
	for (int i=1; i < argc; i++)
	{
		if ((argv[i] == std::string("--help")) || (argv[i] == std::string("-h")))
		{
			// Displays help
			printHelp();
		}
		if ((argv[i] == std::string("--initialize")) || (argv[i] == std::string("-i")))
		{
			// Initialize the data
			initialize = true;
			continue;
		}
		if ((argv[i] == std::string("--relativeGain")) || (argv[i] == std::string("-r")))
		{
			// Initialize the data
			relativeGain = true;
			continue;
		}
		if ((argv[i] == std::string("--singlePeak")) || (argv[i] == std::string("-s")))
		{
			// Initialize the data
			singlePeak = true;
			continue;
		}
		if ((argv[i] == std::string("--filename")) || (argv[i] == std::string("-f")) )
		{
			// Sets input filename
			if (argc > i+1)
			{
				filename = argv[i+1];
				i++;
				continue;
			}
			else
			{
				std::cout << "A filename must be passed!" << std::endl;
				printHelp();
			}
		}
		else
		{
			printHelp(argv[i]);
		}
	}
}

// Print help for using pionDecay and exit afterwards
void printHelp(std::string input)
{
	if (input != "")
	{
		std::cout << "Invalid option: " << input << std::endl;
	}
	
	// Prints help message
	// The formatting below is carefully written, but not nearly as fraigle as it used to be.
	std::cout << "Help: This output describes the possible parameters. Filename and singlePeak must be set!" << std::endl;
	std::cout << "Note: Any options that are not explicitly set are assumed to be default." << std::endl << std::endl;
	std::cout << "Valid options:" << std::endl
			  << std::setw(5) << std::left << "\t-h" << "\t--help" 
				<< "\t\t-> Displays this help message" << std::endl
			  << std::setw(5) << std::left << "\t-i" << "\t--initialize" 
				<< "\t-> Records the current values of the gains to use as comparison in the future. This should be called at the start of every run." << std::endl
			  << std::setw(5) << std::left << "\t-r" << "\t--relativeGain" 
				<< "\t-> Sets whether the analysis uses relative gain, rather than absolute. Until test peaks are used to determine absolute gain, relative gain should always be used." << std::endl			
			  << std::setw(5) << std::left << "\t-s" << "\t--singlePeak" 
				<< "\t-> Sets whether the analysis to assume a single peak (rather than a triple peak)." << std::endl
			  << std::setw(5) << std::left << "\t-f" << "\t--filename"
				<< "\t-> Sets the input filename." << std::endl << std::endl;
				
	std::exit(-1);
}
