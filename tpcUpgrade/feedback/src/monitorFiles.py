#!/usr/bin/env python
import sys
import time
from watchdog.observers import Observer
from watchdog.events import PatternMatchingEventHandler

import ntpath
import requests
import socket

def sendRequest(filename):
    if socket.gethostname() == "ray-ENVY4" or socket.gethostname() == "rehlers-vm":
        baseURL = 'http://127.0.0.1:8080/gainData' 
    else:
        baseURL = 'http://169.254.8.53:8080/gainData' 
    try:
        r = requests.post(baseURL, files={'textFile': open(filename, 'rb')})
        returnValue = r.content
    except requests.exceptions.RequestException:
        returnValue = "failed"
    return returnValue

# Closely following http://brunorocha.org/python/watching-a-directory-for-file-changes-with-python.html
class textFileHandler(PatternMatchingEventHandler):
    patterns = ["*.txt"]

    def process(self, event):
        # Test that the filenames are correct (ie test1.txt or test2.txt)
        if event.is_directory == True:
            return

        # From: http://stackoverflow.com/a/8384788
        path = ntpath.basename(event.src_path)
        if path != "test1.txt" and path != "test2.txt":
            #print "Invalid filename", path
            return

        # Send the file over to the server script
        # From: http://stackoverflow.com/a/10234640
        # Retry if the request failed
        requestCounter = 0
        while requestCounter < 4:
            r = sendRequest(event.src_path)
            if r == "success":
                break
            else:
                print r
            requestCounter += 1
            # Wait for 2 seconds before trying again
            time.sleep(2)

        if requestCounter == 4:
            print 'Sending file "' + path + '" failed at ' + time.ctime(time.time()) + "!"
        #else:
            # Success, so return nothing
            #print "Success!"

        #print event.src_path, event.event_type  # print now only for debug

    # May end up being better to use on_any_event(self, event), but we will see if it works...
    def on_any_event(self, event):
        self.process(event)


def monitorFiles():
    # Monitor files using watchdog
    # Watch for the most recent modified file 
    observer = Observer()
    eventHandler = textFileHandler()
    observer.schedule(eventHandler, path="testData")
    observer.start()
    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        observer.stop()
    observer.join()

if __name__ == "__main__":
    monitorFiles()
