#!/usr/bin/env python
# For the webserver
import cherrypy

# To check the hostname
import socket

# To launch the cpp
import subprocess

# To deal with paths
import ntpath

# To deal with command line arguments
import sys

# To deal with nicely printing the help message
from textwrap import dedent

# Yes, I know global variables are bad. However, this is complicated substantially by Cherrypy.
# To ensure everything works correctly, it is best to use the simple solution
disableRelativeGain = False
singlePeak = True

def printHelp(argument=""):
    if argument != "":
        print "Unrecognized argument \"%s\"!" % argument

    help = """\
    Usage information:
    \t-h\t--help\t\t\t-> Describes this help message
    \t-d\t--disableRelativeGain\t-> Enables absolute gain (disabling relative gain)
    \t-t\t--triplePeak\t\t-> Enables triple peak analysis (as opposed to single peak)
    """

    print dedent(help)
    exit(-1)

# Parse the command line arguments
def parseArguments(arguments):
    # Define the variables as global to ensure that their values are correct 
    # See: http://stackoverflow.com/a/12666008
    global disableRelativeGain
    global singlePeak

    # Parse arguments
    for arg in arguments[1:]:
        arg = str(arg)
        if arg == "-d" or arg == "--disableRelativeGain":
            disableRelativeGain = True
        elif arg == "-t" or arg == "--triplePeak":
            singlePeak = False
        elif arg == "-h" or arg == "--help":
            printHelp()
        else:
            printHelp(arg)

# This class will receive the file and execute the cpp
class receiveFiles(object):
    # Constant to check whether the code needs to be initialized
    firstReceive = True

    @cherrypy.expose
    def gainData(self, textFile):
        # Define the variables as global to ensure that their values are correct
        # See: http://stackoverflow.com/a/12666008
        global disableRelativeGain
        global singlePeak

        # Check filename
        filename = ntpath.basename(textFile.filename)
        if filename != "test1.txt" and filename != "test2.txt":
            return "Invalid filename -> Unlikely to be the appropriate file"

        # From: http://stackoverflow.com/q/15626441
        # Apparently this was nearly exactly from the cherrypy docs, although I cannot find the particular page
        size = 0
        totalData = ''
        while True:
            data = textFile.file.read(8192)
            totalData += data
            if not data:
                break
            size += len(data)

        if size <= 0:
            return "Size == 0. No file was transmitted"

        # Save the transfered file
        filename = "data/" + filename
        saveFile = open(filename, "wb")
        saveFile.write(totalData)
        saveFile.close()

        # Call the cpp file to check the gains and conduct the activefeedback
        programCall = ["activeFeedback"];

        if disableRelativeGain == False:
            programCall.append("--relativeGain");
        if singlePeak == True:
            programCall.append("--singlePeak");

        # Allows intialization for the first run
        if receiveFiles.firstReceive == True:
            endOfCall = ["--initialize", "-f", filename]
            receiveFiles.firstReceive = False
        else:
            endOfCall = ["-f", filename]
        
        programCall = programCall + endOfCall

        print "programCall:", programCall

        # Call cpp
        returnCode = subprocess.call(programCall)

        return "success"

if __name__ == "__main__":
    # Parse the command line arguments
    if len(sys.argv) > 3:
        print "Too many arguments!"
        print "Arguments: %s" % str(sys.argv)
        printHelp()
    else:
        parseArguments(sys.argv)
    
    print "disableRelativeGain:", disableRelativeGain
    print "singlePeak:", singlePeak

    if socket.gethostname() == "ray-ENVY4" or socket.gethostname() == "rehlers-vm":
        cherrypy.config.update({'server.socket_host': '127.0.0.1',
                                'server.socket_port': 8080,
                                })
    else:
        cherrypy.config.update({'server.socket_host': '169.254.8.53',
                                'server.socket_port': 8080,
                                })
    cherrypy.config.update({'log.screen':False})

    cherrypy.quickstart(receiveFiles())
