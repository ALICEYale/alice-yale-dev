#!/usr/bin/env python

# Create test peaks by taking the existing peaks, substracting some set of numbers, and appending those to the file

import sys

def testData(filename):
    # File opening mode is determined by: http://stackoverflow.com/a/2757941
    with open(filename, "ab+") as f:
        # Read files into a list
        values = f.readlines()
        # Strip '\n' from each value and convert to int
        values = [x.strip('\n') for x in values]
        values = [int(x) for x in values]
        # Substract some value from the list of values to create a new peak at another mean
        # We already have the MMG+2GEMS peak, so we need one to correspond to MMG+GEM and just MMG
        MMGAndGEM = [x - 10 for x in values]
        #MMG = [x - 280 for x in values]

        #print values[10], MMGAndGEM[10], MMG[10]
        #print values[2374], MMGAndGEM[2374], MMG[2374]
        # Write out the created values
        f.writelines("%s\n" % x for x in MMGAndGEM)
        #f.writelines("%s\n" % x for x in MMG)


if __name__ == "__main__":
    if len(sys.argv) != 2:
        print "Invalid number of arguments. Must set filename."
        exit(-1)
    else:
        filename = sys.argv[1]
    testData(filename)
