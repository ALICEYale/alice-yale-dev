#ifndef PLOTTINGUTILITIES_H
#define PLOTTINGUTILITIES_H

#include "GEMmeasurement.h"

#include <TNamed.h>
#include <TObjArray.h>
#include <TLegend.h>
#include <TPaveText.h>
#include <TCanvas.h>

class TGraph;
class TH1;
class TPad;

class PlottingUtilities : public TNamed {
  
 public:
  
  PlottingUtilities();
  PlottingUtilities(const char* name, const char* title);

  TCanvas* GetCanvas(const char* name)      { return static_cast<TCanvas*>(fCanvases.FindObject(name)); }

  void     SetDataPath(const char* p)       { fDataPath       = p; }
  void     SetLegendTextSize(Float_t s)     { fLegendTextSize = s; }
  void     SetExpandWidth(Double_t w)       { fExpandWidth    = w; }
  void     SetXLegendPos(Double_t p)        { fXLegendPos     = p; }
  void     SetCrosstalk(Bool_t b)           { fCrosstalk = b; }
  
  GEMmeasurement* AddMeasurement(const char* gasName, Double_t gasfrac=1.0, Double_t CO2frac = 0.0, const char* quencherName = "", Double_t quencherfrac = 0.0);
  Int_t           ReadFile(const char* fname, const char* gasName, Double_t gasfrac=0.9, Double_t CO2frac = 0.1, const char* quencherName = "", Double_t quencherfrac = 0.0,
			   Double_t edrift = 0., Double_t etran = 0., Double_t eind = 0.);
  Int_t           ReadFile(const char* gasName, Double_t gasfrac, Double_t CO2frac, const char* quencherName, Double_t quencherfrac, const char* suffix = "");
  Int_t           WriteFile(const char* mainGasName, Double_t gasfrac=0.9, Double_t CO2frac = 0.1, const char* quencherName = "", Double_t quencherfrac = 0.0, const char* suffix = "");
  
  GEMmeasurement* GetMeasurement(const char* name) const;
  GEMmeasurement* GetMeasurement(const char* gasName, Double_t gasfrac, Double_t CO2frac, const char* quencherName, Double_t quencherfrac,
				 Double_t mmg, Double_t gem1, Double_t gem2, Double_t edrift, Double_t etran, Double_t eind) const;

  Bool_t RemoveMeasurement(GEMmeasurement* meas);  
  TCanvas* PlotResVsIbf(UInt_t groupBy=0, UInt_t freeParams=0);
  TCanvas* PlotMeasurement(GEMmeasurement::EParams freeParam, UInt_t groupBy, UInt_t measParams);
  TObjArray* GeneratePlots(GEMmeasurement::EParams freeParam, UInt_t groupBy, UInt_t measParams);

  TGraph* CreateGraph(GEMmeasurement* meas, UInt_t groupBy, UInt_t xAxisParam, UInt_t yAxisParam);

  static void  SavePlot(TCanvas* canvas);
  
 protected:

  TCanvas*        SetUpMultiPlotCanvas(const char* name, const char* title, UInt_t xAxisParam, UInt_t yAxisParams);
  
  TVirtualPad*    SetUpPad(TVirtualPad* pad,
                           const char* xTitle, Double_t minX, Double_t maxX, Bool_t logX,
                           const char* yTitle, Double_t minY, Double_t maxY, Bool_t logY,
                           Double_t lmar=0.12, Double_t rmar=0.08, Double_t bmar=0.12, Double_t tmar=0.08);
  TCanvas*        SetUpCanvas(const char* name,
                              const char* xTitle, Double_t minX, Double_t maxX, Bool_t logX,
                              const char* yTitle, Double_t minY, Double_t maxY, Bool_t logY,
                              Double_t w = 700, Double_t h = 500, Int_t rows=1, Int_t cols=1,
                              Double_t lmar=0.12, Double_t rmar=0.08, Double_t bmar=0.12, Double_t tmar=0.08);
  TCanvas*        SetUpCanvas(TH1* histo, Bool_t logX, Bool_t logY,
                              Double_t w = 700, Double_t h = 500, Int_t rows=1, Int_t cols=1,
                              Double_t lmar=0.12, Double_t rmar=0.08, Double_t bmar=0.12, Double_t tmar=0.08);
  
  TLegend*        SetUpLegend(Double_t x1, Double_t y1, Double_t x2, Double_t y2, Int_t textSize);
  TPaveText*      SetUpPaveText(Double_t x1, Double_t y1, Double_t x2, Double_t y2, Int_t textSize, const char* text = 0);
  
  static void FitGraphInPad(TGraph* graph, TVirtualPad* pad, Option_t* opt="", Bool_t copyAxisTitle=kFALSE, Double_t extraFactor=1.8);
  static void GetMinMax(TGraph* graph, Double_t& minx, Double_t& maxx, Double_t& miny, Double_t& maxy, Bool_t useInitialValues, Bool_t logY);
  static TLegend* GetLegend(TPad* pad);
  static TH1* DrawBlankHistogram(TVirtualPad* pad, TString xAxisTitle, Double_t minX, Double_t maxX, TString yAxisTitle, Double_t minY, Double_t maxY, Int_t n);

  TObjArray* GenerateResVsIbf(UInt_t groupBy);
  TCanvas* PlotGraphs(TObjArray* graphs, bool ionDensityFlag = false);

  TString       fDataPath;          //
  
  Float_t       fXLegendPos;        //
  Double_t      fExpandWidth;       //
  Float_t       fLegendTextSize;    //
  
  TObjArray     fMeasurements;      //
  TObjArray     fGraphs;            //
  TObjArray     fCanvases;          //
    
  Bool_t    fCrosstalk;         //
  
 private:
   
  PlottingUtilities(const PlottingUtilities &source);
  PlottingUtilities& operator=(const PlottingUtilities& source); 

  ClassDef(PlottingUtilities, 1);
};

#endif
