#!/usr/bin/env python
import u12
import signal
import sys
import csv
import time

def saveValuesToFile(filename, sparkCounts):
    # See: http://stackoverflow.com/a/14037564
    with open(filename, "wb") as f:
        # This will write the counts once we are done
        writer = csv.writer(f)

        # Keeps counts so that if it crashes we only lose a little (It really shouldn't...)
        writer.writerow(sparkCounts)

    # TEMP
    #print "Saved data to file", filename
    # TEMP

def recordLineValues(device, sparkCounts, numberOfLines, threshold):
    # Access all of the line values and increment whichever one is non-zero

    # TEMP
    #print "Checking voltages!"
    # TEMP

    for channel in range(0,numberOfLines):
        value = device.eAnalogIn(channel=channel)

        #TEMP
        #print "Voltage[", channel, "]:", value['voltage']
        #TEMP

        if abs(value['voltage']) > abs(threshold):
        #if value['voltage'] < threshold:
            sparkCounts[channel] += 1

            # TEMP
            #print "Spark found on channel", channel, "with voltage:", value['voltage']
            # TEMP

def sparkRecording(sparkCounts, filename, numberOfLines, threshold):
    # Create signal handler
    # For reason of putting it here, see http://stackoverflow.com/a/12371637
    def handleCtrlC(signal, frame):
        # See: http://stackoverflow.com/a/1112350
        # Extra space so that Ctrl-C doesn't end up on the same line as the information
        print ""
        print "Saving data and exiting!"

        saveValuesToFile(filename, sparkCounts)
        print "Data Saved!"
        sys.exit(0)

    # Register singal handler
    signal.signal(signal.SIGINT, handleCtrlC)

    # Setup u12
    device = u12.U12()
    print "device.eCount()['count']:", device.eCount()['count']
    device.eCount(resetCounter=1)
    count = 0
    print "device.eCount()['count']:", device.eCount()['count']

    # Sleep time and time counter for when to save data
    # Change to sleep .01 for 10 ms sleep
    sleepTime = .01
    # timeBetweenSaves in seconds
    timeBetweenSaves = 10
    # Counts the time elapsed (roughly)
    timeCounter = 0

    # Determines the time between saves
    timeBetweenSaves = int(timeBetweenSaves/sleepTime)

    # Capture data
    while True:
        # This may not be very accurate at all, so it may be better to just have it loop!
        # It appears that it can only do 1 loop ~ 16 ms, so this will have to do.
        # We will just roughly time with the timeCounter
        # Note: it will likely slow down when we actually have to query the device
        #time.sleep(sleepTime)
        timeCounter += 1

        # TEMP
        #print "device.eCount()['count']:", device.eCount()['count']
        # TEMP

        #if device.eCount()['count'] == (count+1):
        if device.eCount()['count'] >= (count+1):

            # TEMP
            #print "Count!"
            # TEMP

            count += 1
            recordLineValues(device, sparkCounts, numberOfLines, threshold)

        # Saves data when the appropraite time has elapsed
        if timeCounter == timeBetweenSaves:
            saveValuesToFile(filename, sparkCounts)
            # Set the time counter to 0 to make sure that it doesn't overflow at some large time
            timeCounter = 0

if __name__ == "__main__":
    # Define parameters for operation
    # Number of lines to monitor (starting from AI0)
    # MMG = 100 ms width, GEM = 200 ms width
    # MMGs on channels 0,1. GEMs on later channels
    numberOfLines = 6
    # Voltage threshold (in V)
    # NIM logic (fast negative logic) defines 1 as -0.8 V or lower.
    # Thus, the threshold is at -0.5 V
    # However, thsi means that polarity matters. So we must choose one way or another!
    # See: http://www-esd.fnal.gov/esd/catalog/intro/intronim.htm
    threshold = -0.5
    # Define spark counter
    sparkCounts = [0] * numberOfLines
    # Filename for saving data
    filename = "sparkCounts.csv"
    
    # Call the function
    sparkRecording(sparkCounts, filename, numberOfLines, threshold)
