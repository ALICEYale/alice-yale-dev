#!/usr/bin/env bash
# vim: expandtab tabstop=4 shiftwidth=4 softtabstop=4

# Offline mode (no snmp message are sent)
offline=0

# Global variable used by function getIndexFromSlotChannel
index=-1

# Global variable used by "get" functions (getSwitchFromIndex, getVoltageFromIndex)
value=-1

checkVoltage ()
{
    # Check that the voltage ${3} is allowed for channel ${2}
    # Set $value=1 if it's ok, $value=0 if it's not

    if [[ "${2}" -eq "${MMG}" ]]
    then
        DVOLT=$3
        MAXVOLT=$MAXVOLTAGE_MMG 
    else 
        if [[ "${2}" -eq "${MGEM_0}" ]]
        then
            DIFFINDEX=$MGEM_1
            MAXVOLT=$MAXVOLTAGE_MGEM
        elif [[ "${2}" -eq "${MGEM_1}" ]]
        then
            DIFFINDEX=$MGEM_0
            MAXVOLT=$MAXVOLTAGE_MGEM
        elif [[ "${2}" -eq "${TGEM_0}" ]]
        then
            DIFFINDEX=$TGEM_1
            MAXVOLT=$MAXVOLTAGE_TGEM
        elif [[ "${2}" -eq "${TGEM_1}" ]]
        then
            DIFFINDEX=$TGEM_0
            MAXVOLT=$MAXVOLTAGE_TGEM
        else
            value=1
            return
        fi
        getVoltageFromIndex $1 $DIFFINDEX
        #echo $value
        DVOLT=`echo "$value - $3" | bc`
        DVOLT=${DVOLT#-}
        value=-1
    fi

    echo "Checking index $2 $DVOLT <= $MAXVOLT..."

    st=`echo "$DVOLT <= $MAXVOLT" | bc`
    if [[ $st -eq 1 ]]
    then
        value=1
    else
        value=0
    fi
}

getIndexFromSlotChannel ()
{
    # $1 = slot, $2 = channel
    slot="${1}"
    channel="${2}"

    # Slot is either 1 or 2
    if [[ "${slot}" -gt 2 || "${slot}" -lt 1 ]]
    then
        echo "Invalid slot number of ${slot}! Slot must be either 1 or 2. Exiting!"
        return 1;
    fi

    # Channel is 1 - 8 for mpod and 1 - 11 for CAEN
    if [[ "${channel}" -gt ${maxChannel} || "${channel}" -lt ${minChannel} ]]
    then
        echo "Invalid channel value of ${channel}! Channel must be ${minChannel}-${maxChannel}. Exiting!"
        return 1;
    fi

    # Construct index
    # For the index, valid slots are 0,1, while valid channels are 1-8
    # Set a global variable with the index: it should be reinitialized to -1 for safety after every call to this function!
    index=$((100*($slot - 1) + ($channel)))
    return 0
}

setVoltageFromIndex ()
{
    # Set the voltage of a particular channel using the index
    echo "IP = ${1}, Index = ${2}, Setting voltage to: ${3}."

    # Check that the voltage is not above the maximum allowed for the channel
    # don't check if the disableLimits variable is set to 1
    if  [[ "${disableLimits}" -eq 1 ]]
    then
        voltOk=1
    else
        checkVoltage $1 $2 $3
        voltOk=$value
        value=-1
    fi

    if  [[ "${voltOk}" -eq 1 ]]
    then
        cmd="${appendToCommand}snmpset -v 2c -m +WIENER-CRATE-MIB -c guru ${1} outputVoltage.${2} F ${3}"
        # Only do it if $offline is false
        if [[ "${offline}" == "0" ]]; then
            eval $(echo "$cmd")
        else
            echo $cmd
        fi
    else
        echo "Attempt to set voltage of channel ${2} to ${3} has been blocked."
    fi
}

setVoltageFromSlotChannel ()
{    
    # $1 = ip, $2 = slot, $3 = channel, $4 = voltage
    getIndexFromSlotChannel $2 $3
    if [[ ! -z "${index}" ]] && [[ "${index}" -gt -1 ]]
    then
        setVoltageFromIndex $1 $index $4
    fi
    index=-1
}

setSwitchFromIndex ()
{
    # Set the switch of a particular channel using the index

    cmd="${appendToCommand}snmpset -v 2c -m +WIENER-CRATE-MIB -c guru ${1} outputSwitch.${2} i ${3}"
    # Only do it if $offline is false
    if [[ "${offline}" == "0" ]]; then
        eval $(echo "$cmd")
    else
        echo $cmd
    fi
}

setSwitchFromSlotChannel ()
{
    # $1 = ip, $2 = slot, $3 = channel, $4 = switch (0,1,2,3,10)
    getIndexFromSlotChannel $2 $3
    if [[ ! -z "${index}" ]] && [[ "${index}" -gt -1 ]]
    then
        setSwitchFromIndex "${1}" "${index}" "${4}"
    fi
    index=-1
}

getSwitchFromIndex ()
{
    # Get the switch of a particular channel using the index
    # Set a global variable ($value) with the index: it should be reinitialized to -1 for safety after every call to this function!

    cmd="${appendToCommand}snmpget -v 2c -m +WIENER-CRATE-MIB -c public ${1} outputSwitch.${2}"
    # Only do it if $offline is false
    if [[ "${offline}" == "0" ]]; then
        line=$($cmd)
        # Need to be tested!
        value=$(echo "$line" | awk '{print $4}')
        if [[ "${value}" == "off(0)" ]]; then
            value=0
        elif [[ "${value}" == "on(1)" ]]; then
            value=1
        else
            echo "WARNING: could not parse correctly the returned string in getSwitchFromIndex"
        fi
    else
        echo $cmd
        # Return a dummy value for test purposes
        value=1
    fi
}

getSwitchFromSlotChannel ()
{
    # $1 = ip, $2 = slot, $3 = channel
    getIndexFromSlotChannel $2 $3
    if [[ ! -z "${index}" ]] && [[ "${index}" -gt -1 ]]
    then
        getSwitchFromIndex "${1}" "${index}" "${4}"
    fi
    index=-1
}

getVoltageFromIndex ()
{
    # Get the voltage of a particular channel using the index
    # Set a global variable ($value) with the volatge: it should be reinitialized to -1 for safety after every call to this function!

    cmd="${appendToCommand}snmpget -v 2c -m +WIENER-CRATE-MIB -c public ${1} outputVoltage.${2}"
    # Only do it if $offline is false
    if [[ "${offline}" == "0" ]]; then
        line=$($cmd)
        #echo $cmd
        #echo $line
        value=$(echo "$line" | awk '{print $5}')
    else
        echo $cmd
        # Return a dummy value for test purposes
        value=300
    fi
}

getVoltageFromSlotChannel ()
{
    # $1 = ip, $2 = slot, $3 = channel
    getIndexFromSlotChannel $2 $3
    if [[ ! -z "${index}" ]] && [[ "${index}" -gt -1 ]]
    then
        getVoltageFromIndex "${1}" "${index}" "${4}"
    fi
    index=-1
}

addVoltageFromIndex ()
{
    # Add voltage to a particular channel using the index
    echo "IP = ${1}, Index = ${2}, Adding voltage: ${3}."

    getVoltageFromIndex $1 $2
    currentVoltage=$value;
    value=-1

    newVoltage=$(bc <<< "${currentVoltage} + ${3}")

    setVoltageFromIndex $1 $2 $newVoltage
}

addVoltageFromSlotChannel ()
{    
    # $1 = ip, $2 = slot, $3 = channel, $4 = voltage
    getIndexFromSlotChannel $2 $3
    if [[ ! -z "${index}" ]] && [[ "${index}" -gt -1 ]]
    then
        addVoltageFromIndex $1 $index $4
    fi
    index=-1
}

setRampRate ()
{
    # Set the ramp rate for all channels
    # $1 = ip, $2 = rampRate

    # The ramp rate is common to all channels of each slot (check!)
    # Only need to set the ramp rate for the first channel of each slot

    cmd="${appendToCommand}snmpset -v 2c -m +WIENER-CRATE-MIB -c guru ${1} outputVoltageRiseRate.1 F -${2}"
    # Only do it if $offline is false
    if [[ "${offline}" == "0" ]]; then
        eval $(echo "$cmd")
    else
        echo $cmd
    fi

    cmd="${appendToCommand}snmpset -v 2c -m +WIENER-CRATE-MIB -c guru ${1} outputVoltageFallRate.1 F -${2}"
    # Only do it if $offline is false
    if [[ "${offline}" == "0" ]]; then
        eval $(echo "$cmd")
    else
        echo $cmd
    fi

    cmd="${appendToCommand}snmpset -v 2c -m +WIENER-CRATE-MIB -c guru ${1} outputVoltageRiseRate.101 F ${2}"
    # Only do it if $offline is false
    if [[ "${offline}" == "0" ]]; then
        eval $(echo "$cmd")
    else
        echo $cmd
    fi

    cmd="${appendToCommand}snmpset -v 2c -m +WIENER-CRATE-MIB -c guru ${1} outputVoltageFallRate.101 F ${2}"
    # Only do it if $offline is false                                                                                                
    if [[ "${offline}" == "0" ]]; then
        eval $(echo "$cmd")
    else
        echo $cmd
    fi
}

setVoltageFromFile ()
{
    ip="$1"
    filename="$2"
    rampDown="$3"

    # Only do it if $offline is false
    if [[ "${offline}" == "0" ]]; then
        # Check if system is off
        line=$(${appendToCommand}snmpget -v 2c -m +WIENER-CRATE-MIB -c public $ip sysMainSwitch.0)
        mainStatus=$(echo "$line" | awk '{print $4}' | sed 's/([0-9])//')
        if [[ "${mainStatus}" == "off" ]]
        then
            return 1
        fi
    fi

    printStatus "${ip}"

    # Check if the file exists
    if [[ ! -e "$filename" ]]
    then
        echo "File ${filename} does not exists"
        exit 1
    fi

    # If the variable is empty, then it is not defined.
    # IF it is not empty, then it was set from the calling function
    if [[ -z "$rampDown" ]]
    then
        # If it doesn't exist, then we are not ramping down
        rampDown=0
    fi

    # Determine number of steps: slot channel startVoltage [steps] protect
    steps=$(awk 'NR==3 {print (NF-4)}' "$filename")

    # Number of entries
    n=$(awk 'END{print NR}' "$filename")

    echo "$n entries found in file $filename"

    # Extract ramp rate from the first line
    rampRate=$(awk 'NR==1 {print $2}' "$filename")
    echo "Setting ramp rate to ${rampRate}"
    setRampRate "${ip}" "${rampRate}"

    # Loop through all lines to check if any of the channels is already on with a voltage set
    # Ignore the first two lines, as they just contain definitions
    # Ignores last line, as that contains the waits (to cover all lines, one needs n+1)
    echo "setVoltageFromFile: Loop through all lines to check if any of the channels is already on with a voltage set"
    switchAllOn=0
    continueAll=0
    resetAll=0
    for (( j=3; $j<$n; j++ ))
    do
        # Determine which column to check based on whether ramping up or down
        local column=3
        if [[ "${rampDown}" == "1" ]]
        then
            # + 2 to account for stepping over the channel and slot
            column=$((steps + 2))
        fi
        echo "column: $column"

        echo "j: $j"
        slot=$(awk -v j=$j 'NR==j {print $1}' "$filename")
        channel=$(awk -v j=$j 'NR==j {print $2}' "$filename")
        checkVoltage=$(awk -v j=$j -v column=$column 'NR==j {print $column}' "$filename" | sed 's/,$//g')
        getSwitchFromSlotChannel ${ip} ${slot} ${channel}
        # Switch is whether the voltage is enabled
        switch=$value
        value=-1

        if [[ ${switch} == "0" ]]; then
            echo "Slot ${slot}, channel ${channel} is off. Zeroing the voltage."
            setVoltageFromSlotChannel ${ip} ${slot} ${channel} 0

            if [[ ${switchAllOn} -eq 0 ]]; then
                read -p "Would you like to turn this channel on before continuing? (yes/no/all/exit) [all] " answer
                echo "Answer: ${answer}"
                answer=$(echo "$answer" | awk '{print tolower($0)}')
                if [[ -z "${answer}" || "${answer}" == "all" ]]; then
                    switchAllOn=1
		    setSwitchFromSlotChannel ${ip} ${slot} ${channel} 1
                elif [[ "${answer}" == "yes" ]]; then
                    setSwitchFromSlotChannel ${ip} ${slot} ${channel} 1
                elif [[ "${answer}" != "no" ]]; then
                    exit 0
                fi
            else
                setSwitchFromSlotChannel ${ip} ${slot} ${channel} 1
            fi
        fi

        if [[ ${continueAll} -eq 0 ]]; then
            # If continueAll == 1 we don't need to check this

            getVoltageFromSlotChannel ${ip} ${slot} ${channel}
            voltage=$value
            value=-1

            testVoltage=$(echo "$voltage == $checkVoltage" | bc)
            echo ${testVoltage}

            if [[ ${testVoltage} == "0" ]]; then
                # Found a channel with voltage != checkVoltage
                # Ask the user if he/she wants to continue
                if [[ ${resetAll} -eq 0 ]]; then 
                    echo "Slot ${slot}, channel ${channel} has a voltage different from the first column in the file provided."
                    echo "Set voltage = ${voltage}. Expected voltage = ${checkVoltage}"
                    echo "Type 'continue' to ignore the discprepancy and proceed setting the voltage given in the second column."
                    echo "Type 'reset' to set the voltage to the first column value before proceeding to the second column."
                    echo "Type 'continue all' or 'reset all' to perform the same operation on all the next conflicts."
                    echo "Type 'exit' to terminate the program."
                    read -p "How would you like to proceed? (continue/reset/continue all/reset all/exit) [continue all] " answer
                    echo "Answer: ${answer}"
                    if [[ -z "${answer}" ]]; then
                        answer="continue all"
                    fi
                else
                    answer="reset"
                fi
                answer=$(echo "$answer" | awk '{print tolower($0)}')

                if [[ "${answer}" == *continue* ]]; then
                    if [[ "${answer}" == *all* ]]; then
                        continueAll=1
                    fi
                elif [[ "${answer}" == *reset* ]]; then
                    if [[ "${answer}" == *all* ]]; then
                        resetAll=1
                    fi
                    setVoltageFromSlotChannel ${ip} ${slot} ${channel} ${checkVoltage}
                else
                    exit 0
                fi
            fi
        fi
    done

    # Loop through voltages
    # i begins at 4, since it must skip the slot and channel columns and the first voltage column
    # It skips the first voltage column, because that column must be 0 for ramping up or the starting
    #  voltage for ramping down.
    # If ramping up
    local iLeft=4
    # + 3 to account for skipping over, but -1 because {#..#} is inclusive, unlike i<$((steps+3))
    local iRight=$((steps + 3 -1))

    # If ramping down, then reverse it
    if [[ "$rampDown" == "1" ]];
    then
        # Need -2 rather than -1 to skip the first column, which is where the voltage is set to match the previous voltage
        iLeft=$((steps + 3 -2))
        iRight=3
    fi
    #echo "Ramping down: $rampDown"

    # Loop through voltages
    for i in $(eval echo "{${iLeft}..${iRight}}")
    do
        maxRampTime=0
        rampTime=0
        # j loops through lines
        # Ignore the first two lines, as they just contain definitions
        # Ignores last line, as that contains the waits (to cover all lines, one needs n+1)
        for (( j=3; $j<$n; j++ ))
        do
            echo "i: $i, j: $j"
            echo "n=$n"
            slot=$(awk -v j=$j 'NR==j {print $1}' "$filename")
            channel=$(awk -v j=$j 'NR==j {print $2}' "$filename")
            voltage=$(awk -v i=$i -v j=$j 'NR==j {print $i}' "$filename" | sed 's/,$//g')

            # Read voltage from the previous column
            k=$i
            if [[ "${rampDown}" == "1" ]]
            then
                let "k += 1"
            else
                let "k -= 1"
            fi
            prev_voltage=$(awk -v i=$k -v j=$j 'NR==j {print $i}' "$filename" | sed 's/,$//g')
            let "rampTime = ($voltage - $prev_voltage) / $rampRate"
            # Take the absolute value
            rampTime=${rampTime#-}
            if [[ $rampTime -gt $maxRampTime ]]
            then
                maxRampTime=$rampTime
            fi

            setVoltageFromSlotChannel "$ip" "$slot" "$channel" "$voltage"
        done

        # Wait until all channels finished the ramp up/down
        echo "Now wait $maxRampTime seconds to allow ramp up/down of the channels..."
        sleep $maxRampTime

        if [[ $i -le $(awk -v n=$n 'NR==n {print NF}' "$filename") ]]
        then
            # Ignores the first column, which declares that we are defining waits
            sleepDuration=$(awk -v n=$n -v i=$((i-1)) 'NR==n {print $i}' "$filename" | sed 's/,$//g')
            if [[ "$sleepDuration" == "-" ]]
            then
                read -p "Press [Enter] to take next step."
            else
                echo "Sleeping for $sleepDuration seconds"
                sleep $sleepDuration
            fi
        fi	
    done

    printStatus "${ip}"
}

# Add ramp rate in here
printStatus ()
{
    ip="$1"
    local logging=0
    if [[ -n "$2" ]]; then
        logging="$2"
    fi

    # Record the time of the status query
    local timestamp="Timestamp: $(date)"
    if [[ "$logging" == true ]]
    then
        echo "$timestamp" | tee -a voltage.log
    else
        echo "$timestamp"
    fi

    # Only do it if $offline is false
    if [[ "${offline}" == "1" ]]; then
        if [[ "$logging" == true ]]
        then
            echo "PrintStatus, offline mode." | tee -a voltage.log
        else
            echo "PrintStatus, offline mode."
        fi
        return 0
    fi

    # Display if the crate is on. If off, the channel query will not work, so simply exit
    line=$(${appendToCommand}snmpget -v 2c -m +WIENER-CRATE-MIB -c public $ip sysMainSwitch.0)
    mainStatus=$(echo "$line" | awk '{print $4}' | sed 's/([0-9])//')
    local systemStatus="The system is $mainStatus"
    if [[ "$logging" == true ]]
    then
        echo "$systemStatus" | tee -a voltage.log
    else
        echo "$systemStatus"
    fi

    if [[ "${mainStatus}" == "off" ]]
    then
        return 1
    fi

    # If the crate is on, display the voltages on the channels
    outputVoltage=$(${appendToCommand}snmpwalk -v 2c -m +WIENER-CRATE-MIB -c public $ip outputVoltage)
    outputMeasurementSenseVoltage=$(${appendToCommand}snmpwalk -v 2c -m +WIENER-CRATE-MIB -c public $ip outputMeasurementSenseVoltage)
    outputMeasurementCurrent=$(${appendToCommand}snmpwalk -v 2c -m +WIENER-CRATE-MIB -c public $ip outputMeasurementCurrent)

    # Display rise and fall rate
    # Note: Should be the same for all crates and all channels
    rampRiseRate=$(${appendToCommand}snmpget -v 2c -m +WIENER-CRATE-MIB -c public $ip outputVoltageRiseRate.1)
    rampFallRate=$(${appendToCommand}snmpget -v 2c -m +WIENER-CRATE-MIB -c public $ip outputVoltageFallRate.1)
    rampRiseRateValue=$(echo "$rampRiseRate" | awk '{print $5}')
    rampFallRateValue=$(echo "$rampFallRate" | awk '{print $5}')

    rampRate="Ramp rise rate: $rampRiseRateValue V/s. Ramp fall rate: $rampFallRateValue V/s."
    if [[ "$logging" == true ]]
    then
        echo "$rampRate" | tee -a voltage.log
    else
        echo "$rampRate"
    fi

    # Total number of channels
    local n=$(echo "$outputVoltage" | awk 'END{print NR}')

    for (( i=1; $i<$((n+1)); i++ ))
    do
        # Use awk to process the information line by line using NR
        # Extract the index from the voltage status output, and then remove the u
        ind=$(echo "$outputVoltage" | awk -v i=$i 'NR==i {print $1}' | awk -F "." '{print $2}' | cut -c2-)

        # Voltage and current values can be extracted directly
        voltage=$(echo "$outputVoltage" | awk -v i=$i 'NR==i {print $5}')
        measVoltage=$(echo "$outputMeasurementSenseVoltage" | awk -v i=$i 'NR==i {print $5}')
        measCurr=$(echo "$outputMeasurementCurrent" | awk -v i=$i 'NR==i {print $5}')

        # Convert index to slot, channel
        if [[ "$ind" -gt ${maxChannel} ]]
        then
            channel=$((ind-100+1))
            slot=$((1+1))
        else
            channel=$((ind+1))
            slot=$((0+1))
        fi

        # Check whether channel is enabled
        getSwitchFromSlotChannel $ip $slot $channel
        voltageEnabled="${value}"
        if [[ "$logging" == true ]]
        then
            tput el
        fi

        statusLine="Slot $slot, channel $channel, enabled $voltageEnabled -> Voltage: set to $voltage V, measured at $measVoltage V; Current: measured at $measCurr A"

        if [[ "$logging" == true ]]
        then
            echo "$statusLine" | tee -a voltage.log
        else
            echo "$statusLine"
        fi
    done

    # Save cursor position to return to after terminating via ctrl-c
    tput sc

    # Allows the terminal to scroll back up to overwrite the previous values in the console
    if [[ "$logging" == true ]]
    then
        for (( i=1; $i<=$((n+3)); i++ ))
        do
            tput cuu1
        done
    fi
}

changeCratePowerState()
{
    commandType="${1}"
    if [[ "${commandType}" == "disable" ]]; then
        desiredPowerState=0
    else
        desiredPowerState=1
    fi
    crateStatus=$(${appendToCommand}snmpget -v 2c -m +WIENER-CRATE-MIB -c public "$ip" sysMainSwitch.0)
    crateStatus=$(echo "$crateStatus" | awk '{print $4}' | sed 's/[^0-9]//g')
    if [[ "$crateStatus" -ne "$desiredPowerState" ]]
    then	
        changeCrate=$(${appendToCommand}snmpset -v 2c -m +WIENER-CRATE-MIB -c private "$ip" sysMainSwitch.0 i "${desiredPowerState}")
    else
        echo "The power supply is already ${commandType}d"
    fi
    sleep 3
    printStatus "${ip}"
    exit 0;
}
