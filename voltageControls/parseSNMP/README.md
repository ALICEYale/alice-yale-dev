## parseSNMP

### To setup:

```bash
# Create the install directory
mkdir $HOME/install
MYINSTALL="$HOME/install"
# Add the install directory to the path (should probably check this command)
PATH="$MYINSTALL/bin:$PATH"
# Add the lib directory to the path (should probably check this command)
LD_LIBRARY_PATH="$MYINSTALL/lib:$LD_LIBRARY_PATH"
# Obtain the CAEN wrapper libraries (see caen/README.md)
cd caen
scp -r rhig:/home/ehlers/libraries/caen/* .
cd ..
# Run autotools and configure
cd build
./autogen.sh --prefix="$HOME/install"
```

### To build:

**NOTE: If Makefile.am is changed (it probably shouldn't need to be), then `./autogen.sh` must be run again as above.**

```bash
make
make install
```

### Source code

The source is somewhat documented as of 8/10/2014. It is located in `parseSNMP/src`.

### To test parseSNMP directly:

See `testScript.sh` for more information about how to what it tests. It describes how to change variables. It can also be run directly via `parseSNMP` (ie parseSNMP <arg>).

```bash
./testScript.sh
# OR
parseSNMP <args>
```

### To test with MPOD

Run any command as normally in mpod, but append the `-x` switch. There will be extra information printed from parseSNMP.  This extra information can be hidden by appending a `2> /dev/null` to the end of the mpod call.

**NOTE: The ip in `mpod.sh` is currently set to `127.0.0.1` (localhost). This must be changed if one is actually making API calls!**

### To run with CAEN API calls:

To run with CAEN API calls, change `#define DEBUG 1` to `#define DEBUG 0` in `src/parseSNMP.cc` and `src/main.cc`. Recompile the source using make as described above. 

**NOTE: The ip in `mpod.sh` is currently set to `127.0.0.1` (localhost). This must be changed if one is actually making API calls!**
