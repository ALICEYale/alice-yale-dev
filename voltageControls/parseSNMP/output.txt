ray@ray-ENVY4 ~/code/alice/microMega/mpod $ IP=""
ray@ray-ENVY4 ~/code/alice/microMega/mpod $ echo $IP

ray@ray-ENVY4 ~/code/alice/microMega/mpod $ snmpget -v 2c -m +WIENER-CRATE-MIB -c public $IP outputSwitch.1
WIENER-CRATE-MIB::outputSwitch.u0 = INTEGER: off(0)
ray@ray-ENVY4 ~/code/alice/microMega/mpod $ snmpget -v 2c -m +WIENER-CRATE-MIB -c public $IP outputSwitch.2
WIENER-CRATE-MIB::outputSwitch.u1 = INTEGER: off(0)
ray@ray-ENVY4 ~/code/alice/microMega/mpod $ snmpget -v 2c -m +WIENER-CRATE-MIB -c public $IP outputSwitch.1
WIENER-CRATE-MIB::outputSwitch.u0 = INTEGER: off(0)
ray@ray-ENVY4 ~/code/alice/microMega/mpod $
ray@ray-ENVY4 ~/code/alice/microMega/mpod $
ray@ray-ENVY4 ~/code/alice/microMega/mpod $ snmpget -v 2c -m +WIENER-CRATE-MIB -c public $IP outputVoltage.1
WIENER-CRATE-MIB::outputVoltage.u0 = Opaque: Float: 0.000000 V
ray@ray-ENVY4 ~/code/alice/microMega/mpod $ snmpget -v 2c -m +WIENER-CRATE-MIB -c public $IP outputVoltage.3
WIENER-CRATE-MIB::outputVoltage.u2 = Opaque: Float: 0.000000 V
ray@ray-ENVY4 ~/code/alice/microMega/mpod $ snmpget -v 2c -m +WIENER-CRATE-MIB -c public $IP outputVoltage.101
WIENER-CRATE-MIB::outputVoltage.u100 = Opaque: Float: 0.000000 V
ray@ray-ENVY4 ~/code/alice/microMega/mpod $ snmpget -v 2c -m +WIENER-CRATE-MIB -c public $IP outputVoltage.100
WIENER-CRATE-MIB::outputVoltage.u99 = No Such Instance currently exists at this OID
ray@ray-ENVY4 ~/code/alice/microMega/mpod $
ray@ray-ENVY4 ~/code/alice/microMega/mpod $
ray@ray-ENVY4 ~/code/alice/microMega/mpod $ snmpget -v 2c -m +WIENER-CRATE-MIB -c public $IP sysMainSwitch.0
WIENER-CRATE-MIB::sysMainSwitch.0 = INTEGER: on(1)
ray@ray-ENVY4 ~/code/alice/microMega/mpod $
ray@ray-ENVY4 ~/code/alice/microMega/mpod $
ray@ray-ENVY4 ~/code/alice/microMega/mpod $ snmpget -v 2c -m +WIENER-CRATE-MIB -c public $IP outputVoltageRiseRate.1
WIENER-CRATE-MIB::outputVoltageRiseRate.u0 = Opaque: Float: -60.000000 V/s
ray@ray-ENVY4 ~/code/alice/microMega/mpod $ snmpget -v 2c -m +WIENER-CRATE-MIB -c public $IP outputVoltageFallRate.1
WIENER-CRATE-MIB::outputVoltageFallRate.u0 = Opaque: Float: -60.000000 V/s
ray@ray-ENVY4 ~/code/alice/microMega/mpod $ snmpget -v 2c -m +WIENER-CRATE-MIB -c public $IP outputVoltageFallRate.101
WIENER-CRATE-MIB::outputVoltageFallRate.u100 = Opaque: Float: 40.000000 V/s
ray@ray-ENVY4 ~/code/alice/microMega/mpod $
ray@ray-ENVY4 ~/code/alice/microMega/mpod $
ray@ray-ENVY4 ~/code/alice/microMega/mpod $ snmpwalk -v 2c -m +WIENER-CRATE-MIB -c public $IP outputVoltage
WIENER-CRATE-MIB::outputVoltage.u0 = Opaque: Float: 0.000000 V
WIENER-CRATE-MIB::outputVoltage.u1 = Opaque: Float: 0.000000 V
WIENER-CRATE-MIB::outputVoltage.u2 = Opaque: Float: 0.000000 V
WIENER-CRATE-MIB::outputVoltage.u3 = Opaque: Float: 0.000000 V
WIENER-CRATE-MIB::outputVoltage.u4 = Opaque: Float: 0.000000 V
WIENER-CRATE-MIB::outputVoltage.u5 = Opaque: Float: 0.000000 V
WIENER-CRATE-MIB::outputVoltage.u6 = Opaque: Float: 0.000000 V
WIENER-CRATE-MIB::outputVoltage.u7 = Opaque: Float: 0.000000 V
WIENER-CRATE-MIB::outputVoltage.u100 = Opaque: Float: 0.000000 V
WIENER-CRATE-MIB::outputVoltage.u101 = Opaque: Float: 0.000000 V
WIENER-CRATE-MIB::outputVoltage.u102 = Opaque: Float: 0.000000 V
WIENER-CRATE-MIB::outputVoltage.u103 = Opaque: Float: 0.000000 V
WIENER-CRATE-MIB::outputVoltage.u104 = Opaque: Float: 0.000000 V
WIENER-CRATE-MIB::outputVoltage.u105 = Opaque: Float: 0.000000 V
WIENER-CRATE-MIB::outputVoltage.u106 = Opaque: Float: 0.000000 V
WIENER-CRATE-MIB::outputVoltage.u107 = Opaque: Float: 0.000000 V
ray@ray-ENVY4 ~/code/alice/microMega/mpod $
ray@ray-ENVY4 ~/code/alice/microMega/mpod $
ray@ray-ENVY4 ~/code/alice/microMega/mpod $ snmpwalk -v 2c -m +WIENER-CRATE-MIB -c public $IP outputMeasurementSenseVoltage
WIENER-CRATE-MIB::outputMeasurementSenseVoltage.u0 = Opaque: Float: -0.012480 V
WIENER-CRATE-MIB::outputMeasurementSenseVoltage.u1 = Opaque: Float: -0.865763 V
WIENER-CRATE-MIB::outputMeasurementSenseVoltage.u2 = Opaque: Float: -0.970264 V
WIENER-CRATE-MIB::outputMeasurementSenseVoltage.u3 = Opaque: Float: -0.421267 V
WIENER-CRATE-MIB::outputMeasurementSenseVoltage.u4 = Opaque: Float: 0.000000 V
WIENER-CRATE-MIB::outputMeasurementSenseVoltage.u5 = Opaque: Float: -0.971019 V
WIENER-CRATE-MIB::outputMeasurementSenseVoltage.u6 = Opaque: Float: -1.199460 V
WIENER-CRATE-MIB::outputMeasurementSenseVoltage.u7 = Opaque: Float: 0.000000 V
WIENER-CRATE-MIB::outputMeasurementSenseVoltage.u100 = Opaque: Float: 0.174355 V
WIENER-CRATE-MIB::outputMeasurementSenseVoltage.u101 = Opaque: Float: 0.175708 V
WIENER-CRATE-MIB::outputMeasurementSenseVoltage.u102 = Opaque: Float: 0.212922 V
WIENER-CRATE-MIB::outputMeasurementSenseVoltage.u103 = Opaque: Float: 0.192997 V
WIENER-CRATE-MIB::outputMeasurementSenseVoltage.u104 = Opaque: Float: 0.142720 V
WIENER-CRATE-MIB::outputMeasurementSenseVoltage.u105 = Opaque: Float: 0.148798 V
WIENER-CRATE-MIB::outputMeasurementSenseVoltage.u106 = Opaque: Float: 0.175205 V
WIENER-CRATE-MIB::outputMeasurementSenseVoltage.u107 = Opaque: Float: 0.156282 V
ray@ray-ENVY4 ~/code/alice/microMega/mpod $
ray@ray-ENVY4 ~/code/alice/microMega/mpod $
ray@ray-ENVY4 ~/code/alice/microMega/mpod $ snmpwalk -v 2c -m +WIENER-CRATE-MIB -c public $IP outputMeasurementCurrent
WIENER-CRATE-MIB::outputMeasurementCurrent.u0 = Opaque: Float: 0.000000 A
WIENER-CRATE-MIB::outputMeasurementCurrent.u1 = Opaque: Float: 0.000000 A
WIENER-CRATE-MIB::outputMeasurementCurrent.u2 = Opaque: Float: 0.000000 A
WIENER-CRATE-MIB::outputMeasurementCurrent.u3 = Opaque: Float: 0.000000 A
WIENER-CRATE-MIB::outputMeasurementCurrent.u4 = Opaque: Float: 0.000000 A
WIENER-CRATE-MIB::outputMeasurementCurrent.u5 = Opaque: Float: 0.000000 A
WIENER-CRATE-MIB::outputMeasurementCurrent.u6 = Opaque: Float: 0.000000 A
WIENER-CRATE-MIB::outputMeasurementCurrent.u7 = Opaque: Float: 0.000000 A
WIENER-CRATE-MIB::outputMeasurementCurrent.u100 = Opaque: Float: 0.000000 A
WIENER-CRATE-MIB::outputMeasurementCurrent.u101 = Opaque: Float: 0.000000 A
WIENER-CRATE-MIB::outputMeasurementCurrent.u102 = Opaque: Float: 0.000000 A
WIENER-CRATE-MIB::outputMeasurementCurrent.u103 = Opaque: Float: 0.000000 A
WIENER-CRATE-MIB::outputMeasurementCurrent.u104 = Opaque: Float: 0.000000 A
WIENER-CRATE-MIB::outputMeasurementCurrent.u105 = Opaque: Float: 0.000000 A
WIENER-CRATE-MIB::outputMeasurementCurrent.u106 = Opaque: Float: 0.000000 A
WIENER-CRATE-MIB::outputMeasurementCurrent.u107 = Opaque: Float: 0.000000 A
ray@ray-ENVY4 ~/code/alice/microMega/mpod $
ray@ray-ENVY4 ~/code/alice/microMega/mpod $
