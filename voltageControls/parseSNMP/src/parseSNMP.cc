// parseSNMP.cc
#include "parseSNMP.h"

#include <iostream>
#include <sstream>
#include <cstring>

#include <CAENHVWrapper.h>

namespace parseSNMP
{
	////////////////////////////////////////////////////////////////////
	// To be implemented via CAENHVWrapper API Calls!
	////////////////////////////////////////////////////////////////////

	// General definitions
	//  handle = the handle return by CAENHV_InitSystem() used to access the system
	//  slot = int (channel / 100)
	//  channel = high voltage channel. Channel is defined as (slot - 1)*100 + (channel in slot)
	void getSlotChannelFromChannelIndex(int channelIndex, int & slot, int & channel)
	{
		slot = static_cast<int>(channelIndex/100);
		channel = channelIndex % 100;
	}

	// setFunctions
	void setChannelVoltage(systemParameters const & sysParams, int slot, const short unsigned int channel, double voltage)
	{
		// Set channel voltage using handle, channel, and voltage
		if (DEBUG == 1)
		{
			std::clog << "setChannelVoltage(): " << std::endl;
			std::clog << "handle: " << sysParams.handle << "\tslot: " << slot << "\tchannel: " << channel << "\tvoltage: " << voltage << std::endl;
		}
		else
		{
			CAENHV_SetChParam(sysParams.handle, slot, "V0Set", 1, &channel, &voltage);
		}
	}
	void setChannelStatus(systemParameters const & sysParams, int slot, const short unsigned int channel, int status)
	{
		// Set channel status (on(1) vs off(0)) using handle, channel, and status value
		if (DEBUG == 1)
		{
			std::clog << "setChannelStatus(): " << std::endl;
			std::clog << "handle: " << sysParams.handle << "\tslot: " << slot << "\tchannel: " << channel << "\tstatus: " << status << std::endl;
		}
		else
		{
			// Send the right string
			std::string statusString = "";
			if (status == 1)
			{
				statusString = "ON";
			}
			else
			{
				statusString = "OFF";
			}
			char * statusCStr = new char[statusString.length() + 1];
			std::strcpy(statusCStr, statusString.c_str());
			CAENHV_SetChParam(sysParams.handle, slot, "Pw", 1, &channel, statusCStr);

			delete [] statusCStr;
		}
	}
	void setChannelRiseRate(systemParameters const & sysParams, int slot, const short unsigned int channel, double voltage)
	{
		// Set channel ramp rise rate using handle, channel, and voltage (V/s)
		if (DEBUG == 1)
		{
			std::clog << "setChannelRiseRate(): " << std::endl;
			std::clog << "handle: " << sysParams.handle << "\tslot: " << slot << "\tchannel: " << channel << "\tvoltage: " << voltage << std::endl;
		}
		else
		{
			CAENHV_SetChParam(sysParams.handle, slot, "RUp", 1, &channel, &voltage);
		}
	}
	void setChannelFallRate(systemParameters const & sysParams, int slot, const short unsigned int channel, double voltage)
	{
		// Set channel ramp fall rate using handle, channel, and voltage (V/s)
		if (DEBUG == 1)
		{
			std::clog << "setChannelFallRate(): " << std::endl;
			std::clog << "handle: " << sysParams.handle << "\tslot: " << slot << "\tchannel: " << channel << "\tvoltage: " << voltage << std::endl;
		}
		else
		{
			CAENHV_SetChParam(sysParams.handle, slot, "RDwn", 1, &channel, &voltage);
		}
	}
	void setSystemPowerStatus(systemParameters const & sysParams, int status)
	{
		// Set system power status using handle and desired power status (on(1) or off(0))
		if (DEBUG == 1)
		{
			std::clog << "setSystemPowerStatus(): " << std::endl;
			std::clog << "handle: " << sysParams.handle << "\tstatus: " << status << std::endl;
		}
		else
		{
			// Not sure if this works...
			std::clog << "I don't think we can power on the crate remotely..." << std::endl;
		}
	}

	// getFunctions
	double getChannelVoltage(systemParameters const & sysParams, int slot, const short unsigned int channel)
	{
		// Get channel voltage using handle and channel. Return the value
		double * voltage = 0;
		double returnVoltage = -1;

		if (DEBUG == 0)
		{
			CAENHV_GetChParam(sysParams.handle, slot, "V0Set", 1, &channel, voltage);
			returnVoltage = *voltage;
		}
		else
		{
			returnVoltage = 100;
		}
		
		return returnVoltage;
	}
	int getChannelStatus(systemParameters const & sysParams, int slot, const short unsigned int channel)
	{
		// Get channel status (on(1) or off(0)) using handle and channel. Retrun the value
		int statusVal = -1;
		if (DEBUG == 0)
		{
			char * status = 0;

			CAENHV_GetChParam(sysParams.handle, slot, "Pw", 1, &channel, status);

			std::clog << "status: " << status << std::endl;

			if (!std::strcmp(status, "ON"))
			{
				statusVal = 1;
			}
			if (!std::strcmp(status, "OFF"))
			{
				statusVal = 0;
			}
		}
		else
		{
			// Be careful with this value!
			statusVal = 1;
		}

		return statusVal;
	}
	double getChannelRiseRate(systemParameters const & sysParams, int slot, const short unsigned int channel)
	{
		// Get channel ramp rise rate using handle and channel. Return the value
		double * voltage = 0;
		double returnVoltage = -1;

		if (DEBUG == 0)
		{
			CAENHV_GetChParam(sysParams.handle, slot, "RUp", 1, &channel, voltage);
			returnVoltage = *voltage;
		}
		else
		{
			returnVoltage = 10;
		}

		return returnVoltage;
	}
	double getChannelFallRate(systemParameters const & sysParams, int slot, const short unsigned int channel)
	{
		// Get channel ramp fall rate using handle and channel. Return the value
		double * voltage = 0;
		double returnVoltage = -1;

		if (DEBUG == 0)
		{
			CAENHV_GetChParam(sysParams.handle, slot, "RDwn", 1, &channel, voltage);
			returnVoltage = *voltage;
		}
		else
		{
			returnVoltage = 10;
		}

		return returnVoltage;
	}
	int getSystemPowerStatus(systemParameters const & sysParams)
	{
		// Get the system power status (on(1) or off(0)) using handle. Return the value
		int status = 0;
		
		if (DEBUG == 0)
		{
			// Not sure I can do this correctly, so I may just always need to send 1...?
		}
		else
		{
			// Be careful with this value!
			status = 1;
		}

		return status;
	}

	// walkFunctions
	std::vector <int> getChannelIndexList(systemParameters const & sysParams)
	{
		// Obtain all of the voltage channels using handle. Return these values in a vector of ints using push back.
		std::vector <int> channels;	

		// Find channels and push them into the vector via
		//channels.push_back(channel);
		if (DEBUG == 1)
		{
			for (unsigned int i = 0; i <= 11; i++)
			{
				channels.push_back(i);
			}
			for (unsigned int i = 100; i <= 111; i++)
			{
				channels.push_back(i);
			}
		}
		else
		{
			// I don't necessarily need to query for these values (modulo losing extensibility).
			// I can just set the values here if desired.
			// Maybe change this to an actual lookup in the future
			for (unsigned int i = 0; i <= 11; i++)
			{
				channels.push_back(i);
			}
		}
		
		return channels;
	}

	double getChannelMeasuredVoltage(systemParameters const & sysParams, int slot, const short unsigned int channel)
	{
		// Get channel measured voltage (as opposed to nominal voltage set above) using handle and channel. Return the value
		double * voltage = 0;
		double returnVoltage = -1;

		if (DEBUG == 0)
		{
			CAENHV_GetChParam(sysParams.handle, slot, "VMON", 1, &channel, voltage);
			returnVoltage = *voltage;
		}
		else
		{
			returnVoltage = 99.9;
		}

		return returnVoltage;
	}

	double getChannelCurrent(systemParameters const & sysParams, int slot, const short unsigned int channel)
	{
		// Get channel current using handle and channel. Return the value
		double * current = 0;
		double returnCurrent = -1;

		if (DEBUG == 0)
		{
			CAENHV_GetChParam(sysParams.handle, slot, "IMON", 1, &channel, current);
			returnCurrent = *current;
		}
		else
		{
			returnCurrent = .001;
		}

		return returnCurrent;
	}

	////////////////////////////////////////////////////////////////////
	// Already implemented
	////////////////////////////////////////////////////////////////////

	// Check whether the argument is contained in the validOptions vector of strings
	std::string checkValidString(std::string argument, std::vector <std::string> const & validOptions)
	{
		std::string returnValue = "";
		for (unsigned int i = 0; i < validOptions.size(); i++)
		{
			if (argument.find(validOptions.at(i)) != std::string::npos)
			{
				returnValue = argument;
				break;
			}
		}

		// Notify and exit if not found
		if (returnValue == "")
		{
			std::clog << "String \"" << argument << "\" not contained in the vector that begins with: " << validOptions.at(0) << ", " << validOptions.at(1) << std::endl;
			std::exit(-1);
		}

		return returnValue;
	}

	// Primary functions to sort the SNMP command into the various functions defined below
	// Set functions
	void setFunctions(systemParameters const & sysParams, std::string operation, std::string voltageString)
	{
		std::vector <std::string> validOperations = {"outputVoltage", "outputSwitch", "outputVoltageRiseRate", "outputVoltageFallRate", "sysMainSwitch"};

		operation = checkValidString(operation, validOperations);

		// Check if operation is completed
		bool operationCompleted = false;

		// Determine channel
		// + 1 to get past the "."
		int channelIndex = std::stoi(operation.substr(operation.find(".") + 1));
		std::clog << "ChannelIndex: " << channelIndex << std::endl;

		// Get channel and slot from index
		int slot = -1;
		int channel = -1;
		getSlotChannelFromChannelIndex(channelIndex, slot, channel);
		const short unsigned int channelArg = static_cast<short unsigned int> (channel);
		
		// For matches, we need to remove the channelIndex from operation
		operation = operation.substr(0, operation.find("."));
		std::clog << "Operation: " << operation << std::endl;
		
		// outputVoltage
		if (operation == validOperations.at(0))
		{
			// Set an individual channel and return following the appropriate format
			setChannelVoltage(sysParams, slot, channelArg, std::stod(voltageString));
			operationCompleted = true;
		}
		
		// outputSwitch
		if (operation == validOperations.at(1))
		{
			// Set the channel status (on(1) vs off(0)) and return following appropriate format
			// voltageString is poorly named here, but represents the desired channel status as an int
			setChannelStatus(sysParams, slot, channelArg, std::stoi(voltageString));	
			operationCompleted = true;
		}
		
		// outputVoltageRiseRate
		if (operation == validOperations.at(2))
		{
			// Set voltage rise rate
			// Loop over all channels in the slot because it appears that ramp rates can be set independently in the CAEN,
			// but not in the case of MPOD
			std::vector <int> channelIndexList = getChannelIndexList(sysParams);
			// Loop over channels calling setChannelRiseRate
			for (unsigned int i = 0; i < channelIndexList.size(); i++)
			{
				// Get channel
				getSlotChannelFromChannelIndex(channelIndexList.at(i), slot, channel);
				const short unsigned int channelArg = static_cast<short unsigned int> (channel);

				// Call operation
				setChannelRiseRate(sysParams, slot, channelArg, std::stod(voltageString));
			}
			operationCompleted = true;
		}

		// outputVoltageFallRate
		if (operation == validOperations.at(3))
		{
			// Set voltage fall rate
			// Loop over all channels in the slot because it appears that ramp rates can be set independently in the CAEN,
			// but not in the case of MPOD
			std::vector <int> channelIndexList = getChannelIndexList(sysParams);
			// Loop over channels calling setChannelFallRate
			for (unsigned int i = 0; i < channelIndexList.size(); i++)
			{
				// Get channel
				getSlotChannelFromChannelIndex(channelIndexList.at(i), slot, channel);
				const short unsigned int channelArg = static_cast<short unsigned int> (channel);

				setChannelFallRate(sysParams, slot, channelArg, std::stod(voltageString));
			}
			operationCompleted = true;
		}
		
		// sysMainSwitch
		if (operation == validOperations.at(4))
		{
			// Check system power status and return following appropriate format
			// voltageString is poorly named here, but represents the desired system status as an int
			setSystemPowerStatus(sysParams, std::stoi(voltageString));
			operationCompleted = true;
		}

		if (operationCompleted == false)
		{
			std::clog << "Somehow found an invalid set operation at " << __PRETTY_FUNCTION__ << ":" << __LINE__ << std::endl << "This shouldn't be possible..." << std::endl;
			std::exit(-1);
		}
	}

	std::string getFunctions(systemParameters const & sysParams, std::string operation)
	{
		std::vector <std::string> validOperations = {"outputVoltage", "outputSwitch", "outputVoltageRiseRate", "outputVoltageFallRate", "sysMainSwitch"};

		operation = checkValidString(operation, validOperations);

		std::stringstream result("");

		// Determine channel
		// + 1 to get past the "."
		int channelIndex = std::stoi(operation.substr(operation.find(".") + 1));
		std::clog << "ChannelIndex: " << channelIndex << std::endl;

		// Get channel and slot from index
		int slot = -1;
		int channel = -1;
		getSlotChannelFromChannelIndex(channelIndex, slot, channel);
		const short unsigned int channelArg = static_cast<short unsigned int> (channel);

		// For matches, we need to remove the channel from operation
		operation = operation.substr(0, operation.find("."));
		std::clog << "Operation: " << operation << std::endl;

		// outputVoltage
		if (operation == validOperations.at(0))
		{
			// Retreive an individual channel and return following the appropriate format
			double returnVoltage = getChannelVoltage(sysParams, slot, channelArg);
			result << "WIENER-CRATE-MIB::outputVoltage.u" << channelIndex << " = Opaque: Float: " << returnVoltage << " V";
		}
		
		// outputSwitch
		if (operation == validOperations.at(1))
		{
			// Check the channel status (on(1) vs off(1)) and return following appropriate format
			int returnStatus = getChannelStatus(sysParams, slot, channelArg);	
			result << "WIENER-CRATE-MIB::outputSwitch.u" << channelIndex << " = INTEGER: ";
			if (returnStatus == 0)
			{
				result << "off(0)";
			}
			else
			{
				result << "on(1)";
			}
		}
		
		// outputVoltageRiseRate
		if (operation == validOperations.at(2))
		{
			// Check voltage (slot) rise rate and return following appropriate format
			// The value on each channel should be exactly the same (but this is taken care of elsewhere)
			double returnVoltage = getChannelRiseRate(sysParams, slot, channelArg);
			result << "WIENER-CRATE-MIB::outputVoltageRiseRate.u" << channelIndex <<" = Opaque: Float: " << returnVoltage << " V/s";
		}

		// outputVoltageFallRate
		if (operation == validOperations.at(3))
		{
			// Check voltage (slot) fall rate and return following appropriate format
			double returnVoltage = getChannelFallRate(sysParams, slot, channelArg);
			result << "WIENER-CRATE-MIB::outputVoltageFallRate.u" << channelIndex <<" = Opaque: Float: " << returnVoltage << " V/s";
		}
		
		// sysMainSwitch
		if (operation == validOperations.at(4))
		{
			// Check system power status and return following appropriate format
			int returnStatus = getSystemPowerStatus(sysParams);
			result << "WIENER-CRATE-MIB::sysMainSwitch.0 = INTEGER: ";
			if (returnStatus == 0)
			{
				result << "off(0)";
			}
			else
			{
				result << "on(1)";
			}
		}

		std::clog << "result.str(): " << std::endl << result.str() << std::endl;

		if (result.str() == "")
		{
			std::clog << "Somehow found an invalid get operation at " << __PRETTY_FUNCTION__ << ":" << __LINE__ << std::endl << "This shouldn't be possible..." << std::endl;
			std::exit(-1);
		}

		return result.str();
	}

	std::string walkFunctions(systemParameters const & sysParams, std::string operation)
	{
		std::vector <std::string> validOperations = {"outputVoltage", "outputMeasurementSenseVoltage", "outputMeasurementCurrent"};

		operation = checkValidString(operation, validOperations);
		std::clog << "Operation: " << operation << std::endl;

		std::stringstream result("");
		int slot = -1;
		int channel = -1;

		// outputVoltage
		if (operation == validOperations.at(0))
		{
			// Scan through all channels, and then format all output as:
			// Find total number of channels
			std::vector <int> channelIndexList = getChannelIndexList(sysParams);
			// Loop over channels calling getChannelVoltage
			for (unsigned int i = 0; i < channelIndexList.size(); i++)
			{
				// Get channel
				getSlotChannelFromChannelIndex(channelIndexList.at(i), slot, channel);
				const short unsigned int channelArg = static_cast<short unsigned int> (channel);

				double returnVoltage = getChannelVoltage(sysParams, slot, channelArg);
				result << "WIENER-CRATE-MIB::outputVoltage.u" << channelIndexList.at(i) << " = Opaque: Float: " << returnVoltage << " V" << std::endl;
			}
		}
		
		// outputMeasurementSenseVoltage
		if (operation == validOperations.at(1))
		{
			// Scan through all channels, and then format all output as:
			// Find total number of channels
			std::vector <int> channelIndexList = getChannelIndexList(sysParams);
			// Loop over channels calling getSenseChannelVoltage
			for (unsigned int i = 0; i < channelIndexList.size(); i++)
			{
				// Get channel
				getSlotChannelFromChannelIndex(channelIndexList.at(i), slot, channel);
				const short unsigned int channelArg = static_cast<short unsigned int> (channel);

				double returnVoltage = getChannelMeasuredVoltage(sysParams, slot, channelArg);
				result << "WIENER-CRATE-MIB::outputMeasurementSenseVoltage.u" << channelIndexList.at(i) << " = Opaque: Float: " << returnVoltage << " V" << std::endl;
			}
		}
		
		// outputMeasurementCurrent
		if (operation == validOperations.at(2))
		{
			// Scan through all channels, and then format all output as:
			// Find total number of channels
			std::vector <int> channelIndexList = getChannelIndexList(sysParams);
			// Loop over channels calling getCurrentOfChannel
			for (unsigned int i = 0; i < channelIndexList.size(); i++)
			{
				// Get channel
				getSlotChannelFromChannelIndex(channelIndexList.at(i), slot, channel);
				const short unsigned int channelArg = static_cast<short unsigned int> (channel);

				double returnCurrent = getChannelCurrent(sysParams, slot, channelArg);
				result << "WIENER-CRATE-MIB::outputMeasurementCurrent.u" << channelIndexList.at(i) << " = Opaque: Float: " << returnCurrent << " A" << std::endl;
			}
		}

		// result already contains an endl for each line
		std::clog << "result.str(): " << std::endl << result.str();

		if (result.str() == "")
		{
			std::clog << "Somehow found an invalid walk operation at " << __PRETTY_FUNCTION__ << ":" << __LINE__ << std::endl << "This shouldn't be possible..." << std::endl;
			std::exit(-1);
		}
		
		return result.str();
	}

}
