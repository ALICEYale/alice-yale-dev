#ifndef PARSESNMP_H
#define PARSESNMP_H

#include <string>
#include <vector>

#define DEBUG 1

namespace parseSNMP
{
	// This was meant to be more full featured. It doesn't need to be right now, but may be in the future
	struct systemParameters
	{
		systemParameters(int _handle): handle(_handle) {}
		int handle;
	};

	////////////////////////////////////////////////////////////////////
	// To be implemented via CAENHVWrapper API Calls!
	////////////////////////////////////////////////////////////////////
	
	// setFunctions
	void setChannelVoltage(systemParameters const & sysParams, int slot, const short unsigned int channel, double voltage);
	// Channel Status turns the channel on (1) and off (2)
	void setChannelStatus(systemParameters const & sysParams, int slot, const short unsigned int channel, int status);
	void setChannelRiseRate(systemParameters const & sysParams, int slot, const short unsigned int channel, double voltage);
	void setChannelFallRate(systemParameters const & sysParams, int slot, const short unsigned int channel, double voltage);
	void setSystemPowerStatus(systemParameters const & sysParams, int status);

	// getFunctions
	double getChannelVoltage(systemParameters const & sysParams, int slot, const short unsigned int channel);
	// Channel Status turns the channel on (1) and off (2)
	int getChannelStatus(systemParameters const & sysParams, int slot, const short unsigned int channel);
	double getChannelRiseRate(systemParameters const & sysParams, int slot, const short unsigned int channel);
	double getChannelFallRate(systemParameters const & sysParams, int slot, const short unsigned int channel);
	int getSystemPowerStatus(systemParameters const & sysParams);

	// getFunctions defined for walkFunctions
	double getChannelMeasuredVoltage(systemParameters const & sysParams, int slot, const short unsigned int channel);
	double getChannelCurrent(systemParameters const & sysParams, int slot, const short unsigned int channel);

	// Returns the channel number of every channel in the system
	std::vector <int> getChannelIndexList(systemParameters const & sysParams);

	////////////////////////////////////////////////////////////////////
	// Already implemented
	////////////////////////////////////////////////////////////////////
	
	// Get appropriate channel
	void getSlotChannelFromChannelIndex(int channelIndex, int & slot, int & channel);

	// Check whether the argument is contained in the validOptions vector of strings
	std::string checkValidString(std::string argument, std::vector <std::string> const & validOptions);

	// Primary functions to sort the SNMP command into the various functions defined below
	void setFunctions(systemParameters const & sysParams, std::string operation, std::string voltageString);
	std::string getFunctions(systemParameters const & sysParams, std::string operation);
	std::string walkFunctions(systemParameters const & sysParams, std::string operation);
}

#endif /* parseSNMP.h */
